({
    doInit : function(component, event, helper) {
        helper.getDataHelper(component, event);
        
    },  
    onCheckAll: function(component, event, helper) {
        var checkBoxAll = [];
        component.find("boxPack") == undefined ? null : (component.find("boxPack").length == undefined ? checkBoxAll.push(component.find("boxPack")) : (checkBoxAll =  component.find("boxPack"))); 
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        var valueReason = component.find("assignReasonAllSelect");
        
        if(component.get("v.selectAll")){
            
            for(var i in checkBoxAll){
                checkBoxAll[i].set('v.value',true);  
                lstReason[i].set("v.value",valueReason.get("v.value"));
            }
            var mydata = component.get("v.mydata");
            var selectedCombo = [];
            for(var i in mydata){
                mydata[i].SelectedRow = true;
                var row =JSON.parse('{"Id":"'+mydata[i].Id+'","motivo":"'+valueReason.get("v.value")+'"}');
                selectedCombo.push(row);
            }
            component.set("v.SelectedCombo",selectedCombo);
            component.set("v.mydata",mydata);
            
        }else if(!component.get("v.selectAll")){
            for(var i in checkBoxAll){
                checkBoxAll[i].set('v.value',false);  
                lstReason[i].set("v.value","");
            }
            
            // resetear mi data principal
            var mydata = component.get("v.mydata");
            for(var i in mydata){
                mydata[i].SelectedRow = false;
            }
            component.set("v.mydata",mydata);
            // resetear el respaldo del los combos seleccionados
            component.set("v.SelectedCombo",[]);
        }
    },
    offCheck: function(component, event, helper) {        
        var idCombo = event.getSource().get("v.name"); // id del combo
        var isSelected = event.getSource().get("v.value"); // seleccionado o no
        var rowIndex = event.getSource().get("v.text"); // posicin del registro
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        if (!isSelected){
            lstReason[rowIndex].set("v.value","");
            helper.refreshLstReasonByCombo(component, event);
        }else if (isSelected && lstReason[rowIndex].get("v.value") == ""){
            var valueReason = component.find("assignReasonAllSelect");
            lstReason[rowIndex].set("v.value",valueReason.get("v.value"));
            helper.refreshLstReasonByCombo(component, event);
        }        
    },
    assignReasonAll: function(component, event, helper) {        
        debugger;
        var valueReason = component.find("assignReasonAllSelect");
        var selectedCombo = component.get("v.SelectedCombo");
        for(var i in selectedCombo){
            selectedCombo[i].motivo = valueReason.get("v.value");
        }
        component.set("v.SelectedCombo",selectedCombo);
        
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        var checkBoxAll = [];
        component.find("boxPack") == undefined ? null : (component.find("boxPack").length == undefined ? checkBoxAll.push(component.find("boxPack")) : (checkBoxAll =  component.find("boxPack"))); 
        for(var i in checkBoxAll){
            for(var j in selectedCombo){
                if(checkBoxAll[i].get('v.name') == selectedCombo[j].Id){
                    lstReason[i].set('v.value',selectedCombo[j].motivo);
                }
            }
        } 
    },
    onSelectReason: function (component, event, helper) {        
        debugger;
        var selectedCombo = component.get("v.SelectedCombo"); 
        var idCombo = event.getSource().get("v.name"); // id del combo
        var reasonValue = event.getSource().get("v.value");
        var rowIndex = event.getSource().get("v.tabindex")
        // agregar valor a la pagina actual
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        
        var foundIdCombo = selectedCombo.filter(function(item,i) { return item.Id === idCombo;});
        foundIdCombo.length > 0 ? lstReason[rowIndex].set("v.value",reasonValue) : lstReason[rowIndex].set("v.value","");
                
        //registarlo en el respaldo
        for(var i in selectedCombo){
            if(idCombo == selectedCombo[i].Id){
                selectedCombo[i].motivo = reasonValue;
            }
        }
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpenCancel" attribute to "true"
        component.set("v.isOpenCancel", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpenCancel" attribute to "Fasle"  
        component.set("v.isOpenCancel", false);
        component.set("v.isOpenAprovals", true);
    },
    cancelWS: function(component, event, helper) {
        var notReason = false;
        var lstCombos = component.get("v.SelectedCombo") == undefined ? [] : component.get("v.SelectedCombo");
        if (lstCombos.length > 0 ){
            for (var i in lstCombos){
                if (lstCombos[i].motivo == "")
                    notReason = true; 
            }
            if(notReason){
                helper.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel01"));
            }else{
                helper.callWebServiceCancel(component, lstCombos);
                component.set("v.isOpenCancel", false);
            }
        }else{
            helper.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_MsgCancel02"));
        }
    },
    setValues: function(component, event, helper) {
        debugger;
        helper.setValues(component, event);
        var selectedCombo = component.get("v.SelectedCombo");
        var currentListTable = component.get("v.currentList"); 
        for(var i=0; i < currentListTable.length; i++){            
            var foundIdCombo = selectedCombo.filter(function(item) { return item.Id === currentListTable[i].Id; });
            if (foundIdCombo.length > 0){
                currentListTable[i].SelectedRow = true;
            }else{
                currentListTable[i].SelectedRow = false;
            }
        }       
        component.set("v.currentList",currentListTable);
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        var checkBoxAll = [];
        component.find("boxPack") == undefined ? null : (component.find("boxPack").length == undefined ? checkBoxAll.push(component.find("boxPack")) : (checkBoxAll =  component.find("boxPack"))); 
        for(var i in checkBoxAll){
            for(var j in selectedCombo){
                if(checkBoxAll[i].get('v.name') == selectedCombo[j].Id){
                    lstReason[i].set('v.value',selectedCombo[j].motivo);
                }
            }
        } 
    },
    
    
})