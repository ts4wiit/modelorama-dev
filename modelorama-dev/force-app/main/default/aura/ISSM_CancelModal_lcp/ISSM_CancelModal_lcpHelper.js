({
    getDataHelper : function(component, event) {
        var action = component.get("c.getComboRecords");
        action.setParams({
            strObjectName : 'ISSM_Combos__c'
        });
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if(state === 'SUCCESS'){
                debugger;
                component.set("v.isOpenCancel", true);
                component.set("v.mycolumns", response.getReturnValue().lstDataTableColumns);
                var strMotivos = response.getReturnValue().motivos; 
                //response.getReturnValue().motivos = JSON.parse(strMotivos);
                component.set("v.motivos", JSON.parse(strMotivos));               
                
                if(response.getReturnValue().lstDataTableData.length > 0){
                    var lstCombosData = response.getReturnValue().lstDataTableData;
                    
                    var newLstCombosData = [];
                    for(var i in lstCombosData){
                        var objetoCombo = JSON.stringify(lstCombosData[i]);
                        objetoCombo = objetoCombo.replace('"}','","SelectedRow":false}');
                        var newjson = JSON.parse(objetoCombo);
                        newLstCombosData.push(newjson);
                    }
                    component.set("v.mydata", newLstCombosData);
                    this.setValues(component,newLstCombosData);
                }else{
                    this.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_MsgCancel03"));// muestra alerta
                }
                
            }else if (state === 'ERROR'){
                var errors = response.getError();
                var strError = ' ';
                if (errors.length>0) {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),errors[0].pageErrors[0].message);// muestra alerta
                } else {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel04"));// muestra alerta
                }
            }else{
                this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel05"));// muestra alerta
            }
        });
        $A.enqueueAction(action);	
    },
    setValues: function (component,LstProd){
        //component.set("v.wrapperList",LstProd);//setea en la variable ObjCall del componente lo que retorna el metodo Apex        
        var LstProd = component.get("v.mydata");   
        component.set("v.maxPage", Math.floor((LstProd.length+4)/10));//setea el numero de paginas
        var pageNumber = component.get("v.pageNumber"); // obtiene el numero de pagina
        var currentListTable = LstProd.slice((pageNumber-1)*10, pageNumber*10);//obtiene el numero de paginas
        component.set("v.currentList", currentListTable);//setea los productos de acuerdo a la paginación
    },
    callWebServiceCancel : function(component, combos) { 
        debugger;
        console.log("JSON.stringify(combos) : "+JSON.stringify(combos));
        var action = component.get("c.cancelCombos"); 
        action.setParams({
            strCombos : JSON.stringify(combos)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.isOpenAprovals", true);
                this.showToast("success","Success",$A.get("$Label.c.TRM_MsgCancel06"));// muestra alerta
            }else if (state === 'ERROR'){
                var errors = response.getError();
                var strError = '';
                if (errors.length>0) {
                    component.set("v.isOpenCancel", true);
                    component.set("v.selectAll",false);
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),errors[0].pageErrors[0].message);// muestra alerta
                } else {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel04"));// muestra alerta
                }
            }else{
                this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel05"));// muestra alerta
            }
        });
        $A.enqueueAction(action);
    },
    //Muestra alerta con los parametros enviados
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": type,
            "title": title,
            "message": msg
        });
        resultsToast.fire();
    },
    refreshLstReasonByCombo: function (component, event) {
        var selectedComboBack = component.get("v.SelectedCombo");
        var sizeSelectedComboBack = selectedComboBack.length;
        var selectedComboBackNew=[];

        var idCombo = event.getSource().get("v.name"); 
        var isSelected = event.getSource().get("v.value"); 
        var rowIndex = event.getSource().get("v.text");         
        var selectedCombo=[];
        var selectedRows = [];
        component.find("boxPack") == undefined ? null : (component.find("boxPack").length == undefined ? selectedRows.push(component.find("boxPack")) : (selectedRows =  component.find("boxPack"))); 
        var lstReason = []; 
        component.find("motivoSeleccted")== undefined ? null : (component.find("motivoSeleccted").length == undefined ? lstReason.push(component.find("motivoSeleccted")) : (lstReason =  component.find("motivoSeleccted")));
        debugger;
            if(isSelected){
                var motivo =  lstReason[rowIndex].get("v.value");
                var row =JSON.parse('{"Id":"'+idCombo+'","motivo":"'+motivo+'"}');
                for (var j = 0; j < sizeSelectedComboBack; j++){
                    if(idCombo !== selectedComboBack[j].Id){
                        selectedComboBackNew.push(selectedComboBack[j]);
                    	}
                    }
                selectedCombo.push(row);
            }else{
                lstReason[rowIndex].set("v.value","");
                for (var j = 0; j < sizeSelectedComboBack; j++){
                    if(idCombo !== selectedComboBack[j].Id){
                        selectedComboBackNew.push(selectedComboBack[j]);
                    }
                }
            }                       
        selectedComboBackNew.forEach(function(element) {selectedCombo.push(element);});
        component.set("v.SelectedCombo",selectedCombo);
        debugger;
    },
})