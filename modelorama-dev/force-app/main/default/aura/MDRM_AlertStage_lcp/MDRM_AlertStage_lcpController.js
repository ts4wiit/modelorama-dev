({
   //Execute Alert
   startCmp : function(cmp, event, helper) {
        // cmp.set("v.isOpen", true);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title :     cmp.get("v.title"),
            message:    cmp.get("v.message"),
            duration:   cmp.get("v.duration"),
            key:        cmp.get("v.key"),
            type:       cmp.get("v.type"),
            mode:       cmp.get("v.mode")
        });
        toastEvent.fire();
    }
})