({
	doInit : function(component, event, helper){
		helper.initMaps(component);
		helper.renderPage(component);
		var isEditMode = component.get('v.isEditMode');
		if(component.get('v.isEditMode')){
			component.set('v.setPreselectedRows', true);
		}

	},

	/** handles the change of the preselected map of customer to set the attribute selected map **/
	changePreselectedMap : function(component, event, helper){
		var preselectedMap = component.get('v.preselectedMap');
		//console.log('preselectedMap:' + preselectedMap.size);
		if(preselectedMap.size > 0){
			component.set('v.selectedMap', preselectedMap);
		}
		
	},

	/** handles the change of the data to call a function that renders the current page **/
	handleChangeData : function(component, event, helper){
		/*if(component.get('v.setPreselectedRows')){
			var selectedMap = component.get('v.selectedMap');
			var preselectedMap = component.get('v.preselectedMap');
			var customerIdList = component.get('v.customerIdList');
			var data = component.get('v.data');

			//console.log('selectedMap:' + selectedMap.size);
			//console.log('customerIdList:' + customerIdList);

			//console.log('are there preselected?');
	        var preselectList = data.filter( function(item){
	        	return customerIdList.includes( item.Id );
	        } );
	        //console.log('preselect:' + preselectList.length);
	        for(var ps = 0; ps < preselectList.length; ps++){
	            preselectedMap.set(preselectList[ps].Id, preselectList[ps]);
	        }
	        component.set('v.selectedMap', preselectedMap);
	        var selectedList = Array.from( preselectedMap.values() );
			component.set('v.selectedList', selectedList);
			component.set('v.setPreselectedRows', false);
		}*/
		if(component.get('v.setPreselectedRows')){
			var preselectedMap = component.get('v.preselectedMap');
			var data = component.get('v.data');
			for(var d = 0; d < data.length; d++){
	            preselectedMap.set(data[d].Id, data[d]);
	        }
	        component.set('v.selectedMap', preselectedMap);
	        var selectedList = Array.from( preselectedMap.values() );
			component.set('v.selectedList', selectedList);
			component.set('v.setPreselectedRows', false);
		}
        /****************************************************/
		
		var pageNumber = component.get('v.pageNumber');
		if( pageNumber != 1 ){
			component.set('v.pageNumber', 1);
		}
		helper.renderPage(component);
	},

	/** function to clear the map and list of selected customers when the filters of customer are changed **/
	changeUpdatedFilters : function(component, event, helper){
		var updatedFilters = component.get('v.updatedFilters');
		if(updatedFilters){
			var selectedMap = new Map();
			component.set('v.selectedMap', selectedMap);
			var selectedList = [];
			component.set('v.selectedList', selectedList);
			component.set('v.updatedFilters', false);
		}
	},

	/** function to handle the change of the page size, the new page must be rendered with new records **/
	handleChangePageSize : function(component, event, helper){
		
		var pageNumber = component.get('v.pageNumber');
		if( pageNumber != 1 ){
			component.set('v.pageNumber', 1);
		}
		helper.renderPage(component);
	},

	/** function to handle the change of the page number, the new page must be rendered with new records **/
	handleChangePageNumber : function(component, event, helper){
		
		helper.renderPage(component);
	},

	/*renderPage : function(component, event, helper) {
		
		helper.renderPage(component);
		
	},*/

	/** function to select all the records in the data at once **/
	onChangeSelectAll : function(component, event, helper){
		var selectAll = component.get('v.selectAll');

		var data = component.get('v.data');
		var selectedMap = component.get('v.selectedMap');

		if(selectAll){
			for(var i = 0; i < data.length; i++){
				selectedMap.set(data[i].Id, data[i]);
			}
		} else{
			for(var i = 0; i < data.length; i++){
				selectedMap.delete(data[i].Id);
			}
		}

		component.set('v.selectedMap', selectedMap);
		var selectedList = Array.from( selectedMap.values() );
		component.set('v.selectedList', selectedList);

		helper.renderPage(component);
	},

	/** hadles the selection of each row of the table **/
	handleOnrowselection : function(component, event, helper){
		
		var customerDataTable = component.find("customerDataTable");
		var selectedRows = customerDataTable.getSelectedRows();

		var currentList = component.get('v.currentList');

		/**delete selectedRows from currentList to get the notSelectedRows**/
		var notSelectedRows = currentList.filter( function(item){
			return !selectedRows.includes( item );
		});

		var selectedMap = component.get('v.selectedMap');
		
		for(var s = 0; s < selectedRows.length; s++){
			selectedMap.set(selectedRows[s].Id, selectedRows[s]);
			
		}
		for(var n = 0; n < notSelectedRows.length; n++){
			selectedMap.delete(notSelectedRows[n].Id);
		}
		component.set('v.selectedMap', selectedMap);
		var selectedList = Array.from( selectedMap.values() );
		component.set('v.selectedList', selectedList);
	},

	/** call the function to update the selected list of customers **/
	handleChangeSelectedList : function(component, event, helper){
		
		helper.updateCustomerList(component);
	},

	/** call the function to update the selected map of customers **/
	handleChangeSelectedMap : function(component, event, helper){
		
		helper.updateCustomerMap(component);
	}
})