({
    /** init the attributes of type map **/
    initMaps : function(component){
        
        var selectedMap = new Map();
        var preselectedMap = new Map();

        component.set('v.selectedMap', selectedMap);
        component.set('v.preselectedMap', preselectedMap);
    },

    /** function to render the records of the current page **/
    renderPage : function(component){
        
        var selectedList = component.get('v.selectedList');
        
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.data");
        var pageNumber = component.get("v.pageNumber"); // obtiene el numero de pagina
        var currentList = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);//obtiene el numero de registros por pagina
        
        //NEW
        var selectedMap = component.get('v.selectedMap');
        var selectedMapKeys = Array.from(selectedMap.keys());

        var toSelectList = currentList.filter( function(item){
            return selectedMapKeys.includes( item.Id );
        } );
        // END NEW

        //NEW
        var selectedRows = [];
        for(var t = 0; t < toSelectList.length; t++){
            selectedRows.push(toSelectList[t].Id);
        }
        //END NEW

        var customerDataTable = component.find("customerDataTable");
        customerDataTable.set('v.selectedRows', selectedRows);

        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        //this.renderCurrentList(component, currentList);
        component.set("v.currentList", currentList);
    },

    /*renderCurrentList : function(component, currentList){
        
        component.set("v.currentList", currentList);
    },*/

    /** fire an event to notify the new list of selected customers **/
    updateCustomerList : function(component){
        
        var selectedList = component.get('v.selectedList');
        //console.log('selectedList length:' + selectedList.length);
        var updateEvent = component.getEvent("updateCustomerList");
        updateEvent.setParams({ 
            "recordList": selectedList
        });
        updateEvent.fire();
    },

    /** fire an event to notify the new map of selected customers **/
    updateCustomerMap : function(component){
        
        var selectedMap = component.get('v.selectedMap');
        var updateEvent = component.getEvent("updateCustomerMap");
        updateEvent.setParams({ 
            "recordMap": selectedMap
        });
        updateEvent.fire();
    }
})