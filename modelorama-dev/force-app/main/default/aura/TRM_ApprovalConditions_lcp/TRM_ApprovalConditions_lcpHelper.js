({	 /** get filtered results by key word, from Apex controller **/	
        getRecords: function (component) {
            // call the apex class method 
            var action = component.get("c.searchByKeyWord");
            // set param to method  
            action.setParams({
                'objectType': component.get('v.objectAPIName'),
                'fieldOrderByList': component.get('v.fieldOrderByList'),
                'numberOfRowsToReturn': component.get('v.rowsToReturnOnSearchByWord'),
                'keyWord': component.get("v.SearchKeyWord"),
                'recordType': component.get('v.searchRecordType'),
                'typeCondition': component.get ('v.typeCondition'),
                'fieldSetName': 'TRM_NameColumns',
                'operation': component.get("v.operation")
                });
            // set a callBack    
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    debugger;
                    component.set("v.columns", response.getReturnValue().lstDataTableColumns);
                    component.set('v.foundRecords', true);
                    if(response.getReturnValue().lstDataTableData.length > 0){  
                        response.getReturnValue().lstDataTableData.length >  component.get('v.pageSize') ? component.set('v.showPagination', true) : component.set('v.showPagination', false);
                        component.set("v.records", response.getReturnValue().lstDataTableData);
                        this.saveSelectedRecords(component);//guarda los seleccionados
                        this.showCheckedRecords(component);// renderiza y pinta los seleccionados
                        
                    }else{
                        component.set('v.showPagination', false);
                        component.set("v.records", response.getReturnValue().lstDataTableData); 
                        this.saveSelectedRecords(component);
                        this.renderPage(component);
                        this.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_NoResults"));
                    } 
                    
                }
            });
            $A.enqueueAction(action);
        },
        /** render the updated page of records **/
        renderPage: function (component) {
            var pageSize = component.get("v.pageSize");
            var records = component.get("v.records");
            component.set("v.maxPage", Math.floor((records.length + (pageSize - 1))/pageSize));//setea el numero de paginas
            var pageNumber = component.get("v.pageNumber"); // obtiene el numero de pagina
            var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);//obtiene el numero de registros por pagina
            component.set("v.currentList", pageRecords);//setea los registros de acuerdo a la paginación
        },
        //Delete backUp records (selectedCombo) that were not selected if applicable and 
            //Add the selected ones if they are not in the backUp (selectedCombo)
        saveSelectedRecords: function (component) {
            var dTable = component.find("tableConditions");        
            var selectedRows = dTable.getSelectedRows() !== undefined ? dTable.getSelectedRows() : []; 
            var selectedCombo = component.get("v.SelectedCombo");
            var currentListTable = component.get("v.currentList");
            var countSelectedRows = dTable.getSelectedRows() === undefined ? 0 : dTable.getSelectedRows().length;
            for (var i = 0; i < currentListTable.length; i++){
                if(selectedRows.some(elem => elem.Id === currentListTable[i].Id)){
                    var auxInclude = false;
                    selectedCombo.forEach(function(item) {
                        if(item.Id === currentListTable[i].Id) 
                            auxInclude = true;
                });
                    if (!auxInclude){
                        selectedCombo.push(currentListTable[i]);
                    }
                }else{
                    selectedCombo.slice().reverse().forEach(function(item, index, object) {
                        if (item.Id ===currentListTable[i].Id) {
                            selectedCombo.splice(object.length - 1 - index, 1);
                        }
                    });
                }
            }
        },
        //Refresh the data for the next page or search (Paint the selected or unselected)
        showCheckedRecords: function (component) {
            var dTable = component.find("tableConditions");  
            var selectedCombo = component.get("v.SelectedCombo");
            this.renderPage(component);
            var selectedRowsIds = [];
            var currentListTable = component.get("v.currentList");
            for (var i = 0; i < currentListTable.length; i++){
                if(selectedCombo.some(elem => elem.Id === currentListTable[i].Id)){
                    selectedRowsIds.push(currentListTable[i].Id);
                }
            }
            dTable.set("v.selectedRows", selectedRowsIds); 
        },
        //Shows alert with the parameters sent
        showToast: function (type, title, msg) { 
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type": type,
                "title": title,
                "message": msg 
            });
            resultsToast.fire();
        },

        sendToApprove: function (component,conditionClass) {
            debugger;
        conditionClass.forEach(function(element) {
          delete element.TRM_Description__c;
          delete element.TRM_ConditionClass__c;
          delete element.TRM_AccessSequence__c;
        });    
        var action = component.get("c.appprovalForIntegrate"); 
        action.setParams({
            strCondition : JSON.stringify(conditionClass),
            operation: component.get("v.operation"),
            typeCondition: component.get ('v.typeCondition')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.isOpenAprovals", true);
                this.showToast("success",$A.get("$Label.c.TRM_Success"),$A.get("$Label.c.TRM_ProcessSuccessfully"));
            }else if (state === 'ERROR'){
                var errors = response.getError();
                var strError='';
                if (errors.length>0) {         
                    component.set("v.isOpenSendSAP", true);
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),errors[0].pageErrors[0].message);
                    
                } else {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel04"));
                }
            }else{
                this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel05"));
            }
        });
        $A.enqueueAction(action);
        }
    })