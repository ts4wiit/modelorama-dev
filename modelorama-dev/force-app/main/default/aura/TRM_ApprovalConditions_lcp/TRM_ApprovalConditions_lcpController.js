({
    doInit : function(component, event, helper){	
        helper.getRecords(component);
        /*var columns = [
            {fieldName: 'Name', label: 'Condition Class Id', type: 'link', typeAttributes: {label: { fieldName: 'name' }}},
            {fieldName: 'TRM_ConditionClass__c', label: 'Condition Class', type: 'picklist'},
            {fieldName: 'TRM_AccessSequence__c', label: 'Access Sequence', type: 'picklist'},
            {fieldName: 'TRM_Status__c', label: 'Status', type: 'picklist'},
            {fieldName: 'TRM_StartDate__c', label: 'Start Date', type: 'string'},
            {fieldName: 'TRM_EndDate__c', label: 'End Date', type: 'string'}
        ];
    component.set('v.columns', columns);*/
    },
    closeModel: function(component, event, helper){
        component.set("v.isOpenApprovalCondition", false);
        component.set("v.isOpenAprovals", true);
    },
    renderPage: function(component, event, helper){
        helper.saveSelectedRecords(component);//guarda los seleccionados
        helper.showCheckedRecords(component);// renderiza y pinta los seleccionados    
    },
    searchKeyChange : function(component, event, helper){
        // control of the page for the search
        var pageNum =  component.get("v.pageNumber");
        component.get("v.SearchKeyWord") === "" ? component.set("v.pageNumber",pageNum) : component.set("v.pageNumber",1);
        helper.getRecords(component);
    },
    getSelectedName : function(component, event, helper){
        helper.addOrDeleteAllRecords(component, event);
    },
    sendToApprove: function(component, event,helper){
        helper.saveSelectedRecords(component);//guarda los seleccionados en la ultima pagina
        var dTable = component.find("tableConditions");
        var lstConditions = component.get("v.SelectedCombo").length > 0 ? component.get("v.SelectedCombo") : dTable.getSelectedRows();
        if (lstConditions.length > 0 ){
            helper.sendToApprove(component,lstConditions);
            component.set("v.isOpenApprovalCondition", false);
        }else{
            helper.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_MsgCancel02"));// muestra alerta
        }
    }
})