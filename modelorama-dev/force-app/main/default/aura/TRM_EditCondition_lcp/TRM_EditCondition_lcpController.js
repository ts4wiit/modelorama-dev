({
	doInit : function(component, event, helper){
        helper.getRecordById(component);
        helper.getCatalogs(component);
        helper.getSructures(component);
    },

    /** get the Custom Metadata Types when the attribute 'externalKey' **/
    handleChangeExternalKey : function(component, event, helper){
        if(component.get('v.externalKey') != null || component.get('v.externalKey') != ''){
            helper.getConditionManagement(component);
            helper.getConditionRelationship(component);
        }
    },

    /** call the function to get the Id List of selected customer when the 'renderCustomerStep' is true **/
    changeRenderCustomerStep : function(component, event, helper){
        if( component.get('v.renderCustomerStep') ){
            helper.getCustomerIdList(component);
        }
    },

    /*handleOnClickSave : function(component, event, helper){
        component.set('v.doSave', true);
        
    },*/

    /** event action handler to get from server the updated record **/
    updatedRecord : function(component, event, helper){
        var updated = event.getParam("updated");
        if(updated){
            helper.getRecordById(component);
        }
    },

    /** set the attribute 'closeModal' to true when the user clicks on the close button **/
	closeModal : function(component, event, helper) {
        component.set('v.closeModal', true);
    },

    /** function to redirect when the closing is confirmed by clicking a button **/
    onClickAccept : function(component, event, helper){
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "TRM_ConditionClass__c"
        });
        homeEvent.fire();
    },

    /** event action handler to redirect when the closing is confirmed **/
    closeModalAction : function(component, event, helper){
        var confirmed = event.getParam("confirmed");

        if(confirmed){
            var objectApiName = component.get('v.objectApiName');
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": objectApiName
            });
            homeEvent.fire();
        }
    },

    updateRecordFields : function(component, event, helper){
        var recordFieldValueMap = event.getParam("recordFieldValueMap");
        for ( var [clave, valor] of recordFieldValueMap ) {
            if(clave == 'TRM_Quota__c'){
                component.set('v.quota', valor);
            }
        }
    }
})