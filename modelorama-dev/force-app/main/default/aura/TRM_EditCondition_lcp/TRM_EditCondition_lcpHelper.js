({
    /** Method to get the TRM_ConditionClass__c record with the attribute v.recordId **/
	getRecordById : function(component) {
        var action = component.get("c.getRecordById");
        var objectType = component.get('v.objectApiName');
        var id = component.get('v.recordId');
        action.setParams({objectType: objectType, id: id});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                console.log('Condition class record retrieved:' + a.getReturnValue());
                component.set('v.condClassRecord', returnValue);
                component.set('v.externalKey', returnValue.TRM_ConditionClass__c + '_' + returnValue.TRM_AccessSequence__c);
                component.set('v.quota', returnValue.TRM_Quota__c   );
            } else{
                console.log('No Condition class record retrieved');
            }
        });
        $A.enqueueAction(action); 
    },

    /** It calls an Apex method to get the Condition Management Custom Metadata Type **/
    getConditionManagement : function(component){
        var action = component.get("c.getConditionMetadata");
        var objectType = 'TRM_ConditionsManagement__mdt';
        var externalKey = component.get('v.externalKey');
        var isParent = true;
        action.setParams({
            objectType : objectType,
            externalKey : externalKey,
            isParent : isParent
        });
        action.setCallback(this, function(response) {
            var returnValue = response.getReturnValue();
            if(returnValue != null){
                //console.log('conditionManagement retrieved:' + returnValue);
                component.set('v.conditionManagement', returnValue);
                component.set('v.fieldSetApiName', returnValue.TRM_FieldSet__c);
                component.set('v.renderCustomerStep', returnValue.TRM_Customers__c);
            } else{
                console.log('No conditionManagement retrieved: ' + response.getError());
            }
        });
        $A.enqueueAction(action); 
    },

    /** It calls an Apex method to get the Condition Relationship Custom Metadata Type **/
    getConditionRelationship : function(component){
        var action = component.get("c.getConditionMetadata");
        var objectType = 'TRM_ConditionRelationships__mdt';
        var externalKey = component.get('v.externalKey');
        var isParent = false;
        action.setParams({
            objectType : objectType,
            externalKey : externalKey,
            isParent : isParent
        });
        action.setCallback(this, function(response) {
            var returnValue = response.getReturnValue();
            if(returnValue != null){
                //console.log('conditionRelationship retrieved:' + response.getReturnValue());
                component.set('v.conditionRelationship', returnValue);
            } else{
                console.log('No conditionRelationship retrieved: ' + response.getError());
            }
        });
        $A.enqueueAction(action); 
    },

    /** get the Id List of the selected customers in the Condition Records **/
    getCustomerIdList : function(component){
        var action = component.get("c.getCustomerIdList");
        var conditionId = component.get('v.recordId');
        action.setParams({
            conditionId : conditionId
        });
        action.setCallback(this, function(response) {
            var returnValue = response.getReturnValue();
            if(returnValue != null){
                console.log('customerIdList retrieved:' + returnValue.length);
                component.set('v.customerIdList', returnValue);
            } else{
                console.log('No customerIdList retrieved: ' + response.getError());
            }
        });
        $A.enqueueAction(action); 
    },

    //get list of catalogs for segment and price zone
    getCatalogs :function(component){
        var action = component.get("c.getCatalog");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnValue = response.getReturnValue();
                component.set('v.mapCatalogs', returnValue);
            }else if (state === 'ERROR'){
                console.log('ERROR getCatalogs-> ',JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    //Get structures for sales office and sales org.
    getSructures :function(component){

        var action = component.get("c.getStructures");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnValue = response.getReturnValue();
                component.set('v.mapStructures', returnValue);
            }else if (state === 'ERROR'){
                console.log('ERROR getCatalogs-> ',JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
})