({
	/** method to handle the action when the user select or deselect the checkbox, 
	 * for each type of action, an event is fired to notify to parent components **/
	handleSelectedRecord : function(component, event, helper) {
		

		if( component.get('v.recordType') == 'Account' ){

			var updateEvent = component.getEvent("changedCheckbox");
            updateEvent.setParams({ 
                "wrapper": component.get('v.wrapper'),
                "record": component.get('v.record'),
                "value": component.get('v.checkboxValue')
            });
            updateEvent.fire();

		} else{

			var inputCheckbox = component.find('checkRecord');
			var inputCheckboxValue = inputCheckbox.get('v.value');

			var record = component.get('v.record');
			

			if(inputCheckboxValue == true){
				var compEvent = component.getEvent("selectRecord");//Invoca el evento a registrar
				compEvent.setParams({"record" : record });//guarda el objeto seleccionado en el atributo del evento
				compEvent.fire();
			} else if(inputCheckboxValue == false){
				var compEvent = component.getEvent("deselectRecord");//Invoca el evento a registrar
				compEvent.setParams({"record" : record });//guarda el objeto seleccionado en el atributo del evento
				compEvent.fire();
			}

		}

	},
	/** method to redirect or navigate to a sObject record page **/
    navigateTosObject: function(component, event, helper){
        var record = component.get('v.record');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": record.Id
        });
        navEvt.fire();
    }
})