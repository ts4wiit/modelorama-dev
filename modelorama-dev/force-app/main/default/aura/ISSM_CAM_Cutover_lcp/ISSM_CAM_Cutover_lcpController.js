({  // obtener la lista de coolers a procesar
	doInit : function(component, event, helper) {  
		var action = component.get("c.getPeriodoCutOver"); 
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(state === 'SUCCESS'){
                debugger;
                var dateTimeShow;
                var isCutOver = false;
                var cutOverGlobal = false;
                var lstSalesofficeNotCutOver = [];
                var lstSalesofficeCutOver = [];
                var salesOfficeShowToast = '';
                var lstCutOver = response.getReturnValue();
                lstCutOver.forEach(function(cutOver) { 
                    if (cutOver.cutover && cutOver.nameCutover === $A.get("$Label.c.ISSM_CAM_NameRecordMdtCutover")){
                        dateTimeShow = cutOver;
                        cutOverGlobal = cutOver.cutover;
                        isCutOver = true;
                    }
                    if (cutOver.cutover && cutOver.nameCutover !== $A.get("$Label.c.ISSM_CAM_NameRecordMdtCutover")){
                        if(dateTimeShow === undefined) dateTimeShow = cutOver;

                        lstSalesofficeCutOver.push('\''+cutOver.nameCutover+'\'');
                        isCutOver = true;
                    }
                    if (!cutOver.cutover && cutOver.nameCutover !== $A.get("$Label.c.ISSM_CAM_NameRecordMdtCutover")){
                        lstSalesofficeNotCutOver.push('\''+cutOver.nameCutover+'\'');
                        salesOfficeShowToast = salesOfficeShowToast + cutOver.nameCutover + ', ';
                    }
                });
                if(isCutOver){
            		var startDateTime     = dateTimeShow.startDateTime.split('T');
            		var strStartDateTime  = startDateTime[0] + '  ' + startDateTime[1].split(':')[0] + ':' + startDateTime[1].split(':')[1] + 'hrs';
            		var endDateTime       = dateTimeShow.endDateTime.split('T');
            		var strEndDateTime    = endDateTime[0] + '  ' + endDateTime[1].split(':')[0] + ':' + endDateTime[1].split(':')[1] + 'hrs';  

                    component.set("v.iniciaCutover",strStartDateTime);
                    component.set("v.terminaCutover",strEndDateTime);
            		component.set("v.isOpenTable",true);            		
            		helper.getDataHelper(component, event, cutOverGlobal, lstSalesofficeCutOver, lstSalesofficeNotCutOver);

                    if(lstSalesofficeNotCutOver.length > 0 ){
                        helper.showToast("info",$A.get("$Label.c.ISSM_CAM_ShowToastInformative"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg11")+" "+salesOfficeShowToast);
                    } 
            	}else{
            		component.find("IdsaveAndExit").set("v.disabled", true);
            		component.find("IdprocessCutOver").set("v.disabled", true);
                    component.find("IdAddRecordHistory").set("v.disabled", true);
            		component.set("v.isOpenTable",false);
            		helper.showToast("info",$A.get("$Label.c.ISSM_CAM_ShowToastInformative"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg04"));
            	}
            }else{
                console.log('ERROR en getPeriodoCutOver ',response.error);
            }
        });
        $A.enqueueAction(action);  
    },
    setValues : function(component, event, helper) { 
        helper.setValues(component);
        helper.refreshCurrentListBeafore(component, event);
    },
    handleChangeSalesoffice : function(component, event, helper) { 
    	helper.handleChangeSalesoffice(component, event);
    },
    onChangeSwitch : function(component, event, helper) { 
        helper.onChangeSwitch(component, event);
    },
    onSelectReason : function(component, event, helper) { 
        helper.onSelectReason(component, event);
    },
    selectedAll : function(component, event, helper) { 
        helper.selectedAll(component, event);
        helper.setValues(component);
        helper.refreshCurrentListBeafore(component, event);
    },
    saveAndExit : function(component, event, helper) {
        component.set("v.operation",$A.get("$Label.c.ISSM_CAM_OperationSaveAndExit"));
        helper.saveAndExit(component, event);
    },
    processCutOver : function(component, event, helper) {        
        component.set("v.operation",$A.get("$Label.c.ISSM_CAM_OperationProcessCutOver"));        
        helper.saveAndExit(component, event);
    },
    closeModel : function(component, event, helper) {
        component.set("v.isOpenConfirm",false);
        component.set("v.operation",'');
    },
    confirmAction : function(component, event, helper) {
        component.find("IdprocessConfirm").set("v.disabled", true);
        var operation = component.get("v.operation");
        helper.saveRecords(component, event,operation);        
    },
    addRecordHistory : function(component, event, helper) {
        component.set("v.isOpenAddHistory", true);
    },
    HandlerAddRecordsHistory : function(component, event, helper) {
        var isOpenAddHistory = event.getParam("isOpenAddHistory");
        var lstNewRecordsHistory = event.getParam("lstNewRecordsHistory");
        if(!isOpenAddHistory) component.set("v.isOpenAddHistory", false);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
})