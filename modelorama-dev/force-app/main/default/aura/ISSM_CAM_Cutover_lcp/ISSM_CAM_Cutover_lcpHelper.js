({
    getDataHelper : function(component, event, cutOverGlobal, lstSalesofficeCutOver, lstSalesofficeNotCutOver) { 
        var action = component.get("c.getRecords");
            action.setParams({
                strObjectName           : 'ISSM_Asset__c',
                fieldOrderByList        : 'ISSM_Centre__c',
                numberOfRowsToReturn    : $A.get("$Label.c.ISSM_CAM_numberOfRowsToReturn"),
                fieldSetName            : 'ISSM_CAM_NameColCutoverTable',
                cutOverGlobal           : cutOverGlobal,
                lstSalesofficeCutOver   : lstSalesofficeCutOver,
                lstSalesofficeNotCutOver: lstSalesofficeNotCutOver,
            });
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(state === 'SUCCESS'){
                debugger;
                component.set("v.myColumns", response.getReturnValue().lstDataTableColumns);
                component.set("v.columnsTableRecodsHistory", response.getReturnValue().lstDataTableColumnsHistory);

                if(response.getReturnValue().lstDataTableData.length > 0){
                    component.set("v.reason", JSON.parse(response.getReturnValue().reason));  
                    component.set("v.myData", response.getReturnValue().lstDataTableData);
                    component.set("v.myDataChange", response.getReturnValue().lstDataTableData);
                    component.set("v.lstSalesoffice", response.getReturnValue().lstSalesOffice.length > 0 ? JSON.parse(response.getReturnValue().lstSalesOffice):[]);
                    component.set("v.cutOverHistoryList", response.getReturnValue().lstDataHistory);
                    component.set("v.cutOverHistoryBackUp", response.getReturnValue().lstDataHistory);
                    this.relationSalesOfficeCooler(component);                    
                    this.setValues(component);
                    this.refreshCurrentListBeafore(component, event); 
                }else{
                    //this.showToast("info",$A.get("$Label.c.ISSM_CAM_ShowToastInformative"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg05"));
                    component.find("IdsaveAndExit").set("v.disabled", true);
                    component.find("IdprocessCutOver").set("v.disabled", true);
                    component.find("IdAddRecordHistory").set("v.disabled", true);
                    component.set("v.isOpenTable",false);
                }
                
            }else{
                console.log('ERROR en getCoolers ',response.error);
            }
        });
        $A.enqueueAction(action);   
    },
    relationSalesOfficeCooler : function (component){
        var myData          = component.get("v.myData");
        var lstSalesoffice  = component.get("v.lstSalesoffice");
        var mapReason       = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        var mapSalesOffice  = new Map();
        myData.forEach(function(asset) {           
            lstSalesoffice.forEach(function(salesoffice) { 
                if (asset.ISSM_Centre__c === salesoffice.value){
                    var tmpLstSalesoffice = [];
                    if (mapSalesOffice.get(salesoffice.value) !== undefined){
                        tmpLstSalesoffice = mapSalesOffice.get(salesoffice.value);    
                    }                            
                    tmpLstSalesoffice.push(asset);
                    mapSalesOffice.set(salesoffice.value, tmpLstSalesoffice);
                }
            });
            if(!asset.ISSM_CutOver__c && (asset.ISSM_CutOverReason__c !== undefined|| asset.ISSM_CutOverReason__c !== '')){                
                mapReason.set(asset.Id,asset.ISSM_CutOverReason__c);                            }
        });
        component.set("v.mapReason",mapReason);
        var counter = 0;
        lstSalesoffice.forEach(function(salesoffice) {            
            if(mapSalesOffice.get(salesoffice.value) !== undefined) { 
                counter = counter + mapSalesOffice.get(salesoffice.value).length;
            }
        });
        console.log('Total de registros myData: ',myData.length);
        console.log('Total de registros agrupados por SalesOffice: ',counter);
        
        component.set("v.totalRecords", myData.length);
        component.set("v.mapSalesOffice", mapSalesOffice);
    },
    setValues: function (component){
        var pageSize = component.get("v.pageSize");
        var records  = component.get("v.myDataChange"); 
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1))/pageSize));//setea el numero de paginas
        var pageNumber  = component.get("v.pageNumber"); // obtiene el numero de pagina
        var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);//obtiene el numero de registros por pagina
        component.set("v.currentList", pageRecords);//setea los registros de acuerdo a la paginación
    },
    onChangeSwitch : function(component, event) {   
        var mapReason   = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        var isSelected  = event.getSource().get("v.value"); 
        var idCooler    = event.getSource().get("v.name"); 
        var lstReason   = []; 
        component.find("reasonSeleccted")== undefined ? null : (component.find("reasonSeleccted").length == undefined ? lstReason.push(component.find("reasonSeleccted")) : (lstReason =  component.find("reasonSeleccted")));   
        lstReason.forEach(function(componentReason) {  
            if(componentReason.get("v.name") == idCooler) { 
                if(componentReason.get("v.name") == idCooler && isSelected) {                    
                    mapReason.delete(idCooler);
                    component.set("v.mapReason",mapReason);
                    componentReason.set("v.value",'');
                }
                componentReason.set("v.disabled",isSelected);
            }
        });
    },
    handleChangeSalesoffice : function(component, event) { 
        var salesofice     = event.getSource().get("v.value"); 
        var mapSalesOffice = component.get("v.mapSalesOffice");
        var myData         = component.get("v.myData");

        if(salesofice == ''){
            component.set("v.myDataChange", myData);
            component.set("v.pageNumber",1)
            this.setValues(component);
            this.refreshCurrentListBeafore(component, event); 
        }else{
            if(mapSalesOffice.get(salesofice) !== undefined) { 
                component.set("v.myDataChange", mapSalesOffice.get(salesofice));
                component.set("v.pageNumber",1)
                this.setValues(component);
                this.refreshCurrentListBeafore(component, event);    
            }else{
                this.showToast("info",$A.get("$Label.c.ISSM_CAM_ShowToastInformative"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg06"));
            }
        } 
    },
    refreshCurrentListBeafore : function(component, event){
        var currentList         = component.get("v.currentList");
        var mapReason           = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        var rowsComponentReason = [];
        var lstReason           = []; 
        component.find("reasonSeleccted")== undefined ? null : (component.find("reasonSeleccted").length == undefined ? lstReason.push(component.find("reasonSeleccted")) : (lstReason =  component.find("reasonSeleccted")));   
 
        currentList.forEach(function(rowCooler) {  
            lstReason.forEach(function(componentReason) {  
                if(rowCooler.Id === componentReason.get("v.name") && rowCooler.ISSM_CutOver__c == true) 
                    rowsComponentReason.push(componentReason);

                if(rowCooler.ISSM_CutOver__c == false){
                    if(mapReason.get(rowCooler.Id) !== undefined && rowCooler.Id === componentReason.get("v.name")) { 
                        componentReason.set("v.value",mapReason.get(rowCooler.Id));
                    }
                    componentReason.set("v.disabled",false);
                }
            });
        });
        for(var i=0 ; i < rowsComponentReason.length ; i++){
            for(var j=0 ; j < lstReason.length ; j++){
                if(rowsComponentReason[i].get("v.name") === lstReason[j].get("v.name")){
                    lstReason[j].set("v.value",'');
                    lstReason[j].set("v.disabled",true);
                }
            }            
        }        
    },
    onSelectReason : function(component, event){
        var idCooler                = event.getSource().get("v.name"); 
        var valueComponentReason    = event.getSource().get("v.value");
        var mapReason               = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        mapReason.set(idCooler,valueComponentReason);
        component.set("v.mapReason",mapReason);
    },
    selectedAll : function(component, event){
        var isSelected  = event.getSource().get("v.value");
        var myDataChange= component.get("v.myDataChange");
        var mapReason   = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        myDataChange.forEach(function(rowCooler) {
          rowCooler.ISSM_CutOver__c = isSelected;
          if(mapReason.get(rowCooler.Id) !== undefined && isSelected) { 
              mapReason.delete(rowCooler.Id);
          }
        });
        component.set("v.myDataChange",myDataChange);

    },
    saveAndExit : function(component, event){
        var myData = component.get("v.myData");
        var allReasons  = false;
        var selectedCooler=[];
        var lstIdsUpdate=[];
        var mapCountReason = new Map(); 
        var mapCountReasonForCenter = new Map();
        var globalCounter = 0;
        var mapReason   = component.get("v.mapReason") === null ? new Map() : component.get("v.mapReason");
        myData.forEach(function(rowCooler) { 
            var row =rowCooler.Id+',,'+rowCooler.ISSM_CutOver__c;
            //valido que todos los que fueron seleccionados como INCORRECTOS tengan asignado un MOTIVO
            if(!rowCooler.ISSM_CutOver__c && (mapReason.get(rowCooler.Id) === undefined || mapReason.get(rowCooler.Id) === '')) { 
              allReasons  = true;
            }
            //contabilizamos cuantos motivos existen
            if(mapReason.get(rowCooler.Id) !== undefined && mapReason.get(rowCooler.Id) !== ''){
                //concatenecion de : idSales,descripción del motivo,false
                row =rowCooler.Id+','+mapReason.get(rowCooler.Id)+','+rowCooler.ISSM_CutOver__c;
                var counter = 0;
                if(mapCountReason.get(mapReason.get(rowCooler.Id)) === undefined){
                    counter = counter + 1;
                }else{
                    counter = mapCountReason.get(mapReason.get(rowCooler.Id)) + 1;
                }
                globalCounter = globalCounter + 1;
                mapCountReason.set(mapReason.get(rowCooler.Id),counter);
            }
           //contabilizamos cuantos motivos existen por centro
            if(mapReason.get(rowCooler.Id) !== undefined && mapReason.get(rowCooler.Id) !== ''){
                //concatenecion de : idSales,descripción del motivo,false
                var counter = 0;
                var keyMapCountReasonForCenter = rowCooler.ISSM_Centre__c+','+mapReason.get(rowCooler.Id);
                if(mapCountReasonForCenter.get(keyMapCountReasonForCenter) === undefined){
                    counter = counter + 1;
                }else{
                    counter = parseInt((mapCountReasonForCenter.get(keyMapCountReasonForCenter)).split(",")[2]) + 1;
                }
                var valueMapCountReasonForCenter = keyMapCountReasonForCenter+','+counter;
                mapCountReasonForCenter.set(keyMapCountReasonForCenter,valueMapCountReasonForCenter);
            }
            selectedCooler.push(row);
            lstIdsUpdate.push(rowCooler.Id);
        });
        

        if (!allReasons){            
            var picklistReasons = component.get("v.reason");
            var listReasonForConfirm = [];
            picklistReasons.forEach(function(itemReason) { 
                if(mapCountReason.get(itemReason.value) !== undefined){
                   debugger; 
                   var reason = '* '+mapCountReason.get(itemReason.value) + '  '+$A.get("$Label.c.ISSM_CAM_AbstractCutoverReason")+'  "' + itemReason.label + '"';
                   listReasonForConfirm.push(reason);
                }
            });
            if(listReasonForConfirm.length <= 0){
                var reason = $A.get("$Label.c.TRM_CAM_TotalRecordsAbstractS") + '('+component.get("v.totalRecords")+') '+$A.get("$Label.c.TRM_CAM_TotalRecordsAbstractE");
                listReasonForConfirm.push(reason);                
            }else{
                var reason = '* '+ (component.get("v.totalRecords") - globalCounter) + '  '+$A.get("$Label.c.TRM_CAM_TotalRecordsAbstract");
                listReasonForConfirm.push(reason);
                
            }
            var cutOverHistoryList = component.get("v.cutOverHistoryList")
            var mapCountSurplusForCenter = new Map();
            cutOverHistoryList.forEach(function(newRecordHistory) {
                var operation = component.get("v.operation");
                if(operation === $A.get("$Label.c.ISSM_CAM_OperationSaveAndExit")){
                    newRecordHistory.ISSM_CAM_Leftover__c = false;
                    newRecordHistory.ISSM_CAM_Temporal__c = true;
                }else{
                    newRecordHistory.ISSM_CAM_Leftover__c = true;
                    newRecordHistory.ISSM_CAM_Temporal__c = false;
                }
                var counter = 0;
                if(mapCountSurplusForCenter.get(newRecordHistory.ISSM_Centre__c) === undefined){
                    counter = counter + 1;
                }else{
                    counter = mapCountSurplusForCenter.get(newRecordHistory.ISSM_Centre__c) + 1;
                }
                mapCountSurplusForCenter.set(newRecordHistory.ISSM_Centre__c,counter);                 
            });

            var picklistReasons         = component.get("v.reason");
            var picklistSalesoffice     = component.get("v.lstSalesoffice");
            var mapSalesofficeForReason = new Map();
            var mapSalesOffice          = component.get("v.mapSalesOffice");
            

            picklistSalesoffice.forEach(function(salesoffice) {
                var lstSalesoffice  = [];
                var isError         = false;
                var isSurpluse      = false;
                picklistReasons.forEach(function(reason) {
                    if(mapCountReasonForCenter.get(salesoffice.value+','+reason.value) !== undefined && mapCountReasonForCenter.get(salesoffice.value+','+reason.value) !== ''){
                        lstSalesoffice.push(mapCountReasonForCenter.get(salesoffice.value+','+reason.value)+','+salesoffice.label+',' + reason.label+','+mapSalesOffice.get(salesoffice.value).length);
                        isError = true;
                    }
                });
                if(mapCountSurplusForCenter.get(salesoffice.value) !== undefined && mapCountSurplusForCenter.get(salesoffice.value) !== undefined){
                    lstSalesoffice.push(salesoffice.value + ',' + 'Surplus equipement,' + mapCountSurplusForCenter.get(salesoffice.value)+','+salesoffice.label+',' + 'Equipo Sobrante,'+mapSalesOffice.get(salesoffice.value).length);
                    isSurpluse = true;
                }
                if(lstSalesoffice.length > 0){
                    debugger;
                    mapSalesofficeForReason.set(salesoffice.value,lstSalesoffice);
                    if(!isError && isSurpluse){
                        var lengthMapSAlesoffice = mapSalesOffice.get(salesoffice.value) === undefined ? 0 :mapSalesOffice.get(salesoffice.value).length;
                        lstSalesoffice.push(salesoffice.value + ',OK,0,'+salesoffice.label+',OK,'+lengthMapSAlesoffice);
                        mapSalesofficeForReason.set(salesoffice.value,lstSalesoffice);
                    }
                }else if(salesoffice.value !== ""){
                    debugger;
                    var lengthMapSAlesoffice = mapSalesOffice.get(salesoffice.value) === undefined ? 0 :mapSalesOffice.get(salesoffice.value).length;
                    lstSalesoffice.push(salesoffice.value + ',OK,0,'+salesoffice.label+',OK,'+lengthMapSAlesoffice);
                    mapSalesofficeForReason.set(salesoffice.value,lstSalesoffice);
                }                
            });
            debugger;
            var surplus = '* ' + cutOverHistoryList.length === undefined ? 0 : cutOverHistoryList.length + ' ' + $A.get("$Label.c.ISSM_CAM_TotalSurplusEquipment");
            listReasonForConfirm.push(surplus);
            component.set("v.mapSalesofficeForReason",mapSalesofficeForReason);
            component.set("v.cutOverHistoryList",cutOverHistoryList);
            component.set("v.listReasonForConfirm",listReasonForConfirm);
            component.set("v.selectedCooler",selectedCooler);
            component.set("v.lstIdsUpdate",lstIdsUpdate);
            component.set("v.isOpenConfirm",true);  
        }else{
            this.showToast("error",'ERROR',$A.get("$Label.c.ISSM_CAM_ShowToastMsg10"));
        } 
    },
    saveRecords: function(component, event,operation){   
        var mapSalesofficeForReason = component.get("v.mapSalesofficeForReason");
        component.find("IdprocessConfirm").set("v.disabled", true);
        var theMap = new Map();
        mapSalesofficeForReason.forEach(function(valor, clave) {
            theMap[clave]=JSON.stringify(valor);
        });
        var action = component.get("c.saveRecords");
            action.setParams({
                mapSalesofficeForReason : theMap,
                lstIdsUpdate            : component.get("v.lstIdsUpdate"),
                lstRecordsUpdate        : component.get("v.selectedCooler"),
                cutOverHistoryList		: JSON.stringify(component.get("v.cutOverHistoryList")),
                cutOverHistoryBackUp	: JSON.stringify(component.get("v.cutOverHistoryBackUp")),
                operation               : operation,
            });
            debugger;
        action.setCallback(this, function(response){            
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(operation === $A.get("$Label.c.ISSM_CAM_OperationSaveAndExit")){
                    this.showToast("success",$A.get("$Label.c.ISSM_CAM_ShowToastSuccess"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg07"));
                }else{
                    this.showToast("success",$A.get("$Label.c.ISSM_CAM_ShowToastSuccess"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg08"));
                }
                this.navHome(component, event);
            }else{
                console.log('ERROR en saveRecords ',response.error);
                component.find("IdprocessConfirm").set("v.disabled", false);
            }
        });
        $A.enqueueAction(action);  
    },
    navHome : function (component, event) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "ISSM_CutOver_History__c"
        });
        homeEvent.fire();
    },
    addRemoveClass : function(element,addcls,removecls) {
        $A.util.addClass(element, addcls);
        $A.util.removeClass(element, removecls);
    },
//Muestra alerta con los parametros enviados
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": type,
            "title": title,
            "message": msg
        });
        resultsToast.fire();
    },
})