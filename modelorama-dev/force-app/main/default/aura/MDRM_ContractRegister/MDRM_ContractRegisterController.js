({
    init: function(cmp, event, helper) {
        var accountId = cmp.get('v.recordId');
        
        /*cmp.set('v.columns', [
            {label: 'Titular', fieldName: 'linkName', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
            {label: 'Nombre', fieldName: 'MDRM_FirstName__c', type: 'text'},
            {label: 'Apellido paterno', fieldName: 'MDRM_Last_Name__c', type: 'text'},
            {label: 'Apellido materno', fieldName: 'MDRM_Second_Last_Name__c', type: 'text'}
        ]);*/
        
        helper.fetchFilePicklistOptions(cmp);
        helper.fetchEntityPicklistOptions(cmp);
        helper.getAccount(cmp, accountId);
        helper.getContract(cmp, accountId);
    },
    addMonths: function(cmp, event, helper) {
        var mdrmFile = cmp.get('v.mdrmFile');
        
        console.log('Date: ' + mdrmFile.MDRM_Start_Date__c);
        console.log('Period: ' + mdrmFile.MDRM_ContractPeriod__c);
        
        console.log('Date type: ' + typeof mdrmFile.MDRM_Start_Date__c);
        console.log('Period type: ' + typeof mdrmFile.MDRM_ContractPeriod__c);
        
        if(typeof mdrmFile.MDRM_Start_Date__c !== 'undefined' && typeof mdrmFile.MDRM_ContractPeriod__c !== 'undefined') {
            var d = new Date(mdrmFile.MDRM_Start_Date__c);
            d.setMonth(d.getMonth() + Number(mdrmFile.MDRM_ContractPeriod__c));
            
            var month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            
            if (month.length < 2) 
                month = '0' + month;
            if (day.length < 2) 
                day = '0' + day;
            
            console.log('Fecha: ' + [year, month, day].join('-'));
            
            /*d.setMonth(d.getMonth() + Number(mdrmFile.MDRM_ContractPeriod__c));
            console.log('Date: ' + d);*/
            
            mdrmFile.MDRM_Exp_Date__c = [year, month, day].join('-');
            console.log('Expiration date: ' + mdrmFile.MDRM_Exp_Date__c);
            
            cmp.set('v.mdrmFile', mdrmFile);
        }
    },
    handleOnActiveTab: function(cmp, event, helper) {
        var activeTab = event.getSource().get('v.id');
        
        //if(activeTab == 'contractTab') {
        if(activeTab == 'titularTab') {
            let mdrmFile = cmp.get('v.mdrmFile');
            helper.fetchTitular(cmp, mdrmFile.Id);
        }
        
        cmp.set('v.activeTab', activeTab);
    },
    handleSaveFile: function(cmp, event, helper) {
        var validform = cmp.find('fileForm').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if(validform){
            let mdrmFile = cmp.get('v.mdrmFile');
            let accountId = cmp.get('v.recordId');
            
            helper.saveFile(cmp, mdrmFile, accountId);
        }
    },
    handleDeleteFile: function(cmp, event, helper) {
        var mdrmFile = cmp.get('v.mdrmFile');
        
        helper.deleteDocument(cmp, mdrmFile.Id);
        helper.closeConfirmModal(cmp);
    },
    fileEditionMode: function(cmp, event, helper) {
        helper.editRecord(cmp);
    },
    blockedMode: function(cmp, event, helper) {
        helper.blockRecord(cmp);
    },
    titularEditionMode: function(cmp, event, helper) {
        helper.editTitular(cmp);
    },
    titularBlockedMode: function(cmp, event, helper) {
        helper.blockTitular(cmp);
    },
    /*handleOpenModal: function(cmp, event, helper) {
        helper.openModal(cmp);
    },*/
    /*handleCloseModal: function(cmp, event, helper) {
        helper.closeModal(cmp);
    },*/
    handleOpenConfirmModal: function(cmp, event, helper) {
        helper.openConfirmModal(cmp);
    },
    handleCloseConfirmModal: function(cmp, event, helper) {
        helper.closeConfirmModal(cmp);
    },
    handleSaveTitular: function(cmp, event, helper) {
        var validForm = cmp.find('titularForm').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if(validForm){
            //let toastEvent = $A.get('e.force:showToast');
            let titular = cmp.get('v.titular');
            let mdrmFile = cmp.get('v.mdrmFile');
            
            /*titular.MDRM_Documento__c = mdrmFile.Id;
            
            var execAction1 = cmp.get('c.saveTitular');
            execAction1.setParams({
                entidad: titular
            });
            
            helper.serverSideCall(cmp, execAction1).then(
            	function(resp) {
                    if(resp.status == 'success') {
                        toastEvent.setParams({
                            'title':'Success',
                            'type':'success',
                            'mode':'dismissable',
                            'message':resp.message
                        });
                        
                        var execAction2 = cmp.get('c.getTitular');
                        execAction2.setParams({
                            titularId: resp.id
                        });
                    } else {
                        toastEvent.setParams({
                            'title':'Error',
                            'type':'error',
                            'mode':'dismissable',
                            'message':resp.message
                        });
                    }
                    toastEvent.fire();
                    
                    return helper.serverSideCall(cmp, execAction2);
                }
            ).then(
            	function(resp) {
                    if(typeof resp !== 'undefined') {
                        console.log(resp);
                    
                        var modal = cmp.find('titularModal');
                        $A.util.removeClass(modal, 'slds-fade-in-open');
                        var modalBack = cmp.find('titularModalBackdrop');
                        $A.util.removeClass(modalBack, 'slds-backdrop_open');
                    }
                }
            );*/
            
            helper.saveTitular(cmp, titular, mdrmFile.Id);
            
            /*var modal = cmp.find('titularModal');
            $A.util.removeClass(modal, 'slds-fade-in-open');
            var modalBack = cmp.find('titularModalBackdrop');
            $A.util.removeClass(modalBack, 'slds-backdrop_open');*/
        }
    },
    //handleGetTitular: function(cmp, event, helper) {
    //Worked with: <lightning:accordion aura:id="accordion" onsectiontoggle="{!c.handleGetTitular}">
    /*var mdrmFile = cmp.get('v.mdrmFile');
        var openSections = event.getParam('openSections');

        if (openSections == 'B') {
            helper.fetchTitular(cmp, mdrmFile.Id);
        }*/
    
    //var mdrmFile = cmp.get('v.mdrmFile');
    //helper.fetchTitular(cmp, mdrmFile.Id);
    //},
    handleDeleteTitular: function(cmp, event, helper) {
        var titular = cmp.get('v.titular');
        
        helper.deleteTitular(cmp, titular);
        helper.closeConfirmModal(cmp);
    },
    handleSendToApproval: function(cmp, event, helper) {
        var toastEvent = $A.get('e.force:showToast');
        var isValid = true;
        var fieldsMissing = new Array();
        
        var formToVal = cmp.find('fileForm');
        for(let i = 0; i < formToVal.length; i++) {
            let valor = formToVal[i].get('v.value');
            let etiqueta = formToVal[i].get('v.label');
            let clase = formToVal[i].get('v.class');
            console.log(etiqueta + ': ' + valor);
            
            if(typeof valor !== 'boolean' && clase != 'no-required' && !valor) {
                isValid = false;
                fieldsMissing.push(etiqueta);
            }
        }
        
        formToVal = cmp.find('titularForm');
        for(let i = 0; i < formToVal.length; i++) {
            let valor = formToVal[i].get('v.value');
            let etiqueta = formToVal[i].get('v.label');
            let clase = formToVal[i].get('v.class');
            console.log(etiqueta + ': ' + valor);
            console.log('Clase: ' + clase);
            
            if(typeof valor !== 'boolean' && clase != 'no-required' && !valor) {
                isValid = false;
                fieldsMissing.push(etiqueta);
            }
        }
        
        console.log('Formulario valido: ' + isValid);
        
        if(isValid) {
            helper.sendToApproval(cmp);
        } else {
            toastEvent.setParams({
                'title':'Error',
                'type':'error',
                'mode':'dismissable',
                'message':'Debe de llenar la información de todos los campos: ' + fieldsMissing
            });
            toastEvent.fire();
        }
    },
    
    onChangePersonType: function(cmp, event, helper){
        
        var titular = cmp.get('v.titular');
        helper.phisycalPersonality(cmp, titular);
        //event.currentTarget
    },
    onblurRFC: function(cmp, event, helper){
        
        var titular = cmp.get('v.titular');
        helper.phisycalPersonality(cmp, titular);
        //event.currentTarget
    },
    
})