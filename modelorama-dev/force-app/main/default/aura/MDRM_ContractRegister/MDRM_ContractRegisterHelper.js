({
    fetchFilePicklistOptions: function(cmp) {
        var lfield = ['MDRM_Status__c','MDRM_ClasificationPick__c','MDRM_PaymentPeriod__c','MDRM_Way_To_Pay__c','MDRM_Currency_Type__c'];
        var mdrmFile = cmp.get('v.mdrmFile');
        var controller = cmp.get('c.fetchPicklistOptions');
        controller.setParams({
            sobj: mdrmFile,
            lpick: lfield
        });
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log('State: ' + state);
            
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                console.log('Result: ' + JSON.stringify(mpResponse));
                
                for(let fieldName in mpResponse) {
                    console.log('fieldName: ' + fieldName);
                    
                    let picklistValues = mpResponse[fieldName];
                    let picklist = [];
                    for(let value in picklistValues) {
                        console.log('Value: ' + value);
                        
                        picklist.push({value:picklistValues[value], key:value});
                    }
                    console.log('option list filled: ' + JSON.stringify(picklist));
                    
                    if(fieldName == 'MDRM_Status__c') {
                        cmp.set('v.status',picklist);
                    }
                    
                    if(fieldName == 'MDRM_ClasificationPick__c') {
                        cmp.set('v.classification',picklist);
                    }
                    
                    if(fieldName == 'MDRM_PaymentPeriod__c') {
                        cmp.set('v.payPeriod',picklist);
                    }
                    
                    if(fieldName == 'MDRM_Way_To_Pay__c') {
                        cmp.set('v.wayToPay',picklist);
                    }
                    
                    if(fieldName == 'MDRM_Currency_Type__c') {
                        cmp.set('v.currencyType',picklist);
                    }
                }
            } else {
                alert('Error en evento');
            }
        });
        $A.enqueueAction(controller);
    },
    getAccount : function(cmp, accountId) {
        var controller = cmp.get('c.getAccount');
        
        controller.setParams({
            id: accountId
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log('State: ' + state);
            
            if(state == 'SUCCESS') {
                let accResp = response.getReturnValue();
                console.log('Result: ' + JSON.stringify(accResp));
                if(accResp != null) {
                    cmp.set('v.mdrmAcc', accResp);
                }
            } else {
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    'title': 'Error',
                    'type': 'error',
                    'mode': 'dismissable',
                    'message': 'Error al consultar el Modelorama'
                });
                toastEvent.fire();
                
                return false;
            }
        });
        $A.enqueueAction(controller);
    },
    getContract : function(cmp, accountId) {
        var controller = cmp.get('c.getAccountContract');
        
        controller.setParams({
            id: accountId
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log('State: ' + state);
            
            if(state == 'SUCCESS') {
                let contractResp = response.getReturnValue();
                console.log('Result: ' + JSON.stringify(contractResp));
                if(contractResp != null) {
                    cmp.set('v.mdrmFile', contractResp);
                    this.blockRecord(cmp);
                } else {
                    this.initMdrmFile(cmp)
                    this.editRecord(cmp);
                }
            } else {
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    'title': 'Error',
                    'type': 'error',
                    'mode': 'dismissable',
                    'message': $A.get('{!Label.c.MDRM_Contracts_ErrorContrato}')
                });
                toastEvent.fire();
                
                return false;
            }
        });
        $A.enqueueAction(controller);
    },
    saveFile : function(cmp, mdrmFile, accountId) {
        var toastEvent = $A.get('e.force:showToast');
        
        mdrmFile.MDRM_Account__c = accountId;
        if(mdrmFile.MDRM_Status__c == '') {
            mdrmFile.MDRM_Status__c = 'Draft';
        }
        console.log('Cuenta: ' + accountId);
        
        var controller = cmp.get('c.saveDocument');
        controller.setParams({
            mdrmFile: mdrmFile
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log(state);
            
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                if(mpResponse.status == 'success') {
                    toastEvent.setParams({
                        'title':'Éxito',
                        'type':'success',
                        'mode':'dismissable',
                        'message':mpResponse.message
                    });
                    mdrmFile.Id = mpResponse.id;
                    
                    cmp.set('v.mdrmFile', mdrmFile);
                    this.blockRecord(cmp);
                    
                    //Validate if we already have a Titular info
                    this.fetchTitular(cmp, mpResponse.id);
                } else {
                    toastEvent.setParams({
                        'title':'Error',
                        'type':'error',
                        'mode':'dismissable',
                        'message':mpResponse.message
                    });
                }
                toastEvent.fire();
            } else {
                alert('Error en evento');
            }
        });
        $A.enqueueAction(controller);
    },
    deleteDocument: function(cmp, recordId) {
        var toastEvent = $A.get('e.force:showToast');
        var controller = cmp.get('c.deleteDocument');
        
        console.log('Document to be deleted: ' + recordId)
        
        controller.setParams({
            documentId: recordId
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log(state);
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                if(mpResponse.status == 'success') {
                    toastEvent.setParams({
                        'title': 'Éxito',
                        'type': 'success',
                        'mode': 'dismissable',
                        'message': mpResponse.message
                    });
                    this.initMdrmFile(cmp);
                    this.editRecord(cmp);
                } else {
                    toastEvent.setParams({
                        'title': 'Error',
                        'type': 'error',
                        'mode': 'dismissable',
                        'message': mpResponse.message
                    });
                }
                toastEvent.fire();
            } else {
                alert('Ocurrió un error al eliminar el documento, favor de notificar al administrador');
            }
        });
        $A.enqueueAction(controller);
    },
    initMdrmFile: function(cmp) {
        var mdrmFile = {sObjectType:'MDRM_Document__c',
                        MDRM_File_Type__c: $A.get('{!$Label.c.MDRM_Docs_Documento}'),
                        MDRM_File_Subtype__c: $A.get('{!$Label.c.MDRM_Docs_ContratoArrendamiento}'),
                        MDRM_Status__c: '',
                        MDRM_License_cost_included_in_rent__c: false,
                        MDRM_Signed_by_Legal_Department__c: false,
                        MDRM_Signed_by_Client__c: false};
        
        cmp.set('v.mdrmFile', mdrmFile);
    },
    editRecord: function(cmp) {
        cmp.set('v.isBlocked', false);
    },
    blockRecord: function(cmp) {
        cmp.set('v.isBlocked', true);
    },
    editTitular: function(cmp) {
        cmp.set('v.isTitularBlocked', false);
    },
    blockTitular: function(cmp) {
        cmp.set('v.isTitularBlocked', true);
    },
    fetchEntityPicklistOptions: function(cmp) {
        var lfield = ['MDRM_State__c','MDRM_Phisycal_Personality__c','MDRM_Type_of_counterpart__c','MDRM_Category__c'];
        var mdrmFile = cmp.get('v.titular');
        var controller = cmp.get('c.fetchPicklistOptions');
        controller.setParams({
            sobj: mdrmFile,
            lpick: lfield
        });
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log('State: ' + state);
            
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                console.log('Result: ' + JSON.stringify(mpResponse));
                
                for(let fieldName in mpResponse) {
                    console.log('fieldName: ' + fieldName);
                    
                    let picklistValues = mpResponse[fieldName];
                    let picklist = [];
                    for(let value in picklistValues) {
                        console.log('Value: ' + value);
                        
                        picklist.push({value:picklistValues[value], key:value});
                    }
                    console.log('option list filled: ' + JSON.stringify(picklist));
                    
                    if(fieldName == 'MDRM_State__c') {
                        cmp.set('v.state',picklist);
                    }
                    
                    if(fieldName == 'MDRM_Phisycal_Personality__c') {
                        cmp.set('v.personality',picklist);
                    }
                    
                    if(fieldName == 'MDRM_Type_of_counterpart__c') {
                        cmp.set('v.counterpart',picklist);
                    }
                    
                    if(fieldName == 'MDRM_Category__c') {
                        cmp.set('v.category',picklist);
                    }
                }
            } else {
                alert('Error en evento');
            }
        });
        $A.enqueueAction(controller);
    },
    initTitular: function(cmp) {
        var titular = {sObjectType:'MDRM_Contract_Entity__c',
                       MDRM_FirstName__c: '',
                       MDRM_Last_Name__c: '',
                       MDRM_Second_Last_Name__c: '',
                       MDRM_Email__c: '',
                       MDRM_Street__c: '',
                       MDRM_External_Number__c: '',
                       MDRM_Internal_Number__c: '',
                       MDRM_Suburb__c: '',
                       MDRM_City__c: '',
                       MDRM_State__c: '',
                       MDRM_Location_References__c: '',
                       MDRM_Phisycal_Personality__c: '',
                       MDRM_RFC__c: '',
                       MDRM_Type_of_counterpart__c: '',
                       MDRM_Category__c: ''};
        
        cmp.set('v.titular', titular);
    },
    saveTitular : function(cmp, titular, fileId) {
        var toastEvent = $A.get('e.force:showToast');
        //var ltitular = cmp.get('v.ltitular');
        
        titular.MDRM_Documento__c = fileId;
        
        console.log('File: ' + fileId);
        
        var controller = cmp.get('c.saveTitular');
        controller.setParams({
            entidad: titular
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log(state);
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                if(mpResponse.status == 'success') {
                    toastEvent.setParams({
                        'title':'Éxito',
                        'type':'success',
                        'mode':'dismissable',
                        'message':mpResponse.message
                    });
                    titular.Id = mpResponse.id;
                    //titular.Name = mpResponse.name;
                    //ltitular.push(titular);
                    cmp.set('v.titular', titular);
                    //this.initTitular(cmp);
                    this.phisycalPersonality(cmp, titular);
                    
                    this.blockTitular(cmp);
                } else {
                    toastEvent.setParams({
                        'title':'Error',
                        'type':'error',
                        'mode':'dismissable',
                        'message':mpResponse.message
                    });
                }
                toastEvent.fire();
            } else {
                alert('Error en evento');
            }
        });
        $A.enqueueAction(controller);
    },
    fetchTitular : function(cmp, fileId) {
        var toastEvent = $A.get('e.force:showToast');
        var controller = cmp.get('c.getTitular');
        
        controller.setParams({
            fileId: fileId
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            
            if(state == 'SUCCESS') {
                let resp = response.getReturnValue();
                
                if(resp != null) {
                    cmp.set('v.titular', resp);
                    this.phisycalPersonality(cmp, resp);
                    this.blockTitular(cmp);
                } else {
                    this.initTitular(cmp)
                    this.editTitular(cmp);
                    
                    toastEvent.setParams({
                        'title': 'Precaución',
                        'type': 'warning',
                        'mode': 'dismissable',
                        'message': $A.get('{!$Label.c.MDRM_Contracts_FaltaTitular}')
                    });
                    toastEvent.fire();
                }
            } else {
                toastEvent.setParams({
                    'title':'Error',
                    'type':'error',
                    'mode':'dismissable',
                    'message':'Error al buscar al Titular'
                });
                toastEvent.fire();
                
                return false;
            }
        });
        $A.enqueueAction(controller);
    },
    deleteTitular: function(cmp, titular) {
        var toastEvent = $A.get('e.force:showToast');
        var controller = cmp.get('c.deleteTitular');
        
        console.log('Titular to be deleted: ' + JSON.stringify(titular));
        
        controller.setParams({
            titular: titular
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log(state);
            
            if(state == 'SUCCESS') {
                let mpResponse = response.getReturnValue();
                
                if(mpResponse.status == 'success') {
                    toastEvent.setParams({
                        'title': 'Éxito',
                        'type': 'success',
                        'mode': 'dismissable',
                        'message': mpResponse.message
                    });
                    this.initTitular(cmp);
                    this.editTitular(cmp);
                } else {
                    toastEvent.setParams({
                        'title': 'Error',
                        'type': 'error',
                        'mode': 'dismissable',
                        'message': mpResponse.message
                    });
                }
                toastEvent.fire();
            } else {
                alert('Ocurrió un error al eliminar al Titular, favor de notificar al administrador');
            }
        });
        $A.enqueueAction(controller);
    },
    openConfirmModal: function(cmp) {
        var modal = cmp.find('confirmationModal');
        $A.util.addClass(modal, 'slds-fade-in-open');
        var modalBack = cmp.find('confirmationModalBackdrop');
        $A.util.addClass(modalBack, 'slds-backdrop_open');
        var activeTab = cmp.get('v.activeTab');
        
        if(activeTab == 'contractTab') {
            cmp.set('v.noticeMsg', '¿Está seguro de que desea eliminar éste Documento?');
        } else if(activeTab == 'titularTab') {
            cmp.set('v.noticeMsg', '¿Está seguro de que desea eliminar éste Titular?');
        } else {
            cmp.set('v.noticeMsg', '');
        }
    },
    closeConfirmModal: function(cmp) {
        var modal = cmp.find('confirmationModal');
        $A.util.removeClass(modal, 'slds-fade-in-open');
        var modalBack = cmp.find('confirmationModalBackdrop');
        $A.util.removeClass(modalBack, 'slds-backdrop_open');
    },
    sendToApproval: function(cmp) {
        var toastEvent = $A.get('e.force:showToast');
        var controller = cmp.get('c.sendToApproval');
        var mdrmFile = cmp.get('v.mdrmFile');
        
        controller.setParams({
            fileId: mdrmFile.Id
        });
        
        controller.setCallback(this, function(response) {
            let state = response.getState();
            console.log('State: ' + state);
            
            if(state == 'SUCCESS') {
                let resp = response.getReturnValue();
                
                if(resp.status == 'success') {
                    mdrmFile.MDRM_Status__c = resp.contract;
                    cmp.set('v.mdrmFile', mdrmFile);
                    
                    toastEvent.setParams({
                        'title': 'Éxito',
                        'type': 'success',
                        'mode': 'dismissable',
                        'message': resp.mensaje
                    });
                } else {
                    toastEvent.setParams({
                        'title': 'Error',
                        'type': 'error',
                        'mode': 'dismissable',
                        'message': resp.mensaje,
                        'duration': 10000
                    });
                }
            } else {
                toastEvent.setParams({
                    'title': 'Error',
                    'type': 'error',
                    'mode': 'dismissable',
                    'message': 'Ocurrió un error al invocar el servicio de Mulesoft'
                });
            }
            toastEvent.fire();
        });
        $A.enqueueAction(controller);
    },
    
    phisycalPersonality: function (cmp, titular){
        var phisycalField = titular.MDRM_Phisycal_Personality__c;
        var RFCField = titular.MDRM_RFC__c;
        var idElement = '';
        var RFCLabel = cmp.get('v.RFCLabel');
        
        cmp.find("titularForm").forEach(component => {
            
            if (component.get("v.label") == RFCLabel) {
            
            idElement = component;
            
        }
                                        })
        
        if(!$A.util.isEmpty(phisycalField) && (phisycalField == 'Moral' || phisycalField == 'Física')){
            
            cmp.set('v.isRFCDisabled', false);
            
            if(phisycalField == 'Moral'){
                const regex = /^(([ÑA-Z|ña-z|&amp;]{3})\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)(\w{2})([A|a|0-9]{1}))$|^(([ÑA-Z|ña-z|&amp;]{3})([02468][048]|[13579][26])0229)(\w{2})([A|a|0-9]{1})$/;
                
                if(!regex.test(RFCField)){
                    idElement.setCustomValidity("Formato invalido para persona Moral");
                    
                } else idElement.setCustomValidity('');
                
            }else{
                var regex = /^(([A-Z|a-z]{4})\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)(\w{2})([A|a|0-9]{1}))$|^(([A-Z|a-z]{4})([02468][048]|[13579][26])0229)(\w{2})([A|a|0-9]{1})$/;
                
                if(!regex.test(RFCField)){
                    idElement.setCustomValidity("Formato invalido para persona Física");
                } else idElement.setCustomValidity('');
                
            }
            
        } else cmp.set('v.isRFCDisabled', true);
        
        idElement.reportValidity();
        
    },
    
    /*serverSideCall: function(cmp, action) {
        return new Promise(function(resolve,reject) {
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                console.log('Promise status: ' + state);
                
                if(state == 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    reject(new Error(response.getError()));
                }
            });
            $A.enqueueAction(action);
        });
    }*/
})