({
    //Exectue event for add scale in parent component
    AddNewRow : function(component, event, helper){
       // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").fire();     
    },
    
    //Exectue event for remove scale in parent component
    removeRow : function(component, event, helper){
        var index = component.get("v.rowIndex");
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : index }).fire();
    },
})