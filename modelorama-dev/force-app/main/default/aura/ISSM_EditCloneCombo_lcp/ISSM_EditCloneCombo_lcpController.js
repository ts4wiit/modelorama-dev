({
    // Get related data to the combo: combo limit, products, pruce group
    startComboEdit: function(cmp, event, helper){
        cmp.set('v.blnStart',false);
        helper.getComboLimit(cmp);
        helper.getComboByProduct(cmp);
        helper.getPriceGroup(cmp);
    },

    //Method for upsert Combo.
    updateCombo: function(cmp,event,helper){
        //EDIT COMBO
        var startDate = cmp.get('v.newComboRecord.ISSM_StartDate__c');
        var endDate   = cmp.get('v.newComboRecord.ISSM_EndDate__c');
        cmp.set('v.BlnErrors',false);
        helper.validateDates(cmp,startDate,endDate);//Validate Dates for COMBO
        helper.comboValidations(cmp);
        
        if(!cmp.get('v.BlnErrors')){
            if(!cmp.get('v.LimitId')){
                var comboType      = cmp.getComboTypeMethod( cmp.get('v.selectedProdRecords') );
                var newComboRecord = cmp.get('v.newComboRecord');
                newComboRecord.ISSM_ComboType__c = comboType;
                newComboRecord.ISSM_ModifiedCombo__c = true; //Active checkbox to indicate when the combo has been modified
                newComboRecord.ISSM_SynchronizedWithSAP__c = false; //Inactive the checkbox that indicates that the combo info is synchronized with SAP
                newComboRecord.ISSM_SynchronizedCodeSAP__c = ''; //Clear the code SAP field
                newComboRecord.ISSM_SynchronizedDescribeCodeSAP__c = ''; //Clear the code SAP description field
                newComboRecord.ISSM_StatusCombo__c = $A.get("$Label.c.TRM_OnComboUpdateSetStatusTo");//'ISSM_Open'; //Set the combo status to 'ISSM_Open'
                cmp.set('v.newComboRecord', newComboRecord);

                cmp.find("recordLoader").saveRecord(function(saveResult) {
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                        cmp.upsertComboByProductMethod(saveResult.recordId);
                        helper.showToast("success",$A.get("$Label.c.TRM_Updated"),$A.get("$Label.c.TRM_ComboUpdatedSuccessfully") );// muestra alerta
                        helper.redirectPage(saveResult.recordId);//Redirecciona al registro Creado        
                    }else{
                        helper.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_Msg06") );
                        console.log('ERROR ON SAVE: ',saveResult.error);
                    }
                });
            //CLONE COMBO
            }else{
                var limitId = cmp.get('v.LimitId');
                if(limitId == cmp.get('v.newComboRecord.ISSM_ComboLimit__c')){
                    helper.getComboByAccount(cmp);
                }else{
                    helper.getStructureforComboByAccount(cmp);
                }
                helper.castCombo(cmp,limitId);   
            }
        }    
    },

    //get combo list for assignment combo type
    getComboType: function (cmp, event) {
        var params = event.getParam('arguments');
        var lstProducts = params.lstProducts;
        var blnProd  = 0;
        var blnQuote = 0;
        var strComboType = null;
        var ProductType = $A.get("$Label.c.TRM_Product");
        var mixed       = $A.get("$Label.c.TRM_Mixed");
        var specific    = $A.get("$Label.c.TRM_Expecific");
        var quota       = $A.get("$Label.c.TRM_Quotas");

        for (var i = 0; i < lstProducts.length; i++) {
            blnProd  = (lstProducts[i].StrType == ProductType) ? blnProd += 1 : blnProd;
            blnQuote = (lstProducts[i].StrType != ProductType) ? blnQuote += 1 : blnQuote;
        }

        strComboType = (blnProd > 0 && blnQuote > 0) ? mixed : strComboType;
        strComboType = (blnProd > 0 && blnQuote == 0) ? specific : strComboType;
        strComboType = (blnProd == 0 && blnQuote > 0) ? quota : strComboType;

        return strComboType;
    },

    //upsert records to ComboByProduct
    upsertComboByProduct: function (cmp, event, helper) {
        var ProductType = $A.get("$Label.c.TRM_Product");
        var params = event.getParam('arguments');
        var comboId = params.comboId;
        var comboByProductList = cmp.get('v.oldProdRecords');
        var selectedProdRecords = cmp.get('v.selectedProdRecords');
        var pos=0;

        for (var i = 0; i < selectedProdRecords.length; i++) {
            pos+=10;
            var comboByProd = {
                'sobjectType':             'ISSM_ComboByProduct__c',
                'ISSM_ComboNumber__c':     comboId,
                'ISSM_QuantityProduct__c': selectedProdRecords[i].IntQuantity,
                'ISSM_UnitPriceTax__c':    selectedProdRecords[i].DecUnitPriceTax,  
                'ISSM_UnitPrice__c':       selectedProdRecords[i].DecUnitPrice,                
                'ISSM_Product__c':         (selectedProdRecords[i].StrType == ProductType) ? (selectedProdRecords[i].objProducts.ISSM_Product__c == null ? selectedProdRecords[i].objProducts.Id : selectedProdRecords[i].objProducts.ISSM_Product__c) : null,
                'ISSM_Quota__c':           (selectedProdRecords[i].StrType != ProductType) ? (selectedProdRecords[i].objProducts.ISSM_Quota__c == null ? selectedProdRecords[i].objProducts.Id : selectedProdRecords[i].objProducts.ISSM_Quota__c) : null,
                'ISSM_Type__c':            selectedProdRecords[i].StrType,
                'ISSM_Position__c':        ''+pos
            };
            comboByProductList.push(comboByProd);
        }

        var action = cmp.get('c.upsertComboByProductList');
        action.setParams({
            'comboByProductList': comboByProductList,
            'StrComboId'        : comboId
        });
          
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("SaveComboByProduct upserted successfully");
            } else {
                console.log("Error in upsert updateComboByProduct",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //set flag for select structure
    cloneAction : function(component, event, helper) {
        var rec = component.get('v.recordId');
        component.set('v.isClone',true);
        component.set('v.blnLevels',true);
    },

    //cancel operation
    closeModal : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        helper.redirectPage(recordId);
        helper.showToast('info',$A.get("$Label.c.TRM_Msg00"),$A.get("$Label.c.TRM_Msg03"));   
    },

    //Evento for register structure selected
    setLimitId : function(component, event, helper) {
        var setValueFromEvent = event.getParam("getId");
        component.set("v.LimitId" , setValueFromEvent);
        component.set('v.blnStart',false);
        component.set('v.blnLevels',false);
        component.getMethod();
    }
})