({
    // get combo limit record
    getComboLimit : function(cmp,idLimit){
        var idLimit = (cmp.get('v.LimitId')) ? cmp.get('v.LimitId') : cmp.get("v.newComboRecord.ISSM_ComboLimit__c");
        var action  = cmp.get('c.getLimitRecord');
        action.setParams({
            'IdRec': idLimit
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse.length==0){
                    this.showToast("info",$A.get("$Label.c.TRM_Msg00"), $A.get("$Label.c.TRM_Msg04")); 
                    this.redirectPage(idLimit);
                }
                cmp.set("v.recordLimitCombos", storeResponse[0]);
                console.log('getComboLimit SUCCESS',storeResponse);
            }else{
                console.log('##ERROR getComboLimit ', response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //getComboByProduct records
    getComboByProduct : function(cmp){
        var action = cmp.get('c.getComboByProduct');
        action.setParams({
            'IdRec': cmp.get("v.recordId")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var comboByProd = response.getReturnValue();
                var sum = 0.0;
                for(var i=0; i < comboByProd.length; i++){
                    sum += parseFloat(comboByProd[i].DecUnitPriceTax)*parseInt(comboByProd[i].IntQuantity);
                }
                cmp.set('v.SumProds',sum);
                cmp.set("v.selectedProdRecords", comboByProd);
                
                if(!cmp.get('v.LimitId')){
                    this.inactiveOldProducts(cmp);
                }
                console.log('getComboByProduct SUCCESS');
            }else{
                console.log('##ERROR IN getComboByProduct ',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //Get all records from ISSM_ComboByAccount__c 
    getComboByAccount : function(cmp){
        var action = cmp.get('c.getComboByAccount');
        action.setParams({
            'IdRec': cmp.get("v.newComboRecord.Id")
        });  
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ComboByAccount = response.getReturnValue();
                cmp.set("v.ComboByAccount", ComboByAccount);
                console.log('getComboByAccount SUCCESS');
            }else{
                console.log('##ERROR EN getComboByAccount->',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    // getPriceGroup Records
    getPriceGroup : function(cmp){
        var ObjPriceG = cmp.get("v.newComboRecord.ISSM_PriceGrouping__c");
        var LstPG = []
        
        if(ObjPriceG != null){
            ObjPriceG  = ObjPriceG.split(';');
            var action = cmp.get('c.getPriceGroup');
            action.setParams({
                'PriceGroupCode': ObjPriceG
            });  
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var PriceGroupIds = response.getReturnValue();
                    cmp.set("v.selectedPriceGroup", PriceGroupIds);
                    console.log('getPriceGroup SUCCESS');
                }else{
                    console.log('ERROR EN getPriceGroup->',response.error);
                }
            });
            $A.enqueueAction(action);
        }
    },

    //set flag ISSM_ActiveValue__c in false for old products
    inactiveOldProducts : function(cmp){
        console.log('inactiveOldProducts');
        var selectedProdRecords = cmp.get('v.selectedProdRecords');
        var oldProdRecords = [];

        for(var s = 0; s < selectedProdRecords.length; s++){
            oldProdRecords.push(selectedProdRecords[s].objProducts);
        }

        for(var o = 0; o < oldProdRecords.length; o++){
            oldProdRecords[o].ISSM_ActiveValue__c = false;
        }

        cmp.set('v.oldProdRecords', oldProdRecords);
    },

    // get Structure for Combo By Account 
    getStructureforComboByAccount : function(cmp){
        var LimitId       = cmp.get("v.LimitId");
        var PriceGroupIds = cmp.get("v.selectedPriceGroup");
        var action        = cmp.get('c.getStructureforComboByAccount');
        action.setParams({
            'strLimitId':    LimitId,
            'LstPriceGroup': PriceGroupIds
        });  
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lstComboByAcc = response.getReturnValue();
                cmp.set("v.ComboByAccount", lstComboByAcc);
                console.log('getStructureforComboByAccount SUCCESSS',lstComboByAcc);
            }else{
                console.log('ERROR IN getStructureforComboByAccount->',response.error);
            }
        });
        $A.enqueueAction(action);
    },


    // Cast record of combos
    castCombo: function (cmp, LimitId){  
        var comboRecord = cmp.get('v.newComboRecord');
        var newCombo = {
            'sobjectType':                  'ISSM_Combos__c',
            'ISSM_ComboLimit__c':           LimitId,
            'ISSM_EndDate__c':              comboRecord.ISSM_EndDate__c,
            'ISSM_StartDate__c':            comboRecord.ISSM_StartDate__c,
            'ISSM_TypeApplication__c':      comboRecord.ISSM_TypeApplication__c,
            'ISSM_NumberByCustomer__c':     comboRecord.ISSM_NumberByCustomer__c,
            'ISSM_NumberSalesOffice__c':    comboRecord.ISSM_NumberSalesOffice__c,
            'ISSM_ShortDescription__c':     comboRecord.ISSM_ShortDescription__c,
            'ISSM_LongDescription__c':      comboRecord.ISSM_LongDescription__c,
            'ISSM_Currency__c':             comboRecord.ISSM_Currency__c,
            'ISSM_ComboType__c':            comboRecord.ISSM_ComboType__c,
            'ISSM_PriceGrouping__c':        comboRecord.ISSM_PriceGrouping__c,
            'ISSM_ModifiedCombo__c':        false,
            'ISSM_SynchronizedWithSAP__c':  false,
            'ISSM_SynchronizedCodeSAP__c':  null
        };

        this.saveCombo(cmp,newCombo);
    },

    // Save combos
    saveCombo: function (cmp, newCombo){  
       var action = cmp.get('c.saveCombo');
        action.setParams({
            'objCombo': newCombo
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var comboId = response.getReturnValue();
                console.log("saveCombo SUCCESS");
                this.castComboByProduct(cmp,comboId);
                this.castComboByAccount(cmp,comboId);
                this.showToast("success","Creado",$A.get("$Label.c.TRM_Msg05"));// show alert
                this.redirectPage(comboId);
            }else{
                console.log("##ERROR saveCombo",response.error);
            }
        });
        $A.enqueueAction(action); 
    },

    //set cast sObject to ISSM_ComboByProduct__c
    castComboByProduct: function (cmp, comboId){
        var lstProducts = cmp.get('v.selectedProdRecords');
        var ProductType = $A.get("$Label.c.TRM_Product");
        var lstSaveProducts = [];
        var pos=0;
        var prodId='';
        var quotId='';
        
        for (var i = 0; i < lstProducts.length; i++) {
            pos+=10;
            prodId='';
            quotId='';
            
            if(lstProducts[i].isSelected && lstProducts[i].StrType == ProductType){
                prodId = lstProducts[i].objProducts.ISSM_Product__r.Id;   
            }else if(!lstProducts[i].isSelected && lstProducts[i].StrType == ProductType){
                prodId = lstProducts[i].objProducts.Id;
            }
            
            if(lstProducts[i].isSelected && lstProducts[i].StrType != ProductType){
                quotId = lstProducts[i].objProducts.ISSM_Quota__r.Id;
            }else if(!lstProducts[i].isSelected && lstProducts[i].StrType != ProductType){
                quotId = lstProducts[i].objProducts.Id;
            }

            var comboByProd = {
                'sobjectType':             'ISSM_ComboByProduct__c',
                'ISSM_ComboNumber__c':     comboId,
                'ISSM_QuantityProduct__c': lstProducts[i].IntQuantity,
                'ISSM_UnitPriceTax__c':    lstProducts[i].DecUnitPriceTax,
                'ISSM_UnitPrice__c':       lstProducts[i].DecUnitPrice,
                'ISSM_Product__c':         prodId,
                'ISSM_Quota__c':           quotId,
                'ISSM_Type__c':            lstProducts[i].StrType,
                'ISSM_Position__c':        ''+pos
            };
            
            lstSaveProducts.push(comboByProd);
        }
        this.saveComboByProducts(cmp,lstSaveProducts);
    },

    // save combo by products records
    saveComboByProducts: function (cmp,lstProducts){
        console.log('lstProducts->',lstProducts);
        var action = cmp.get('c.upsertComboByProductList');
        action.setParams({
            'comboByProductList': lstProducts
        }); 
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("castProducts SUCCESS");
            }else{
                console.log("Error in castProducts",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    // Cast record of Combo By Account
    castComboByAccount: function(cmp,comboId){
        var lstComboByAcc     = cmp.get('v.ComboByAccount');
        var lstSaveComboByAcc = [];

        for (var i = 0; i < lstComboByAcc.length; i++) {
            var ComboByAcc = {
                'sobjectType':                     'ISSM_ComboByAccount__c',
                'ISSM_ComboNumber__c':             comboId,
                'ISSM_Segment__c':                 lstComboByAcc[i].ISSM_Segment__c,
                'ISSM_RegionalSalesDirection__c':  lstComboByAcc[i].ISSM_RegionalSalesDirection__c,
                'ISSM_SalesOrganization__c':       lstComboByAcc[i].ISSM_SalesOrganization__c,
                'ISSM_SalesOffice__c':             lstComboByAcc[i].ISSM_SalesOffice__c,
                'ISSM_Customer__c':                lstComboByAcc[i].ISSM_Customer__c,
                'ISSM_ExclusionAccount__c':        lstComboByAcc[i].ISSM_ExclusionAccount__c
            };
            lstSaveComboByAcc.push(ComboByAcc);
        }

        this.saveComboByAccounts(cmp,comboId,lstSaveComboByAcc);
    },

    //save Combo By Accounts
    saveComboByAccounts: function(cmp,comboId,lstComboByAcc){
        var action = cmp.get('c.saveComboByAccountList');
        action.setParams({
            'comboByAccountList': lstComboByAcc,
            'StrCombo'          : comboId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log("castAccounts SUCCESS");
            }else{
                console.log("##ERROR saveCombo",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //Get Record Type
    getRecordTypeIdByDeveloperName :  function(cmp, objectType, developerName, attributeName){
        var action = cmp.get('c.getRecordTypeIdByDeveloperName');
        var attribute = 'v.' + attributeName;
        action.setParams({
            'objectType': objectType,
            'developerName': developerName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var recordTypeId = response.getReturnValue();
                cmp.set( attribute, String(recordTypeId) );
                console.log("getRecordTypeIdByDeveloperName SUCCESS");
            }else{
                console.log("##ERROR getRecordTypeIdByDeveloperName " + response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //Show Alert
    showToast: function (type, title, msg){
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "duration": 5000,
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    //Redirect to Page indicated for the RecordId
    redirectPage: function (recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },

    validateDates: function (cmp, startDate, endDate) {
        var mode = cmp.get('v.mode');
        var isClone = cmp.get('v.isClone');
        //it is validated that the dates are not empty
        if (startDate != null && endDate != null) {
            // var today = new Date(startDate.replace("-", ","));
            var EndDateLimit     = parseInt($A.get("$Label.c.TRM_EndDateLimit"));
            var today = new Date();
            var startFormattedDate  = this.setNewDate(today, 0);
            var endFormattedDate    = this.setNewDate(today, EndDateLimit);

            if ( (isClone || mode != 'EDIT') && (startDate < startFormattedDate)) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg12"));
                cmp.set('v.BlnErrors', true);
            }
            if (endDate < startDate) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg13"));
                cmp.set('v.BlnErrors', true);
            }
            if (endDate > endFormattedDate) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg14"));
                cmp.set('v.BlnErrors', true);
            }
            if (mode != 'EDIT' && (startDate < startFormattedDate)) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg12"));
                cmp.set('v.BlnErrors', true);
            }
        }
    },

    //assign format to dates to be evaluated
    setNewDate: function (currentDate, numDays) {
        currentDate.setDate(currentDate.getDate() + numDays); //add two days
        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1; //January is 0!
        var yy = currentDate.getFullYear();
        dd = (dd < 10) ? '0' + dd : dd;
        mm = (mm < 10) ? '0' + mm : mm;
        var formattedDate = yy + '-' + mm + '-' + dd;

        return formattedDate;
    },

        //Input validations to be able to carry out the creation of the order
    comboValidations: function (cmp) {
        var BlnrequiereFields = false;
        var minNumericFields = false;
        var overMaxNumberBySalesStructure = false;
        var overMaxNumberByCustomer = false;
        var nByCustomerGTNBySalesStructure = false;
        var lstProducts = cmp.get('v.selectedProdRecords');
        var ProductType = $A.get("$Label.c.TRM_Product");

        BlnrequiereFields = (!cmp.get('v.newComboRecord.ISSM_EndDate__c') ||
                             !cmp.get('v.newComboRecord.ISSM_StartDate__c') ||
                             !cmp.get("v.newComboRecord.ISSM_TypeApplication__c") ||
                             !cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') ||
                             !cmp.get('v.newComboRecord.ISSM_LongDescription__c') ||
                             !cmp.get('v.newComboRecord.ISSM_ShortDescription__c')) ? true : false;

        minNumericFields = (cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') > 0) ? true : false;
        overMaxNumberBySalesStructure = (cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') > Number($A.get("$Label.c.TRM_MaxNumberBySalesStructure") ) ) ? true : false;
        overMaxNumberByCustomer = (cmp.get('v.newComboRecord.ISSM_NumberByCustomer__c') > Number($A.get("$Label.c.TRM_MaxNumberByCustomer") ) ) ? true : false;
        nByCustomerGTNBySalesStructure = ( Number(cmp.get('v.newComboRecord.ISSM_NumberByCustomer__c')) > Number(cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c')) ) ? true : false;

        //If at least one product is not selected
        if (lstProducts.length <= 0) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg08"));
            cmp.set('v.BlnErrors', true);
        }
        //if any field is empty send alert
        if (BlnrequiereFields) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg09"));
            cmp.set('v.BlnErrors', true);
        }

        if (overMaxNumberBySalesStructure) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumberBySalesStructureTooLong"));
            cmp.set('v.BlnErrors', true);
        }
        //If the maximum number of combos per structure is smaller or = to zero
        if (!minNumericFields) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg10"));
            cmp.set('v.BlnErrors', true);
        }

        if (overMaxNumberByCustomer) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumberByCustomerTooLong"));
            cmp.set('v.BlnErrors', true);
        }
        //If the max combos by customer is greater than max combos by sales structure
        if (nByCustomerGTNBySalesStructure) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumByCustomerCanNotBeGreaterThanNumByStructure"));
            cmp.set('v.BlnErrors', true);
        }
        //If any product comes without quantity or price
        if (lstProducts.length > 0) {
            for(var i=0; i < lstProducts.length; i++){
                if(Number(lstProducts[i].IntQuantity) == 0){
                    this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg11"));
                    cmp.set('v.BlnErrors', true);
                }
            }  
        }
    }
})