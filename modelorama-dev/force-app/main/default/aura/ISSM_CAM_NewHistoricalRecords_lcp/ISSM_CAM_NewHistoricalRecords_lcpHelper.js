({
    createObjectData: function(component, event, ope) {
        debugger;
        // get the cutOverHistoryList from component and add(push) New Object to List  
        var RowItemList = component.get("v.cutOverHistoryList");
        if(RowItemList.length <= 0 || ope){
            RowItemList.splice(0, 0,{
            'ISSM_Asset_Description__c': ''
            ,'ISSM_Asset_number__c': ''
            ,'ISSM_CAM_Leftover__c': ''
            ,'ISSM_CAM_Temporal__c': ''
            ,'ISSM_Centre__c': ''
            ,'ISSM_Equipment_number__c': ''
            ,'ISSM_Material_number__c': ''
            ,'ISSM_Serial_Number__c': ''
            ,'Id': ''
        });
        }
        
        // set the updated list to attribute (cutOverHistoryList) again    
        component.set("v.cutOverHistoryList", RowItemList);       
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        debugger;
        var isValid = true;
        var isValidSerial = true;
        var isValidEquipmentNumber = true;
        var allCutOverHistoryRows = component.get("v.cutOverHistoryList");
        for (var indexVar = 0; indexVar < allCutOverHistoryRows.length; indexVar++) {
            debugger;
             if ((allCutOverHistoryRows[indexVar].ISSM_Asset_Description__c == ''||allCutOverHistoryRows[indexVar].ISSM_Asset_Description__c == null)
                ||(allCutOverHistoryRows[indexVar].ISSM_Asset_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Asset_number__c == null)
                ||(allCutOverHistoryRows[indexVar].ISSM_Equipment_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Equipment_number__c == null)
                ||(allCutOverHistoryRows[indexVar].ISSM_Material_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Material_number__c == null)
                ||(allCutOverHistoryRows[indexVar].ISSM_Serial_Number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Serial_Number__c == null)
                ||(allCutOverHistoryRows[indexVar].ISSM_Centre__c == ''||allCutOverHistoryRows[indexVar].ISSM_Centre__c == null)) {
                    isValid = false;
                    this.showToast("info",$A.get("$Label.c.ISSM_CAM_ShowToastInformative"),$A.get("$Label.c.ISSM_CAM_CaptureAllFields"));
            }
            var listForValidateDuplicate = allCutOverHistoryRows.filter(function(arr){return arr.ISSM_Serial_Number__c == allCutOverHistoryRows[indexVar].ISSM_Serial_Number__c});
            if(listForValidateDuplicate.length > 1){
                    isValidSerial = false;
            }  
            var listDuplicate = allCutOverHistoryRows.filter(function(arr){return arr.ISSM_Equipment_number__c == allCutOverHistoryRows[indexVar].ISSM_Equipment_number__c});
            if(listDuplicate.length > 1){
                    isValidEquipmentNumber = false;
            }
            var saleOfficeOK = false;
            var mapSalesOffice = component.get("v.mapSalesOffice");
            mapSalesOffice.forEach(function(valor, clave) {
                if(clave === allCutOverHistoryRows[indexVar].ISSM_Centre__c) { 
                    saleOfficeOK = true;
                }
            });
            if(!saleOfficeOK){
                this.showToast("error",'ERROR',$A.get("$Label.c.ISSM_CAM_Center")+" '" + allCutOverHistoryRows[indexVar].ISSM_Centre__c + "' "+$A.get("$Label.c.ISSM_CAM_NotValid"));
                isValid = false;
            }
            
        }
        if(!isValidSerial && isValid){
            isValid = false;
            this.showToast("error",'ERROR',$A.get("$Label.c.ISSM_CAM_RepeatedSerialNumber"));
        }
        if(!isValidEquipmentNumber && isValid){
            isValid = false;
            this.showToast("error",'ERROR',$A.get("$Label.c.ISSM_CAM_RepeatedTeamNumber"));
        }
        return isValid;
    },
    //Muestra alerta con los parametros enviados
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": type,
            "title": title,
            "message": msg
        });
        resultsToast.fire();
    },
    close: function(component, event) {
        component.getEvent("ISSM_CAM_AddRecordsHistory_evt").setParams({
            "lstNewRecordsHistory" : [],
            "isOpenAddHistory" : false
        }).fire();
    },
})