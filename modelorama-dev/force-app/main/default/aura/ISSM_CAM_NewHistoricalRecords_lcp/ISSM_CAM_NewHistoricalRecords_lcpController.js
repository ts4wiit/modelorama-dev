({  // function call on component Load
    doInit: function(component, event, helper) {
        // create a Default RowItem [ Instance] on first time Component Load
        // by call this helper function  
        component.set("v.cutOverHistoryListbackUp",component.get("v.cutOverHistoryList"));
        helper.createObjectData(component, event, false);
        var RowItemList = component.get("v.cutOverHistoryListbackUp");
        if(RowItemList.length > 0) helper.createObjectData(component, event,true); 
    },
 
    // function for save the Records 
    Save: function(component, event, helper) {
        var allCutOverHistoryRows = component.get("v.cutOverHistoryList");
        var index;
        for (var indexVar = 0; indexVar < allCutOverHistoryRows.length; indexVar++) {
            if ((allCutOverHistoryRows[indexVar].ISSM_Asset_Description__c == ''||allCutOverHistoryRows[indexVar].ISSM_Asset_Description__c == null)
                && (allCutOverHistoryRows[indexVar].ISSM_Asset_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Asset_number__c == null)
                && (allCutOverHistoryRows[indexVar].ISSM_Equipment_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Equipment_number__c == null)
                && (allCutOverHistoryRows[indexVar].ISSM_Material_number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Material_number__c == null)
                && (allCutOverHistoryRows[indexVar].ISSM_Serial_Number__c == ''||allCutOverHistoryRows[indexVar].ISSM_Serial_Number__c == null)
                && (allCutOverHistoryRows[indexVar].ISSM_Centre__c == ''||allCutOverHistoryRows[indexVar].ISSM_Centre__c == null)) index = indexVar;
        }
        if (index > -1) {
           allCutOverHistoryRows.splice(index, 1);
        }
        component.set("v.allCutOverHistoryRows",allCutOverHistoryRows);
        if(helper.validateRequired(component, event)){
            component.set("v.cutOverHistoryListbackUp",component.get("v.cutOverHistoryList"));
            helper.close(component, event);
        }
        
    },
 
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        if(helper.validateRequired(component, event))
            helper.createObjectData(component, event,true);       
    },
 
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (cutOverHistoryList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.cutOverHistoryList");
        AllRowsList.splice(index, 1);
        // set the cutOverHistoryList after remove selected row element  
        component.set("v.cutOverHistoryList", AllRowsList); 
    },
    // function for delete the row 
    closeModel: function(component, event, helper) {
        component.set("v.cutOverHistoryList",component.get("v.cutOverHistoryListbackUp"));
        helper.close(component, event);
    },
})