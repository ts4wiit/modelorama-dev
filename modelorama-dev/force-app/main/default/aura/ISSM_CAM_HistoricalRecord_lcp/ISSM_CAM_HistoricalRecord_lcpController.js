({
    AddNewRow : function(component, event, helper){
       // fire the AddNewRowEvt Lightning Event 
        component.getEvent("ISSM_CAM_AddNewRow_evt").fire();     
    },
    
    removeRow : function(component, event, helper){
     // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
       component.getEvent("ISSM_CAM_DeleteRow_evt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
  
})