({
	getBusinessmanRelated : function(cmp, evt) {
        var params = evt.getParam('arguments');
        var callback;
        var tienda;
        if (params) {
            tienda = params.tienda;
            callback = params.callback;
        }

        var action = cmp.get('c.getBusinessmanRelated');

        action.setParams({
            id : tienda
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                console.log("result: " + response.getReturnValue());
                if (callback) 
                    callback(response.getReturnValue());
            } else if(state === 'INCOMPLETE') {
                
            } else if(state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    }
})