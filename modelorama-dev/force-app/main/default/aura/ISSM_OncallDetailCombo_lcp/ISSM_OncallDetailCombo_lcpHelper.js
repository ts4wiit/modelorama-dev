({
	getProductsExpecific : function(component) {
		var action = component.get('c.getResultProductExpecific');
		action.setParams({                  
			"strIdCombo" : component.get("v.strIdComboReceived"),
			"acc_Id" : component.get("v.strIdAccount"),
            "combos_map" : component.get("v.combos_map")
        }); 
        action.setCallback(this, function(response){ 
            if(action.getState() === "SUCCESS"){
            var result = response.getReturnValue(); 
            console.log('result especific = ');

               component.set("v.lstDivisionDetail", result); 
               console.log('************** DIVISION DETAIL ***********+ ');
               console.log(result);
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);  
	},
	getProductsQuotes : function(component, event) {
		//alert('entro aqui 2');
		component.set("v.isOpenSpinner",true);
		var key    = component.get("v.StrKey");
		var action = component.get('c.getResultProductQuotes');
		action.setParams({                  
			"strIdCombo" : component.get("v.strIdComboReceived"),
			 "strIdaccount" : component.get("v.strIdAccount"),
			 "strSearchKeyWord" : key
			 
        }); 
        action.setCallback(this, function(response){ 
            if(action.getState() === "SUCCESS"){
            var result = response.getReturnValue(); 
            component.set("v.lstDataProducts", result); 
            var LstProductsBase =  component.get('v.lstProBase');  
            var LstNameProductsBase =  component.get('v.lstNameProBase');  
            
            for(var res of result){
            	LstProductsBase.push(res.label+':'+res.quantity);
            	LstNameProductsBase.push(res.label);
            }
            component.set("v.lstProBase", LstProductsBase);
            component.set("v.lstNameProBase", LstNameProductsBase);
           /* var LstProductsBase = component.get("v.lstDataProducts");
            LstProductsBase.push(result);
            component.set("v.lstProBase", LstProductsBase); */
            
            
            component.set('v.columns', [
            {label: 'SKU', fieldName: 'ISSM_ProductSKU__c', type: 'text'},
            {label: 'Nombre', fieldName: 'ONTAP__MaterialProduct__c', type: 'text'},   
            {label: 'Cantidad', fieldName:'ISSM_QuantityInput__c', type: 'number', editable : true}
            ]);
            component.set("v.isOpenSpinner",false);
            }else if (state === "ERROR") { 
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);  
	},
	getValuesSelected: function(component, event, helper) {
		component.set("v.blnActivebuttonExist", true);
		component.set("v.isOpenStock", false);
		component.set("v.enabledAddBtn_Bln", true);
		
		
		var idSelected = event.target.id;
    	var NameSelected = event.target.name;
    	var maxSelected = event.target.max;      
    	var valueSelected = event.target.value;        
    	var ProductFamilyOk = component.get('v.lstProductFamilyOk');  
    	var IdsxFamily = component.get('v.lstIdsxFamily');   
    	var valProduct  =  component.get('v.lstDataProducts');
    	var IdsxFamilyBK = component.get('v.lstIdsxFamilyBK');
    	var IdsxFamilyBKMax = component.get('v.lstIdsxFamilyBKMax');
    	var IdsxProductSelected = component.get('v.lstProductSelected');
    	var globalsum =  component.get('v.intInputquantitySelect');
    	var NameFamily = component.get('v.lstNameFam');
    	
    	 
    	if(valueSelected > 0){
            document.getElementById(idSelected).checked = true; 
        }else{
            document.getElementById(idSelected).checked = false; 
        }
        var lstJson = JSON.parse(JSON.stringify(valProduct));
        var lstFamilyAcum = component.get('v.lstkeyFamilyAcum'); 
        var lstValxProduct =  component.get('v.lstValxProductxFam');
        var lstIdProduct=  component.get('v.lstIdsSelectedFamily');  
        var  lstDataSumVal = component.get('v.lstDataSumVal');
        
        var lengthName = NameSelected.length;
        var lengthID = NameSelected.length + idSelected.length +1;
       
        // ---------------------
        if(IdsxFamily.includes(idSelected)){			 	
		 	for(var x = 0; x < IdsxFamilyBK.length; x++){
		 		var ComboSelected_Str = IdsxFamilyBK[x].split(":");
	
		 		if(String(ComboSelected_Str[1]) == String(idSelected)){
		 			var strIds  = ComboSelected_Str[2];
		 			lstFamilyAcum.push(strIds);
		 			
		 			component.set('v.lstkeyFamilyAcum',lstFamilyAcum);	
		 		}        				
		 	}
		 	IdsxFamilyBK.remove(NameSelected+':'+idSelected+':'+strIds);
			IdsxFamilyBK.push(NameSelected+':'+idSelected+':'+valueSelected);
			
			IdsxFamilyBKMax.remove(NameSelected+':'+maxSelected+':'+strIds);
			IdsxFamilyBKMax.push(NameSelected+':'+maxSelected+':'+valueSelected);
			
			IdsxProductSelected.remove(idSelected+':'+strIds);
			IdsxProductSelected.push(idSelected+':'+valueSelected);
			
        }else{
        	IdsxFamilyBK.push(NameSelected+':'+idSelected+':'+valueSelected);
        	component.set('v.lstIdsxFamilyBK',IdsxFamilyBK);
        	
        	IdsxFamilyBKMax.push(NameSelected+':'+maxSelected+':'+valueSelected);
        	component.set('v.lstIdsxFamilyBKMax',IdsxFamilyBKMax);
        	
        	IdsxProductSelected.push(idSelected+':'+valueSelected);
        	component.set('v.lstProductSelected',IdsxProductSelected);

        	IdsxFamily.push(idSelected);
        			console.log(component.get('v.lstIdsxFamilyBK'));
				console.log(component.get('v.lstIdsxFamilyBKMax'));
        }
       
       // ---------------------
        if(component.get('v.lstNameFam').includes(NameSelected)){
        	component.get('v.lstNameFam').remove(NameSelected);
        	 NameFamily.push(NameSelected);
        	 component.set('v.lstNameFam',NameFamily);

        }else{
        	NameFamily.push(NameSelected);
        	component.set('v.lstNameFam',NameFamily);
        }
        
      	 // ---------------------
         var valueSelected = Number(event.target.value);
        // var lstIds = component.get('v.lstIdsSelected');
         
         if(valueSelected > 0){
            component.set('v.lstIdsSelected',IdsxProductSelected);
        }else if (valueSelected == 0){
            document.getElementById(idSelected).checked = false; 
            IdsxProductSelected.remove(idSelected+':'+valueSelected); 
            component.set('v.lstIdsSelected',IdsxProductSelected);
        }
    	
	}, 
	limitNum: function (component, event) {
        //******************Start to check the checkbox********************************
		var myObject = [];
		var lstIds = component.get('v.lstIdsSelected');
        var idSelected = event.target.id;       
        var valueSelected = Number(event.target.value);         
        //console.log('ENTRA AQUI limitNum :  '+valueSelected);
        var arreIdsSelected =  component.get('v.lstIdsSelected');
        
        //alert(valueSelected);
        //******************End to check the checkbox********************************


        //******************************************        
        var max = parseInt(event.target.max);

        if(parseInt(event.target.value.substring(0, 1)) == 0){
            event.target.value = event.target.value.substring(1, event.target.value.length);
        }         

        if(event.target.value == '' || event.target.value === null || event.target.value === undefined){
            event.target.value = 0;
        }

        if(parseInt(event.target.value) >= max){
            event.target.value = max;
        }
        //******************************************
    },
        emptyNum: function (component, event) {
        var min = Number(event.target.min);

        if(event.target.value == '' || event.target.value === null || event.target.value === undefined || event.target.value.substring(0,1) == '-'){
            event.target.value = min;
        }
    },
    ValidateMaxCombos: function (component, event) {

    	component.set("v.isOpenSpinner",false);
    	var action = component.get('c.getAlertsByCombo');    	
    	var lstAllsuccess = [];
		action.setParams({                  
			"lstProductsxComboxFamily" : JSON.stringify(component.get("v.lstIdsxFamilyBKMax")), 
			"lstNameProductBase": JSON.stringify(component.get("v.lstNameProBase")),
			"lstNameProductBase2": JSON.stringify(component.get("v.lstNameProBase"))
			
        }); 
		//console.log('JSON 1 : '+JSON.stringify(component.get("v.lstIdsxFamilyBKMax")));
		//console.log('JSON 2 : '+JSON.stringify(component.get("v.lstNameProBase")));
		//console.log('JSON 3 : '+JSON.stringify(component.get("v.lstNameProBase")));
        action.setCallback(this, function(response){ 
            if(action.getState() === "SUCCESS"){
                var result = response.getReturnValue(); 
              
               for(var res of result){
            	  if(res.blnSuccess !=  true){
            		  lstAllsuccess.push(res.blnSuccess);
            	  }
               }
               if(lstAllsuccess.length >0){
            	    component.set("v.blnActivebuttonExist", true);
               }else{
            	    component.set("v.blnActivebuttonExist",false); 
               }
               component.set("v.lstAlertsReceived", result); 
               component.set("v.isOpenALerts", true); 
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action); 
    	
    },
    handleClickValidateExistHelper:  function(component, event, helper) {
        var valueReceived = component.get('v.strComboTypeReceived');
	    var comboId = component.get('v.strIdComboReceived');
	    var name = component.get('v.comboNameReceived_Str');
	    //var counter_Int = component.get("v.counterAvail_Int");        
        var inputId = comboId+'-'+name;
	    var idMaxByAvail_Str = inputId +'*';
        var idused_Str = inputId +'-';
        var idMaxPerUser = inputId +':';
        var currentMaxInput_Int = parseInt(document.getElementById(inputId).max);             
        console.log('currentMaxInput_Int = ' + currentMaxInput_Int);	
        // alert('currentMaxInput_Int = ' + currentMaxInput_Int);
        var currentUsedValue_Int = parseInt(document.getElementById(idused_Str).htmlFor);
        var maxPerUser_Int = parseInt(document.getElementById(idMaxPerUser).htmlFor);
        //alert(maxPerUser_Int); 
        // alert('currentUsedValue_Int = ' + currentUsedValue_Int);
        console.log('currentUsedValue_Int = ' + currentUsedValue_Int);
	  	component.set('v.inputId_Str',inputId);
    	component.set("v.isOpenALerts", false); 
    	component.set("v.isOpenSpinner",true);        
    	var action = component.get('c.getMatAvailabilities');
    	action.setParams({
            "acc_Id" : component.get("v.strIdAccount"),             
            "idsQtyProds_Lst" : component.get("v.lstIdsSelected"),
            "cbp_Lst" : component.get('v.lstDivisionDetail')
        }); 
        action.setCallback(this, function(response){       
            if(action.getState() === "SUCCESS"){         
                //counter_Int += 1;   
            	var result = response.getReturnValue(); 
            	component.set('v.lstAvailabilities', result);
            	component.set("v.isOpenSpinner",false);
            	component.set("v.isOpenStock",true);  
            	component.set("v.enabledAddBtn_Bln",false);     
                //component.set("v.counterAvail_Int",counter_Int);               
            	var lstIdAggregateRecord = component.get('v.lstIdAggregateR');
                var maxAvail_lst = []; 
                var division = 0;                
                for(var res of result){
                    console.log('******************');
                    console.log(res);
                    console.log('******************');
                    var avail_Int = parseInt(res.avaialability_Lng);
                    var requested_Int = parseInt(res.requested_Int);
                    if(avail_Int != 0 && requested_Int != 0){
                        division = Math.floor(avail_Int / requested_Int);
                    }else{
                        division = 0;
                    }
                    maxAvail_lst.push(division);
                }

                var maxValue = parseInt(Math.min(...maxAvail_lst));
                console.log('maxValue = ' + maxValue);
                document.getElementById(idMaxByAvail_Str).innerHTML = maxValue.toString();

                
                // if(currentUsedValue_Int == 0){
                document.getElementById(idused_Str).htmlFor = Math.min(currentMaxInput_Int,maxValue);  
                document.getElementById(idused_Str).innerHTML = Math.min(currentMaxInput_Int,maxValue);
                
                component.set("v.qtyMaxInputChild_IntMK",maxPerUser_Int);
                //alert(document.getElementById(idused_Str).htmlFor);                    
                // }else if(currentUsedValue_Int >= 1){
                    // component.set("v.currentMax_Int",currentMaxInput_Int);
                // }
               
            	for(var res of result){
                    console.log('***************RES***************');
                    console.log(res);
                    
                    if(res.requested_Int > res.avaialability_Lng){
                        console.log('ENTRO IF');
                        console.log(res.requested_Int+ ' -- ' +  res.avaialability_Lng);
                        document.getElementById(idMaxByAvail_Str).innerHTML = maxValue.toString();
                        document.getElementById(inputId).max = maxValue;
                    	component.set('v.enabledAddBtn_Bln',true);                       
                        break;
                    }else{ 
                        console.log('ENTRO ELSE');
                        console.log(res.requested_Int+ ' -- ' +  res.avaialability_Lng);
                    	lstIdAggregateRecord.push(res);
                    	component.set('v.lstIdAggregateR',lstIdAggregateRecord);
                        component.set('v.enabledAddBtn_Bln',false);                        
                        document.getElementById(inputId).disabled = true;                     
                    } 
                }
                var lstProductIterate = component.get("v.lstIdAggregateR");
                var lstJson = JSON.parse(JSON.stringify(lstProductIterate));
                var lstkerProduct = component.get("v.lstkeyAggregateR");
                for( var e = 0; e < lstJson.length; e++){
                	lstkerProduct.push(lstJson[e].Id +':'+lstJson[e].requested_Int+':'+component.get('v.comboNameReceived_Str'));
                	component.set('v.lstkeyAggregateR',lstkerProduct);
                }     
                                                       
            }else if(state === "ERROR"){
           
                var errors = response.getError();
                console.error('ERR : '+errors);
                document.getElementById(inputId).disabled = true;
            }
        });
        $A.enqueueAction(action);
    }

})