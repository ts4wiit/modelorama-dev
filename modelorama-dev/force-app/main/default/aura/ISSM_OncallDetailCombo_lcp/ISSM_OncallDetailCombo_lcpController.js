({
	 init: function (component, event, helper) {
	    
    	var valueReceived = component.get('v.strComboTypeReceived');
	   
        console.log('valueReceived = ' + valueReceived);

	    if(valueReceived == 'Expecific' || valueReceived == 'Specific' || valueReceived == 'Específico' || valueReceived == 'Específicos'){ //$A.get("$Label.c.ISSM_SpecificCombos")
		   component.set("v.isOpenExpecifc",true);
		   helper.getProductsExpecific(component, event, helper);  
		
	   	}
	   
	   if(valueReceived == 'Quota' || valueReceived == 'Cupo'){//$A.get("$Label.c.ISSM_QuotasCombos")
		   component.set("v.intValueSelected",0);
		   component.set("v.intValueSelectedOld",0); 
		   component.set("v.intValueSelectedOldR",0);
		   component.set("v.intInputquantitySelect",0);
		   component.set("v.intInputquantity",0);
		   
		   component.set("v.isOpenQuotes",true);
		   helper.getProductsQuotes(component);
	   }
	   if(valueReceived == 'Mixed' || valueReceived == 'Mixto'){//$A.get("$Label.c.ISSM_MixedCombos")
		   component.set("v.intValueSelected",0);
		    component.set("v.intValueSelectedOld",0);
		   component.set("v.intValueSelectedOldR",0);
		   component.set("v.intInputquantitySelect",0);
		   component.set("v.intInputquantity",0);
		   component.set("v.isOpenMixed",true);
		   helper.getProductsExpecific(component); 
		   helper.getProductsQuotes(component);  
		   
	   } 
    },
    handleClickQuantities : function(component, event, helper){
    	component.set("v.isOpenStock",false);
    	component.set("v.isOpenSpinner",true);        
        var lstIDProduct = component.get('v.lstDataProducts');
        var lstNameProductSelect = component.get('v.lstNameFam');
        
        // var comboId = component.get('v.strIdComboReceived');
        // var name = component.get('v.comboNameReceived_Str');      
        // var inputId = comboId+'-'+name;
        // document.getElementById(inputId).max = parseInt(component.get("v.currentMax_Int"));
    
        if(lstIDProduct.length  == lstNameProductSelect.length  ){            
        	helper.ValidateMaxCombos(component, event, helper);        	
        }else {
        	component.set("v.isOpenSpinner",false);
        	component.set("v.isOpenALerts", true); 
        	component.set("v.lstAlertsReceived", {strStatus: "slds-notify slds-notify_toast slds-theme_warning", strTexto: "No Se Agregaron Combos "});   	
        }

    },
     handleClickValidateExistCtr : function(component, event, helper){      
    	 helper.handleClickValidateExistHelper(component, event, helper); 
    },
    ChangeQuantity: function (component, event, helper) {
    	//helper.limitNum(component, event);
    	helper.getValuesSelected(component, event, helper); 
    	 
    },
    limits: function(component, event, helper){    	
    	helper.limitNum(component, event);
    },
    emptyValue: function(component, event, helper){    	
    	helper.emptyNum(component, event); 
    },
    addToCar : function(component, event){
        var inputId = component.get('v.inputId_Str');
        document.getElementById(inputId).disabled = false; //Pasar al botón agregar
       console.log('**** SE MANDA AL EVENTO : '+component.get("v.lstkeyAggregateR"));
        var EvCloseModal = component.getEvent("cmpEvCloseModal");
			EvCloseModal.setParams({
				"blnCloseModal":false,
				"lstAggregateRecord" : component.get("v.lstkeyAggregateR"),
                "qtyMaxInput_Int" : component.get("v.qtyMaxInputChild_IntMK")});
				EvCloseModal.fire();  
    },
     addToCarExpecific : function(component, event, helper){
    	 var comboId = component.get('v.strIdComboReceived');
         var name = component.get('v.comboNameReceived_Str');
         var inputId = comboId+'-'+name;
         var idMaxByAvail_Str = inputId +'*';     
      
    	 var lstProductIterate = component.get("v.lstDivisionDetail");
    	 var lstkerProduct = component.get("v.lstkeyAggregateR");
    	 console.log('EXPE 1 : '+lstProductIterate);
    	 for( var e = 0; e < lstProductIterate.length; e++){
    		 lstkerProduct.push(lstProductIterate[e].ISSM_Product__r.Id +':'+lstProductIterate[e].ISSM_QuantityProduct__c+':'+component.get('v.comboNameReceived_Str'));
		  	component.set('v.lstkeyAggregateR',lstkerProduct);
		}    
       
       //console.log('SE MANDA AL EVENTO : '+component.get("v.lstkeyAggregateR"));
        var EvCloseModal = component.getEvent("cmpEvCloseModal");
			EvCloseModal.setParams({
				"blnCloseModal":false,
				"lstAggregateRecord" : component.get("v.lstkeyAggregateR")});
				EvCloseModal.fire();  
				document.getElementById(inputId).disabled = false;
				
				
    },
    
    closeModel: function(component, event, helper) {
    	var idSelected = event.target.id;
		document.getElementById(idSelected).style.display = "none"; 
	}, 
	filterTable : function(component, event, helper) {
	var keyword = component.get("v.searchKeyword"); //obtiene por auraid el valor del input
      component.set('v.StrKey', keyword); //asigna el valor ingresado al attributo
      //getProductsQuotes
      helper.getProductsQuotes(component, event);//invoca el helper pasando como parametro el componente con la
		//component.set('v.StrKey', '3000015'); 
		//helper.getValuesSelectTable(component, event, helper); 
	}
})