({
    //Method start execution
	doInit : function(cmp, event, helper) {
		var userId =  $A.get("$SObjectType.CurrentUser.Id");
        helper.getUserInfo(cmp,userId);
		helper.fetchPickListVal(cmp, 'ISSM_TypeApplication__c', 'listAppsOptions');
        helper.getCurrency(cmp,'ISSM_Currency__c','optCurrency');
	},

    //Method that is executed when selecting or deselecting a checkgroup value
    ChangeAppsItems: function (cmp, event) {
        var OptionSelected = event.getParam("value");
        cmp.set("v.selectedAppsItems" , OptionSelected);
        var opts='';
    
        for (var i = 0; i < OptionSelected.length; i++) {
            opts+=OptionSelected[i]+';';
        }
        cmp.set("v.newComboRecord.ISSM_TypeApplication__c" , opts);
    },

    //Method that is executed when selecting the currency
    changeCurrency: function (cmp, event) {
        var OptionSelected = event.getParam("value");//Obtiene los valores seleccionados
        cmp.set("v.newComboRecord.ISSM_Currency__c" , OptionSelected);
    },

    //Method run when selecting a record of the Son Component (ISSM_ShowSearchResult_lcp)
    setPriceGroup : function(cmp, event) {
        var listSelectedItems =  cmp.get("v.priceGroup"); 
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.priceGroup" , listSelectedItems);
    },

    //Method run when deselecting a record of priceGroup
    removePriceGroup : function(cmp, event) {
        var getParamFromEvent = event.getParam("recordByEvent");
        cmp.set("v.priceGroup", getParamFromEvent);
    }
})