({
    //Get current User
    getUserInfo : function(component,userId) {
        var LstUsr=[];
        var strName='';
        var action = component.get("c.getUserInfo");
        action.setParams({
            "userId": userId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                LstUsr  = response.getReturnValue();
                strName = LstUsr[0].Name;
                component.set("v.StrUserName" ,strName);
            }else{
                console.log('ERROR IN getUserInfo',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //Check values ​​from a selection list of apps
	fetchPickListVal: function(component, fieldName,targetAttribute) {
        var mode = component.get('v.mode');
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "strFld":    fieldName
        });
        var opts = [];
        var startSelect="";
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                allValues     = JSON.parse(allValues);
                if(mode == 'EDIT'){
                    var TypeApplication = component.get('v.newComboRecord').ISSM_TypeApplication__c;
                    var TypeApplicationArray = String(TypeApplication).split(';');
                    for(var i = 0; i < TypeApplicationArray.length; i++){
                        opts.push( TypeApplicationArray[i] );
                    }
                }else{
                    for (var i = 0; i < allValues.length; i++) {
                        opts.push(allValues[i].value);
                        startSelect+=allValues[i].value+';';
                    }
                }
                
                component.set("v."+targetAttribute, allValues);
                component.set("v.selectedAppsItems" , opts);
                
                if(component.get("v.newComboRecord")){
                    if(mode != 'EDIT'){
                        component.set("v.newComboRecord.ISSM_TypeApplication__c" , startSelect);
                        component.set("v.newComboRecord.ISSM_StartDate__c" ,this.setStartDate());
                    }
                    if(component.get('v.isClone')){
                        component.set("v.newComboRecord.ISSM_StartDate__c" ,this.setStartDate());
                    }
                    
                    console.log('CON NEW ORDERr');
                }else{
                    console.log('SIN NEW ORDER: ',component.get("v.newComboRecord"));
                }
            }else{
                console.log('ERROR EN fetchPickListVal',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //get currency
    getCurrency: function(cmp, fieldName,targetAttribute) {
        var action = cmp.get("c.getselectOptions");
        var opts   = [];
        action.setParams({
            "objObject":  cmp.get("v.objInfo"),
            "strFld":     fieldName
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                allValues     = JSON.parse(allValues);
                cmp.set("v."+targetAttribute, allValues);//obtiene los valores
                cmp.set("v.newComboRecord.ISSM_Currency__c",allValues[0].value);
            }else{
                console.log('ERROR EN getCurrency',response.error);    
            }
        });
        $A.enqueueAction(action);
    },

    //set start date
    setStartDate : function() {
        var StartDateLimit   = parseInt($A.get("$Label.c.TRM_StartDateLimit"));
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + StartDateLimit);
        var yy =currentDate.getFullYear();
        var mm =currentDate.getMonth()+1;
        var dd =currentDate.getDate();
        dd = (dd < 10) ? '0'+dd : dd;  
        mm = (mm < 10) ? '0'+mm : mm;
        var formattedDate = yy+'-'+mm+'-'+dd;

        return formattedDate;
    }
})