({
	//Determines what section of the path is shown
    selectFromHeaderStep1 : function(component,event,helper){
        component.set("v.currentStep", "1");
    },    

    selectFromHeaderStep2 : function(component,event,helper){
        component.set("v.currentStep", "2");
        var isEditMode = component.get('v.isEditMode');
        if(isEditMode){
            component.set('v.doCustomerSearch', true);
        }
    },

    selectFromHeaderStep3 : function(component,event,helper){
        component.set("v.currentStep", "3");
        //var renderCustomerStep = component.get('v.renderCustomerStep');
        //if(renderCustomerStep){
        //    helper.requireSelectedCustomers(component);
        //}
    },
    selectFromHeaderStep4 : function(component,event,helper){
        component.set("v.currentStep", "4");
        //helper.requireSelectedMaterials(component);
    },
    // selectFromHeaderStep5 : function(component,event,helper){
    //     component.set("v.currentStep", "5");
    // },

    /** handles the change of the currentStep, 
    it validades what is the new value to validate selected customers or materials **/
    changeCurrentStep : function(component, event, helper){
        var currentStep = component.get('v.currentStep');
        switch(currentStep){
            case '3' : 
                var renderCustomerStep = component.get('v.renderCustomerStep');
                if(renderCustomerStep){
                    helper.requireSelectedCustomers(component);
                }
                break;
            case '4' :
                helper.requireSelectedMaterials(component); 
                break;
        }
    },

    /** handles the change of the newRecordId.
    It validates if the newRecordId is different of null,
    to update attributes to notify the change, 
    do the customer search and render next step of the path **/
    handleOnChangeNewRecordId : function(component, event, helper){
        if( component.get('v.newRecordId') != '' ){
            component.set('v.isNewRecordSaved', true);
            component.set('v.doCustomerSearch', true);
            var renderCustomerStep = component.get('v.renderCustomerStep');
            if(renderCustomerStep){
                component.set("v.currentStep", "2");
            } else{
                component.set("v.currentStep", "3");
            }
        }
    },

    /** get the values of the record fields and set the attributes from the values **/
    updateRecordFields : function(component, event, helper){
        var recordFieldValueMap = event.getParam("recordFieldValueMap");
        component.set('v.recordFieldValueMap', recordFieldValueMap);
        var segmentList = recordFieldValueMap.get('TRM_Segment__c');
        component.set('v.segmentList', segmentList);
        
        var priceZoneList = recordFieldValueMap.get('TRM_StatePerZone__c');
        component.set('v.priceZoneList', priceZoneList);
        
        /** NEW **/
        var priceZone = recordFieldValueMap.get('TRM_PriceZoneOnly__c');
        component.set('v.priceZone', priceZone);
        /** / NEW **/
        
        var salesOfficeList = recordFieldValueMap.get('TRM_SalesOffice__c');
        component.set('v.salesOfficeList', salesOfficeList);
        
        /** NEW **/
        var salesOfficeId = recordFieldValueMap.get('TRM_SalesOfficeOnly__c');
        component.set('v.salesOfficeId', salesOfficeId);
        /** / NEW **/
        
        var salesOrgList = recordFieldValueMap.get('TRM_SalesOrg__c');
        component.set('v.salesOrgList', salesOrgList);
        
        var TRM_DLCGpo__c = recordFieldValueMap.get('TRM_DLCGpo__c');
        component.set('v.TRM_DLCGpo__c', TRM_DLCGpo__c);
        
        /*console.log('-----------------------------');
        for ( var [clave, valor] of recordFieldValueMap ) {
          console.log(clave + " = " + valor);
        }*/
    },

    /** handles the event action when the filters of customers are changed **/
    updatedFilters : function(component, event, helper){
        var updated = event.getParam("updated");
        component.set('v.updatedFilters', updated);
    },

    /** event action handler to get the updated selected customer list **/
    updateCustomerList : function(component, event, helper){
        var recordList = event.getParam("recordList");
        component.set('v.selectedCustomerList', recordList);
    },

    //receive the added scales
    saveScales :function(component,event,helper){
        var lstScalesForEvent  = event.getParam("records");
        var lstConditionScales = component.get('v.lstConditionScales');
        
        //remove current scales 
        if(lstScalesForEvent.length > 0){
            var id = lstScalesForEvent[0].TRM_ConditionRecord__c;
            lstConditionScales.slice().reverse().forEach(function(item, index, object) {
                if (item.TRM_ConditionRecord__c === id) {
                    lstConditionScales.splice(object.length - 1 - index, 1);
                }
            });
        }

        //Add new values
        lstScalesForEvent.forEach(element => {
            lstConditionScales.push(element);
        });

        component.set('v.lstConditionScales',lstConditionScales);
    },

    //Process conditions by debugging selected and unselected records
    generateConditions : function(component,event,helper){
        var LstProdCondition = event.getParam("lstRecordsCndt");
        var LstScalesConditn = event.getParam("lstRecordsScls");

        console.log('LstProdCondition-> ',LstProdCondition);
        console.log('LstScalesConditn-> ',LstScalesConditn);

        LstProdCondition = helper.removeDuplicates(LstProdCondition, JSON.stringify);
        LstScalesConditn.forEach(element => {
        	element.sobjectType='TRM_ProductScales__c';
        });

        component.set('v.LstProdCondition',LstProdCondition);
        component.set('v.lstConditionScales',LstScalesConditn);
    }
})