({
	//Remove duplicate 
    removeDuplicates :function (array, key) {
        var setArray = new Set();
        return array.filter(item => {
            var k = key(item);
            return setArray.has(k) ? false : setArray.add(k);
        });
    },

    /** validates if there are selected customers to be allowed to go to the next step **/
    requireSelectedCustomers : function(component){
    	var selectedCustomerList = component.get('v.selectedCustomerList');
    	if(selectedCustomerList.length == 0){
    		this.showToast('Error', $A.get("$Label.c.TRM_TitleReqCustSelect"), $A.get("$Label.c.TRM_MsgReqCustSelect"));
    		component.set("v.currentStep", "2");
    	}
    },

    /** validates if there are selected materials to be allowed to go to the next step **/
    requireSelectedMaterials : function(component){
        var LstProdCondition = component.get('v.LstProdCondition');
        if(LstProdCondition.length == 0){
            this.showToast('Error', $A.get("$Label.c.TRM_TitleReqMatSelect"), $A.get("$Label.c.TRM_MsgReqMatSelect"));
            component.set("v.currentStep", "3");
        }
    },

    // SHOW ALERT
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },
})