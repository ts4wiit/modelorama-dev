({
    closeModal : function(component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": $A.get("$Label.c.TRM_OverrideStandardNewComboNavigateToObject")
        });
        homeEvent.fire();
    }
})