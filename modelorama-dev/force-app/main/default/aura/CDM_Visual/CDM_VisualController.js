({
	doInit : function(component, event, helper) {
        var Order0 = component.get("c.getOrderOne");
        Order0.setParams({ 
               order  : '00'  
                         });
    	Order0.setCallback(this, function(response) {
    		component.set("v.Order0", response.getReturnValue());
    		});   
	$A.enqueueAction(Order0);
        
        var OrderOne = component.get("c.getOrderOne");
        OrderOne.setParams({ 
               order  : '01'  
                         });
    	OrderOne.setCallback(this, function(response) {
    		component.set("v.Order1", response.getReturnValue());
    		});   
	$A.enqueueAction(OrderOne);
        
        var OrderTwo = component.get("c.getOrderOne");
    	OrderTwo.setParams({ 
               order  : '02'  
                         });
        OrderTwo.setCallback(this, function(response) {
    		component.set("v.Order2", response.getReturnValue());
    		});   
	$A.enqueueAction(OrderTwo);
        
        var OrderThree = component.get("c.getOrderOne");
    	OrderThree.setParams({ 
               order  : '03'  
                         });
        OrderThree.setCallback(this, function(response) {
    		component.set("v.Order3", response.getReturnValue());
    		});   
	$A.enqueueAction(OrderThree);
        
        var OrderFour = component.get("c.getOrderOne");
    	OrderFour.setParams({ 
               order  : '04'  
                         });
        OrderFour.setCallback(this, function(response) {
    		component.set("v.Order4", response.getReturnValue());
    		});   
	$A.enqueueAction(OrderFour);
        
        var OrderFive = component.get("c.getOrderOne");
    	OrderFive.setParams({ 
               order  : '05'  
                         });
        OrderFive.setCallback(this, function(response) {
    		component.set("v.Order5", response.getReturnValue());
    		});   
	$A.enqueueAction(OrderFive);
		
	}
})