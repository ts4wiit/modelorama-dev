({
    hideSpinner : function(component, event, helper) {
        var spinner = component.find('spinner');
        if (component.get('v.hideSpinner'))
            $A.util.addClass(spinner, 'slds-hide');

	},
	showSpinner : function(component, event, helper) {
		var spinner = component.find('spinner');
		if (component.get('v.showSpinner'))
			$A.util.removeClass(spinner, 'slds-hide');
	}
})