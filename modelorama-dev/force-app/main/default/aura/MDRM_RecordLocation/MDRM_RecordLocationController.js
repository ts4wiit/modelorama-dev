({
	doInit : function(cmp, event, helper) {
		if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
                cmp.set('v.location', [{
                        location: {
                            Latitude: position.coords.latitude,
                            Longitude: position.coords.longitude
                        },
                }]);
                cmp.set("v.map_ready",true);
            },function(res){
                console.log("Error code " + res.code + ". " + res.message);
            });
        }else{
            console.log("Tu navegador o dispositivo no soporta la geolocalización");
        }
	},
    
    
    updateGeo: function(cmp, event, helper){
        var update = cmp.get("c.updateGeolocalization");
        update.setParams({
            accId: cmp.get('v.recordId'),
            lat: cmp.get('v.location')[0]['location']['Latitude'],
            lon: cmp.get('v.location')[0]['location']['Longitude']
        });
        update.setCallback(this, function(response){
            var state = response.getState();
            if(state=="SUCCESS"){
                cmp.set("v.update_done", true);
            }
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(update);
    },
})