({
	showToast : function(component, event, helper) {
		var params = event.getParam('arguments');

		var type = params.type;
		var title = params.title;
		var message = params.message;
		var duration = params.duration;
		var mode = params.mode;	

		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: type,
            title : title,
            message: message,
            duration: duration,
            mode: mode
        });
        toastEvent.fire();
	},

	showQuickToast : function(component, event, helper) {
		var params = event.getParam('arguments');

		var title = params.title;
		var message = params.message;
		var type = params.type;
		
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			type: type,
            title : title,
            message: message
        });
        toastEvent.fire();
	}
})