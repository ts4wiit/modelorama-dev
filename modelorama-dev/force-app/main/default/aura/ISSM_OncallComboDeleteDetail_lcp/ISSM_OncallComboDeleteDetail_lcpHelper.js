({
	getDataComboReceived : function(component, event) { 
		var lstRecordsComboQuantity = component.get('v.lstComboAccumulatedReceived');
		//var lstRecordsComboQuantity = ["a1Jg0000006N0lFEAS:6:9000000001:4:C1","a1Jg0000006N0lHEAS:5:9000000001:4:C1","a1Jg0000006N0lFEAS:6:9000000001:20:C2","a1Jg0000006N0l6EAC:5:9000000001:20:C2"]		
		var ComboSelected = component.get('v.comboNameReceived_Str');
		var lstShowcomboSelected = [];
		var lstRecordsCombination = [];
		
		console.log(component.get('v.lstComboAccumulatedReceived'));		
		var action = component.get('c.getProductsDetailxCombination');
		
		action.setParams({                  
			"lstProductsxComboCombinations" : JSON.stringify(lstRecordsComboQuantity),
			"ComboNumber" : component.get('v.comboNameReceived_Str')
        }); 
        action.setCallback(this, function(response){ 
            if(action.getState() === "SUCCESS"){
            var result = response.getReturnValue(); 
	            if(result.length > 0 ){
	            	document.getElementById('ShowCombination').style.visibility='hidden';
	            	component.set("v.lstDataProductsWPR", result); 
	            	console.log(component.get('v.lstDataProductsWPR'));
	            	
	            	for(var dataComplete of result){
	            		component.set("v.intUsedCombos", dataComplete.strQuantityUsedCombination); 
	            	}
	            }else{
	            	 document.getElementById('ShowCombination').style.visibility='visible';

	             
	            }
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);  
    },
    fnDeleteCombinationCombo : function(component, event) {
    	component.set("v.isOpenSpinner",true);
    	var CombinationIdSelected_Str = component.get("v.comboIdReceived_Str");
    	var ComboSelected = component.get('v.comboNameReceived_Str');
    	var btnCombinationdelete = event.getSource().get("v.value");
    	var CombinationSelected_Str = btnCombinationdelete.split(":");
       	var cmpEventCombinationDel = component.getEvent("cmpEvtDeleteCombination");
       	var CombinationType = component.get("v.strComboTypeReceived");     
       	var IdComboName = CombinationIdSelected_Str+'-'+ComboSelected+'*';
       	var IdComboNameMax = CombinationIdSelected_Str+'-'+ComboSelected;	    	
    	var UsedCombos  =component.get('v.intUsedCombos');
    	var intMaxCombinat = component.get("v.intMaxCombinationInt");
    	var deleting = parseInt(CombinationSelected_Str[2]);
    	var lengthMAX=0;
    	
    	
    		
    	component.set('v.strDeleteCombination',CombinationSelected_Str[0]); 
    	component.set('v.isOpenALertsComboDeleted',true); 
    	lengthMAX = parseInt(intMaxCombinat)-(parseInt(UsedCombos) - parseInt(deleting));//(intMaxCombinat + deleting) - UsedCombos ;
    	
    	document.getElementById(IdComboNameMax).max=lengthMAX;
    	
    	if(lengthMAX >= 1){
    		document.getElementById(CombinationIdSelected_Str).disabled = false;  
    		document.getElementById(CombinationIdSelected_Str+ComboSelected).checked = false;        
        }else{
            document.getElementById(CombinationIdSelected_Str).disabled = false;  
            document.getElementById(CombinationIdSelected_Str+ComboSelected).checked = false;         
        }
        document.getElementById(IdComboNameMax).value=0;    
         
    	
    	window.setTimeout(
    		$A.getCallback(function() {
    			component.set('v.isOpenALertsComboDeleted',false); 
    			cmpEventCombinationDel.setParams({
    				"StrCombinationDelete":CombinationSelected_Str[0],
    				"StrNumberCombo":CombinationSelected_Str[1],
    				"StrquantityCombo":CombinationSelected_Str[2]
    			});
		
    			cmpEventCombinationDel.fire();  
    			component.set('v.isOpenALertsComboDeleted',true);	    	
    			component.set('v.strDeletecombination',CombinationSelected_Str);	
    			component.set("v.isOpenSpinner",false);
    		}), 3000
    	)		    	
    }
    
})