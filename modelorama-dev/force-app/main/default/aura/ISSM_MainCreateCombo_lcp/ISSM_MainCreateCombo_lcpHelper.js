({
    //Initialize registration
    startCombo: function (component) {
        component.find("NewComboCreator").getNewRecord(
            "ISSM_Combos__c",
            null,//recordType
            false,//cache
            $A.getCallback(function () {
                var rec = component.get("v.newComboRecord");
                var error = component.get("v.recordError");
                if (error || (rec === null)) {
                    console.log('ERROR IN startCombo: ',rec);
                }else{
                    console.log('SUCCESS startCombo: ',rec);
                }
            })
        );
    },   

    //Redirect to the record indicated by the "recordId"
    redirectPage: function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":     recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },

    //Shows alert with the parameters sen
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    //Input validations to be able to carry out the creation of the order
    comboValidations: function (cmp) {
        var BlnrequiereFields = false;
        var minNumericFields  = false;
        var overMaxNumberBySalesStructure  = false;
        var overMaxNumberByCustomer = false;
        var nByCustomerGTNBySalesStructure = false;
        var lstProducts = cmp.get('v.lstProdRecords');
        var ProductType = $A.get("$Label.c.TRM_Product");

        BlnrequiereFields = (!cmp.get('v.newComboRecord.ISSM_StartDate__c') ||
                             !cmp.get('v.newComboRecord.ISSM_EndDate__c') ||
                             !cmp.get("v.newComboRecord.ISSM_TypeApplication__c") ||
                             !cmp.get('v.newComboRecord.ISSM_LongDescription__c') ||
                             !cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') ||
                             !cmp.get('v.newComboRecord.ISSM_ShortDescription__c')) ? true : false;

        minNumericFields = (cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') > 0) ? true : false;
        overMaxNumberBySalesStructure = (cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c') > Number($A.get("$Label.c.TRM_MaxNumberBySalesStructure") ) ) ? true : false;
        overMaxNumberByCustomer = (cmp.get('v.newComboRecord.ISSM_NumberByCustomer__c') > Number($A.get("$Label.c.TRM_MaxNumberByCustomer") ) ) ? true : false;
        nByCustomerGTNBySalesStructure = ( Number(cmp.get('v.newComboRecord.ISSM_NumberByCustomer__c')) > Number(cmp.get('v.newComboRecord.ISSM_NumberSalesOffice__c')) ) ? true : false;
        
        if(cmp.get("v.newComboRecord.ISSM_TypeApplication__c")==null){
            cmp.set("v.newComboRecord.ISSM_TypeApplication__c" , $A.get("$Label.c.TRM_DefaultApps"));
        }
        //If at least one product is not selected
        if (lstProducts.length <= 0) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg08"));
            cmp.set('v.BlnErrors', true);
        }
        //if any field is empty send alert
        if (BlnrequiereFields) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg09"));
            cmp.set('v.BlnErrors', true);
        }
        //If the maximum number of combos per structure is smaller or = to zero
        if (!minNumericFields) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg10"));
            cmp.set('v.BlnErrors', true);
        }
        
        if (overMaxNumberBySalesStructure) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumberBySalesStructureTooLong"));
            cmp.set('v.BlnErrors', true);
        }
        
        if (overMaxNumberByCustomer) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumberByCustomerTooLong"));
            cmp.set('v.BlnErrors', true);
        }
        //If the max combos by customer is greater than max combos by sales structure
        if (nByCustomerGTNBySalesStructure) {
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_NumByCustomerCanNotBeGreaterThanNumByStructure"));
            cmp.set('v.BlnErrors', true);
        }
        //If any product comes without quantity or price
        if (lstProducts.length > 0) {
            for(var i=0; i < lstProducts.length; i++){
                if(Number(lstProducts[i].IntQuantity) == 0){
                    this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg11"));
                    cmp.set('v.BlnErrors', true);
                }
            }  
        }
    },

    //Perform validation of business rules for dates
    validateDates: function (cmp, startDate, endDate) {
        //it is validated that the dates are not empty
        if (startDate != null && endDate != null) {
            // var today = new Date(startDate.replace("-", ","));
            var StartDateLimit   = parseInt($A.get("$Label.c.TRM_StartDateLimit"));
            var EndDateLimit     = parseInt($A.get("$Label.c.TRM_EndDateLimit"));
            var today = new Date();
            var startFormattedDate  = this.setNewDate(today, StartDateLimit);
            var endFormattedDate    = this.setNewDate(today, EndDateLimit);

            if (startDate < startFormattedDate) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg12"));
                cmp.set('v.BlnErrors', true);
            }
            if (endDate < startDate) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg13"));
                cmp.set('v.BlnErrors', true);
            }
            if (endDate > endFormattedDate) {
                this.showToast('error', $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg14"));
                cmp.set('v.BlnErrors', true);
            }
        }
    },

    //assign format to dates to be evaluated
    setNewDate: function (currentDate, numDays) {
        currentDate.setDate(currentDate.getDate() + numDays); //add two days
        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1; //January is 0!
        var yy = currentDate.getFullYear();
        dd = (dd < 10) ? '0' + dd : dd;
        mm = (mm < 10) ? '0' + mm : mm;
        var formattedDate = yy + '-' + mm + '-' + dd;

        return formattedDate;
    },

    //get combo list for assignment combo type
    getComboType: function (lstProducts) {
        var blnProd  = 0;
        var blnQuote = 0;
        var strComboType = null;
        var ProductType = $A.get("$Label.c.TRM_Product");
        var mixed       = $A.get("$Label.c.TRM_Mixed");
        var specific    = $A.get("$Label.c.TRM_Expecific");
        var quota       = $A.get("$Label.c.TRM_Quotas");

        for (var i = 0; i < lstProducts.length; i++) {
            blnProd  = (lstProducts[i].StrType == ProductType) ? blnProd += 1 : blnProd;
            blnQuote = (lstProducts[i].StrType != ProductType) ? blnQuote += 1 : blnQuote;
        }

        strComboType = (blnProd > 0 && blnQuote > 0) ? mixed : strComboType;
        strComboType = (blnProd > 0 && blnQuote == 0) ? specific : strComboType;
        strComboType = (blnProd == 0 && blnQuote > 0) ? quota : strComboType;

        return strComboType;
    },

    /** get sales structure parent ids of the field 'ISSM_SalesStructure__c' **/
    getParentSalesStructure: function (cmp, newComboRecordId) {
        var action = cmp.get('c.getParentSalesStructure');

        action.setParams({
            'Id': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c
        });

        action.setCallback(this, function (actionResult) {
            var state = actionResult.getState();
            if(state == 'SUCCESS'){
                var salesStructure = actionResult.getReturnValue();
                this.createComboByAccountList(cmp, newComboRecordId, salesStructure);
                console.log("SUCCESS IN getParentSalesStructure:");
            }else{
                console.log("ERROR IN getParentSalesStructure:");
            }
            
        });
        $A.enqueueAction(action);
    },
    
    /** MÉTODO ALTERNO **/
    /** 07-25-2018: This method overrides the functionality of the 'createComboByAccount' method **/
    /** Create the list of records 'ISSM_ComboByAccount__c' **/
    createComboByAccountList: function (cmp, newComboRecordId, salesStructure) {
        //get Record Type Id for developer name 'ISSM_ComboByCustomerMx'
        var ISSM_ComboByCustomerMx = cmp.get('v.recordTypeIdComboByAccount');
        var comboByAccountList = cmp.get('v.comboByAccountList');
        var deselectedRecordsList = cmp.get('v.deselectedRecordsList');
        var comboLevel = cmp.get('v.recordLimitCombos').ISSM_ComboLevel__c;
        /****************************** NIVEL NACIONAL ******************************/
        if (comboLevel == 'ISSM_National') {
            var priceGroup = cmp.get('v.priceGroup');
            var selectedDivisionList = cmp.get('v.selectedDivisionList');
            var deselectedDivisionList = cmp.get('v.deselectedDivisionList');
            var selectedOrganizationList = cmp.get('v.selectedOrganizationList');
            var deselectedOrganizationList = cmp.get('v.deselectedOrganizationList');
            var selectedOfficeList = cmp.get('v.selectedOfficeList');
            var deselectedOfficeList = cmp.get('v.deselectedOfficeList');
            var fullSelectedDivisionList = this.getFullSelectedParentRecordList(selectedDivisionList, deselectedOrganizationList, 'ParentAccount');
            fullSelectedDivisionList = this.getFullSelectedParentRecordList(fullSelectedDivisionList, deselectedOfficeList, 'GrandParentAccount');
            //fullSelectedDivisionList = this.getFullSelectedParentRecordList(selectedDivisionList, deselectedOfficeList, 'GrandParentAccount');
            
            // GUARDAR DRV
            if (priceGroup.length > 0) {
                for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                    for (var s = 0; s < fullSelectedDivisionList.length; s++) {
                        var comboByAccount = {
                            'sobjectType': 'ISSM_ComboByAccount__c',
                            'ISSM_ComboNumber__c': newComboRecordId,
                            'ISSM_RegionalSalesDirection__c': fullSelectedDivisionList[s].Id,
                            'ISSM_Segment__c': priceGroup[pgi].Id,
                            'RecordTypeId': ISSM_ComboByCustomerMx
                        };
                        comboByAccountList.push(comboByAccount);
                    }
                }
            } else {
                for (var s = 0; s < fullSelectedDivisionList.length; s++) {
                    var comboByAccount = {
                        'sobjectType': 'ISSM_ComboByAccount__c',
                        'ISSM_ComboNumber__c': newComboRecordId,
                        'ISSM_RegionalSalesDirection__c': fullSelectedDivisionList[s].Id,
                        'RecordTypeId': ISSM_ComboByCustomerMx
                    };
                    comboByAccountList.push(comboByAccount);
                }
            }
            // GUARDAR DRV />
            
            // GUARDAR ORGANIZACIONES
            if( deselectedOrganizationList.length > 0 ){                
                //obtener lista de orgs que no tienen una DRV ya agregada arriba
                var selectedOrganizationWithoutFullSelectedParent = this.getFilteredSelectedRecords(fullSelectedDivisionList, selectedOrganizationList);
                
                //obtener lista de orgs que no tengan oficinas sin selección
                var fullSelectedOrganizationList = this.getFullSelectedParentRecordList(selectedOrganizationWithoutFullSelectedParent, deselectedOfficeList, 'ParentAccount');
                if (priceGroup.length > 0) {
                    for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                        for( var f = 0; f < fullSelectedOrganizationList.length; f++ ){
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': fullSelectedOrganizationList[f].ISSM_ParentAccount__c,
                                'ISSM_SalesOrganization__c': fullSelectedOrganizationList[f].Id,
                                'ISSM_Segment__c': priceGroup[pgi].Id,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }
                } else {
                    for( var f = 0; f < fullSelectedOrganizationList.length; f++ ){
                        var comboByAccount = {
                            'sobjectType': 'ISSM_ComboByAccount__c',
                            'ISSM_ComboNumber__c': newComboRecordId,
                            'ISSM_RegionalSalesDirection__c': fullSelectedOrganizationList[f].ISSM_ParentAccount__c,
                            'ISSM_SalesOrganization__c': fullSelectedOrganizationList[f].Id,
                            'RecordTypeId': ISSM_ComboByCustomerMx
                        };
                        comboByAccountList.push(comboByAccount);
                    }
                }
            }
            // GUARDAR ORGANIZACIONES />

            // GUARDAR OFICINAS
            if( deselectedOfficeList.length > 0 ){
                //obtener lista de oficias seleccionadas cuya ORG padre tiene oficinas sin selección
                //var filteredSelectedOfficeList = this.getFilteredSelectedRecordList(deselectedOfficeList, selectedOfficeList);
                var fullSelectedOfficeList = this.getFilteredSelectedRecordList(deselectedOfficeList, selectedOfficeList);

                //obtener lista de oficinas que no tengan clientes sin selección
                //var fullSelectedOfficeList = this.getFullSelectedParentRecordList(filteredSelectedOfficeList, deselectedRecordsList, 'SalesOffice');

                if (priceGroup.length > 0) {
                    for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                        for( var f = 0; f < fullSelectedOfficeList.length; f++ ){
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': fullSelectedOfficeList[f].ISSM_ParentAccount__r.ISSM_ParentAccount__c,
                                'ISSM_SalesOrganization__c': fullSelectedOfficeList[f].ISSM_ParentAccount__c,
                                'ISSM_SalesOffice__c': fullSelectedOfficeList[f].Id,
                                'ISSM_Segment__c': priceGroup[pgi].Id,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }
                } else {
                    for( var f = 0; f < fullSelectedOfficeList.length; f++ ){
                        var comboByAccount = {
                            'sobjectType': 'ISSM_ComboByAccount__c',
                            'ISSM_ComboNumber__c': newComboRecordId,
                            'ISSM_RegionalSalesDirection__c': fullSelectedOfficeList[f].ISSM_ParentAccount__r.ISSM_ParentAccount__c,
                            'ISSM_SalesOrganization__c': fullSelectedOfficeList[f].ISSM_ParentAccount__c,
                            'ISSM_SalesOffice__c': fullSelectedOfficeList[f].Id,
                            'RecordTypeId': ISSM_ComboByCustomerMx
                        };
                        comboByAccountList.push(comboByAccount);
                    }
                }
            }
            // GUARDAR OFICINAS />
        } else
            /****************************** NIVEL DIRECCIÓN DE VENTAS ******************************/
            if (comboLevel == 'ISSM_RegionalSalesOffice') {
                var priceGroup = cmp.get('v.priceGroup');
                var selectedOrganizationList = cmp.get('v.selectedOrganizationList');
                var deselectedOrganizationList = cmp.get('v.deselectedOrganizationList');
                var selectedOfficeList = cmp.get('v.selectedOfficeList');
                var deselectedOfficeList = cmp.get('v.deselectedOfficeList');

                // GUARDAR DRV
                // ONLY SAVE THE REGIONAL SALES DIVISION IF THERE ARE NOT DESELECTED ORGS OR OFFICES
                if (deselectedOrganizationList.length == 0 && deselectedOfficeList.length == 0) {
                    //THERE ARE SOME SELECTED SEGMENTS
                    if (priceGroup.length > 0) {
                        for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                'ISSM_Segment__c': priceGroup[pgi].Id,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }
                    //THERE ARE NOT SELECTED SEGMENTES 
                    else {
                        var comboByAccount = {
                            'sobjectType': 'ISSM_ComboByAccount__c',
                            'ISSM_ComboNumber__c': newComboRecordId,
                            'ISSM_RegionalSalesDirection__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                            'RecordTypeId': ISSM_ComboByCustomerMx
                        };
                        comboByAccountList.push(comboByAccount);
                    }
                }
                // GUARDAR DRV />

                // GUARDAR ORG
                if( deselectedOrganizationList.length > 0 ){
                    //obtener lista de orgs que no tengan oficinas sin selección
                    var fullSelectedOrganizationList =
                    this.getFullSelectedParentRecordList(selectedOrganizationList, deselectedOfficeList, 'ParentAccount');
                    if (priceGroup.length > 0) {
                        for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                            for( var f = 0; f < fullSelectedOrganizationList.length; f++ ){
                                var comboByAccount = {
                                    'sobjectType': 'ISSM_ComboByAccount__c',
                                    'ISSM_ComboNumber__c': newComboRecordId,
                                    'ISSM_RegionalSalesDirection__c': fullSelectedOrganizationList[f].ISSM_ParentAccount__c,
                                    'ISSM_SalesOrganization__c': fullSelectedOrganizationList[f].Id,
                                    'ISSM_Segment__c': priceGroup[pgi].Id,
                                    'RecordTypeId': ISSM_ComboByCustomerMx
                                };
                                comboByAccountList.push(comboByAccount);
                            }
                        }
                    } else {
                        for( var f = 0; f < fullSelectedOrganizationList.length; f++ ){
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': fullSelectedOrganizationList[f].ISSM_ParentAccount__c,
                                'ISSM_SalesOrganization__c': fullSelectedOrganizationList[f].Id,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }
                }
                // GUARDAR ORG />

                // GUARDAR OFF
                if( deselectedOfficeList.length > 0 ){
                    //obtener lista de oficias seleccionadas cuya ORG padre tiene oficinas sin selección
                    //var filteredSelectedOfficeList = this.getFilteredSelectedRecordList(deselectedOfficeList, selectedOfficeList);
                    var fullSelectedOfficeList = this.getFilteredSelectedRecordList(deselectedOfficeList, selectedOfficeList);

                    //obtener lista de oficinas que no tengan clientes sin selección
                    //var fullSelectedOfficeList = this.getFullSelectedParentRecordList(filteredSelectedOfficeList, deselectedRecordsList, 'SalesOffice');

                    if (priceGroup.length > 0) {
                        for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                            for( var f = 0; f < fullSelectedOfficeList.length; f++ ){
                                var comboByAccount = {
                                    'sobjectType': 'ISSM_ComboByAccount__c',
                                    'ISSM_ComboNumber__c': newComboRecordId,
                                    'ISSM_RegionalSalesDirection__c': fullSelectedOfficeList[f].ISSM_ParentAccount__r.ISSM_ParentAccount__c,
                                    'ISSM_SalesOrganization__c': fullSelectedOfficeList[f].ISSM_ParentAccount__c,
                                    'ISSM_SalesOffice__c': fullSelectedOfficeList[f].Id,
                                    'ISSM_Segment__c': priceGroup[pgi].Id,
                                    'RecordTypeId': ISSM_ComboByCustomerMx
                                };
                                comboByAccountList.push(comboByAccount);
                            }
                        }
                    } else {
                        for( var f = 0; f < fullSelectedOfficeList.length; f++ ){
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': fullSelectedOfficeList[f].ISSM_ParentAccount__r.ISSM_ParentAccount__c,
                                'ISSM_SalesOrganization__c': fullSelectedOfficeList[f].ISSM_ParentAccount__c,
                                'ISSM_SalesOffice__c': fullSelectedOfficeList[f].Id,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }
                }
                // GUARDAR OFF />

            } else
                /****************************** NIVEL ORGANIZACIÓN DE VENTAS ******************************/
                if (comboLevel == 'ISSM_SalesOrganization') {
                    var priceGroup = cmp.get('v.priceGroup');
                    var deselectedOfficeList = cmp.get('v.deselectedOfficeList');
                    if (deselectedOfficeList.length == 0) {
                        if (priceGroup.length > 0) {
                            for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                                var comboByAccount = {
                                    'sobjectType': 'ISSM_ComboByAccount__c',
                                    'ISSM_ComboNumber__c': newComboRecordId,
                                    'ISSM_RegionalSalesDirection__c': salesStructure.parentId,
                                    'ISSM_SalesOrganization__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                    'ISSM_Segment__c': priceGroup[pgi].Id,
                                    'RecordTypeId': ISSM_ComboByCustomerMx
                                };
                                comboByAccountList.push(comboByAccount);
                            }
                        } else {
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': salesStructure.parentId,
                                'ISSM_SalesOrganization__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    } else
                        if (deselectedOfficeList.length > 0) {
                            var selectedOfficeList = cmp.get('v.selectedOfficeList');
                            if (priceGroup.length > 0) {
                                for (var pgi = 0; pgi < priceGroup.length; pgi++) {
                                    for (var i = 0; i < selectedOfficeList.length; i++) {
                                        var comboByAccount = {
                                            'sobjectType': 'ISSM_ComboByAccount__c',
                                            'ISSM_ComboNumber__c': newComboRecordId,
                                            'ISSM_RegionalSalesDirection__c': salesStructure.parentId,
                                            'ISSM_SalesOrganization__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                            'ISSM_SalesOffice__c': selectedOfficeList[i].Id,
                                            'ISSM_Segment__c': priceGroup[pgi].Id,
                                            'RecordTypeId': ISSM_ComboByCustomerMx
                                        };
                                        comboByAccountList.push(comboByAccount);
                                    }
                                }
                            } else {
                                for (var i = 0; i < selectedOfficeList.length; i++) {
                                    var comboByAccount = {
                                        'sobjectType': 'ISSM_ComboByAccount__c',
                                        'ISSM_ComboNumber__c': newComboRecordId,
                                        'ISSM_RegionalSalesDirection__c': salesStructure.parentId,
                                        'ISSM_SalesOrganization__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                        'ISSM_SalesOffice__c': selectedOfficeList[i].Id,
                                        'RecordTypeId': ISSM_ComboByCustomerMx
                                    };
                                    comboByAccountList.push(comboByAccount);
                                }
                            }

                        }

                } else
                    /****************************** NIVEL OFICINA DE VENTAS ******************************/
                    if (comboLevel == 'ISSM_SalesOffice') {
                        var priceGroup = cmp.get('v.priceGroup');
                        if (priceGroup.length > 0) {
                            for (var i = 0; i < priceGroup.length; i++) {
                                var comboByAccount = {
                                    'sobjectType': 'ISSM_ComboByAccount__c',
                                    'ISSM_ComboNumber__c': newComboRecordId,
                                    'ISSM_RegionalSalesDirection__c': salesStructure.grandParentId,
                                    'ISSM_SalesOrganization__c': salesStructure.parentId,
                                    'ISSM_SalesOffice__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                    'ISSM_Segment__c': priceGroup[i].Id,
                                    'RecordTypeId': ISSM_ComboByCustomerMx
                                };
                                comboByAccountList.push(comboByAccount);
                            }
                        } else {
                            var comboByAccount = {
                                'sobjectType': 'ISSM_ComboByAccount__c',
                                'ISSM_ComboNumber__c': newComboRecordId,
                                'ISSM_RegionalSalesDirection__c': salesStructure.grandParentId,
                                'ISSM_SalesOrganization__c': salesStructure.parentId,
                                'ISSM_SalesOffice__c': cmp.get('v.recordLimitCombos').ISSM_SalesStructure__c,
                                'RecordTypeId': ISSM_ComboByCustomerMx
                            };
                            comboByAccountList.push(comboByAccount);
                        }
                    }

        /** EXCLUSIÓN DE CLIENTES **/
        if (deselectedRecordsList.length > 0) { // deselected customers?
            for (var i = 0; i < deselectedRecordsList.length; i++) {
                var comboByAccount = {
                    'sobjectType': 'ISSM_ComboByAccount__c',
                    'ISSM_ComboNumber__c': newComboRecordId,
                    'ISSM_RegionalSalesDirection__c': deselectedRecordsList[i].ISSM_RegionalSalesDivision__c,
                    'ISSM_SalesOrganization__c': deselectedRecordsList[i].ISSM_SalesOrg__c,
                    'ISSM_SalesOffice__c': deselectedRecordsList[i].ISSM_SalesOffice__c,
                    'ISSM_Segment__c': deselectedRecordsList[i].ISSM_SegmentCode__c,
                    'ISSM_Customer__c': deselectedRecordsList[i].Id,
                    'ISSM_ExclusionAccount__c': true,
                    'RecordTypeId': ISSM_ComboByCustomerMx
                };
                comboByAccountList.push(comboByAccount);
            }
        }

        cmp.set('v.comboByAccountList', comboByAccountList);
        this.saveComboByAccount(cmp,newComboRecordId);
    },
    /** / MÉTODO ALTERNO **/
    /** returns a list of selected records where their parent records has deselected children records **/
    getFilteredSelectedRecordList: function(deselectedRecordList, selectedRecordList){
        var filteredSelectedRecordList = [];
        var parentRecordIdSet = new Set();
        for( var d = 0; d < deselectedRecordList.length; d++ ){
            parentRecordIdSet.add( deselectedRecordList[d].ISSM_ParentAccount__c );
        }
        var parentRecordIdList = Array.from( parentRecordIdSet );
        for( var s = 0; s < selectedRecordList.length; s++ ){
            var parentFound = false;
            for( var p = 0; p < parentRecordIdList.length; p++ ){
                if(!parentFound){
                    if( selectedRecordList[s].ISSM_ParentAccount__c == parentRecordIdList[p] ){
                        parentFound = true;
                    }
                }
            }
            if(parentFound){
                filteredSelectedRecordList.push( selectedRecordList[s] );
            }
        }
        return filteredSelectedRecordList;
    },
    /** returns a list of parent records with out deselected children records **/
    getFullSelectedParentRecordList: function (parentRecordList, childrenDeselectedRecordList, parentField) {
        var fullSelectedParentRecordList = [];
        var deselectedParentRecordIdSet = new Set();
        for (var i = 0; i < childrenDeselectedRecordList.length; i++) {
            if (parentField == 'GrandParentAccount') {
                deselectedParentRecordIdSet.add(childrenDeselectedRecordList[i].ISSM_ParentAccount__r.ISSM_ParentAccount__c);
            } else
            if (parentField == 'ParentAccount') {
                deselectedParentRecordIdSet.add(childrenDeselectedRecordList[i].ISSM_ParentAccount__c);
            } else if (parentField == 'SalesOffice') {
                deselectedParentRecordIdSet.add(childrenDeselectedRecordList[i].ISSM_SalesOffice__c);
            }
        }
        var deselectedParentRecordIdList = Array.from(deselectedParentRecordIdSet);
        for (var i = 0; i < parentRecordList.length; i++) {
            var found = false;
            for (var d = 0; d < deselectedParentRecordIdList.length; d++) {
                if (!found) {
                    if (parentRecordList[i].Id == deselectedParentRecordIdList[d]) {
                        found = true;
                    }
                }
            }
            if (!found) {
                fullSelectedParentRecordList.push(parentRecordList[i]);
            }
        }
        return fullSelectedParentRecordList;
    },
    /** Returns a list of selected children records only with out full selected parent record **/
    getFilteredSelectedRecords: function (fullSelectedParentRecordList, selectedChildrenList) {
        var filteredSelectedRecords = [];
        for (var s = 0; s < selectedChildrenList.length; s++) {
            var parentFound = false;
            for (var f = 0; f < fullSelectedParentRecordList.length; f++) {
                if (!parentFound) {
                    if (selectedChildrenList[s].ISSM_ParentAccount__c == fullSelectedParentRecordList[f].Id) {
                        parentFound = true;
                    }
                }
            }
            if (!parentFound) {
                filteredSelectedRecords.push(selectedChildrenList[s]);
            }
        }
        return filteredSelectedRecords;
    },
    /** Save the list of records 'ISSM_ComboByAccount__c' **/
    saveComboByAccount: function (cmp,newComboRecordId) {

        var action = cmp.get('c.saveComboByAccount');
        action.setParams({
            'comboByAccountList': cmp.get('v.comboByAccountList'),
            'StrIdCombo' :        newComboRecordId
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                // this.submitForApproval(cmp,newComboRecordId);
                console.log("SUCCESS IN saveComboByAccount");
            }else{
                console.log("ERROR IN saveComboByAccount",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //set cast sObject to ISSM_ComboByProduct__c
    castProducts: function (cmp, comboId) {
        var lstProducts = cmp.get('v.lstProdRecords');
        var ProductType = $A.get("$Label.c.TRM_Product");
        var lstSaveProducts = [];
        var ISSM_ProdByComboMx = cmp.get('v.recordTypeIdComboByProduct');
        var pos=0;
        for (var i = 0; i < lstProducts.length; i++) {
            pos+=10;
            var comboByProd = {
                'sobjectType':             'ISSM_ComboByProduct__c',
                'ISSM_ComboNumber__c':     comboId,
                'ISSM_QuantityProduct__c': lstProducts[i].IntQuantity,
                'ISSM_UnitPriceTax__c':    lstProducts[i].DecUnitPriceTax,
                'ISSM_UnitPrice__c':       lstProducts[i].DecUnitPrice,
                'ISSM_Product__c':         (lstProducts[i].StrType == ProductType) ? lstProducts[i].objProducts.Id : null,
                'ISSM_Quota__c':           (lstProducts[i].StrType != ProductType) ? lstProducts[i].objProducts.Id : null,
                'ISSM_Type__c':            lstProducts[i].StrType,
                'RecordTypeId':            ISSM_ProdByComboMx,
                'ISSM_Position__c':        ''+pos
            };
            lstSaveProducts.push(comboByProd);
        }
        cmp.set('v.comboByProductList', lstSaveProducts);
        this.saveComboByProduct(cmp);
    },

    //save records to ComboByProduct
    saveComboByProduct: function (cmp) {
        var action = cmp.get('c.saveComboByProductList');
        action.setParams({
            'comboByProductList': cmp.get('v.comboByProductList')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log("SUCCESS IN SaveComboByProduct");
            }else{
                console.log("ERROR IN saveComboByProduct",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //GET RECORD TYPE
    getRecordTypeIdByDeveloperName :  function(cmp, objectType, developerName, attributeName){
        var action = cmp.get('c.getRecordTypeIdByDeveloperName');
        var attribute = 'v.' + attributeName;
        action.setParams({
            'objectType':       objectType,
            'developerName':    developerName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var recordTypeId = response.getReturnValue();
                cmp.set( attribute, String(recordTypeId) );
                console.log("SUCCESS IN getRecordTypeIdByDeveloperName");
            }else{
                console.log("ERROR IN getRecordTypeIdByDeveloperName:",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //get price group codes for set to combo
    getPriceGroup: function (cmp) {
        var priceGroup      = cmp.get('v.priceGroup');
        var priceGroupCodes = '';
        
        for (var i = 0; i < priceGroup.length; i++) {
            priceGroupCodes += priceGroup[i].Code__c + ';';
        }
        return priceGroupCodes;
    }
})