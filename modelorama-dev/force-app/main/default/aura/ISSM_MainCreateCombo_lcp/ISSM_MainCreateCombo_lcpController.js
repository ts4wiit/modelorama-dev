({
    //START THE RECORD OF COMBO
	doInit : function(component, event, helper) {
        helper.startCombo(component);
        helper.getRecordTypeIdByDeveloperName(component, 'ISSM_ComboByAccount__c', 'ISSM_ComboByCustomerMx', 'recordTypeIdComboByAccount');
        helper.getRecordTypeIdByDeveloperName(component, 'ISSM_ComboByProduct__c', 'ISSM_ProdByComboMx', 'recordTypeIdComboByProduct');
    },

    //GET INFO OF CURRENT RECORD
	loadedRecord: function(component, event, helper) {
        var rec = component.get('v.recordId');
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("SUCCESS loadedRecord");
        }else{
            console.log('ERROR loadedRecord');
        }
    },
    
    //SAVE THE COMBO
    saveCombo: function(cmp, event, helper) {
        var startDate       = cmp.get('v.newComboRecord.ISSM_StartDate__c');
        var endDate         = cmp.get('v.newComboRecord.ISSM_EndDate__c');
        var strComboType    = '';
        var priceGroupCodes = '';
        
        try {
            cmp.set('v.BlnErrors',false);
            //performs validations on the start and end dates
            helper.validateDates(cmp,startDate,endDate);
            helper.comboValidations(cmp);
            //If there are no errors in the dates, continue to validate the rest of the fields
            if(!cmp.get('v.BlnErrors')){
                strComboType = helper.getComboType(cmp.get("v.lstProdRecords"));
                priceGroupCodes = helper.getPriceGroup(cmp);
                cmp.set("v.newComboRecord.ISSM_ComboLimit__c", cmp.get("v.recordId"));
                cmp.set("v.newComboRecord.ISSM_ComboType__c", strComboType);
                cmp.set('v.newComboRecord.ISSM_PriceGrouping__c',priceGroupCodes);
                cmp.find("NewComboCreator").saveRecord(function(saveResult) {
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                        helper.castProducts(cmp,saveResult.recordId);
                        helper.getParentSalesStructure(cmp, saveResult.recordId); //get Sales Strucure parent record Ids
                        helper.redirectPage(saveResult.recordId);
                        helper.showToast("success",$A.get("$Label.c.TRM_Msg07"),$A.get("$Label.c.TRM_Msg05"));// Show Alert    
                    }else{
                        helper.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_Msg06"));
                        console.log('ERROR IN saveCombo: ',saveResult.error);
                    }
                });
            }   
        }catch(e){
            console.log('EXCEPTION saveCombo: ',e.getMessage());
        }
    },

    //CANCEL THE COMBO
    cancelCombo: function(component, event, helper) {
        helper.redirectPage(component.get("v.recordId"));
        helper.showToast("info","Cancelar",$A.get("$Label.c.TRM_Msg03"));//Show Alert
    },

    /** get the event parameter with the list of records and updates the component attribute **/
    updateRecordList: function(component, event, helper){
        var recordListType = event.getParam("recordListType");
        var recordList = event.getParam("recordList");
        var eventType = event.getParam("eventType");
        if( recordListType == 'SalesDivision' ){
            if( eventType == 'selected' ){
                component.set('v.selectedDivisionList', recordList);
            } else if( eventType == 'deselected' ){
                component.set('v.deselectedDivisionList', recordList);
            }
        } else
        if( recordListType == 'SalesOrganization' ){
            if( eventType == 'selected' ){
                component.set('v.selectedOrganizationList', recordList);
            } else if( eventType == 'deselected' ){
                component.set('v.deselectedOrganizationList', recordList);
            }
        } else
        if( recordListType == 'SalesOffice' ){
            if( eventType == 'selected' ){
                component.set('v.selectedOfficeList', recordList);
            } else if( eventType == 'deselected' ){
                component.set('v.deselectedOfficeList', recordList);
            }
        } else
        if( recordListType == 'Account' ){
            if( eventType == 'deselected' ){
                component.set('v.deselectedRecordsList', recordList);
            }
        }
    },

    // update Deselected Account List
    updateDeselectedAccountList: function(component, event, helper){
        var recordList = event.getParam("recordList");
        component.set('v.deselectedRecordsList', recordList);
    },
    
    //Run when selecting a record of the Son Component (ISSM_ShowSearchResult_lcp)
    setPriceGroup : function(cmp, event) {
        var listSelectedItems =  cmp.get("v.priceGroup");
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.priceGroup" , listSelectedItems);
    },

    //REMOVE PRICE GROUP
    removePriceGroup : function(cmp, event) {
        var getParamFromEvent = event.getParam("recordByEvent");
        cmp.set("v.priceGroup", getParamFromEvent);
    },

    //SET NEW PRODUCTS SELECTED
    setProdRecEvt: function(cmp, event, helper){
        var listSelectedItems = cmp.get("v.lstProdRecords");
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.lstProdRecords" , listSelectedItems);
    },

    //SET NEW PRODUCTS REMOVED
    setProdRemoveEvt: function(cmp, event, helper){
        cmp.set("v.lstProdRecords" , null);
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        cmp.set("v.lstProdRecords" , selectedParamGetFromEvent);
    }
})