({
    doInit : function(component, event, helper) {		                
        helper.getDataHelper(component, event);
    },
    getSelectedName: function (component, event, helper) {
    },
    // for Display Model,set the "isOpenSendSAP" attribute to "true"
    openModel: function(component, event, helper) {
        component.set("v.isOpenSendSAP", true);
    },
    // for Hide/Close Model,set the "isOpenSendSAP" attribute to "Fasle" 
    closeModel: function(component, event, helper) {
        component.set("v.isOpenSendSAP", false);
        component.set("v.isOpenAprovals", true);
    },
    sendSapWS: function(component, event, helper) {
        var dTable = component.find("ltngCombos");      
        var lstCombos = component.get("v.SelectedCombo").length > 0 ? component.get("v.SelectedCombo") : dTable.getSelectedRows();
        if (lstCombos.length > 0 ){
            helper.callWebServiceSendSAP(component, event, lstCombos);
            component.set("v.isOpenSendSAP", false);
        }else{
            helper.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_MsgCancel02"));// muestra alerta
        }
        
    },
    setValues: function(component, event, helper) {
        var dTable = component.find("ltngCombos");        
        var selectedRows = dTable.getSelectedRows(); 
        var selectedCombo = component.get("v.SelectedCombo");
        var currentListTable = component.get("v.currentList");
        
        for (var i = 0; i < selectedRows.length; i++){
            if (!selectedCombo.includes(selectedRows[i])){
                selectedCombo.push(selectedRows[i]);
            }
        }
        component.set("v.SelectedCombo",selectedCombo);
        var rowsDelete = currentListTable.filter(item  => !selectedRows.includes(item ));
        if (selectedCombo.length > 0 && rowsDelete.length > 0){
          selectedCombo = selectedCombo.filter(item  => !rowsDelete.includes(item ));
          component.set("v.SelectedCombo",selectedCombo);
        }
            
        helper.setValues(component);
        debugger;
        var selectedRowsIds = [];
        var currentListTable = component.get("v.currentList");
        for (var i = 0; i < currentListTable.length; i++){
            if (selectedCombo.includes(currentListTable[i])){
                selectedRowsIds.push(currentListTable[i].Id);
            }
        }
        dTable.set("v.selectedRows", selectedRowsIds);   
    },
})