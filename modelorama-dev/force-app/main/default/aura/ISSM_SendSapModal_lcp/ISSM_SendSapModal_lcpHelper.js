({
    getDataHelper : function(component, event) {
        var action = component.get("c.getComboRecords");
        action.setParams({
            strObjectName : 'ISSM_Combos__c'
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.mycolumns", response.getReturnValue().lstDataTableColumns);
                if(response.getReturnValue().lstDataTableData.length > 0){                    
                    component.set("v.mydata", response.getReturnValue().lstDataTableData); 
                    this.setValues(component,response.getReturnValue().lstDataTableData);
                }else{
                    this.showToast("info",$A.get("$Label.c.TRM_MsgCancel08"),$A.get("$Label.c.TRM_MsgCancel07"));// muestra alerta
                }                
            }else if (state === 'ERROR'){
                var errors = response.getError();
                var strError='';
                if (errors.length>0) {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),errors[0].pageErrors[0].message);// muestra alerta
                } else {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel04"));// muestra alerta
                }
            }else{
                this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel05"));// muestra alerta
            }
        });
        $A.enqueueAction(action);	
    },
    setValues: function (component,LstProd){
        //component.set("v.wrapperList",LstProd);//setea en la variable ObjCall del componente lo que retorna el metodo Apex        
        var LstProd = component.get("v.mydata");   
        component.set("v.maxPage", Math.floor((LstProd.length+4)/10));//setea el numero de paginas
        var pageNumber = component.get("v.pageNumber"); // obtiene el numero de pagina
        var pageRecords = LstProd.slice((pageNumber-1)*10, pageNumber*10);//obtiene el numero de paginas
        component.set("v.currentList", pageRecords);//setea los productos de acuerdo a la paginación
    },
    callWebServiceSendSAP : function(component,event,combos) {
        console.log("JSON.stringify(combos) : "+JSON.stringify(combos));
        var action = component.get("c.sendSapCombos"); 
        action.setParams({
            strCombos : JSON.stringify(combos)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.isOpenAprovals", true);
                this.showToast("success","Success","Aprobación exitosa.");// muestra alerta
            }else if (state === 'ERROR'){
                var errors = response.getError();
                var strError='';
                if (errors.length>0) {         
                    component.set("v.isOpenSendSAP", true);
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),errors[0].pageErrors[0].message);// muestra alerta
                    
                } else {
                    this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel04"));// muestra alerta
                }
            }else{
                this.showToast("error",$A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_MsgCancel05"));// muestra alerta
            }
        });
        $A.enqueueAction(action);
    },
    //Muestra alerta con los parametros enviados
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": type,
            "title": title,
            "message": msg
        });
        resultsToast.fire();
    }
})