({
	doInit : function(component, event, helper) {
        var records = component.get("v.records");
        var pageSize = component.get("v.pageSize");
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        helper.renderPage(component);
	},
    
    renderPage : function(component, event, helper){
        var records = component.get("v.records");
        var pageSize = component.get("v.pageSize");
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        helper.renderPage(component);
    }
})