({
	renderPage : function(component) {
		var pageSize = component.get("v.pageSize");
        var records = component.get("v.records"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
        component.set("v.currentList", pageRecords);
	}
})