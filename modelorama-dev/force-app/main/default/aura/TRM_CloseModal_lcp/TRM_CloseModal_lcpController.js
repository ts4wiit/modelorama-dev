({
	//Cancel process
	cancel : function(component, event, helper) {
		component.set('v.closeModal', false);
		var compEvent = component.getEvent("closeModalAction");
        compEvent.setParams({"confirmed" : false});
        compEvent.fire();
	},

	//Confirm process
	confirm : function(component, event, helper){
        component.set('v.closeModal', false);
		var compEvent = component.getEvent("closeModalAction");
        compEvent.setParams({"confirmed" : true});
        compEvent.fire();
	}
})