({
    /** first method executed **/
    doInit: function(component, event, helper) {
        helper.getParentId(component);
        var isParentComponent = component.get('v.isParentComponent');
        if( isParentComponent == false ){
            
            if( component.get('v.searchRecordType') == 'Account' ){
                helper.limitParentRecordIdList(component);
            }

            // Fetch the account list from the Apex controller
            helper.getRecordList(component); //MODIFIED 08-06-2018
        }
        helper.initSets(component);


    },
    /** call helper method 'renderPage' to display the updated page of records in the pagination table **/
    renderPage: function(component, event, helper) {
        var filteringMode = component.get("v.filteringMode");//NEW 28-5
        var searckByListMode = component.get('v.searckByListMode');
        if( filteringMode == true || searckByListMode == true ){//NEW 28-5
            helper.renderResultPage(component);//NEW 28-5
        } else{//NEW 28-5
            helper.renderPage(component);
        }//NEW 28-5
        
    },
    updateParentRecordIdList: function(component, event, helper){
        if( component.get('v.searchRecordType') == 'Account' ){
            helper.limitParentRecordIdList(component);
        }
        helper.getRecordList(component);
    },
    updatePriceGroupList: function(component, event, helper){
        helper.getRecordList(component);
        //helper.updateSelectedAndDeselectedLists(component);
    },
    /*Select all records (when the checkbox "all" changes)*/
    handleSelectAllRecords: function(component, event, helper) {
        
        var records = component.get("v.records");
        var checkvalue = component.find("selectAll").get("v.value");
        var filteringMode = component.get("v.filteringMode");
        var searckByListMode = component.get('v.searckByListMode');
        var resultList = component.get('v.resultList');
        var recordsMap = component.get('v.recordsMap');
        // IF ACCOUNT
        if( component.get('v.searchRecordType') == 'Account' ){
            console.log('Wait a moment...');
            // IF FILTERING ACCOUNTS OR SEARCHING BY LIST
            if( filteringMode == true || searckByListMode == true  ){
                var selectedAccountMap = component.get('v.selectedAccountMap');
                var deselectedAccountMap = component.get('v.deselectedAccountMap');

                if(checkvalue){
                    for( var i = 0; i < resultList.length; i++ ){
                        resultList[i].selected = checkvalue;
                        selectedAccountMap.set(resultList[i].record.Id, resultList[i].record);
                        deselectedAccountMap.delete(resultList[i].record.Id);
                        recordsMap.set(resultList[i].record.Id, resultList[i]);
                    }
                } else{
                    for( var i = 0; i < resultList.length; i++ ){
                        resultList[i].selected = checkvalue;
                        deselectedAccountMap.set(resultList[i].record.Id, resultList[i].record);
                        selectedAccountMap.delete(resultList[i].record.Id);
                        recordsMap.set(resultList[i].record.Id, resultList[i]);
                    }
                }

                component.set('v.resultList', resultList);
                helper.renderResultPage(component);
                component.set('v.selectedAccountMap', selectedAccountMap);
                component.set('v.deselectedAccountMap', deselectedAccountMap);
                component.set('v.recordsMap', recordsMap);
                records = Array.from(recordsMap.values());
                component.set('v.records', records);

            } 
            // / IF FILTERING ACCOUNTS OR SEARCHING BY LIST
            // SELECTING , DESELECTING ALL ACCOUNTS
            else{

                    var selectedAccountMap = new Map();
                    var deselectedAccountMap = new Map();
                    if( checkvalue ){
                        for( var i = 0 ; i < records.length; i++){
                            selectedAccountMap.set(records[i].record.Id, records[i].record);
                            records[i].selected = true;
                        }
                    } 
                    else {
                        for( var i = 0 ; i < records.length; i++){
                            deselectedAccountMap.set(records[i].record.Id, records[i].record);
                            records[i].selected = false;
                        }
                    }
                    component.set('v.selectedAccountMap', selectedAccountMap);
                    component.set('v.deselectedAccountMap', deselectedAccountMap);
                    component.set('v.records', records);
                    helper.renderPage(component);

            }
            // / SELECTING , DESELECTING ALL ACCOUNTS
            console.log('Done');
        }
        // / IF ACCOUNT
        // DRV, ORG, OFF
        else{
            
            if( filteringMode == true ){
                //var currentList = component.get('v.currentList');
                var currentList = component.get('v.resultList');
                for( var i = 0; i < currentList.length; i++ ){
                    for( var r = 0; r < records.length; r++){
                        if( currentList[i].record.Id == records[r].record.Id ){
                            if( checkvalue == true ){
                                records[r].selected = true;
                                currentList[i].selected = true;
                            } else{
                                records[r].selected = false;
                                currentList[i].selected = false;
                            }
                        }
                    }
                }
                //component.set('v.currentList', currentList);
                component.set('v.resultList', currentList);
                helper.renderResultPage(component);
            } else{
                if(checkvalue == true){
                    for(var i=0; i<records.length; i++){
                        records[i].selected = true;
                    }
                }
                else{ 
                    for(var i=0; i<records.length; i++){
                        records[i].selected = false;
                    }
                }
                helper.renderPage(component);
            }
            
            component.set('v.records', records);
            helper.updateSelectedAndDeselectedLists(component);//UPDATED 18-06-2018           
        }
        // DRV, ORG, OFF
        
    },
    
    /** call the helper method 'selectRecord' to get the event parameter with the selected record **/
    selectRecord : function(component, event, helper){
        helper.selectRecord(component, event);
    },
    /** call the helper method 'deselectRecord' to get the event parameter with the deselected record **/
    deselectRecord : function(component, event, helper){
        helper.deselectRecord(component, event);
    },
    /** call the helper method 'searchByKeyWord' to get filtered results of records **/
    searchKeyChange : function(component, event, helper){
        var searckByListMode = component.get('v.searckByListMode');
        if( searckByListMode == true ){
            var SearchKeyWord = component.get('v.SearchKeyWord');
            if( SearchKeyWord == '' || SearchKeyWord == undefined){
                component.set('v.foundRecords', false);
            } else{
                helper.searchByList(component);
            }
        } else {
            helper.searchByKeyWord(component);
        }
        
    },

    searchByList: function(component, event, helper){
        var searckByListMode = component.get('v.searckByListMode');
        if( searckByListMode == true ){
            var SearchKeyWord = component.get('v.SearchKeyWord');
            if( SearchKeyWord == '' || SearchKeyWord == undefined ){
                component.set('v.foundRecords', false);
                //helper.renderBlankPage(component);
            } else{
                helper.searchByList(component);
            }
            
        } else{
            var pageSize = component.get("v.pageSize");//NEW 28-5
            var records = component.get('v.records');
            if(records.length > 0){
                component.set('v.foundRecords', true);
            } else {
                component.set('v.foundRecords', false);
            }
            //component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
            component.set('v.SearchKeyWord', '');
            component.set("v.filteringMode", false);
            component.set('v.filteredResults', false);
            
            component.set('v.pageNumber', 1);//ADDED 25-06-2018 : Set the page 1 of the first loaded records, to return
            component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
            helper.renderPage(component);
        }
    },

    calculateWidth : function(component, event, helper) {
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }
            var mouseStart=event.clientX;
            component.set("v.mouseStart",mouseStart);
            component.set("v.oldWidth",parObj.offsetWidth);
    },
    setNewWidth : function(component, event, helper) {
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = component.get("v.oldWidth");
            var newWidth = event.clientX- parseFloat(mouseStart)+parseFloat(oldWidth);
            parObj.style.width = newWidth+'px';
    },

    changedCheckbox : function(component, event, helper) {
        var recordsMap = component.get('v.recordsMap');
        var recordsMapSize = recordsMap.size;
        var records = component.get('v.records');

        var wrapper = event.getParam("wrapper");
        var record = event.getParam("record");
        var value = event.getParam("value");

        var selectedAccountMap = new Map(component.get('v.selectedAccountMap'));
        var deselectedAccountMap = new Map(component.get('v.deselectedAccountMap'));

        if( value == true ){
            selectedAccountMap.set(record.Id, record);
            deselectedAccountMap.delete(record.Id);
            
        } else
        if( value == false ){
            deselectedAccountMap.set(record.Id, record);
            selectedAccountMap.delete(record.Id);
            
        }
        var newRecordsMap = recordsMap.set(record.Id, wrapper);
            component.set('v.recordsMap', newRecordsMap);
            records = Array.from(newRecordsMap.values());
            component.set('v.records', records);
        
        component.set('v.selectedAccountMap', selectedAccountMap);
        component.set('v.deselectedAccountMap', deselectedAccountMap);
    },

    selectedAccountMapChanged : function(component, event, helper){
        helper.selectedAccountMapChanged(component);
    },

    deselectedAccountMapChanged : function(component, event, helper){
        helper.deselectedAccountMapChanged(component);
    }
})