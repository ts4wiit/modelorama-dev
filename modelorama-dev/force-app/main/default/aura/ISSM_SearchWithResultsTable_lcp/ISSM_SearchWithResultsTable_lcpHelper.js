({
    /** Get the Parent Record Id from the 'parentRecordId' attribute and then added to the list 'parentRecordIdList' **/
    getParentId: function(component){
        var parentRecordId = component.get('v.parentRecordId');
        if(parentRecordId != ""){
            var parentRecordIdList = component.get('v.parentRecordIdList');
            parentRecordIdList.length = 0;
            parentRecordIdList.push( '\'' + parentRecordId + '\'' );
            component.set('v.parentRecordIdList', parentRecordIdList);
        }
    },
    /** Get the records from the Apex controller **/
    getRecordList: function (component) {

        var deselectedRecordIdList = this.getDeselectedRecordIdList(component);//AGREGADO EL 15-05-18

        var pageSize = component.get("v.pageSize");
        var parentRecordId_List = [];
        if( component.get('v.isLimitedParentRecordIdList') == true){
            parentRecordId_List = component.get('v.limitedParentRecordIdList');
        } else{
            parentRecordId_List = component.get('v.parentRecordIdList');
        }
        var parentRecordIdList = component.get('v.parentRecordIdList');
        var records = [];
        var action = component.get('c.getRecordWrapperList');
        // set params to method
        action.setParams({
            'deselectedRecordIds': deselectedRecordIdList,
            'fieldList': component.get('v.fieldList'),
            'objectType': component.get('v.objectAPIName'),
            'fieldOrderByList': component.get('v.fieldOrderByList'),
            //'numberOfRowsToReturn': '50000',
            'numberOfRowsToReturn': component.get('v.rowsToReturnOnLoad'),
            'recordType': component.get('v.searchRecordType'),
            'parentRecordFieldName': component.get('v.parentRecordFieldName'),
            'parentRecordIdList': parentRecordId_List,
            'priceGroupList': component.get('v.priceGroupList')
        });
        // Set up the callback
        action.setCallback(this, function (actionResult) {
            records = actionResult.getReturnValue();
            if(records.length > 0){
                component.set('v.foundRecords', true);
            } else {
                component.set('v.foundRecords', false);
            }
            
            //add all retrieved records (Wrapper) to aura:attribute 'records'
            component.set('v.records', records);
            
            component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
            
            if( component.get('v.searchRecordType') == 'Account' ){
                this.addRecordsToSelectedMap(component);
            } else{
                this.updateSelectedAndDeselectedLists(component);
            }


            this.renderPage(component);
            
        });
        if( parentRecordIdList.length > 0 ){
            $A.enqueueAction(action);
        } else {
            component.set('v.records', records);
            if( component.get('v.searchRecordType') != 'Account' ){
                this.updateSelectedAndDeselectedLists(component);
            }
        }
        
    },
    /** render the updated page of records **/
    renderPage: function (component) {
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.records"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
        component.set("v.currentList", pageRecords);
    },
    renderBlankPage: function (component) {
        var currentList = component.get("v.currentList");
        currentList.length = 0;
        component.set("v.currentList", currentList);
    },
    renderResultPage: function (component) {//NEW 28-5
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.resultList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
        component.set("v.currentResultList", pageRecords);
        if( pageRecords.length > 0 ){
            component.set("v.filteredResults", true);
        } else{
            component.set("v.filteredResults", false);
        }
    },
    /** method to handle an event update when a record checkbox is selected **/
    selectRecord: function(component, event){
        //get the updated record
        var record = event.getParam("record");
        var selectedRecordId = record.Id;
        
        //var filteringMode = component.get("v.filteringMode");
        var filteredResults = component.get("v.filteredResults");
        
        if( filteredResults == true ){
            var records = component.get("v.records");
            for(var i = 0; i<records.length; i++){
                if(records[i].record.Id == selectedRecordId){
                    //if(records[i].record.Id == selectedRecordId){
                    records[i].selected = true;
                }
            }
            component.set('v.records', records);
        }

        if( component.get('v.searchRecordType') != 'Account' ){
            this.updateSelectedAndDeselectedLists(component);
        }
    },
    /** method to handle an event update when a record checkbox is deselected **/
    deselectRecord: function(component, event){
        var record = event.getParam("record");
        var selectedRecordId = record.Id;
        
        //var filteringMode = component.get("v.filteringMode");
        var filteredResults = component.get("v.filteredResults");
        
        if( filteredResults == true ){
            var records = component.get("v.records");
            for(var i = 0; i<records.length; i++){
                if(records[i].record.Id == selectedRecordId){
                    records[i].selected = false;
                }
            }
            component.set('v.records', records);
        }

        if( component.get('v.searchRecordType') != 'Account' ){
            this.updateSelectedAndDeselectedLists(component);
        }
    },
    updateSelectedAndDeselectedLists: function(component){
        var records = component.get("v.records");
        var searchRecordType = component.get('v.searchRecordType');
        var selectedDivision_List = [];// Array of selected division
        var deselectedDivision_List = [];// Array of deselected division
        var selectedOrganization_List = [];// Array of selected organization
        var deselectedOrganization_List = [];// Array of deselected organization
        var selectedOffice_List = [];// Array of selected office
        var deselectedOffice_List = [];// Array of deselected office
        var selectedRecords_List = [];
        var deselectedRecords_List = [];
        
        for(var i = 0; i<records.length; i++){
            if(records[i].selected == true){
                if( searchRecordType == 'ISSM_RegionalSalesDivision' ){
                    selectedDivision_List.push( records[i].record );
                } else if( searchRecordType == 'SalesOrg' ){
                    selectedOrganization_List.push( records[i].record );
                } else if( searchRecordType == 'SalesOffice' ){
                    selectedOffice_List.push( records[i].record );
                } else if( searchRecordType == 'Account' ){
                    selectedRecords_List.push( records[i].record );
                }
                
            } else if(records[i].selected == false){
                if( searchRecordType == 'ISSM_RegionalSalesDivision' ){
                    deselectedDivision_List.push( records[i].record );
                } else if( searchRecordType == 'SalesOrg' ){
                    deselectedOrganization_List.push( records[i].record );
                } else
                if( searchRecordType == 'SalesOffice' ){
                    deselectedOffice_List.push( records[i].record );
                } else
                if( searchRecordType == 'Account' ){
                    deselectedRecords_List.push( records[i].record );
                }
            }
        }
        
        component.set('v.selectedDivision_List', selectedDivision_List);
        component.set('v.selectedOrganization_List', selectedOrganization_List);
        component.set('v.selectedOffice_List', selectedOffice_List);
        component.set('v.selectedRecords_List', selectedRecords_List);
        component.set('v.deselectedRecords_List', deselectedRecords_List);

        if( component.get('v.searchRecordType') == 'ISSM_RegionalSalesDivision' ){
            this.updateRecordList(component, 'SalesDivision', selectedDivision_List, 'selected');
            this.updateRecordList(component, 'SalesDivision', deselectedDivision_List, 'deselected');
        }
        if( component.get('v.searchRecordType') == 'SalesOrg' ){
            this.updateRecordList(component, 'SalesOrganization', selectedOrganization_List, 'selected');
            this.updateRecordList(component, 'SalesOrganization', deselectedOrganization_List, 'deselected');
        }
        if( component.get('v.searchRecordType') == 'SalesOffice' ){
            this.updateRecordList(component, 'SalesOffice', selectedOffice_List, 'selected');
            this.updateRecordList(component, 'SalesOffice', deselectedOffice_List, 'deselected');
        }
        if( component.get('v.searchRecordType') == 'Account' ){
            this.updateRecordList(component, 'Account', selectedRecords_List, 'selected');
            this.updateRecordList(component, 'Account', deselectedRecords_List, 'deselected');
        }

    },
    /** updates the list of records and fires an event to notify to the parent component **/
    updateRecordList: function(component, recordListType, recordList, eventType){

            var updateEvent = component.getEvent("updateRecordList");
            updateEvent.setParams({ 
                "recordListType": recordListType,
                "recordList": recordList,
                "eventType": eventType
            });
            updateEvent.fire();
          
    },
    
    /** get filtered results by key word, from Apex controller **/
    searchByKeyWord: function (component) {
        
        var searchKeyWord = component.get("v.SearchKeyWord");
        var selectAllLabel = component.find('selectAllLabel');
        var pageSize = component.get("v.pageSize");//NEW 28-5
        if (searchKeyWord == "") {
            var records = component.get('v.records');
            if(records.length > 0){
                component.set('v.foundRecords', true);
            } else {
                component.set('v.foundRecords', false);
            }
            component.set('v.pageNumber', 1);//ADDED 20-06-2018 : Set the page 1 of the first loaded records, to return
            component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
            
            this.renderPage(component);
            component.set("v.filteringMode", false);
            component.set('v.filteredResults', false);
            
        } else {
            
            var searchRecordType = component.get('v.searchRecordType');
            var parentRecordId = component.get('v.parentRecordId');
            component.set("v.filteringMode", true);
            
            var deselectedRecordIdList = this.getDeselectedRecordIdList(component);//AGREGADO EL 15-05-18
            
            // call the apex class method 
            var action = component.get("c.searchByKeyWord");
            // set param to method  
            action.setParams({
                'deselectedRecordIds': deselectedRecordIdList,
                'fieldList': component.get('v.fieldList'),
                'objectType': component.get('v.objectAPIName'),
                'fieldOrderByList': component.get('v.fieldOrderByList'),
                'numberOfRowsToReturn': component.get('v.rowsToReturnOnSearchByWord'),
                'keyWord': component.get("v.SearchKeyWord"),
                'searchByList': component.get('v.searckByListMode'),
                'recordType': component.get('v.searchRecordType'),
                'parentRecordFieldName': component.get('v.parentRecordFieldName'),
                'parentRecordIdList': component.get('v.parentRecordIdList'),
                'priceGroupList': component.get('v.priceGroupList')
            });
            // set a callBack    
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var recordsResponse = response.getReturnValue();
                    if(recordsResponse.length > 0){
                        component.set('v.foundRecords', true);
                    } else {
                        component.set('v.foundRecords', false);
                    }
                    
                    //component.set("v.currentList", recordsResponse);

                    component.set('v.resultList', recordsResponse);//NEW 28-5
            
                    component.set("v.maxPage", Math.floor((recordsResponse.length + (pageSize - 1)) / pageSize));//NEW 28-5

                    component.set('v.pageNumber', 1);//NEW 28-5

                    //this.activeSelectAll(component);
                    this.renderResultPage(component);//NEW 28-5
                }
                
            });
            // enqueue the Action  
            $A.enqueueAction(action);
            
        }
        
    },
    /** get results filtered by list, from Apex controller **/
    searchByList: function(component){

        var deselectedRecordIdList = this.getDeselectedRecordIdList(component);//AGREGADO EL 15-05-18
        var pageSize = component.get("v.pageSize");//NEW 28-5


        // call the apex class method 
        var action = component.get("c.searchByKeyWord");
        // set param to method  
        action.setParams({
            'deselectedRecordIds': deselectedRecordIdList,
            'fieldList': component.get('v.fieldList'),
            'objectType': component.get('v.objectAPIName'),
            'fieldOrderByList': component.get('v.fieldOrderByList'),
            //'numberOfRowsToReturn': '50',
            'numberOfRowsToReturn': component.get('v.rowsToReturnOnSearchByList'),
            'keyWord': component.get("v.SearchKeyWord"),
            'searchByList': component.get('v.searckByListMode'),
            'recordType': component.get('v.searchRecordType'),
            'parentRecordFieldName': component.get('v.parentRecordFieldName'),
            'parentRecordIdList': component.get('v.parentRecordIdList'),
            'priceGroupList': component.get('v.priceGroupList')
        });
        // set a callBack    
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var recordsResponse = response.getReturnValue();
                if(recordsResponse.length > 0){
                    component.set('v.foundRecords', true);
                } else {
                    component.set('v.foundRecords', false);
                }
                
                //component.set("v.currentList", recordsResponse);

                component.set('v.resultList', recordsResponse);//NEW 28-5
            
                    component.set("v.maxPage", Math.floor((recordsResponse.length + (pageSize - 1)) / pageSize));//NEW 28-5

                    component.set('v.pageNumber', 1);//NEW 28-5

                    this.activeSelectAll(component);
                    this.renderResultPage(component);//NEW 28-5
                
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },

    initSets: function(component) {
        var selectedAccountMap = new Map();
        var deselectedAccountMap = new Map();
        var recordsMap = new Map();
        component.set('v.selectedAccountMap', selectedAccountMap);
        component.set('v.deselectedAccountMap', deselectedAccountMap);
        component.set('v.recordsMap', recordsMap);
    },

    getDeselectedRecordIdList: function(component){
        var records = component.get('v.records');
        var deselectedRecordIdList = [];
        if( component.get('v.searchRecordType') == 'Account' ){
            var deselectedAccountMap = component.get('v.deselectedAccountMap');
            if( deselectedAccountMap.size > 0 ){
                for (var clave of deselectedAccountMap.keys()) {
                  deselectedRecordIdList.push( clave);
                }
            }
            
        } else{
            for( var i = 0; i < records.length; i++ ){
                if( records[i].selected == false ){
                    deselectedRecordIdList.push( records[i].record.Id );
                }
            }
        }
        return deselectedRecordIdList;
    },

    addRecordsToSelectedMap: function(component) {
        var records = component.get('v.records');
        var recordsMap = component.get('v.recordsMap');
        var selectedAccountMap = new Map();
        for( var i = 0 ; i < records.length; i++){
            selectedAccountMap.set(records[i].record.Id, records[i].record);
            recordsMap.set(records[i].record.Id, records[i]);
        }
        component.set('v.selectedAccountMap', selectedAccountMap);
        component.set('v.recordsMap', recordsMap);
    },

    selectedAccountMapChanged: function(component){
        var selectedAccountMap = component.get('v.selectedAccountMap');
        var recordList = Array.from( selectedAccountMap.values() );
        var updateEvent = component.getEvent("updateSelectedAccountList");
            updateEvent.setParams({ 
                "recordList": recordList
            });
            updateEvent.fire();
    },

    deselectedAccountMapChanged: function(component){
        var deselectedAccountMap = component.get('v.deselectedAccountMap');
        var recordList = Array.from( deselectedAccountMap.values() );
        var updateEvent = component.getEvent("updateDeselectedAccountList");
            updateEvent.setParams({ 
                "recordList": recordList
            });
            updateEvent.fire();
    },

    limitParentRecordIdList : function(component){
        var parentRecordIdList = component.get('v.parentRecordIdList');
        var limitedParentRecordIdList = [];
        if( parentRecordIdList.length >= 1 ){
            limitedParentRecordIdList.push( parentRecordIdList[0] );
        }
        component.set('v.limitedParentRecordIdList', limitedParentRecordIdList);
        component.set('v.isLimitedParentRecordIdList', true);
    },

    activeSelectAll : function(component){
        var check = component.find("selectAll");
        var checkvalue = check.get("v.value");
        if(checkvalue == false){
            check.set('v.value', true);
        }

    }

})