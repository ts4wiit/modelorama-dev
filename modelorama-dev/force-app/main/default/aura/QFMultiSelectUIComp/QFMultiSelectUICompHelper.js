({

    setInfoText: function(component, labels) {

        if (labels.length === 0) {

            component.set("v.infoText", $A.get("$Label.c.CDM_TextHelp2"));

        }

        if (labels.length === 1) {

            component.set("v.infoText", labels[0]);

        }

        else if (labels.length > 1) {

            component.set("v.infoText", labels.length + " " + $A.get("$Label.c.CDM_TextHelp3"));

        }

    },

    

    getSelectedValues: function(component){

        var options = component.get("v.options_");

        console.log('options:='+options);

        var values = [];

        if(options!==undefined){

            options.forEach(function(element) {

                if (element.selected) {

                    values.push(element.Name);

                }

            });

        }
console.log('values:='+values);
   return values;

    },

    

    getSelectedLabels: function(component){

        var options = component.get("v.options_");

        var labels = [];

        if(options!==undefined){

            options.forEach(function(element) {

                if (element.selected) {

                    labels.push(element.Name);

                }

            });  

        }

        

        return labels;

    },

    

    despatchSelectChangeEvent: function(component,values){

        var compEvent = component.getEvent("selectChange");

        compEvent.setParams({ "values": values });
console.log('event:='+values);
        compEvent.fire();

    }

})