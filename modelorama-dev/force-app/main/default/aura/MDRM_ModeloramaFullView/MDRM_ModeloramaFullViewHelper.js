({
	/*getImages : function(cmp) {
		var action = cmp.get("c.getImages");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.images', response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	},*/
    getSummary : function(cmp, recordId, order) {
        var action = cmp.get('c.getSummary');
        action.setParams({
            recordId: recordId,
            soqlOrder : order
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mpMonths = cmp.get('v.mpMonthsVal');
                for(var key in mpMonths){
                    console.log('Key: ' + key);
                }
                
                var lsummaryAx = new Array();
                var lsummary = response.getReturnValue();
                
                lsummary.forEach(function(row){
                    row.linkName = '/'+row.Id;
                    row.MDRM_MixHighEnd__c = row.MDRM_MixHighEnd__c/100;
                    row.MDRM_MixAboveCore__c = row.MDRM_MixAboveCore__c/100;
                    //row.MDRM_Month__c = mpMonths[row.MDRM_Month__c];
                    row.MDRM_Month__c = mpMonths[row.MDRM_Month__c] + '/' + row.MDRM_Year__c;
                });
                cmp.set('v.lsummary', lsummary);
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchPicklistValues : function(cmp, objName, fieldName) {
        var action = cmp.get('c.fetchPicklistLabels');
        action.setParams({
            objName: objName,
            fieldName: fieldName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mpPickVal = new Map();
                mpPickVal = response.getReturnValue();
                for(var key in mpPickVal){
                    console.log('Key helper: ' + key + ' Val: ' + mpPickVal[key]);
                }
                cmp.set('v.mpMonthsVal',mpPickVal);
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAccount : function(cmp, accId) {
        var ownerId = '';
        var action = cmp.get('c.getAccount');
        action.setParams({
            id: accId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var cta = response.getReturnValue();
                cmp.set('v.cta',cta);
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    getUser : function(cmp, ownerId) {
        var action = cmp.get('c.getUser');
        action.setParams({
            id: ownerId
        });
        
        console.log('Owner-4: ' + ownerId);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var user = response.getReturnValue();
                
                console.log('User' + JSON.stringify(user));
                
                cmp.set('v.user', user);
                cmp.set('v.supervisor', user.MDRM_SupervisorApprover__c);
                cmp.set('v.lider', user.MDRM_LiderGerenteApprover__c);
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    getVistaPermission : function(cmp) {
        var action = cmp.get('c.getEditPermission');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var editPermission = response.getReturnValue();
                
                console.log('Edit permission' + editPermission);
                
                if(editPermission) {
                    cmp.set('v.editPermission','view');
                }
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    getDRV : function(cmp, roleId) {
        var action = cmp.get('c.getDRV');
        action.setParams({
            id : roleId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var user = response.getReturnValue();
                
                console.log('User' + JSON.stringify(user));
                
                cmp.set('v.drvUser', user);
            } else if (state === 'INCOMPLETE') {
            
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    capitalize : function(s) {
        if (typeof s !== 'string') return ''
  			return s.charAt(0).toUpperCase() + s.slice(1)
    },
    getBusinessmanRelated : function(cmp, tienda) {
        var action = cmp.get('c.getBusinessman');
		action.setParams({
            id : tienda
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                
                console.log('Businessman: ' + JSON.stringify(result));
                
                cmp.set('v.businessman', result);
            } else if(state === 'INCOMPLETE') {
                
            } else if(state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchDocuments : function(cmp, tienda, lfileName) {
        var action = cmp.get('c.fetchDocuments');
		action.setParams({
            id : tienda,
            lfileName : lfileName
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                
                console.log('Businessman: ' + JSON.stringify(result));
                
                cmp.set('v.mpDocument', result);
            } else if(state === 'INCOMPLETE') {
                
            } else if(state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchSurveys : function(cmp, businessman, tienda) {
        var action = cmp.get('c.getBusinessmanSurveys');
        
        action.setParams({
            businessman : businessman,
            tienda : tienda
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var result = response.getReturnValue();
                
                console.log('Surveys: ' + JSON.stringify(result));
                
                cmp.set('v.mpHistDoc', result);
            } else if(state === 'INCOMPLETE') {
                
            } else if(state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    sortData : function (cmp, fieldName, sortDirection) {
        var recordId = cmp.get('v.recordId');
        var data = cmp.get("v.lsummary");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            //data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
            data.sort(this.getSummary(cmp, recordId, sortDirection))
        );
        cmp.set("v.lsummary", data);
    }
    /*sortBy : function (field, reverse, primer) {
        var key = primer ? function(x) {
 			return primer(x[field]);
		} : function(x) {
 			return x[field];
		};
        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    }*/
})