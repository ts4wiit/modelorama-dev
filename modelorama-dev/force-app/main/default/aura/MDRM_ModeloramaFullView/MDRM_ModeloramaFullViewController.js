({
    doInit : function(cmp, evt, helper) {
        var recordId = cmp.get('v.recordId');
        
        //Setup table headers
        cmp.set('v.columns', [
            {label: 'Venta', fieldName: 'linkName', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
            {label: 'Venta Mensual promedio en Hectolitro', fieldName: 'MDRM_AveMonthlySaleHect__c', type: 'currency'},
            {label: '%Mix de High End', fieldName: 'MDRM_MixHighEnd__c', type: 'percent'},
            {label: '%Mix de Above Core', fieldName: 'MDRM_MixAboveCore__c', type: 'percent'},
            {label: '# de SKUs', fieldName: 'MDRM_SKUs__c', type: 'number'},
            {label: 'Mes/Año', fieldName: 'MDRM_Month__c', type: 'text', sortable: true}
            //{label: 'Mes', fieldName: 'MDRM_Month__c', type: 'text'},
            //{label: 'Año', fieldName: 'MDRM_Year__c', type: 'text', sortable: true}
        ]);
        
        //helper.getImages(cmp);
        helper.fetchPicklistValues(cmp, 'MDRM_Sale_Summary__c','MDRM_Month__c');
        helper.getSummary(cmp, recordId, 'desc');
        helper.getAccount(cmp, recordId);
        helper.getUser(cmp, recordId);
        helper.getVistaPermission(cmp);
    },
    handleSubmit: function(cmp, evt) {
        const fields = evt.getParam('fields');
        cmp.find('storeInfoForm').submit(fields);
    },
    handleSuccess: function(cmp, evt) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            'title': 'Success: ',
            'type': 'success',
            'mode': 'dismissable',
            'message': 'Success'
        });
        toastEvent.fire();
        
        $A.get('e.force:refreshView').fire();
    },
    handleError: function(cmp, evt) {
        let toastEvent = $A.get("e.force:showToast");
        
        // Get the error message
        var errorMessage = evt.getParam("message");
        var errorDetail = evt.getParam("detail");
        
        /*console.log('Event params: ' + evt.getParams());
        var errorhand = evt.getParams();
        for (var f in errorhand) {
            console.log(f + ' = ' + errorhand[f])
        }*/
        
        toastEvent.setParams({
            'title': 'Error: ' + errorMessage,
            'type': 'error',
            'mode': 'dismissable',
            'message': errorDetail
        });
        // Fire error toast event ( Show toast )
        toastEvent.fire();
    },
    fetchOwnerInfo : function(cmp, evt, helper) {
        var user = cmp.get('v.user');
        
        console.log('Role Id: ' + user.UserRoleId);
        
        helper.getDRV(cmp, user.UserRoleId);
    },
    editBusinessman : function(cmp, evt, helper) {
        cmp.set('v.businessEditable',true);
    },
    cancelBusinessman : function(cmp, evt, helper) {
        cmp.set('v.businessEditable',false);
    },
    submitBusinessman : function(cmp, evt, helper) {
        cmp.find("businessmanForm").submit();
        cmp.set('v.businessEditable',false);
    },
    handleExpansor : function(cmp, evt, helper) {
        var account = cmp.get('v.cta');
        var nameMonth = '';
        if(account.MDRM_Opening_Date__c != null) {
            nameMonth = $A.localizationService.formatDate(account.MDRM_Opening_Date__c, "MMMM");
        }
        
        cmp.set('v.nameMonth', helper.capitalize(nameMonth));
    },
    handleBusinessman : function(cmp, evt, helper) {
        var recordId = cmp.get('v.recordId');
        helper.getBusinessmanRelated(cmp, recordId);
    },
    handleLegal : function(cmp, evt, helper) {
        var tienda = cmp.get('v.recordId');
        var lfileName = cmp.get('v.documents');
        
        console.log('tienda: ' + tienda);
        console.log('lfileName: ' + lfileName);
        
        helper.fetchDocuments(cmp, tienda, lfileName);
    },
    handleHistoryPicture : function(cmp, evt, helper) {
        var businessman = cmp.get('v.businessman');
        var recordId = cmp.get('v.recordId');
        if(businessman != null) 
        	helper.fetchSurveys(cmp, businessman.Id, recordId);
        else
            helper.fetchSurveys(cmp, '', recordId);
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        setTimeout($A.getCallback(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            
            console.log('Field Name: ' + fieldName);
            console.log('Sort direction: ' + sortDirection);
            
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }), 0);
    },
})