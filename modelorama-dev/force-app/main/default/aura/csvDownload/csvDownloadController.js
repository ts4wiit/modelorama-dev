({

//HANDLER EVENT
EventShowCSV: function(component, event, helper) {
  var parametrohijo0 = event.getParam("showCSVDetails");
  var parametrohijo1 = event.getParam("visualLevel");
  var parametrohijo2 = event.getParam("idLevelTxt");
  var parametrohijo3 = event.getParam("tourName");
  var parametrohijo4 = event.getParam("tourStatus");
  var parametrohijo5 = event.getParam("executionDate");

  component.set("v.showCsvButton", parametrohijo0);

  if(parametrohijo0) {
    component.set("v.visualLevel", parametrohijo1);
    component.set("v.idLevelTxt", parametrohijo2);
    component.set("v.tourName", parametrohijo3);
    component.set("v.tourStatus", parametrohijo4);
    component.set("v.executionDate", parametrohijo5);
    helper.onLoad(component, event);
  }
},

//DOWNLOAD CSV
downloadCsv : function(component,event,helper) {
  var stockData = component.get("v.ListOfDocs");
  var csv = helper.convertArrayOfObjectsToCSV(component,stockData);
  if (csv == null) {
    return;
  }
  else {
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_self'; // 
    hiddenElement.download = 'ExportData.csv';// CSV file Name* you can change it.[only name not .csv] 
    document.body.appendChild(hiddenElement); // Required for FireFox browser
    hiddenElement.click(); // using click() js function to download csv file
  }
}
})