({
  onLoad: function(component, event) {
    //call apex class method
    var action = component.get('c.generateCSVDoc');

    action.setParams({strVisualLevel : component.get('v.visualLevel'), strIdLevelField : component.get('v.idLevelTxt'), strTourStatus : component.get('v.tourStatus'), strExecutionDate : component.get('v.executionDate')});
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set('v.ListOfDocs', response.getReturnValue());
      }
      if (component.get('v.ListOfDocs') == null) {
        component.set('v.showCsvButton', false);
      }
    });
    $A.enqueueAction(action);
  },

  convertArrayOfObjectsToCSV : function(component,objectRecords) {
    var csvStringResult, counter, keys, columnDivider, lineDivider;
    
    // check if "objectRecords" parameter is null, then return from function.
    if (objectRecords == null || !objectRecords.length) {
      return null;
    }

    // store ,[comma] in columnDivider variabel for sparate CSV values and 
    // for start next line use '\n' [new line] in lineDivider varaible.
    columnDivider = ',';
    lineDivider ='\n';

    // in the keys variable store fields API Names as a key.
    // this labels use in CSV file header.
    keys = ['strOrgId',
            'strOrgName',
            'strOffId',
            'strOffName',
            'strRouId',
            'strTouId',
            'strDocSAP',
            'strTipoDoc',
            'strEstatus'];

    //Rows Labels.
    labels = ['Id de Organización de Ventas',
            'Organización de Ventas',
            'Id de Oficina de Ventas',
            'Oficina de Ventas',
            'Id de Ruta',
            'Id de Tour',
            'Número de Documento SAP',
            'Tipo de documento',
            'Estatus'];
    csvStringResult = '';
    csvStringResult += labels.join(columnDivider);
    csvStringResult += lineDivider;

    for(var i=0; i < objectRecords.length; i++){ 
      counter = 0;
      for(var sTempkey in keys) {
        var skey = keys[sTempkey] ;
        // add , [comma] after every String value,. [except first]
        if(counter > 0){ 
          csvStringResult += columnDivider; 
        }
        csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
        counter++;
      } // inner for loop close 
      csvStringResult += lineDivider;
    }// outer main for loop close
    // return the CSV formate String
    return csvStringResult;
    },
})