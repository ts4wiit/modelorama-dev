({
    /** get the values of the select fields to set to the attributes **/
	handleOnChangeSequence : function(component, event, helper){
        var conditionClass = component.find('TRM_ConditionClass__c').get('v.value');
        var conditionSequence = component.find('TRM_AccessSequence__c').get('v.value');
        if(conditionClass != '' && conditionSequence != ''){
        	component.set('v.conditionClass', conditionClass);
        	component.set('v.accessSequence', conditionSequence);
            component.set('v.externalKey', conditionClass + '_' + conditionSequence);
        	console.log('externalKey:' + component.get('v.externalKey') );
        }
        
    }
})