({
    /** check if the mode is EDIT, so then set the attribute 'priceGroup' with the received array from parent */
    updatePriceGroup: function(component, event, helper){
        if( component.get('v.mode') == 'EDIT' ){//if the mode is EDIT
            //then, set the attribute 'priceGroup' with the array received of 'selectedPriceGroup'
            var selectedPriceGroup = component.get('v.selectedPriceGroup');
            component.set('v.priceGroup', selectedPriceGroup);
        }
    },
    //Determines what section of the path is shown
    selectFromHeaderStep1 : function(component,event,helper){
        component.set("v.currentStep", "1");
    },

    selectFromHeaderStep2 : function(component,event,helper){
        component.set("v.currentStep", "2");
    },

    selectFromHeaderStep3 : function(component,event,helper){
        component.set("v.currentStep", "3");
    },

    selectFromHeaderStep4 : function(cmp,event,helper){
        cmp.set("v.currentStep", "4");
        helper.getSumProducts(cmp);
        cmp.set('v.newComboRecord',cmp.get('v.newComboRecord'));
        cmp.set("v.lstProdRecords" , cmp.get('v.lstProdRecords'));
    },
    
    activeTab : function(component, event, helper){
        var tabId = event.getSource().getLocalId();
        if( tabId == 'ORG' ){
            component.set('v.isOFFVisible', true);
        } else if( tabId == 'OFF' ){
            component.set('v.isCTEVisible', true);
        }
    },
    /** get the event parameter with the list of records and updates the component attribute **/
    updateRecordList: function(component, event, helper){
        var recordListType = event.getParam("recordListType");
        var recordList = event.getParam("recordList");
        var eventType = event.getParam("eventType");
        if( recordListType == 'SalesDivision' ){
            if( eventType == 'selected' ){
                var selectedDivisionIdList = [];
                for(var i=0; i<recordList.length; i++){
                    selectedDivisionIdList.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.selectedDivisionIdList', selectedDivisionIdList);
            } else if( eventType == 'deselected' ){
                var deselectedRecords = [];
                for(var i=0; i<recordList.length; i++){
                    deselectedRecords.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.deselectedDivisionIdList', deselectedRecords);
            }
        } else
        if( recordListType == 'SalesOrganization' ){
            if( eventType == 'selected' ){
                var selectedOrganizationIdList = [];
                for(var i=0; i<recordList.length; i++){
                    selectedOrganizationIdList.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.selectedOrganizationIdList', selectedOrganizationIdList);
            } else if( eventType == 'deselected' ){
                var deselectedRecords = [];
                for(var i=0; i<recordList.length; i++){
                    deselectedRecords.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.deselectedOrganizationIdList', deselectedRecords);
            }
        } else
        if( recordListType == 'SalesOffice' ){
            if( eventType == 'selected' ){
                var selectedOfficeIdList = [];
                for(var i=0; i<recordList.length; i++){
                    selectedOfficeIdList.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.selectedOfficeIdList', selectedOfficeIdList);
            } else if( eventType == 'deselected' ){
                var deselectedRecords = [];
                for(var i=0; i<recordList.length; i++){
                    deselectedRecords.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.deselectedOfficeIdList', deselectedRecords);
            }
        } else


        if( recordListType == 'Account' ){
            if( eventType == 'selected' ){
                var selectedAccountIdList = [];
                for(var i=0; i<recordList.length; i++){
                    selectedAccountIdList.push('\'' + recordList[i].Id + '\'' );
                }
                component.set('v.selectedAccountIdList', selectedAccountIdList);
                component.set('v.selectedRecordsList', recordList);
            } else if( eventType == 'deselected' ){
                component.set('v.deselectedRecordsList', recordList);
            }
        }
    },

    //SET NEW ACCNTS SELECTED
    updateSelectedAccountList: function(component, event, helper){
        var recordList = event.getParam("recordList");
        var selectedAccountIdList = [];
        for(var i=0; i<recordList.length; i++){
            selectedAccountIdList.push('\'' + recordList[i].Id + '\'' );
        }
        component.set('v.selectedAccountIdList', selectedAccountIdList);
        component.set('v.selectedRecordsList', recordList);
    },

    //REMOVE NEW ACCNTS SELECTED
    updateDeselectedAccountList: function(component, event, helper){
        var recordList = event.getParam("recordList");
        component.set('v.deselectedRecordsList', recordList);
    },

    // Run when selecting a record of the Son Component (ISSM_ShowSearchResult_lcp)  
    setPriceGroup : function(cmp, event) {
        var listSelectedItems =  cmp.get("v.priceGroup"); 
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.priceGroup" , listSelectedItems);
    },

    //REMOVE NEW ACCNTS SELECTED
    removePriceGroup : function(cmp, event) {
        var getParamFromEvent = event.getParam("recordByEvent");
        cmp.set("v.priceGroup", getParamFromEvent);
    },

    //SET NEW PRODUCTS SELECTED
    setProdRecEvt: function(cmp, event, helper){
        var listSelectedItems = cmp.get("v.lstProdRecords");
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.lstProdRecords" , listSelectedItems);
    },

    //SET NEW PRODUCTS REMOVED
    setProdRemoveEvt: function(cmp, event, helper){
        cmp.set("v.lstProdRecords" , null);
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        cmp.set("v.lstProdRecords" , selectedParamGetFromEvent);
    }
})