({
	getSumProducts : function(cmp) {
        var getRecords = cmp.get('v.lstProdRecords');
        var sum = 0.0;
        for(var i=0; i < getRecords.length; i++){
            sum += parseFloat(getRecords[i].DecUnitPriceTax)*parseInt(getRecords[i].IntQuantity);
        }
        cmp.set('v.SumProds',sum);
        
        //Validate if apps is null
        if(cmp.get("v.newComboRecord.ISSM_TypeApplication__c")==null){
            cmp.set("v.newComboRecord.ISSM_TypeApplication__c" , 'ALLMOBILE;BDR;B2B;TELESALES;');
        }
    },

    setOptions : function(cmp){
        lstOption = cmp.get('v.recordLimitCombos');
        console.log('lstOption: ',lstOption);
        for (var i = 0; i < lstOption.length; i++) {
            opts.push(lstOption[i].value);
            startSelect+=lstOption[i].value+';';
        }
    }
})