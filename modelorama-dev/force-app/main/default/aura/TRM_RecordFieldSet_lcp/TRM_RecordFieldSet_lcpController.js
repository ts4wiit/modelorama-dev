({
	doInit : function(component, event, helper) {
		helper.initAttributes(component);

		//If the attribute 'fieldSetName' has a value, then get the field set API name
		if( component.get('v.fieldSetName') ){
			helper.getFieldsetNameByField(component);
		}

		//If the attribute 'fieldSetApiName' has a value, then get the field definition of the field set
		if(component.get('v.fieldSetApiName')){
			helper.getFields(component, event, helper);
		}
		
		var autoSetRecordTypeId = component.get('v.autoSetRecordTypeId');
		if(autoSetRecordTypeId){
			var objectApiName = component.get('v.objectApiName');
			var recordTypeDevName = component.get('v.recordTypeDevName');
			helper.setRecordTypeIdByDeveloperName(component, objectApiName, recordTypeDevName, 'recordTypeId');
		}
	},

	/** call the function to get the field definitions of the FieldSet **/
	handleChangeFieldSetName : function(component, event, helper){
		helper.getFields(component, event, helper);
	},

	/**  function to check if the save can be done, 
	if true and the are not blank requiered fields, 
	then the save of the record is done **/
	doSave : function(component, event, helper){
		var doSave = component.get('v.doSave');
		component.set('v.errorOnSave',  false);
		if(doSave == true){
			
			var requiredFields = helper.validateMandatoryFields(component);
			if(!requiredFields){
				var recordEditForm = component.find('recordEditForm');
				component.find('recordEditForm').submit();
				console.log('New record saved');
			} else{
				component.set('v.doSave', false);
			}
		}
	},

	/**  function to validate the current step where the user is. 
	If current step is different of 1 and there are updates, 
	then the record is updated **/
	handleChangeCurrentStep : function(component, event, helper){
		var currentStep = component.get('v.currentStep');
		var updateAfterSave = component.get('v.updateAfterSave');

		if( currentStep != '1' && updateAfterSave == true ){
			helper.doUpdate(component);
		}
	},

	/** function to get the update when a field value is changed.
	The updated field values are saved into a Map attribute.
	If filter fields for customer are updated, 
	an event is called to notify parent components.**/
	handleOnChangeField: function(component,event,helper){
		var fieldName = event.getSource().get('v.fieldName');
		var fieldValue = event.getSource().get('v.value');
		if(fieldName == 'TRM_PriceZoneOnly__c'){
			helper.filtersUpdated(component);
			helper.getPriceZoneCataloge(component, fieldValue);
		}
		if(fieldName == 'TRM_SalesOfficeOnly__c'){
			helper.filtersUpdated(component);
			helper.getSalesOffId(component, fieldValue);
		}

		if(fieldName == 'TRM_Segment__c'){
			helper.filtersUpdated(component);
		}

		if(fieldName == 'TRM_DLCGpo__c'){
			helper.filtersUpdated(component);
		}

		if(fieldName == 'TRM_Quota__c'){
			//console.log('QUOTA: ' + fieldValue)
		}
		//console.log('fieldName:' + fieldName + ' - ' + 'fieldValue:' + fieldValue);

		var recordFieldValueMap = component.get('v.recordFieldValueMap');
		recordFieldValueMap.set(fieldName, fieldValue);
		component.set('v.recordFieldValueMap', recordFieldValueMap);
		//console.log('-----------------------------');
		//for ( var [clave, valor] of recordFieldValueMap ) {
		//  console.log(clave + " = " + valor);
		//}
		helper.updateRecordFieldValueMap(component, recordFieldValueMap);

		var newRecordId = component.get('v.newRecordId');
		if(newRecordId.length > 0){
			component.set('v.updateAfterSave', true);
			//helper.doUpdate(component);
		}
	},

	/** function to handle the success of the record save **/
	handleSuccess : function(component, event, helper) {
        var payload = event.getParams().response;
		component.set('v.newRecordId', payload.id);
		component.set('v.updateAfterSave', false); //there are not updates on fields after last save
		console.log('Created Record Id:' + payload.id);
		helper.setNewRecordId(component, payload.id);
    },

    /** function to handle the error of the record save.
    An boolean attribute is set to true when an error occurs**/
    handleError : function(component, event, helper){
    	component.set('v.doSave', false);
    	component.set('v.errorOnSave',  true);
    	var errors = event.getParams();
        //console.log("Error on save:", JSON.stringify(errors));
        var toastCmp = component.find("toastCmp");
        toastCmp.showQuickToast($A.get("$Label.c.TRM_TitleError"), $A.get("$Label.c.TRM_ErrorOnSave"), errors);
    },

    /** call a function to update the Condition Price Zone Multi Select field **/
    changePriceZoneExtId : function(component, event, helper){
    	helper.updateConditionPriceZone(component);
    },

    /** call a function to update the Sales Office Multi Select field **/
    changeSalesOffId : function(component, event, helper){
    	helper.updateConditionSalesOffice(component);
    }
})