({
    /** Init the required attributes **/
    initAttributes : function(component){
        var recordFieldValueMap = new Map();
        component.set('v.recordFieldValueMap', recordFieldValueMap);
    },

    /** gets the Record Type Id
    Receives input attributes with the object api name, 
    record type developer name and the attribute name to save the result value. **/
    setRecordTypeIdByDeveloperName :  function(component, objectType, developerName, attributeName){
        var action = component.get('c.getRecordTypeIdByDeveloperName');
        var attribute = 'v.' + attributeName;
        action.setParams({
            'objectType':       objectType,
            'developerName':    developerName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var recordTypeId = response.getReturnValue();
                component.set( attribute, String(recordTypeId) );
                var recordEditForm = component.find("recordEditForm");
                recordEditForm.set('v.recordTypeId', recordTypeId);
                //console.log("SUCCESS IN setRecordTypeIdByDeveloperName: " + recordTypeId);
            }else{
                console.log("ERROR IN setRecordTypeIdByDeveloperName:",response.error);
            }
        });
        $A.enqueueAction(action);
    },

    /** gets the Field Set API name, from the value of a formula field with the external key **/
    getFieldsetNameByField : function(component) {
        var action = component.get("c.getFieldsetNameByField");
        var objectType = component.get('v.objectApiName');
        var id = component.get('v.recordId');
        var field = component.get('v.fieldSetName');
        action.setParams({objectType: objectType, id: id, field: field});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                //console.log('Success in getFieldsetNameByField() - returnValue:' + a.getReturnValue());
                component.set('v.fieldSetApiName', a.getReturnValue());
            } else{
                console.log('Error in getFieldsetNameByField(): ' + a.getError() );
            }
        });
        $A.enqueueAction(action); 
    },

    /** gets the Field Definitions of the field set defined in the attribute 'fieldSetApiName' **/
    getFields : function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgLookingFor"));
        var setPredefinedValues = component.get('v.setPredefinedValues');
        var action = component.get("c.getFields");
        var objectApiName = component.get("v.objectApiName");
        var fieldSetApiName = component.get("v.fieldSetApiName");
        action.setParams({objectApiName: objectApiName, fieldSetApiName: fieldSetApiName});
        action.setCallback(this, function(a) {
            var fields = a.getReturnValue();
            if(fields != null){
                var recordFieldValueMap = component.get('v.recordFieldValueMap');
                for( var f = 0; f < fields.length; f++ ){
                    fields[f].hidden = this.isHidden(component, fields[f].fieldPath);
                    fields[f].readOnly = this.isReadOnly(component, fields[f].fieldPath);
                    recordFieldValueMap.set(fields[f].fieldPath, '');
                }
                fields = setPredefinedValues ? helper.setConditionClassValues(component, fields) : fields;
                component.set('v.recordFieldValueMap', recordFieldValueMap);
                component.set("v.fields", fields);
                //console.log('Returned fields: ' + fieldsList.length);
                if(fields.length > 0){
                    component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgLoading"));
                } else{
                    component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgNoFieldsFound"));
                }
            } else{
                console.log('No fields found to return');
                component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgNoFieldsFound"));
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action); 
    },

    /** set the pre selected values for fields TRM_ConditionClass__c and TRM_AccessSequence__c **/
    setConditionClassValues : function(component, fields){
        var conditionClass = component.get('v.conditionClass');
        var accessSequence = component.get('v.accessSequence');
        var recordFieldValueMap = component.get('v.recordFieldValueMap');
        for( var f = 0; f < fields.length; f++ ){
            if(fields[f].fieldPath == 'TRM_ConditionClass__c'){
                fields[f].predefinedValue = true;
                fields[f].value = conditionClass;
                recordFieldValueMap.set(fields[f].fieldPath, fields[f].value);
            }
            if(fields[f].fieldPath == 'TRM_AccessSequence__c'){
                fields[f].predefinedValue = true;
                fields[f].value = accessSequence;
                recordFieldValueMap.set(fields[f].fieldPath, fields[f].value);
            }
        }
        component.set('v.recordFieldValueMap', recordFieldValueMap);
        return fields;
    },

    /** validates if the field is defined in the list of fields to be hidden **/
    isHidden : function(component, fieldApiName){
        var hiddenFieldApiNames = component.get('v.hiddenFieldApiNames').replace(/ /g,'');

        var hiddenFieldApiNameArray = hiddenFieldApiNames.split(',');

        var isHidden = hiddenFieldApiNameArray.includes(fieldApiName);
        //console.log(fieldApiName + ' isHidden:'+isHidden);

        return isHidden;
    },

    /** validates if the field is defined in the list of fields to be read only **/
    isReadOnly : function(component, fieldApiName){
        var readOnlyFieldApiNames = component.get('v.readOnlyFieldApiNames').replace(/ /g,'');

        var readOnlyFieldApiNamesArray = readOnlyFieldApiNames.split(',');

        var isReadOnly = readOnlyFieldApiNamesArray.includes(fieldApiName);
        //console.log(fieldApiName + ' isHidden:'+isHidden);

        return isReadOnly;
    },

    /** validates if there are blank mandatory fields **/
    validateMandatoryFields : function(component){
        var requiredFields = false;
        var missingFieldList = '';
        var fields = component.get('v.fields');
        var fieldComponentList = component.find("field");
        for( var f = 0; f < fields.length; f++ ){
            for( var c = 0; c < fieldComponentList.length; c++  ){
                if( fields[f].fieldPath ==  fieldComponentList[c].get("v.fieldName") && 
                    ( fields[f].required || fields[f].DBRequired ) &&
                    (fieldComponentList[c].get("v.value") == '' || fieldComponentList[c].get("v.value") == null ) ){
                    //console.log( 'requerido:' + fieldComponentList[c].get("v.fieldName") );
                    //console.log( 'requerido:' + fields[f].label );
                    missingFieldList += missingFieldList.length > 0 ? ', ' + fields[f].label : fields[f].label;  
                    requiredFields = true;
                }
            }
            
        }
        if(requiredFields){
            this.showToast('error', $A.get("$Label.c.TRM_TitleReqFields"), $A.get("$Label.c.TRM_MsgReqFields") + missingFieldList);
            //console.log('required missing fields:' + missingFieldList);
            //console.log('requiredFields:' + requiredFields);
        }
        
        return requiredFields;
    },

    /** fires an event to notify to the parent component when the map of field-value is updated **/
    updateRecordFieldValueMap : function(component, recordFieldValueMap){
        var updateEvent = component.getEvent("updateRecordFields");
            updateEvent.setParams({ 
                "recordFieldValueMap": recordFieldValueMap
            });
            updateEvent.fire();
    },

    /** show a Toast message **/
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    /** set the recordId to the RecordEditForm Component **/
    setNewRecordId : function(component, id){
        var recordEditForm = component.find("recordEditForm");
        recordEditForm.set('v.recordId', id);
    },

    /**  if there are not blank requiered fields, 
    then the save of the record is done **/
    doUpdate : function(component){
        var requiredFields = this.validateMandatoryFields(component);
        if(!requiredFields){
            component.find('recordEditForm').submit();
            console.log('Record updated');
            //fire event to notify that the record has been updated
            this.updatedRecord(component);
        } 
    },

    /** fires an event to notify to the parent component when the record has been updated **/
    updatedRecord : function(component){
        var updateEvent = component.getEvent("updatedRecord");
        updateEvent.setParams({
            "updated": true
        });
        updateEvent.fire();
    },

    /** fires an event to notify to the parent component when the filters fields have been updated **/
    filtersUpdated : function(component){
        var updateEvent = component.getEvent("updatedFilters");
        updateEvent.setParams({
            "updated": true
        });
        updateEvent.fire();
    },

    /** updates the value of the field 'TRM_StatePerZone__c' with the value of the attribute 'priceZoneExtId' **/
    updateConditionPriceZone : function(component){
        var fieldComponentList = component.find("field");
        var priceZoneExtId = component.get('v.priceZoneExtId');
        for( var c = 0; c < fieldComponentList.length; c++  ){
            var fieldName = fieldComponentList[c].get("v.fieldName");
            var value = fieldComponentList[c].get("v.value");
            //console.log(fieldName + ':' + value);
            if(fieldName == 'TRM_StatePerZone__c'){
                fieldComponentList[c].set("v.value", priceZoneExtId);
            }
        }
    },

    /** updates the value of the field 'TRM_SalesOffice__c' with the value of the attribute 'salesOffId' **/
    updateConditionSalesOffice : function(component){
        var fieldComponentList = component.find("field");
        var salesOffId = component.get('v.salesOffId');
        for( var c = 0; c < fieldComponentList.length; c++  ){
            var fieldName = fieldComponentList[c].get("v.fieldName");
            var value = fieldComponentList[c].get("v.value");
            if(fieldName == 'TRM_SalesOffice__c'){
                fieldComponentList[c].set("v.value", salesOffId);
            }
        }
    },

    /** gets from MDM Parameter the code of the Price Zone Catalogue **/
    getPriceZoneCataloge : function(component, fieldValue){
        var action = component.get("c.getRecordById");
        var objectType = $A.get("$Label.c.TRM_MDMParameterAPI");
        
        action.setParams({objectType: objectType, id: fieldValue});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                //console.log('priceZoneExtId:' + returnValue.ExternalId__c);
                component.set('v.priceZoneExtId', returnValue.ExternalId__c);
            } else{
                console.log('No priceZoneExtId retrieved');
            }
        });
        $A.enqueueAction(action); 
    },

    /** gets the External Key from Account of the Sales Office **/
    getSalesOffId : function(component, fieldValue){
        var action = component.get("c.getRecordById");
        var objectType = $A.get("$Label.c.TRM_AccountAPI");
        
        action.setParams({objectType: objectType, id: fieldValue});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                //console.log('salesOffId:' + returnValue.ONTAP__ExternalKey__c);
                component.set('v.salesOffId', returnValue.ONTAP__ExternalKey__c);
            } else{
                console.log('No salesOffId retrieved');
            }
        });
        $A.enqueueAction(action); 
    }

})