({
    //se ejecuta al seleccionar un registro devuelto por la busqueda del componente principal
    selectRecord : function(component, event, helper){      
        var getSelectRecord = component.get("v.oRecord");//Obtiene el registro seleccionado 
        var compEvent = component.getEvent("oSelectedRecordEvent");//Invoca el evento a registrar
        compEvent.setParams({"recordByEvent" : getSelectRecord });//guarda el objeto seleccionado en el atributo del evento
        compEvent.fire();
    },
})