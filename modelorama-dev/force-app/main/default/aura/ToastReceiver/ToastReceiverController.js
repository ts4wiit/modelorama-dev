({
    doInit : function(component) {
        if(component.get("v.autoHide")){
            setTimeout(function(){component.destroy()}, component.get("v.duration"));
        }
    },
    handleClick: function(component){
        component.destroy();
    }

})