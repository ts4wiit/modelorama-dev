({
    //SEARCH
    search : function(component, event, helper) 
    {
      //Event to Display Cockpit Resultss
      var eventToFire = $A.get('e.c:AMCockpitEvt');
      eventToFire.setParams({
        visualLevel: component.find('idVisual').get('v.value'),
        idLevelTxt: component.get('v.searchText'),
        tourName: component.get('v.tour'),
        tourStatus: component.find('idEstatus').get('v.value'),
        executionDate: component.find('fechaEje').get('v.value'),
        showList: true,
        showResultDetails: false, 
        showCSVDetails: false});
      eventToFire.fire();           
    },

    //GET TOUR ESTATUS
    getEstatusCtrl : function(component, event, helper) 
    {
        var action = component.get('c.getEstatus');
        
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               if(state === 'SUCCESS')
                               {
                                   component.set('v.estatusList',response.getReturnValue());
                               }
                           });
        $A.enqueueAction(action);
    },
})