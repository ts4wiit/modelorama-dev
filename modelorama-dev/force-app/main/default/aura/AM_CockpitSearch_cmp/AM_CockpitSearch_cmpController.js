({
    //LOADING COMPONENT
    init : function(component, event, helper) {        
        helper.getEstatusCtrl(component, event, helper);
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) 
        {
            monthDigit = '0' + monthDigit;
        }
        component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
    },

    //SEARCH
    searchObjData : function(component, event, helper) {
        //Event Display Results
        var eventToFire = $A.get('e.c:AMCockpitEvt');
        eventToFire.setParams({
        visualLevel: '',
        idLevelTxt: '',
        tourName: '',
        tourStatus: '',
        executionDate: '',
        showResultDetails: false, 
        showTreeList: false, 
        showCSVDetails: false});
        eventToFire.fire();

        helper.search(component,event);
    },

    //CLEAR FIELDS
    clearFields : function(component, event, helper) {
        component.set('v.hasChange',false);
        component.set('v.isRoute',false);
        component.set('v.isOffice',false);
        component.set('v.isUEN',false);
        component.set('v.showList',false);
        component.set('v.showResults',false);
        component.set('v.hasChange',false);
        component.find("idVisual").set("v.value","");
        component.find("searchTxt").set("v.value","");
        component.find("searchTxt").set("v.placeholder","");
        component.find("tourIdTxt").set("v.value","");
        component.find("tourIdTxt").set("v.placeholder","");
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) 
        {
            monthDigit = '0' + monthDigit;
        }
        component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());

        //Event Display Results
        var eventToFire = $A.get('e.c:AMCockpitEvt');
        eventToFire.setParams({
        visualLevel: '',
        idLevelTxt: '',
        tourName: '',
        tourStatus: '',
        executionDate: '',
        showResultDetails: false, 
        showTreeList: false, 
        showCSVDetails: false});
        eventToFire.fire();
    },

    //CLEAR OTHER INPUT TEXT FIELDS WHEN TOUR ID CHANGED
    clearInputsTourId : function(component, event, helper) {
        component.find("idVisual").set("v.value","");
        component.find("searchTxt").set("v.value","");
        component.find("searchTxt").set("v.placeholder","");
        component.find("tourIdTxt").set("v.value","");
        component.find("tourIdTxt").set("v.placeholder","");
        var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) 
        {
            monthDigit = '0' + monthDigit;
        }
        component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
    },

    //HIDE FIELDS
    hideFields : function(component, event, helper) {
        var value = component.find("idVisual").get('v.value');
        //Event Display Results
        var eventToFire = $A.get('e.c:AMCockpitEvt');
        eventToFire.setParams({
        visualLevel: '',
        idLevelTxt: '',
        tourName: '',
        tourStatus: '',
        executionDate: '',
        showResultDetails: false, 
        showTreeList: false, 
        showCSVDetails: false});
        eventToFire.fire();

        //DRV            
        if(value == component.get('v.DRV'))
        {
            component.set('v.isDRV',true);
            component.set('v.isRoute',false);
            component.set('v.isOffice',false);
            component.set('v.isUEN',false);
            component.set('v.showList',false);
            component.set('v.hasChange',true);
            component.set('v.showResults',false);
        }
        //UEN
        else if (value == component.get('v.UEN'))
        {
            component.set('v.isDRV',false);
            component.set('v.isRoute',false);
            component.set('v.isOffice',false);
            component.set('v.isUEN',true);
            component.set('v.showList',false);
            component.set('v.hasChange',true);
            component.set('v.showResults',false);
        }
        //Oficina            
        else if(value == component.get('v.Office'))
        {
            component.set('v.isDRV',false);
            component.set('v.isRoute',false);
            component.set('v.isOffice',true);
            component.set('v.isUEN',false);
            component.set('v.showList',false);
            component.set('v.hasChange',true);
            component.set('v.showResults',false);
        }
        //Route
        else if (value == component.get('v.Route'))
        {
            component.set('v.isDRV',false);
            component.set('v.isRoute',true);
            component.set('v.isOffice',false);
            component.set('v.isUEN',false);
            component.set('v.showList',false);
            component.set('v.hasChange',true);
            component.set('v.showResults',false);
        }
        //Select an option
        else if((value != component.get('v.DRV')) && (value != component.get('v.UEN')) && (value != component.get('v.Office')) && (value != component.get('v.Route')))
        {
            component.set('v.isDRV',false);
            component.set('v.isRoute',false);
            component.set('v.isOffice',false);
            component.set('v.isUEN',false);
            component.set('v.showList',false);
            component.set('v.hasChange',false);
            component.set('v.showResults',false);
        }
        component.find("searchTxt").set("v.value","");
        component.find("searchTxt").set("v.placeholder","");
        component.find("tourIdTxt").set("v.value","");
        component.find("tourIdTxt").set("v.placeholder","");
    }
})