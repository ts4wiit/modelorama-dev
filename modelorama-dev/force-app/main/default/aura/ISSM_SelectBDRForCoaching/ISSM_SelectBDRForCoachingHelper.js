({
    getCoachingEvents: function(cmp, executionDate, mId){
        cmp.set('v.showSpinner', true);
        cmp.set('v.doGetAccounts', false);
        var selectedAccIdList = [];
        var selectedRows = cmp.get('v.selectedRows');
        var selectedIds = [];
        for(var i = 0; i < selectedRows.length; i++){
            selectedIds.push(selectedRows[i].Id);
        }
        var action = cmp.get("c.getAccountCoachingEvents");
        action.setParams({ "dDate": executionDate, "managerId": mId, "selectedAccIdList": selectedIds});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue().length > 0 ){
                    var result = response.getReturnValue();
                    console.log('Coaching Events retrieved: ' + result.length);
                    var accountNameList = '';
                    for(var i = 0; i < result.length; i++){
                        accountNameList += i == 0 && result.length > 1 ? result[i].Name + ', ' : result[i].Name;
                    }
                    this.showToast(cmp, 'info', $A.get("$Label.c.ISSM_MsgEvtCoachings") + accountNameList, 5000, true);
                } else{
                    //console.log('No Coaching Events retrieved: ');
                    cmp.set('v.doGetAccounts', true);
                }
            }
            cmp.set('v.showSpinner', false);
        });
     $A.enqueueAction(action);
    },

	getAccounts: function(cmp, executionDate, uId){
        cmp.set('v.showSpinner', true);
        cmp.set('v.hideCheckboxAcc', false);
        cmp.set('v.accAreSelectable', false);
        console.log('getAccounts');
        var action = cmp.get("c.getToursVisitsFromUser");
        action.setParams({ "tourDate": executionDate, "userId": uId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue().length > 0){
                    cmp.set("v.accounts", response.getReturnValue());
                    if(response.getReturnValue().length <= $A.get("$Label.c.ISSM_MinSelectCoachAcc")){
                        cmp.set('v.hideCheckboxAcc', true);
                        
                    } else {
                        cmp.set('v.hideCheckboxAcc', false);
                        cmp.set('v.accAreSelectable', true);
                        var selectedRows = [];
                        for(var t = 0; t < response.getReturnValue().length; t++){
                            selectedRows.push(response.getReturnValue()[t].Id);
                        }
                        var accountDataTable = cmp.find("accountDataTable");
                        accountDataTable.set('v.selectedRows', selectedRows);
                    }
                    cmp.set('v.selectedRows', response.getReturnValue());
                    cmp.set('v.selectedRowsCount', response.getReturnValue().length);
                }
                this.toggleComponent(cmp,"tableSpinner");
            }
            cmp.set('v.showSpinner', false);
        });
	 $A.enqueueAction(action);
    },
    getCoaching: function(cmp, executionDate,endDate, uId, range){
        cmp.set('v.showSpinner', true);
        //this.toggleComponent(cmp,"coachingDataTable");
        this.toggleComponent(cmp,"tableSpinner");
        var action = cmp.get("c.getCoaching");
        action.setParams({"range":range, "startDate": executionDate,"endDate":endDate, "userId": uId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.dataCoach", response.getReturnValue());
                //this.toggleComponent(cmp,"coachingDataTable");
                this.toggleComponent(cmp,"tableSpinner");
                console.log("success");
            }else{
                console.log("error");
            }
            cmp.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    clearAll: function(cmp){
        cmp.set("v.bdrId", null);
        cmp.set("v.executionDate", null);
        cmp.set("v.accounts", null);
    },
    saveCoaching: function(cmp, selectedAccounts, assignTo, executionDate){
        cmp.set('v.showSpinner', true);
        var action = cmp.get("c.saveCoaching");
        action.setParams({ 
            "accountsJSON": selectedAccounts,
            "assignTo": assignTo,
            "executionDate": executionDate
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state=="SUCCESS"){
                this.clearAll(cmp);
                this.toggleComponent(cmp,"tableSpinner");
                this.showToast(cmp, 'success', 'Las visitas para el supervisor han sido creadas.', 5000, true)
            }
            cmp.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);
    },
    toggleComponent: function(cmp, componentName){
        console.log("toggle component - "+componentName);
        var mycomponent = cmp.find(componentName);
        $A.util.toggleClass(mycomponent, "slds-hide");
    },
    showToast: function(component, severity, message, duration, autoHide){
        console.log("show toast");
        $A.createComponent(
            "c:ToastReceiver",
            {
                "severity": severity,
                "messageBody": message,
                "autoHide": autoHide,
                "duration": duration
            },
            function(msgBox, status, errorMessage){  
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(msgBox);
                    component.set("v.body", body);
                }
            }
        );
    }
})