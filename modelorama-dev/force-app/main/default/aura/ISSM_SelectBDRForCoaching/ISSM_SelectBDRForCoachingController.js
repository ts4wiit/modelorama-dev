({
	init : function(component, event, helper) {
        var getToday = new Date();
        var month = getToday.getMonth()+1;
        var monthVal = month <= 9 ? "0"+month : ""+month;
        component.set("v.today",getToday.getFullYear()+'-'+monthVal+'-'+getToday.getDate());
		var columns = [
            {label: 'Cliente', fieldName: 'Id', type: 'url', typeAttributes:{label:{fieldName:'Name'}}},
            {label: 'Número SAP', fieldName: 'ONTAP__SAPCustomerId__c', type: 'text', sortable: true},
            {label: 'Oficina de Ventas', fieldName: 'ONTAP__SalesOffId__c ', type: 'text', sortable: true}
        ];
        component.set('v.columns', columns);
        var columnsCoach = [
            {label: 'Fecha de Coaching', fieldName: 'coachingDate', type: 'date'},
            {label: 'Desarrollador', fieldName: 'bdr', type: 'text'},
            {label: 'Ruta', fieldName: 'route', type: 'text'},
            {label: 'No. de visitas', fieldName: 'numberOfVisits', type: 'number'},
            {label: 'Aplicada', fieldName: 'coachingApplied', type: 'text'}
        ];
        component.set('v.columnsCoaching', columnsCoach);
        component.set("v.selRange", "today");
        
	},
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRows', selectedRows);
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },

    save : function(component, event, helper){
        var accAreSelectable = component.get('v.accAreSelectable');
        var selectedRows = component.get('v.selectedRows');
        var continueDoSave = false;
        
        if(accAreSelectable){
            if(selectedRows.length < $A.get("$Label.c.ISSM_MinSelectCoachAcc")){
                continueDoSave = false;
                helper.showToast(component, 'error', $A.get("$Label.c.ISSM_MsgMinSelectAcc"), 5000, true)
            } else{
                continueDoSave = true;
            }
        } else {
            continueDoSave = true;
        }
        
        if(continueDoSave){
            var executionDate = component.get("v.executionDate"); 
        	var uId = component.get("v.bdrId");
            var mId = component.get("v.managerId");
        	helper.getCoachingEvents(component, executionDate, mId);
        }  
        
    },

    saveCoachingData: function(component, event, helper){
        var doGetAccounts = component.get('v.doGetAccounts');
        if(doGetAccounts){
            var selectedRows = JSON.stringify(component.get('v.selectedRows'));
            var assignTo = component.get("v.managerId");
            var executionDate = component.get("v.executionDate");

            console.log('selectedRows:' + selectedRows);
            
            helper.toggleComponent(component,"tableSpinner");
            helper.saveCoaching(component,selectedRows,assignTo, executionDate);
        }
    },
    onSDateChange:function (component, event, helper) {
        component.set("v.eDate", null);
    },
    onEDateChange: function(component, event, helper){
        console.log("onEdatechange");
        var uId = component.get("v.managerId");
        var executionDate = component.get("v.sDate");
        var endDate = component.get("v.eDate");
        var range = "CUSTOM";
        helper.getCoaching(component, executionDate,endDate, uId, range);
    },

    getAccounts : function(component, event, helper){
        
            var executionDate = component.get("v.executionDate");
            var uId = component.get("v.bdrId");
            if(executionDate != null){
                helper.getAccounts(component, executionDate, uId);
            } else{
                var accounts = [];
                component.set('v.accounts', accounts);
            }
        
    },

    applyCoachingDate: function(component,event,helper) {
        console.log('applyCoachingDate');
        var accounts = [];
        component.set('v.accounts', accounts);
        var executionDate = component.get("v.executionDate"); 
        var uId = component.get("v.bdrId");
        var mId = component.get("v.managerId");
        //helper.toggleComponent(component,"accountDataTable");
        helper.toggleComponent(component,"tableSpinner");
        //console.log('toggle applied');
        if(executionDate != null){
            helper.getAccounts(component, executionDate, uId);
        } /*else{
            var accounts = [];
            component.set('v.accounts', accounts);
        }*/
        
    },
    clearData: function(component,event,helper){
        helper.clearAll(component);
    },
    rangeSelected: function(component, event,helper){
        var selectedRange = component.get("v.selRange");
        if(selectedRange!='custom'){
            component.set("v.sDate",null);
        	component.set("v.eDate",null);
            var executionDate = new Date();
            var endDate  = new Date();
            var uId = component.get("v.managerId");
            var range;
            switch(selectedRange){
                case 'today':
                    range = "TODAY";
                    break;
                case 'thisWeek':
                    range = "THIS_WEEK";
                    executionDate = new Date(executionDate.setDate(executionDate.getDate() - executionDate.getDay() +1));
                    endDate = new Date(endDate.setDate((executionDate.getDate() - executionDate.getDay() +1) + 6)); 
                    break;
                case 'thisMonth':
                    range = "THIS_MONTH";
                    var mydate = new Date(), y = mydate.getFullYear(), m = mydate.getMonth();
                    executionDate = new Date(y, m, 1);
                    endDate = new Date(y, m + 1, 0);
                    break;
            }
            helper.getCoaching(component, executionDate,endDate, uId, range);
        }
    }
})