({
    //CLOSE ALERT
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
        var compEvent = component.getEvent("confirmExecution");
        compEvent.setParams({"isExecute" : false});
        compEvent.fire();
    },
 
    //CONFIRM ALERT
    confirmAction: function(component, event, helper) {
        component.set("v.isOpen", false);
        var compEvent = component.getEvent("confirmExecution");
        compEvent.setParams({"isExecute" : true });
        compEvent.fire();
    },
})