({ //carga de datos al picklist
    doInit : function(component) {
        // Load options for Type of Products
        var action  = component.get("c.getOptions");
        action.setParams({
            "objObject": component.get('v.objConditionClass'),
            "strFld":    'TRM_ConditionClass__c'
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = JSON.parse(response.getReturnValue()); 
                var obj = {};
                obj["value"] = "Combos";
                obj["label"] = "Combos";
                allValues.push(obj);
                component.set("v.options", allValues);
            }else{
                console.log('ERROR en fetchPickListVal ',response.error);
            }
        });
        $A.enqueueAction(action);
    },               
    // Al cerrar el modal padre, te redirecciona al tab de COMBOS
    closeModel: function(component, event, helper) {         
        component.set("v.isOpenAprovals", false);
        /*var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "home"
        });
        homeEvent.fire();*/
    },
    // Si la condicion seleccionada es COMBOS muestra 2 opciones, de no ser así 
    // muestra las opciones de condiciones comerciales
    changeTypeApproval: function(cmp, evt, helper){
        var typeApproval = cmp.find("typeApproval").get("v.value");
        cmp.set('v.typeCondition',typeApproval);  
        var typeApproval = cmp.find("typeApproval");
        if(typeApproval.get("v.value") === "Combos"){            
            var fclose = cmp.find("bottonsConditions");
        	helper.addRemoveClass(fclose,'slds-hide','slds-show');            
            var fclose = cmp.find("bottonsCombo");
        	helper.addRemoveClass(fclose,'slds-show','slds-hide');            
        }else{
            var fclose = cmp.find("bottonsCombo");
        	helper.addRemoveClass(fclose,'slds-hide','slds-show');
            var fclose = cmp.find("bottonsConditions");
        	helper.addRemoveClass(fclose,'slds-show','slds-hide');
        }
    },
    // contro,del componente a mostrar
    clickOperationMassive: function (cmp, event) {
        var operation = event.getSource().get("v.name");
        cmp.set('v.operation',operation);
        if(operation === $A.get("$Label.c.TRM_CreateCC") ||
           operation === $A.get("$Label.c.TRM_CancelCC") || 
           operation === $A.get("$Label.c.TRM_RetryCC")){ 
            
            cmp.set('v.isOpenAprovals',false);
            cmp.set('v.isOpenCancel',false);
            cmp.set('v.isOpenSendSAP',false);
            cmp.set('v.isOpenConditionClass',true);
        }
        if(operation === "cancel"){            
            cmp.set('v.isOpenAprovals',false);
            cmp.set('v.isOpenCancel',true);
            cmp.set('v.isOpenSendSAP',false);
            cmp.set('v.isOpenConditionClass',false);
        }
        if(operation === "sendSap"){
           cmp.set('v.isOpenAprovals',false);
            cmp.set('v.isOpenCancel',false);
            cmp.set('v.isOpenSendSAP',true);
            cmp.set('v.isOpenConditionClass',false);
        }
    }
})