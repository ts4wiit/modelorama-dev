({
	addRemoveClass : function(element,addcls,removecls) {
        $A.util.addClass(element, addcls);
        $A.util.removeClass(element, removecls);
    },
})