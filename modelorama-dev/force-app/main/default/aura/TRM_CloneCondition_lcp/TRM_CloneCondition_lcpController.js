({
	//CLOSE ALERT
    closeModal: function(component, event, helper) {
    	helper.redirectPage(component.get('v.recordId'));
        helper.showToast('success', $A.get("$Label.c.TRM_Success"), $A.get("$Label.c.TRM_Msg27"));
    },
 
    //CONFIRM ALERT
    confirmAction: function(component, event, helper) {
    	helper.getRecord(component);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
})