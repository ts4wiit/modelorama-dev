({
	//Get record of clone
    getRecord: function(component){
        var action = component.get("c.getConditions");
        action.setParams({"IdCondClass"     : component.get('v.recordId')});
        
        action.setCallback(this, function(resp){
            var state = resp.getState();
            if (state === "SUCCESS") {
                var IdCondClass = resp.getReturnValue();
                this.showToast('success', $A.get("$Label.c.TRM_Success"), $A.get("$Label.c.TRM_Msg26"));
                this.redirectPage(IdCondClass);
            }else{
                console.log('##error->getRecord##: ',JSON.stringify(resp.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    //Show Alert
    showToast: function (type, title, msg){
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "duration": 5000,
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    //Redirect to Page indicated for the RecordId
    redirectPage: function (recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})