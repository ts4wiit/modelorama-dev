({
        /** init the attributes of type maps **/
        initAttributes : function(component, event, helper){
                var selectedMap = new Map();
                var preselectedMap = new Map();
                component.set('v.selectedMap', selectedMap);
                component.set('v.preselectedMap', preselectedMap);
        },

        /** call Apex method to get the Field Definition of the field set **/
        getDataTableColumnsByFieldset: function(component){
                var action = component.get('c.getDataTableColumnsByFieldset');
                action.setParams({
                        sObjectApiName: component.get('v.objectType'),
                        fieldSetApiName: $A.get("$Label.c.TRM_AccColumnsFieldset")
                });
                action.setCallback(this, function (response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                                var returnValue = response.getReturnValue();
                                component.set('v.columns', returnValue);
                                //console.log('Success on getDataTableColumnsByFieldset() ' + returnValue.length + ' columns');
                        } else{
                            var toastCmp = component.find("toastCmp");
                            toastCmp.showQuickToast($A.get("$Label.c.TRM_TitleError"), $A.get("$Label.c.TRM_NoTableColumns"), response.getError());
                            //console.log('Error on getDataTableColumnsByFieldset():' + JSON.stringify(response.getError()) );
                        }
                });
                $A.enqueueAction(action);
        },

        /** call Apex method to get the data of accounts **/
        getAccounts2 : function(component, numberOfRowsToReturn) {
                console.log('START getAccounts()');
                var isEditMode = component.get('v.isEditMode');
                var customerIdList = component.get('v.customerIdList');
                var selectedMap = component.get('v.selectedMap');                
                var preselectedMap = component.get('v.preselectedMap');

                component.set('v.showSpinner', true);
                component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgLookingFor"));
                component.set('v.selectAll', false);
                
                var selectedMapKeys = Array.from(component.get('v.selectedMap').keys());
                selectedMapKeys.forEach(function(element, index){
                        selectedMapKeys[index] = '\'' + element + '\'';
                });

                var searchByList = component.get('v.searchByList');

                var action = component.get("c.getAccounts");
                action.setParams({
                        searchByList : searchByList,
                        searchKeyWord: component.get('v.searchKeyWord'),
                        fieldList: component.get('v.fieldList'), 
                        objectType: component.get('v.objectType'),
                        recordType: component.get('v.recordType'),
                        fieldOrderByList: component.get('v.fieldOrderByList'),
                        numberOfRowsToReturn: numberOfRowsToReturn,
                        filterIdList: selectedMapKeys,
                        filterBySegment: component.get('v.filterCustomerBySegment'),
                        segmentList: component.get('v.segmentList'),
                        filterByZone: component.get('v.filterCustomerByPriceZone'),
                        priceZoneCatCod: component.get('v.priceZoneCatCod'),
                        filterBySalesOffice: component.get('v.filterCustomerBySalesOffice'),
                        salesOfficeId: component.get('v.salesOfficeId'),
                        filterBySalesOrg: component.get('v.filterCustomerBySalesOrg'),
                        salesOrgList: component.get('v.salesOrgList'),
                        filterByDiscList: component.get('v.filterCustomerByDiscList'),
                        discListCatalogCode: component.get('v.discListCatalogCode')
                });
                action.setCallback(this, function (response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                                var records = response.getReturnValue();
                                console.log('Success on getAccounts() ' + records.length + ' records');
                                /*if(isEditMode){
                                        console.log('are there preselected?');
                                        var preselectList = records.filter( function(item){
                                            return customerIdList.includes( item.Id );
                                        } );
                                        console.log('preselect:' + preselectList.length);
                                        for(var ps = 0; ps < preselectList.length; ps++){
                                                preselectedMap.set(preselectList[ps].Id, preselectList[ps]);
                                        }
                                        component.set('v.selectedMap', preselectedMap);
                                }*/
                                var searchKeyWord = component.get('v.searchKeyWord');
                                var searchByList = component.get('v.searchByList');
                                var dataTable = [];
                                if(searchKeyWord.length == 0 && searchByList == false){
                                        var selectedMap = component.get('v.selectedMap');
                                        if(selectedMap.size > 0){
                                                var selectedMapValues = Array.from(selectedMap.values());
                                                dataTable.push.apply(dataTable, selectedMapValues);
                                        }
                                } 
                                dataTable.push.apply(dataTable, records);
                                component.set('v.dataTable', dataTable);
                                if(dataTable.length > 0){
                                        component.set('v.loadingMessage', dataTable.length + ' results');
                                } else {
                                        component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgNoResults"));
                                }
                                if(searchByList){
                                        component.set('v.pageSize', $A.get("$Label.c.TRM_PageSize100"));
                                } else{
                                        component.set('v.pageSize', $A.get("$Label.c.TRM_PageSizeDefault"));
                                }
                                
                        } else{
                                //console.log('Error on getAccounts():' + response.getError());
                                var toastCmp = component.find("toastCmp");
                                toastCmp.showQuickToast($A.get("$Label.c.TRM_TitleError"), $A.get("$Label.c.TRM_NoAccounts"), response.getError());
                                component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgNoResults"));
                        }
                        component.set('v.showSpinner', false);
                });
                $A.enqueueAction(action);
        },

        getAccountsByIdList : function(component){
            component.set('v.showSpinner', true);
            component.set('v.loadingMessage', $A.get("$Label.c.TRM_MsgLookingFor"));

            var customerIdList = component.get('v.customerIdList');
            customerIdList.forEach(function(element, index){
                customerIdList[index] = '\'' + element + '\'';
            });

            var action = component.get("c.getAccountsByIdList");
            action.setParams({
                fieldList: component.get('v.fieldList'), 
                objectType: component.get('v.objectType'),
                customerIdList: customerIdList,
                numberOfRowsToReturn: $A.get("$Label.c.TRM_MaxRowsReturned")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var records = response.getReturnValue();
                    console.log('Success on getAccountsByIdList() ' + records.length + ' records');
                    component.set('v.dataTable', records);
                } else{
                    var toastCmp = component.find("toastCmp");
                    toastCmp.showQuickToast($A.get("$Label.c.TRM_TitleError"), $A.get("$Label.c.TRM_NoSelectedAccounts"), response.getError());
                }
                component.set('v.showSpinner', false);
            });
            $A.enqueueAction(action);
        }
})