({
	doInit : function(component, event, helper) {
		
		helper.initAttributes(component);
		helper.getDataTableColumnsByFieldset(component);
		if( component.get('v.isEditMode') ){
			//get the filter values from ConditionClass
			var conditionClass = component.get('v.conditionClass');
			component.set('v.segmentList', conditionClass.TRM_Segment__c);
			//component.set('v.priceZoneList', conditionClass.TRM_StatePerZone__c);
			//component.set('v.priceZoneCatCod', conditionClass.TRM_StatePerZone__c.replace('PriceZone-', ''));
			component.set('v.priceZoneCatCod', conditionClass.TRM_StatePerZone__c ? conditionClass.TRM_StatePerZone__c.replace('PriceZone-', '') : '');
			//component.set('v.salesOfficeList', conditionClass.TRM_SalesOffice__c);
			component.set('v.salesOfficeId', conditionClass.TRM_SalesOfficeOnly__c);
			component.set('v.salesOrgList', conditionClass.TRM_SalesOrg__c);
			component.set('v.TRM_DLCGpo__c', conditionClass.TRM_DLCGpo__c);
		}
	},

	/** function to handle the change of the search word,
	it calls the function to get the data from the server **/
	handleKeyupSearchKeyWord : function(component, event, helper){
		var doSearch = component.get('v.doSearch');
		if(doSearch){
			helper.getAccounts2(component, component.get('v.numberOfRowsToReturn'));
		}
	},

	/** function to handle the change of the search by list mode checkbox,
	it calls the function to get the data from the server **/
	handleChangeSearchByList : function(component, event, helper){
		var searchByList = component.get('v.searchByList');
		var doSearch = component.get('v.doSearch');
		if(!searchByList){
			component.set('v.searchKeyWord', '');
		}
		if(doSearch){
			helper.getAccounts2(component, component.get('v.numberOfRowsToReturn'));
		}
	},

	/** function to handle the change of the doSearch checkbox,
	it calls the function to get the data from the server **/
	onChangeDoSearch : function(component, event, helper){
		var doSearch = component.get('v.doSearch');
		var isEditMode = component.get('v.isEditMode');
		if(doSearch && isEditMode == false){
			helper.getAccounts2(component, component.get('v.numberOfRowsToReturn'));
		}
		if(doSearch && isEditMode){
			helper.getAccountsByIdList(component);
		}
	},

	/** function to handle the change of any fielter of customers,
	it calls the function to get the data from the server **/
	onChangeFilters : function(component, event, helper){
		var doSearch = component.get('v.doSearch');
		if(doSearch){
			helper.getAccounts2(component, component.get('v.numberOfRowsToReturn'));
		}
	},

	/** function to handle the change of the attribute TRM_DLCGpo__c,
	it calls the function to get the catalog from the MDM Parameter and get the code **/
	handleOnChangeTRM_DLCGpo__c : function(component, event, helper){
		var action = component.get("c.getRecordById");
        var objectType = $A.get("$Label.c.TRM_MDMParameterAPI");
        var id = component.get('v.TRM_DLCGpo__c');
        action.setParams({objectType: objectType, id: id});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.discListCatalog', returnValue);
                component.set('v.discListCatalogCode', returnValue.Code__c);
                //console.log('discListCatalogCode:' + returnValue.Code__c);
                //console.log('discListCatalog retrieved:' + JSON.stringify(returnValue));
            } else{
                console.log('No discListCatalog retrieved');
            }
        });
        $A.enqueueAction(action); 
	},

	/** function to handle the change of the attribute priceZone,
	it calls the function to get the catalog from the MDM Parameter and get the code **/
	handleOnChangePriceZone : function(component, event, helper){
		var action = component.get("c.getRecordById");
        var objectType = $A.get("$Label.c.TRM_MDMParameterAPI");
        var id = component.get('v.priceZone');
        action.setParams({objectType: objectType, id: id});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.priceZoneCatalog', returnValue);
                component.set('v.priceZoneCatCod', returnValue.Code__c);
                //console.log('priceZoneCatCod:' + returnValue.Code__c);
                //console.log('discListCatalog retrieved:' + JSON.stringify(returnValue));
            } else{
                console.log('No priceZoneCatalog retrieved');
            }
        });
        $A.enqueueAction(action); 
	},

	/** event action function to get the updated map of selected records **/
	updateCustomerMap : function(component, event, helper){
		var recordMap = event.getParam("recordMap");
		component.set('v.selectedMap', recordMap);
	}
})