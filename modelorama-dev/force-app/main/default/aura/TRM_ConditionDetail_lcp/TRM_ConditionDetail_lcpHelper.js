({  
    //Method for finish process  
    finishProcess :function(component){
        var lstConditionScales      = component.get('v.lstConditionScales');
        var conditionManagement     = component.get('v.conditionManagement');
        var conditionRelationship   = component.get('v.conditionRelationship');
        var conditionClassRecord    = component.get('v.conditionClassRecord');
        var selectedCustomerList    = component.get('v.selectedCustomerList');
        var mapCatalogs             = component.get('v.mapCatalogs');
        var mapStructures           = component.get('v.mapStructures');
        var lstProdCondition        = component.get('v.LstProdCondition');
        var action                  = component.get('c.mainSaveConditions');

        action.setParams({
            'lstProducts':      lstProdCondition,
            'lstConditionScales': lstConditionScales,
            'mapStructures':    mapStructures,
            'mapCatalogs':      mapCatalogs,
            'lstCustomer':      selectedCustomerList,
            'condClssRec':      conditionClassRecord,
            'mtdRelation':      conditionRelationship,
            'mtdMngRecord':     conditionManagement
        });
        action.setCallback(this, function (response){
            var state = response.getState();
            if(state === "SUCCESS"){
                this.showToast("success",$A.get("$Label.c.TRM_Success"),$A.get("$Label.c.TRM_Msg25"));//Show Alert
                this.redirectPage(conditionClassRecord.Id);
            }else{
                console.log("##ERROR saveConditions"+' ##ERROR# '+JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action); 
    },

    //Get info for Current User
    getUserLogged :function(component){ 
        var action = component.get('c.getUserLogged');
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                if (response.getReturnValue() == null) 
                    component.set('v.visibleButtonEnd', true);
            }else{
                console.log("##ERROR saveConditions"+' ##ERROR# '+JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action); 
    },

	//Show Alert
    showToast: function (type, title, msg){
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "duration": 5000,
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    //Redirect to Page indicated for the RecordId
    redirectPage: function (recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})