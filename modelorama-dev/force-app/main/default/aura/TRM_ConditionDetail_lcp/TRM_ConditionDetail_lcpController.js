({
	doInit : function(component, event, helper) {
		helper.getUserLogged(component);
	},
	
    saveAndExitProcess : function(component, event, helper) {
       helper.finishProcess(component);
    },

    finishProcess : function(component, event, helper) {
        var conditionClassRecord                  = component.get('v.conditionClassRecord');
        conditionClassRecord.TRM_ReadyApproval__c = true; 
        component.set('v.conditionClassRecord',conditionClassRecord);
        helper.finishProcess(component);
    },

    /** handle the action of cancel the modal **/
    cancel : function(component, event, helper){
    	component.set('v.cancel', true);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
})