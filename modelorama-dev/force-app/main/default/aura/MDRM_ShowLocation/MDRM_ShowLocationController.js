({
	doInit : function(cmp, event, helper) {
		var action = cmp.get("c.getRecord");
        action.setParams({
            accId: cmp.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var event_record = response.getReturnValue();
                console.log(event_record);
                
                if(event_record['ONTAP__Latitude__c']!= undefined & event_record['ONTAP__Longitude__c']!= undefined){
                    var lat = parseFloat(event_record['ONTAP__Latitude__c']);
                	var lon = parseFloat(event_record['ONTAP__Longitude__c']);
                	var dir = event_record['MDRM_Store_Location__c'];
                    console.log(dir);
                    cmp.set('v.location', [{
                        location: {
                            Latitude: lat,
                            Longitude: lon
                        },
                    }]);
                    cmp.set("v.MDRM_Store_Location__c",dir);
                    cmp.set("v.map_ready",true);
                }
            }else if(state == "ERROR"){
                var errors = response.getError();
                console.log(errors[0].message);
            }
        });
        $A.enqueueAction(action);
	}
})