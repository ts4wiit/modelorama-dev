({
    // Load options for Type of Products
    loadOptions: function (component, event, helper) {
        var opts    = [];
        var action  = component.get("c.getOptions");
        action.setParams({
            "objObject": component.get('v.objProd'),
            "strFld":    'ISSM_Type__c'
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.options", JSON.parse(allValues));
            }else{
                console.log('ERROR EN fetchPickListVal ',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    // show or hide list of grouping
    onblur : function(cmp,event,helper){
        cmp.set("v.listOfSearchRecords", null);
        cmp.set("v.SearchKeyWord", '');
        cmp.set("v.Message", null);
        var fclose = cmp.find("searchRes");
        helper.addRemoveClass(fclose,'slds-is-close','slds-is-open');
    },

    //When clicking on the Input, execute search
    onfocus : function(cmp,event,helper){
        $A.util.addClass(cmp.find("mySpinner"), "slds-show");
        cmp.set("v.listOfSearchRecords", null ); 
        var fOpen = cmp.find("searchRes");
        helper.addRemoveClass(fOpen,'slds-is-open','slds-is-close');
        var getInputkeyWord = '';
        helper.searchHelper(cmp,event,getInputkeyWord);
    },

    //Execute search according to the entered word
    keyPressController : function(cmp, event, helper) {
        $A.util.addClass(cmp.find("mySpinner"), "slds-show");
        var getInputkeyWord = cmp.get("v.SearchKeyWord");
        if(getInputkeyWord.length > 0){
            var fOpen = cmp.find("searchRes");
            helper.addRemoveClass(fOpen,'slds-is-open','slds-is-close');
            helper.searchHelper(cmp,event,getInputkeyWord);
        }
        else{  
            cmp.set("v.listOfSearchRecords", null ); 
            var fclose = cmp.find("searchRes");
            helper.addRemoveClass(fclose,'slds-is-close','slds-is-open');
        }
    },

    //delete the record that was selected
    clear :function(cmp,event,helper){
        var selectedPillId  = event.getSource().get("v.name");
        var AllPillsList    = cmp.get("v.lstSelectedRecords"); 
        for(var i = 0; i < AllPillsList.length; i++){
            if(AllPillsList[i].objProducts.Id == selectedPillId){
                AllPillsList.splice(i, 1);
                cmp.set("v.lstSelectedRecords", AllPillsList);
            }  
        }
        cmp.set("v.SearchKeyWord",null);
        cmp.set("v.listOfSearchRecords", null);
        helper.getSumProducts(cmp);

        var compEvent = cmp.getEvent("oRemoveProdEvent");
        compEvent.setParams({"recordByEvent" : AllPillsList });
        compEvent.fire();    
    },

    // Run when selecting a record of the Son Component (ISSM_ShowSearchResult_lcp)
    setValuesProdEvent : function(cmp, event, helper) {
        var mode = cmp.get("v.mode");

        cmp.set("v.SearchKeyWord",null);
        var listSelectedItems =  cmp.get("v.lstSelectedRecords");
        if(mode === 'NEW'){
            var selectedParamGetFromEvent = event.getParam("recordByEvent");
            listSelectedItems.push(selectedParamGetFromEvent);
            cmp.set("v.lstSelectedRecords" , listSelectedItems);
        }    
        helper.getSumProducts(cmp);
        
        var fclose = cmp.find("lookup-pill");
        helper.addRemoveClass(fclose,'slds-show','slds-hide');
        var forclose = cmp.find("searchRes");
        helper.addRemoveClass(forclose,'slds-is-close','slds-is-open');
    },

    //Calculae Total
    calculateTotal : function(cmp,event,helper) {
        helper.getSumProducts(cmp);  
    }
})