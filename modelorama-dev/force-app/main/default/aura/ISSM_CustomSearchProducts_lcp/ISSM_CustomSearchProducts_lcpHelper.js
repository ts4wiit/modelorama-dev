({
    // Execute search for products
    searchHelper : function(cmp,event,getInputkeyWord) {  
        var AllSelected = cmp.get("v.lstSelectedRecords");
        var ArraySelect =[];
        var StrMethodName="";
        var action      = "";
        var col         = "";
        var ObjectName  = "";
        var ProductType = $A.get("$Label.c.TRM_Product");

        for(var i = 0; i < AllSelected.length; i++){
            ArraySelect.push(AllSelected[i].objProducts);
        }

        console.log('cmp.find("mySelect").get("v.value"):' + cmp.find("mySelect").get("v.value"));
            console.log('ProductType:' + ProductType);

        //Evaluate the object you wish to consult
        if(cmp.find("mySelect").get("v.value") == ProductType){
            action = cmp.get('c.SearchProducts');
            col="Name, ONTAP__ProductId__c, ONTAP__MaterialProduct__c, ONTAP__ProductShortName__c";
            ObjectName="ONTAP__Product__c";
            cmp.set('v.IconName','custom:custom51');
        }else{
            action = cmp.get('c.SearchQuotas');
            col="Name, Code__c, Description__c";
            ObjectName="MDM_Parameter__c";
            cmp.set('v.IconName','custom:custom62');
        }

        action.setParams({
            'StrColumns':       col,
            'searchKeyWord':    getInputkeyWord,
            'ObjectName' :      ObjectName,
            'ExcludeitemsList': ArraySelect,
            'strLimit':         '5'
        });
    
        action.setCallback(this, function(response) {
            $A.util.removeClass(cmp.find("mySpinner"), "slds-show");
            var state = response.getState();
            if(state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse.length == 0) {
                    cmp.set("v.listOfSearchRecords", null); 
                    cmp.set("v.Message", 'No Records Found...');
                }else{
                    cmp.set("v.listOfSearchRecords", storeResponse);
                    cmp.set("v.Message", null);
                }
            }else{
                console.log('ERROR IN searchHelper',response.error);
            }
        });
        $A.enqueueAction(action);
    },

    //allows you to show or hide an item
    addRemoveClass : function(element,addcls,removecls) {
        $A.util.addClass(element, addcls);
        $A.util.removeClass(element, removecls);
    },

    //get sum of products
    getSumProducts : function(cmp) {
        var getRecords = cmp.get('v.lstSelectedRecords');
        var sum     = 0.0;
        for(var i=0; i < getRecords.length; i++){
            sum += parseFloat(getRecords[i].DecUnitPriceTax)*parseInt(getRecords[i].IntQuantity);
        }
        cmp.set('v.SumProds',sum);
    }
})