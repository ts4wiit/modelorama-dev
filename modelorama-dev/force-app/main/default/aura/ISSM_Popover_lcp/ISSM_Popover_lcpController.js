({
	// Method that is executed when initializing the combo
	doInit : function(component, event, helper) {
		var recordId = component.get('v.recordId');
        var action 	 = component.get('c.getLimitRecords');
        action.setParams({
        	'IdRec': recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.recordLimitCombos", storeResponse);
                console.log('recordLimitCombos: ',storeResponse); 
                helper.setOptions(component);
                
            }else{
                console.log('ERROR IN doInit', response.error);
            }
        });
        $A.enqueueAction(action);
	},

    //SET LIMIT SELECTED
    handleChange: function (cmp, event) {
        var slctValue = event.getParam("value");
        cmp.set('v.LimitId',slctValue);
    },

    //EXECUTE EVENT
	updateClone : function(component, event, helper) {
		var limitId = component.get('v.LimitId');
        if(limitId == ''){
            helper.showToast('error', $A.get("$Label.c.TRM_Error"),$A.get("$Label.c.TRM_Msg15"));
        }else{
            var compEvent = component.getEvent("setNewId");
            compEvent.setParams({"getId" : limitId });
            compEvent.fire(); 
        }
	},

    //CLOSE MODAL
	closeModal : function(component, event, helper) {
		var recordId = component.get('v.recordId');
        helper.redirectPage(recordId);
        helper.showToast('info',$A.get("$Label.c.TRM_Msg00"),'Operación Cancelada');
	}
	
})