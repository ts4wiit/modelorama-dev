({
	//Shows alert with the parameters sent
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    redirectPage: function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":     recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    setOptions : function(cmp){
        var lstOption = cmp.get('v.recordLimitCombos');
        var opts=[];
        console.log('lstOption: ',lstOption);
        for (var i = 0; i < lstOption.length; i++) {

            opts.push({
                label: lstOption[i].ISSM_SalesStructure__r.Name,
                value: lstOption[i].Id
            });
        }
        cmp.set('v.options',opts);
    }
})