({
    //get list of catalogs for segment and price zone
    getCatalogs :function(component){
        var action = component.get("c.getCatalog");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnValue = response.getReturnValue();
                component.set('v.mapCatalogs', returnValue);
            }else if (state === 'ERROR'){
                console.log('ERROR getCatalogs-> ',JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    //Get structures for sales office and sales org.
    getSructures :function(component){

        var action = component.get("c.getStructures");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var returnValue = response.getReturnValue();
                component.set('v.mapStructures', returnValue);
            }else if (state === 'ERROR'){
                console.log('ERROR getCatalogs-> ',JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    /** calls the Apex method to get the Condition Management (custom metadata types) **/
	getConditionManagement : function(component) {
        var action = component.get("c.getConditionManagement");
        var externalKey = component.get('v.externalKey');
        action.setParams({externalKey: externalKey});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.conditionManagement', returnValue);
                //console.log('conditionManagement retrieved:' + returnValue);
                component.set('v.fieldSetApiName', returnValue.TRM_FieldSet__c);
                //console.log('fieldSetApiName retrieved:' + returnValue.TRM_FieldSet__c);
                component.set('v.renderCustomerStep', returnValue.TRM_Customers__c);
                //console.log('includeCustomerStep:' + returnValue.TRM_Customers__c);
            } else{
                console.log('No conditionManagement retrieved');
            }
        });
        $A.enqueueAction(action); 
    },

    /** calls the Apex method to get the Condition Relationship (custom metadata types) **/
    getConditionRelationship : function(component) {
        var action = component.get("c.getConditionRelationship");
        var externalKey = component.get('v.externalKey');
        action.setParams({externalKey: externalKey});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.conditionRelationship', returnValue);
                //console.log('conditionRelationship retrieved:' + JSON.stringify(returnValue));
            } else{
                console.log('No conditionRelationship retrieved');
            }
        });
        $A.enqueueAction(action); 
    },

    /** calls the Apex method to get the Record by Id (sObject type) **/
    getRecordById : function(component) {
        var action = component.get("c.getRecordById");
        var objectType = component.get('v.objectApiName');
        var id = component.get('v.newRecordId');
        action.setParams({objectType: objectType, id: id});
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.newRecord', returnValue);
                //console.log('newRecord retrieved:' + JSON.stringify(returnValue));
                console.log('newRecord retrieved:' + a.getReturnValue());
            } else{
                console.log('No newRecord retrieved');
            }
        });
        $A.enqueueAction(action); 
    },

    /** function to navigate to sObject **/
    navigateToSObject: function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":     recordId
        });
        navEvt.fire();
    },

    /** function to show a Toast message **/
    showToast: function (type, title, msg) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },

    /** call the Apex method to delete the record **/
    deleteRecord : function(component, record) {
        var action = component.get("c.deleteRecord");
        action.setParams({
            "record": record
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Record deleted');
            } else{
                console.log('Error on delete record: ' + response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})