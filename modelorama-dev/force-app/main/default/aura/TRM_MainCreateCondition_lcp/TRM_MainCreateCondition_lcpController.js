({
    doInit : function(component, event, helper){
        //helper.getUIThemeDescription(component);
        helper.getCatalogs(component);
        helper.getSructures(component);
    },

    /** function to set the attribute 'closeModal' to true in order to display a Modal to handle the close operation **/
	closeModal : function(component, event, helper) {
        component.set('v.closeModal', true);
    },

    /** function to clear the attributes of the selection of the class condition **/
    handleCancel : function(component, event, helper){
        component.set('v.externalKey', '');
        component.set('v.fieldSetApiName', '');
        component.set('v.classConditionName', '');
    },

    /** function to set the attribute 'classConditionName' when access sequences is changed **/
    handleOnChangeAccessSequence : function(component, event, helper){
        var conditionClass = component.get('v.conditionClass');
        var accessSequence = component.get('v.accessSequence');
        component.set('v.classConditionName', conditionClass + ' ' + accessSequence);
    },

    /** call the functions to get the Custom Metadata Types when the externalKey is chnaged **/
    handleOnChangeExternalKey : function(component, event, helper){
    	if(component.get('v.externalKey') != null || component.get('v.externalKey') != ''){
            helper.getConditionManagement(component);
            helper.getConditionRelationship(component);
    	}
    },

    /** function to set the attribute 'doSave' to true in order to make the RecordFieldSet to save the record **/
    handleOnClickSave : function(component, event, helper){
    	component.set('v.doSave', true);
    },

    /** call the function to get the Record by Id when the attribute 'newRecordId' is different of null **/
    handleOnChangeNewRecordId : function(component, event, helper){
        if( component.get('v.newRecordId') != null || component.get('v.newRecordId') != '' ){
            component.set('v.isNewRecordSaved', true);
            //helper.navigateToSObject(component.get('v.newRecordId'));
            helper.getRecordById(component);
        }
    },

    /** call the function to show a Toast message when there is an error on Save in the RecordFieldSet component **/
    handleOnChangeErrorOnSave : function(component, event, helper){
        var errorOnSave = component.get('v.errorOnSave');
        if(errorOnSave){
            helper.showToast('error', $A.get("$Label.c.TRM_TitleError"), $A.get("$Label.c.TRM_MsgCheck"));
        }
    },

    /** call a function to get the update of the record when changes are done in the RecordFieldSet component **/
    updatedRecord : function(component, event, helper){
        var updated = event.getParam("updated");
        if(updated){
            helper.getRecordById(component);
        }
    },

    /** function to handle the action of the modal when the user is closing the process
     ** if the actions is confirmed, the record is deleted (roll back) and the user is navigated to the object tab
     **/
    closeModalAction : function(component, event, helper){
        var confirmed = event.getParam("confirmed");//true?
        var newRecord = component.get('v.newRecord');
        if(confirmed){
            if( newRecord.Id != undefined ){
                helper.deleteRecord(component, newRecord);
            }
            var objectApiName = component.get('v.objectApiName');
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": objectApiName
            });
            homeEvent.fire();
        }
        
    },

    updateRecordFields : function(component, event, helper){
        var recordFieldValueMap = event.getParam("recordFieldValueMap");
        for ( var [clave, valor] of recordFieldValueMap ) {
            if(clave == 'TRM_Quota__c'){
                component.set('v.quota', valor);
            }
        }
    }
})