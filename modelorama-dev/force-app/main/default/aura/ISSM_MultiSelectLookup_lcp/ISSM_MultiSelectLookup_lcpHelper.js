({
    //Search for Price Grouper
    searchHelper : function(cmp,event,getInputkeyWord) {
        var StrMethodName=cmp.get("v.StrMethodName");
        var action = cmp.get(StrMethodName); 
        action.setParams({
        	'StrColumns':           cmp.get("v.selectColumns"),
            'searchKeyWord':        getInputkeyWord,
            'ObjectName' :          cmp.get("v.objectAPIName"),
            'ExcludeitemsList' :    cmp.get("v.lstSelectedRecords"),
            'strLimit':             '5'
        });

        action.setCallback(this, function(response) {
            $A.util.removeClass(cmp.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    cmp.set("v.Message", $A.get("$Label.c.TRM_Msg02"));
                }else{
                    cmp.set("v.Message", '');
                }
                cmp.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        $A.enqueueAction(action);
    },

    //allows you to show or hide an item
    addRemoveClass : function(element,addcls,removecls) {
        $A.util.addClass(element, addcls);
        $A.util.removeClass(element, removecls);
    }
})