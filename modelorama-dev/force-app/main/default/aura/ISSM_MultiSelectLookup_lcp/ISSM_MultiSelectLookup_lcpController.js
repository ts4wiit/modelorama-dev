({
    doInit : function(cmp, event, helper){
        var mode = cmp.get('v.mode');
        if( mode == 'EDIT' ){
            console.log('elements: ' + cmp.getElements());
            var pill = cmp.find('pill');
        }
    },
    // Shows or hides groupers list
    onblur : function(cmp,event,helper){
        // on mouse leave clear the listOfSeachRecords & hide the search result component 
        cmp.set("v.listOfSearchRecords", null );
        cmp.set("v.SearchKeyWord", '');
        var fclose = cmp.find("searchRes");
        helper.addRemoveClass(fclose,'slds-is-close','slds-is-open');
    },

    //when clicking on the Input, execute search
    onfocus : function(cmp,event,helper){
        // show the spinner,show child search result component and call helper function
        $A.util.addClass(cmp.find("mySpinner"), "slds-show");
        cmp.set("v.listOfSearchRecords", null ); 
        var fOpen = cmp.find("searchRes");
        helper.addRemoveClass(fOpen,'slds-is-open','slds-is-close');
        // Get Default 5 Records order by createdDate DESC 
        var getInputkeyWord = '';
        helper.searchHelper(cmp,event,getInputkeyWord);
    },

    //Execute search according to the entered word
    keyPressController : function(cmp, event, helper) {
        $A.util.addClass(cmp.find("mySpinner"), "slds-show");
        // get the search Input keyword   
        var getInputkeyWord = cmp.get("v.SearchKeyWord");
        if(getInputkeyWord.length > 0){
            var fOpen = cmp.find("searchRes");
            helper.addRemoveClass(fOpen,'slds-is-open','slds-is-close');
            helper.searchHelper(cmp,event,getInputkeyWord);
        }
        else{  
            cmp.set("v.listOfSearchRecords", null ); 
            var fclose = cmp.find("searchRes");
            helper.addRemoveClass(fclose,'slds-is-close','slds-is-open');
        }
    },

    //delete the record that was selected
    clear :function(cmp,event,heplper){
        var mode = cmp.get('v.mode');
        if( mode != 'EDIT'){
            var selectedPillId = event.getSource().get("v.name");
            var AllPillsList = cmp.get("v.lstSelectedRecords"); 
            for(var i = 0; i < AllPillsList.length; i++){
                if(AllPillsList[i].Id == selectedPillId){
                    AllPillsList.splice(i, 1);
                    cmp.set("v.lstSelectedRecords", AllPillsList);
                }  
            }
            cmp.set("v.SearchKeyWord",null);
            cmp.set("v.listOfSearchRecords", null);
    
            var compEvent = cmp.getEvent("oRemoveRecordEvent");
            compEvent.setParams({"recordByEvent" : AllPillsList });
            compEvent.fire();
            }
            
    },

    // Run when selecting a record of the Son Component (ISSM_ShowSearchResult_lcp)
    setValuesEvent : function(cmp, event, helper) {
        cmp.set("v.SearchKeyWord",null);
        var listSelectedItems =  cmp.get("v.lstSelectedRecords"); 
        var selectedParamGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedParamGetFromEvent);
        cmp.set("v.lstSelectedRecords" , listSelectedItems);
        
        var fclose = cmp.find("lookup-pill");
        helper.addRemoveClass(fclose,'slds-show','slds-hide');
        var forclose = cmp.find("searchRes");
        helper.addRemoveClass(forclose,'slds-is-close','slds-is-open');
    }
})