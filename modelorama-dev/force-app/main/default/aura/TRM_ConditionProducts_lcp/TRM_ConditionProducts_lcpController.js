({
    //When changing the value of the metadata, it generates the query again
    OnChangeConditionManagement : function(component,event,helper){
        var labelAmnt = (component.get('v.conditionManagement').TRM_IsPercentage__c) ? $A.get("$Label.c.TRM_Percentage") : $A.get("$Label.c.TRM_Amount");
        component.set('v.labelAmount',labelAmnt);  

        if(!component.get('v.isEditMode')){
        	helper.searchProduct(component);
        }
    },
    
    //When changing the value of the condition class, it generates the query again
    OnChangeConditionClass : function(component,event,helper){
        
        if(component.get('v.isEditMode') && component.get('v.isExecute')){
            helper.getProducts(component);    
        }else{
            helper.searchProduct(component);
        }
    },
    
	//Method that is executed when entering a value in the search
    doInit : function(component, event, helper) {
        helper.startSearch(component);
    },

    //Execute serach by tab
    searchByTab : function(component,event,helper){
        var blnSbyTab = component.find("checkSearchByList").get("v.value");

        if(blnSbyTab){
            helper.searchProduct(component);
        }
    }, 
    
    //Method that is executed when entering a value in the search
    filterTable : function(component, event, helper) {
        helper.searchProduct(component);
    },

    //renders the page
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },

    //Select everything that is currently in the search
    selectAll: function(component, event, helper) {
        helper.selectAllCheck(component);
    },

    //Turn on the check from the input of quantity
    SelectCheck: function (cmp,event,helper){
        var LstPrd = cmp.get("v.wrapperList");
        for(var i = 0; i < LstPrd.length; i++) {
            LstPrd[i].IsSelected = (Number(LstPrd[i].DecAmount) > 0) ? true : false;
        }
        helper.renderPage(cmp);
    },

    //Process the selected products to convert them into conditions
    ProcessCondition : function(component,event,helper){
        // the second param is for indicate process products
        helper.setProducts(component,true);
    },

    //Apply pricing for products selected
    ApplyPricing: function(component,event,helper){
        helper.setProducts(component,false);
    },

    //Action that is executed when clicking on clean
    cleanRecords : function(component,event,helper){
        component.set('v.blnClean',true);
        var LstIds = component.get("v.LstProdCondition");
        var childComponent = component.find("CmpClean");
        childComponent.executeQuery(LstIds);
    },

    //Remove products and scales
    removeProducts: function(component, event, helper){
        helper.removeDeleteProducts(component,event);
    },

    //Show modal for add scales
    addScales :function(cmp,event,helper){
        cmp.set("v.blnAdd",false); 
        var objCondition  = event.getSource().get("v.name");
        if(objCondition.DecAmount){
            cmp.set('v.ConditionProduct',objCondition);
            cmp.set("v.blnAdd",true);
        }else{
            helper.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg19"));
        }
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
})