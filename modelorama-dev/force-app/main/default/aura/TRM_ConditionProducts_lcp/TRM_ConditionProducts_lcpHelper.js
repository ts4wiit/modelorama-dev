({
    //Query do init process
    startSearch: function(component){
        var key    = component.get("v.keyWord");
        var LstIds = component.get('v.LstIds');
        var action = component.get("c.SearchProducts");

        action.setParams({"StrQuota"    : '',
                          "StrKey"      : '',
                          "LstIds"      : LstIds,
                          "StrCondition": '',
                          "blnSbyTab":    false});
        
        action.setCallback(this, function(resp){
            var state = resp.getState();
            if (state === "SUCCESS") {
                var NewLstProd = resp.getReturnValue();
                component.set('v.wrapperList',NewLstProd);
                if(component.get('v.isExecute') && component.get('v.isEditMode')){
                    this.getProducts(component);
                }
                this.renderPage(component);
            }else{
                this.showToast("error", $A.get("$Label.c.TRM_Error"), JSON.stringify(resp.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    //Method for product search
    searchProduct : function(component) {
        var blnSbyTab   = component.find("checkSearchByList").get("v.value");
        var LstSelect   = [];//contains the selected products
        var LstAllPrd   = component.get("v.wrapperList");

        var key    = component.get("v.keyWord");
        var LstIds = component.get('v.LstIds');
        var action = component.get("c.SearchProducts");
        var condnt = '';
        var quota  = '';

        //Validate if the metadata is correct search if it contains an additional filter
        if(component.get('v.conditionManagement')){
            condnt = component.get('v.conditionManagement').TRM_FilterProducts__c;
            quota  = component.get('v.quota');
        }
        
        //Perform the iteration of the products already selected
        for(var i=0; i<LstAllPrd.length; i++) {
            if(LstAllPrd[i].IsSelected){
                LstSelect.push(LstAllPrd[i]);
                LstIds.push('\''+LstAllPrd[i].ObjProducts.Id+'\'');
            }
        }
        
        action.setParams({"StrQuota"    : quota,
                          "StrKey"      : key,
                          "LstIds"      : LstIds,
                          "StrCondition": condnt,
                          "blnSbyTab":    blnSbyTab});
        
        action.setCallback(this, function(resp){
            var state = resp.getState();
            if (state === "SUCCESS") {
                var NewLstProd = resp.getReturnValue();
                if(component.get('v.isExecute') && component.get('v.isEditMode')){
                    this.getProducts(component);
                }else{
                    //Iterate the products already selected and add them to the list returned by the search
                    for(var i=0; i<LstSelect.length; i++) {
                        NewLstProd.push(LstSelect[i]);
                    }

                    NewLstProd.sort(function(a,b) {
                        return a.ObjProducts.ONTAP__ExternalKey__c - b.ObjProducts.ONTAP__ExternalKey__c;
                    });
                }
                component.set('v.wrapperList',NewLstProd);
                this.renderPage(component);
            }else{
                this.showToast("error", $A.get("$Label.c.TRM_Error"), JSON.stringify(resp.getError()));
            }
        });
        $A.enqueueAction(action); 
    },

    //Get products of condition class
    getProducts : function(component) {

        if(component.get('v.conditionClassRecord')){
            var action = component.get('c.getProducts');
            action.setParams({"IdCondClass": component.get('v.conditionClassRecord').Id});
            action.setCallback(this, function(resp){
                var state = resp.getState();
                if (state === "SUCCESS") {
                    var NewLstProd = resp.getReturnValue();
                    NewLstProd = this.removeDuplicates(NewLstProd, JSON.stringify);
                    component.set('v.wrapperList',NewLstProd);
                    component.set('v.isExecute',false);
                    this.getScales(component);
                }else{
                    this.showToast("error", $A.get("$Label.c.TRM_Error"), JSON.stringify(resp.getError()));
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    //Get scales of condition class
    getScales : function(component){

        if(component.get('v.conditionClassRecord')){
            var action = component.get('c.getScales');
            action.setParams({"IdCondClass": component.get('v.conditionClassRecord').Id});
            action.setCallback(this, function(resp){
                var state = resp.getState();
                if (state === "SUCCESS") {
                    var LstScales = resp.getReturnValue();
                    LstScales = this.removeDuplicates(LstScales, JSON.stringify);
                    console.log('###getScales###-> ',LstScales);
                    component.set('v.lstConditionScales',LstScales);
                    this.renderPage(component);
                }else{
                    this.showToast("error", $A.get("$Label.c.TRM_Error"), JSON.stringify(resp.getError()));
                }
            });
            $A.enqueueAction(action);
        }
    },

    //It allows rendering every time the page is changed
    renderPage: function(component) {
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.wrapperList");
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        var pageNumber = component.get("v.pageNumber");
        var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
        component.set("v.currentList", pageRecords);
    },

    //It is executed by clicking on the process button
    //set the products for process in batch
    setProducts : function(component,blnEvent){
        var lstSelected = component.get('v.wrapperList');
        var lstProdCond = component.get('v.LstProdCondition');
        var LstIds      = component.get('v.LstIds');
        var amount      = component.get('v.amount');
        var amountOp    = component.get('v.amountOp');
        var isClone     = component.get('v.conditionClassRecord').TRM_IsClone__c;
        var isPercent   = component.get('v.conditionManagement').TRM_IsPercentage__c;
        var option      = (isClone && !isPercent) ? component.find('select').get('v.value') : $A.get("$Label.c.TRM_Percentage");
        var blnError    = false;
        var blnAmnt     = false;

        for(var i=0; i<lstSelected.length; i++){
            if(lstSelected[i].IsSelected){
                var amnt = Number((amount>0) ? Number(amount) : Number(lstSelected[i].DecAmount));
                amnt = (isNaN(amnt)) ? 0 : amnt;
                
                if(isClone && option == $A.get("$Label.c.TRM_Percentage") && !isPercent){
                    amnt = Number(amnt)+(Number(amnt)*(Number(amountOp)/100));
                }else if(isClone){
                    amnt = Number(amnt)+Number(amountOp);
                }

                if(amnt>0){
                    //Remove previous SKU with Same Id
                    var id = lstSelected[i].ObjProducts.Id;
                    lstProdCond.slice().reverse().forEach(function(item, index, object) {
                        if (item.Id === id) {
                            lstProdCond.splice(object.length - 1 - index, 1);
                        }
                    });

                    lstSelected[i].DecAmount = amnt;
                    lstProdCond.push({"Id"                : lstSelected[i].ObjProducts.Id,
                                      "ISSM_UnitPrice__c" : amnt,
                                      "ISSM_SectorCode__c": lstSelected[i].ObjProducts.ISSM_SectorCode__c});
                    LstIds.push('\''+lstSelected[i].ObjProducts.Id+'\'');
                    blnAmnt = (lstSelected[i].DecAmntPrev > lstSelected[i].DecAmount) ? true:false;
                }else{
                    blnError = true;
                    break;
                }

                if(blnEvent){
                    component.set('v.blnProcess',false);
                    delete lstSelected[i];
                }else{
                    component.set('v.blnApply',false);
                }
            }
        }

        //Valida si el monto nuevo es menor al anterior muestra alerta pero continua el proceso
        if(blnAmnt){
            this.showToast("info", $A.get("$Label.c.TRM_Msg00"), $A.get("$Label.c.TRM_Msg20"));
        }
        //If you have not selected any material
        if(lstProdCond.length == 0){
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg21"));
            blnError = true;
        }
        //If the amount is less than or equal to zero
        if(blnError){
            this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg22"));
        }

        component.set('v.amount','');
        component.set('v.amountOp','');
        component.set('v.keyWord','');
        //Click in process and not error
        if(blnEvent && !blnError){
            lstSelected = lstSelected.filter(element => element !== undefined);
            component.set('v.LstIds',LstIds);
            component.set('v.LstProdCondition',lstProdCond);
            component.set('v.wrapperList',lstSelected);
            this.sendConditionEvent(component);
            component.find("checkAll").set("v.value",false);
            this.searchProduct(component);
            this.showToast("success", $A.get("$Label.c.TRM_Success") ,$A.get("$Label.c.TRM_Msg23"));  
        }
        this.renderPage(component);
    },

    //Send the selected products to the path
    sendConditionEvent : function(component){
        var LstProdCondition = component.get('v.LstProdCondition');//Lista de productos seleccionados
        var LstScalesConditn = component.get('v.lstConditionScales');
        var compEvent        = component.getEvent("generateConditions");
        compEvent.setParams({"lstRecordsCndt" : LstProdCondition,
                             "lstRecordsScls" : LstScalesConditn});
        compEvent.fire();  
    },

    //Select everything that is currently in the search
    selectAllCheck: function(component) {
        var BlnselectAll = component.find("checkAll").get("v.value");
        var LstPrd = component.get("v.wrapperList");
        for(var i = 0; i < LstPrd.length; i++) {
            LstPrd[i].IsSelected = (BlnselectAll) ? true : false;
        }
        this.renderPage(component);
    },

    //Remove products and scales
    removeDeleteProducts: function(component,event){
        var isExecute   = event.getParam("isExecute");
        var LstIds      = event.getParam("LstIds");
        var lstProdCond = component.get('v.LstProdCondition');
        var lstScales   = component.get('v.lstConditionScales');
        component.set('v.blnClean',false);
        if(isExecute){
            //Filter and remove deselected records
            lstProdCond = lstProdCond.filter(function(cv){
                return LstIds.find(function(e){
                    return e.replace(/(^'|'$)/g, '') == cv.Id;
                });
            });

            lstScales = lstScales.filter(function(cv){
                return LstIds.find(function(e){
                    return e.replace(/(^'|'$)/g, '') == cv.TRM_ConditionRecord__c;
                });
            });
            
            component.set('v.LstProdCondition',lstProdCond);
            component.set('v.lstConditionScales',lstScales);  
            component.set('v.LstIds',LstIds);
            this.sendConditionEvent(component);
            this.searchProduct(component);
        }
        this.renderPage(component);
    },

    //Show Alert
    showToast: function (type, title, msg) {
        var resultToast = $A.get("e.force:showToast");
        console.log('resultToast ',resultToast);
        resultToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultToast.fire();
    },

    //Remove duplicate 
    removeDuplicates :function (array, key) {
        var setArray = new Set();
        return array.filter(item => {
            var k = key(item);
            return setArray.has(k) ? false : setArray.add(k);
        });
    }
})