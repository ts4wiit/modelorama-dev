({
	onChangeSwitch: function(component, event, helper) {
    	var action = component.get("c.changeStatus");
        var Rule = component.get("v.Rule").MDM_Code__c;
        var status = component.get("v.Rule").MDM_IsActive__c; 
        
        //var en = component.find("ruleSec");
		//en.set("v.disabled",true );        
        
        action.setParams({ 
               code : Rule, 
               status  : status 
                         });
        action.setCallback(this, function(response) {});
        $A.enqueueAction(action);

}
})