({
	sectionZero : function(component, event, helper) {
		helper.helperFun(component,event,'articleZero');
	},

	sectionOne : function(component, event, helper) {
		helper.helperFun(component,event,'articleOne');
	},

	sectionTwo : function(component, event, helper) {
		helper.helperFun(component,event,'articleTwo');
	},

	sectionThree : function(component, event, helper) {
		helper.helperFun(component,event,'articleThree');
	},

	sectionFour : function(component, event, helper) {
		helper.helperFun(component,event,'articleFour');
	},

	sectionOneDD : function(component, event, helper) {
		helper.helperFunDD(component,event,'articleOne',helper);
	},

	sectionTwoDD : function(component, event, helper) {
		helper.helperFunDD(component,event,'articleTwo',helper);
	},

	sectionThreeDD : function(component, event, helper) {
		helper.helperFunDD(component,event,'articleThree',helper);
	},

	showResultCtrl: function (component, event, helper) {
		helper.showResultHlp(component, event, helper);
	},

	EventShowList : function(component, event, helper) {
		var parametrohijo0 = event.getParam("visualLevel");
		var parametrohijo1 = event.getParam("idLevelTxt");
		var parametrohijo2 = event.getParam("tourName");
		var parametrohijo3 = event.getParam("tourStatus");
		var parametrohijo4 = event.getParam("executionDate");
		var parametrohijo5 = event.getParam("showResultDetails");
		var parametrohijo6 = event.getParam("showList");

		component.set("v.visualLevel",parametrohijo0);
		component.set("v.routeName",parametrohijo1);
		component.set("v.idLevelTxt",parametrohijo1);
		component.set("v.tourName",parametrohijo2);
		component.set("v.tourStatus",parametrohijo3);
		component.set("v.executionDate",parametrohijo4);
		component.set("v.showResults",parametrohijo5);
		component.set("v.showList",parametrohijo6);

		if (parametrohijo6) {
			component.set("v.ListOfChIChOHd",null);
			component.set("v.ListOfChIChOIt",null);
			component.set("v.ListOfChIChOPy",null);
			component.set("v.ListOfRoutesTable",null);
			component.set("v.ListOfValues",null);
			component.set("v.ListOfTourDocs",null);
			component.set("v.ListOfVisitDocs",null);
			component.set("v.ListOfVisits",null);
			component.set("v.ListOfClientVisits",null);
			component.set("v.ListOfClientSales",null);
			component.set("v.ListOfClientBalance",null);
			helper.fetchInitialData(component, event, helper);
		}
	},

	updateCOCICtrl: function (component, event, helper) {
		helper.updateCOCIHlp(component, event, helper);
	},

	updateClientVisitsCtrl: function (component, event, helper) {
		helper.updateClientVisitsHlp(component, event, helper);
	}
})