({

	//SHOW RESULTS AND PASS TOUR AND ROUTE INFO.
	showResultHlp : function(component, event, helper) {
		var idx = event.getSource().get("v.name");
		var bits = idx.split(/[\s,]+/);
		component.set("v.routeName",bits[0]);
		component.set("v.tourId",bits[1]);
		component.set("v.showResults",true);
		component.find("tabs").set("v.selectedTabId","routeTab");
		this.fetchData(component, event, helper);
	},

	//SHOW MESSAGES POP UP.
	handleShowToast: function(cmp, event, helper) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({ title: "Error", message: cmp.get('v.errMessage'), type: "error" });
		toastEvent.fire();
	},

	//FETCH INITIAL DATA TO FILL RESULTS TAB.
	fetchInitialData : function(component, event, helper) {
		var action = component.get("c.fetchResults");
		var msgNoResults = component.get("v.LabelNoResults");
		var requiredField = component.get("v.RequiredField");
		var requiredFieldId = component.get("v.RequiredFieldId");
		var requiredFieldDate = component.get("v.RequiredFieldDate");
		var msg = requiredField;
		var visualLevel = component.get("v.visualLevel");
		var idLevelField = component.get("v.idLevelTxt");
		var tourStatus = component.get("v.tourStatus");
		var exeDate = component.get("v.executionDate");
		var tourId = component.get("v.tourName");
		var doSearch = false;

		//SEARCH CASE 2.
		if(tourId != '') {
			idLevelField = tourId;
			visualLevel = 'Tour ID';
			doSearch = true;
			exeDate = '';
			tourStatus = '';
			component.set("v.showDRV", false);
			component.set("v.showORG", false);
			component.set("v.showOFF", false);
			component.set("v.showROU", true);
		} else {

			//SEARCH CASE 3.
			if(tourStatus != 'option') {
				if(idLevelField != '') {
					if(exeDate != '') {
						doSearch = true;
					} else {

						//DATE IS REQUIRED
						msg = requiredFieldDate;
						doSearch = false;
					}
				} else {

					//ID LEVEL FIELD IS REQUIRED
					msg = requiredFieldId;
					doSearch = false;
				}
			} else {
				if(idLevelField != '') {
					if(exeDate != '') {
						doSearch = true;
					} else {

						//DATE IS REQUIRED
						msg = requiredFieldDate;
						doSearch = false;
					}
				} else {

					//ID LEVEL FIELD IS REQUIRED
					msg = requiredFieldId;
					doSearch = false;
				}
			}
		}

		if(doSearch) {
			action.setParams({strVisualLevel : visualLevel, strIdLevelField : idLevelField, strTourStatus : tourStatus, strExecutionDate : exeDate});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if(state === "SUCCESS") {
		 			if(response.getReturnValue()) {
						component.set("v.ListResult",response.getReturnValue());
						component.set("v.isListResults",true);

						//Event Display Download Logs Documents Button.
						var eventToFire = $A.get('e.c:AMCsvEvt');
						eventToFire.setParams({visualLevel : visualLevel, idLevelTxt : idLevelField, tourStatus : tourStatus, executionDate : exeDate, showCSVDetails: true});
						eventToFire.fire();

						//SEARCH CASE 1 AND 2.
						if(tourStatus == 'option') {
							if(visualLevel == component.get("v.DRV")) {
								component.set("v.showDRV", true);
								component.set("v.showORG", false);
								component.set("v.showOFF", false);
								component.set("v.showROU", false);
							} else if(visualLevel == component.get("v.UEN")) {
								component.set("v.showDRV", false);
								component.set("v.showORG", true);
								component.set("v.showOFF", false);
								component.set("v.showROU", false);
							} else if(visualLevel ==component.get("v.Office")) {
								component.set("v.showDRV", false);
								component.set("v.showORG", false);
								component.set("v.showOFF", true);
								component.set("v.showROU", false);
							} else if(visualLevel ==component.get("v.Route")) {
								component.set("v.showDRV", false);
								component.set("v.showORG", false);
								component.set("v.showOFF", false);
								component.set("v.showROU", true);
							}

						//SEARCH CASE 3.
						} else {
							component.set("v.showDRV", false);
							component.set("v.showORG", false);
							component.set("v.showOFF", false);
							component.set("v.showROU", true);
						}
					} else {

						//NO RESULTS FOR THIS SEARCH.
						component.set("v.errMessage",msgNoResults);
						this.handleShowToast(component,event,helper);
						component.set("v.ListResult",null);
						component.set("v.showResults",false);
						component.set("v.showList", false);
						component.set("v.showDRV", false);
						component.set("v.showORG", false);
						component.set("v.showOFF", false);
						component.set("v.showROU", false);
					}
				} else {

					//ERROR IN THE SEARCH
					component.set("v.errMessage",msgNoResults);
					this.handleShowToast(component,event,helper);
					component.set("v.ListResult",null);
					component.set("v.showResults",false);
					component.set("v.showList", false);
					component.set("v.showDRV", false);
					component.set("v.showORG", false);
					component.set("v.showOFF", false);
					component.set("v.showROU", false);
				}
			});
			$A.enqueueAction(action);
		} else {

			//REQUIRES FIELDS
			component.set("v.errMessage",msg);
			this.handleShowToast(component,event,helper);
			component.set("v.ListResult",null);
			component.set("v.showResults",false);
			component.set("v.showList", false);
			component.set("v.showDRV", false);
			component.set("v.showORG", false);
			component.set("v.showOFF", false);
			component.set("v.showROU", false);
		}
	},

	//FETCH DATA TO FILL ROUTE TABLE.
	fetchData : function(component, event, helper) {
		var action = component.get("c.fetchFTRoute");
		var msgNoResults = component.get("v.LabelNoResults");
		action.setParams({strAlternateName : component.get("v.tourId")});
		component.set("v.ListOfRoutesTable", null);
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				var array=response.getReturnValue();
				if(response.getReturnValue()) {
					component.set("v.ListOfRoutesTable",response.getReturnValue());
				} else {
					component.set("v.errMessage",msgNoResults);
					this.handleShowToast(component,event,helper);
				}
			}
 		});
		$A.enqueueAction(action);
		this.fetchCOCI(component, event, helper);
		this.fetchTourDocs(component, event, helper);
		this.fetchClient(component, event, helper);
	},

	//FETCH DATA TO FILL COCI HEADER TABLE.
	fetchCOCI: function(component, event, helper) {
		var action = component.get("c.fetchSTChIChOHd");
		action.setParams({strAlternateName : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfChIChOHd",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	//UPDATE COCI ITEMS AND PAYMENT ACCORDING TO THE COCI HEADER SELECTED.
	updateCOCIHlp : function(component, event, helper) {
		var idx = event.getSource().get("v.name");
		component.set("v.cociId",idx);
		this.fetchCOCIItem(component, event, helper);
	},

	//UPDATE CLIENTS INFOR ACCORDING TO THE CLIENT ID SELECTED.
	updateClientVisitsHlp : function(component, event, helper) {
		var idx = event.getSource().get("v.name");
		component.set("v.clientId",idx);
		component.set("v.showClientVisits", true);
		this.fetchClientVisits(component, event, helper);
	},

	//FETCH DATA TO FILL COCI ITEM  AND PAYMENT TABLES.
	fetchCOCIItem : function(component, event, helper) {
		var cociId = component.get("v.cociId");
		var strTourIdAlternateName = component.get("v.tourId");
		var action = component.get("c.fetchSTChIChOIt");
		component.set("v.showCOCIItem", true);
		action.setParams({douCociIdIt : cociId, strAlternateName : strTourIdAlternateName});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfChIChOIt",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
		action = component.get("c.fetchSTChIChOPy");
		action.setParams({douCociIdIt : cociId, strAlternateName : strTourIdAlternateName});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfChIChOPy",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	//FETCH DATA TO FILL CLIENTS TABLE.
	fetchClientVisits : function(component, event, helper) {
		var action = component.get("c.fetchClientVis");
		action.setParams({strClientId : component.get("v.clientId"), strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfClientVisits",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
		action = component.get("c.fetchClientSales");
		action.setParams({strClientId : component.get("v.clientId"), strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfClientSales",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
		action = component.get("c.fetchClientBalance");
		action.setParams({strClientId : component.get("v.clientId"), strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfClientBalance",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
		action = component.get("c.fetchVisitDoc");
		action.setParams({strClientId : component.get("v.clientId"), strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfVisitDocs",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	//FETCH DATA TO FILL TOUR DOCS TABLE.
	fetchTourDocs : function(component, event, helper) {
		var action = component.get("c.fetchTourDoc");
		action.setParams({strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfTourDocs",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	//FETCH DATA TO FILL CLIENT TABLE.
	fetchClient : function(component, event, helper) {
		var action = component.get("c.fetchClient");
		action.setParams({strTourId : component.get("v.tourId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				component.set("v.ListOfVisits",response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	//MANAGE COLLAPSABLE TABLES WITH DRILL DOWN BEHAVIOR.
	helperFunDD : function(component, event, secId, helper) {
		var msgNoResults = component.get("v.LabelNoResults");
		var idx = event.getSource().get("v.name");
		var bits = idx.split(/[\s,]+/);
		var idFather = bits[0];
		var dateExe = bits[1];
		var visLev = bits[2];
		var action = component.get("c.fetchResultsDrillDown");
		action.setParams({strVisualLevel : visLev, strIdLevelField : idFather, strTourStatus : '', strExecutionDate : dateExe});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
 				if(response.getReturnValue()) {
					if(visLev == '0') {
						component.set("v.ListOrg",response.getReturnValue());
						component.set("v.ListOff",null);
						component.set("v.ListRoute",null);
						component.set("v.showDRV", true);
						component.set("v.showORG", true);
						component.set("v.showOFF", false);
						component.set("v.showROU", false);
						component.set("v.isOrgDd", true);
						component.set("v.isOffDd", false);
						component.set("v.isRouDd", false);
					} else if(visLev == '1') {
						component.set("v.ListOff",response.getReturnValue());
						component.set("v.ListRoute",null);
						component.set("v.showORG", true);
						component.set("v.showOFF", true);
						component.set("v.isOffDd", true);
						component.set("v.isRouDd", false);
					} else if(visLev == '2') {
						component.set("v.ListRoute",response.getReturnValue());
						component.set("v.showOFF", true);
						component.set("v.showROU", true);
						component.set("v.isRouDd", true);
					}
				} else {
					component.set("v.errMessage",msgNoResults);
					this.handleShowToast(component,event,helper);
					component.set("v.showResults",false);
					component.set("v.showList", false);
					component.set("v.isListResults",false);
					component.set("v.showDRV", false);
					component.set("v.showORG", false);
					component.set("v.showOFF", false);
					component.set("v.showROU", false);
					component.set("v.isRouDd", false);
					component.set("v.isOffDd", false);
					component.set("v.isOrgDd", false);
				}
			}
 		});
		$A.enqueueAction(action);
		var acc = component.find(secId);
		for(var cmp in acc) {
			$A.util.toggleClass(acc[cmp], 'slds-show');
			$A.util.toggleClass(acc[cmp], 'slds-hide');
		}
	},

	//MANAGE.COLLAPSABLE TABLES BEHAVIOR.
	helperFun : function(component, event, secId) {
		var acc = component.find(secId);
		for(var cmp in acc) {
			$A.util.toggleClass(acc[cmp], 'slds-show');
			$A.util.toggleClass(acc[cmp], 'slds-hide');
		}
	}
})