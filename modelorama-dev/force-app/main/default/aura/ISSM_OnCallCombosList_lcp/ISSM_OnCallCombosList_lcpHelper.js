({
	getDataCombo : function(cmp, event) {  
		cmp.set("v.isOpenSpinner",true);
        var action = cmp.get('c.getResultRecords');
        action.setParams({
        	"accountId1" : cmp.get('v.accountId'),
        	"strObjectName" : 'ISSM_Combos__c'
        });
        action.setCallback(this, function (response) {
        	var state = response.getState();
        	if(response.getReturnValue().lstDataTableData.length > 0 ){
        		//manda al VF para mostrar u ocultar la seccion de combos 
        		var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        		myEvent.setParams({"ShowCombos":'inline'});
        		myEvent.fire();
        		
        		if (state === "SUCCESS") {
        			cmp.set("v.maxByAvail_Str",'-');
                    cmp.set("v.used_Str",'0');
	                cmp.set("v.mycolumns", response.getReturnValue().lstDataTableColumns);
	                cmp.set("v.mydata", response.getReturnValue().lstDataTableData);
	                var data_lst = response.getReturnValue().lstDataTableData;
	                // console.log(data_lst);
	                cmp.set("v.combos_map", response.getReturnValue().comboByProds_map);
	                for(var data of data_lst){
	                    //console.log(data.ISSM_ComboType__c);
	                    if(data.ISSM_ComboType__c == 'Expecific'){
	                        data.ISSM_ComboType__c = $A.get("$Label.c.ISSM_SpecificCombos");
	                    }
	                    if(data.ISSM_ComboType__c == 'Mixed'){
	                        data.ISSM_ComboType__c = $A.get("$Label.c.ISSM_MixedCombos");
	                    }
	                    if(data.ISSM_ComboType__c == 'Quotas'){
	                        data.ISSM_ComboType__c = $A.get("$Label.c.ISSM_QuotasCombos");    
	                    }
	                }
	                cmp.set("v.mydata", data_lst);
	                // console.log(response.getReturnValue().comboByProds_map); 
	                cmp.set("v.isOpenSpinner",false);  
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
           
        	
        }else{
        	//manda al VF para mostrar u ocultar la seccion de combos 
        	var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        	myEvent.setParams({"ShowCombos":'none'});
        	myEvent.fire();
        }
           
        });
        $A.enqueueAction(action);
       
    },

    checkCheckbox: function(component, event, helper){
        var NameSelected = event.target.name;        
        var valueSelected = Number(event.target.value);        

        if(valueSelected > 0){
            document.getElementById(NameSelected).checked = true; 
        }else{
            document.getElementById(NameSelected).checked = false; 
        }
    },

   /* getValuesSelected: function(component, event, helper) {        
        var idSelected = event.target.id;
        var NameSelected = event.target.name;
        var maxSelected = event.target.max;      
        var valueSelected = Number(event.target.value);        
        var valuesAcum = 0;
        var cantidad = component.get('v.intValueSelected');             
        var TotalSelected = 0;

        if(valueSelected > 0){
            document.getElementById(NameSelected).checked = true; 
        }else{
            document.getElementById(NameSelected).checked = false; 
        }
        
        var arreIdsSelected =  component.get('v.strIdValueSelected');
        if(arreIdsSelected.length>0){
            arreIdsSelected.push(idSelected);
            component.set("v.strIdValueSelected",arreIdsSelected);
        }else{
            arreIdsSelected.push(idSelected);
            component.set("v.strIdValueSelected",arreIdsSelected);
        }
        
        var valProduct  =  component.get('v.lstDataProducts');
        TotalSelected =  component.get('v.intValueSelected');
         
         for (var e = 0; e < valProduct.length; e++){
             if(valProduct[e].label == NameSelected){
                 console.log('TotalSelected : '+TotalSelected);          
                 alert(TotalSelected +'--'+ maxSelected);
                 if(parseInt(TotalSelected) == parseInt(maxSelected)){
                     //alert(valueSelected);
                     
                     component.set('v.strTypeAlert','slds-notify slds-notify_toast slds-theme_success');
                     component.set('v.strTextAlert','Se agregaron los productos de : ');
                     document.getElementById(valProduct[e].label).style.display = "inline";     
                     component.set('v.intValueSelected',parseInt(TotalSelected)- parseInt(valueSelected));  
                     component.set('v.intValueSelected',0);
                     document.getElementById(idSelected).checked = true;
                     
                 }
                 if (parseInt(TotalSelected) < parseInt(maxSelected)){
                     //alert(valueSelected);
                     component.set('v.strTypeAlert','slds-notify slds-notify_toast slds-theme_warning');
                     component.set('v.strTextAlert','Falta agregar productos de : ');
                     document.getElementById(valProduct[e].label).style.display = "inline";  
                     document.getElementById(idSelected).checked = true;             
                 }
                 if(parseInt(TotalSelected) > parseInt(maxSelected) ){
                     //alert(valueSelected);
                     component.set('v.strTypeAlert','slds-notify slds-notify_toast slds-theme_error');
                     component.set('v.strTextAlert','No debe seleccionar mas de : '+maxSelected + ' Para el producto : ');
                     document.getElementById(valProduct[e].label).style.display = "inline"; 
                     component.set('v.intValueSelected',parseInt(TotalSelected)- parseInt(valueSelected));
                         
                 }
             }
         }
    },*/
    
  
    
    limitNum: function (component, event) {
        //******************Start to check the checkbox********************************

        var IdSelected = event.target.id;    
        var NameSelected = event.target.name;        
        var valueSelected = Number(event.target.value);        
        var lstItemComboSelected =component.get('v.lstComboSelected'); 
        var lstItemComboSelectedComp =component.get('v.lstItemComboSelectedComplete'); 

        var NameSelectedCombo = IdSelected.split("-"); 
        if(valueSelected > 0){
        	
            document.getElementById(NameSelected).checked = true; 
            document.getElementById(IdSelected).disabled = false;
            
        }else{
        	
            document.getElementById(NameSelected).checked = false; 
            //document.getElementById(IdSelected).disabled = true;
            //document.getElementById(IdSelected).value = '0';
            
        }
   
        //Valida boton para agregar combos al aVF
        lstItemComboSelected.push(NameSelectedCombo[1]+':'+valueSelected);

        for(var i = 0; i < lstItemComboSelected.length; i++){	
        	var ItemComboSelected = lstItemComboSelected[i].split(":");   
	        
	        if(document.getElementById(NameSelected).checked){
		    	   
		    	//   if(lstItemComboSelected.includes(NameSelectedCombo[1]+':'+ItemComboSelected[1])){    		
			        	
			        	lstItemComboSelected.remove(NameSelectedCombo[1]+':'+ItemComboSelected[1]);
			        	lstItemComboSelected.remove(NameSelectedCombo[1]+':'+valueSelected);
			        	lstItemComboSelected.push(NameSelectedCombo[1]+':'+valueSelected);
			        
			        	component.set('v.lstComboSelected',lstItemComboSelected);   		
			        	component.set('v.lstItemComboSelectedComplete',lstItemComboSelected);
		
			        	console.log('COMBO	lstItemComboSelected : ' +lstItemComboSelected );
      		
	        	
	        }else{
	        	lstItemComboSelected.remove(NameSelectedCombo[1]+':'+ItemComboSelected[1]);
	        	lstItemComboSelected.remove(NameSelectedCombo[1]+':'+valueSelected); 
	        	component.set('v.lstComboSelected',lstItemComboSelected);
	        	component.set('v.lstItemComboSelectedComplete',lstItemComboSelected);
	        	
	        	console.log('CKECKED FALSE : '+ component.get('v.lstItemComboSelectedComplete'));
	        }
        }
      	console.log('CKECKED TRUE 1 : '+ component.get('v.lstComboSelected'));
      	console.log('CKECKED TRUE 2: '+ component.get('v.lstItemComboSelectedComplete'));
      	
      	

        //ACTIVA O DESACTIVA BOTON DE AGREGAR COMBOS
        if(component.get('v.lstComboSelected').length > 0 ){
        	component.set('v.blnActivebuttonAddCombo',false);
        }else{
        	component.set('v.blnActivebuttonAddCombo',true);     
        }
       
        //******************End to check the checkbox********************************

        //******************************************        
        var max = parseInt(event.target.max);

        if(parseInt(event.target.value.substring(0, 1)) == 0){
            event.target.value = event.target.value.substring(1, event.target.value.length);
        }         

        if(event.target.value == '' || event.target.value === null || event.target.value === undefined){
            event.target.value = '';
        }

        if(parseInt(event.target.value) >= max){
            event.target.value = max;
        }        
        
        //******************************************
    },

    emptyNum: function (component, event) {
        var min = Number(event.target.min);

        if(event.target.value == '' || event.target.value === null || event.target.value === undefined || event.target.value.substring(0,1) == '-'){
            event.target.value = min;
        }
    },
    
	ProcessComboToVF: function (component, event) {		
        var strIdCombo = component.get('v.strIdCombo');    
		var lstRecordsComboQuantity = component.get('v.lstComboSelected');
		var lstRecordsCombo = component.get('v.lstValuesSendVF');
		var IdcomboBlock = '';
		var lstRecordsComboAcum = component.get('v.lstRecordsComboAcumulatedCompleted');
        var counterComb = 1;
        var combinationVal_lst = lstRecordsComboQuantity[0].split(":");
        var inputId = strIdCombo+'-'+combinationVal_lst[0]+'-';
        var idMaxByAvail_Str = strIdCombo+'-'+combinationVal_lst[0];
        var maxByAvailValue_Int = parseInt(document.getElementById(idMaxByAvail_Str).max);
        var currentValueInput_Int = 0;
        var newMaxValue = 0;
        var oldUsedCombos = 0;        
                
        oldUsedCombos = parseInt(document.getElementById(inputId).htmlFor);
        currentValueInput_Int = parseInt(document.getElementById(idMaxByAvail_Str).value);        
        console.log('oldUsedCombos : '+oldUsedCombos);
        console.log('currentValueInput_Int : '+currentValueInput_Int);    
        document.getElementById(inputId).innerHTML = 0;
        document.getElementById(inputId).innerHTML = currentValueInput_Int + oldUsedCombos;
        newMaxValue = maxByAvailValue_Int - currentValueInput_Int;
        newMaxValue = newMaxValue <= 0 ? 0 : newMaxValue;
        document.getElementById(idMaxByAvail_Str).max = newMaxValue;    
        console.log('newMaxValue : '+newMaxValue);        
                
        if(lstRecordsComboAcum.length > 0){                        
            var comboContained = false;
            var counterCombination = [];

            for(var dataAcumComplete of lstRecordsComboAcum){
                if(dataAcumComplete.includes(combinationVal_lst[0])){
                    comboContained = true;                    
                    var dataAcumComplete_lst = dataAcumComplete.split(":");
                    var currentValue_Str = dataAcumComplete_lst[4].substr(1,dataAcumComplete_lst.length);
                    currentValue_Str = parseInt(currentValue_Str);
                    counterCombination.push(currentValue_Str);                    
                }
            }

            counterComb = comboContained ? Math.max(...counterCombination) : counterComb;
            console.log('counterComb final = ' + counterComb);

            if(comboContained){
                for(var dataAcumComplete of lstRecordsComboAcum){
                    if(dataAcumComplete.includes(combinationVal_lst[0])){                        
                        var dataAcumComplete_lst = dataAcumComplete.split(":");
                        counterComb = parseInt(++counterComb);
                        lstRecordsComboQuantity[0] = lstRecordsComboQuantity[0] + ':C' + counterComb;
                    }
                }    
            }else{
                lstRecordsComboQuantity[0] = lstRecordsComboQuantity[0] + ':C' +counterComb;
            }              
        }else{
            lstRecordsComboQuantity[0] = lstRecordsComboQuantity[0] + ':C' +counterComb;
        }

		for(var data1 of lstRecordsComboQuantity){
			var RecordsComboQuantity = data1.split(":");			
			for(var data of lstRecordsCombo){
				var ComboSelectedToVF_Str = data.split(":");								
				if(lstRecordsComboAcum.length > 0){					
					for(var dataAcumComplete of lstRecordsComboAcum){
						var ComboSelectedComplete_Str = dataAcumComplete.split(":");						
						if(dataAcumComplete.includes(combinationVal_lst[0]) && component.get('v.strComboType')=='Specific'){
							if(ComboSelectedComplete_Str[4] != RecordsComboQuantity[2]){
								lstRecordsComboAcum.remove(ComboSelectedComplete_Str[0]+':'+ComboSelectedComplete_Str[1] +':'+ ComboSelectedComplete_Str[2]+':'+ComboSelectedComplete_Str[3]+':'+ComboSelectedComplete_Str[4]);	
								document.getElementById(strIdCombo+'-'+RecordsComboQuantity[2]).max=0;
							}						
						}
						component.set('v.intcombinationSum',parseInt(ComboSelectedComplete_Str[4].substring(1)));
						lstRecordsComboAcum.remove(ComboSelectedToVF_Str[0]+':'+ComboSelectedToVF_Str[1] +':'+ ComboSelectedToVF_Str[2]+':'+RecordsComboQuantity[1]+':'+RecordsComboQuantity[2]);						
						lstRecordsComboAcum.push(ComboSelectedToVF_Str[0]+':'+ComboSelectedToVF_Str[1] +':'+ ComboSelectedToVF_Str[2]+':'+RecordsComboQuantity[1]+':'+RecordsComboQuantity[2]);
						component.set('v.lstRecordsComboAcumulatedCompleted',lstRecordsComboAcum);			
					}									
				}else{
					lstRecordsComboAcum.push(ComboSelectedToVF_Str[0]+':'+ComboSelectedToVF_Str[1] +':'+ ComboSelectedToVF_Str[2]+':'+RecordsComboQuantity[1]+':'+RecordsComboQuantity[2]);	
					component.set('v.lstRecordsComboAcumulatedCompleted',lstRecordsComboAcum);
				}				
				IdcomboBlock= component.get('v.strIdCombo') +'-'+ComboSelectedToVF_Str[2];
				document.getElementById(IdcomboBlock).disabled = true;		
			}		
			component.set('v.lstComboSelected',[]);  
		}

        if(document.getElementById(idMaxByAvail_Str).max == 0){
            document.getElementById(strIdCombo).disabled = true;
        }
		
        var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        myEvent.setParams({
        	"lstAggregateRecordVF":component.get('v.lstRecordsComboAcumulatedCompleted'),
        	"lstAllSelectecBK" : component.get('v.lstItemComboSelectedComplete'),
        	"blnIsDeleted" :component.get('v.isDeletedCombo') 
        });    
         
        myEvent.fire();
         component.set('v.isOpenALerts',true);
         
         window.setTimeout(
    		$A.getCallback(function() {
    			component.set('v.isOpenALerts',false);
    		}), 2000
    	)
        document.getElementById(idMaxByAvail_Str).value=0;
        component.set('v.blnActivebuttonAddCombo',true);
	},
	  CleanComboToVF: function (component, event, helper) {
	
    	var lstRecordsCombo = component.get('v.lstCompleteComboSelected');   	
    	var idDeleteSelect = component.get('v.strIdComboDeleteVF');
    	var lstRecordsComboBK = component.get('v.lstItemComboSelectedComplete');   	
    	
    	
    	
    	for(var f = 0; f < lstRecordsCombo.length; f++){
    		var ComboSelectedToVF_Str = lstRecordsCombo[f].split(":");
    		if(idDeleteSelect == ComboSelectedToVF_Str[2]){
    		
    			lstRecordsComboBK.remove(ComboSelectedToVF_Str[2]+':'+ComboSelectedToVF_Str[3]);
    			
    			component.set('v.lstItemComboSelectedComplete',lstRecordsComboBK);
    	
    			document.getElementById(event.target.id+'-'+event.target.name).value = 0; 
    			document.getElementById(event.target.id+event.target.name).checked = false; 
    		}
    	}
  
    	console.log('SALIO 3 FOR ');
    	console.log('*************************** START **********************');
    	console.log(component.get('v.lstCompleteComboSelected'));
    	console.log(component.get('v.lstItemComboSelectedComplete'));
    	//component.set('v.lstItemComboSelectedComplete',lstRecordsCombo);
    	console.log('*************************** FINISH **********************');

    	component.set('v.isDeletedCombo',true);
    	var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        myEvent.setParams({ 
        	"lstAggregateRecordVF":component.get('v.lstCompleteComboSelected'),
        	"lstAllSelectecBK" : component.get('v.lstItemComboSelectedComplete'),
        	"blnIsDeleted" :component.get('v.isDeletedCombo'),
        	"strIdIsDeleted" :idDeleteSelect
        }); 
        myEvent.fire();
         component.set('v.isOpenALertsComboDeleted',true);
         window.setTimeout(
    		$A.getCallback(function() {
    			component.set('v.isOpenALertsComboDeleted',false); 
    		}), 5000
    	)
    	
         //for(var i = 0; i <= lstRecordsCombo.length; i++){
    	 for(var data of lstRecordsCombo){
	    	var ComboSelectedToVF_Str = data.split(":");
	    	if(idDeleteSelect == ComboSelectedToVF_Str[2]){
		    	lstRecordsCombo.remove(ComboSelectedToVF_Str[0]+':'+ComboSelectedToVF_Str[1]+':'+ComboSelectedToVF_Str[2]+':'+ComboSelectedToVF_Str[3]);
		    	component.set('v.lstCompleteComboSelected',lstRecordsCombo);
		    		var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
		    		myEvent.setParams({ 
		    			"lstAggregateRecordVF":component.get('v.lstCompleteComboSelected')
		    		}); 
		    		myEvent.fire();
	    	}
    	}
    	component.set('v.isDeletedCombo',false);
    	
    },	
    DeleteCombinationEvt: function(component, event, helper){
    	var Combination = event.getParam("StrCombinationDelete");
    	var numCombo = event.getParam("StrNumberCombo");
    	var quantityCombo  = event.getParam("StrquantityCombo");
    	var lstRecordsComboAcum = component.get('v.lstRecordsComboAcumulatedCompleted');
    
        for(var dataAcumCompleteDelete of lstRecordsComboAcum){
        	var CombinationDelete = dataAcumCompleteDelete.split(":");	
        	if(CombinationDelete[2] == numCombo && CombinationDelete[4] == Combination){
        		lstRecordsComboAcum.remove(CombinationDelete[0]+':'+CombinationDelete[1] +':'+ CombinationDelete[2]+':'+CombinationDelete[3]+':'+CombinationDelete[4]);
        		component.set('v.lstRecordsComboAcumulatedCompleted',lstRecordsComboAcum);		
        	}
        }
        
        var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        myEvent.setParams({ 
        	"lstAggregateRecordVF":component.get('v.lstRecordsComboAcumulatedCompleted'),
        	"blnExistCombo": true
        }); 
        myEvent.fire();
        component.set('v.isOpenDeleteCombo',false);		
        
    }, 
    
})