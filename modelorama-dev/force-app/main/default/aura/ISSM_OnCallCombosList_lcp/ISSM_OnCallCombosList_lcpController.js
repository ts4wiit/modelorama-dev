({
    init: function (component, event, helper) {
    	helper.getDataCombo(component, event);
    },
    
    getSelectedName: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        // for (var i = 0; i < selectedRows.length; i++){
        //    alert('You selected: ' + selectedRows[i].Name);
        // }
    },

   /* ChangeQuantity: function (component, event, helper) {
    	helper.getValuesSelected(component, event, helper);  
    },*/
    
    openDetailCombo: function(component, event, helper) {
	   try{ 
	   		var idSelected = event.target.id;
	   		var valueSelected = event.target.value;
	   		var nameButton = event.target.name;
	   		
	   		component.set("v.isOpenALerts", false);
		    component.set("v.strComboType", valueSelected);
		    component.set("v.strIdCombo", idSelected);
		    component.set("v.comboName_Str",nameButton);
		    component.set("v.isOpen", true);
		    document.getElementById(idSelected+'-'+nameButton).value='0';
		    
		    
	   	}catch(e) {
		   console.log(e);
	   	}
    },

    limits: function(component, event, helper){    	
    	helper.limitNum(component, event);
    },

    emptyValue: function(component, event, helper){    	
    	helper.emptyNum(component, event);
    },

    checkCheckbox: function(component, event, helper){
    	helper.checkCheckbox(component, event);
    },
   
	closeModel: function(component, event, helper) {		
        //Cierra modal cuando se da clic al boton del componente hijo agregar 
		component.set("v.isOpen", false); 
		component.set("v.isOpenDeleteCombo", false); 
		//component.set("v.isOpen", event.getParam("blnCloseModal"));
		
		//component.set("v.lstValuesSendVF", event.getParam("lstAggregateRecord"));
	},
	closeModelAndSendDataVF: function(component, event, helper) {		
		component.set("v.isOpen", false); 
		component.set("v.lstValuesSendVF", event.getParam("lstAggregateRecord"));
		component.set("v.intMaxCombinationMKCHILD", event.getParam("qtyMaxInput_Int"));
        // alert(component.get("v.intMaxCombinationMKCHILD"));	 
		var myEvent = $A.get("e.c:ISSM_OnCallListenerVF_evt");
        myEvent.setParams({"blnExistCombo":true});
        myEvent.fire();
	},
	
	AddCombosVF: function(component, event, helper) {	
       helper.ProcessComboToVF(component, event);
	},
	CleanCombosVF: function(component, event, helper) {	
		component.set('v.comboNamePassComponent',event.target.name); 
		component.set('v.isOpenDeleteCombo',true); 
		
		//component.set('v.strIdComboDeleteVF', event.target.name); 
		//helper.CleanComboToVF(component, event, helper);
	},
	handleComponentEvent: function(component, event, helper) {
		helper.DeleteCombinationEvt(component, event, helper);
	}
	
	
})