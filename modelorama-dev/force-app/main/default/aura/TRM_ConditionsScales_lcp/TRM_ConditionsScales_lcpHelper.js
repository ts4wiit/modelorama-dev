({
    //Create instance of record 
    createObjectData: function(component) {
        // get the scaleList from component and add(push) New Object to List  
        var ExtKey = component.get('v.ConditionProduct');
        var RowItemList = component.get("v.scaleList");
        var intLine = (RowItemList.length == 0) ? 2 : 1;
        for(var i = 0; i < intLine; i++){
            RowItemList.push({ 
            'TRM_Quantity__c':          RowItemList.length > 0 ? null : Number($A.get("$Label.c.TRM_QuantittyInit")),  
            'TRM_AmountPercentage__c':  RowItemList.length > 0 ? null : Number(ExtKey[0].DecAmount),
            'TRM_ConditionRecord__c':   ExtKey[0].ObjProducts.Id
            }); 
        }
        component.set("v.scaleList", RowItemList);
    },

    //function for check if null values
    validateRequired: function(component, event) {
        var isValid = true;
        var scaleRows = component.get("v.scaleList");
        for (var i = 0; i < scaleRows.length; i++) {
            if (Number(scaleRows[i].TRM_Quantity__c) <= 0 || Number(scaleRows[i].TRM_AmountPercentage__c) <= 0) {
                isValid = false;
                this.showToast("error", $A.get("$Label.c.TRM_Error"), $A.get("$Label.c.TRM_Msg16"));
            }
        }
        return isValid;
    },

    // Match the scales to the corresponding condition
    includeRecords: function(component) {
        var lstCndt = component.get('v.lstConditionScales');
        var ExtKey = component.get('v.ConditionProduct');
        var RowItemList = component.get("v.scaleList");

        for(var i=0; i<lstCndt.length;i++){
            var Amount = (lstCndt[i].TRM_Quantity__c == $A.get("$Label.c.TRM_QuantittyInit")) ? ExtKey[0].DecAmount : lstCndt[i].TRM_AmountPercentage__c; 
            if(lstCndt[i].TRM_ConditionRecord__c == ExtKey[0].ObjProducts.Id){    
                RowItemList.push({
                    'TRM_Quantity__c':          Number(lstCndt[i].TRM_Quantity__c),
                    'TRM_AmountPercentage__c':  Number(Amount),
                    'TRM_ConditionRecord__c':   ExtKey[0].ObjProducts.Id
                });
            }
        }
        component.set("v.scaleList", RowItemList);
        if(RowItemList.length==0){
            this.createObjectData(component);
        }
    },

    // function for delete the row scale
    removeRow: function(component, event) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.scaleList");
        var AllRowsList2 = component.get("v.lstConditionScales");
        AllRowsList.splice(index, 1);
        // AllRowsList2.splice(index, 1);

        if(AllRowsList.length == 1){
            var id = AllRowsList[0].TRM_ConditionRecord__c;
            AllRowsList2.slice().reverse().forEach(function(item, index, object) {
                if (item.TRM_ConditionRecord__c === id) {
                    AllRowsList2.splice(object.length - 1 - index, 1);
                }
            });

            AllRowsList.splice(0,1);
        }

        component.set("v.scaleList", AllRowsList);
        component.set('v.lstConditionScales',AllRowsList2);
    },

    //Show Alert
    showToast: function (type, title, msg) {
        var resultToast = $A.get("e.force:showToast");
        console.log('resultToast ',resultToast);
        resultToast.setParams({
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultToast.fire();
    },
})