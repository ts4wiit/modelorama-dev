({
    //method that is executed when the process starts
    doInit: function(component, event, helper) { 
        helper.includeRecords(component);
    },
    
    // function for add Records in the list and past for event
    Save: function(component, event, helper) {
        if (helper.validateRequired(component, event)) {
            component.getEvent("AddScaleEvt").setParams({"records" : component.get("v.scaleList") }).fire();
            component.set("v.blnAdd", false);
        }
    },
 
    // function for create new object Row in Condition List 
    addNewRow: function(component, event, helper) {
        helper.createObjectData(component);
    },
 
    // function for delete the row scale
    removeDeletedRow: function(component, event, helper) {
        helper.removeRow(component,event);
    },

    //function for close modal
    closeModal: function(component, event, helper) { 
      component.set("v.blnAdd", false);
    },
})