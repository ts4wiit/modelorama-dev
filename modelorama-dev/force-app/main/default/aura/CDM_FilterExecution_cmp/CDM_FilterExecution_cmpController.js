({
	doInit: function(component, event, helper) {
    var action = component.get("c.getRules");
    // Add callback behavior for when response is received
    action.setCallback(this, function(response) {
	//var state = response.getState();
    //alert("consulta"+ state);
    // if (component.isValid() && state === "SUCCESS") {
        component.set("v.Rules", response.getReturnValue());
    //}
    //else {console.log("Failed with state: " + state);}
    });
    $A.enqueueAction(action); 
        
    var locked = component.get("c.getNewRecords");
    locked.setCallback(this, function(response) {
        if(response.getReturnValue()>0){
            component.set("v.locked", true);
        }else{
            component.set("v.locked", false);
        }
    	
    });
    $A.enqueueAction(locked);
        
    var totals = component.get("c.getTotalTemp");
    totals.setCallback(this, function(response) {
    	component.set("v.Totaltemp", response.getReturnValue());
    });   
	$A.enqueueAction(totals);
        
    var offices = component.get("c.getSalesOff");
    offices.setCallback(this, function(response) {
    	component.set("v.Offices", response.getReturnValue());
    });   
	$A.enqueueAction(offices);
   
    var Groups = component.get("c.getKTOKD");
    Groups.setCallback(this, function(response) {
    	component.set("v.Groups", response.getReturnValue());
   	});   
	$A.enqueueAction(Groups);
        
    var Groups = component.get("c.getExpenseDetails");
    Groups.setCallback(this, function(response) {
    	component.set("v.lstExpenseData", response.getReturnValue());
   	});   
	$A.enqueueAction(Groups);
        
    var Orgs = component.get("c.getSalesOrg");
    Orgs.setCallback(this, function(response) {
    	component.set("v.SalesOrg", response.getReturnValue());
    });   
	$A.enqueueAction(Orgs);
        
    var Sec = component.get("c.getSector");
    Sec.setCallback(this, function(response) {
    	component.set("v.Sectors", response.getReturnValue());
    });   
	$A.enqueueAction(Sec);
        
    var channel = component.get("c.getChannel");
    channel.setCallback(this, function(response) {
    	component.set("v.Channels", response.getReturnValue());
    });   
	$A.enqueueAction(channel);
    
},

    myAction : function(component, event, helper) {
		//var a = component.get('c.showToast');
        var action = component.get("c.ExecuteProcessRules");
        var parm = component.get("v.limitQry");
        var org = component.find("org").get("v.value");
        var off = component.find("off").get("v.value");
        var chan = component.find("cha").get("v.value");
        var sec = component.find("sec").get("v.value");
        //var gp = component.find("gp").get("v.value");
        var gp = component.get("v.temporal");
        console.log(' Valores ingresados '+ gp);
        action.setParams({ 
               lmtQry : parm, 
               KTOKD  : gp, 
               VKORG  : org, 
               VKBUR  : off, 
               SPART  : sec, 
               VTWEG  : chan 
                         });
        
        action.setCallback(this, function(response) {
           //var state = response.getState();
            //console.log('state = '+ state);
           //if (component.isValid() && state === "SUCCESS") {
               var result = response.getReturnValue();
             //  console.log('var  = '+result);
             component.set("v.isOpen", false);
           });
        //$A.enqueueAction(a);
   			$A.enqueueAction(action);
        
         
         

},	myAction2 : function(component, event, helper) {
    	var a = component.get('c.showToast');	
    swal({ 
                title: $A.get("$Label.c.CDM_Relate"),
          		text:  $A.get("$Label.c.CDM_AlertText6"),
          		type: "info",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_Cancel"),
          		confirmButtonColor: "#0e84d3",
          		confirmButtonText: $A.get("$Label.c.CDM_Continue"),
            	closeOnConfirm: true },
             function(){ 
              	var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
                 $A.enqueueAction(a);
                var action = component.get("c.ExecuteRelationships");
        		action.setCallback(this, function(response) {
           		var state = response.getState();
            	console.log('state = '+ state);
           			if (component.isValid() && state === "SUCCESS") {
               			var result = response.getReturnValue();
               			console.log('var  = '+result);
           			}});
   				
                $A.enqueueAction(action);
         		
         			
             }  
      	);

},	myAction3 : function(component, event, helper) {
	var a = component.get('c.showToast');
    swal({ 
                title: $A.get("$Label.c.CDM_Reprocess"),
          		text: $A.get("$Label.c.CDM_AlertText7"),
          		type: "info",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_Cancel"),
          		confirmButtonColor: "#0e84d3",
          		confirmButtonText: $A.get("$Label.c.CDM_Continue"),
            	closeOnConfirm: true },
             function(){ 
              	var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
                 $A.enqueueAction(a);
                var action = component.get("c.ExecuteReprocessRules");
        		action.setCallback(this, function(response) {
           		var state = response.getState();
            	console.log('state = '+ state);
           			if (component.isValid() && state === "SUCCESS") {
               			var result = response.getReturnValue();
               			console.log('var  = '+result);
           			}});
   				 
                 $A.enqueueAction(action);
         		
         		
             }
        );	
    
},	myAction4 : function(component, event, helper) {
	var a = component.get('c.showToast');	
    swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle5"),
          		text: $A.get("$Label.c.CDM_AlertText8"),
          		type: "warning",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_Cancel"),
          		confirmButtonColor: "#c62d2d",
          		confirmButtonText: $A.get("$Label.c.CDM_Continue"),
            	closeOnConfirm: true },
             function(){
                var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
              	$A.enqueueAction(a);
                var action = component.get("c.ExecuteDeleteProcess");
        		action.setCallback(this, function(response) {
           		var state = response.getState();
            			console.log('state = '+ state);
           		if (component.isValid() && state === "SUCCESS") {
              		 var result = response.getReturnValue();
               			console.log('var  = '+result);
           		}});
   				
                $A.enqueueAction(action);
         		
         		  
             }  
                );
    
    	

},	showToast : function(component, event, helper) {
    var toastEvent = $A.get("e.force:showToast");
	toastEvent.setParams({
    		title: $A.get("$Label.c.CDM_AlertTitle6"),
    		message: $A.get("$Label.c.CDM_AlertText9"),
    		type: "success"
		});
		toastEvent.fire();
        var a = component.get('c.closeModel');
         $A.enqueueAction(a);

},	onChangeVKORG :	function(component, event, helper) {
    	var selected = component.find("org").get("v.value");
	    var dir = component.get("c.getSalesOffOnChange");
    	dir.setParams({
        org : selected
    				});

        dir.setCallback(this,function(a){
            component.set("v.Offices",a.getReturnValue());
        });
        $A.enqueueAction(dir);
    
    	var update = component.get('c.updateTotalTempforVKORG');
         $A.enqueueAction(update);
    
},	updateTotalTemp : function(component, event, helper) {
		var action = component.get("c.updateTotal");
        var parm = component.get("v.limitQry");
        var org = component.find("org").get("v.value");
        var off = component.find("off").get("v.value");
        var chan = component.find("cha").get("v.value");
        var sec = component.find("sec").get("v.value");
        //var gp = component.find("gp").get("v.value");
        var gp = component.get("v.temporal");
        console.log(' Valores ingresados '+ parm);
        action.setParams({ 
               KTOKD  : gp, 
               VKORG  : org, 
               VKBUR  : off, 
               SPART  : sec, 
               VTWEG  : chan 
                         });
        
       action.setCallback(this,function(response){
            component.set("v.Totaltemp",response.getReturnValue());
        });
        $A.enqueueAction(action);

},	updateTotalTempforVKORG : function(component, event, helper) {
		var action = component.get("c.updateTotal");
        var parm = component.get("v.limitQry");
        var org = component.find("org").get("v.value");
        var chan = component.find("cha").get("v.value");
        var sec = component.find("sec").get("v.value");
        //var gp = component.find("gp").get("v.value");
        var gp = component.get("v.temporal");
        console.log(' Valores ingresados '+ parm);
        action.setParams({ 
               KTOKD  : gp, 
               VKORG  : org, 
               VKBUR  : null, 
               SPART  : sec, 
               VTWEG  : chan 
                         });
        
       action.setCallback(this,function(response){
            component.set("v.Totaltemp",response.getReturnValue());
        });
        $A.enqueueAction(action);

},
    handleComponentEvent : function(cmp, event) {
        var temporal = event.getParam("values");
console.log("END = "+ temporal);
        // set the handler attributes based on event data
        cmp.set("v.temporal", temporal);
        var update = cmp.get('c.updateTotalTempforVKORG');
         $A.enqueueAction(update);
        
},	afterScripts : function(component, event, helper) {
		
},	executeProcess : function(component, event, helper) {
    var a = component.get('c.showToast');
    var action = component.get("c.getRulesApply");
    var bln = false;
    var rulesActived;
    action.setCallback(this, function(response) {
        rulesActived= response.getReturnValue();
        //console.log('size = '+rulesActived.length); 
        if(rulesActived.length==0){
           console.log('r1 = '+rulesActived); 
            swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle1"),
          		text: $A.get("$Label.c.CDM_AlertText1"),
          		type: "error"
             		}  
                );
        }  else if(rulesActived.length>=10){
			console.log('r22 = '+rulesActived); 
            bln=true;
            swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle2"),
          		text: $A.get("$Label.c.CDM_AlertText2"),
          		type: "error"
             		}  
                );            
        }
        for(var i=0;i<rulesActived.length; i++){
           console.log('r333 = '+rulesActived[i]); 
        
            if((rulesActived[i]=='R0005' || rulesActived[i]=='R0006' || rulesActived[i]=='R0007') && bln==false){
            swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle3"),
          		text: $A.get("$Label.c.CDM_AlertText3"),
          		type: "warning",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_No"),
          		confirmButtonColor: "#61B3C7",
          		confirmButtonText: $A.get("$Label.c.CDM_Yes"),
            	closeOnConfirm: true },
             function(){ 
                // window.location ="https://demoandroid-dev-ed.lightning.force.com/one/one.app#/sObject/Gestor__c/list?filterName=00B6A000002rwRYUAY";
             	var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
                 $A.enqueueAction(a);
                 var execute = component.get('c.myAction');
        		 $A.enqueueAction(execute); 
             		}  
                );
            } else if ((rulesActived[i]=='R0018' || rulesActived[i]=='R0022' || rulesActived[i]=='R0024' || rulesActived[i]=='R0025' || rulesActived[i]=='R0027' || rulesActived[i]=='R0028') && bln==false){
                swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle3"),
          		text: $A.get("$Label.c.CDM_AlertText4"),
          		type: "warning",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_No"),
          		confirmButtonColor: "#61B3C7",
          		confirmButtonText: $A.get("$Label.c.CDM_Yes"),
            	closeOnConfirm: true },
             function(){ 
                // window.location ="https://demoandroid-dev-ed.lightning.force.com/one/one.app#/sObject/Gestor__c/list?filterName=00B6A000002rwRYUAY";
             	var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
                 $A.enqueueAction(a);
                 var execute = component.get('c.myAction');
        		 $A.enqueueAction(execute); 
             		}  
                );
            } else if(bln==false){
                swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle3"),
          		type: "warning",
          		showCancelButton: true,
           		cancelButtonText: $A.get("$Label.c.CDM_No"),
          		confirmButtonColor: "#61B3C7",
          		confirmButtonText: $A.get("$Label.c.CDM_Yes"),
            	closeOnConfirm: true },
             function(){ 
                // window.location ="https://demoandroid-dev-ed.lightning.force.com/one/one.app#/sObject/Gestor__c/list?filterName=00B6A000002rwRYUAY";
             	var al = component.get('c.closeModel');
         		$A.enqueueAction(al);
                 $A.enqueueAction(a);
                 var execute = component.get('c.myAction');
        		 $A.enqueueAction(execute); 
                 //window.location.reload();
             		}  
                );
            }
    }
    });
    $A.enqueueAction(action); 
    
    //console.log('r = '+rulesActived);
        	

    },
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
       // var a = component.get('c.applyCSS');
         //$A.enqueueAction(a);
      var pend = component.get('v.locked');
        if(pend){
            swal({ 
                title: $A.get("$Label.c.CDM_AlertTitle4"),
          		text: $A.get("$Label.c.CDM_AlertText5"),
                type: "warning"
					 }
  
                );
        }
   },
    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
       // var a = component.get('c.revertCssChange');
        // $A.enqueueAction(a);
   }

})