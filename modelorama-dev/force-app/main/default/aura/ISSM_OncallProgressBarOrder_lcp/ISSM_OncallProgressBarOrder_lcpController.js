({
     doAction : function(component, event, helper) {      
        var params = event.getParam('arguments');
        if (params) {        
	        if(params.paramVal >= '0' &&  params.paramVal <= '70'){
	        	//component.set('v.styleBar', '#feda16');
	        	component.set('v.value', params.paramVal);
	        }else if(params.paramVal > '70' &&  params.paramVal <= '80'){
	        	//component.set('v.styleBar', '#f89201');
	        	component.set('v.value', params.paramVal);
	        }else if(params.paramVal > '80' &&  params.paramVal <= '100'){
	        	//component.set('v.styleBar', '#d34a28');
	        	component.set('v.value', params.paramVal);
	        }else if( params.paramVal > '100'){
	        	//component.set('v.styleBar', '#d34a28');
	        	component.set('v.value', '100');
	        }
        }
    }	
})