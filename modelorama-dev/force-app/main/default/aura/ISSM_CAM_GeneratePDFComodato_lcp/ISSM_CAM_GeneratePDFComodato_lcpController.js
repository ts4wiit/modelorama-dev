/* Component controller */
({  
    doInit: function(component, event, helper){
        helper.getRecord(component,event);
    },
    //CONFIRM ALERT
    confirmModal : function(component, event, helper){
        component.set('v.isGeneratePDF',false);
        component.set('v.isPDF',true);
    },
    closeModal: function(component, event, helper) {
        helper.redirectPage(component.get('v.recordId'));
        helper.showToast('success', $A.get("$Label.c.ISSM_CAM_ShowToastSuccess"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg01"));
    }
})