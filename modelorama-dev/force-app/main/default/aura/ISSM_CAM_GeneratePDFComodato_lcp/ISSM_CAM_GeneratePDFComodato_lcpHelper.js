({ 
    //Get record of clone
    getRecord: function(component,event){
        var action = component.get("c.getCaseForce");
        action.setParams({"idCaseForce" : component.get('v.recordId')});
        
        action.setCallback(this, function(resp){
            var state = resp.getState();
            if (state === "SUCCESS") {
                var caseForce = resp.getReturnValue();
                if(caseForce.ISSM_CAM_SAP_Order_Number__c !== undefined){
                    var Id  = caseForce.Id;
                    var url = '/apex/ISSM_CAM_PDFGenerator_pag?id='+Id;
                    component.set('v.url',url);
                    component.set('v.isGeneratePDF',true);   
                }else{
                    this.redirectPage(component.get('v.recordId'));
                    this.showToast('error',$A.get("$Label.c.ISSM_CAM_ShowToastMsg02"),$A.get("$Label.c.ISSM_CAM_ShowToastMsg03"));
                }
            }else{
                console.log('##error->getRecord##: ',JSON.stringify(resp.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    //Show Alert
    showToast: function (type, title, msg){
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "duration": 5000,
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultsToast.fire();
    },
    //Redirect to Page indicated for the RecordId
    redirectPage: function (recordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})