({
	//Render the list of materials
	renderPage: function(component) {
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.wrapperProd");
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        var pageNumber = component.get("v.pageNumber"); // obtiene el numero de pagina
        var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);//obtiene el numero de registros por pagina
        component.set("v.currentList", pageRecords);//setea los registros de acuerdo a la paginación
    },

    //confirms the selection or deselection of products
    confirmActions: function(component) {
        var lstAllProds = component.get('v.wrapperProd');
        var LstIds = [];
        for(var i=0; i<lstAllProds.length;i++){
            if(lstAllProds[i].IsSelected){
                LstIds.push('\''+lstAllProds[i].ObjProducts.Id+'\'');  
            } 
        }
    
        var compEvent = component.getEvent("confirmRemove");
        compEvent.setParams({"isExecute" : true,
                             "LstIds":     LstIds });
        compEvent.fire();
    },

    //Execute the query for material search
    execute : function(component,event){
        var MapProd = {};
        var lstIds  = [];
        var args    = event.getParam("arguments");
        
        if(args){
            var lstAllProds = args.productList;
            component.set("v.blnClean", true);
            
            for(var i=0;i<lstAllProds.length;i++){
                MapProd[lstAllProds[i].Id] = lstAllProds[i].ISSM_UnitPrice__c;
                lstIds.push('\''+lstAllProds[i].Id+'\''); 
            }

            var action = component.get("c.SearchProducts");
            action.setParams({"LstIds"     : lstIds,
                              "MapProd"    : MapProd});
            
            action.setCallback(this, function(resp){
                var state = resp.getState();
                if (state === "SUCCESS") {
                    var NewLstProd = resp.getReturnValue();
                    component.set('v.wrapperProd',NewLstProd);
                    this.renderPage(component);
                }else{
                    console.log('##error->searchProduct##: ',resp.getError());
                }
            });
            $A.enqueueAction(action);
        }
    },

    showToast: function (type, title, msg) {
        var resultToast = $A.get("e.force:showToast");
        console.log('resultToast ',resultToast);
        resultToast.setParams({
            "duration": Number($A.get("$Label.c.TRM_DurationToast")),
            "type":     type,
            "title":    title,
            "message":  msg
        });
        resultToast.fire();
    },
})