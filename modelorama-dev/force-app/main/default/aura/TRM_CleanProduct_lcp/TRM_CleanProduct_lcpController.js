({
    //Render the list of materials
	renderPage: function(component,helper,event) {
        var pageSize = component.get("v.pageSize");
        var records = component.get("v.wrapperProd");
        component.set("v.maxPage", Math.floor((records.length + (pageSize - 1)) / pageSize));
        var pageNumber = component.get("v.pageNumber"); 
        var pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
        component.set("v.currentList", pageRecords);
        // helper.renderPage(component);
    },

    //Cancel the process
    closeModel: function(component, event, helper) {
        var compEvent = component.getEvent("confirmRemove");
        compEvent.setParams({"isExecute" : false,
                             "LstIds":     []});
        compEvent.fire();
    },
 
    //confirms the selection or deselection of products
    confirmAction: function(component, event, helper) {
        helper.confirmActions(component);
    },

    //Execute the query for material search
    executeQuery : function(component,event,helper){
        helper.execute(component,event);
        helper.showToast("info", $A.get("$Label.c.TRM_Msg00"),$A.get("$Label.c.TRM_Msg29"));
    },

    //Select everything that is currently in the search
    selectAll: function(component, event, helper) {
        var BlnselectAll = component.find("checkAll").get("v.value");
        var LstPrd = component.get("v.wrapperProd");
        for(var i = 0; i < LstPrd.length; i++) {
            LstPrd[i].IsSelected = (BlnselectAll) ? true : false;
        }
        helper.renderPage(component);
    },
})