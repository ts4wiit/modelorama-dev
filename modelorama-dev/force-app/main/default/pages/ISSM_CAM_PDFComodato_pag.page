<!--******************************************************************************** 
    Company:            Avanxo México
    Author:             Oscar Alvarez Garcia
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Data and structure of the CONTRACT and promissory note

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    20-Octubre-2018    Oscar Alvarez Garcia           Creation
    ******************************************************************************* -->
<apex:page controller="ISSM_CAM_DataDisplay_ctr" renderAs="pdf" applyHtmlTag="false" applyBodyTag="false" showHeader="false" sidebar="false" standardStylesheets="false">

    <html>
    <head>
      <style>
        @page {
            size: letter;
            margin: 15mm;
            @top-right {
                content: "{!labelNumOfImpressions} {!numberOfImpressions}";
                color: #b0b6ba;
            }
            @bottom-center{
                content: counter(page);
            }
        }
        .page-break {
            display:block;
            page-break-after:always;
        }
    </style>
    </head>
    <body>
      <div class="page-break"> <!-- inicio <div >paginacion  CONTRATO-->
      <table  width="100%" style='font-size:12px'>
        <tr>
          <td width="17%" align="left">
            <apex:image value="{!URLFOR($Resource.ISSM_CAM_LogoGroupModel)}" style="float:right;width:80%; height:80px;"/>
          </td>
          <td  align="left">
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!nameCompany}" />
              <apex:outputText value="{!companyAddress}" />
              <apex:outputText value="{!postalCode}" />
              <apex:outputText value="RFC: {!rfc}" />
            </apex:panelGrid>
          </td>
        </tr>
  	</table>
    <table align="right" style='font-size:14px'>
        <tr >
          <td width="100%" align="right">
            <apex:panelGrid columns="1" width="100%">
              <b>{!typeDocument}</b> 
              <apex:outputText value="Fecha: {!dateOfAssignment}"/>
              <apex:outputText value="Cliente: {!objectCaseForce.ONTAP__Account__r.ONTAP__SAP_Number__c}"/>
            </apex:panelGrid>
          </td>
        </tr>
    </table>
    <br/><br/><br/><br/>
    <table style='font-size:14px'>
		    <tr>
          <td width="100%" align="justify">          
            <b>{!contractHeaderPart1} {!nameCompany} {!contractHeaderPart2} {!comodante}{!contractHeaderPart3} {!objectCaseForce.ONTAP__Account__r.ONTAP__LegalName__c}{!contractHeaderPart4}
            </b>                  
          </td>
		    </tr>
        <tr>                                          <!-- Inicia sección 1 -->
          <td width="100%" align="center">  
          <br/>      
            <b>{!declaracionTitle}</b>                  
          </td>
        </tr>
        <tr>
          <td width="100%" align="justify">     
          <apex:panelGrid columns="1" width="100%">
            <b>{!declacacionI}</b> 
            <apex:outputText value="{!declacacionIA}"/>
            <apex:outputText value="{!declacacionIB}"/>
            <apex:outputText value="{!declacacionIC}" />
          </apex:panelGrid>                 
          </td>
        </tr>
      </table>
      <br/>
     <apex:form >
     <apex:pagemessages />
     <apex:variable value="{!0}" var="index" />
       <apex:pageBlock >
           <apex:pageBlockTable value="{!assetCAM}" var="rowData" border="1px" align="center" style="font-size:12px">
              <apex:column >
                  <apex:facet name="header">
                  <apex:outputLabel value="{!colum1}" />
                  </apex:facet>
                  <apex:outputText >
                  <apex:variable value="{!index + 1}" var="index" />
                  {!index}
                  </apex:outputText>
              </apex:column>
              <apex:column >
                  <apex:facet name="header">
                    <apex:outputLabel value="{!colum2}"/>
                  </apex:facet>
                  {!rowData.ISSM_Asset__r.ISSM_Material_number__c}{!rowData.ISSM_Asset__r.ISSM_Asset_Description__c}
              </apex:column>
              <apex:column >  
                  <apex:facet name="header">
                    <apex:outputLabel value="{!colum3}"/>
                  </apex:facet> 
                  {!rowData.ISSM_Asset__r.ISSM_Provider_Serial_Number__c}
              </apex:column>
              <apex:column > 
                  <apex:facet name="header">
                    <apex:outputLabel value="{!colum4}"/>
                  </apex:facet> 
                  {!rowData.ISSM_Asset__r.Asset_number__c}
              </apex:column>
              <apex:column > 
                   <apex:facet name="header">
                    <apex:outputLabel value="{!colum5}"/>
                  </apex:facet> 
                  {!dateOfAssignment}
              </apex:column>
          </apex:pageBlockTable>
        </apex:pageBlock>
      </apex:form>
      <table style='font-size:14px'>
        <tr>
           <td width="100%" align="justify">     
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!declacacionID}"/>
              <br/>
              <b>{!declacacionII}</b> 
              <apex:outputText value="{!declacacionIIA1}"/>
              <apex:outputText value="{!declacacionIIA2}" />
              <apex:outputText value="{!declacacionIIA21}" />
              <apex:outputText value="{!declacacionIIBpart1} {!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c},  {!objectCaseForce.ONTAP__Account__r.ONTAP__Street_Number__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}, Estado {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c} {!declacacionIIBpart2}" />
              <apex:outputText value="{!declacacionIIC} {!rfcCustomer}. " />
              <apex:outputText value="{!declacacionIIDpart1} {!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Street_Number__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}, Estado {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c}, de {!declacacionIIDpart2}" />
              <apex:outputText value="{!declacacionIIEpart1} {!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c},  {!objectCaseForce.ONTAP__Account__r.ONTAP__Street_Number__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}, Estado {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c} {!declacacionIIEpart2} {!rfcCustomer}. " />
              <apex:outputText value="{!declacacionIIend} " />  
            </apex:panelGrid>                 
          </td>
        </tr>
        <tr>                                          <!-- Inicia sección 1 -->
          <td width="100%" align="center">          
            <b>{!clauseTitle}</b>                  
          </td>
        </tr>
        <tr>
          <td width="100%" align="justify">     
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!clause1Paragraph1}"/>
              <apex:outputText value="{!clause1Paragraph1_1}"/>
              <apex:outputText value="{!clause1Paragraph2}"/>
              <apex:outputText value="{!clause1Paragraph3}"/>
              <apex:outputText value="{!clause1Paragraph4}"/>
              <apex:outputText value="{!clause1Paragraph5}"/>
              <apex:outputText value="{!clause2}"/>
              <apex:outputText value="{!clause3}"/>
              <apex:outputText value="{!clause4}"/>
              <apex:outputText value="{!clause5}"/>
              <apex:outputText value="{!clause5Paragraph1}"/>
              <apex:outputText value="{!clause5Paragraph2}"/>
              <apex:outputText value="{!clause5Paragraph3}"/>
              <apex:outputText value="{!clause5Paragraph4}"/>
              <apex:outputText value="{!clause5Paragraph5}"/>
              <apex:outputText value="{!clause5Paragraph6}"/>
              <apex:outputText value="{!clause5Paragraph7}"/>
              <apex:outputText value="{!clause5Paragraph8}"/>
              <apex:outputText value="{!clause5Paragraph8i}"/>
              <apex:outputText value="{!clause5Paragraph9}"/>
              <apex:outputText value="{!clause5Paragraph9_1}"/>
              <apex:outputText value="{!clause6}"/>
              <apex:outputText value="{!clause7}"/>
              <apex:outputText value="{!clause8}"/>
              <apex:outputText value="{!clause9}"/>
              <apex:outputText value="{!clause10}"/>
              <apex:outputText value="{!clause11}"/>
            </apex:panelGrid>              
          </td>
        </tr>
        <tr>
          <td width="100%" align="left">
            <b>{!labelComodante}</b>
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!nameCompany}" />
              <apex:outputText value="{!companyAddress}" />
              <apex:outputText value="{!postalCode}" />
              <apex:outputText value="RFC: {!rfc}" />
            </apex:panelGrid>
          </td>
        </tr>
        <!-- <tr>
          <td width="100%" align="left">
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!avenueCorpo}" />
              <apex:outputText value="{!coloniaCorpo}" />
              <apex:outputText value="{!delegationCorpo}" />
              <apex:outputText value="{!postalCodeCorpo}, {!cityCorpo}" />
              <apex:outputText value="{!mentionCorpo}" />
            </apex:panelGrid>
          </td>
        </tr> -->
        <tr>
          <td width="100%" align="justify">
            <b>{!labelComodatario}</b>
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!objectCaseForce.ONTAP__Account__r.ONTAP__LegalName__c}" />
              <apex:outputText value="{!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c},  {!objectCaseForce.ONTAP__Account__r.ONTAP__Street_Number__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}, Estado {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c} " />
              <apex:outputText value="{!clause11Paragraph1}" />
            </apex:panelGrid>
          </td>
        </tr>
        <tr>
          <td width="100%" align="justify">
            <apex:panelGrid columns="1" width="100%">
              <apex:outputText value="{!clause12}" />
              <apex:outputText value="{!clause12Paragraph1} {!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c},  {!objectCaseForce.ONTAP__Account__r.ONTAP__Street_Number__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}, {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}, Estado {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c} el dia {!dateOfAssignment}" />
            </apex:panelGrid>
          </td>
        </tr>
      </table>
    </div> 
    <div class="page-break"> 
      <div style='font-size:14px' align="center">
        <br/><br/><br/><br/><br/>
        <b>{!labelNameCompany}</b>
        <p>{!nameCompany}</p>
      </div>

    <br/><br/><br/><br/><br/><br/>
    <table style='font-size:14px'  width="100%">
      <tr>
        <td width="50%">    
          <div align="center" >
              <HR style='width: 200px;'/>
              <!-- <b><apex:outputText value="{!comodante}"/></b> -->
              <p><apex:outputText value="{!legalRepresentative}"/></p>
          </div>              
        </td>
        <td width="50%">   
          <div align="center">
              <HR style='width: 200px;'/>
              <!-- <b><apex:outputText value="{!comodante2}"/></b> -->
              <p><apex:outputText value="{!legalRepresentative}"/></p>
          </div>                 
        </td>
      </tr>
    </table>
    <table style='font-size:14px'  width="100%">
      <tr>
        <td >    
          <div align="center" >
              <br/><br/>
              <b>El {!labelComodatario}</b>
              <br/><br/><br/><br/><br/>
              <HR style='width: 200px;'/>
              <b><apex:outputText value="{!objectCaseForce.ONTAP__Account__r.ONTAP__LegalName__c}"/></b>
          </div>              
        </td>
      </tr>
    </table>
    </div> <!-- div ultima pagina  CONTRATO-->

    <!--div ultima pagina PAGARÉ -->
    <div> 
       <table style='font-size:14px;border-collapse: collapse;' border = "0.5px solid #dddddd">
          <tr>
            <td align="justify">
              <table  width="100%"> 
                <tr>
                  <td width="45%"></td>
                  <td width="20%" align="left">{!promissoryNoteTitle}</td>
                </tr> 
                <tr>
                  <td width="20%"></td>
                  <td width="80%" align="right">{!promissoryNoteLabelBueno}{!promissoryNoteBueno}</td>
                </tr>
              </table>

              <table  width="100%"> 
                <tr>
                  <td width="100%" align="right">En {!postalCode} a {!dateOfAssignment}</td>
                </tr>
              </table>

              <table >  
                <tr>
                  <td align="justify">
                    <apex:panelGrid columns="1" width="100%">
                      <br/>
                      <apex:outputText value="{!promissoryNotePart1}" />
                      <br/>
                      <apex:outputText value="{!nameCompany}" />
                      <br/>
                      <apex:outputText value="{!promissoryNoteLabelBuenoletter} {!promissoryNoteBueno} {!promissoryNoteBuenoletter}" />
                      <br/>
                      <apex:outputText value="{!promissoryNotePart2} {!numberOfCoolers} {!promissoryNotePart3}" />
                      <br/>
                      <apex:outputText value="{!promissoryNotePart4}" />
                      <br/>
                      <apex:outputText value="{!promissoryNotePart51}" />
                      <br/>
                      <apex:outputText value="{!promissoryNotePart6}" />
                    </apex:panelGrid>
                 </td>
                </tr>
              </table>
              <table  width="100%">  
                <tr>
                  <td align="center"><br/>{!promissoryLabelDebtor}</td>
                </tr>
                <tr>
                  <td align="left">
                    <apex:panelGrid columns="1" width="100%">
                      <apex:outputText value="{!promissoryLabelName}: {!objectCaseForce.ONTAP__Account__r.ONTAP__LegalName__c}" />
                      <apex:outputText value="{!promissoryLabelDomicile}: {!objectCaseForce.ONTAP__Account__r.ONTAP__Street__c}, C.P. {!objectCaseForce.ONTAP__Account__r.ONCALL__Postal_Code__c}" />
                      <apex:outputText value="{!promissoryLabelColony}: {!objectCaseForce.ONTAP__Account__r.ONTAP__Municipality__c}" />
                      <apex:outputText value="{!promissoryLabelPopulation}: {!objectCaseForce.ONTAP__Account__r.ONTAP__Province__c}" />
                    </apex:panelGrid>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br/>
                      <pre>                    {!promissoryLabelFirm}</pre> 
                    <HR style='width: 350px;'/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
      </table>
    </div><!-- fin <div >paginacion --> 
  </body>
  </html>
</apex:page>