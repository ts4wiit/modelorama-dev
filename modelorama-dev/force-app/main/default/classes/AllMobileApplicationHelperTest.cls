/**
 * Test Class for AllMobileApplicationHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileApplicationHelperTest {

	/**
	 * Method to setup data.
	 */
	public static testMethod void setup() {

		//Create Application.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;

		//Update Application.
		objAllMobileApplication.Name = 'AUTOVENTA';
		objAllMobileApplication.AllMobileApplicationId__c = '2';
		update objAllMobileApplication;
	}
}