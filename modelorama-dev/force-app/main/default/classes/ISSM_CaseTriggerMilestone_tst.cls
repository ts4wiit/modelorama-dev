@isTest
private class ISSM_CaseTriggerMilestone_tst {
    
    static testMethod  void testUpdateTargetDateMilestone() {
        // Implement test code
        List<String> caseID_list = new List<String>();
        List<SlaProcess> lstSlaProcess = new List<SlaProcess> ();
        ISSM_TypificationMatrix__c  TypificationMatrix;
        User user;
        String SlaName ='';
         
        
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix2(true,'México - HMM - 0008','Modelorama','Modelorama empresarios','Mantenimiento general','Instalación eléctrica',null,null,'Queue',null,'00G36000002iqzz');
    
        Account acc = new account(name='Test Account');
        insert acc;
        lstSlaProcess =  [select id,name from SlaProcess limit 1];
        SlaName = lstSlaProcess[0].Name;
        Entitlement entl = new entitlement(name='Test Entilement', 
            accountid=acc.id,
            type='Gateway Support',
            StartDate=Date.valueof(System.now()),
            EndDate=Date.valueof(System.now()),
            SlaProcessid =  [select id from SlaProcess  where name =: SlaName ].Id);
        insert entl;
     
   
        Case testCaseMilestone = new Case();
            testCaseMilestone.Subject = 'Subject';
            testCaseMilestone.Description = 'Description';
            testCaseMilestone.Status = 'New';
            testCaseMilestone.ISSM_TypificationNumber__c = TypificationMatrix.Id;
            testCaseMilestone.EntitlementId = entl.Id;
        insert testCaseMilestone;
        caseID_list.add(testCaseMilestone.Id);
    
        Test.startTest();   
        ISSM_CaseTriggerMilestone_cls.UpdateTargetDateMilestone(caseID_list);
        Test.stopTest();  
    }
    
}