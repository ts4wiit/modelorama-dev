/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
public with sharing class ISSM_CaseTriggerMilestone_cls {
	
 /**
* @Hector Diaz
* @date 18-07-2017
* @description Método para consultar los milestona a partir del id del caso
* @String strIdCaso id del caso a consultar
**/
    @future
    public static void UpdateTargetDateMilestone(List<String> lstIds){
    	ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        System.debug('####FUTURO -- ISSM_CaseTriggerMilestone_cls.UpdateTargetDateMilestone');
        List<CaseMilestone> lstCaseMilestones = objCSQuerys.QueryCaseMilestoneLst(lstIds);
        Boolean flagUpdate= false;  
        List<Case> lstUpdateCase = new List<Case>();
        Case objCaseUpdate = new Case();
        for(CaseMilestone objCaseMilestone : lstCaseMilestones){
            if(objCaseMilestone.IsCompleted != true && objCaseMilestone.IsViolated != true){
                flagUpdate = true;
                objCaseUpdate.id = objCaseMilestone.CaseId;
                objCaseUpdate.DataTargetCaseMilestone__c = objCaseMilestone.TargetDate;
                lstUpdateCase.add(objCaseUpdate);
            }
        }

        if(flagUpdate){     
           try{
            ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger);
            Database.SaveResult[] srList = Database.update(lstUpdateCase, false);
           }catch(Exception e){
            System.debug('###### Exception : '+e);
           }   
        }
    }
}