/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller for process empty balance

    Information about changes (versions)
    ================================================================================================
    Number    Dates            Author                       Description          
    ------    --------         --------------------------   -----------------------------------------
    1.0       05-Agosto-2017   Luis Licona                  Class Creation
    ================================================================================================
****************************************************************************************************/
global with sharing class ISSM_EmptyBalanceProcess_cls { 
   
  public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
	public static WrpEmptyBalance getEmptyBalance(ONTAP__Order_Item__c[] LstEOI,ISSM_Bonus__c[] LstBns,String IdAcc, String IdOrder,ONTAP__Product__c[] LstProductEmB){
        EmptyBalanceB__c ObjEB          = new EmptyBalanceB__c();
        EmptyBalanceB__c[] LstEB        = new List<EmptyBalanceB__c>(); //Lst Current order
        EmptyBalanceB__c[] LstEBBonuses = new List<EmptyBalanceB__c>(); //Lst BonusEB
        EmptyBalanceB__c[] LstEBPending = new List<EmptyBalanceB__c>(); //Lst Historic order
        EmptyBalanceB__c[] LstAllEB     = new List<EmptyBalanceB__c>(); // Lst Current+Historic order
        EmptyBalanceB__c[] LstEBProductEmpty = new List<EmptyBalanceB__c>(); //Lst Current order
        WrapperEBG WrpEB;
        
        List<ONTAP__Product__c> lstProducts = new List<ONTAP__Product__c>(); 
        Set<String> setSkuProduct = new Set<String>();
        Set<String> setSkuProduct2 = new Set<String>();
        List<ONTAP__Order_Item__c> lstOrderItem = new List<ONTAP__Order_Item__c>();
     	Map<String,List<ONTAP__Order_Item__c>> mapOrderItemXSku = new Map<String,List<ONTAP__Order_Item__c>>();
        List<ISSM_ProductByPlant__c> lstProductByplant = new List<ISSM_ProductByPlant__c>(); 
		List<ONTAP__Order_Item__c> lstOrderI = new List<ONTAP__Order_Item__c>();
		
        Account ObjAcc      = CTRSOQL.getAccountbyId(IdAcc);
        Integer IntCD       = ObjAcc.ONTAP__EmptyLoanDays__c != null ? Integer.valueOf(ObjAcc.ONTAP__EmptyLoanDays__c) : 0;
        Datetime StrDate    = System.now();
        DateTime StrDateBon = Datetime.parse(StrDate.format())+1;
        //se suman los dias de credito a la fecha de vencimiento        
        DateTime DtDD = ObjAcc.ISSM_EmptyCreditQty__c ? Datetime.parse(StrDate.format())+IntCD+1 : Datetime.parse(StrDate.format())+1;
		Boolean isActiveIntegrationProxPlant = CTRSOQL.isSuggestedProdsActive(Label.ISSM_InteProductByPlant); 
		System.debug(loggingLevel.Error, '*** isActiveIntegrationProxPlant: ' + isActiveIntegrationProxPlant);
		if(isActiveIntegrationProxPlant){
            System.debug(loggingLevel.Error, '*** LstEOI: ' + LstEOI);
            System.debug(loggingLevel.Error, '*** LstEOI.size(): ' + LstEOI.size());
			for(ONTAP__Order_Item__c objEOI : LstEOI){            
				setSkuProduct.add(objEOI.ONCALL__OnCall_Product__c);
				setSkuProduct2.add(objEOI.ISSM_OrderItemSKU__c);
				System.debug('************** objEOI :  '+objEOI);
	        }
        	System.debug('************** setSkuProduct2 :  '+setSkuProduct2);
  		
  		
	        if(setSkuProduct.size() > 0 ){   	
	        	id RecordTypeIdProductByPlan_id =  Schema.SObjectType.ISSM_ProductByPlant__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProductByPlant').getRecordTypeId();
	       		id RecordTypeId_id =  Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId();
				System.debug(loggingLevel.Error, '*** RecordTypeId_id: ' + RecordTypeId_id);
                System.debug(loggingLevel.Error, '*** RecordTypeIdProductByPlan_id: ' + RecordTypeIdProductByPlan_id);
                System.debug(loggingLevel.Error, '*** ObjAcc: ' + ObjAcc);
                System.debug(loggingLevel.Error, '*** setSkuProduct: ' + setSkuProduct);
                lstProducts =  CTRSOQL.getEmptiesList(RecordTypeId_id,RecordTypeIdProductByPlan_id,ObjAcc,setSkuProduct,'ENVASE%');
				lstProductByplant = CTRSOQL.getProductByPlantEmp(lstProducts,setSkuProduct,'ENVASE%',ObjAcc.ISSM_DistributionCenterLU__c);
				
				System.debug('********** lstProducts : '+lstProducts); //producstos base de envace que estan en productbypant
				System.debug('********** lstProductByplant : '+lstProductByplant); //producstos base de envace que estan en productbypant
	        }
         
			ONTAP__Order_Item__c objEOI2 =  new ONTAP__Order_Item__c();
	        for(ONTAP__Order_Item__c objEOI : LstEOI  ){		
				for( ISSM_ProductByPlant__c objProductsEmptys : lstProductByplant ){   
	        		objEOI2 =  new ONTAP__Order_Item__c();
	        		
	        		objEOI2.ISSM_EmptyMaterialProduct__c	= objProductsEmptys.ISSM_AssociatedEmpty__r.ONTAP__MaterialProduct__c;
					objEOI2.ISSM_MaterialAvailable__c 		= objEOI.ISSM_MaterialAvailable__c;	
					objEOI2.ISSM_ProductDesc__c 			= objEOI.ISSM_ProductDesc__c;	
					objEOI2.ISSM_BalanceNum__c				= objProductsEmptys.ISSM_AssociatedEmpty__r.ISSM_ProductSKU__c;	
					objEOI2.ISSM_EmptyMaterial__c			= objProductsEmptys.ISSM_AssociatedEmpty__r.ISSM_ProductSKU__c;
					objEOI2.ISSM_IsSuggested__c				= objEOI.ISSM_IsSuggested__c;	
					objEOI2.ONCALL__OnCall_Quantity__c		= objEOI.ONCALL__OnCall_Quantity__c;
					objEOI2.ISSM_ComboNumber__c				= objEOI.ISSM_ComboNumber__c;
					objEOI2.ISSM_TaxAmount__c				= objEOI.ISSM_TaxAmount__c;
					objEOI2.ISSM_ItemDiscount__c			= objEOI.ISSM_ItemDiscount__c;
					objEOI2.ISSM_ItemPosition__c			= objEOI.ISSM_ItemPosition__c;
					objEOI2.ONTAP__CustomerOrder__c			= objEOI.ONTAP__CustomerOrder__c;
					objEOI2.ONCALL__SAP_Order_Item_Number__c = objEOI.ONCALL__SAP_Order_Item_Number__c;
					objEOI2.ISSM_NoDiscountTotalAmount__c	= objEOI.ISSM_NoDiscountTotalAmount__c;
					objEOI2.ONCALL__OnCall_Product__c		= objEOI.ONCALL__OnCall_Product__c;
					objEOI2.ISSM_UnitPrice__c				= objEOI.ISSM_UnitPrice__c;
					objEOI2.ISSM_Condition__c				= objEOI.ISSM_Condition__c;
					objEOI2.ISSM_MaterialProduct__c			= objProductsEmptys.ISSM_AssociatedEmpty__r.ONTAP__MaterialProduct__c;
					objEOI2.ONCALL__SFDC_Suggested_Order_Item_Number__c	= objEOI.ONCALL__SFDC_Suggested_Order_Item_Number__c;
					objEOI2.ISSM_Is_returnable__c			= objEOI.ISSM_Is_returnable__c;
					objEOI2.ISSM_OrderItemSKU__c			= objEOI.ISSM_OrderItemSKU__c;
					objEOI2.ISSM_UnitofMeasure__c			= objEOI.ISSM_UnitofMeasure__c;
					objEOI2.ISSM_TotalAmount__c				= objEOI.ISSM_TotalAmount__c;
					objEOI2.ISSM_Uint_Measure_Code__c 		= objEOI.ISSM_Uint_Measure_Code__c;
	        		
	        		System.debug('**bjProductsEmptys.ISSM_ProductSKU__c  : '+objProductsEmptys.ISSM_ProductSKU__c  +'  ========  objEOI : '+ objEOI.ISSM_OrderItemSKU__c); 
	        		if(String.valueOf(objProductsEmptys.ISSM_ProductSKU__c) == String.valueOf(objEOI.ISSM_OrderItemSKU__c )){
	        			System.debug('SI ENTRO  : ');
		        		lstOrderItem.add(objEOI2);  
	        		}  		
	        	}
			}
        
        	System.debug('lstOrderItem ***** : '+lstOrderItem);
		
			for(ONTAP__Order_Item__c objEOI : lstOrderItem){
				System.debug('******* objEOI2s : '+objEOI);
                System.debug(loggingLevel.Error, '*** objEOI.ISSM_Is_returnable__c: ' + objEOI.ISSM_Is_returnable__c);
                System.debug(loggingLevel.Error, '*** objEOI.ISSM_EmptyMaterial__c: ' + objEOI.ISSM_EmptyMaterial__c);
				if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyMaterial__c != null) || objEOI.ISSM_EmptyMaterial__c != null ){
	                ObjEB = new EmptyBalanceB__c();
	                ObjEB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
	                ObjEB.Account__c           = IdAcc;
	                ObjEB.OrderID__c           = IdOrder;
	                ObjEB.MaterialProduct__c   = objEOI.ISSM_EmptyMaterialProduct__c;
	                ObjEB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
	                ObjEB.EmptyType__c         = objEOI.ISSM_EmptyType__c;
	                ObjEB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
	                ObjEB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
	                ObjEB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
	                ObjEB.Process__c           = false;
	                ObjEB.EmptyMaterial__c     = objEOI.ISSM_EmptyMaterial__c;
	                LstEB.add(ObjEB);
	            } 
			}
		}else{
            System.debug('********** NO SE CUENTA CON LA INTEGRACION PRODUCT BY PLANT ***********');
			for(ONTAP__Order_Item__c objEOI : LstEOI){            
	            if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyMaterial__c != null) || objEOI.ISSM_EmptyMaterial__c != null ){
	                ObjEB = new EmptyBalanceB__c();
	                ObjEB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
	                ObjEB.Account__c           = IdAcc;
	                ObjEB.OrderID__c           = IdOrder;
	                ObjEB.MaterialProduct__c   = objEOI.ISSM_EmptyMaterialProduct__c;
	                ObjEB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjEB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
	                ObjEB.EmptyType__c         = objEOI.ISSM_EmptyType__c;
	                ObjEB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
	                ObjEB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
	                ObjEB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
	                ObjEB.Process__c           = false;
	                ObjEB.EmptyMaterial__c     = objEOI.ISSM_EmptyMaterial__c;
	                LstEB.add(ObjEB);
	            }
 			}
			
		}
        
        System.debug(loggingLevel.Error, '*** LstBns: ' + LstBns);
        for(ISSM_Bonus__c objEBonus : LstBns){
            System.debug(loggingLevel.Error, '*** objEBonus.ISSM_Product__r.ISSM_Is_returnable__c: ' + objEBonus.ISSM_Product__r.ISSM_Is_returnable__c);
            System.debug(loggingLevel.Error, '*** objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c: ' + objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c);
            if((objEBonus.ISSM_Product__r.ISSM_Is_returnable__c && objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c !=  null)||Test.isRunningTest()){
                ObjEB = new EmptyBalanceB__c();
                ObjEB.Product__c           = objEBonus.ISSM_Product__c;
                ObjEB.Account__c           = IdAcc;
                ObjEB.OrderID__c           = IdOrder;
                ObjEB.MaterialProduct__c   = objEBonus.ISSM_EmptyMaterialProduct__c;
                ObjEB.BoxesNumReturn__c    = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.BoxesNumRequest__c   = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.Balance__c           = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.EmptyBalanceKey__c   = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c; 
                ObjEB.DueDate__c           = StrDateBon.date();
                ObjEB.Material_Number__c   = objEBonus.ISSM_Material_Number__c;
                ObjEB.Uint_Measure_Code__c = objEBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c;
                ObjEB.Process__c           = false;
                ObjEB.EmptyMaterial__c     = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c;
                LstEBBonuses.add(ObjEB);
            }
        }
        
        for(ONTAP__Product__c objProdEmB : LstProductEmB){
            System.debug('*********** OBJETO  : ');
            System.debug(objProdEmB.ISSM_QuantityInput__c);
            if(objProdEmB.ONCALL__Material_Number__c !=null){
                decimal valorBox = Decimal.valueOf(String.valueOf(objProdEmB.ISSM_QuantityInput__c));
        
                ObjEB = new EmptyBalanceB__c();
                
                ObjEB.Product__c        = objProdEmB.Id;
                ObjEB.Account__c        = IdAcc;
                ObjEB.OrderID__c      = IdOrder;
                ObjEB.MaterialProduct__c  = objProdEmB.ONCALL__OnCall_Product__c;
                ObjEB.BoxesNumReturn__c   = valorBox;
                ObjEB.BoxesNumRequest__c  = valorBox;
                ObjEB.Balance__c        = valorBox;
                ObjEB.EmptyBalanceKey__c= objProdEmB.ONCALL__Material_Number__c;
                ObjEB.DueDate__c        = StrDateBon.date();
                ObjEB.Material_Number__c  = objProdEmB.ONCALL__Material_Number__c;
                ObjEB.Uint_Measure_Code__c= objProdEmB.ISSM_Uint_Measure_Code__c;
                ObjEB.Process__c          = false; 
                ObjEB.EmptyMaterial__c    = objProdEmB.ONCALL__Material_Number__c;      
            }
             LstEBProductEmpty.add(ObjEB);
        }

        System.debug('********LstEBProductEmpty  : '+LstEBProductEmpty);
        LstAllEB.addAll(LstEBProductEmpty);
        LstAllEB.addAll(LstEB);
        LstAllEB.addAll(LstEBBonuses); 
        LstEBPending = getEmptyPending(IdAcc);
        LstAllEB.addAll(LstEBPending);
		System.debug('************LstAllEB : '+LstAllEB);
		System.debug('************LstEBPending : '+LstEBPending);
        WrpEB = groupEmptyBalance(LstAllEB,DtDD,IntCD,IdOrder,IdAcc,ObjAcc.ISSM_EmptyCreditQty__c);//return Empty Balance Group

        return new WrpEmptyBalance(LstEB,LstEBBonuses,LstEBPending,WrpEB,LstEBProductEmpty);
    }
	
    /*public static WrpEmptyBalance getEmptyBalance(ONTAP__Order_Item__c[] LstEOI,ISSM_Bonus__c[] LstBns,String IdAcc, String IdOrder,ONTAP__Product__c[] LstProductEmB){
        System.debug('****************  getEmptyBalance  HMDH : '+ LstProductEmB);
        EmptyBalanceB__c ObjEB          = new EmptyBalanceB__c();
        EmptyBalanceB__c[] LstEB        = new List<EmptyBalanceB__c>(); //Lst Current order
        EmptyBalanceB__c[] LstEBBonuses = new List<EmptyBalanceB__c>(); //Lst BonusEB
        EmptyBalanceB__c[] LstEBPending = new List<EmptyBalanceB__c>(); //Lst Historic order
        EmptyBalanceB__c[] LstAllEB     = new List<EmptyBalanceB__c>(); // Lst Current+Historic order
        EmptyBalanceB__c[] LstEBProductEmpty = new List<EmptyBalanceB__c>(); //Lst Current order
        WrapperEBG WrpEB;

        Account ObjAcc      = CTRSOQL.getAccountbyId(IdAcc);
        Integer IntCD       = ObjAcc.ONTAP__EmptyLoanDays__c != null ? Integer.valueOf(ObjAcc.ONTAP__EmptyLoanDays__c) : 0;
        Datetime StrDate    = System.now();
        DateTime StrDateBon = Datetime.parse(StrDate.format())+1;
        //se suman los dias de credito a la fecha de vencimiento        
        DateTime DtDD = ObjAcc.ISSM_EmptyCreditQty__c ? Datetime.parse(StrDate.format())+IntCD+1 : Datetime.parse(StrDate.format())+1;

        for(ONTAP__Order_Item__c objEOI : LstEOI){            
            if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyMaterial__c != null) || objEOI.ISSM_EmptyMaterial__c != null ){
                ObjEB = new EmptyBalanceB__c();
                ObjEB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
                ObjEB.Account__c           = IdAcc;
                ObjEB.OrderID__c           = IdOrder;
                ObjEB.MaterialProduct__c   = objEOI.ISSM_EmptyMaterialProduct__c;
                ObjEB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
                ObjEB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
                ObjEB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
                ObjEB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
                ObjEB.EmptyType__c         = objEOI.ISSM_EmptyType__c;
                ObjEB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
                ObjEB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
                ObjEB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
                ObjEB.Process__c           = false;
                ObjEB.EmptyMaterial__c     = objEOI.ISSM_EmptyMaterial__c;
                LstEB.add(ObjEB);
            }
        }

        for(ISSM_Bonus__c objEBonus : LstBns){
            if(objEBonus.ISSM_Product__r.ISSM_Is_returnable__c && objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c !=  null){
                ObjEB = new EmptyBalanceB__c();
                ObjEB.Product__c           = objEBonus.ISSM_Product__c;
                ObjEB.Account__c           = IdAcc;
                ObjEB.OrderID__c           = IdOrder;
                ObjEB.MaterialProduct__c   = objEBonus.ISSM_EmptyMaterialProduct__c;
                ObjEB.BoxesNumReturn__c    = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.BoxesNumRequest__c   = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.Balance__c           = objEBonus.ISSM_BonusQuantity__c;
                ObjEB.EmptyBalanceKey__c   = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c; 
                ObjEB.DueDate__c           = StrDateBon.date();
                ObjEB.Material_Number__c   = objEBonus.ISSM_Material_Number__c;
                ObjEB.Uint_Measure_Code__c = objEBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c;
                ObjEB.Process__c           = false;
                ObjEB.EmptyMaterial__c     = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c;
                LstEBBonuses.add(ObjEB);
            }
        }
        
        for(ONTAP__Product__c objProdEmB : LstProductEmB){
            System.debug('*********** OBJETO  : ');
            System.debug(objProdEmB.ISSM_QuantityInput__c);
            if(objProdEmB.ONCALL__Material_Number__c !=null){
                decimal valorBox = Decimal.valueOf(String.valueOf(objProdEmB.ISSM_QuantityInput__c));
        
                ObjEB = new EmptyBalanceB__c();
                
                ObjEB.Product__c        = objProdEmB.Id;
                ObjEB.Account__c        = IdAcc;
                ObjEB.OrderID__c      = IdOrder;
                ObjEB.MaterialProduct__c  = objProdEmB.ONCALL__OnCall_Product__c;
                ObjEB.BoxesNumReturn__c   = valorBox;
                ObjEB.BoxesNumRequest__c  = valorBox;
                ObjEB.Balance__c        = valorBox;
                ObjEB.EmptyBalanceKey__c= objProdEmB.ONCALL__Material_Number__c;
                ObjEB.DueDate__c        = StrDateBon.date();
                ObjEB.Material_Number__c  = objProdEmB.ONCALL__Material_Number__c;
                ObjEB.Uint_Measure_Code__c= objProdEmB.ISSM_Uint_Measure_Code__c;
                ObjEB.Process__c          = false; 
                ObjEB.EmptyMaterial__c    = objProdEmB.ONCALL__Material_Number__c;      
            }
             LstEBProductEmpty.add(ObjEB);
        }

        System.debug('********LstEBProductEmpty  : '+LstEBProductEmpty);
        LstAllEB.addAll(LstEBProductEmpty);
        LstAllEB.addAll(LstEB);
        LstAllEB.addAll(LstEBBonuses); 
        LstEBPending = getEmptyPending(IdAcc);
        LstAllEB.addAll(LstEBPending);

        WrpEB = groupEmptyBalance(LstAllEB,DtDD,IntCD,IdOrder,IdAcc,ObjAcc.ISSM_EmptyCreditQty__c);//return Empty Balance Group

        return new WrpEmptyBalance(LstEB,LstEBBonuses,LstEBPending,WrpEB,LstEBProductEmpty);
    }*/

    //Obtiene el historico de envases 
    public static EmptyBalanceB__c[] getEmptyPending(String IdAcc){
        Id RecType = CTRSOQL.getRecordTypeId('EmptyBalanceB__c','Compact');
        // "ENVASE%" DEFINIDO POR EL AREA FUNCIONAL
        EmptyBalanceB__c[] LstEBPending = CTRSOQL.getEmptyBalanceBByIdAcc(IdAcc,'ENVASE%',RecType);
        
        for(EmptyBalanceB__c ObjEB : LstEBPending){
        	System.debug('*****ObjEB5 : '+ObjEB);
            ObjEB.EmptyMaterial__c     = ObjEB.Product__r.ONCALL__Material_Number__c;
            ObjEB.MaterialProduct__c   = ObjEB.Product__r.ONTAP__MaterialProduct__c;
            ObjEB.BoxesNumRequest__c = ObjEB.Balance__c;
            ObjEB.BoxesNumReturn__c  = ObjEB.Balance__c;
        }

        if(!LstEBPending.isEmpty())
            update LstEBPending;

        return LstEBPending;
    }

    //Agrupa los envases de historico, bonificación y del pedido
    public static WrapperEBG groupEmptyBalance(EmptyBalanceB__c[] LstEBInput,DateTime DtDueDate,Integer DaysOfCredit, String IdOrder,String IdAcc,Boolean BlnSlct){
        Map<String, EmptyBalanceB__c[]> MapEBGroup = new Map<String, EmptyBalanceB__c[]>();
        EmptyBalanceB__c[] LstEB  = new List<EmptyBalanceB__c>(); 
        EmptyBalanceB__c OEB      = new EmptyBalanceB__c();
        Map<String,Decimal> MapEB       = new Map<String,Decimal>();
        EmptyBalanceGroup[] LstWrpObjEB = new List<EmptyBalanceGroup>();
        Integer boxesRet  = 0;
        Integer boxesReq  = 0;
        Integer MinReturn = 0;
        String StrMat;
        String IdProd;
        String MatNumber;
        String unitMeasure;

        LstEBInput.sort();//Ordena la lista

        //create Map for GROUP OF EMPTIES
        for(EmptyBalanceB__c recEB : LstEBInput) {

            if(MapEBGroup.containsKey(recEB.EmptyMaterial__c))
                MapEBGroup.get(recEB.EmptyMaterial__c).add(recEB);
            else
                MapEBGroup.put(recEB.EmptyMaterial__c, new List<EmptyBalanceB__c> { recEB });
        }

        
        for(String recordId : MapEBGroup.keySet()){
            boxesRet    = 0;boxesReq = 0;
            MinReturn   = 0;StrMat   = null;
            Datetime StrDate = System.now()+1;
            for(EmptyBalanceB__c EBRecord : MapEBGroup.get(recordId)){
                //MinReturn+=Integer.valueOf(EBRecord.BoxesNumRequest__c);

                System.debug('EBRecord = ' + EBRecord);
            
                if(EBRecord.DueDate__c > StrDate && BlnSlct){
                    MinReturn-=Integer.valueOf(EBRecord.BoxesNumRequest__c);
                    System.debug('MinReturn = ' + MinReturn);
                }
                
                if(EBRecord.DueDate__c <= StrDate){
                    MinReturn+=Integer.valueOf(EBRecord.BoxesNumRequest__c);
                    System.debug('MinReturn = ' + MinReturn);
                }
                
                unitMeasure = EBRecord.Uint_Measure_Code__c;
                MatNumber =  EBRecord.EmptyMaterial__c == null ? EBRecord.Material_Number__c : EBRecord.EmptyMaterial__c; 
                boxesRet  += Integer.valueOf(EBRecord.BoxesNumReturn__c) != null ? Integer.valueOf(EBRecord.BoxesNumReturn__c) : 0;
                StrMat    =  EBRecord.MaterialProduct__c;
                boxesReq  += Integer.valueOf(EBRecord.BoxesNumRequest__c) != null ? Integer.valueOf(EBRecord.BoxesNumRequest__c) : 0;
                IdProd    =  EBRecord.Product__c;                
            }
            LstWrpObjEB.add(new EmptyBalanceGroup(MatNumber,boxesReq,boxesRet,StrMat,DtDueDate,MinReturn,false,IdProd,IdOrder,IdAcc,unitMeasure));
        }

        return new WrapperEBG(LstWrpObjEB);
    }

    /**
    * Wrapper Class for the WrpEmptyBalance
    **/
    global class WrpEmptyBalance{
        global EmptyBalanceB__c[] LstEBOrder;
        global EmptyBalanceB__c[] LstEBBonuses;
        global EmptyBalanceB__c[] LstEBPending;
        global WrapperEBG LstEBGroup;
        global EmptyBalanceB__c[] LstEBProductEmpty;

        /**
        * Constructor Method
        * @param LstEBOrder: EmptyBalanceB__c array of the Order
        * @param LstEBPending: EmptyBalanceB__c array for the Historic of the Empties for the Customer
        **/ 
        public WrpEmptyBalance(EmptyBalanceB__c[] LstEBOrder, EmptyBalanceB__c[] LstEBBonuses, EmptyBalanceB__c[] LstEBPending,WrapperEBG LstEBGroup,EmptyBalanceB__c[] LstEBProductEmpty){
            this.LstEBOrder=LstEBOrder;
            this.LstEBBonuses = LstEBBonuses;
            this.LstEBPending=LstEBPending;
            this.LstEBGroup=LstEBGroup;
            this.LstEBProductEmpty =  LstEBProductEmpty;
        }    
    }

    global class EmptyBalanceGroup{
        global String MatNumber;
        global Integer NumReq;
        global Integer NumRet;
        global String Name;
        global DateTime DtDueDate;
        global Integer IntMin;
        global Boolean BlnSlct;
        global String IdProd;
        global String IdOrder;
        global String IdAcc;
        global String unitMeasure;
        
        public EmptyBalanceGroup(String MatNumber,Integer NumReq,Integer NumRet,String Name,DateTime DtDueDate,Integer IntMin,Boolean BlnSlct,String IdProd,String IdOrder,String IdAcc,String unitMeasure){
            this.MatNumber=MatNumber;
            this.NumReq=NumReq;
            this.NumRet=NumRet;
            this.Name=Name;
            this.DtDueDate=DtDueDate;
            this.IntMin=IntMin;
            this.BlnSlct=BlnSlct;
            this.IdProd=IdProd;
            this.IdOrder=IdOrder;
            this.IdAcc=IdAcc;
            this.unitMeasure = unitMeasure;
        }
    }

    global class WrapperEBG {
        global EmptyBalanceGroup[] WrpLstEB;

        public WrapperEBG(EmptyBalanceGroup[] WrpLstEB){
            this.WrpLstEB=WrpLstEB;
        }
    }
}