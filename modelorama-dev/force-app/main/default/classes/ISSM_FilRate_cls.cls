/****************************************************************************************************
    General Information
    -------------------
    Authores:   Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Class to get the Fill Rate for the Order Taking process
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       14-September-2017   Marco Zúñiga             Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_FilRate_cls {
	public ISSM_FilRate_cls() {}

	public static List<ISSM_FillRate__c > getFillRate(ONTAP__Order_Item__c[] lstIds, ONTAP__Order_Item__c[] lstOICash, 
                                                      ONTAP__Order_Item__c[] lstOICredit, String IdOrderr, String StrIdAcc){ 
        Map<Id,Double> MapFillRate = new Map<Id,Decimal>();
        ISSM_FillRate__c ObjFillRate; 
        List<ISSM_FillRate__c> LstFillRate = new List<ISSM_FillRate__c>();
        Map<Id,Id> MapIdPBO = new Map<Id,Id>();        
        for(ONTAP__Order_Item__c objOICredit : lstOICash){
            Double DecQuantity = objOICredit.ISSM_MaterialAvailable__c != null ? Double.valueOf(objOICredit.ISSM_MaterialAvailable__c) : 0.0;                        
            MapFillRate.put(objOICredit.ONCALL__OnCall_Product__c,DecQuantity);
            MapIdPBO.put(objOICredit.ONCALL__OnCall_Product__c,objOICredit.ISSM_RelatedProduct__c);
        }

        for(ONTAP__Order_Item__c objOICash : lstOICredit){
            Double DecQuantity = objOICash.ISSM_MaterialAvailable__c != null ? Double.valueOf(objOICash.ISSM_MaterialAvailable__c) : 0.0;                        
            MapFillRate.put(objOICash.ONCALL__OnCall_Product__c,DecQuantity);
            MapIdPBO.put(objOICash.ONCALL__OnCall_Product__c,objOICash.ISSM_RelatedProduct__c);
        }

        try{
            for(ONTAP__Order_Item__c objOI : lstIds){      
                ObjFillRate = new ISSM_FillRate__c();

                ObjFillRate.ISSM_AssociatedOrder__c        = IdOrderr;
                ObjFillRate.ISSM_Product__c                = objOI.ONCALL__OnCall_Product__c;
                ObjFillRate.ISSM_RequestedProductAmount__c = objOI.ONCALL__OnCall_Quantity__c;
                ObjFillRate.ISSM_AssociatedAccount__c      = StrIdAcc;
                ObjFillRate.ISSM_AvailableProductAmount__c = MapFillRate.containsKey(objOI.ONCALL__OnCall_Product__c) ? MapFillRate.get(objOI.ONCALL__OnCall_Product__c) : 0.0 ;
                ObjFillRate.ISSM_ProductByOrg__c           = MapIdPBO.containsKey(objOI.ONCALL__OnCall_Product__c)    ? MapIdPBO.get(objOI.ONCALL__OnCall_Product__c) : null;
                LstFillRate.add(ObjFillRate);                                
            }

            if(LstFillRate.size() > 0){
                insert LstFillRate;
            }            

        }catch(Exception exc){throw exc; 
            System.debug('Fill Rate Error = ' + exc.getMessage());
        }
        return LstFillRate;
    }
}