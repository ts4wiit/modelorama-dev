/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ABInBev (MDM Rules) / CDM 
Description: Batch class to process the validation rules for the fields of the object MDM_temp_Account__c
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-01-2018   Rodrigo RESENDIZ (RR)   Initial version
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_ProcessRules_bch implements Database.Batchable<SObject> {
    
    Private Integer limitQuery;
    Private String[] filterKTOKD;
    Private String filterVKORG;
    Private String filterVKBUR;
    Private String filterSPART;
    Private String filterVTWEG;
    
    //Constructor method to receive params from the Lightning component 'CDM_FilterExecution_cmp'
    public MDM_ProcessRules_bch(Integer lmtQry, String[] KTOKD, String VKORG, String VKBUR, String SPART, String VTWEG ){
        limitQuery= lmtQry;
        filterKTOKD= KTOKD;
    	filterVKORG= VKORG;
    	filterVKBUR= VKBUR;
    	filterSPART= SPART;
    	filterVTWEG= VTWEG;
        System.debug('LIMITQERY = '+lmtQry ); System.debug('filterKTOKD = '+filterKTOKD );System.debug('filterVKORG = '+filterVKORG );System.debug('filterVKBUR = '+filterVKBUR );
    }
    
    public Database.QueryLocator start(Database.BatchableContext ctx){
        String query = 'SELECT Id, Name, Id_External__c,PARVW__c,KUNN2__c, KTOKD__c, ANRED__c, NAME1__c, NAME2__c, NAME3__c, NAME4__c, SORTL__c, STRAS__c,'
            +' HOUSE_NUM1__c, HOUSE_NUM2__c, CITY2__c, POST_CODE1__c, CITY1__c, COUNTRY__c, REGION__c, POST_CODE3__c, '
            +'SPRAS__c, TEL_NUMBER__c, SMTP_ADDR__c, TELBX__c, BRSCH__c, BAHNS__c, STCD1__c, STCD3__c, STCD4__c, FITYP__c,AUFSD__c,'
            +' MDM_Control_Check__c, STKZN__c, KUKLA__c, KATR1__c, KATR2__c, KATR3__c, KATR4__c, KATR5__c, KATR6__c, KATR7__c,LIFSD__c,'
            +' KATR8__c, KATR9__c, KATR10__c, KDKG1__c, KDKG2__c, KDKG3__c, KDKG4__c, KDKG5__c, LZONE__c, BANKL__c, BANKS__c,FAKSD__c,'
            +' BANKN__c, KOINH__c, BKREF__c, BUKRS__c, AKONT__c, ZUAWA__c, FDGRV__c, ALTKN__c, ZTERM__c, GUZTE__c, TOGRU__c,TAXKD06Id__c,'
            +' XZVER__c, ZWELS__c, MAHNA__c, EIKTO__c, ZSABE__c, TLFNS__c, GRIDT__c, GRICD__c, VKORG__c, VTWEG__c, SPART__c,TAXKD05Id__c,'
            +' BZIRK__c, VKBUR__c, VKGRP__c, KDGRP__c, KLABC__c, WAERS__c, KONDA__c, KALKS__c, PLTYP__c, VERSG__c, LPRIO__c,TAXKD04Id__c,'
            +' VSBED__c, VWERK__c, KZAZU__c, PERFK__c, PERRL__c, INCO1__c, INCO2__c, ZTERM2__c, KTGRD__c, TAXKD_01__c, TAXKD03Id__c,CDM_Flag_Control__c,'
            +' TAXKD_02__c, TAXKD_03__c, TAXKD_04__c, TAXKD_05__c, TAXKD_06__c, PARVWSP__c, PARVWBP__c, PARVWPY__c, PARVWSH__c,CDM_Order_Type__c,'
            +' KVGR1__c, KVGR3__c, KVGR4__c, KVGR5__c, TAXKD01Id__c	,TAXKD02Id__c FROM MDM_Temp_Account__c WHERE MDM_Control_Check__c= true ' ;
         
        query= ( filterKTOKD== null || filterKTOKD.isEmpty()) ?query :query+'AND KTOKD__c IN: filterKTOKD';   
        query= (filterVKORG=='--'||filterVKORG==null) ?query :query+' AND VKORG__c= \''+filterVKORG +'\'';
        query= (filterVKBUR=='--'||filterVKBUR==null) ?query :query+' AND VKBUR__c= \''+filterVKBUR +'\'';
        query= (filterSPART=='--'||filterSPART==null) ?query :query+' AND SPART__c= \''+filterSPART +'\'';
        query= (filterVTWEG=='--'||filterVTWEG==null) ?query :query+' AND VTWEG__c= \''+filterVTWEG +'\'';
        query= (limitQuery==null) ?query :query+' LIMIT '+limitQuery ;
        System.debug('QUUUUUUUUUUUUUUUUUUUUUUUUERY:::::: '+query);
            return Database.getQueryLocator(query);
    } 
    
    public void execute(Database.BatchableContext ctx, List<SObject> scope){
        
        Set<String> setHelper = new Set<String>();
        List<MDM_Validation_Result__c> validationResult_lst = new List<MDM_Validation_Result__c>();
        List<MDM_Account__c> validatedAccount_lst = new List<MDM_Account__c>();
        Set<String> acctToInsert_set = new Set<String>();
        Map<String, Id> accIdExternal = new Map<String, Id>();
        MDM_ProcessRulesHelper_cls.MDM_ValidationResult validResults;

        SObject tmpAcc;
        String codeFieldName;
        Map<String, List<String>> parametersList = new Map<String, List<String>>();
        Set<String> setSAGranularity = new Set<String>();
        Set<String> setACenterGranularity = new Set<String>();
        Set<String> setSTGranularity = new Set<String>();
        Set<String> setKUNN2 = new Set<String>();
        Set<String> setKUNNR = new Set<String>();
        Set<String> setIDs = new Set<String>();
        Set<String> setClientType = new Set<String>();
        Set<String> setPARVW_BP = new Set<String>();
         for(MDM_AccountFieldsMapping__mdt mappedFields : MDM_Account_cls.getAccountMappingLst().Values() ){
            codeFieldName = (String)mappedFields.MasterLabel+'__c';
            for(MDM_Temp_Account__c accScope : (List<MDM_Temp_Account__c>)scope){ 
                tmpAcc = accScope;
                if(accScope.KTOKD__c==Label.CDM_Apex_Z010){
                   setKUNN2.add(accScope.PARVW__c); 
                }
                If(accScope.KTOKD__c==Label.CDM_Apex_Z001){
					setIDs.add(String.valueOf(accScope.Id));                    
                }
                if(accScope.CDM_Order_Type__c==Label.CDM_Apex_Y054 || accScope.CDM_Order_Type__c==Label.CDM_Apex_Y064){
                    setClientType.add(String.valueOf(accScope.Id));
                }
                If(accScope.KTOKD__c==Label.CDM_Apex_Z019){
					setPARVW_BP.add(String.valueOf(accScope.Id));  
                    setKUNNR.add(accScope.PARVW__c);
                }
                setSAGranularity.add(accScope.VKORG__c +'-'+ accScope.VTWEG__c +'-'+ accScope.SPART__c +'-'+ accScope.VKBUR__c);
                setACenterGranularity.add(accScope.VKORG__c + '-' + accScope.VTWEG__c + '-' + accScope.VWERK__c);
                setSTGranularity.add(accScope.VKBUR__c + '-' + accScope.VKGRP__c );
                if(!parametersList.containsKey(codeFieldName)){
                    parametersList.put(codeFieldName, new List<String>{(String)tmpAcc.get(codeFieldName)});
                }else{
                    parametersList.get(codeFieldName).add((String)tmpAcc.get(codeFieldName));
                }
            }
        }
        //Loop to proccess by record
        for(MDM_Temp_Account__c accScope : (List<MDM_Temp_Account__c>)scope){ 
            validResults = MDM_ProcessRulesHelper_cls.processRules(accScope, parametersList, setSAGranularity, setACenterGranularity, setSTGranularity , setKUNN2,setKUNNR,setPARVW_BP,setIDs,setClientType );
            if(validResults.getAccount().Unique_Id__c!=null && !acctToInsert_set.contains(validResults.getAccount().Unique_Id__c)){
                validationResult_lst.addAll(validResults.getVResult());
                acctToInsert_set.add(validResults.getAccount().Unique_Id__c);
                validatedAccount_lst.add(validResults.getAccount());
            }
            if(validResults.getTmpAccount().CDM_Flag_Control__c){
                accScope.CDM_Flag_Control__c=true;
            }
            accScope.MDM_Control_Check__c = false;
        }
        //Iterate to get Temp Account Errors.
        for(MDM_Validation_Result__c iteration: validationResult_lst){
            String concat = iteration.UNIQUE_ID__c;
            setHelper.add(concat.substring(0,18));
        }
        List<MDM_Account__c> validatedAccount_lst2 = new List<MDM_Account__c>();
        //Iterate to update only validate Accounts.
        for(MDM_Account__c accounts: validatedAccount_lst){
			if(!setHelper.contains(accounts.Unique_Id__c)){
            validatedAccount_lst2.add(accounts);
        	}
        }
        

        //update validated temp accounts
        update scope;
        //upsert accounts once validated
        upsert validatedAccount_lst2 Unique_Id__c;
        //map accounts with its validation results
        for(MDM_Account__c acc_obj : validatedAccount_lst){
            accIdExternal.put(acc_obj.Unique_Id__c, acc_obj.Id);
        }
        for(MDM_Validation_Result__c vResults : validationResult_lst){
            vResults.MDM_Account__c = accIdExternal.get(vResults.Unique_Id__c);
        }
        //upsert validation results
        upsert validationResult_lst Unique_Id__c;
        // print results of batch process
        System.debug(':::: MDM_ProcessRules_bch -RESULTS: validatedAccount_lst SIZE ' + validatedAccount_lst.size());
        System.debug(':::: MDM_ProcessRules_bch -RESULTS: validationResult_lst SIZE ' + validationResult_lst.size());
        System.debug(':::: MDM_ProcessRules_bch -RESULTS: (' + scope.size() + ') records processed.');
    }
    
    public void finish(Database.BatchableContext ctx){
        Database.executeBatch(new CDM_UpdateAccountRshp_bch(), 200);
        EmailTemplate template =[SELECT Id FROM EmailTemplate WHERE Name=:Label.CDM_Apex_CDM_Execute_Rules];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(template.Id);
        mail.setTargetObjectId(System.UserInfo.getUserId());
        mail.setSaveAsActivity(false);
    
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}