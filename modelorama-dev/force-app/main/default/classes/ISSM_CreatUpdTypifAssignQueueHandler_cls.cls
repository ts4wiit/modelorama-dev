/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public with sharing class ISSM_CreatUpdTypifAssignQueueHandler_cls {
	ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls(); 
    /**
    * Descripcion :  
    * @param  none
    * @return none  
    **/
    public void CreateTypificationAssignQueue(List <ISSM_TypificationMatrix__c> lstDataTypificationMatix ) {
        System.debug('#####ISSM_CreatUpdTypifAssignQueueHandler_cls -- CreateTypificationAssignQueue');
        ISSM_TypificationMatrix__c objTypificationMatrix = new ISSM_TypificationMatrix__c();
        List<Group> lstQueueSobject = new List<Group>();
        lstQueueSobject = objCSQuerys.QueryGroup(lstDataTypificationMatix);
        for(ISSM_TypificationMatrix__c objDateTypification:lstDataTypificationMatix){
                
            if(!lstQueueSobject.isEmpty()){
                objDateTypification.ISSM_IdQueue__c=lstQueueSobject[0].id;
                objDateTypification.ISSM_ID_Escalation_Queue__c = objDateTypification.ISSM_EscalationQueue__c;

            }else{
                if(objDateTypification.ISSM_AssignedTo__c == System.label.ISSM_AssignCaseQueue){
                    objDateTypification.addError(System.label.ISSM_AssignCaseQueueError); 
                }
            }
        } 
    }
}