/*******************************************************************************
Desarrollado por: Accenture
Autor: Fernando Engel
Proyecto: MDRM
Descripción: Test Class for MDRM_Vision360 Visualforce Page

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    26/04/2018 Fernando Engel            Created Class
*******************************************************************************/
@isTest
public class MDRM_Vision360_tst {
    
    static testmethod void MDRM_Vision360_ctr_test_NoUser() {
        test.startTest();
        
        MDRM_Vision360_ctr ctr = new MDRM_Vision360_ctr();
        //Error: Force to found Month 13
        MDRM_Vision360_ctr.dataPerformance data = new MDRM_Vision360_ctr.dataPerformance(13, 100);
        //Error: Force to get info null;
        ctr.genInfoBusinessman();
        
        ctr.genZ019PickList();
        
        test.stoptest();
    }
    
    static testmethod void MDRM_Vision360_ctr_test_UserNotFound() {
        test.startTest();
        
        PageReference pageRef = Page.MDRM_Vision360;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('MDRM_Z019', '1');        
        MDRM_Vision360_ctr ctr = new MDRM_Vision360_ctr();
        
        test.stoptest();
    }
    
    static testmethod void MDRM_Vision360_ctr_test_NoBusinessman() {
        String Z019 = '1';
        String Z001 = '2';
        
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        Businessman.Z001__c = Z001;
        Insert Businessman;
        
        
        test.startTest();
        
        PageReference pageRef = Page.MDRM_Vision360;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('MDRM_Z019', '1');        
        MDRM_Vision360_ctr ctr = new MDRM_Vision360_ctr();
        ctr.getYears();
        
        test.stoptest();
    }
    static testmethod void MDRM_Vision360_ctr_test_Correct() {
        String Z019 = '1';
        String Z001 = '2';
        
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        Businessman.Z001__c = Z001;
        Insert Businessman;
        
        Account Expansor = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        Expansor.Z019__c = Z019;
        Expansor.ParentId = Businessman.id;
        Insert Expansor;
        
        MDRM_Businessman_Expansor__c Businessman_Expansor = MDRM_TestUtils_tst.generateBusinessman_Expansor( Businessman,  Expansor);
        insert Businessman_Expansor;
        
        MDRM_Modelorama_Performance__c Modelorama_Performance = MDRM_TestUtils_tst.generateModelorama_Performance( Businessman_Expansor);
        insert Modelorama_Performance;
        
        
        test.startTest();
        
        PageReference pageRef = Page.MDRM_Vision360;
        Test.setCurrentPage(pageRef);
        pageRef.getHeaders().put('MDRM_Z019', '1');    
        pageRef.getHeaders().put('MDRM_Year', String.valueOf( system.today().Year() ));  
        MDRM_Vision360_ctr ctr = new MDRM_Vision360_ctr();
        ctr.selectedYear = '2018';
        ctr.getYears();
        ctr.genInformation();
        
        test.stoptest();
    }
}