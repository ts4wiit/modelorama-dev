/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Batch Class for delete conditión records

    Information about changes (versions)
    ===============================================================================
    No.    Date             	Author                      Description
    1.0    30-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
global class TRM_DeleteConditions_bch implements Database.Batchable<sObject> {

	global  ONTAP__Product__c[] lstTotalProducts  = new List<ONTAP__Product__c>();
	global  Map<String,String> mapStructures = new Map<String,String>();
	global  Map<String,String> mapCatalogs 	= new Map<String,String>();
	global  Account[] lstCustomer 			= new List<Account>();
	global  TRM_ConditionClass__c condClssRec = new TRM_ConditionClass__c();
	global  TRM_ConditionRelationships__mdt mtdRelation = new TRM_ConditionRelationships__mdt();
	global  TRM_ConditionsManagement__mdt mtdMngRecord = new TRM_ConditionsManagement__mdt();
	global  TRM_ProductScales__c[] lstScales = new List<TRM_ProductScales__c>();
	
	global String StrQuery='';
	global String StrIdClass;
	
	//Constructor Method
	global TRM_DeleteConditions_bch(ONTAP__Product__c[] products, 
                                    Map<String,String> structures,
                                    Map<String,String> catalogs,
                                    Account[] customer,
                                    TRM_ConditionClass__c clssRec,
                                    TRM_ConditionRelationships__mdt relation,
                                    TRM_ConditionsManagement__mdt mngRecord,
                                    TRM_ProductScales__c[] lstScals) {
		StrIdClass 			= clssRec.Id;
		lstTotalProducts 	= products;
		mapStructures 		= structures;
		mapCatalogs 		= catalogs;
		lstCustomer  		= customer;
		condClssRec 		= clssRec;
		mtdRelation 		= relation;
		mtdMngRecord 		= mngRecord;
		lstScales			= lstScals;

	}

	//Start the execution of the batch that allows to consult the records of TRM_ConditionRecord__c to be deleted
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('####################START BATCH: TRM_DeleteConditions_bch####################');

		StrQuery= 'SELECT TRM_ConditionClass__c ';
        StrQuery+='FROM TRM_ConditionRecord__c ';
		StrQuery+='WHERE TRM_ConditionClass__c = \'' + StrIdClass + '\'';

		return Database.getQueryLocator(StrQuery);
	}

	//Starts the execution of the deletion of condition records
   	global void execute(Database.BatchableContext BC, List<TRM_ConditionRecord__c> lstCondRec) {
   		System.debug('####################EXECUTE BATCH: TRM_DeleteConditions_bch#################### ');
   		
   		if(!lstCondRec.isEmpty()){
   			delete lstCondRec;	
   		}
	}
	
	//Ends batch execution
	global void finish(Database.BatchableContext BC) {
		System.debug('####################FINISH BATCH: TRM_DeleteConditions_bch####################');
		
		TRM_GenerateConditions_bch bchGenerateProcess = new TRM_GenerateConditions_bch(lstTotalProducts,
                                                                                     mapStructures,
                                                                                     mapCatalogs,
                                                                                     lstCustomer,
                                                                                     condClssRec,
                                                                                     mtdRelation,
                                                                                     mtdMngRecord,
                                                                                     lstScales);
        ID batchGenerateProcessId  = Database.executeBatch(bchGenerateProcess);
        System.debug('###batchGenerateProcessId### '+batchGenerateProcessId); 
	}
}