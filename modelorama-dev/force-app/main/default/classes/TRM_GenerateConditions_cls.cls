/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows the generation of condition records according to the type of sequence

    Information about changes (versions)
    ===============================================================================
    No.    Date             	Author                      Description
    1.0    06-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class TRM_GenerateConditions_cls {
	public TRM_GenerateConditions_cls() {}

	/**
    * @description  Method that allows the generation of a condition
    *
    * @param    condClssRec 		Condition Class Registration
    * @param    mtdRelation         Metadata indicating that fields are full
    * @param    blnCustomer 		Indicate if clients apply
    * @param    objProduct          Product to process
    * @param    strZone   			Price zone to condition
    * @param    strSegment        	Segment to condition
    * @param    strSalesOff         sales oficce to condition
    * @param    strSalesOrg         sales org to condition
    * @param    objCustomer         selected Customers
    * @param    strChanel          	Chanel distribution
    * @param    strKey          	External Key
    * @param    mapStructures       Map with sales structures (Office, Organization)
    * @param    mapCatalogs         Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c: 	Condition registration generated
    */
	public TRM_ConditionRecord__c conditionRecord(TRM_ConditionClass__c condClssRec,
												  TRM_ConditionRelationships__mdt mtdRelation,
												  Boolean blnCustomer,
												  ONTAP__Product__c objProduct,
												  String strZone,
												  String strSegment,
												  String strSalesOff,
												  String strSalesOrg,
												  Account objCustomer,
												  String strChanel,
												  String strKey,
												  Map<String,String> mapStructures,
												  Map<String,String> mapCatalogs
												  ){
		TRM_ConditionRecord__c objConditionRec  = new TRM_ConditionRecord__c();
		objConditionRec.TRM_DummyKey__c 		= strKey;
		objConditionRec.TRM_ConditionClass__c 	= condClssRec.Id;
		objConditionRec.TRM_AmountPercentage__c = objProduct.ISSM_UnitPrice__c;
		objConditionRec.TRM_Product__c 			= objProduct.Id;
		objConditionRec.TRM_Sector__c 			= (mtdRelation.TRM_Sector__c) ? condClssRec.TRM_Sector__c : null;
		objConditionRec.TRM_Customer__c 		= (blnCustomer) ? objCustomer.Id : null;
		objConditionRec.TRM_UnitMeasure__c		= (mtdRelation.TRM_ConditionUnit__c)  ? condClssRec.TRM_ConditionUnit__c : null;
		objConditionRec.TRM_Segment__c 			= (mtdRelation.TRM_Segment__c)        ? mapCatalogs.get(strSegment) : null;
		objConditionRec.TRM_StatePerZone__c 	= (mtdRelation.TRM_StatePerZone__c)   ? mapCatalogs.get(strZone)    : null;
		//objConditionRec.TRM_SalesOrg__c 		= (mtdRelation.TRM_SalesOrg__c)       ? mapStructures.get(strSalesOrg) : null;
		objConditionRec.TRM_SalesOffice__c 		= (mtdRelation.TRM_SalesOffice__c)    ? mapStructures.get(strSalesOff) : null;
       // objConditionRec.TRM_Continent__c 		= (mtdRelation.TRM_Continent__c)      ? condClssRec.TRM_Continent__r.Code__c : null;
	    objConditionRec.TRM_Utilization__c 		= (mtdRelation.TRM_Utilization__c)    ? condClssRec.TRM_Utilization__c : null;
		objConditionRec.TRM_DistributionChannel__c = (mtdRelation.TRM_DistChannel__c) ? strChanel : null;
	
		return objConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by customer
    * 
    * @param    lstProducts          List of selected products
    * @param    lstCustomer          List of selected Customers
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    condClssRec          Condition Class Registration
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchByCustomer(ONTAP__Product__c[] lstProducts,
													 Account[] lstCustomer,
													 TRM_ConditionRelationships__mdt mtdRelation,
													 TRM_ConditionsManagement__mdt mtdMngRecord,
													 TRM_ConditionClass__c condClssRec,
													 Map<String,String> mapStructures,
													 Map<String,String> mapCatalogs){
		System.debug('#_#SearchByCustomer#_#');
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        for(Integer p=0; p<lstProducts.size(); p++){
            for (Integer z=0; z<lstCustomer.size(); z++) {
                TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
																	  mtdRelation,
																	  mtdMngRecord.TRM_Customers__c,
																	  lstProducts[p],
																	  'PriceZone-'+lstCustomer[z].TRM_PriceZone__c,
																	  lstCustomer[z].ISSM_SegmentCode__r.ExternalId__c,
																	  lstCustomer[z].ONTAP__SalesOffId__c,
																	  lstCustomer[z].ONTAP__SalesOgId__c,
																	  lstCustomer[z],
																	  lstCustomer[z].ONTAP__ChannelId__c,
																	  condClssRec.Name+'R'+acum++,
																	  mapStructures,
																	  mapCatalogs);
                lstConditionRec.add(objCondition);
            }    
        }
      
        return lstConditionRec;									
	}

	/**
    * @description  Method that allows generating conditions by Product
    * 
    * @param    lstProducts          List of selected products
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    condClssRec          Condition Class Registration
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchByProduct(ONTAP__Product__c[] lstProducts,
													TRM_ConditionRelationships__mdt mtdRelation,
													TRM_ConditionsManagement__mdt mtdMngRecord,
													TRM_ConditionClass__c condClssRec,
													Map<String,String> mapStructures,
													Map<String,String> mapCatalogs){
		System.debug('#_#SearchByProduct#_#');
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        for(Integer p=0; p<lstProducts.size(); p++){
            TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
			                                                      mtdRelation,
			                                                      mtdMngRecord.TRM_Customers__c,
			                                                      lstProducts[p],
			                                                      null,
			                                                      null,
			                                                      null,
			                                                      null,
			                                                      null,
			                                                      condClssRec.TRM_DistributionChannel__c,
			                                                      condClssRec.Name+'R'+acum++,
			                                                      mapStructures,
			                                                      mapCatalogs);
            lstConditionRec.add(objCondition);
        }
     
        return lstConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by segment-zone
    * 
    * @param    lstProducts          List of selected products
    * @param    lstSegments          List of segments selected
    * @param    lstPriZones          List of price zones selected
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchBySegmentZone(ONTAP__Product__c[] lstProducts,
														String[] lstSegments,
														String[] lstPriZones,
														TRM_ConditionClass__c condClssRec,
														TRM_ConditionRelationships__mdt mtdRelation,
														TRM_ConditionsManagement__mdt mtdMngRecord,
														Map<String,String> mapStructures,
														Map<String,String> mapCatalogs){

        System.debug('#_#SearchBySegmentZone#_#');
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        if(lstSegments.size() > 0){
            for (Integer p=0; p<lstProducts.size(); p++){
                for (Integer z=0; z<lstPriZones.size(); z++) {
                    for (Integer s=0; s<lstSegments.size(); s++) {

                        TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                              mtdRelation,
				                                                              mtdMngRecord.TRM_Customers__c,
				                                                              lstProducts[p],
				                                                              lstPriZones[z],
				                                                              lstSegments[s],
				                                                              null,
				                                                              null,
				                                                              null,
				                                                              condClssRec.TRM_DistributionChannel__c,
				                                                              condClssRec.Name+'R'+acum++,
				                                                              mapStructures,
				                                                              mapCatalogs);
                        lstConditionRec.add(objCondition);
                    }
                }    
            }
        }else{
            for(Integer p=0; p < lstProducts.size(); p++){
                for (Integer z=0; z < lstPriZones.size(); z++) {
                    TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                         mtdRelation,
				                                                         mtdMngRecord.TRM_Customers__c,
				                                                         lstProducts[p],
				                                                         lstPriZones[z],
				                                                         null,
				                                                         null,
				                                                         null,
				                                                         null,
				                                                         condClssRec.TRM_DistributionChannel__c,
				                                                         condClssRec.Name+'R'+acum++,
				                                                         mapStructures,
				                                                         mapCatalogs);
                    lstConditionRec.add(objCondition);
                }    
            } 
        }
    
        return lstConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by segment-office
    * 
    * @param    lstProducts          List of selected products
    * @param    lstSegments          List of segments selected
    * @param    lstSalesOff          List of sales office selected
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchBySegmentOffice(ONTAP__Product__c[] lstProducts,
														  String[] lstSegments,
														  String[] lstSalesOff,
														  TRM_ConditionClass__c condClssRec,
														  TRM_ConditionRelationships__mdt mtdRelation,
														  TRM_ConditionsManagement__mdt mtdMngRecord,
														  Map<String,String> mapStructures,
														  Map<String,String> mapCatalogs){

        System.debug('#_#SearchBySegmentOffice#_#');
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        if(lstSegments.size() > 0){
            for(Integer p=0; p<lstProducts.size(); p++){
                for (Integer z=0; z<lstSalesOff.size(); z++) {
                    for (Integer s=0; s<lstSegments.size(); s++) {
                        TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                              mtdRelation,
				                                                              mtdMngRecord.TRM_Customers__c,
				                                                              lstProducts[p],
				                                                              null,
				                                                              lstSegments[s],
				                                                              lstSalesOff[z],
				                                                              null,
				                                                              null,
				                                                              condClssRec.TRM_DistributionChannel__c,
				                                                              condClssRec.Name+'R'+acum++,
				                                                              mapStructures,
				                                                              mapCatalogs);
                        lstConditionRec.add(objCondition);
                    }
                }    
            }
        }else{
            for(Integer p=0; p<lstProducts.size(); p++){
                for (Integer z=0; z<lstSalesOff.size(); z++) {
                    TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                          mtdRelation,
				                                                          mtdMngRecord.TRM_Customers__c,
				                                                          lstProducts[p],
				                                                          null,
				                                                          null,
				                                                          lstSalesOff[z],
				                                                          null,
				                                                          null,
				                                                          condClssRec.TRM_DistributionChannel__c,
				                                                          condClssRec.Name+'R'+acum++,
				                                                          mapStructures,
				                                                          mapCatalogs);
                    lstConditionRec.add(objCondition);
                }    
            }
        }

        return lstConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by Zone
    * 
    * @param    lstProducts          List of selected products
    * @param    lstPriZones          List of price zones selected
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchByZone(ONTAP__Product__c[] lstProducts,
												String[] lstPriZones,
												TRM_ConditionClass__c condClssRec,
												TRM_ConditionRelationships__mdt mtdRelation,
												TRM_ConditionsManagement__mdt mtdMngRecord,
												Map<String,String> mapStructures,
												Map<String,String> mapCatalogs){

        System.debug('#_#SearchByZone#_#'+lstPriZones);
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        for(Integer p=0; p < lstProducts.size(); p++){
            for (Integer z=0; z < lstPriZones.size(); z++){
                TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                     mtdRelation,
				                                                     mtdMngRecord.TRM_Customers__c,
				                                                     lstProducts[p],
				                                                     lstPriZones[z],
				                                                     null,
				                                                     null,
				                                                     null,
				                                                     null,
				                                                     condClssRec.TRM_DistributionChannel__c,
				                                                     condClssRec.Name+'R'+acum++,
				                                                     mapStructures,
				                                                     mapCatalogs);
                lstConditionRec.add(objCondition);
            }    
        }

        return lstConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by Organization
    * 
    * @param    lstProducts          List of selected products
    * @param    lstSalesOrg          List of sales org selected
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchByOrganization(ONTAP__Product__c[] lstProducts,
														 String[] lstSalesOrg,
														 TRM_ConditionClass__c condClssRec,
														 TRM_ConditionRelationships__mdt mtdRelation,
														 TRM_ConditionsManagement__mdt mtdMngRecord,
														 Map<String,String> mapStructures,
														 Map<String,String> mapCatalogs){

        System.debug('#_#SearchByOrganization#_#');
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        for(Integer p=0; p<lstProducts.size(); p++){
            for (Integer z=0; z<lstSalesOrg.size(); z++) {
                TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                      mtdRelation,
				                                                      mtdMngRecord.TRM_Customers__c,
				                                                      lstProducts[p],
				                                                      null,
				                                                      null,
				                                                      null,
				                                                      lstSalesOrg[z],
				                                                      null,
				                                                      condClssRec.TRM_DistributionChannel__c,
				                                                      condClssRec.Name+'R'+acum++,
				                                                      mapStructures,
				                                                      mapCatalogs);
                lstConditionRec.add(objCondition);
            }    
        }

        return lstConditionRec;
	}

	/**
    * @description  Method that allows generating conditions by Office
    * 
    * @param    lstProducts          List of selected products
    * @param    lstSalesOff          List of sales office selected
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * 
    * @return   TRM_ConditionRecord__c[] List of generated condition records
    */
	public TRM_ConditionRecord__c[] SearchByOffice(ONTAP__Product__c[] lstProducts,
												   String[] lstSalesOff,
												   TRM_ConditionClass__c condClssRec,
												   TRM_ConditionRelationships__mdt mtdRelation,
												   TRM_ConditionsManagement__mdt mtdMngRecord,
												   Map<String,String> mapStructures,
												   Map<String,String> mapCatalogs){

        System.debug('#_#SearchByOffice#_#');
        Integer acum = Integer.valueOf(condClssRec.TRM_TotalConditionRecords__c);
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        TRM_ProductScales__c[] lstConditionScale = new List<TRM_ProductScales__c>();
        for(Integer p=0; p<lstProducts.size(); p++){
            for (Integer z=0; z<lstSalesOff.size(); z++) {
                TRM_ConditionRecord__c objCondition = conditionRecord(condClssRec,
				                                                      mtdRelation,
				                                                      mtdMngRecord.TRM_Customers__c,
				                                                      lstProducts[p],
				                                                      null,
				                                                      null,
				                                                      lstSalesOff[z],
				                                                      null,
				                                                      null,
				                                                      condClssRec.TRM_DistributionChannel__c,
				                                                      condClssRec.Name+'R'+acum++,
				                                                      mapStructures,
				                                                      mapCatalogs);
                lstConditionRec.add(objCondition);
            }    
        }
    
        return lstConditionRec;
    }
}