global class ISSM_CAM_UpsertInventory_sch implements Schedulable {
	global void execute(SchedulableContext sc) {
		String strQuery = 'SELECT CreatedDate, Id FROM ISSM_Asset_Inventory__c';
		System.debug(loggingLevel.Error, '*** strQuery: ' + strQuery);
		ISSM_CAM_UpsertInventory_bch uptInventory = new ISSM_CAM_UpsertInventory_bch(strQuery);
		database.executebatch(uptInventory,500);
	}
}