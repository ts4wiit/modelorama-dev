/***********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto: ISSM  AB Int Bev (Customer Service)
Descripción: CLase que realiza la insercion de caso 
Clase de prueba: ISSM_CreateCaseGlobal_tst
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    25-Agosto-2017   Hector Diaz (HD)     Creador.
1.1    26-Abril-2018    Leopoldo Ortega      Se ajustó la asignación del caso de los roles "Trade Marketing" y "Billing Manager".
2.0    07-Junio-2018    Leopoldo Ortega (LO) Process CAM & CS - Refrigeration Process
***********************************************************************************/
public class ISSM_CreateCaseGlobal_cls { 

    public static Case CreateCase(String strOptionSelectedLevel1,String strOptionSelectedLevel2,String strOptionSelectedLevel3,String strOptionSelectedLevel4,
                                  String strOptionSelectedLevel5, String strOptionSelectedLevel6,Boolean blnUpdateAccountTeam,RecordType rt,ISSM_TypificationMatrix__c typificationMatrixResult,
                                  Id ownerId, Id ownerIdForce,Account accEmail,String strAccountID , String CaseIdRe,String strDescription, String asset_Id, Integer intStatusNoSerie) {
                                      
                                      try {
                                          Case ObjCase = new Case (
                                              ISSM_TypificationLevel1__c  = strOptionSelectedLevel1,
                                              ISSM_TypificationLevel2__c  = strOptionSelectedLevel2,
                                              ISSM_TypificationLevel3__c  = strOptionSelectedLevel3,
                                              ISSM_TypificationLevel4__c  = strOptionSelectedLevel4,
                                              ISSM_TypificationLevel5__c  = strOptionSelectedLevel5,
                                              ISSM_TypificationLevel6__c  = strOptionSelectedLevel6,
                                              ISSM_UpdateAccountTeam__c   = blnUpdateAccountTeam,
                                              RecordTypeID                = rt.Id,
                                              ISSM_TypificationNumber__c  = typificationMatrixResult.Id,
                                              EntitlementId               = typificationMatrixResult.ISSM_Entitlement__c,
                                              ISSM_Email1CommunicationLevel3__c= typificationMatrixResult.ISSM_Email1CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel3__c: null,
                                              ISSM_Email1CommunicationLevel4__c= typificationMatrixResult.ISSM_Email1CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel4__c: null,
                                              ISSM_Email2CommunicationLevel3__c= typificationMatrixResult.ISSM_Email2CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel3__c: null,
                                              ISSM_Email2CommunicationLevel4__c= typificationMatrixResult.ISSM_Email2CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel4__c: null,
                                              ISSM_Email3CommunicationLevel3__c= typificationMatrixResult.ISSM_Email3CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel3__c: null,
                                              ISSM_Email3CommunicationLevel4__c= typificationMatrixResult.ISSM_Email3CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel4__c: null,
                                              ISSM_Email4CommunicationLevel4__c= typificationMatrixResult.ISSM_Email4CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email4CommunicationLevel4__c: null,
                                              OwnerId                     = ownerId,
                                              ISSM_OwnerCaseForce__c      = ownerIdForce,
                                              ISSM_FirstUserOwner__c       = ownerIdForce,
                                              Priority                     = typificationMatrixResult.ISSM_Priority__c,
                                              SuppliedEmail               = accEmail.ONTAP__Email__c,
                                              AccountID = strAccountID,
                                              Id = CaseIdRe,
                                              Description = strDescription,
                                              ISSM_Asset_CAM__c = asset_Id,
                                              ISSM_StatusSerialNumber__c = intStatusNoSerie
                                          );
                                          return ObjCase;
                                      } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' &&& ' + 'LINE NUMBER: ' + ex.getLineNumber() + 'asset_Id: ' + asset_Id); return null; }
                                  }
    
    public static Case CreateCaseCAM(String strOptionSelectedLevel1,String strOptionSelectedLevel2,String strOptionSelectedLevel3,String strOptionSelectedLevel4,
                                  String strOptionSelectedLevel5, String strOptionSelectedLevel6,Boolean blnUpdateAccountTeam,RecordType rt,ISSM_TypificationMatrix__c typificationMatrixResult,
                                  Id ownerId, Id ownerIdForce,Account accEmail,String strAccountID , String CaseIdRe,String strDescription, String asset_Id, Integer intStatusNoSerie, String rtCAM) {
                                      
                                      try {
                                          Case ObjCase = new Case (
                                              ISSM_TypificationLevel1__c  = strOptionSelectedLevel1,
                                              ISSM_TypificationLevel2__c  = strOptionSelectedLevel2,
                                              ISSM_TypificationLevel3__c  = strOptionSelectedLevel3,
                                              ISSM_TypificationLevel4__c  = strOptionSelectedLevel4,
                                              ISSM_TypificationLevel5__c  = strOptionSelectedLevel5,
                                              ISSM_TypificationLevel6__c  = strOptionSelectedLevel6,
                                              ISSM_UpdateAccountTeam__c   = blnUpdateAccountTeam,
                                              RecordTypeID                = rt.Id,
                                              IdRecordTypeCaseForce__c = rtCAM,
                                              ISSM_TypificationNumber__c  = typificationMatrixResult.Id,
                                              EntitlementId               = typificationMatrixResult.ISSM_Entitlement__c,
                                              ISSM_Email1CommunicationLevel3__c= typificationMatrixResult.ISSM_Email1CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel3__c: null,
                                              ISSM_Email1CommunicationLevel4__c= typificationMatrixResult.ISSM_Email1CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel4__c: null,
                                              ISSM_Email2CommunicationLevel3__c= typificationMatrixResult.ISSM_Email2CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel3__c: null,
                                              ISSM_Email2CommunicationLevel4__c= typificationMatrixResult.ISSM_Email2CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel4__c: null,
                                              ISSM_Email3CommunicationLevel3__c= typificationMatrixResult.ISSM_Email3CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel3__c: null,
                                              ISSM_Email3CommunicationLevel4__c= typificationMatrixResult.ISSM_Email3CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel4__c: null,
                                              ISSM_Email4CommunicationLevel4__c= typificationMatrixResult.ISSM_Email4CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email4CommunicationLevel4__c: null,
                                              OwnerId                     = ownerId,
                                              ISSM_OwnerCaseForce__c      = ownerIdForce,
                                              ISSM_FirstUserOwner__c       = ownerIdForce,
                                              Priority                     = typificationMatrixResult.ISSM_Priority__c,
                                              SuppliedEmail               = accEmail.ONTAP__Email__c,
                                              AccountID = strAccountID,
                                              Id = CaseIdRe,
                                              Description = strDescription,
                                              ISSM_Asset_CAM__c = asset_Id,
                                              ISSM_StatusSerialNumber__c = intStatusNoSerie
                                          );
                                          return ObjCase;
                                      } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' &&& ' + 'LINE NUMBER: ' + ex.getLineNumber() + 'asset_Id: ' + asset_Id); return null; }
                                  }
    
    
    //Metodo para obtener el Id del tipo de registro por metido del Developer Name  
    //@Param :  String Developer Name del Tipo de registro.
    public static String ObtanRecordTypeDeveloperN(String DeveloperName){
        String RecordtTypeId = '';
        try {
            //RecordType rt = [SELECT Id,Name FROM RecordType WHERE DeveloperName  =: DeveloperName LIMIT 1];
            RecordType rt = ISSM_CreateCaseGlobal_cls.getRecordType(DeveloperName);
            if(rt != null){
                RecordtTypeId = rt.Id;
            }
        } catch(Exception e) { System.debug('Exception : '+e); }
        
        return RecordtTypeId; 
    } 
    
    public static RecordType getRecordType(String DeveloperName){
        String RecordtTypeId = '';
        try {
            return [SELECT Id,Name FROM RecordType WHERE DeveloperName  =: DeveloperName LIMIT 1];
        } catch(Exception e) { System.debug('Exception : '+e); return null; }
    }
    
    public static String getRecordType_str(String DeveloperName){
        String RecordtTypeId = '';
        List<RecordType> rt_lst = new List<RecordType>();
        try {
            rt_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName =: DeveloperName LIMIT 1];
            if (rt_lst.size() > 0) {
                for (RecordType rt : rt_lst) {
                    return rt.Id;
                }
            } return null;
        } catch(Exception e) { System.debug('Exception : '+e); return null; }
    }
    
    public static Case getCaseCustomerService(ISSM_TypificationMatrix__c typicationMatrix, 
		Account accEmail,ISSM_AppSetting_cs__c OISSMCS, 
      	RecordType recordType, Boolean blnUpdateAccountTeam, String CaseIdRe, String strDescriptionTextArea, String strNumSerieText, Integer intStatusNoSerie, String rtCAM) {
            try {
                Id ownerId = ISSM_CreateCaseGlobal_cls.getOwner(typicationMatrix, OISSMCS);
                //Account accEmail = new ISSM_CustomerServiceQuerys_cls().QueryAccount(accountId);
                WrOwnerIdCaseForce caseForceOwner = ISSM_CreateCaseGlobal_cls.getForceOwner(typicationMatrix, accEmail);
                Id ownerIdForce;
                Boolean blnUpdateAccountTeamFinal = false;
                
                // Id del asset
                String asset_Id = '';
                
                if(caseForceOwner != null){
                    ownerIdForce = caseForceOwner.caseOwnerId;
                    blnUpdateAccountTeamFinal = blnUpdateAccountTeam ? caseForceOwner.blnUpdateAccountTeam : false;
                }
                
                if(ownerId == null || (ownerId != null && String.isEmpty(ownerId))) {
                    ownerId =  OISSMCS.ISSM_IdQueueWithoutOwner__c;
                }
                
                // Recuperamos el id del asset seleccionado en la pantalla de tipificación
                List<ONTAP__Account_Asset__c> lstAccountAssets = new List<ONTAP__Account_Asset__c>();
                if ((strNumSerieText != null) && (strNumSerieText != '')) {
                    lstAccountAssets = [SELECT Id, Name, ISSM_Asset_CAM__c, ISSM_Asset_CAM__r.ISSM_Serial_Number__c, ISSM_Asset_CAM__r.ISSM_Provider_Serial_Number__c
                                        FROM ONTAP__Account_Asset__c
                                        WHERE ((ISSM_Asset_CAM__r.ISSM_Serial_Number__c =: strNumSerieText)
                                               OR (ISSM_Asset_CAM__r.ISSM_Provider_Serial_Number__c =: strNumSerieText))
                                        LIMIT 10000];
                    if (lstAccountAssets.size() > 0) { for (ONTAP__Account_Asset__c reg : lstAccountAssets) { asset_Id = reg.ISSM_Asset_CAM__c; }
                    } else { asset_Id = null; }
                } else { asset_Id = null; }
                
                // Valida que el equipo exista en Salesforce
                // asset_Id = asset_Id != null ? asset_Id : 'a0q1F000000KQP1';
                
                if (rtCAM == '') {
                    Case returnCase = ISSM_CreateCaseGlobal_cls.CreateCase (
                        typicationMatrix.ISSM_TypificationLevel1__c,
                        typicationMatrix.ISSM_TypificationLevel2__c,
                        typicationMatrix.ISSM_TypificationLevel3__c,
                        typicationMatrix.ISSM_TypificationLevel4__c,
                        typicationMatrix.ISSM_TypificationLevel5__c,
                        typicationMatrix.ISSM_TypificationLevel6__c,
                        blnUpdateAccountTeamFinal,
                        recordType,
                        typicationMatrix,
                        ownerId,
                        ownerIdForce,
                        accEmail,
                        accEmail.Id,
                        CaseIdRe,
                        strDescriptionTextArea,
                        asset_Id,
                        intStatusNoSerie
                    ); return returnCase;
                } else {
                    Case returnCase = ISSM_CreateCaseGlobal_cls.CreateCaseCAM (
                        typicationMatrix.ISSM_TypificationLevel1__c,
                        typicationMatrix.ISSM_TypificationLevel2__c,
                        typicationMatrix.ISSM_TypificationLevel3__c,
                        typicationMatrix.ISSM_TypificationLevel4__c,
                        typicationMatrix.ISSM_TypificationLevel5__c,
                        typicationMatrix.ISSM_TypificationLevel6__c,
                        blnUpdateAccountTeamFinal,
                        recordType,
                        typicationMatrix,
                        ownerId,
                        ownerIdForce,
                        accEmail,
                        accEmail.Id,
                        CaseIdRe,
                        strDescriptionTextArea,
                        asset_Id,
                        intStatusNoSerie,
                        rtCAM
                    ); return returnCase;
                }
            } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());  return null; }
	}
    
    public static Id getOwner(ISSM_TypificationMatrix__c typicationMatrix, ISSM_AppSetting_cs__c OISSMCS){
        Id idReturn = null;
        Account salesOffice = null;
        Account accountData = null;
        
        String IdQueueProviders_str = '';
        List<ISSM_AppSetting_cs__c> lstAppSettings = new List<ISSM_AppSetting_cs__c>();
        lstAppSettings  =  [SELECT ISSM_IdQueueWithoutOwner__c, ISSM_IdQueueFriendModel__c, ISSM_IdProviderWithOutMail__c, ISSM_IdQueueTelecolletion__c, ISSM_IdQueueProviders__c
                            FROM ISSM_AppSetting_cs__c 
                            LIMIT 1];
        if (lstAppSettings.size() > 0) {
            for (ISSM_AppSetting_cs__c reg : lstAppSettings) {
                IdQueueProviders_str = reg.ISSM_IdQueueProviders__c;
            }
        }
        
        if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.QUEUE) {
            idReturn = typicationMatrix.ISSM_IdQueue__c;
        }
        else if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BOSSREFRIGERATION) {
            idReturn = IdQueueProviders_str;
        }
        else if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.USER 
                  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.SUPERVISOR 
                  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BILLINGMANAGER 
                  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.TRADEMARKETING
                  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
                  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO) {
                      idReturn = OISSMCS.ISSM_IdQueueFriendModel__c; } else { idReturn = ''; }
        
        return idReturn;
        
    }
    
    public static WrOwnerIdCaseForce getForceOwner(ISSM_TypificationMatrix__c typicationMatrix, Account accountData){
        Id idReturn = null;
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        List<Id> ids = null;
        ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
        //Account accountData = accountCls.getAccount(accountId); 
        Boolean blnUpdateAccountTeam = false;
        List<Account> lstAccounts = new List<Account>();
        //Account salesParnert = accountCls.getParnertAccount(accountId); 
        if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.USER){
            idReturn = typicationMatrix.ISSM_OwnerUser__c;
        } 
        if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.QUEUE){
            idReturn = accountData.OwnerId;
        }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.SUPERVISOR 
           || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
           || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
               
               //accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountId);
               if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
                      ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.ISSM_SalesOffice__r.Id); } else { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id); }
               if (ids != null && ids.size() > 0) { idReturn = ids.get(0); }
               else {
                   idReturn = accountData.OwnerId;
                   blnUpdateAccountTeam = true;
               }
           } 
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BILLINGMANAGER){
            if(accountData.ISSM_SalesOffice__c != null) {
                ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.BILLINGMANAGER, accountData.Id);
                if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else { idReturn = accountData.OwnerId; }
        }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BOSSREFRIGERATION) {
            if (accountData.ISSM_SalesOffice__c != null) {
                String recordTypeAccountProvider = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Provider_RecordType);
                lstAccounts = ISSM_AccountSelector_cls.newInstance().selectByMultipleFields(recordTypeAccountProvider, typicationMatrix.ISSM_TypificationLevel3__c, typicationMatrix.ISSM_TypificationLevel4__c, accountData.ISSM_SalesOffice__c);
                
                if (lstAccounts != null && !lstAccounts.isEmpty() && lstAccounts[0] != null) {
                    if (lstAccounts[0].ISSM_BossRefrigeration__c != null && String.IsnotBlank(lstAccounts[0].ISSM_BossRefrigeration__c)) { idReturn = lstAccounts[0].ISSM_BossRefrigeration__c; } else {
                        ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id);
                        if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; }
                    }
                } else {
                    ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id);
                    if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; }
                }
            } else { idReturn = accountData.OwnerId; }
        }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.TRADEMARKETING) {
            if (accountData.ISSM_SalesOffice__c != null) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.TRADEMARKETING, accountData.Id); if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else {  idReturn = accountData.OwnerId; }
        }
        System.debug('##### : idReturn:' +idReturn);
        return new WrOwnerIdCaseForce(blnUpdateAccountTeam, idReturn);//idReturn;
    }
    
    class WrOwnerIdCaseForce{
        Boolean blnUpdateAccountTeam {get; private set;}
        Id caseOwnerId {get; private set;}
        
        public WrOwnerIdCaseForce(Boolean blnUpdateAccountTeamParam, Id caseOwnerIdParam){
            blnUpdateAccountTeam = blnUpdateAccountTeamParam;
            caseOwnerId = caseOwnerIdParam;
        }
        
    }
    
}