/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to get external order items

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OrderItemExtern_cls {

	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setOrderNumber set of setOrderNumber id to request
	 */
	public static List<salesforce_ontap_order_item_c__x> getOrderItemsExten(Set<String> setOrderNumber){
		if(test.isRunningTest()){
			List<salesforce_ontap_order_item_c__x> orderItemExt_lst = new List<salesforce_ontap_order_item_c__x>();
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003117',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003118',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003119',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003121',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003122',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003123',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003124',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			orderItemExt_lst.add(new salesforce_ontap_order_item_c__x(ontap_productid_c__c = '0003003120',ontap_customerorder_c__c='0100152874',oncall_sap_order_number_c__c='0100152874'));
			return orderItemExt_lst;
		}else{
			return [SELECT issm_countrycode_c__c,issm_hectolitersvolumen_c__c,issm_itemproductname_c__c,
				issm_ordertype_c__c,issm_presentation_c__c,issm_salesoffid_c__c,issm_sap_number_c__c,
				issm_sector_c__c,issm_taxamount_c__c,issm_uniqueid_c__c,name__c,oncall_brand_c__c,
				oncall_oncall_order_c__c,oncall_oncall_order_r_oncall_sap_order_n__c,
				oncall_oncall_quantity_c__c,oncall_sap_order_number_c__c,ontap_actualquantity_c__c,
				ontap_billtosapcustomerid_c__c,ontap_customerorder_c__c,ontap_customerorder_r_oncall_sap_order_n__c,
				ontap_itemid_c__c,ontap_itemproduct_c__c,ontap_lineamount_c__c,ontap_netvalue_c__c,
				ontap_productid_c__c,ontap_product_description_c__c,ontap_taxvalue_c__c 
				FROM salesforce_ontap_order_item_c__x WHERE oncall_sap_order_number_c__c in :setOrderNumber];
		}
		
	}
}