/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 28: Validation for KONDA
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       16-05-2018   	Iván Neria    			Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0028_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0028_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            
            for(MDM_FieldByRule__c field : uniqueFields(rule.MDM_Fields_by_Rule__r).Values()){
                //Validation for start proccess when the record is SH 
                    if(field.MDM_IsActive__c && (toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z001)) {
                        Map<String, CDM_Temp_Interlocutor__c> mapInter = MDM_Account_cls.getInterlocutors(setsGranularity, Label.CDM_Apex_SH);
                            	for(MDM_Temp_Account__c child: MDM_Account_cls.getR0028(MDM_Account_cls.getKUNN2r25()).values()){
                                        Boolean hasErrorSH =false;
                                        String errorFields='';
                                        if(child.KONDA__c != toEvaluate.get(field.MDM_APIFieldName__c)){
                                            hasErrorSH=true;
                                            errorFields=errorFields+ ' '+Label.CDM_Apex_KONDA_c+',';
                                        }
                                        if(child.PLTYP__c != toEvaluate.get(Label.CDM_Apex_PLTYP_c)){
                                            hasErrorSH=true;
                                            errorFields=errorFields+ ' '+Label.CDM_Apex_PLTYP_c+',';
                                        }
                                        if(child.KDGRP__c != toEvaluate.get(Label.CDM_Apex_KDGRP_c)){
                                            hasErrorSH=true;
                                            errorFields=errorFields+ ' '+Label.CDM_Apex_KDGRP_c+',';
                                        }
                                        if(child.KVGR3__c != toEvaluate.get(Label.CDM_Apex_KVGR3_c)){
                                            hasErrorSH=true;
                                            errorFields=errorFields+ ' '+Label.CDM_Apex_KVGR3_c+',';
                                        }
                                        if(child.KVGR5__c != toEvaluate.get(Label.CDM_Apex_KVGR5_c)){
                                            hasErrorSH=true;
                                            errorFields=errorFields+ ' '+Label.CDM_Apex_KVGR5_c+',';
                                        }
                                        if(hasErrorSH){
                                            fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  			validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c+child.Id_External__c);
                        		  			validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c +errorFields+ Label.CDM_Apex_Error10+' '+child.Id_External__c);
                        		  			validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  			validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        		  			validationResults.add((MDM_Validation_Result__c)validationResult);
                                            validationResult = new MDM_Validation_Result__c();
                							validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
                                            hasErrorSH = false;
                                        }
                        		} 
                }
                
            }
            
        }
        return validationResults;
    }
}