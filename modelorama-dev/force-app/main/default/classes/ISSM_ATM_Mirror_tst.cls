/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_ATM_Mirror_tst
Versión : 1.0
Fecha de Creación : 20 Abril 2018
Funcionalidad : Clase de prueba para el helper ISSM_ATM_Mirror_thr del trigger ISSM_ATM_Mirror_tgr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Abril - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_ATM_Mirror_tst {
    static testmethod void manageATMMirror() {

        List<ISSM_ATM_Mirror__c> ATMMirrorUpdUser_lst = new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATMMirrorUpdEstatus_lst = new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATMMirrorUpdEstatus5_lst = new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATMMirrorUpdRol_lst = new List<ISSM_ATM_Mirror__c>();
        List<RecordType> RecordTypeSO_lst = new List<RecordType>();
        List<RecordType> RecordTypeOrg_lst = new List<RecordType>();
        
        /*AsyncApexJob apexJob = new AsyncApexJob();
        apexJob.ApexClass.Name = 'ISSM_AssignUserToATMInAccount_bch';
        insert apexJob;*/

        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@testorg.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@testorg.com');
        insert user1;

        User user2 = new User(Alias = 'admin', Email='systemadmin@testorg.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin2@testorg.com');
        insert user2;
        
        Id RecordTypeIdOrg_id;
        RecordTypeOrg_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOrg' LIMIT 1];
        for (RecordType rt : RecordTypeOrg_lst) { RecordTypeIdOrg_id = rt.Id; }
        Account accOrg = new Account(Name = 'Org 1', RecordTypeId = RecordTypeIdOrg_id);
        insert accOrg;

        Id RecordTypeIdSO_id;
        RecordTypeSO_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
        for (RecordType rt : RecordTypeSO_lst) { RecordTypeIdSO_id = rt.Id; }
        
        Account acc = new Account(Name = 'SalesOffice 1', RecordTypeId = RecordTypeIdSO_id, ParentId = accOrg.Id);
        insert acc;
        
        ISSM_Manager_ATM__c managerATM1 = new ISSM_Manager_ATM__c();
        managerATM1.ISSM_Role__c = 'Supervisor';
        managerATM1.ISSM_Manager__c = 'Gerente de ventas';
        insert managerATM1;
        
        ISSM_Manager_ATM__c managerATM2 = new ISSM_Manager_ATM__c();
        managerATM2.ISSM_Role__c = 'Modelorama';
        managerATM2.ISSM_Manager__c = 'Gerente general';
        insert managerATM2;

        ISSM_ATM_Mirror__c atmMirror1 = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user1.Id, Oficina_de_Ventas__c = acc.Id, Rol__c = 'Supervisor', Estatus__c = 'Activo');
        insert atmMirror1;
        
        ISSM_ATM_Mirror__c atmMirror5 = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user1.Id, Oficina_de_Ventas__c = acc.Id, Rol__c = 'Gerente', Estatus__c = 'Activo');
        insert atmMirror5;

        test.startTest();

            atmMirror1.Usuario_relacionado__c = user2.Id;
            ATMMirrorUpdUser_lst.add(atmMirror1);

            if (ATMMirrorUpdUser_lst.size() > 0) {
                update ATMMirrorUpdUser_lst;
            }

            atmMirror1.Estatus__c = 'Inactivo';
            ATMMirrorUpdEstatus_lst.add(atmMirror1);
            if (ATMMirrorUpdEstatus_lst.size() > 0) {
                update ATMMirrorUpdEstatus_lst;
            }
        
        	atmMirror5.Estatus__c = 'Inactivo';
            ATMMirrorUpdEstatus5_lst.add(atmMirror5);
            if (ATMMirrorUpdEstatus5_lst.size() > 0) {
                update ATMMirrorUpdEstatus5_lst;
            }
        
            ISSM_ATM_Mirror__c atmMirror2 = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user1.Id, Oficina_de_Ventas__c = acc.Id, Rol__c = 'Supervisor', Estatus__c = 'Activo');
            insert atmMirror2;
            
            ISSM_ATM_Mirror__c atmMirror3 = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user1.Id, Oficina_de_Ventas__c = acc.Id, Rol__c = 'Gerente', Estatus__c = 'Activo');
            insert atmMirror3;
        
        	ISSM_ATM_Mirror__c atmMirror4 = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user1.Id, Oficina_de_Ventas__c = accOrg.Id, Rol__c = 'Gerente General', Estatus__c = 'Activo');
            insert atmMirror4;

            atmMirror2.Rol__c = 'Trade Marketing';
            ATMMirrorUpdRol_lst.add(atmMirror2);

            if (ATMMirrorUpdRol_lst.size() > 0) {
                update ATMMirrorUpdRol_lst;
            }

        test.stopTest();
    }
}