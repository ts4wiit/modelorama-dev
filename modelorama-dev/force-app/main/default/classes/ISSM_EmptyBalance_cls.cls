/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice empty balance

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
	1.1.	  03-10-2017	  Wilmer Piedrahita / Carlos Duque	New filters added on fixes 20171002 and 20171003
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_EmptyBalance_cls implements ISSM_ObjectInterface_cls{
	
	public List<ONTAP__EmptyBalance__c> accountsEmpties;
	private List<salesforce_ontap_emptybalance_c__x> emptiesDelete;
	
	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  Map<String, Account> mapAccountParam
	 */
	public ISSM_EmptyBalance_cls(List<salesforce_ontap_emptybalance_c__x> accountEmptiesExt, Map<String, Account> mapAccount) {
		accountsEmpties = getList(accountEmptiesExt, mapAccount);
	}

	/**
	 * Class constructor		                             
	 */
	public ISSM_EmptyBalance_cls() {
	}

	/**
	 * Create Objects to sincronice		                             
	 * Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObject(Map<String, Account> mapAccount){
		accountsEmpties = getList(null, mapAccount);
	}

	/**
	 * Create objects to delete		                             
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObjectDelete(Map<String, Account> mapAccount){
		emptiesDelete = ISSM_EmptyBalanceExtern_cls.getList(mapAccount.keySet(), true);
	}

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public List<sObject> getList(List<sObject> accountEmpties, Map<String, Account> mapAccount){
		Map<String, ONTAP__Product__c> mapProducts = new Map<String, ONTAP__Product__c>();
		for(ONTAP__Product__c  prod_obj : [SELECT Id,ONTAP__ProductId__c FROM ONTAP__Product__c]){
			mapProducts.put(prod_obj.ONTAP__ProductId__c,prod_obj);
		}
		List<ONTAP__EmptyBalance__c> accountsEmptiesReturn;
		ONTAP__EmptyBalance__c acctEmpty;
		
		// =================================================================
        // WP 20170930: FIX to filter accounts that don't have active Tours between the last 7 days and in 7 days in future to avoid impact to iRep project
        
        Set<String> accountSap = new Set<String>();
		Set<String> accountSapToExclude = new Set<String>();
        for(ONTAP__Tour_Visit__c activeTourVisit : [SELECT ONTAP__Account__r.ONTAP__SAPCustomerId__c 
													FROM ONTAP__Tour_Visit__c 
													WHERE ONTAP__Account__r.ONTAP__SAPCustomerId__c in:mapAccount.keySet()
													AND (ONTAP__Tour__r.ONTAP__IsActive__c = TRUE OR 
														ONTAP__Tour__r.ONTAP__TourStatus__c = 'Downloading')
													AND (ONTAP__Tour__r.ONTAP__TourDate__c >= LAST_N_DAYS:7
													AND ONTAP__Tour__r.ONTAP__TourDate__c <= NEXT_N_DAYS:7)
													]){
               accountSapToExclude.add(activeTourVisit.ONTAP__Account__r.ONTAP__SAPCustomerId__c);
         }
        
        // Removes customers with active or pending iRep visits, mapAccount will keep only the requiered accounts until this step
		for(String strAccountSAPCustomerId : accountSapToExclude){
        	mapAccount.remove(strAccountSAPCustomerId);
        }
        
		// accountSap = mapAccount.keySet(); 

        // WP 20170930: FIX ENDING
		// =================================================================

		// =================================================================
		// WP 20171003: FIX Filter accounts that are not part of the pending calls or visits on current day
		// Customers with pending calls
		for(ONCALL__Call__c pendingCall : [SELECT Id, OwnerId, Name, RecordTypeId, ONCALL__Call_Status__c, ONCALL__Date__c, ONCALL__POC__c, ONCALL__POC__r.ONTAP__SAPCustomerId__c 
													FROM ONCALL__Call__c
													WHERE ONCALL__Call_Status__c = 'Pending to call' 
													AND ONCALL__Date__c = TODAY
													AND ONCALL__POC__r.ONTAP__SAPCustomerId__c IN :mapAccount.keySet()
													]){
               accountSap.add(pendingCall.ONCALL__POC__r.ONTAP__SAPCustomerId__c);
         }

		 // As the Event object doesn't have access to SAP Customer Id field on Account object, it is necessary to get the Account's record id
		 map<id, string> accountIdBySAPCustomerId = new map<id, string>();
		 for(Account acc : mapAccount.values()){
			accountIdBySAPCustomerId.put(acc.Id, acc.ONTAP__SAPCustomerId__c);
		 }
		
		// Customers with pending BDR Visits
		String strRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
		for(Event pendingVisit : [SELECT Id, OwnerId, WhatId, ActivityDate 
									FROM Event
									WHERE ONTAP__Estado_de_visita__c = :Label.ISSM_Pending //'Pending' 
									AND ActivityDate = TODAY
									AND Subject = :Label.ISSM_Visit_Subject //'Visit'
									AND RecordTypeId = :strRecordTypeId // ('0122F0000008Xd2QAE') 
									AND WhatId IN :accountIdBySAPCustomerId.keySet()
								 ]){
               accountSap.add(accountIdBySAPCustomerId.get(pendingVisit.WhatId));
         }

		 // If customer was not found in the list of customers with pending calls or BDR visits for the day, then it must be remove from the set of accounts to load 360 data
		for(String strAccountSAPCustId : mapAccount.keySet()){
        	if(!accountSap.contains(strAccountSAPCustId))
				mapAccount.remove(strAccountSAPCustId);
        }

		// WP 20171003: END OF FIX
		// =================================================================
        
		accountsEmptiesReturn = new List<ONTAP__EmptyBalance__c>();
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByName().get('ONTAP_Compact').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByName().get('ONTAP_Compact').getRecordTypeId();
		}
		if(test.isRunningTest()){
			for(Integer i = 0;i<15;i++){
				salesforce_ontap_emptybalance_c__x accountEmptyExt = new salesforce_ontap_emptybalance_c__x(
					ontap_emptybalancekey_c__c = '0003003117',
					isdeleted__c=false, 
					ontap_sapcustomerid_c__c='000000000'+i,
					ontap_balance_c__c = 35,
					systemmodstamp__c = System.today());
				accountsEmptiesReturn.add(getEmpty(mapAccount , accountEmptyExt, devRecordTypeId, mapProducts));
			}
		} else{
			for(SObject accountEmptyExt: [SELECT systemmodstamp__c, 
			ontap_sapcustomerid_c__c, ontap_balance_c__c, ontap_duedate_c__c, ontap_emptybalancekey_c__c,
			ontap_product_c_ontap_productid_c__c, ontap_productid_c__c, issm_sap_number_c__c FROM salesforce_ontap_emptybalance_c__x 
			WHERE ontap_sapcustomerid_c__c in :accountSap and isdeleted__c = false and systemmodstamp__c >= YESTERDAY]){
				accountsEmptiesReturn.add(getEmpty(mapAccount , accountEmptyExt, devRecordTypeId, mapProducts));
			}
		}
		System.debug('### ISSM_EmptyBalance_cls ListSize: ' + accountsEmptiesReturn.size());
		return accountsEmptiesReturn;
	}

	/**
	 * Get asset record		                             
	 * @param  Map<String, Account> mapAccount
	 * @param  sObject accountEmptyExt
	 * @param  Id devRecordTypeId
	 * @param  Map<String, ONTAP__Product__c> mapProducts
	 */
	private ONTAP__EmptyBalance__c getEmpty(Map<String, Account> mapAccount , sObject accountEmptyExt, Id devRecordTypeId, Map<String, ONTAP__Product__c> mapProducts){
		ONTAP__EmptyBalance__c acctEmpty;
		acctEmpty = new ONTAP__EmptyBalance__c();
		try{
			acctEmpty.ONTAP__Account__c = mapAccount.get(((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_sapcustomerid_c__c).Id;
			acctEmpty.ONTAP__Balance__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_balance_c__c;
			acctEmpty.ONTAP__DueDate__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_duedate_c__c;
			acctEmpty.ONTAP__EmptyBalanceKey__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_emptybalancekey_c__c;		
			acctEmpty.ONTAP__ProductId__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_product_c_ontap_productid_c__c;
			acctEmpty.ONTAP__ProductId__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_productid_c__c;
			acctEmpty.ONTAP__SAPCustomerId__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).issm_sap_number_c__c;
			acctEmpty.ISSM_BoxesNumRequest__c = ((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_balance_c__c;
			acctEmpty.ISSM_IsMigrated__c = true; // WP 20171002: Flag to determine if the source of the record is this upload process
			acctEmpty.RecordTypeId = devRecordTypeId;
			try{
				acctEmpty.ONTAP__Product__c = mapProducts.get(((salesforce_ontap_emptybalance_c__x)accountEmptyExt).ontap_productid_c__c).Id;
			} catch(NullPointerException ne){
				system.debug( '\n\n  ****** Error buscando producto = ' + +'\n\n' );
			}
		} catch(NullPointerException ne){
			system.debug( '\n\n  ****** ne = ' + ne+'\n\n' );
		}
		return acctEmpty;
	}

	/**
	 * Save the result of the sincronization process		                             
	 */
	public void save(){
		System.debug('### ISSM_EmptyBalance_cls empties: ' +accountsEmpties);
		if(accountsEmpties != null){
			try {
	        	upsert accountsEmpties ONTAP__EmptyBalanceKey__c;
	        	System.debug('### ISSM_EmptyBalance_cls empties: ' +accountsEmpties);
	    	} catch (DmlException e) {
	        	System.debug(e.getMessage());
	    	}

		}
	}

	/**
	 * Delete internal records that has the flag isdeleted on true		                             
	 */
	public Boolean deleteObjectsFinish(){
		if(emptiesDelete != null){
			Set<String> emptiesSet = ISSM_EmptyBalanceExtern_cls.getSetExternalId(emptiesDelete);
			List<ONTAP__EmptyBalance__c> toDeleteEmpty_lst = getEmpties(emptiesSet);
			Boolean success = true;
			try{
				System.debug('### ISSM_EmptyBalance_cls: toDeleteEmpty_lst size ('+toDeleteEmpty_lst.size()+')'); 
				Database.DeleteResult[] drList = Database.delete(toDeleteEmpty_lst);
				for(Database.DeleteResult dr : drList) {
				    success &= dr.isSuccess();
				}
				return success;
			} catch(Exception e){
				System.debug('### ISSM_EmptyBalance_cls: removeExistingEmpties (DELETE FAILED)'); 
				return false;
			}
		}
		return false;
	}

	/**
	 * Get list of empties based on ONTAP__EmptyBalanceKey__c		                             
	 * @param  Set<String> assetsSet
	 */
	private List<ONTAP__EmptyBalance__c> getEmpties(Set<String> emptiesSet){
		return [SELECT Id FROM ONTAP__EmptyBalance__c WHERE ONTAP__EmptyBalanceKey__c IN : emptiesSet ];
	}

	/**
	 * Delete external records that has the flag isdeleted on true		                             
	 */
	public void deleteObjectsFinishExt(){

		if(emptiesDelete != null){
			try{
				Database.DeleteResult[] drListExt = Database.deleteAsync(emptiesDelete);	
				system.debug( '\n\n  ****** drListExt = ' + drListExt +'\n\n' );
			} catch(Exception e){
				System.debug('### ISSM_EmptyBalance_cls: removeExistingEmpties (DELETE FAILED)'); 
			}
		}
	}
}