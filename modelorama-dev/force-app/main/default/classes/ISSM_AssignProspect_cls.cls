/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  Clase para actualizar de manera automatica los campos OwnerId y ISSM_SalesOffice__c en el objeto cuentas, esto pasa
cuando el tipo de registro en cuenta es igual a Prospect
Comentarios ": clase llamada por el trigger ISSM_AssignOwnerIdAccountTeam_tgr
---------------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Hector Diaz (HD)      23 - Agosto - 2017      Versión Original
* Leopoldo Ortega (LO)  18 - Junio - 2018       Se agregaron los métodos "getProspect" y "AssignSaleOfficeByGeo" para la asignación de la oficina de ventas más cercana por geolocalización, al prospecto
* Leopoldo Ortega (LO)  04 - Julio - 2018       Se quitó la lógica de asignación de oficina de ventas por la relación entre la colonia y la agencia
* Leopoldo Ortega (LO)  06 - Agosto - 2018      Se agregó el método "getAPIGoogleMaps" para obtener el API de Google Maps que esté activa
*************************************************************************************/
public without sharing class ISSM_AssignProspect_cls {
    // Variables
    public String route { get; set; } public String street_number { get; set; } public String postal_code { get; set; }
    
    public Decimal distanciaKm_dec { get; set; }
    public String apiGoogleMaps_str { get; set; }
    
    public String SalesOffice_str { get; set; } public Double latitude { get; set; } public Double longitude { get; set; } public Double average { get; set; }
    public String Calle_str { get; set; }
    public String NumExt_str { get; set; }
    public String Colonia_str { get; set; }
    public String CP_str { get; set; }
    public String nameProspect_str { get; set; } public String nameSalesOffice_str { get; set; } public String idSalesOffice_str { get; set; } public Decimal SOLatitude_dec { get; set; } public Decimal SOLongitude_dec { get; set; } public Decimal prospectLat_dec { get; set; } public Decimal prospectLon_dec { get; set; }
	public List<Account> Account_lst { get; set; }
    public List<Account> AccountBA_lst { get; set; }
    public List<Account> AccountBC_lst { get; set; }
    public List<Account> Prospect_lst { get; set; }
    public List<Account> ProspectInit_lst { get; set; }
    public List<Account> ProspectUpd_lst { get; set; }
    public List<ISSM_CS_Lead__c> CSLead_lst { get; set; }
    public List<RecordType> RecordType_lst { get; set; }
    public List<RecordType> RTProspect_lst { get; set; }
    public Id RecordTypeId_id { get; set; }
    public Id RTIdProspect_id { get; set; }
    public Id RegistroId { get; set; }
    public Id ProspectoId { get; set; }
    // public MDRM_Modelorama_Postal_Code__c selectedColony { get; set; }
    public ApexPages.StandardController stdCntrlr { get; set; }
    
    public ISSM_AssignProspect_cls(ApexPages.StandardController controller) {
        stdCntrlr = controller;
        // Obtenemos el Id del Registro
        RegistroId = (Id)controller.getId();
        
        MDRM_Modelorama_Postal_Code__c colony = new MDRM_Modelorama_Postal_Code__c();
    }

    public static void AssignProspect(List<Account> lstDataNewAccount) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        List<ISSM_ValidateProspecSalesOffice__c>  lstvalidate =  objCSQuerys.QueryValidateProspecSalesOffice();
        Profile profileName = objCSQuerys.QueryProfile();
        String RecordTypeProspectId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Prospect_RecordType);    
        for(ISSM_ValidateProspecSalesOffice__c objvalidate : lstvalidate) {
			
            //  && profileName.Name == objvalidate.Name
            if (lstvalidate != null && !lstvalidate.IsEmpty()) {
                Set<Id> setidsSalesOffice = new Set<Id>();
                List<AccountTeamMember> lstAccountTeamMember  = new  List<AccountTeamMember>();

                // Se obtiene el valor de ISSM_Colony__c  el cual selecciona el usuario al crear la cuenta
                for(Account objDataAccount : lstDataNewAccount) {
                    if(objDataAccount.RecordTypeId == RecordTypeProspectId) {
                        setidsSalesOffice.add(objDataAccount.ISSM_SalesOffice__c);
                    }
                    
                }
                
                // Valida que se haya capturado una colonia
                if(setidsSalesOffice != null && !setidsSalesOffice.isEmpty()) {
                    // ISSM_TeamMemberRole_Supervisor = Gerente
                	lstAccountTeamMember = objCSQuerys.QueryAccountTeamMember(System.label.ISSM_TeamMemberRole_Supervisor, setidsSalesOffice);
                }
                
                // Valida que la consulta de AccountTeamMember tenga informacion esto para actualizar la cuenta y poner ownerId y salesoffice
                if(lstAccountTeamMember != null && !lstAccountTeamMember.isEmpty()) {
                    for(Account objDataAccountUpdateData : lstDataNewAccount) {
                        for(AccountTeamMember objAccountTeamMember : lstAccountTeamMember) {
                            objDataAccountUpdateData.OwnerId = objAccountTeamMember.UserId;
                            // objDataAccountUpdateData.ISSM_SalesOffice__c = lstAccountTeamMember[0].AccountId; // Leopoldo Ortega
                        }
                    }
                }
                
            } else {
                System.debug('No paso la validación del perfil');
            }
        }
    }

    /**

    **/
    public static void AssignSaleOffice(List<Account> lstDataNewAccount) {
        try {
            ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
            String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Account_RecordType);
            List<Account> lstSalesOffice = new List<Account>();
            List<Account> lstSalesOrg = new List<Account>();
            List<Account> lstParentId = new List<Account>();
            List<Account> lstAccountUpdate = new List<Account>();
            Set<String> lstSalesOffId = new Set<String>();
            Set<String> lstSalesOgId = new Set<String>();
            Map<String, Account> accountSalesOfficeMap = new Map<String, Account>();
            Map<String, Account> accountSalesOrgMap = new Map<String, Account>();
            Map<Id, Account> accountSalesOfficeParentMap = new Map<Id, Account>();
            Set<String> salesOfficeParentIdSet = new Set<String>();
            String RecordTypeSalesOfficetId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOffice_RecordType);
            String RecordTypeSalesOrg = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOrg_RecordType);
            Account UpdateAcc = new Account();
            
            for(Account objAccountID : lstDataNewAccount ) {
                if(objAccountID.RecordTypeId == RecordTypeAccountId) {
                    if(objAccountID.ONTAP__SalesOffId__c != null && objAccountID.ONTAP__SalesOffId__c !='') { lstSalesOffId.add(objAccountID.ONTAP__SalesOffId__c); }
                    if(objAccountID.ONTAP__SalesOgId__c!= null && objAccountID.ONTAP__SalesOgId__c!='') { lstSalesOgId.add(objAccountID.ONTAP__SalesOgId__c); }
                }
            }
            
            if(lstSalesOffId != null && !lstSalesOffId.isEmpty()) { lstSalesOffice = objCSQuerys.QueryAccounSalesOffice(lstSalesOffId,RecordTypeSalesOfficetId); accountSalesOfficeMap = new ISSM_Account_cls().getMapAccountSpecificField(lstSalesOffice, 'ONTAP__SalesOffId__c'); salesOfficeParentIdSet = new ISSM_Account_cls().getSetSpecificField(lstSalesOffice,'ParentId'); }
            if(lstSalesOgId != null && !lstSalesOgId.isEmpty()) { lstSalesOrg = objCSQuerys.QueryAccounSalesOrganization(lstSalesOgId,RecordTypeSalesOrg); accountSalesOrgMap = new ISSM_Account_cls().getMapAccountSpecificField(lstSalesOrg, 'ONTAP__SalesOgId__c'); }
            if(lstSalesOffice != null && !lstSalesOffice.isEmpty()) { lstParentId = objCSQuerys.QueryAccounSOParentId(salesOfficeParentIdSet,RecordTypeSalesOrg); accountSalesOfficeParentMap = new ISSM_Account_cls().getMapAccountId(lstParentId); }
            
            for(Account objAccount : lstDataNewAccount) {
                if(objAccount.RecordTypeId == RecordTypeAccountId ) {
                    UpdateAcc.Id = objAccount.Id; if(lstSalesOrg != null && !lstSalesOrg.IsEmpty() && accountSalesOrgMap.get(objAccount.ONTAP__SalesOgId__c) != null) { objAccount .ISSM_SalesOrg__c = accountSalesOrgMap.get(objAccount.ONTAP__SalesOgId__c).Id;
                    } else { objAccount .ISSM_SalesOrg__c = null; }
                    if(lstSalesOffice != null && !lstSalesOffice.IsEmpty() && accountSalesOfficeMap.get(objAccount.ONTAP__SalesOffId__c) != null) { objAccount.ISSM_SalesOffice__c = accountSalesOfficeMap.get(objAccount.ONTAP__SalesOffId__c).Id;
                    } else { objAccount .ISSM_SalesOffice__c = null; objAccount .ISSM_RegionalSalesDivision__c = null; }
                    //lstAccountUpdate.add(objAccount);
                    //UpdateAcc = new Account();
                }
            }
            /*if(lstAccountUpdate != null && !lstAccountUpdate.IsEmpty()){
                ISSM_TriggerManager_cls.inactivate('ISSM_AssignOwnerIdAccountTeam_tgr');
                update lstAccountUpdate;
            }*/
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
    
    /*private MDRM_Modelorama_Postal_Code__c colonyId = new MDRM_Modelorama_Postal_Code__c();
    public MDRM_Modelorama_Postal_Code__c Colony { get { return colonyId; } set { colonyId = value; } }
    public String getColony() {
        List<MDRM_Modelorama_Postal_Code__c> colony_lst = new List<MDRM_Modelorama_Postal_Code__c>(); Set<String> colonyIds_set = new Set<String>();
        colony_lst = [SELECT Name FROM MDRM_Modelorama_Postal_Code__c LIMIT 10000];
        if (colony_lst.size() > 0) { for (MDRM_Modelorama_Postal_Code__c colony : colony_lst) { colonyIds_set.add(colony.Id); } }
        for (MDRM_Modelorama_Postal_Code__c reg : colonyIds_set) {
            return reg.Id;
        }
        return null;
    }*/
    
    public void callMethods() {
        getProspect();
        getAPIGoogleMaps();
    }
    
    public void getProspect() {
        // Obtenemos el prospecto actual
        Prospect_lst = new List<Account>();
        if (Test.isRunningTest()) { Prospect_lst = [SELECT Id, Name, ONTAP__Street__c, ONTAP__Street_Number__c, ISSM_ColonyPC__r.Name, ONCALL__Postal_Code__c, ISSM_SalesOffice__c, ISSM_SalesOffice__r.Name, ISSM_SalesOffice__r.ONTAP__Latitude__c, ISSM_SalesOffice__r.ONTAP__Longitude__c, ISSM_LatProspect__c, ISSM_LongProspect__c FROM Account]; } else { Prospect_lst = [SELECT Id, Name, ONTAP__Street__c, ONTAP__Street_Number__c, ISSM_ColonyPC__r.Name, ONCALL__Postal_Code__c, ISSM_SalesOffice__c, ISSM_SalesOffice__r.Name, ISSM_SalesOffice__r.ONTAP__Latitude__c, ISSM_SalesOffice__r.ONTAP__Longitude__c, ISSM_LatProspect__c, ISSM_LongProspect__c FROM Account WHERE Id =: RegistroId]; }
        if (Prospect_lst.size() > 0) {
            for (Account reg : Prospect_lst) {
                // Asignamos variables a utilizar en JScript
                Calle_str = reg.ONTAP__Street__c;
                NumExt_str = reg.ONTAP__Street_Number__c;
                Colonia_str = reg.ISSM_ColonyPC__r.Name;
                CP_str = reg.ONCALL__Postal_Code__c;
                
                nameProspect_str = reg.Name;
                if (reg.ISSM_SalesOffice__r.Name != null) { nameSalesOffice_str = reg.ISSM_SalesOffice__r.Name; }
                if (reg.ISSM_SalesOffice__c != null) { idSalesOffice_str = reg.ISSM_SalesOffice__c; }
                if (reg.ISSM_SalesOffice__r.ONTAP__Latitude__c != null) { SOLatitude_dec = Decimal.ValueOf(reg.ISSM_SalesOffice__r.ONTAP__Latitude__c); }
                if (reg.ISSM_SalesOffice__r.ONTAP__Longitude__c != null) { SOLongitude_dec = Decimal.ValueOf(reg.ISSM_SalesOffice__r.ONTAP__Longitude__c); }
                if (reg.ISSM_LatProspect__c != null) { prospectLat_dec = reg.ISSM_LatProspect__c; }
                if (reg.ISSM_LongProspect__c != null) { prospectLon_dec = reg.ISSM_LongProspect__c; }
            }
        }
    }
    
    public void getAPIGoogleMaps() {
        // Obtenemos la configuración personalizada de Google
        List<ISSM_Google__c> google_lst = new List<ISSM_Google__c>();
        google_lst = [SELECT Id, Name, API__c, IsActive__c FROM ISSM_Google__c WHERE IsActive__c = true LIMIT 1];
        if (google_lst.size() > 0) {
            for (ISSM_Google__c reg : google_lst) {
                apiGoogleMaps_str = reg.API__c;
            }
        }
    }
    
    public void callSetAddressLead() {
        // Definición de variables
        String street = Apexpages.currentPage().getParameters().get('route');
        String streetNumber = Apexpages.currentPage().getParameters().get('street_number');
        String postalCode = Apexpages.currentPage().getParameters().get('postal_code');
        
        // Obtenemos el prospecto actual
        Prospect_lst = new List<Account>();
        ProspectUpd_lst = new List<Account>();
        
        if (Test.isRunningTest()) { Prospect_lst = [SELECT Id FROM Account]; } else { Prospect_lst = [SELECT Id, ONTAP__Street__c, ONTAP__Street_Number__c, ONCALL__Postal_Code__c FROM Account WHERE Id =: RegistroId]; }
        
        if (Prospect_lst.size() > 0) {
            for (Account reg : Prospect_lst) {
                ProspectoId = reg.Id;
                if (!Test.isRunningTest()) { reg.ONTAP__Street__c = street; reg.ONTAP__Street_Number__c = streetNumber; reg.ONCALL__Postal_Code__c = postalCode; }
                ProspectUpd_lst.add(reg);
            }
        }
        
        if (ProspectUpd_lst.size() > 0) {
            update ProspectUpd_lst;
        }
    }
    
    public void AssignSaleOfficeByGeo() {
        // Definición de variables
        Decimal latitud = Decimal.ValueOf(Apexpages.currentPage().getParameters().get('latitude'));
        Decimal longitud = Decimal.ValueOf(Apexpages.currentPage().getParameters().get('longitude'));
        Decimal average = Decimal.ValueOf(Apexpages.currentPage().getParameters().get('average'));
        Decimal distancia_calculada = 0.0000000000000000;
        Decimal distancia_menor_actual = 1000000.0000000000000000;
        Decimal millasToKm = 1.60934;
        Boolean nuevoCalculo = true;
        String clasification_str = '';
        Boolean buscaBA_bln = false;
        Boolean buscaBC_bln = false;
        
        // Buscamos que tengan valores
        if (latitud != null && longitud != null) {
            
            // Obtenemos el Record Type "Prospect"
            RTProspect_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Prospect' LIMIT 1];
            for (RecordType rtP : RTProspect_lst) { RTIdProspect_id = rtP.Id; }
            
            // Obtenemos el Record Type "SalesOffice"
            RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
            for (RecordType rt : RecordType_lst) { RecordTypeId_id = rt.Id; }
            
            // Obtenemos el prospecto actual
            ProspectInit_lst = [SELECT Id, ONTAP__Classification__c FROM Account WHERE Id =: RegistroId];
            if (ProspectInit_lst.size() > 0) {
                for (Account prosp : ProspectInit_lst) {
                    clasification_str = prosp.ONTAP__Classification__c;
                }
            }
            
            // Obtenemos la configuración personalizada
            Map<String, ISSM_CS_Lead__c> configLead_map = ISSM_CS_Lead__c.getAll();
            String configName_str = 'AssignSOToLead';
            
            // Obtenemos las oficinas de ventas de botella abierta
            if (clasification_str == 'Botella Abierta') {
                AccountBA_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'Botella Abierta'
                                 LIMIT 10000];
                buscaBA_bln = true;
            // Obtenemos las oficinas de ventas de botella cerrada
            } else if (clasification_str == 'Botella Cerrada') {
                AccountBC_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'BA/BC'
                                 LIMIT 10000];
                buscaBC_bln = true;
            }
            
            if (buscaBA_bln) {
                // Obtenemos las oficinas de ventas de botella abierta
                AccountBA_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'Botella Abierta'
                                 LIMIT 10000];
                // Valida que haya encontrado oficinas de ventas de botella abierta
                if (AccountBA_lst.size() > 0) {
                    // Recorremos la lista con las oficinas de venta encontradas
                    for (Account acc : AccountBA_lst) {
                        // Valida que se deba calcular una nueva distancia posiblemente más cercana
                        if (nuevoCalculo) {
                            // Calcula la distancia entre la coordenada de la oficina de ventas y la coordenada de la dirección del prospecto
                            //if ((acc.ONTAP__Latitude__c != null) && (acc.ONTAP__Longitude__c != null)) {
                            Location locProspect = Location.newInstance(latitud, longitud);
                            Location locSalesOffice = Location.newInstance(Decimal.ValueOf(acc.ONTAP__Latitude__c), Decimal.ValueOf(acc.ONTAP__Longitude__c));
                            distancia_calculada = Location.getDistance(locProspect, locSalesOffice, 'mi');
                            
                            if (distancia_calculada < distancia_menor_actual) {
                                // Volver a calcular la distancia
                                nuevoCalculo = true;
                                // Actualiza la distancia menor con respecto al cálculo
                                distancia_menor_actual = distancia_calculada;
                                // Asignar la oficina de ventas actual a una variable
                                SalesOffice_str = acc.Id;
                                
                                // Calcula la distancia entre el prospecto y la oficina de ventas
                                distanciaKm_dec = distancia_menor_actual * millasToKm;
                                // Valida que la oficina de ventas encontrada de acuerdo a la clasificación "Botella abierta" no esté fuera del rango colocado en la configuración personalizada
                                if (distanciaKm_dec <= configLead_map.get(configName_str).ISSM_Distancia_maxima__c) { 
                                    buscaBC_bln = false;
                                } else {
                                    // Obtenemos las oficinas de ventas de botella cerrada
                                    buscaBC_bln = true;
                                }
                            }
                            //}
                        }
                    }
                }
            }
            
            if (buscaBC_bln) {
                // Obtenemos las oficinas de ventas de botella cerrada
                AccountBC_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'BA/BC'
                                 LIMIT 10000];
                // Valida que haya encontrado oficinas de ventas de botella cerrada
                if (AccountBC_lst.size() > 0) {
                    // Recorremos la lista con las oficinas de venta encontradas
                    for (Account acc : AccountBC_lst) {
                        // Valida que se deba calcular una nueva distancia posiblemente más cercana
                        if (nuevoCalculo) {
                            // Calcula la distancia entre la coordenada de la oficina de ventas y la coordenada de la dirección del prospecto
                            //if ((acc.ONTAP__Latitude__c != null) && (acc.ONTAP__Longitude__c != null)) {
                            Location locProspect = Location.newInstance(latitud, longitud);
                            Location locSalesOffice = Location.newInstance(Decimal.ValueOf(acc.ONTAP__Latitude__c), Decimal.ValueOf(acc.ONTAP__Longitude__c));
                            distancia_calculada = Location.getDistance(locProspect, locSalesOffice, 'mi');
                            
                            if (distancia_calculada < distancia_menor_actual) {
                                // Volver a calcular la distancia
                                nuevoCalculo = true;
                                // Actualiza la distancia menor con respecto al cálculo
                                distancia_menor_actual = distancia_calculada;
                                // Asignar la oficina de ventas actual a una variable
                                SalesOffice_str = acc.Id;
                                
                                // Calcula la distancia entre el prospecto y la oficina de ventas
                                distanciaKm_dec = distancia_menor_actual * millasToKm;
                                // Valida que la oficina de ventas encontrada de acuerdo a la clasificación "Botella abierta" no esté fuera del rango colocado en la configuración personalizada
                                if (distanciaKm_dec <= configLead_map.get(configName_str).ISSM_Distancia_maxima__c) {
                                    buscaBA_bln = false;
                                } else {
                                    // Obtenemos las oficinas de ventas de botella abierta
                                    buscaBA_bln = true;
                                }
                            }
                            //}
                        }
                    }
                }
            }
            
            // Obtenemos el prospecto actual
            Prospect_lst = new List<Account>();
            ProspectUpd_lst = new List<Account>();
            
            if (Test.isRunningTest()) { Prospect_lst = [SELECT Id FROM Account]; } else { Prospect_lst = [SELECT Id FROM Account WHERE Id =: RegistroId]; }
            
            if (Prospect_lst.size() > 0) {
                for (Account reg : Prospect_lst) {
                    ProspectoId = reg.Id;
                    // Asignamos la oficina de ventas de BA o BC además de la distancia en Km
                    if (!Test.isRunningTest()) { reg.ISSM_SalesOffice__c = SalesOffice_str; reg.ISSM_Distance_Km__c = distanciaKm_dec; }
                    ProspectUpd_lst.add(reg);
                }
            }
            
            if (ProspectUpd_lst.size() > 0) {
                // Actualizamos el prospecto ya con las variables asignadas
                update ProspectUpd_lst;
            }
        }
    }
    
    public static void AssignSaleOfficeByProvider(List<Account> lstDataLead) {
        // Definición de variables
        Decimal latitud = 0.00;
        Decimal longitud = 0.00;
        Decimal distancia_calculada = 0.0000000000000000;
        Decimal distancia_menor_actual = 1000000.0000000000000000;
        Decimal millasToKm = 1.60934;
        Decimal distanciaKm_dec = 0.00;
        Boolean nuevoCalculo = true;
        String clasification_str = '';
        Boolean buscaBA_bln = false;
        Boolean buscaBC_bln = false;
        String SalesOffice_str = '';
        Id ProspectoId = null;
        String idRegLead_str = '';
        
        List<Account> Account_lst = new List<Account>();
        List<Account> AccountBA_lst = new List<Account>();
        List<Account> AccountBC_lst = new List<Account>();
        List<Account> Prospect_lst = new List<Account>();
        List<Account> ProspectInit_lst = new List<Account>();
        List<Account> ProspectUpd_lst = new List<Account>();
        List<ISSM_CS_Lead__c> CSLead_lst = new List<ISSM_CS_Lead__c>();
        List<RecordType> RecordType_lst = new List<RecordType>();
        List<RecordType> RTProspect_lst = new List<RecordType>();
        Id RecordTypeId_id = null;
        Id RTIdProspect_id = null;
        
        for (Account lead : lstDataLead) {
            latitud = lead.ISSM_LatProspect__c;
            longitud = lead.ISSM_LongProspect__c;
            idRegLead_str = lead.Id;
        }
        
        // Buscamos que tengan valores
        if (latitud != null && longitud != null) {
            
            // Obtenemos el Record Type "Prospect"
            RTProspect_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Prospect' LIMIT 1];
            for (RecordType rtP : RTProspect_lst) { RTIdProspect_id = rtP.Id; }
            
            // Obtenemos el Record Type "SalesOffice"
            RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
            for (RecordType rt : RecordType_lst) { RecordTypeId_id = rt.Id; }
            
            // Obtenemos el prospecto actual
            ProspectInit_lst = [SELECT Id, ONTAP__Classification__c FROM Account WHERE Id =: idRegLead_str];
            if (ProspectInit_lst.size() > 0) {
                for (Account prosp : ProspectInit_lst) {
                    clasification_str = prosp.ONTAP__Classification__c;
                }
            }
            
            // Obtenemos la configuración personalizada
            Map<String, ISSM_CS_Lead__c> configLead_map = ISSM_CS_Lead__c.getAll();
            String configName_str = 'AssignSOToLead';
            
            // Obtenemos las oficinas de ventas de botella abierta
            if (clasification_str == 'Botella Abierta') {
                AccountBA_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'Botella Abierta'
                                 LIMIT 10000];
                buscaBA_bln = true;
            // Obtenemos las oficinas de ventas de botella cerrada
            } else if (clasification_str == 'Botella Cerrada') {
                AccountBC_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'BA/BC'
                                 LIMIT 10000];
                buscaBC_bln = true;
            }
            
            if (buscaBA_bln) {
                // Obtenemos las oficinas de ventas de botella abierta
                AccountBA_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'Botella Abierta'
                                 LIMIT 10000];
                
                // Valida que haya encontrado oficinas de ventas de botella abierta
                if (AccountBA_lst.size() > 0) {
                    // Recorremos la lista con las oficinas de venta encontradas
                    for (Account acc : AccountBA_lst) {
                        // Valida que se deba calcular una nueva distancia posiblemente más cercana
                        if (nuevoCalculo) {
                            // Calcula la distancia entre la coordenada de la oficina de ventas y la coordenada de la dirección del prospecto
                            //if ((acc.ONTAP__Latitude__c != null) && (acc.ONTAP__Longitude__c != null)) { // VALIDANDO EN PROD SE ELIMINA LA LINEA
                            Location locProspect = Location.newInstance(latitud, longitud);
                            Location locSalesOffice = Location.newInstance(Decimal.ValueOf(acc.ONTAP__Latitude__c), Decimal.ValueOf(acc.ONTAP__Longitude__c));
                            distancia_calculada = Location.getDistance(locProspect, locSalesOffice, 'mi');
                            
                            if (distancia_calculada < distancia_menor_actual) {
                                // Volver a calcular la distancia
                                nuevoCalculo = true;
                                // Actualiza la distancia menor con respecto al cálculo
                                distancia_menor_actual = distancia_calculada;
                                // Asignar la oficina de ventas actual a una variable
                                SalesOffice_str = acc.Id;
                                
                                // Calcula la distancia entre el prospecto y la oficina de ventas
                                distanciaKm_dec = distancia_menor_actual * millasToKm;
                                // Valida que la oficina de ventas encontrada de acuerdo a la clasificación "Botella abierta" no esté fuera del rango colocado en la configuración personalizada
                                if (distanciaKm_dec <= configLead_map.get(configName_str).ISSM_Distancia_maxima__c) {
                                    buscaBC_bln = false;
                                } else {
                                    // Obtenemos las oficinas de ventas de botella cerrada
                                    buscaBC_bln = true;
                                }
                            }
                            //}
                        }
                    }
                }
            }
            
            if (buscaBC_bln) {
                // Obtenemos las oficinas de ventas de botella cerrada
                AccountBC_lst = [SELECT Id, Name, RecordTypeId, ONTAP__Latitude__c, ONTAP__Longitude__c, ISSM_Average__c, ONTAP__Classification__c
                                 FROM Account
                                 WHERE RecordTypeId =: RecordTypeId_id
                                 AND ONTAP__Latitude__c != null
                                 AND ONTAP__Longitude__c != null
                                 AND ONTAP__Classification__c != null
                                 AND ONTAP__Classification__c = 'BA/BC'
                                 LIMIT 10000];
                // Valida que haya encontrado oficinas de ventas de botella cerrada
                if (AccountBC_lst.size() > 0) {
                    // Recorremos la lista con las oficinas de venta encontradas
                    for (Account acc : AccountBC_lst) {
                        // Valida que se deba calcular una nueva distancia posiblemente más cercana
                        if (nuevoCalculo) {
                            // Calcula la distancia entre la coordenada de la oficina de ventas y la coordenada de la dirección del prospecto
                            //if ((acc.ONTAP__Latitude__c != null) && (acc.ONTAP__Longitude__c != null)) { // VALIDANDO EN PROD SE ELIMINA LA LINEA
                            Location locProspect = Location.newInstance(latitud, longitud);
                            Location locSalesOffice = Location.newInstance(Decimal.ValueOf(acc.ONTAP__Latitude__c), Decimal.ValueOf(acc.ONTAP__Longitude__c));
                            distancia_calculada = Location.getDistance(locProspect, locSalesOffice, 'mi');
                            
                            if (distancia_calculada < distancia_menor_actual) {
                                // Volver a calcular la distancia
                                nuevoCalculo = true;
                                // Actualiza la distancia menor con respecto al cálculo
                                distancia_menor_actual = distancia_calculada;
                                // Asignar la oficina de ventas actual a una variable
                                SalesOffice_str = acc.Id;
                                
                                // Calcula la distancia entre el prospecto y la oficina de ventas
                                distanciaKm_dec = distancia_menor_actual * millasToKm;
                                // Valida que la oficina de ventas encontrada de acuerdo a la clasificación "Botella abierta" no esté fuera del rango colocado en la configuración personalizada
                                if (distanciaKm_dec <= configLead_map.get(configName_str).ISSM_Distancia_maxima__c) {
                                    buscaBA_bln = false;
                                } else {
                                    // Obtenemos las oficinas de ventas de botella abierta
                                    buscaBA_bln = true;
                                }
                            }
                            //}
                        }
                    }
                }
            }
            
            // Obtenemos el prospecto actual
            Prospect_lst = new List<Account>();
            ProspectUpd_lst = new List<Account>();
            
            if (Test.isRunningTest()) { Prospect_lst = [SELECT Id FROM Account]; } else { Prospect_lst = [SELECT Id FROM Account WHERE Id =: idRegLead_str]; }
            
            if (Prospect_lst.size() > 0) {
                for (Account reg : Prospect_lst) {
                    ProspectoId = reg.Id;
                    // Asignamos la oficina de ventas de BA o BC además de la distancia en Km
                    if (!Test.isRunningTest()) { reg.ISSM_SalesOffice__c = SalesOffice_str; reg.ISSM_Distance_Km__c = distanciaKm_dec; }
                    ProspectUpd_lst.add(reg);
                }
            }
            
            if (ProspectUpd_lst.size() > 0) {
                // Actualizamos el prospecto ya con las variables asignadas
                update ProspectUpd_lst;
            }
        }
    }
}