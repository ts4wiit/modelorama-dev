/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_OrderErase_sh_tst {

	@testSetup
	private static void testSetup(){
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}

		Id RecordTypeAccountId;
		try{
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		// create Accounts
		List<Account> accounts_lst = new List<Account>{	};
		for(Integer i = 0;i<10;i++){
			accounts_lst.add(new Account(Name='Test Account '+i,ISSM_MainContactA__c=true,recordTypeId = RecordTypeAccountId,ONTAP__SAPCustomerId__c = i<10 ? '000000000' + i : '00000000' + i));
		}
		insert accounts_lst;
		// create Products
		List<ONTAP__Product__c> product_lst = new List<ONTAP__Product__c>{
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003117',
				ONTAP__ProductType__c = 'FERT',
				ONTAP__DivisionName__c = 'Agua',
				ONTAP__ProductShortName__c = 'AG-Frt',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003118',
				ONTAP__ProductType__c = 'CRN',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Cr',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003119',
				ONTAP__ProductType__c = 'VTR',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Vct',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003120',
				ONTAP__ProductType__c = 'STLL',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Stl',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL')
		};
		insert product_lst;

		// create Orders For Accounts
		List<ONTAP__Order__c> orders_lst = new List<ONTAP__Order__c>();
		Integer reps = 5;
		for(Integer i = 0; i< accounts_lst.size() ; i++){
			if(i>0) reps = 10;
			for(Integer j = 0;j<reps;j++){
				orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(i),
					ONTAP__OrderAccount__c = accounts_lst[i].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'Telesales',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = j<10 ? i+'000000010' + j : i+'0000010' + j
				));
			}
			for(Integer j = 0;j<reps;j++){
				orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(i),
					ONTAP__OrderAccount__c = accounts_lst[i].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'B2B',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = j<10 ? i+'000000020' + j : i+'0000020' + j
				));
			}
		}
		orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(0),
					ONTAP__OrderAccount__c = accounts_lst[0].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'B2B',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = '0100152874'
				));
		insert orders_lst;

		/*List<ONTAP__Order_Item__c> orderItem_lst = new List<ONTAP__Order_Item__c>();
		for(Integer i = 0; i<orders_lst.size(); i++){
			for(Integer j = 0; j < product_lst.size(); j++){
				orderItem_lst.add(new ONTAP__Order_Item__c(
						ONTAP__ItemProduct__c = product_lst[j].Id,
						ONCALL__OnCall_Order__c = orders_lst[i].Id
					));
			}
		}
		insert orderItem_lst;*/
	}
	
	@isTest static void test_method_one() {
		Test.StartTest();
		ISSM_OrderErase_sh sh1 = new ISSM_OrderErase_sh();      
		String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
		Test.stopTest();

	}
	
}