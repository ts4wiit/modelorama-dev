@isTest
public class ISSM_CAM_WSEstatus_tst {
    @testSetup
    static void setup(){
        ISSM_PriceEngineConfigWS__c wsEstatus = new ISSM_PriceEngineConfigWS__c(
            Name='ConfigCAMWSEstatus', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/estado',
            ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsEstatus;
        ISSM_Asset__c a = new ISSM_Asset__c(Equipment_Number__C='0000NUMERODESERIE_', ISSM_Serial_Number__C='NUMERODESERIE_', ISSM_Status_SFDC__c='Available', ISSM_Status_SAP__C='DISP');
        insert a;
        ISSM_CAM_Estatus_SFDC_SAP__c estatusSAPSFDC = new ISSM_CAM_Estatus_SFDC_SAP__c(Name='1', ISSM_Status_SFDC__c='With customer', ISSM_Status_SAP__c='COMO', ISSM_Send_to_SAP__c=true);
        insert estatusSAPSFDC;
        system.debug(':::FIN SETUP DATA');
    }
    static testMethod void runTestCAMWSEstatus(){
        test.startTest();
        ISSM_Asset__c a = [SELECT id, ISSM_Serial_Number__C, Equipment_Number__C, ISSM_Status_SFDC__c, ISSM_Status_SAP__C FROM ISSM_Asset__c WHERE Equipment_Number__C='0000NUMERODESERIE_'];
        system.debug('Asset: ' + a);
        a.ISSM_Status_SFDC__c='With customer';
        update a;
        system.debug(':::Actualizado');
        //ISSM_CAM_WSEstatusFuture_cls.callClassWSEstatus('NUMERODESERIE_', 'With Customer');
        //ISSM_CAM_WSEstatus_cls.wsEstatus('NUMERODESERIE_', 'With Customer', 'COMO');
        test.stopTest();
        list<ISSM_Asset_CAM_Log__c> al =[SELECT id, ISSM_Activity_Date_time__c, Status_SAP__c, Status_SFDC__c, Integration_Status__c, ISSM_Short_description__c, name 
                                         FROM ISSM_Asset_CAM_Log__c WHERE ISSM_Asset_CAM__c=:a.id];
        system.assert(al.size()>0);
        system.debug('Registros insertados en LOG: ' + al);
    }
    static testMethod void runTestCAMEstatusSAP(){
        test.startTest();
        ISSM_Asset__c a = 
            new ISSM_Asset__C( Name='TestAnonApex', ISSM_Centre__c='FG00',
                              ISSM_Serial_Number__c='TestAnonApex', Equipment_number__c='TestAnonApex',
                              ISSM_Status_SAP__c='ALMA');
        insert a;
        a.asset_number__C='TestAssetNumber';
        update a;
        a.ISSM_Status_SAP__c='ASAE';
        update a;
        test.stopTest();
    }
}