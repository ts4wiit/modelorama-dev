/****************************************************************************************************************
    General Information
    -------------------
    Authors:    Marco Zúñiga, Héctor Díaz
    email:      mzuniga@avanxo.com, hdiaz@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the Taking Order Process

    Information about changes (versions)
    =============================================================================================================
    Number    Dates           Author                       Description
    ------    --------        --------------------------   ------------------------------------------------------
    1.0       May 24, 2018     Marco Zúñiga                Class creation
    1.1       August 24, 2018  Marco Zúñiga                Improvement of the query yo retrieve combos
    =============================================================================================================
****************************************************************************************************************/
public class ISSM_OnCallCombos_cls {
    public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();

    @AuraEnabled
    public static DataTableResponse getResultRecords(String accountId1,String strObjectName){

        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('ISSM_OnCallCombos');

        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        List<String> lstFieldsToQuery = new List<String>();
        DataTableResponse response = new DataTableResponse();
        Boolean blnEditable = false;
        String strType = '';
        String strLabel = '';
        String strpath = '';
        Integer intWidth = 0;
        DataTableColumns datacolumns;
        typeAttributes objtypeAttributes;
        String strNameButton =  '';

        for(Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            strNameButton='';
            strLabel = String.valueOf(eachFieldSetMember.getLabel());
            strpath =  String.valueOf(eachFieldSetMember.getFieldPath());
            strType =  String.valueOf(eachFieldSetMember.getType());

            List<typeAttributes> lsttypeAttributes = new List<typeAttributes>();

            if(String.valueOf(eachFieldSetMember.getFieldPath()) == 'ISSM_RequestedCombos__c'){
                blnEditable=true;
                strType = 'number';
                intWidth = 40;
            }
            if(String.valueOf(eachFieldSetMember.getFieldPath()) == 'ISSM_CombosByCustomer__c'){
                strType = 'number';
                intWidth = 30;
            }
            if(String.valueOf(eachFieldSetMember.getFieldPath()) == 'ISSM_ViewAssemble__c'){
                strType = 'button';
                intWidth = 160;
                objtypeAttributes = new typeAttributes('Ver/Armar','Ver/Armar','clickButton','utility:edit','{!c.getSelectedName}');
            }else{
                intWidth = 110;
            }

            datacolumns = new DataTableColumns(strLabel , strpath, strType, blnEditable, intWidth, objtypeAttributes );

            lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }

        if(!lstDataColumns.isEmpty()){
            response.lstDataTableColumns = lstDataColumns;

            Account customer_Obj = getAccount(accountId1);
            List<ISSM_Combos__c> allCombos = new List<ISSM_Combos__c>();
            List<ISSM_Combos__c> allCombos2 = new List<ISSM_Combos__c>();

            allCombos = getCombos(customer_Obj);
            allCombos2 = setAvailabilitiesCombos(allCombos,customer_Obj);

            response.lstDataTableData = allCombos2;

        }
        System.debug('***RESPONSE  : '+response);
        return response;
    }

    public static List<ISSM_Combos__c> setAvailabilitiesCombos(List<ISSM_Combos__c> combos_lst, Account acc_Obj){
        List<ISSM_Combos__c> combosCopy_lst = new List<ISSM_Combos__c>(combos_lst);
        Map<String,Integer> counterByCustomer_map = assembledCombosAvailiabilities(combos_lst,acc_Obj);
        for(ISSM_Combos__c combos_Obj : combosCopy_lst){
            combos_Obj.ISSM_CombosByCustomer__c = counterByCustomer_map.get(combos_Obj.Name);
        }
        return combosCopy_lst;
    }

    public static Map<String,Integer> assembledCombosAvailiabilities(List<ISSM_Combos__c> combosToWpr_lst, Account acc_Obj){
        Map<String,Integer> abc_map = new Map<String,Integer>();

        Set<Id> idsCombos_set = new Set<Id>();

        for(ISSM_Combos__c combos_Obj : combosToWpr_lst){
            idsCombos_set.add(combos_Obj.Id);
        }

        List<ISSM_AccountantByCustomer__c> counterByComboByCustomer_lst = CTRSOQL.getCounterByComboByCustomer(idsCombos_set,acc_Obj);

        System.debug('counterByComboByCustomer_lst = ' + counterByComboByCustomer_lst);

        for(ISSM_Combos__c combos_Obj : combosToWpr_lst){

            if(combos_Obj.ISSM_NumberByCustomer__c == NULL){
                abc_map.put(combos_Obj.Name,Integer.valueOf(combos_Obj.ISSM_AvailableCombos__c));
            }else if (combos_Obj.ISSM_NumberByCustomer__c == 0){
                abc_map.put(combos_Obj.Name,Integer.valueOf(combos_Obj.ISSM_AvailableCombos__c));
            }else{
                abc_map.put(combos_Obj.Name,Integer.valueOf(combos_Obj.ISSM_NumberByCustomer__c));
            }

            for(ISSM_AccountantByCustomer__c abc_Obj : counterByComboByCustomer_lst){
                if(abc_Obj.ISSM_ExternalKey__c == combos_Obj.Name+ Label.ISSM_Pipe + acc_Obj.ONTAP__SAP_Number__c){
                    abc_map.put(combos_Obj.Name,Integer.valueOf(abc_Obj.ISSM_AvailableCombos__c));
                }
            }          

            if(combos_Obj.ISSM_ComboType__c == Label.ISSM_Mixed || combos_Obj.ISSM_ComboType__c == Label.ISSM_Quotas){ //Blocked the mixed and quotas combos
                combos_Obj.ISSM_HasStock__c = true;
            }
        }

        //System.debug('abc_map = ' + abc_map);

        return abc_map;
    }

    public static List<ISSM_Combos__c> getMatAvailableForSpecificCombos(List<ISSM_Combos__c> allCombos_lst, String acc_Id){
        Id recTypeComboByProd_Id =  Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId();
        Id recTypeProdByOrg_Id =  Schema.SObjectType.ISSM_ProductByOrg__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallProduct').getRecordTypeId();
        List<ISSM_Combos__c> combosCopy_lst = new List<ISSM_Combos__c>(allCombos_lst);
        List<ISSM_Combos__c> specCombos_lst = new List<ISSM_Combos__c>();
        ISSM_MaterialAvailability_cls matAvailabilty_rest = new ISSM_MaterialAvailability_cls();
        ISSM_DeserializeMaterialAvailability_cls desMA_resp = new ISSM_DeserializeMaterialAvailability_cls();
        Map<Id,Double> prods_map = new Map<Id,Double>();
        Map<String,Long> matAvailabilty_map = new Map<String,Long>();
        Map<String, List<ISSM_ComboByProduct__c>> prodsByCombo_map = new Map<String, List<ISSM_ComboByProduct__c>>();
        List<ISSM_ComboByProduct__c> cbp_lst = new List<ISSM_ComboByProduct__c>();
        Map<String,Map<String,Long>> availabilitiesForCombo_map = new Map<String,Map<String,Long>>();
        List<ISSM_ComboByProduct__c> prodsByCombo_lst = new List<ISSM_ComboByProduct__c>();
        Account acc_Obj = CTRSOQL.getAccountbyId(acc_Id);

        for(ISSM_Combos__c combos_Obj : combosCopy_lst){
            if(combos_Obj.ISSM_ComboType__c == Label.ISSM_Expecific && combos_Obj.ISSM_StatusCombo__c == Label.ISSM_Actived && combos_Obj.ISSM_SynchronizedWithSAP__c){ //Doesn't matter if the combo is edited or not
                specCombos_lst.add(combos_Obj);
                for(ISSM_ComboByProduct__c cbp_Obj : combos_Obj.ComboByProducts__r){
                    if(cbp_Obj.ISSM_ActiveValue__c){
                        prodsByCombo_lst.add(cbp_Obj);
                    }
                }
            }
            if(combos_Obj.ISSM_ComboType__c == Label.ISSM_Expecific && (combos_Obj.ISSM_StatusCombo__c == Label.ISSM_SentApproval || combos_Obj.ISSM_StatusCombo__c == Label.ISSM_Approved) && !combos_Obj.ISSM_SynchronizedWithSAP__c && combos_Obj.ISSM_ModifiedCombo__c ){
                specCombos_lst.add(combos_Obj);
                for(ISSM_ComboByProduct__c cbp_Obj : combos_Obj.ComboByProducts__r){
                    if(!cbp_Obj.ISSM_ActiveValue__c){
                        prodsByCombo_lst.add(cbp_Obj);
                    }
                }
            }
        }

        Set<ISSM_Combos__c> specCombos_set = new Set<ISSM_Combos__c>();
        specCombos_set.addAll(specCombos_lst);

        for(ISSM_Combos__c combos_Obj : specCombos_set){
            if(combos_Obj.ISSM_StatusCombo__c == Label.ISSM_Approved && combos_Obj.ISSM_SynchronizedWithSAP__c && !combos_Obj.ISSM_ModifiedCombo__c){
                specCombos_set.remove(combos_Obj);
            }
            if(combos_Obj.ISSM_StatusCombo__c == Label.ISSM_SentApproval && !combos_Obj.ISSM_SynchronizedWithSAP__c && !combos_Obj.ISSM_ModifiedCombo__c){
                specCombos_set.remove(combos_Obj);
            }
        }

        System.debug('specCombos_set = ' + specCombos_set);

        specCombos_lst.clear();
        specCombos_lst.addAll(specCombos_set);

        if(!specCombos_lst.isEmpty()){

            List<ISSM_ComboByProduct__c> productByCombo_lst = CTRSOQL.getProductByCombo(specCombos_lst,
                                                                                        recTypeComboByProd_Id,
                                                                                        prodsByCombo_lst,
                                                                                        recTypeProdByOrg_Id,
                                                                                        acc_Obj);

            for(ISSM_ComboByProduct__c productByCombo_Obj : productByCombo_lst){
                prods_map.put(productByCombo_Obj.ISSM_Product__c,productByCombo_Obj.ISSM_QuantityProduct__c);
            }

            for(ISSM_Combos__c specCombo_Obj : specCombos_lst){
                cbp_lst = new List<ISSM_ComboByProduct__c>();
                for(ISSM_ComboByProduct__c productByCombo_Obj : productByCombo_lst){
                    if(specCombo_Obj.Name == productByCombo_Obj.ISSM_ComboNumber__r.Name){
                        cbp_lst.add(productByCombo_Obj);
                    }
                }
                prodsByCombo_map.put(specCombo_Obj.Name,cbp_lst);
            }

            List<ONTAP__Product__c> prods_lst = CTRSOQL.getProductListbyMap(prods_map);

            desMA_resp = matAvailabilty_rest.CallOutMaterialAvailability(acc_Id,prods_lst,false,new List<ISSM_Bonus__c>());

            for(ISSM_DeserializeMaterialAvailability_cls.Materials matAvail_obj : desMA_resp.Materials){
                matAvailabilty_map.put(matAvail_obj.productId, matAvail_obj.availableQuantity);
            }

            for(String comboName_Str : prodsByCombo_map.keySet()){
                Map<String,Long> availByProduct_map = new Map<String,Long>();
                for(ISSM_ComboByProduct__c productByCombo_Obj : prodsByCombo_map.get(comboName_Str)){
                    availByProduct_map.put(productByCombo_Obj.ISSM_Product__r.ONCALL__Material_Number__c,matAvailabilty_map.get(productByCombo_Obj.ISSM_Product__r.ONCALL__Material_Number__c));
                }
                availabilitiesForCombo_map.put(comboName_Str,availByProduct_map);
            }

            System.debug('availabilitiesForCombo_map = ' + availabilitiesForCombo_map);

            if(!availabilitiesForCombo_map.isEmpty()){
                for(ISSM_Combos__c combos_Obj : combosCopy_lst){
                    if((combos_Obj.ISSM_ComboType__c == Label.ISSM_Expecific && combos_Obj.ISSM_StatusCombo__c == Label.ISSM_Actived && combos_Obj.ISSM_SynchronizedWithSAP__c) ||
                        (combos_Obj.ISSM_ComboType__c == Label.ISSM_Expecific && (combos_Obj.ISSM_StatusCombo__c == Label.ISSM_SentApproval || combos_Obj.ISSM_StatusCombo__c == Label.ISSM_Approved) && !combos_Obj.ISSM_SynchronizedWithSAP__c && combos_Obj.ISSM_ModifiedCombo__c )){
                        for(ISSM_ComboByProduct__c productByCombo_Obj : combos_Obj.ComboByProducts__r){
                            if(availabilitiesForCombo_map.get(combos_Obj.Name).get(productByCombo_Obj.ISSM_Product__r.ONCALL__Material_Number__c) <= 0){
                                combos_Obj.ISSM_HasStock__c = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return combosCopy_lst;
    }

    public static Account getAccount(String accId_Str){
        return CTRSOQL.getAccountbyId(accId_Str);
    }

    public static Set<String> getKeysCombos(Account Acc_obj){
        List<ISSM_ComboByAccount__c> cba_lst = CTRSOQL.getExcludedCombos(Acc_obj);
		System.debug('TEST HMDH cba_lst : '+cba_lst);
        Set<Id> excludedCombos_set = new Set<Id>();
        
        for(ISSM_ComboByAccount__c cba_obj : cba_lst){
            excludedCombos_set.add(cba_obj.ISSM_ComboNumber__c);
        }
        
		System.debug('TEST HMDH excludedCombos_set : '+excludedCombos_set);
        
        List<ISSM_Combos__c> activeCombosOff_lst = CTRSOQL.getCombosByLevel(excludedCombos_set,
                                                                            Label.ISSM_SalesOffice,
                                                                            Acc_obj.ISSM_SalesOffice__c);
        String key_Str = '';

        Set<String> keys_set = new Set<String>();
		System.debug('HMDH -- TEST METHOD : '+ activeCombosOff_lst);
        for(ISSM_Combos__c combos_obj : activeCombosOff_lst){
            if(combos_obj.ISSM_PriceGrouping__c == null){
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
            }else{
                List<String> priceGrp_lst = combos_Obj.ISSM_PriceGrouping__c.split(';');

                System.debug('TST Acc_obj.ISSM_SegmentCode__r.Code__c = ' + Acc_obj.ISSM_SegmentCode__r.Code__c);
                for(String pgValue_Str : priceGrp_lst){
                    System.debug('TST pgValue_Str = ' + pgValue_Str);
                    if(pgValue_Str == Acc_obj.ISSM_SegmentCode__r.Code__c){
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        break;
                    }
                }
            }
        }

        List<ISSM_Combos__c> activeCombosOrg_lst = CTRSOQL.getCombosByLevel(excludedCombos_set,
                                                                            Label.ISSM_SalesOrganization,
                                                                            Acc_obj.ISSM_SalesOrg__c);

        for(ISSM_Combos__c combos_obj : activeCombosOrg_lst){    
            if(combos_obj.ISSM_PriceGrouping__c == null){
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
            }else{ 
                List<String> priceGrp_lst = combos_Obj.ISSM_PriceGrouping__c.split(';');

                for(String pgValue_Str : priceGrp_lst){
                    if(pgValue_Str == Acc_obj.ISSM_SegmentCode__r.Code__c){
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        break;
                    }
                }
            }
        }

        List<ISSM_Combos__c> activeCombosDRV_lst = CTRSOQL.getCombosByLevel(excludedCombos_set,
                                                                            Label.ISSM_RegionalSalesOffice,
                                                                            Acc_obj.ISSM_RegionalSalesDivision__c);

        for(ISSM_Combos__c combos_obj : activeCombosDRV_lst){
            if(combos_obj.ISSM_PriceGrouping__c == null){
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
            }else{
                List<String> priceGrp_lst = combos_Obj.ISSM_PriceGrouping__c.split(';');

                for(String pgValue_Str : priceGrp_lst){
                    if(pgValue_Str == Acc_obj.ISSM_SegmentCode__r.Code__c){
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        break;
                    }
                }
            }
        }

        List<ISSM_Combos__c> activeCombosNat_lst = CTRSOQL.getNationalCombos(excludedCombos_set,
                                                                             Label.ISSM_National);
        for(ISSM_Combos__c combos_obj : activeCombosNat_lst){
            if(combos_obj.ISSM_PriceGrouping__c == null){
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
                key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c;
                keys_set.add(key_Str);
            }else{
                List<String> priceGrp_lst = combos_Obj.ISSM_PriceGrouping__c.split(';');

                for(String pgValue_Str : priceGrp_lst){
                    if(pgValue_Str == Acc_obj.ISSM_SegmentCode__r.Code__c){
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        key_Str = combos_obj.Name + Label.ISSM_Pipe + Acc_obj.ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c + Label.ISSM_Pipe + Acc_obj.ISSM_SegmentCode__r.Code__c;
                        keys_set.add(key_Str);
                        break;
                    }
                }
            }
        }

        for(String s : keys_set){
            System.debug('key = ' + s);
        }

        return keys_set;
    }

    public static List<ISSM_Combos__c> getCombos(Account acc_Obj){
        Id recTypeCombos_Id         = Schema.SObjectType.ISSM_Combos__c.getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        List<String> currency_lst = String.isNotBlank(Label.ISSM_ComboCurrency) ? Label.ISSM_ComboCurrency.split(';') : new List<String>();
        Set<String> keysInSet_set = getKeysCombos(acc_Obj);
        List<ISSM_ComboByAccount__c> cba_lst = CTRSOQL.getCBA(keysInSet_set);

        Set<Id> cbaIds_set = new Set<Id>();
        if(cba_lst != null && cba_lst.size() > 0){
            for(ISSM_ComboByAccount__c cba_obj : cba_lst){
                cbaIds_set.add(cba_obj.ISSM_ComboNumber__c);
            }
        }

        List<ISSM_Combos__c> combosByAcc_lst = CTRSOQL.getComboByAccount(recTypeCombos_Id,
                                                                         cbaIds_set,
                                                                         currency_lst);
        return combosByAcc_lst;
    }

    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String fieldName {get;set;}
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public Boolean editable {get;set;}
        @AuraEnabled public Integer initialWidth {get;set;}
        @AuraEnabled public typeAttributes typeAttributes {get;set;}

        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type,Boolean editable,Integer initialWidth,typeAttributes  typeAttributes){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
            this.editable = editable;
            this.initialWidth = initialWidth;
            this.typeAttributes=typeAttributes;
        }
    }

    public class typeAttributes {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String title {get;set;}
        @AuraEnabled public String name {get;set;}
        @AuraEnabled public String iconName {get;set;}
        @AuraEnabled public String onclick {get;set;}

        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public typeAttributes(String strlabel, String strtitle, String strname,String striconName,String stronclick){
            this.label = strlabel;
            this.title = strtitle;
            this.name = strname;
            this.iconName = striconName;
            this.onclick=stronclick;
        }
    }

    public class DataTableResponse {
        @AuraEnabled public List<DataTableColumns> lstDataTableColumns{get;set;}
        @AuraEnabled public List<sObject> lstDataTableData{get;set;}
        @AuraEnabled public List<ISSM_ComboByProduct__c> comboByProds_lst{get;set;}
        @AuraEnabled public Map<String,List<ISSM_ComboByProduct__c>> comboByProds_map{get;set;}

        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
            comboByProds_lst = new List<ISSM_ComboByProduct__c>();
            comboByProds_map = new Map<String,List<ISSM_ComboByProduct__c>>();
        }
    }

    @AuraEnabled
    public static List<ISSM_ComboByProduct__c> getResultProductExpecific(String strIdCombo, String acc_Id, Map<String,List<ONTAP__Product__c>> combos_map){

        Boolean activeValueProds = true;
        ISSM_Combos__c combo_Obj = CTRSOQL.getCombo(strIdCombo);
        Id recTypeComboByProd_Id = Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId();
        Id recTypeProdByOrg_Id =  Schema.SObjectType.ISSM_ProductByOrg__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallProduct').getRecordTypeId();
        Account acc_Obj = CTRSOQL.getAccountbyId(acc_Id);
        List<ISSM_ComboByProduct__c> lstProductByCombo =  new List<ISSM_ComboByProduct__c>();
        List<ISSM_ComboByProduct__c> lstProductByOrg =  new List<ISSM_ComboByProduct__c>();
        List<Id> lstProductId =  new List<Id>();

        if(combo_Obj.ISSM_StatusCombo__c == Label.ISSM_Actived && combo_Obj.ISSM_SynchronizedWithSAP__c){ //Doesn't matte if the combo is edited or not
           activeValueProds = true;
        }
        if((combo_Obj.ISSM_StatusCombo__c == Label.ISSM_SentApproval || combo_Obj.ISSM_StatusCombo__c == Label.ISSM_Approved) && !combo_Obj.ISSM_SynchronizedWithSAP__c && combo_Obj.ISSM_ModifiedCombo__c ){
            activeValueProds = false;
        }

        lstProductByCombo = CTRSOQL.getProductByCombo2(strIdCombo, recTypeComboByProd_Id, activeValueProds, acc_Obj, recTypeProdByOrg_Id);
        System.debug('*********** lstProductByCombo : '+lstProductByCombo);
        for(ISSM_ComboByProduct__c objProductByCombo :  lstProductByCombo){
            lstProductId.add(objProductByCombo.ISSM_Product__c);
        }
        System.debug('PRODUCTOS : ' + lstProductId);
        if(lstProductId!=null && !lstProductId.isEmpty()){
            lstProductByOrg = CTRSOQL.getProductByCombo3(strIdCombo,activeValueProds,recTypeComboByProd_Id,lstProductId);
        }
        System.debug('lstProductByOrg : ' + lstProductId);
        return lstProductByOrg;

    }

    public class WprProductQuotes {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public Decimal quantity {get;set;}
        @AuraEnabled public List<ONTAP__Product__c> lstProductByCombo {get;set;}

        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public WprProductQuotes(String label,Decimal quantity, List<ONTAP__Product__c> lstProductByCombo){
            this.label = label;
            this.quantity =  quantity;
            this.lstProductByCombo = lstProductByCombo;
        }
    }

    @AuraEnabled
    public static List<WprProductQuotes> getResultProductQuotes(String strIdCombo,String strIdaccount,String strSearchKeyWord){
        Boolean activeValueProds = true;
        ISSM_Combos__c combo_Obj = CTRSOQL.getCombo(strIdCombo);
        Id recTypeComboByProd_Id = Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId();
        Id recTypeProds_Id = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId();
        Id recTypeAccount_Id = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();

        System.debug('combo_Obj = ' + combo_Obj);

        if(combo_Obj.ISSM_StatusCombo__c == Label.ISSM_Actived && combo_Obj.ISSM_SynchronizedWithSAP__c){ //Doesn't matte if the combo is edited or not
           activeValueProds = true;
        }
        if((combo_Obj.ISSM_StatusCombo__c == Label.ISSM_SentApproval || combo_Obj.ISSM_StatusCombo__c == Label.ISSM_Approved) && !combo_Obj.ISSM_SynchronizedWithSAP__c && combo_Obj.ISSM_ModifiedCombo__c ){
            activeValueProds = false;
        }


        String searchKey;
        if(strSearchKeyWord != null && strSearchKeyWord!=''){
            searchKey = strSearchKeyWord + '%';
        }else{
            searchKey = '3'+ '%';
        }
        Account acc_Obj = CTRSOQL.getAccountbyId(strIdaccount);
        System.debug('********* obj   :'+acc_Obj);

        Map<Id,List<ISSM_ComboByProduct__c>> mapProductByCombo =  new Map<Id,List<ISSM_ComboByProduct__c>>();
        Map<Id,List<ONTAP__Product__c>> mapProducts =  new Map<Id,List<ONTAP__Product__c>>();

        List<ISSM_ComboByProduct__c> lstProductByCombo =  new List<ISSM_ComboByProduct__c>();
        List<ONTAP__Product__c> lstProduct =  new List<ONTAP__Product__c>();

        List<WprProductQuotes> lstWprProductBy =  new List<WprProductQuotes>();

        lstProductByCombo = CTRSOQL.getProductByCombo4(strIdCombo,recTypeComboByProd_Id,activeValueProds);

        for(ISSM_ComboByProduct__c objProductByCombo :  lstProductByCombo){
            if(mapProductByCombo.containsKey(objProductByCombo.ISSM_Quota__c)){
                mapProductByCombo.get(objProductByCombo.ISSM_Quota__c).add(objProductByCombo);
            }
            else{
                mapProductByCombo.put(objProductByCombo.ISSM_Quota__c, new List<ISSM_ComboByProduct__c>{objProductByCombo});
            }
        }

        lstProduct = CTRSOQL.getProdsCombo(mapProductByCombo,searchKey,acc_Obj);
        System.debug('************** lstProduct :  '+lstProduct);
        for(ONTAP__Product__c objProduct : lstProduct ){
            if(mapProducts.containsKey(objProduct.ISSM_Quota__c)){
                mapProducts.get(objProduct.ISSM_Quota__c).add(objProduct);
            }
            else{
                mapProducts.put(objProduct.ISSM_Quota__c, new List<ONTAP__Product__c>{objProduct});
            }
        }

        List<ONTAP__Product__c> lstProductFinal = new List<ONTAP__Product__c>();
        String strLabel = '';
        Decimal decQuantity =0;
        for(Id  objMapProduct :  mapProducts.keySet()){
            lstProductFinal = new List<ONTAP__Product__c>();
            for(ONTAP__Product__c objProduct : mapProducts.get(objMapProduct)){
                decQuantity = 0;
                for(ISSM_ComboByProduct__c objProductByCombo : mapProductByCombo.get(objMapProduct)){
                    if(objProduct.ISSM_Quota__c == objProductByCombo.ISSM_Quota__c){
                        strLabel = objProduct.ISSM_Quota__r.Name;
                        System.debug('********* strLabel : '+strLabel);
                        decQuantity += objProductByCombo.ISSM_QuantityProduct__c;
                        lstProductFinal.add(objProduct);
                    }
                }
            }
            lstWprProductBy.add(new WprProductQuotes(strLabel,decQuantity,lstProductFinal));
        }
        return lstWprProductBy;
    }

    @AuraEnabled
    public static List<WprAvailabilities> getMatAvailabilities(String acc_Id, List<String> idsQtyProds_Lst, List<ISSM_ComboByProduct__c> cbp_Lst){
        System.debug('acc_Id : '+acc_Id);
        System.debug('idsQtyProds_Lst : '+idsQtyProds_Lst);
        System.debug('cbp_Lst : '+cbp_Lst);

        Map<String,Integer> idsQty_map = new Map<String,Integer>();
        ISSM_MaterialAvailability_cls matAvailabilty_rest = new ISSM_MaterialAvailability_cls();
        ISSM_DeserializeMaterialAvailability_cls desMA_resp = new ISSM_DeserializeMaterialAvailability_cls();
        Map<String,Long> matAvailabilty_map = new Map<String,Long>();
        List<WprAvailabilities> wa_Lst = new List<WprAvailabilities>();

        for(String idQty_Str : idsQtyProds_Lst){
            List<String> ids_lst = idQty_Str.split(':');
            idsQty_map.put(ids_lst[0],Integer.valueOf(ids_lst[1]));
        }

        if(!cbp_Lst.isEmpty() && cbp_Lst != null){
            for(ISSM_ComboByProduct__c cpb_Obj : cbp_Lst){
                if(idsQty_map.containsKey(String.valueOf(cpb_Obj.ISSM_Product__r.Id))){
                    Integer totalReq_Int = idsQty_map.get(String.valueOf(cpb_Obj.ISSM_Product__r.Id)) + Integer.valueOf(cpb_Obj.ISSM_QuantityProduct__c);
                    idsQty_map.put(String.valueOf(cpb_Obj.ISSM_Product__r.Id), totalReq_Int);
                }else{
                    idsQty_map.put(String.valueOf(cpb_Obj.ISSM_Product__r.Id), Integer.valueOf(cpb_Obj.ISSM_QuantityProduct__c));
                }
            }
        }

        System.debug('idsQty_map = ' + idsQty_map);
        System.debug('idsQty_map : '+ idsQty_map);
        List<ONTAP__Product__c> prods_lst = CTRSOQL.getProdsCombo2(idsQty_map);
        System.debug('prods_lst : '+prods_lst);
        desMA_resp = matAvailabilty_rest.CallOutMaterialAvailability(acc_Id,prods_lst,false,new List<ISSM_Bonus__c>());

        for(ISSM_DeserializeMaterialAvailability_cls.Materials matAvail_obj : desMA_resp.Materials){
            matAvailabilty_map.put(matAvail_obj.productId, matAvail_obj.availableQuantity);
        }

        for(ONTAP__Product__c prod_Obj : prods_lst){
            if(matAvailabilty_map.containsKey(prod_Obj.ONCALL__Material_Number__c)){
                Long available_Lng = matAvailabilty_map.get(prod_Obj.ONCALL__Material_Number__c);
                Integer req_Int = idsQty_map.get(prod_Obj.Id);
                String sty_Str = req_Int > available_Lng ? Label.ISSM_red : '';
                wa_Lst.add(new WprAvailabilities(prod_Obj.Id,
                                                prod_Obj.ONCALL__Material_Number__c,
                                                prod_Obj.ONTAP__MaterialProduct__c,
                                                available_Lng,
                                                req_Int,
                                                sty_Str));
            }
        }

        System.debug('wa_Lst = ');
        System.debug(wa_Lst);
        return wa_Lst;
    }

    @AuraEnabled
    public static List<WprAlertCombos> getAlertsByCombo(String lstProductsxComboxFamily,String lstNameProductBase,String lstNameProductBase2){

        List<String> stageListProductxCombo = (List<String>)JSON.deserialize(lstProductsxComboxFamily,List<String>.class);
        List<String> stageListNameProductBase = (List<String>)JSON.deserialize(lstNameProductBase,List<String>.class);
        List<String> stageListNameProductBase2 = (List<String>)JSON.deserialize(lstNameProductBase2,List<String>.class);

        Integer Suma = 0;
        List<WprAlertCombos> lstWprAlerts = new List<WprAlertCombos> ();
        Map<String,Integer> mapNamexLength = new Map<String,Integer>();
        List<String> lstNameSelec = new List<String> ();
        Integer valueItemMax = 0;

        for(String lstNameProBase:stageListNameProductBase){
            for(String lstNameProBase2:stageListNameProductBase2){
                if(lstNameProBase == lstNameProBase2 ){
                    mapNamexLength.put(lstNameProBase,lstNameProBase.length());
                }
            }
        }

        for(String iterateMap :  mapNamexLength.keySet()){
            System.debug(iterateMap);
            System.debug(mapNamexLength.get(iterateMap));
            lstNameSelec.add(iterateMap);
            //System.debug('LISTA  : '+lstNameSelec);
            Suma=0;
            valueItemMax = 0;
            if(lstNameSelec.contains(iterateMap)){
                System.debug('CONTIEN : '+ iterateMap);
                for(String iterateProductCombo : stageListProductxCombo ){

                    List<String> arrTest = iterateProductCombo.split(':');
                    System.debug('arrTest : '+arrTest);
                    if(arrTest[0] == iterateMap  ){ //Nombre Familia
                        Suma += Integer.valueOf(arrTest[2]);    //Cantidades ingresadas por familia
                        valueItemMax =  Integer.valueOf(arrTest[1]); // cantidad maxima a ingresar
                        System.debug('Suma : '+  + Suma);
                        System.debug('valueItemMax : '+   valueItemMax);
                    }
                }
            }

            if(Suma == valueItemMax){
                lstWprAlerts.add(new WprAlertCombos (iterateMap +' : ' + Label.ISSM_CorrectRecords,'slds-notify slds-notify_toast slds-theme_success',true));
            }else if(Suma > valueItemMax){
                lstWprAlerts.add(new WprAlertCombos (iterateMap +' : ' + Label.ISSM_RecordsMustNotBeGreaterThan + ' : ' + valueItemMax,'slds-notify slds-notify_toast slds-theme_error',false));
            }else if(Suma < valueItemMax){
                lstWprAlerts.add(new WprAlertCombos (iterateMap +' : ' + Label.ISSM_RecordsMustNotBeLessThan    + ' : ' + valueItemMax,'slds-notify slds-notify_toast slds-theme_warning',false));
            }
        }

        return lstWprAlerts;

    }

    public class WprAlertCombos{
        @AuraEnabled public String strTexto{get;set;}
        @AuraEnabled public String strStatus{get;set;}
        @AuraEnabled public Boolean blnSuccess{get;set;}

        public WprAlertCombos(String strTexto,String strStatus,Boolean blnSuccess){
            this.strTexto = strTexto;
            this.strStatus = strStatus;
            this.blnSuccess = blnSuccess;
        }
    }

    public class WprAvailabilities{
        @AuraEnabled public String Id{get;set;}
        @AuraEnabled public String sku_Str{get;set;}
        @AuraEnabled public String name_Str{get;set;}
        @AuraEnabled public Long avaialability_Lng{get;set;}
        @AuraEnabled public Integer requested_Int{get;set;}
        @AuraEnabled public String style_Str{get;set;}

        public WprAvailabilities(String Id,
                                String sku_Str,
                                String name_Str,
                                Long avaialability_Lng,
                                Integer requested_Int,
                                String style_Str){

            this.id = id;
            this.sku_Str = sku_Str;
            this.name_Str = name_Str;
            this.avaialability_Lng = avaialability_Lng;
            this.requested_Int = requested_Int;
            this.style_Str = style_Str;
        }
    }
}