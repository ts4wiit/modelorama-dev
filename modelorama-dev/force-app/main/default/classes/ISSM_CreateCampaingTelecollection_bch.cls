global class ISSM_CreateCampaingTelecollection_bch implements Database.Batchable<sObject>, Database.Stateful {
    
    String query;
    String strOrder;
    Integer count;
    String strCampaing;
    String openItemsId;
    Date startDate;
    Date endDate;
    Boolean active;
    String idCampaign;
    String soqlFilters;
    Boolean validaListaCalls;
    public ISSM_AppSetting_cs__c oIdQueueWithoutOwner;
    public List<ONCALL__Call__c> callList;

    global ISSM_CreateCampaingTelecollection_bch(String idCampaignParam, String soqlFiltersParam) {
        oIdQueueWithoutOwner = [SELECT ISSM_IdQueueWithoutOwner__c, ISSM_IdQueueFriendModel__c, ISSM_IdQueueTelecolletion__c
                                FROM ISSM_AppSetting_cs__c 
                                limit 1];
        validaListaCalls = false;
        count = 0;
        idCampaign = idCampaignParam;
        soqlFilters = soqlFiltersParam;
        query = 'Select Id,ISSM_POC__c,AccountName__c, CampaingName__c,' + 
            'ISSM_OutstandingBalanceAmount__c, ISSM_MaximunDaysOpenItem__c,' + 
            'ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c,'+
            'ISSM_LastPromisePaymentDate__c, OpenItemsId__c From ISSM_TelecolletionOrder__c where Telecollection_Campaign__c =\''+idCampaign+'\'';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<ISSM_TelecolletionOrder__c> setTelecolletionOrder =  new Set<ISSM_TelecolletionOrder__c>();
        setTelecolletionOrder.addAll((List<ISSM_TelecolletionOrder__c>) scope);
        ONCALL__Call__c call;
        callList = new List<ONCALL__Call__c>();
        String openItems;
        String[] opentItemsList = new List<String>();
        Set<Id> idOpenItemSet = new Set<Id>();
        Set<Id> idOpenItemSet2;
        Map<Id,Set<Id>> idsOpenItemCall = new Map<Id,Set<Id>>();
    
        Map<Id, ISSM_OpenItemB__c> openItemsMap = new Map<Id, ISSM_OpenItemB__c>();
        List<ISSM_OpenItemB__c> openItemUpdate = new List<ISSM_OpenItemB__c>();
        ISSM_OpenItemB__c opItem = new ISSM_OpenItemB__c();
        
        String RecordTypeTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
        for (ISSM_TelecolletionOrder__c telecollectionOrder : setTelecolletionOrder) {
            count++;
            call = new ONCALL__Call__c();
            call.ONCALL__Date__c = System.now();
            call.ONCALL__POC__c = telecollectionOrder.ISSM_POC__c;
            call.Telecollection_Campaign__c = idCampaign;
            call.Name = telecollectionOrder.AccountName__c;
            call.OwnerId = oIdQueueWithoutOwner.ISSM_IdQueueTelecolletion__c;
            call.ISSM_Active__c = true;
            call.RecordTypeId = RecordTypeTelecollection;
            call.ISSM_CampaingName__c = telecollectionOrder.CampaingName__c;
            call.ISSM_OutstandingBalanceAmount__c = telecollectionOrder.ISSM_OutstandingBalanceAmount__c;
            call.ISSM_DaysSinceExpired__c  = telecollectionOrder.ISSM_MaximunDaysOpenItem__c;
            call.ISSM_Country__c = System.label.Country_CallOpenItem;
            //call.ISSM_Order__c = count;
            openItems = telecollectionOrder.OpenItemsId__c;
            opentItemsList = openItems.split(';');
            idOpenItemSet2 = new Set<Id>();
            for(String openItem : opentItemsList){
                idOpenItemSet.add(openItem);
                idOpenItemSet2.add(openItem);
                idsOpenItemCall.put(telecollectionOrder.ISSM_POC__c, idOpenItemSet2);
            }
            callList.add(call);
        }
        if (!callList.isEmpty()) {
            validaListaCalls = true;
        }
        insert callList;
        Set<Id> IdsCalls = new Set<Id>();
        openItemsMap = new ISSM_CustomerServiceQuerys_cls().getOpenItemsById(idOpenItemSet);
        for (ONCALL__Call__c oCall2 : callList) {
            IdsCalls.add(oCall2.Id);
            if (idsOpenItemCall.containsKey(oCall2.ONCALL__POC__c)) {
                for (Id openItemId : idsOpenItemCall.get(oCall2.ONCALL__POC__c)) {
                    opItem = openItemsMap.get(openItemId);
                    opItem.Call__c = oCall2.Id; 
                    openItemUpdate.add(opItem);
                }
            }  
        } 
        try {
            if (openItemUpdate!=null && !openItemUpdate.isEmpty()) {
                Database.update(openItemUpdate, false);
                 
                List<ONCALL__Call__c> lstdeleteCallNotOpenItem = [Select id,name From ONCALL__Call__c where id NOT IN(select Call__c  From ISSM_OpenItemB__c)  And RecordTypeId=: RecordTypeTelecollection];
                if (lstdeleteCallNotOpenItem!=null && ! lstdeleteCallNotOpenItem.isEmpty()){
                    delete lstdeleteCallNotOpenItem;
                }
                
            }
            
            
        } catch(Exception e) {
            System.debug('Exception : '+e.getMessage());
        } 
         
    }
    
    global void finish(Database.BatchableContext BC) {
        Telecollection_Campaign__c telecollectionCampaign = new Telecollection_Campaign__c();
        telecollectionCampaign.id = idCampaign;
        telecollectionCampaign.Number_of_Call__c = count;
        
        if (validaListaCalls) {
            telecollectionCampaign.ISSM_SendEmail__c = true;
        } else {
            telecollectionCampaign.ISSM_DontCreateCalls__c = true;
        }
        
        telecollectionCampaign.SoqlFilters__c = soqlFilters;
        update telecollectionCampaign;
        /*ISSM_IntObject_bch intObjectbch = new ISSM_IntObject_bch('EraseTelecollection','Telecollection_Campaign__c',idCampaign);    
        database.executebatch(intObjectbch,150);*/
        
    }
     
}