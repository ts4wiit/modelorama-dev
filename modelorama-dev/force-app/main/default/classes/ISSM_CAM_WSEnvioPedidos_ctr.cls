/**************************************************************************************
Nombre de la clase: ISSM_CAM_WSEnvioPedidos_ctr
Versión : 1.0
Fecha de Creación : 20 Julio 2018
Funcionalidad : Apex class to send orders to Mulesoft and SAP instead Case Force
Clase de Prueba: ISSM_WSEnvioPedidos_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Julio - 2018      Versión Original
*************************************************************************************/
public class ISSM_CAM_WSEnvioPedidos_ctr {
    // VARIABLES
    public Id RegistroId_id { get; set; }
    public List<ONTAP__Case_Force__c> CaseForce_lst { get; set; }
    //public List<ONTAP__Case_Force__c> UpdCaseForce_lst { get; set; }
    public List<ONTAP__Case_Force__c> ValidaCaseForce_lst { get; set; }
    //public List<ONTAP__Case_Force__c> JsonCaseForce_lst { get; set; }
    public List<ISSM_Case_Force_Asset__c> CaseForceAsset = new List<ISSM_Case_Force_Asset__c>();
    public Set<String> setCaseForce_set = new Set<String>();
    //public List<RecordType> RecordType_lst { get; set; }
    
    public location location { get; set; }
    
    //public Id RecordTypeId_id { get; set; }
    public String RecordType_str { get; set; }
    public String strlanguage { get; set; }
    
    public Boolean sendOrderToSAP_bln { get; set; }
    public Boolean errorQuantity_bln { get; set; }
    
    public Boolean result_bln { get; set; }
    public Boolean showOtherError { get; set; }
    
    public Boolean obtenerDatosCaseForce_bln { get; set; }
    public String salesoff_str { get; set; }
    public String salesorg { get; set; }
    public String reqdate_str { get; set; }
    public Date orderdateOrigin_dt { get; set; }
    public String orderdateFormat_str { get; set; }
    public String orderdate_str { get; set; }
    public String numDeudor_str { get; set; }
    public String skuArticulo_str { get; set; }
    public String numSerie_str { get; set; }
    public String uninstallDetails_str { get; set; }
    
    // CONSTANTES
    String CANALDISTRIBUCION = '01';
    String TIPODOCASSIGNATION = 'ZCOM';
    String TIPODOCUNASSIGNATION = 'ZDML';
    String SECTORASSIGNATION = '16';
    String SECTORUNASSIGNATION = '00';
    String FUNCIONINTERLOCUTOR = 'SH';
    String POSICIONDOC = '000000';
    String CANTIDADPREVISTA = '1';
    String CANTIDADDEPEDIDO = '1';
    String UNIDADMEDIDAVENTA = 'EA';
    
    public String strShowButton { get; set; }
    
    public ISSM_CAM_WSEnvioPedidos_ctr(ApexPages.StandardController controller) {
        // Obtenemos el Id del registro del pedido
        RegistroId_id = (Id)controller.getId();
        // Obtenemos el idioma de la configuración del usuario
        strlanguage = UserInfo.getLanguage();
        String strSapOrderNumber = [Select id, ISSM_CAM_SAP_Order_Number__c from ONTAP__Case_Force__c where id =: RegistroId_id limit 1 ].ISSM_CAM_SAP_Order_Number__c; 
        if(String.isNotBlank(strSapOrderNumber) && strSapOrderNumber !=null) {
            strShowButton = 'none';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Ya se cuenta con id Sap'));
        }else{ 
            strShowButton = 'inline';
        } 
        System.debug('EN EL CONTROLLER : '+strShowButton);
        // Inicializamos variables
        sendOrderToSAP_bln = true;
        errorQuantity_bln = false;
        obtenerDatosCaseForce_bln = false;
        salesoff_str = '';
        salesorg = '';
        reqdate_str = '';
        orderdateFormat_str = '';
        orderdate_str = '';
        numDeudor_str = '';
        skuArticulo_str = '';
        numSerie_str = '';
        uninstallDetails_str = 'ZM1';
        showOtherError = true;
    }
    
    public PageReference inicializar() {
        
        CaseForce_lst = [SELECT Id,
                         Name,
                         RecordTypeId,
                         RecordType.DeveloperName,
                         ISSM_CAM_Coolers_by_Case__c,
                         ONTAP__Quantity__c, // tampoco es necesario, usar mejor el rollup  ISSM_CAM_Coolers_by_Case__c
                         ONTAP__Account__c,
                         ONTAP__Account__r.ONTAP__SalesOgId__c,
                         ONTAP__Account__r.ONTAP__SalesOffId__c,
                         ONTAP__Account__r.ONTAP__SAP_Number__c,
                         ISSM_iCam_Material_Num__c,
                         ISSM_Delivery_Date_CAM__c,
                         ISSM_Asset_CAM__r.ISSM_Material_Number__c,
                         ISSM_Asset_CAM__r.ISSM_Serial_Number__c,
                         ONTAP__Uninstall_Details__c
                         FROM ONTAP__Case_Force__c
                         WHERE Id =: RegistroId_id
                         LIMIT 10000];

// START: Oscar Alvarez G. 29 octubre 2018
		String  cutOverNacional				  = 'CutOver';
        String  cutOverSalesOfficeException   = CaseForce_lst[0].ONTAP__Account__r.ONTAP__SalesOffId__c;
        Boolean isCutOverNacional 			  = false;
        Boolean iscutOverSalesOfficeException = false;
        ISSM_CAM_PeriodCutover__mdt[] lstMtdPeriodCutover = [SELECT  DeveloperName
                                                                ,ISSM_CAM_StartTimeCutOver__c
                                                                ,ISSM_CAM_EndTimeCutOver__c
                                                                ,ISSM_CAM_EndDayCutOver__c
                                                                ,ISSM_CAM_StartDayCutOver__c
                                                        	FROM ISSM_CAM_PeriodCutover__mdt
                                                            WHERE DeveloperName =: cutOverNacional 
                                                            OR DeveloperName =: cutOverSalesOfficeException];
        boolean lengthLstMtdPeriodCutover = lstMtdPeriodCutover.size() == 1 ? true :false;
        for(ISSM_CAM_PeriodCutover__mdt  mdtPeriodCutover: lstMtdPeriodCutover){
            ISSM_CAM_ValidateCutover_ctr.WrpPrediodCutOver wrpPrediodCutOver = ISSM_CAM_ValidateCutover_ctr.isInTheDateTimePeriod(mdtPeriodCutover);
            if(wrpPrediodCutOver.cutover && (wrpPrediodCutOver.nameCutover == cutOverNacional) && lengthLstMtdPeriodCutover) isCutOverNacional = true;
            if(wrpPrediodCutOver.cutover && (wrpPrediodCutOver.nameCutover == cutOverSalesOfficeException)) iscutOverSalesOfficeException = true;
        }
        if(Test.isRunningTest()) {
            isCutOverNacional               = false;
            iscutOverSalesOfficeException   = false;
        }
		
        if(iscutOverSalesOfficeException || isCutOverNacional) {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_CAM_ShowToastMsg09));
        }else{
// END: Oscar Alvarez G. 29 octubre 2018

            if (CaseForce_lst.size() > 0) {
                for (ONTAP__Case_Force__c reg : CaseForce_lst) {
                    RecordType_str = reg.RecordType.DeveloperName;
                    
                    // Agregamos los id's de CaseForce a un set de Strings
                    setCaseForce_set.add(reg.Id); // usar un mapa? list <-> map
                    
                    // Verificamos si el CaseForce tiene valor de Quantity mayor a 0
                    if (RecordType_str == 'CAM_Asset_Assignation') {
                        Long intStock = null;
                        if(!test.isRunningTest()) {intStock = ISSM_CAM_WSExistencia_cls.wsExistencia(reg.ONTAP__Account__r.ONTAP__SalesOffId__c, reg.ISSM_iCam_Material_Num__c);} else{intStock=999;}
                        system.debug('Existencia:' + intStock);
                        system.debug('Coolers by case' + reg.ISSM_CAM_Coolers_by_Case__c);
                        if(reg.ISSM_CAM_Coolers_by_Case__c > 0 && intStock>=reg.ISSM_CAM_Coolers_by_Case__c) {
                            obtenerDatosCaseForce_bln = true; 
                        } else { sendOrderToSAP_bln = false; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_Quantity)); }
                        
                        //===============================================
                        //
                        
                    } else if (RecordType_str == 'CAM_Asset_UnAssignation') {
                        ValidaCaseForce_lst = [SELECT Id, Name, ISSM_Asset_CAM__c FROM ONTAP__Case_Force__c WHERE Id =: RegistroId_id];
                        if (ValidaCaseForce_lst.size() > 0) {
                            for (ONTAP__Case_Force__c cf : ValidaCaseForce_lst) {
                                // Se valida que tenga valor el campo "ISSM_Asset_CAM__c" en el objeto "Case Force"
                                if (cf.ISSM_Asset_CAM__c != null) {
                                    sendOrderToSAP_bln = true;
                                    obtenerDatosCaseForce_bln = true;
                                } else { sendOrderToSAP_bln = false; }
                            }
                        }
                    }
                    
                    if (obtenerDatosCaseForce_bln) {
                        // Obtenemos todos los valores para la construcción del JSON
                        salesoff_str = reg.ONTAP__Account__r.ONTAP__SalesOffId__c;
                        salesorg = reg.ONTAP__Account__r.ONTAP__SalesOgId__c;
                        reqdate_str = String.ValueOf(reg.ISSM_Delivery_Date_CAM__c);
                        
                        orderdateOrigin_dt = Date.today();
                        orderdateFormat_str = String.ValueOf(orderdateOrigin_dt);
                        orderdate_str = orderdateFormat_str.substring(0, 10);
                        reqdate_str = orderdate_str;
                        
                        numDeudor_str = reg.ONTAP__Account__r.ONTAP__SAP_Number__c;
                        numSerie_str = reg.ISSM_Asset_CAM__r.ISSM_Serial_Number__c;
                        skuArticulo_str = reg.ISSM_Asset_CAM__r.ISSM_Material_Number__c; //Para desasignación solamente 1:1 CaseFroce to Asset. Cuando es asignación se toma de CASE_FORCE_ASSET 1:N CaseForce to Asset
                    }
                }
            }
            
            // Valida que se pueda enviar el pedido
            if (sendOrderToSAP_bln) {
                String Json_str = '';
                Json_str += '{';
                
                if (RecordType_str == 'CAM_Asset_Assignation') { Json_str += '"' + 'doctype' + '":' + '"' + TIPODOCASSIGNATION + '",'; } 
                else if (RecordType_str == 'CAM_Asset_UnAssignation') { Json_str += '"' + 'doctype' + '":' + '"' + TIPODOCUNASSIGNATION + '",'; }
                Json_str += '"' + 'salesorg' + '":' + '"' + salesorg + '",';
                Json_str += '"' + 'distrchann' + '":' + '"' + CANALDISTRIBUCION + '",';
                
                if (RecordType_str == 'CAM_Asset_Assignation') { Json_str += '"' + 'division' + '":' + '"' + SECTORASSIGNATION + '",'; } 
                else if (RecordType_str == 'CAM_Asset_UnAssignation') { Json_str += '"' + 'division' + '":' + '"' + SECTORUNASSIGNATION + '",'; }
                Json_str += '"' + 'salesoff' + '":' + '"' + salesoff_str + '",';
                Json_str += '"' + 'reqdate' + '":' + '"' + reqdate_str + '",';
                Json_str += '"' + 'orderdate' + '":' + '"' + orderdate_str + '",';
                
                Json_str += '"' + 'coolerOrderItems' + '":' + '[';
                Json_str += '{';
                
                Integer valor = 10;
                if (RecordType_str == 'CAM_Asset_Assignation') { // Tomar datos de junction CASEFORCEASSET (1:N)
                    CaseForceAsset = [SELECT Id, Name, ISSM_Case_Force__c, ISSM_Asset__r.ISSM_Serial_Number__c, ISSM_Asset__r.ISSM_Material_Number__c FROM ISSM_Case_Force_Asset__c WHERE ISSM_Case_Force__c IN: setCaseForce_set];
                    if (CaseForceAsset.size() > 0) {
                        for (ISSM_Case_Force_Asset__c cfAsset : CaseForceAsset) {            
                            Json_str += '"' + 'orderline' + '":' + '"' + valor + '",';
                            valor = valor + 10;
                            Json_str += '"' + 'sku' + '":' + '"' + cfAsset.ISSM_Asset__r.ISSM_Material_Number__c + '",';
                            Json_str += '"' + 'targetqty' + '":' + '"' + CANTIDADPREVISTA + '",';
                            Json_str += '"' + 'reqqty' + '":' + '"' + CANTIDADDEPEDIDO + '",';
                            Json_str += '"' + 'salesunit' + '":' + '"' + UNIDADMEDIDAVENTA + '",';
                            Json_str += '"' + 'numberserie' + '":' + '"' + cfAsset.ISSM_Asset__r.ISSM_Serial_Number__c + '"';
                            Json_str += '},{';
                        }
                    }
                } else if ((RecordType_str == 'CAM_Asset_UnAssignation')) { //Tomar datos de lookup ASSET_cAM en CASEFORCE (1:1) 
                    Json_str += '"' + 'orderline' + '":' + '"' + valor + '",';
                    valor = valor + 10;
                    
                    Json_str += '"' + 'sku' + '":' + '"' + skuArticulo_str + '",';
                    Json_str += '"' + 'targetqty' + '":' + '"' + CANTIDADPREVISTA + '",';
                    Json_str += '"' + 'reqqty' + '":' + '"' + CANTIDADDEPEDIDO + '",';
                    Json_str += '"' + 'salesunit' + '":' + '"' + UNIDADMEDIDAVENTA + '",';
                    Json_str += '"' + 'numberserie' + '":' + '"' + numSerie_str + '"';
                    Json_str += '},{';
                }
                Json_str = Json_str.removeEnd(',{');
                Json_str += '],';
                
                Json_str += '"' + 'coolerOrderPartner' + '":' + '[{';
                Json_str += '"' + 'partnfunct' + '":' + '"' + FUNCIONINTERLOCUTOR + '",';
                Json_str += '"' + 'partnumber' + '":' + '"' + numDeudor_str + '",';
                Json_str += '"' + 'partnline' + '":' + '"' + POSICIONDOC + '"';
                Json_str += '}]';
                
                Json_str += '}';
                
                System.debug('JSON: ' + Json_str);
                
                // ======================== WS REQUEST START =========================================
                Map<String, ISSM_PriceEngineConfigWS__c> configuracionWS_map = ISSM_PriceEngineConfigWS__c.getAll();
                String configName_str = 'ConfigCAMWSEnvioPedidos';
                
                
                if(Test.isRunningTest()){
                    system.debug('TEST DENTRO Order Assign - Setting up MOCK');
                    Test.setMock(HttpCalloutMock.class, new ISSM_CAM_WSGenMockHttpResponse_cls());
                }
                
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndpoint(configuracionWS_map.get(configName_str).ISSM_EndPoint__c);
                request.setMethod(configuracionWS_map.get(configName_str).ISSM_Method__c);
                request.setTimeout(60000);
                request.setHeader('Content-Type', configuracionWS_map.get(configName_str).ISSM_HeaderContentType__c);
                
                request.setBody(Json_str);
                HttpResponse response = new HttpResponse();
                try {
                    if(!test.isRunningTest()){ response = http.send(request);}
                    else{
                        response.setHeader('Content-Type', 'application/json');
                        response.setBody('{"orderID":"12345"}');
                        response.setStatusCode(200);
                    }
                    // ======================== WS REQUEST END =========================================
                    // If the request is successful, parse the JSON response.
                    if (response.getStatusCode() == 200) {
                        strShowButton = 'none';
                        // Get ResponseMap
                        String responseBody = response.getBody();
                        if(Test.isRunningTest()){system.debug('TEST' + responseBody);}
                        ISSM_CAM_WSEnvioPedidos_ctr.location orderID_map = (ISSM_CAM_WSEnvioPedidos_ctr.location)JSON.deserialize(responseBody, ISSM_CAM_WSEnvioPedidos_ctr.location.class);
                        String orderId = orderID_map.orderID;
                        
                        if ((orderId != null) && (orderId != '')) {
                            result_bln = ISSM_CAM_WSEnvioPedidos_helper.SaveOrderId(orderId, setCaseForce_set);
                        } else {
                            showOtherError = false;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_EmptyOrderId));
                            //orderId = '123456';
                            //result_bln = ISSM_WSEnvioPedidos_helper.SaveOrderId(orderId, setCaseForce_set);
                        }
                        if (result_bln == true) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.ISSM_OrderSendSuccess));} 
                        else { if (showOtherError) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_OrderSendError)); }     }
                    } else {
                        system.debug(response.getStatusCode());
                        strShowButton = 'inline';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_EmptyOrderId));
                    }
                } catch(System.CalloutException e) { system.debug('Error al llamar order creation ws: ' + e.getMessage()); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'TimeOut'));
                } return null;
            }
        } return null;
    }
    
    public class Location {
        public String orderID { get; set; }
    }
}