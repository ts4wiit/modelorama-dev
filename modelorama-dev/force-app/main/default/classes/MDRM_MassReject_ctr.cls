/*********************************************************************************************************************************
    General Information
    -------------------
    author:     Cindy Fuentes
    email:      c.fuentes.tapia@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Class for site control

    Information about changes (versions)
    ==============================================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------------------------------------
    1.0       05/05/2018      Cindy Fuentes		           Class created to control Mass Reject tool and execute Mass Reject batch
    ==============================================================================================================================
*********************************************************************************************************************************/
public with sharing class MDRM_MassReject_ctr  {
    Public account acc {get;set;}
    public list <account> lstAcc {get;set;}
    public list <account> accs {get;set;}
    public list <AsyncApexJob> batchinprogress {get;set;}
    public double lowlimit {get;set;}
    public double highlimit {get;set;}
    public integer accnumber {get;set;}
    public integer availableaccs {get;set;}
    public integer batchs {get;set;}
    public boolean noerror {get;set;}
    public boolean basicfilter {get;set;}
    public boolean canproceed {get;set;}
    public string idusuario {get;set;}
    public user usuario {get;set;}
	    
    public MDRM_MassReject_ctr(ApexPages.StandardController stdController) {
		//Method to validate the user information to run the mass reject.        
        idusuario = system.UserInfo.getUserId();
        usuario = [select id, name, MDRM_emails_sent__c, MDRM_UpdateDate__c from user where id=:idusuario limit 1];
        noerror = false;
        acc =[select id, name,ONTAP__Province__c from account limit 1 ];
        accs = new list<account>([select id, name, ONTAP__Province__c from account where ONTAP__Province__c !=null 
                                  AND MDRM_of_Attachment__c !=null]);
        availableaccs = accs.size();

        batchinprogress = new list<AsyncApexJob>([select createdbyid, id, status, JobType from AsyncApexJob where CreatedById =: idusuario 
                                                  AND JobType = 'BatchApex' and Status = 'Processing']);
        batchs = batchinprogress.size();
        system.debug(batchs);
        if (usuario.MDRM_emails_sent__c <=900 && usuario.MDRM_UpdateDate__c == date.today() || usuario.MDRM_UpdateDate__c != date.today()){ 
            noerror = true;
            
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_LimitReached));
        } 
        if(batchs !=0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_Batchinprogress));
            noerror = false;
        }if (availableaccs == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_NoaccountAvailable));
            noerror = false;
        }
        
    } 
    public void search(){ 
        //Method for the search button. Validates the information submitted by the user. 
        basicfilter = true;
        if (highlimit > 100 || highlimit < 0 || lowlimit > 100 || lowlimit < 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_LimitWarning));
            basicfilter =false;
        }if (lowlimit > highlimit){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_LimitsInverted));
            basicfilter =false;
        }else{
            lstAcc= new list<Account>([select name,id, ONTAP__Province__c from account where ONTAP__Province__c =: acc.ONTAP__Province__c 
                                       AND MDRM_of_Attachment__c >=: lowlimit AND MDRM_of_Attachment__c <=: highlimit AND ONTAP__Account_Status__c != 'Rejected Prospect' 
                                       AND MDRM_Substatus__c !='In % of Attachment' AND MDRM_Stage__c = 'Prospect']);
            
            accnumber = lstAcc.size();
        } 
        canproceed = true; 
        
        if (highlimit > 100 || highlimit < 0 || lowlimit > 100 || lowlimit < 0 || lowlimit > highlimit){
            canproceed = false;
        }
        if ( accnumber == 0 && basicfilter ==true){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_NoresultsWaRNING));
            canproceed = false;
        }
        if(usuario.MDRM_emails_sent__c  ==null){
            usuario.MDRM_emails_sent__c = 0;
        }
       
    }

    public PageReference step2() {
        /*Method to move to the confirmation page. Validates that the search results are correct and that the results do not 
        surpasses the user's daily limits. */
        if(usuario.MDRM_emails_sent__c + accnumber >= 900 && usuario.MDRM_UpdateDate__c == system.today()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_Willexceedlimit));
            canproceed = false;
            return null;
        }if(usuario.MDRM_emails_sent__c + accnumber >= 900 && usuario.MDRM_UpdateDate__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_Willexceedlimit));
            canproceed = false;
             return null;
    }
        if(usuario.MDRM_emails_sent__c + accnumber > 900 && usuario.MDRM_UpdateDate__c != date.today()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.MDRM_Willexceedlimit));
            canproceed = false;
            return null;
        }else{
        
         return Page.mdrm_updateaccountsconfirmation;
        }
    }
    public PageReference Regresar() {
        // Method to return to the first page.
        
        return Page.mdrm_updateaccounts;
        
    }
    public PageReference Confirmar() {
        // Method to execute the batch and go to the confirmation page. 
        
        MDRM_MassReject_btch batch = new MDRM_MassReject_btch (acc.ONTAP__Province__c, lowlimit, highlimit, accnumber);
        
        database.executeBatch(batch);
        
        return Page.mdrm_accsupdated;
        
    }
}