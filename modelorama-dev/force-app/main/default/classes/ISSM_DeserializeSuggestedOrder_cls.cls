/****************************************************************************************************
    Información general
    -------------------
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que deserealiza el response de creación de pedidos sugeridos

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-Octubre-2018       Hector Diaz                 Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_DeserializeSuggestedOrder_cls {
	
	public String numClient;
	public Date startDate;
	public Date EndDate;
	public List<CrossSelling> CrossSelling;
	public List<Replanishment> Replanishment;
	public List<Substitute> Substitute;
	public String strMessageError;
	
   
	 
	public static ISSM_DeserializeSuggestedOrder_cls parse(String json) {
		return (ISSM_DeserializeSuggestedOrder_cls) System.JSON.deserialize(json, ISSM_DeserializeSuggestedOrder_cls.class);
	}
	
	public class CrossSelling{
		public String sku = '';
   		public Integer numboxes = 0;
   		public Integer curentsale = 0;
   		public String brand='';
   		public String packagesize = '';
   		public Integer rank = 0;
   		public Integer documenttype=0;
   		
		public CrossSelling(String sku,Integer numboxes,Integer curentsale,String brand,String packagesize,Integer rank,Integer documenttype){
			this.sku = sku;
			this.numboxes = numboxes;
			this.curentsale = curentsale;
			this.brand = brand;
			this.packagesize = packagesize;
			this.rank = rank;
			this.documenttype = documenttype;
		}
	}
	
	public class Replanishment{
		public String sku = '';
   		public Integer numboxes = 0;
   		public Integer curentSale = 0;
   		public String brand='';
   		public String packagesize = '';
   		public Integer rank = 0;
   		public Integer documenttype=0;
   		
		public Replanishment(String sku,Integer numboxes,Integer curentSale,String brand,String packagesize,Integer rank,Integer documenttype){
			this.sku = sku;
			this.numboxes = numboxes;
			this.curentSale = curentSale;
			this.brand = brand;
			this.packagesize = packagesize;
			this.rank = rank;
			this.documenttype = documenttype;
		}
	}
		
	public class Substitute{
		public String brand = '';
   		public String packagesize = '';
   		public Integer substitute = 0;
   		public String brandsubstitute1='';
   		public String packageSizeSubstitute1 = '';
   		public String brandsubstitute2 = '';
   		public Integer documenttype=0;
   		public Integer rank=0;
   		
		public Substitute(String brand,
						String packagesize,
						Integer substitute,
						String brandsubstitute1,
						String packageSizeSubstitute1,
						String brandsubstitute2,
						Integer documenttype,
						Integer rank){
			this.brand = brand;
			this.packagesize = packagesize;
			this.substitute = substitute;
			this.brandsubstitute1 = brandsubstitute1;
			this.packageSizeSubstitute1 = packageSizeSubstitute1;
			this.brandsubstitute2 = brandsubstitute2;
			this.documentType = documentType;
			this.rank = rank;
		}
	}
	
    
}