/**
 * This class serves as batch class for AllMobileVersionOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileVersionBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	/**
	 * This method returns a list with Version records to be deleted.
	 *
	 * @param objBatchableContext   Database.BatchableContext
	 */
	global Database.QueryLocator start(Database.BatchableContext objBatchableContext) {
		return Database.getQueryLocator(AllMobileStaticVariablesClass.STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_VERSIONS);
	}

	/**
	 * This method execute a batch to delete the RouteAppVersion records.
	 *
	 * @param objBatchableContext	Database.BatchableContext
	 * @param lstAllMobileVersionSF	List<AllMobileRouteAppVersion__c>
	 */
	global void execute(Database.BatchableContext objBatchableContext, List<AllMobileVersion__c> lstAllMobileVersionSF) {
		AllMobileVersionOperationClass.syncDeleteAllMobileVersionInSF(lstAllMobileVersionSF);
	}

	/**
	 * This method finish the execution of batch which deletes the RouteAppVersion records.
	 *
	 * @param objBatchableContext	Database.BatchableContext
	 */
	global void finish(Database.BatchableContext objBatchableContext) {
	}
}