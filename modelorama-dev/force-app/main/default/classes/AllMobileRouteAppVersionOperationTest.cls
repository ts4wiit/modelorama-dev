/**
 * Test class for AllMobileRouteAppVersionOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileRouteAppVersionOperationTest {

	/**
	 * Test method to setup data.
	 */
	@testSetup
	static void setup() {

		//Create a List of RouteAppVersion.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion = new List<AllMobileRouteAppVersion__c>();
		for(Integer intI = 0; intI < 9; intI++) {
			lstAllMobileRouteAppVersion.add(AllMobileUtilityHelperTest.createAllMobileRouteAppVersion('FG000' + String.valueOf(intI), '1.0.0', 1, Date.newInstance(2030, 07, 15), Date.newInstance(2098, 10, 20), false));
		}
		insert lstAllMobileRouteAppVersion;
	}

	/**
	 * Test method to test Queueable insertRouteAppVersionInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableInsertRouteAppVersionInSFAndSendToHeroku() {

		//Variables to enqueue Job.
		String strEventTriggerFlagRouteAppVersionAfterInsert = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_INSERT;
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToInsertInHeroku = [SELECT Id, Name, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileValidStartDate__c, AllMobileValidEndDate__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileRouteAppVersion__c WHERE AllMobileRouteId__c LIKE 'FG000%'];
		AllMobileRouteAppVersionOperationClass objAllMobileRouteAppVersionOperationClassInsert = new AllMobileRouteAppVersionOperationClass(lstAllMobileRouteAppVersionToInsertInHeroku, strEventTriggerFlagRouteAppVersionAfterInsert);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileRouteAppVersionOperationClassInsert);

		//End Test.
		Test.stopTest();
	}

	/**
	 * Test method to test Queueable updateRouteAppVersionInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableUpdateRouteAppVersionInSFAndSendToHeroku() {

		//Variables to enqueue job.
		String strEventTriggerFlagRouteAppVersionAfterUpdate = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_UPDATE;
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToUpdate = [SELECT Id, Name, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileValidStartDate__c, AllMobileValidEndDate__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileRouteAppVersion__c WHERE AllMobileRouteId__c LIKE 'FG000%'];
		AllMobileRouteAppVersionOperationClass objAllMobileRouteAppVersionOperationClassUpdate = new AllMobileRouteAppVersionOperationClass(lstAllMobileRouteAppVersionToUpdate, strEventTriggerFlagRouteAppVersionAfterUpdate);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileRouteAppVersionOperationClassUpdate);

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test syncInsertUpdateRouteAppVersionFromHerokuRemoteAction method.
	 */
	static testMethod void testSyncInsertUpdateRouteAppVersionFromHerokuRemoteAction() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileRouteAppVersionOperationClass.syncInsertUpdateRouteAppVersionFromHeroku();

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test syncDeleteAllMobileRouteAppVersionInSF method.
	 */
	static testMethod void testSyncDeleteAllMobileRouteAppVersionInSF() {

		//Variables.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionInSF = [SELECT Id, Name, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileValidStartDate__c, AllMobileValidEndDate__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileRouteAppVersion__c WHERE AllMobileRouteId__c LIKE 'FG000%'];

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileRouteAppVersionOperationClass.syncDeleteAllMobileRouteAppVersionInSF(lstAllMobileRouteAppVersionInSF);

		//Stop Test.
		Test.stopTest();
	}
}