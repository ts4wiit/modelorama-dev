/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for Profile
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public class ISSM_ProfileSelector_cls extends fflib_SObjectSelector implements ISSM_IProfileSelector_cls
{
	public static ISSM_IProfileSelector_cls newInstance()
	{
		return (ISSM_IProfileSelector_cls) ISSM_Application_cls.Selector.newInstance(Profile.SObjectType);
	}

	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Profile.Id,
				Profile.Name
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Profile.sObjectType;
	}

	public List<Profile> selectById(Set<ID> idSet)
	{
		return (List<Profile>) selectSObjectsById(idSet);
	}

	public Profile selectById(ID profileID){
		return (Profile) selectSObjectsById(profileID);
	}

	public List<Profile> selectById(String profileID){
		return (List<Profile>) selectSObjectsById(profileID);
	}		

	/*
	 * For more examples see https://github.com/financialforcedev/fflib-apex-common-samplecode
	 * 
	public List<Profile> selectBySomethingElse(List<String> somethings)
	{
		assertIsAccessible();
		return (List<Profile>) Database.query(
				String.format(
				'select {0}, ' +
				  'from {1} ' +
				  'where Something__c in :somethings ' + 
				  'order by {2}',
				new List<String> {
					getFieldListString(),
					getSObjectName(),
					getOrderBy() } ) );
	}
	 */
}