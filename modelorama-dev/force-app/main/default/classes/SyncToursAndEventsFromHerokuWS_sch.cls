/****************************************************************************************************
   General Information
   -------------------
   author: Andrés Garrido
   email: agarrido@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Scheduler to invoke the batch process to synchronize the Event and Tour records 
   between Heroku and Salesforce

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       01-08-2017        Andrés Garrido (AG)			Creation Class
****************************************************************************************************/
global class SyncToursAndEventsFromHerokuWS_sch implements Schedulable{
    global void execute(SchedulableContext sc) {
		SyncToursAndEventsFromHerokuWS_bch batchSync = new SyncToursAndEventsFromHerokuWS_bch();
		database.executebatch(batchSync,200);
	}
}