/**************************************************************************************
Nombre del Trigger: ISSM_Manager_ATM_tst
Versión : 1.0
Fecha de Creación : 28 Agosto 2018
Funcionalidad : Clase de prueba del trigger ISSM_Manager_ATM_tgr y la clase ISSM_Manager_ATM_thr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        28 - Agosto - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_Manager_ATM_tst {
	static testmethod void manageATM() {
        
        List<ISSM_Manager_ATM__c> dataManagerATM_lst = new List<ISSM_Manager_ATM__c>();
        
        ISSM_Manager_ATM__c managerATM1 = new ISSM_Manager_ATM__c();
        managerATM1.ISSM_Role__c = 'Supervisor';
        managerATM1.ISSM_Manager__c = 'Gerente de ventas';
        insert managerATM1;
        
        Test.startTest();
        
        ISSM_Manager_ATM_thr.validExistRole(dataManagerATM_lst);
        
        Test.stopTest();
    }
}