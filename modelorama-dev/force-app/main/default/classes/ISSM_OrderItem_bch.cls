/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Batch use to realize the external objects sincronization order items

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_OrderItem_bch implements Database.Batchable<sObject> {
	
	String query='SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false';
    //ONCALL__SAP_Order_Number__c IN (\'0100152874\',\'0100152887\',\'0100152900\',\'0100152911\',\'0100152928\',\'0100152938\',\'0100152944\',\'0100152947\',\'010015293\',\'0100152976\',\'0100152995\',\'0100152998\',\'0100153017\',\'0100153036\',\'0100153128\',\'0100153232\',\'0100153234\',\'010013243\',\'0100153251\',\'0100153254\',\'0100153379\',\'0100153444\',\'0100153452\',\'0100153551\',\'0100153596\',\'0100153614\',\'0100153616\',\'0100153645\',\'0100153649\',\'0100153659\',\'0100153669\',\'0100153683\',\'0100153703\',\'0100153725\',\'0100153737\'\'0100153781\',\'0100153804\',\'0100153893\',\'0100153904\',\'0100153948\',\'0100153951\',\'0100153976\',\'0100154005\') and
	ISSM_Order_cls orderCls;
	Map<String, ONTAP__Order__c> mapOrders;
	Set<String> setOrderNumber;

	/**
	 * Class constructor		                             
	 * @param  String sapCustomerId
	 */	
	global ISSM_OrderItem_bch(String sapCustomerId) {
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		query ='SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false and ONTAP__OrderAccount__r.ONTAP__SAPCustomerId__c=\''+ sapCustomerId +'\' and RecordTypeId=\''+ devRecordTypeId +'\' and LastModifiedDate  >= YESTERDAY';
	}

	/**
	 * Class constructor
	 */	
	global ISSM_OrderItem_bch() {
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		query ='SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false and RecordTypeId=\''+ devRecordTypeId +'\' and LastModifiedDate  >= YESTERDAY ';
	}

		/**
	 * Class constructor
	 */	
	global ISSM_OrderItem_bch(Boolean salesOfficesB,String salesOfficeParam) {
		Id devRecordTypeId;
		String stSalesOffice =Label.ISSM_SalesOfficeToProcess;
		if(salesOfficeParam != null && salesOfficeParam != ''){
			stSalesOffice = salesOfficeParam;
		}
		stSalesOffice = stSalesOffice.replace(',', '\',\'');
		stSalesOffice = '\''+ stSalesOffice +'\'';
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		query ='SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false and RecordTypeId=\''+ devRecordTypeId +'\' and LastModifiedDate  >= YESTERDAY and ONTAP__OrderAccount__r.ONTAP__SalesOffId__c IN ('+ stSalesOffice +')';
	}

	/**
	 * Start method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('### ISSM_OrderItem_bch start()'); 
		system.debug( '\n\n  ****** query = ' + query+'\n\n' );
		return Database.getQueryLocator(query);
	}

	/**
	 * Execute method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('### ISSM_OrderItem_bch execute()'); 
   		system.debug( '\n\n  ****** scope = ' + scope+'\n\n' );
   		orderCls = new ISSM_Order_cls();
   		mapOrders = orderCls.getMapOrderOrderNumber(scope);
   		List<salesforce_ontap_order_item_c__x> orderItems_lst = ISSM_OrderItemExtern_cls.getOrderItemsExten(mapOrders.keySet());
   		system.debug( '\n\n  ****** orderItems_lst = ' + orderItems_lst+'\n\n' );

   		ISSM_OrderItem_cls orderItemsList = new ISSM_OrderItem_cls(orderItems_lst, mapOrders);
   		
   		orderItemsList.save();
   		System.debug('### ISSM_OrderItem_bch new orders were saved'); 
	}

	/**
	 * Finish method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
	global void finish(Database.BatchableContext BC) {
		System.debug('### ISSM_OrderItem_bch finish()'); 
	}
	
}