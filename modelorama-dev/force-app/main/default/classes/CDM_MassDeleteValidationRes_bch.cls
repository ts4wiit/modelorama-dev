/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Batch to delete all records in the org.  (MDM_Validation_Result__c)
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-Mayo-2018   Iván Neria			   Initial version
***********************************************************************************/
global class CDM_MassDeleteValidationRes_bch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT  Id, Name '
        +' FROM MDM_Validation_Result__c ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        delete scope;
    }  
    global void finish(Database.BatchableContext BC)
    {
        Database.executeBatch(new CDM_MassDeleteMDMAccount_bch(), 500);
    }
}