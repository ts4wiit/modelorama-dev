/****************************************************************************************************
General Information
-------------------
author: Nelson Sáenz Leal
email: nsaenz@avanxo.com
company: Avanxo Colombia
Project: ISSM DSD
Customer: AbInBev Grupo Modelo
Description: Class execution manual batch

Information about changes (versions)
-------------------------------------
Number    Dates             Author                       Description
------    --------          --------------------------   -----------
1.0       05-07-2017        Nelson Sáenz Leal (NSL)  Creation Class
1.1       04-06-2018        Rodrigo Resendiz			Parametrizable value
****************************************************************************************************/
global with sharing class DailyVisitPlanBatchExc_cls {
    WebService static string executeDailyVisitPlan(Integer execution)
    {
        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
        Integer batchSize = visitPlanSettings.BatchSize__c != null ? Integer.valueOf(visitPlanSettings.BatchSize__c) : 50;
        System.debug(':::: execution: '+execution);
        //DailyVisitplan_bch b = new DailyVisitplan_bch();
        DailyVisitPlanV2_bch b = new DailyVisitPlanV2_bch('Telesales', 1, execution);
        ID batchprocessid     = Database.executeBatch(b,batchSize);
        return batchprocessid;
    }
    WebService static string executeDailyVisitPlan2()
    {
        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
        Integer batchSize = visitPlanSettings.BatchSize__c != null ? Integer.valueOf(visitPlanSettings.BatchSize__c) : 50;
        //DailyVisitplan_bch b = new DailyVisitplan_bch();
        DailyVisitPlanV2_bch b = new DailyVisitPlanV2_bch('Telesales', 1);
        ID batchprocessid     = Database.executeBatch(b,batchSize);
        return batchprocessid;
        //return null;
    }
}