/****************************************************************************************************
    General Information
    -------------------
    author:     Andrés Morales
    email:      lmorales@avanxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Guarda la cantidad de adjuntos de una cuenta de tipo "Expansor"

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                    	Description          
    ------    --------        --------------------------   	-----------------------------------------
    1.0       26/10/2017      Andrés Morales				Created
    ================================================================================================
****************************************************************************************************/

public with sharing class AttachmentCountService_cls {
	/*public AttachmentCountService_cls() {
		
	}*/
	public static void AttachmentCountInsert(List<attachment> newRecords){

		List<attachment> attachmentPI = new List<attachment>();
		Set<id> accIds = new Set<id>();		

	    //Validate if ParentId is type Account
	    for(Attachment att : newRecords){
	         //Check if added attachment is related to Account or not
	         if(att.ParentId.getSobjectType() == Account.SobjectType){
	              accIds.add(att.ParentId);
	         }
	    }

	    UpdateCount(accIds);
	}

	public static void AttachmentCount(Map<Id, attachment> oldMap){

		List<attachment> attachmentPI = new List<attachment>();
		Set<id> accIds = new Set<id>();		

	    //Validate if ParentId is type Account
	    for(Attachment att : oldMap.values()){
	         //Check if added attachment is related to Account or not
	         if(att.ParentId.getSobjectType() == Account.SobjectType){
	              accIds.add(att.ParentId);
	         }
	    }
	    
	    UpdateCount(accIds);
	}

	public static void UpdateCount(Set<id> accIds){
		List<Account> accountList = new List<Account>();
		map<id, integer> mapCount = new map<id, integer>();
		integer intCount;
		
		//List of all Attachments
		List<attachment> allAttachments = [select ParentId from Attachment where ParentId in :accIds];
		
		//Count attachments by Account Id
		for(id ide: accIds){
			//system.debug('*****AM: for 1: ' + ide);
			intCount = 0;				
			for(attachment atch: allAttachments){
				//system.debug('*****AM: for 2: ' + atch.ParentId);
				if(ide == atch.ParentId){
					intCount ++;
				}
			}
			mapCount.put(ide, intCount);
		}

		//system.debug('*****AM: mapCount: ' + mapCount);		
		accountList = [select id, MDRM_Attachments_Quantity__c, RecordType.Name from Account where id in : accIds];

		if(accountList!=null && accountList.size()>0){
	        for(Account acc : accountList){
	        	//Only for RecordType "Expansor"        	
	        	if(acc.RecordType != null && acc.RecordType.Name == 'Expansor'){		        		
	        		acc.MDRM_Attachments_Quantity__c = mapCount.get(acc.id);
	        	}
	        }
	        update accountList;
	    }
	}
}