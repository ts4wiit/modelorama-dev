public class MDRM_ModeloramaHelperTest {

    @future
    public static void createFullViewInfo(String id) {
        insert new ISSM_SurveyAssignmentCase__c(
        	ISSM_Classification__c = 'X',
            ISSM_Grouping__c = 'X',
            ISSM_Value__c = 80,
            Name = 'Passing Score'
        );
        
        User user = [SELECT Id, Name, ProfileId, LastName, Email, Username, CompanyName, Title, Alias, TimeZoneSidKey, EmailEncodingKey, LanguageLocaleKey,
                     	LocaleSidKey, UserRoleId, MDRM_Corporative_Approver__c, MDRM_Legal_Approver__c, MDRM_LiderGerenteApprover__c, MDRM_Procurement_Approver__c,
                     	MDRM_UEN_Approver__c, MDRM_SupervisorApprover__c, EmployeeNumber
                     FROM User WHERE Id =: id];
        Id businessmanRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MDRM_Businessman').getRecordTypeId();
        Id expansorsRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MDRM_Expansor').getRecordTypeId();
        Id salesOfficeRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        Id salesOrgRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        Id regionalSalesDivisionRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ISSM_RegionalSalesDivision').getRecordTypeId();
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        Account store, businessman;
        Account drvOffice = new Account(
            Name = 'drv Office',
            RecordTypeId = regionalSalesDivisionRt
        );
        insert drvOffice;
        
        Account salesOrg = new Account(
            Name = 'Sales Org',
            RecordTypeId = salesOrgRt,
            ISSM_ParentAccount__c = drvOffice.Id
        );
        insert salesOrg;
        
        Account salesOffice = new Account(
            Name = 'Sales Office',
            RecordTypeId = salesOfficeRt,
            ISSM_ParentAccount__c = salesOrg.Id
        );
        insert salesOffice;
        
        System.runAs(user) {
            store = new Account(
        		Name = 'Modelorama',
                MDRM_Stage__c = 'Negotiation',
                RecordTypeId = expansorsRt,
                ISSM_SalesOffice__c = salesOffice.Id,
                MDRM_LiderGerenteApprover__c = 'modelorama@gpmodelo.testing.mx',
                MDRM_Number_Assignments__c = 0
            );
            insert store;
            
            businessman = new Account(
        		Name = 'Businessman',
                MDRM_Stage__c = 'Link to shop (Z001 to Z019)',
                RecordTypeId = businessmanRt
            );
            insert businessman;
            
            MDRM_Document__c doc = new MDRM_Document__c(
                RecordTypeId = licenseRt,
                Name = 'Licencia',
                MDRM_License_Type__c = 'Liquor Store',
                MDRM_Exp_Date__c = Date.today().addYears(1),
                MDRM_Account__c = store.Id,
                MDRM_Status__c = 'Aprobado'
            );
            insert doc;
            
            ContentVersion contentVersion = new ContentVersion(
                Title = 'License',
                PathOnClient = 'License.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
            );
            insert contentVersion;
            
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            ContentDocumentLink cdl = New ContentDocumentLink();
                cdl.LinkedEntityId = doc.id;
                cdl.ContentDocumentId = documents[0].Id;
                cdl.shareType = 'V';
            insert cdl;
            
            store.MDRM_Stage__c = 'Businessman';
            update store;
            
            MDRM_Businessman_Expansor__c relationshipBE = new MDRM_Businessman_Expansor__c(
            	MDRM_Businessman__c = businessman.Id,
                MDRM_Expansor__c = store.Id,
                MDRM_Status__c = 'Active'
            );
            insert relationshipBE;
            
            MDRM_Sale_Summary__c summary = new MDRM_Sale_Summary__c(
                MDRM_AveMonthlySaleHect__c = 50000,
                MDRM_MixHighEnd__c = 50,
                MDRM_MixAboveCore__c = 50,
                MDRM_SKUs__c = 1000,
                MDRM_Year__c = String.valueOf(Date.today().year()),
                MDRM_Month__c = 'August',
                MDRM_Account__c = store.Id
            );
            insert summary;  
        }
        
        ONTAP__Survey__c surv = new ONTAP__Survey__c(
        	Name = 'Survey Modelorama Test',
            ONTAP__HTML5_Bundle__c = 'xxxxxxxxx',
            ONTAP__Profiles__c = 'BDR'
        );
        insert surv;
        
        ONTAP__Survey_Question__c quest = new ONTAP__Survey_Question__c(
        	Name = 'quest',
            ONTAP__Survey__c = surv.Id,
            ONTAP__OrderNumber__c = 0,
            ONTAP__Question__c = 'xxxxxxxx'
        );
        insert quest;
        
        ONTAP__SurveyTaker__c taker = new ONTAP__SurveyTaker__c(
        	ONTAP__Account__c = businessman.Id,
            ONTAP__Survey__c = surv.Id
        );
        insert taker;
        
        ONTAP__SurveyQuestionResponse__c response = new ONTAP__SurveyQuestionResponse__c(
        	ONTAP__SurveyTaker__c = taker.Id,
            ONTAP__Survey_Question__c = quest.Id,
            ONTAP__Response__c = 'xxxxxxxxx'
        );
        insert response;
        
        insert new Attachment(
        	Name = 'Unit Test Attachment',
            body = Blob.valueOf('Unit Test Attachment Body'),
            parentId = response.id
        );
        
        insert new Attachment(
        	Name = 'Unit Test Attachment',
            body = Blob.valueOf('Unit Test Attachment Body'),
            parentId = response.id
        );
    }
    
}