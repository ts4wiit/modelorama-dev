@isTest
public class clonaConDocsCtrTest {
	
    static testMethod void testContrClone() {
        PageReference pref = Page.copiaContrato;
        Test.setCurrentPage(pref);
        
        Agri_Material__c am = new Agri_Material__c();
        am.Name = 'Test';
        am.Agri_Type__c = 'Semilla';
        insert am;        
        
        Agri_Contract__c ac = new Agri_Contract__c();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Bank_Account_Modelo__c = '12345678901';
        ac.Agri_Name_of_Bank_Account_Modelo__c = '021 HSBC';
        ac.Agri_Bank_Reference_Number__c = '12345678901';
        insert ac;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test';
        cv.PathOnClient = 'Test.txt';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;        
        insert cv;    
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = ac.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        ApexPages.StandardController con = new ApexPages.StandardController(ac);
        clonaConDocsCtr ext = new clonaConDocsCtr(con);
        
        Test.startTest();        
        PageReference ref = ext.copiaContrato();
        PageReference redir = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId);                
        System.assertEquals(ref.getUrl(),redir.getUrl());        
        Agri_Contract__c newAC = [Select Id FROM Agri_Contract__c Where Id = : ext.newRecordId];
        System.assertNotEquals(newAC, null);        
        Test.stopTest();
    }
    
    static testMethod void testContrCloneExcep() {
        PageReference pref = Page.copiaContrato;
        Test.setCurrentPage(pref);
        
        Agri_Material__c am = new Agri_Material__c();
        am.Name = 'Test';
        am.Agri_Type__c = 'Semilla';
        insert am;  
        
        Agri_Contract__c ac = new Agri_Contract__c();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Bank_Account_Modelo__c = '12345678901';
        ac.Agri_Name_of_Bank_Account_Modelo__c = '021 HSBC';
        ac.Agri_Bank_Reference_Number__c = '12345678901';
        insert ac;
        
		ApexPages.StandardController con = new ApexPages.StandardController(ac);
        clonaConDocsCtr ext = new clonaConDocsCtr(con);
              
        PageReference ref = ext.copiaContrato();  
        delete ac;        
        ref = ext.copiaContrato();                  
    }
    
}