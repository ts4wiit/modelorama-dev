/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Interface use tu define methods that external objects must use

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public interface ISSM_ObjectExternInterface_cls {
	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setSapCustomerId set of sap customer id to request
	 * @param  Boolean isDeleted if the record must be deleted
	 */
	List<sObject> getList(Set<String> setSapCustomerId, Boolean isDeleted);

	/**
	 * Get a set of external id for the object		                             
	 * @param  List<sObject> sObjectList list of objects to return the set
	 */
	Set<String> getSetExternalId(List<sObject> sObjectList);
}