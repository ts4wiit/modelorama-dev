/****************************************************************************************************************
    General Information
    -------------------
    Authors:    Héctor Díaz
    email:      hdiaz@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the Delete combos Process
 
    Information about changes (versions)
    =============================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   ------------------------------------------------------
    1.0       Aug 30, 2018     Hector Diaz	               Class creation
    =============================================================================================================
****************************************************************************************************************/
public class ISSM_ComboDeleteDetail_cls {
	
	
	 
	@AuraEnabled 
    public static List<WprDetailCombinationCombo> getProductsDetailxCombination (String lstProductsxComboCombinations,String ComboNumber){
    	Map<String,List<String>> MapCombinationxproduct = new Map<String,List<String>>();
    	Map<String,List<ONTAP__Product__c>> MapProductfinal = new Map<String,List<ONTAP__Product__c>>();
    	List<String> listFinalcombination = new List<String>();
    	List<String> stageListProductxCombo = (List<String>)JSON.deserialize(lstProductsxComboCombinations,List<String>.class);
    	Set<Id> setIdsProducts = new Set<Id>();
    	Set<Id> setIdsProducts2 = new Set<Id>();
    	List<ONTAP__Product__c>	 lstBaseProduct =  new List<ONTAP__Product__c>	();
    	List<ONTAP__Product__c>	 lstBaseProductFinal =  new List<ONTAP__Product__c>	();
    	List<WprDetailCombinationCombo>	 lstWprProduct =  new List<WprDetailCombinationCombo>	();
    	Set<String> setIdsQuantity = new Set<String>();
		Map<String,String> MapCombinationQuantity = new Map<String,String>();
    	Map<String,map<String,String>> mapPrincipal = new Map<String,map<String,String>>();
    	Integer valsumtotalInt =  0 ; 
    	if(stageListProductxCombo != null && !stageListProductxCombo.isEmpty()){
    		for(String objProducts : stageListProductxCombo){
	    		listFinalcombination = new List<String>();
	    		List<String> viewDetailComboSelected  = objProducts.split(':');	
	    		
	    		if( ComboNumber == viewDetailComboSelected[2]){
	    			listFinalcombination.add(objProducts);
	    			setIdsProducts.add(viewDetailComboSelected[0]);
	    			setIdsQuantity.add(viewDetailComboSelected[0]+':'+viewDetailComboSelected[4]+':'+viewDetailComboSelected[1]); 
	    			MapCombinationQuantity.put(viewDetailComboSelected[4],viewDetailComboSelected[3]);
	    			if(MapCombinationxproduct.containsKey(viewDetailComboSelected[4])){				
	    				MapCombinationxproduct.get(viewDetailComboSelected[4]).add(objProducts);
	    			}else{
	    				MapCombinationxproduct.put(viewDetailComboSelected[4],listFinalcombination);
	    			}
	    			
	    		}
    		}	
    	}

		System.debug('MapCombinationQuantity : '+MapCombinationQuantity);
		System.debug('MapCombinationxproduct values : ' +MapCombinationxproduct.values());
    	for(String iterateMap  : MapCombinationxproduct.keySet()){
    		List<String> lstKeys = new List<String>();
    		Map<String,String> maplstKeys = new Map<String,String>();
    		lstKeys = MapCombinationxproduct.get(iterateMap);
    		for(String lstIterateList :lstKeys ){
    			List<String> viewDetailComboSelected3  = lstIterateList.split(':');	
    			maplstKeys.put(viewDetailComboSelected3[0],viewDetailComboSelected3[1]);
    			
    		}
    		mapPrincipal.put(iterateMap,maplstKeys);
    		valsumtotalInt += Integer.valueOf(MapCombinationQuantity.get(iterateMap)); 
    	}
    	
    	if(MapCombinationxproduct !=null &&  !MapCombinationxproduct.isEmpty()){   	   		
    	
    		for(String iterateMap  : MapCombinationxproduct.keySet()){
    			setIdsProducts2 = new Set<Id>();
    			lstBaseProduct =  new List<ONTAP__Product__c>();
    			
    			for(String objIdsProducts : MapCombinationxproduct.get(iterateMap)){
    				List<String> IdsProductsSelected  = objIdsProducts.split(':');	
    				setIdsProducts2.add(IdsProductsSelected[0]);
    			}
    			lstBaseProduct =  [Select id,ONCALL__Material_Number__c,ONCALL__Material_Name__c,ISSM_QuantityInput__c From ONTAP__Product__c Where Id IN : setIdsProducts2 ];
				for( ONTAP__Product__c objBaseProduct :  lstBaseProduct ){	
	    			
	    			objBaseProduct.ISSM_QuantityInput__c= mapPrincipal.get(iterateMap).get(objBaseProduct.id);
			   		lstBaseProductFinal =  new List<ONTAP__Product__c>();	
			    	lstBaseProductFinal.add(objBaseProduct);
			    	
			    	if(MapProductfinal.containsKey(iterateMap)){					
				   		MapProductfinal.get(iterateMap).add(objBaseProduct);				
			    	}else{
			    		MapProductfinal.put(iterateMap,lstBaseProductFinal);
			    	}
	    					
    			}
    						
    		}	
    		if(MapProductfinal != null && !MapProductfinal.isEmpty()){
    			
    			for(String iteraMapProducts : MapProductfinal.keySet()){
    				
					lstWprProduct.add(new WprDetailCombinationCombo(iteraMapProducts,MapProductfinal.get(iteraMapProducts),ComboNumber,MapCombinationQuantity.get(iteraMapProducts),valsumtotalInt));    				
    			}
    		}
    		
    		System.debug('FINAL MAP WprDetailCombinationCombo : ' + lstWprProduct );
    	}
    	return lstWprProduct;
    	
    } 
	public class WprDetailCombinationCombo{ 
        @AuraEnabled public String strCombination{get;set;}
        @AuraEnabled public List<ONTAP__Product__c> lstProductDetail{get;set;}
      	@AuraEnabled public String strComboNumber{get;set;}
      	@AuraEnabled public String strQuantityInput{get;set;}
      	@AuraEnabled public Integer strQuantityUsedCombination{get;set;}
   
        
        public WprDetailCombinationCombo(String strCombination,List<ONTAP__Product__c> lstProductDetail,String strComboNumber,String strQuantityInput,Integer strQuantityUsedCombination){
            this.strCombination = strCombination;
            this.lstProductDetail = lstProductDetail;
            this.strComboNumber = strComboNumber;
            this.strQuantityInput = strQuantityInput;
            this.strQuantityUsedCombination = strQuantityUsedCombination;
        }
    }
}