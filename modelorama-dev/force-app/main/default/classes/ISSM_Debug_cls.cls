public with sharing class ISSM_Debug_cls {
	

	public static void debug(String className, String classMethod, String classLine, String text){
		System.debug('\n***** '+ className +'\n***** ' + classMethod + '\n***** ' + classLine + '\n***** ' + text);
	}

	public static void debugError(String className, String classMethod, String classLine, String text, Exception exc){
		debug(className, classMethod, classLine, text);
		System.debug('\n***** ERROR ***** '+exc.getMessage()+' '+exc.getLineNumber()+' '+exc.getCause());

	}

	public static void debugList(String className, String classMethod, String classLine, String text, List<SObject> sObjectList){
		debug(className, classMethod, classLine, text + '\n***** List' + sObjectList + '\n***** Size ' + sObjectList.size());

	}


	public static void debug(String className, String classMethod, String classLine, Set<string> text){
		for(String textToPrint : text){
			System.debug('\n***** '+ className +'\n***** ' + classMethod + '\n***** ' + classLine + '\n***** ' + textToPrint);
		}
	}
}