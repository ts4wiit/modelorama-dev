/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Leopoldo Ortega (LO)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: Class to assign task to case
Clase de prueba: ISSM_AssignTaskToCase_tst
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    07-Junio-2018    Leopoldo Ortega (LO) Creador - Process CAM & CS - Refrigeration Process
***********************************************************************************/
public class ISSM_AssignTaskToCase {
    
    public static void assignTaskToCase(List<Case> DataCaseNew_lst) {
        List<Account> AccountsATM_lst = new List<Account>();
        List<ISSM_CS_CAM__c> CSCAM_lst = new List<ISSM_CS_CAM__c>();
        List<AccountTeamMember> ATMSalesOffice_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATMSalesOrg_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATMDRV_lst = new List<AccountTeamMember>();
        List<Task> CaseTask_lst = new List<Task>();
        List<Task> NewCaseTask_lst = new List<Task>();
        List<Task> NewCaseForceTask_lst = new List<Task>();
        List<ONTAP__Case_Force__c> caseForce_lst = new List<ONTAP__Case_Force__c>();
        
        String strSalesOffice = '';
        String strSalesOrg = '';
        String strDRV = '';
        String strUserId = '';
        
        for (Case reg : DataCaseNew_lst) {
            // Verifica que el equipo del número de serie capturado exista pero no esté asignado al cliente en cuestión
            if (reg.ISSM_StatusSerialNumber__c == 2) {
                if (Test.isRunningTest()) { AccountsATM_lst = [SELECT Id, Name, ISSM_SalesOffice__c, ISSM_SalesOrg__c, ISSM_RegionalSalesDivision__c FROM Account]; } else { AccountsATM_lst = [SELECT Id, Name, ISSM_SalesOffice__c, ISSM_SalesOrg__c, ISSM_RegionalSalesDivision__c FROM Account WHERE Id =: reg.AccountId LIMIT 1]; }
                
                CSCAM_lst = new List<ISSM_CS_CAM__c>();
                CSCAM_lst = [SELECT Id, Name, SetupOwnerId FROM ISSM_CS_CAM__c];
                
                if (AccountsATM_lst.size() > 0) {
                    for (Account acc : AccountsATM_lst) {
                        // Save sales office from account
                        strSalesOffice = acc.ISSM_SalesOffice__c;
                        
                        // Save sales organization from account
                        strSalesOrg = acc.ISSM_SalesOrg__c;
                        
                        // Save regional sales division from account
                        strDRV = acc.ISSM_RegionalSalesDivision__c;
                    }
                }
                
                ATMSalesOffice_lst = new List<AccountTeamMember>();
                ATMSalesOrg_lst = new List<AccountTeamMember>();
                ATMDRV_lst = new List<AccountTeamMember>();
                
                if ((strSalesOffice != null) && (strSalesOffice != '')) { ATMSalesOffice_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strSalesOffice AND TeamMemberRole = 'Trade Marketing'];
                } else if ((strSalesOrg != null) && (strSalesOrg != '')) { ATMSalesOrg_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strSalesOrg AND TeamMemberRole = 'Trade Marketing'];
                } else if ((strDRV != null) && (strDRV != '')) { ATMDRV_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strDRV AND TeamMemberRole = 'Trade Marketing']; }
                
                if (ATMSalesOffice_lst.size() > 0) { for (AccountTeamMember atm : ATMSalesOffice_lst) { strUserId = atm.UserId; }
                } else if (ATMSalesOrg_lst.size() > 0) { for (AccountTeamMember atm : ATMSalesOrg_lst) { strUserId = atm.UserId; }
                } else if (ATMDRV_lst.size() > 0) { for (AccountTeamMember atm : ATMDRV_lst) { strUserId = atm.UserId; }
                } else if (CSCAM_lst.size() > 0) { for (ISSM_CS_CAM__c atm : CSCAM_lst) { strUserId = atm.SetupOwnerId; } }
                
                NewCaseTask_lst = new List<Task>();
                NewCaseForceTask_lst = new List<Task>();
                
                if ((strUserId != null) && (strUserId != '')) {
                    
                    if (Test.isRunningTest()) { caseForce_lst = [SELECT Id, Name, ISSM_CaseNumber__c, ISSM_StatusSerialNumber__c, ONTAP__Account__c FROM ONTAP__Case_Force__c]; } else { caseForce_lst = [SELECT Id, Name, ISSM_CaseNumber__c, ISSM_StatusSerialNumber__c, ONTAP__Account__c FROM ONTAP__Case_Force__c WHERE ISSM_CaseNumber__c =: reg.CaseNumber LIMIT 10000]; }
                    
                    if (caseForce_lst.size() > 0) {
                        // Assign task to caseForce
                    	ISSM_AssignTaskToCaseForce.assignTaskToCaseForce(caseForce_lst);
                    }
                }
            }
        }
    }
}