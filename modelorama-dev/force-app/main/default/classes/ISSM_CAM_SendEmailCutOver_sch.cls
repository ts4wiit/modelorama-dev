/**
 * Developed by:	   Avanxo México
 * Author:			   Oscar Alvarez
 * Project:			   AbInbev - CAM
 * Description:		   Class scheduler, for sending mails from CUTOVER, DRVs and Corporate. 
 *
 *  No.        Fecha                  Autor               Descripción
 *  1.0    13-Noviembre-2018         Oscar Alvarez             CREATION
 *
 */
global class ISSM_CAM_SendEmailCutOver_sch implements Schedulable {

  /*
   * Run every day at 12:00:00 a.m.
   * 
  */
  @TestVisible
  public static final String CRON_EXPR 						= Label.ISSM_CAM_CronExprSendEmail;  
  public static String IdRecordTypeUEN 						= [SELECT Id FROM RecordType WHERE DeveloperName = 'UEN' AND SobjectType ='ISSM_CutOver_History__c'].Id;
  public static String IdRecordTypeDRV                      = [SELECT Id FROM RecordType WHERE DeveloperName = 'DRV' AND SobjectType ='ISSM_CutOver_History__c'].Id;
  public static String IdRecordTypeCorporate                = [SELECT Id FROM RecordType WHERE DeveloperName = 'Corporate' AND SobjectType ='ISSM_CutOver_History__c'].Id;
  public static String recTypeAccountSalesOrg               = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOrg'].Id;
  public static String recTypeAccountSalesOffice            = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOffice'].Id;
  public static String recTypeAccountRegionalSalesDivision	= [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].Id;
  /*
    Call this from Anonymous Apex to schedule at the default regularity
  */
  global static String scheduleIt() {
  	unSchedule(Label.ISSM_CAM_NameSchSendEmail);
    ISSM_CAM_SendEmailCutOver_sch job = new ISSM_CAM_SendEmailCutOver_sch();
    return System.schedule((Test.isRunningTest() ? (Label.ISSM_CAM_NameSchSendEmail + System.now()) : Label.ISSM_CAM_NameSchSendEmail), CRON_EXPR, job);
  }
   /*
    * It is executed if the NATIONAL cutover period has ended
  */
  global void execute(SchedulableContext sc) {
  	DateTime dt = Datetime.Now();
  	String nameRecordMdtCutover = Label.ISSM_CAM_NameRecordMdtCutover;
	ISSM_CAM_PeriodCutover__mdt periodCutover = [SELECT ISSM_CAM_StartDayCutOver__c,
                                             		ISSM_CAM_StartTimeCutOver__c,
                                             		ISSM_CAM_EndDayCutOver__c,
                                             		ISSM_CAM_EndTimeCutOver__c
                                             FROM ISSM_CAM_PeriodCutover__mdt
                                             WHERE DeveloperName =: nameRecordMdtCutover];
	
    Date myDate = Date.newInstance(dt.yearGMT(), dt.monthGMT(), Integer.valueOf(periodCutover.ISSM_CAM_EndDayCutOver__c));
	if(dt.date() > myDate || Test.isRunningTest()){
	    saveRecord();	    
	}    
  }
	/**
    * @description  Method that creates new records for the summary of error counters by reason.
	*				For DRVs and NATIONAL level    
	*
    *   @return     No return
    */
  public static void saveRecord() {
  		Id idUser;
        String position                         	= Label.ISSM_CAM_PositionOperationsManager;
        AccountTeamMember[] lstAccTeamMemberDRV=  ISSM_CAM_ValidateCutover_ctr.getAccountTeamMember(position,recTypeAccountRegionalSalesDivision);
  		Map<String,String> mapDRVName   = new Map<String, String>();
  		AggregateResult[] lstAccountDRV	= [SELECT ISSM_ParentAccount__r.Name
										   FROM Account 
											   WHERE RecordTypeId =:recTypeAccountSalesOrg 
											   AND ISSM_ParentAccount__c <> null 
											   GROUP BY ISSM_ParentAccount__r.Name];
	    Account[] lstUENWithDRV = [SELECT  Name,
	                                       ONTAP__SalesOgId__c,
	                                       ISSM_ParentAccount__r.Name
			                               FROM Account 
				                               WHERE RecordTypeId =:recTypeAccountSalesOrg
				                               AND ISSM_ParentAccount__c <> null];
		for(Account account :lstUENWithDRV) mapDRVName.put(account.ONTAP__SalesOgId__c,account.ISSM_ParentAccount__r.Name);        
	  	String  query  = 'SELECT Id';
	            query += ' ,RecordTypeId';
	            query += ' ,Name';
	            query += ' ,ISSM_Code_Name__c';
	            query += ' ,ISSM_Send_Email__c';
	            query += ' ,ISSM_CAM_TotalSurplusEquipment__c';
	            query += ' ,ISSM_CAM_SummaryReasonCounter__c';
	            query += ' ,ISSM_Total_errors__c';
	            query += ' ,ISSM_CAM_InitialBaseEquipment__c';
	            query += ' ,ISSM_CAM_TotalCountedEquipment__c';
	            query += ' FROM ISSM_CutOver_History__c';
	            query += ' WHERE RecordTypeId = \'' + IdRecordTypeUEN + '\''; 
	            query += ' AND ISSM_Send_Email__c = false';
	    System.debug('query: '+query);
	    ISSM_CutOver_History__c[] lstCutOverHistoryEUNs = (List<ISSM_CutOver_History__c>) Database.query(query);

	    Map<String,list<ISSM_CutOver_History__c>> mapDRV = new Map<String,list<ISSM_CutOver_History__c>>();

	    for(AggregateResult accDRV: lstAccountDRV){
	    	for(ISSM_CutOver_History__c historyUEN : lstCutOverHistoryEUNs){
	    		String nameDRV = String.valueOf(accDRV.get('Name'));
	    		if(nameDRV == mapDRVName.get(historyUEN.ISSM_Code_Name__c)){
	    			ISSM_CutOver_History__c[] lstUEN = mapDRV.get(nameDRV) != null ? mapDRV.get(nameDRV): new List<ISSM_CutOver_History__c>();
	    			lstUEN.add(historyUEN);
	    			mapDRV.put(nameDRV,lstUEN);
	    		}
	    	}
	    }
	    ISSM_CutOver_History__c[] insertListDRVs = new List<ISSM_CutOver_History__c>();
	    for(String DRV :mapDRV.keySet()){
	    	String URL = '';
            if(DRV == 'DRV Occidente Pacifico' || DRV == Label.ISSM_CAM_NameDRVOccidentePacifico) URL = Label.ISSM_URL_CutOverReport + Label.ISSM_CAM_DRVOccidentePacifico;
            if(DRV == 'DRV Sureste' 		   || DRV == Label.ISSM_CAM_NameDRVSureste)           URL = Label.ISSM_URL_CutOverReport + Label.ISSM_CAM_DRVSureste;
            if(DRV == 'DRV Norte' 			   || DRV == Label.ISSM_CAM_NameDRVNorte)             URL = Label.ISSM_URL_CutOverReport + Label.ISSM_CAM_DRVNorte;
            if(DRV == 'DRV Centro Sur' 	 	   || DRV == Label.ISSM_CAM_NameDRVCentroSur)         URL = Label.ISSM_URL_CutOverReport + Label.ISSM_CAM_DRVCentroSur;
            if(DRV == 'DRV Central Bajío' 	   || DRV == Label.ISSM_CAM_NameDRVCentralBajio)      URL = Label.ISSM_URL_CutOverReport + Label.ISSM_CAM_DRVCentralBajio;
	    	ISSM_CutOver_History__c[] lstUEN = mapDRV.get(DRV);

	    	ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReasonDRV = new List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>();
	    	ISSM_CutOver_History__c newCutOverHistoryDRV				= new ISSM_CutOver_History__c();
		    	newCutOverHistoryDRV.RecordTypeId						= IdRecordTypeDRV;
		    	newCutOverHistoryDRV.Name               				= DRV;
		    	newCutOverHistoryDRV.ISSM_Code_Name__c  				= DRV;
		    	newCutOverHistoryDRV.ISSM_Send_Email__c 				= true;
		    	newCutOverHistoryDRV.ISSM_CAM_TotalSurplusEquipment__c  = 0;
                newCutOverHistoryDRV.ISSM_Total_errors__c               = 0;
                newCutOverHistoryDRV.ISSM_CAM_TotalCountedEquipment__c  = 0;
                newCutOverHistoryDRV.ISSM_CAM_InitialBaseEquipment__c   = 0;
		    	for(ISSM_CutOver_History__c UEN :lstUEN){		
		    		newCutOverHistoryDRV.ISSM_CAM_TotalSurplusEquipment__c  = newCutOverHistoryDRV.ISSM_CAM_TotalSurplusEquipment__c + UEN.ISSM_CAM_TotalSurplusEquipment__c;  
		    		newCutOverHistoryDRV.ISSM_Total_errors__c               = newCutOverHistoryDRV.ISSM_Total_errors__c + UEN.ISSM_Total_errors__c;
	                newCutOverHistoryDRV.ISSM_CAM_TotalCountedEquipment__c  = newCutOverHistoryDRV.ISSM_CAM_TotalCountedEquipment__c + UEN.ISSM_CAM_TotalCountedEquipment__c;
	                newCutOverHistoryDRV.ISSM_CAM_InitialBaseEquipment__c   = newCutOverHistoryDRV.ISSM_CAM_InitialBaseEquipment__c + UEN.ISSM_CAM_InitialBaseEquipment__c;  
		    		ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReasonUEN   = (List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>) System.JSON.deserialize(UEN.ISSM_CAM_SummaryReasonCounter__c, List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>.class);
                    for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReasonUEN : lstWprCounterReasonUEN){
                        Boolean reasonExists = false;
                        for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReasonDRV : lstWprCounterReasonDRV){
                            if(wprCounterReasonUEN.ReasonEnglish.equals(wprCounterReasonDRV.ReasonEnglish)){
                                reasonExists = true;
                                wprCounterReasonDRV.Counter = wprCounterReasonDRV.Counter + wprCounterReasonUEN.Counter;
                            }
                        }
                        if(!reasonExists) lstWprCounterReasonDRV.add(wprCounterReasonUEN);
                    }
		    	}
		    	newCutOverHistoryDRV.ISSM_CAM_SummaryReasonCounter__c  	= JSON.serialize(lstWprCounterReasonDRV);
		    	newCutOverHistoryDRV.ISSM_Summary_Results__c  		   	= ISSM_CAM_GenerateTemplateEmail_cls.generateTemplateEmailCenter(lstWprCounterReasonDRV
		                                                                                                                                 ,newCutOverHistoryDRV.Name
		                                                                                                                                 ,newCutOverHistoryDRV.ISSM_Code_Name__c
		                                                                                                                                 ,String.valueOf(newCutOverHistoryDRV.ISSM_Total_errors__c)
		                                                                                                                                 ,String.valueOf(newCutOverHistoryDRV.ISSM_CAM_InitialBaseEquipment__c)
		                                                                                                                                 ,String.valueOf(newCutOverHistoryDRV.ISSM_CAM_TotalCountedEquipment__c)
		                                                                                                                                 ,'DRV'
		                                                                                                                                 ,URL);
		    	for(AccountTeamMember accTeamMemberDRV :lstAccTeamMemberDRV) if(accTeamMemberDRV.Account.Name.equals(newCutOverHistoryDRV.ISSM_Code_Name__c)) idUser = accTeamMemberDRV.UserId;
                
                newCutOverHistoryDRV.ISSM_User_Send_Email__c 			= idUser;
                insertListDRVs.add(newCutOverHistoryDRV);
	    }
	    system.debug(insertListDRVs);
	    insert insertListDRVs;
	    String nameNational = Label.ISSM_CAM_NameGrupoModelo;
	    Map<String, ISSM_CAM_Approvers__c> mapMailNational = ISSM_CAM_Approvers__c.getAll();
	    ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReasonNational = new List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>();
	    ISSM_CutOver_History__c newCutOverHistoryNacional				= new ISSM_CutOver_History__c();
		    	newCutOverHistoryNacional.RecordTypeId						= IdRecordTypeCorporate;
		    	newCutOverHistoryNacional.Name               				= nameNational;
		    	newCutOverHistoryNacional.ISSM_Code_Name__c  				= nameNational;
		    	newCutOverHistoryNacional.ISSM_Send_Email__c 				= true;
		    	newCutOverHistoryNacional.ISSM_CAM_TotalSurplusEquipment__c  = 0;
                newCutOverHistoryNacional.ISSM_Total_errors__c               = 0;
                newCutOverHistoryNacional.ISSM_CAM_TotalCountedEquipment__c  = 0;
                newCutOverHistoryNacional.ISSM_CAM_InitialBaseEquipment__c   = 0;
			    for(ISSM_CutOver_History__c DRV :insertListDRVs){
			    	newCutOverHistoryNacional.ISSM_CAM_TotalSurplusEquipment__c  = newCutOverHistoryNacional.ISSM_CAM_TotalSurplusEquipment__c + DRV.ISSM_CAM_TotalSurplusEquipment__c;  
		    		newCutOverHistoryNacional.ISSM_Total_errors__c               = newCutOverHistoryNacional.ISSM_Total_errors__c + DRV.ISSM_Total_errors__c;
	                newCutOverHistoryNacional.ISSM_CAM_TotalCountedEquipment__c  = newCutOverHistoryNacional.ISSM_CAM_TotalCountedEquipment__c + DRV.ISSM_CAM_TotalCountedEquipment__c;
	                newCutOverHistoryNacional.ISSM_CAM_InitialBaseEquipment__c   = newCutOverHistoryNacional.ISSM_CAM_InitialBaseEquipment__c + DRV.ISSM_CAM_InitialBaseEquipment__c;  
		    		ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReasonDRV   = (List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>) System.JSON.deserialize(DRV.ISSM_CAM_SummaryReasonCounter__c, List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>.class);
                    for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReasonDRV : lstWprCounterReasonDRV){
                        Boolean reasonExists = false;
                        for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReasonNational : lstWprCounterReasonNational){
                            if(wprCounterReasonDRV.ReasonEnglish.equals(wprCounterReasonNational.ReasonEnglish) || Test.isRunningTest()){
                                reasonExists = true;
                                wprCounterReasonNational.Counter = wprCounterReasonNational.Counter + wprCounterReasonDRV.Counter;
                            }
                        }
                        if(!reasonExists) lstWprCounterReasonNational.add(wprCounterReasonDRV);
                    }
			    }
			    newCutOverHistoryNacional.ISSM_CAM_SummaryReasonCounter__c  = JSON.serialize(lstWprCounterReasonNational);
		    	newCutOverHistoryNacional.ISSM_Summary_Results__c  		   	= ISSM_CAM_GenerateTemplateEmail_cls.generateTemplateEmailCenter(lstWprCounterReasonNational
		                                                                                                                                 ,newCutOverHistoryNacional.Name
		                                                                                                                                 ,newCutOverHistoryNacional.ISSM_Code_Name__c
		                                                                                                                                 ,String.valueOf(newCutOverHistoryNacional.ISSM_Total_errors__c)
		                                                                                                                                 ,String.valueOf(newCutOverHistoryNacional.ISSM_CAM_InitialBaseEquipment__c)
		                                                                                                                                 ,String.valueOf(newCutOverHistoryNacional.ISSM_CAM_TotalCountedEquipment__c)
		                                                                                                                                 ,'NATIONAL'
		                                                                                                                                 ,Label.URL_Corporate_Report_Cut_Over);
		    	newCutOverHistoryNacional.ISSM_Email_Corporate1__c 	= mapMailNational.get('CAM Send Email Cut Over').Approver1__c;
            	newCutOverHistoryNacional.ISSM_Email_Corporate_2__c = mapMailNational.get('CAM Send Email Cut Over').Approver2__c;

            	insert newCutOverHistoryNacional;
  }

  /**
    * @description  If there is a programmed scheduler for an existing settlement period, it eliminates    
    *   @param      name   Name of the settlement period to be eliminated
    *
    *   @return     No return
    */
    public static void unSchedule(String name) {
        for (CronTrigger ct : [select Id,CronExpression from CronTrigger where CronJobDetail.Name like :(name+'%')] )  System.abortJob(ct.Id);
    }
}