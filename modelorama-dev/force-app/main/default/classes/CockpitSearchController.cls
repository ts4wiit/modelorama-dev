/**-------------------------------------------------------------------------------------------------------------
 * Name: CockpitSearchController_cls
 * Description : Class to search route from huen or office
 * Created date : July 23 2018
 * @author name: Daniel Goncalves
 * -------------------------------------------------------------------------------------------------------------
 * Developer					Date				Description<p />
 * -------------------------------------------------------------------------------------------------------------
 * Daniel Goncalves				23/07/2018		Original version
 * Isaías Velázquez Cortés		12/08/2018		Edited
 * Daniel Goncalves				22/08/2018		Change to Collapsable Tables
 * Daniel Goncalves				03/09/2018		Method for generate CSV File
 * Daniel Goncalves				03/09/2018		Code Refactoring
 * Alberto Gómez				24/09/2018		Edited fetchClientVis method; Deleted fetchClientStatus method.
 * -------------------------------------------------------------------------------------------------------------
 */
public class CockpitSearchController {

	//Class attributes.
	public static String cnivelVisual;
	public static String csearchText;
	public static String ctour;
	public static String cestatus;
	public static String cfechaEjec;
	public static String routeId;

	/**
	 * Return search results
	 *
	 * @param strVisualLevel String
	 * @param strIdLevelField String
	 * @param strTourStatus String
	 * @param strExecutionDate String
	 * @return List ResultsMatrix
	 */
	@AuraEnabled
	public static List<ResultsMatrix> fetchResults(String strVisualLevel, String strIdLevelField, String strTourStatus, String strExecutionDate) {

		//Variables.
		Integer typeVisual = 0;
		Date datExeDate = Date.Today();
		List<ResultsMatrix> valuesList = new List<ResultsMatrix>();
		ResultsMatrix result;

		//Covnert date.
		if(String.isNotEmpty(strExecutionDate)) {
			datExeDate = Date.valueOf(strExecutionDate);
		}

		//IF IS DRV.CockpitStatusOption
		if(strVisualLevel == System.Label.CockpitVisualizationLevelDRV) {
			typeVisual = 0;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				typeVisual = 5;
			}

		//IF IS UEN.
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelSalesOrg) {
			typeVisual = 1;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				typeVisual = 6;
			}

		//IF IS OFFICE.
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelSalesOffice) {
			typeVisual = 2;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				typeVisual = 7;
			}

		//IF IS ROUTE.
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelRoute) {
			typeVisual = 3;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				typeVisual = 8;
			}

		//IF IS TOUR ID.
		} else if(strVisualLevel == AllMobileStaticVariablesClass.STRING_VISUAL_LEVEL_TOUR_ID) {
			typeVisual = 4;
		}

		//Validate according typeVisual.
		switch on typeVisual {
			when 0 {

				//DRV.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitDRVWrapperClass> resDrv = (List<CockpitStatusLogicClass.CockpitDRVWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitDRVWrapperClass item : resDrv) {
					result = new ResultsMatrix(item.strDRVId, item.strDRVDescription, item.strDRVStatusColorText, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, datExeDate, item.datDRVDate);
					valuesList.add(result);
				}
			} when 1 {

				//ORG.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitOrgWrapperClass> resOrg = (List<CockpitStatusLogicClass.CockpitOrgWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitOrgWrapperClass item : resOrg) {
					result = new ResultsMatrix(item.strOrgDRVId, item.strOrgDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOrgId, item.strOrgDescription, item.strOrgStatusColorText, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, datExeDate, item.datOrgDate);
					valuesList.add(result);
				}
			} when 2 {

				//OFFICE.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> resOff = (List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitOfficeWrapperClass item : resOff) {
					result = new ResultsMatrix(item.strOfficeDRVId, item.strOfficeDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOfficeOrgId, item.strOfficeOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOfficeId, item.strOfficeDescription, item.strOfficeStatusColorText, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, datExeDate, item.datOfficeDate);
					valuesList.add(result);
				}
			} when 3 {

				//ROUTE.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			} when 4 {

				//TOUR ID.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, null, strIdLevelField, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			} when 5 {

				//STATUS DRV.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, strTourStatus);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			} when 6 {

				//STATUS ORG.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, strTourStatus);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			} when 7 {

				//STATUS OFFICE.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, strTourStatus);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			} when 8 {

				//STATUS ROUTE.
				Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, strTourStatus);
				if(mpResults == null) {
					return null;
				}
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(typeVisual);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
					valuesList.add(result);
				}
			}
		}
		if((valuesList.size() == 0) || (valuesList.isEmpty()) || (valuesList == null)) {
			return null;
		} else {
			return valuesList;
		}
	}

	/**
	 * Return search results with drill down behavior.
	 *
	 * @param strVisualLevel String
	 * @param strIdLevelField String
	 * @param strTourStatus String
	 * @param strExecutionDate String
	 * @return List ResultsMatrix
	 */
	@AuraEnabled
	public static List<ResultsMatrix> fetchResultsDrillDown(String strVisualLevel, String strIdLevelField, String strTourStatus, String strExecutionDate) {

		//Variables.
		Integer typeVisual = Integer.valueOf(strVisualLevel);
		Date datExeDate = Date.Today();
		ResultsMatrix result;
		List<ResultsMatrix> valuesList = new List<ResultsMatrix>();

		//Assing date.
		if(String.isNotEmpty(strExecutionDate)) {
			datExeDate = Date.valueOf(strExecutionDate);
		}

		//Generate List according visual level.
		Map<Integer,Object> mpResults = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(typeVisual, datExeDate, strIdLevelField, strTourStatus);
		if(mpResults == null) {
			return null;
		}

		//Validate according typeVisual.
		switch on typeVisual {
			when 0 {

				//DRV CHILDS - ORGs.
				List<CockpitStatusLogicClass.CockpitOrgWrapperClass> resOrg = (List<CockpitStatusLogicClass.CockpitOrgWrapperClass>) mpResults.get(1);
				for (CockpitStatusLogicClass.CockpitOrgWrapperClass item : resOrg) {
					if(item.strOrgDRVId == strIdLevelField) {
						result = new ResultsMatrix(item.strOrgDRVId, item.strOrgDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOrgId, item.strOrgDescription, item.strOrgStatusColorText, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, datExeDate, item.datOrgDate);
						valuesList.add(result);
					}
				}
			} when 1 {

				//ORG CHILDS - OFFICEs.
				List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> resOff = (List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>) mpResults.get(2);
				for (CockpitStatusLogicClass.CockpitOfficeWrapperClass item : resOff) {
					if(item.strOfficeOrgId == strIdLevelField) {
						result = new ResultsMatrix(item.strOfficeDRVId, item.strOfficeDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOfficeOrgId, item.strOfficeOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strOfficeId, item.strOfficeDescription, item.strOfficeStatusColorText, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, datExeDate, item.datOfficeDate);
						valuesList.add(result);
					}
				}
			} when 2 {

				//OFFICE CHILDS - ROUTEs.
				List<CockpitStatusLogicClass.CockpitTourWrapperClass> resRou = (List<CockpitStatusLogicClass.CockpitTourWrapperClass>) mpResults.get(3);
				for (CockpitStatusLogicClass.CockpitTourWrapperClass item : resRou) {
					if(item.strTourOfficeId == strIdLevelField) {
						result = new ResultsMatrix(item.strTourDRVId, item.strTourDRVDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOrgId, item.strTourOrgDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourOfficeId, item.strTourOfficeDescription, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourRouteId, AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK, item.strTourId, item.strTourStatus, item.strTourStatusColorText, item.strTourSubStatus, datExeDate, item.datTourDate);
						valuesList.add(result);
					}
				}
			}
		}
		if(valuesList.size() == 0) {
			return null;
		} else {
			return valuesList;
		}
	}

	/**
	 * Class with info from Tour and Route.
	 */
	public class routeTB {

		//Get set variables.
		@AuraEnabled public String strTour {get; set;}
		@AuraEnabled public String strModelo {get; set;}
		@AuraEnabled public String strChofer {get; set;}
		@AuraEnabled public String strVehiculo {get; set;}
		@AuraEnabled public Date datFecha {get; set;}
		@AuraEnabled public String dtmTimeSt {get; set;}
		@AuraEnabled public String dtmTimeEn {get; set;}
		@AuraEnabled public String strEstatus {get; set;}
		@AuraEnabled public String strSubestatus {get; set;}
		@AuraEnabled public String strTourId {get; set;}

		//Builder
		public routeTB (String mod, String tou, Date fec, String cho, String veh, String est, String touId, String tiS, String tiE, String sube) {
			this.strTour = tou;
			this.strModelo = mod;
			this.strChofer = cho;
			this.strVehiculo = veh;
			this.datFecha = fec;
			this.dtmTimeSt = tiS;
			this.dtmTimeEn = tiE;
			this.strEstatus = est;
			this.strSubestatus = sube;
			this.strTourId = touId;
		}
	}

	/**
	 * Wrapper class to return all data results.
	 */
	public class ResultsMatrix {

		//Get set variables.
		@AuraEnabled public String strIdDRV {get; set;}
		@AuraEnabled public String strNameDRV {get; set;}
		@AuraEnabled public String strStatusDRV {get; set;}
		@AuraEnabled public String strIdUEN {get; set;}
		@AuraEnabled public String strNameUEN {get; set;}
		@AuraEnabled public String strStatusUEN {get; set;}
		@AuraEnabled public String strIdOff {get; set;}
		@AuraEnabled public String strNameOff {get; set;}
		@AuraEnabled public String strStatusOff {get; set;}
		@AuraEnabled public String strIdRoute {get; set;}
		@AuraEnabled public String strIdTour {get; set;}
		@AuraEnabled public String strStatusTour {get; set;}
		@AuraEnabled public String strStatusTourColor {get; set;}
		@AuraEnabled public String strSubStatusTour {get; set;}
		@AuraEnabled public Date datSearchDate {get; set;}
		@AuraEnabled public Date datTourDate {get; set;}

		//Builder.
		public ResultsMatrix ( String id1, String name1, String status1, String id2, String name2, String status2, String id3, String name3, String status3, String id4, String name4, String id5, String status5, String color, String subStatus5, Date seaDate, Date touDate) {
			this.strIdDRV = id1;
			this.strNameDRV = name1;
			this.strStatusDRV = status1;
			this.strIdUEN = id2;
			this.strNameUEN = name2;
			this.strStatusUEN = status2;
			this.strIdOff = id3;
			this.strNameOff = name3;
			this.strStatusOff = status3;
			this.strIdRoute = id4;
			this.strStatusTour = name4;
			this.strIdTour = id5;
			this.strStatusTour = status5;
			this.strStatusTourColor = color;
			this.strSubStatusTour = subStatus5;
			this.datSearchDate = seaDate;
			this.datTourDate = touDate;
		}
	}

	/**
	 * Fetch route data.
	 *
	 * @param strAlternateName String
	 * @return routeTB List
	 */
	@AuraEnabled
	public static List<routeTB> fetchFTRoute(String strAlternateName) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		String strDateStart = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		String strDateEnd = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		ONTAP__Tour__c objTourQueried = new ONTAP__Tour__c();
		List<routeTB> lstRouteTB = new List<routeTB>();
		List<ONTAP__Tour__c> lstOntapTourQueried = new List<ONTAP__Tour__c>();

		//Validate empty strAlternateName.
		if(String.isEmpty(strAlternateName)) {
			return null;
		} else {
			strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ROUTE_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALTERNATE_NAME_WHERE_EQUALS + strAlternateName + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_LIMIT_1;
			lstOntapTourQueried = Database.query(strQuery);
			if(!lstOntapTourQueried.isEmpty()) {
				objTourQueried = lstOntapTourQueried.get(0);
				if(objTourQueried != null ) {
					if(objTourQueried.ONTAP__ActualStart__c != NULL) {
						strDateStart = objTourQueried.ONTAP__ActualStart__c.format(AllMobileStaticVariablesClass.STRING_DATE_FORMAT_DD_MM_H_MM_A);
					}
					if(objTourQueried.ONTAP__ActualEnd__c != NULL) {
						strDateEnd = objTourQueried.ONTAP__ActualEnd__c.format(AllMobileStaticVariablesClass.STRING_DATE_FORMAT_DD_MM_H_MM_A);
					}
					routeTB rtResult = new routeTB(objTourQueried.RecordType.DeveloperName, objTourQueried.AlternateName__c, objTourQueried.ONTAP__TourDate__c, objTourQueried.ONTAP__DriverId__c, objTourQueried.ONTAP__VehicleId__c, objTourQueried.ONTAP__TourStatus__c, objTourQueried.ONTAP__TourId__c, strDateStart, strDateEnd, objTourQueried.TourSubStatus__c);
					lstRouteTB.add(rtResult);
				}
			}
		}
		return lstRouteTB;
	}

	/**
	 * Returns Coci Header data.
	 *
	 * @param strAlternateName String
	 * @return List ONTAP__COCIHeader__c
	 */
	@AuraEnabled
	public static List<ONTAP__COCIHeader__c> fetchSTChIChOHd(String strAlternateName) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<ONTAP__COCIHeader__c> lstCOCIHeader = new List<ONTAP__COCIHeader__c>();

		//Validate empty strAlternateName.
		if(String.isNotEmpty(strAlternateName)) {
			strQuery = AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_WHERE + strAlternateName + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
			if(String.isNotEmpty(strQuery)) {
				lstCOCIHeader = Database.query(strQuery);
			}
		}
		return lstCOCIHeader;
	}

	/**
	 * Returns Data of Coci Item.
	 *
	 * @param douCociIdIt Double
	 * @param strAlternateName String
	 * @return List ONTAP__COCIItem__c
	 */
	@AuraEnabled
	public static List<ONTAP__COCIItem__c> fetchSTChIChOIt(Double douCociIdIt, String strAlternateName) {

		//Variables.
		String strQueryCH = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<ONTAP__COCIItem__c> lstCOCIItem = new List<ONTAP__COCIItem__c>();
		List<ONTAP__COCIHeader__c> lstCociHeader = new List<ONTAP__COCIHeader__c>();

		//Fetch Coci Header.
		strQueryCH = AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_WHERE + douCociIdIt + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_AND + strAlternateName + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_LIMIT_1;
		lstCociHeader = Database.query(strQueryCH);
		if(!lstCociHeader.isEmpty()) {
			Set<Id> setIdCOciHeader = new Set<Id>();
			for(ONTAP__COCIHeader__c objONTAPCOCIHeader : lstCociHeader) {
				setIdCOciHeader.add(objONTAPCOCIHeader.Id);
			}
			lstCOCIItem = [SELECT ONTAP__ProductId__c, ProductDescription__c, ONTAP__COCIId__c, ONTAP__UnitId__c, ONTAP__PlanQuantity__c, ONTAP__RealQuantity__c, Difference_Quantity__c FROM ONTAP__COCIItem__c WHERE ONTAP__COCI_Header__c =: setIdCOciHeader];
		}
		return lstCOCIItem;
	}

	/**
	 * Return data of Coci Payment.
	 *
	 * @param douCociIdIt Double
	 * @param strAlternateName String
	 * @return List ONTAP_COCI_Payment__c
	 */
	@AuraEnabled
	public static List<ONTAP__COCI_Payment__c> fetchSTChIChOPy(Double douCociIdIt, String strAlternateName) {

		//Variables.
		String strQueryCH = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<ONTAP__COCI_Payment__c> lstCOCIPayment = new List<ONTAP__COCI_Payment__c>();
		List<ONTAP__COCIHeader__c> lstCociHeader = new List<ONTAP__COCIHeader__c>();

		//Fetch Coci Header.
		strQueryCH = AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_WHERE + douCociIdIt + AllMobileStaticVariablesClass.STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_AND + strAlternateName + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_LIMIT_1;
		lstCociHeader = Database.query(strQueryCH);
		if(!lstCociHeader.isEmpty()) {
			Set<Id> setIdCOciHeader = new Set<Id>();
			for(ONTAP__COCIHeader__c objONTAPCOCIHeader : lstCociHeader) {
				setIdCOciHeader.add(objONTAPCOCIHeader.Id);
			}
			lstCOCIPayment = [SELECT ONTAP__COCIId__c, ONTAP__PaymentTypeId__c, ONTAP__PlanQuantity__c, ONTAP__RealQuantity__c, Difference_Quantity__c FROM ONTAP__COCI_Payment__c WHERE ONTAP__COCI_Header__c =: setIdCOciHeader];
		}
		return lstCOCIPayment;
	}

	/**
	 * Tour Docs Info.
	 *
	 * @param strTourId String
	 * @return List CockpitEventDocument__c
	 */
	@AuraEnabled
	public static List<CockpitEventDocument__c> fetchTourDoc(String strTourId) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<CockpitEventDocument__c> pickListValuesList = new List<CockpitEventDocument__c>();

		//Fetch Tour documents.
		strQuery = AllMobileStaticVariablesClass.STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR_ID + strTourId + AllMobileStaticVariablesClass.STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE_SEQUENCE;
		pickListValuesList = Database.query(strQuery);
		return pickListValuesList;
	}

	/**
	 * Visit Docs Info.
	 *
	 * @param strClientId String
	 * @param strTourId String
	 * @return List CockpitEventDocument__c
	*/
	@AuraEnabled
	public static List<CockpitEventDocument__c> fetchVisitDoc(String strClientId, String strTourId) {

		//Variables.
		String strQueryVisDoc = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		String strQueryVisit = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		Set<Decimal> setDecVisitSequence = new Set<Decimal>();
		List<Event> lstEvent = new List<Event>();
		List<CockpitEventDocument__c> pickListValuesList = new List<CockpitEventDocument__c>();

		//Fetch the event.
		if(String.isNotEmpty(strClientId)) {
			strQueryVisit = AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_ALTERNATE_NAME + strTourId + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_AND_VISIT + strClientId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
			lstEvent = Database.query(strQueryVisit);
			if(!lstEvent.isEmpty()) {
				for(Event objEvent : lstEvent) {
					setDecVisitSequence.add(objEvent.Sequence__c);
				}
			}
		}

		//Fetch the documents of the event.
		if(!setDecVisitSequence.isEmpty() && String.isNotEmpty(strTourId)) {
			pickListValuesList = [SELECT Name, CockpitDocumentType__c, CockpitErrorDescription__c, CockpitTourId__c FROM CockpitEventDocument__c WHERE CockpitTourId__c  =: strTourId AND CockpitVisitSequence__c =: setDecVisitSequence];
		}
		return pickListValuesList;
	}

	/**
	 * Class for Client Visits Data.
	 */
	public class clientVisits {

		//Get set variables.
		@AuraEnabled public String strClient {get; set;}
		@AuraEnabled public String strTour {get; set;}
		@AuraEnabled public String strEstatus {get; set;}
		@AuraEnabled public Decimal decVisitId {get; set;}

		//Builder.
		public clientVisits (String cli, String tou, String est, Decimal decVisitSequence) {
			this.strClient = cli;
			this.strTour = tou;
			this.strEstatus = est;
			this.decVisitId = decVisitSequence;
		}
	}

	/**
	 * Tour Vistis Data.
	 *
	 * @param strTourId String
	 * @return List clientVisits
	 */
	@AuraEnabled
	public static List<clientVisits> fetchClient(String strTourId) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<Event> lstEvent = new List<Event>();
		List<clientVisits> lstClientVis = new List<clientVisits>();

		//Fetch the event.
		if(String.isNotEmpty(strTourId)) {
			strQuery = AllMobileStaticVariablesClass.STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE + strTourId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
		}
		lstEvent = Database.query(strQuery);
		if(lstEvent.size() > 0) {
			for (Event item : lstEvent) {
				clientVisits result = new clientVisits(item.What.Name, item.VisitList__r.AlternateName__c, item.ONTAP__Estado_de_visita__c, item.Sequence__c);
				lstClientVis.add(result);
			}
		}
		return lstClientVis;
	}

	/**
	 * Class for Visit Event info.
	 */
	public class visitEvents {

		//Get set variables.
		@AuraEnabled public String strClienteSAP {get; set;}
		@AuraEnabled public Decimal decVisitId {get; set;}
		@AuraEnabled public String strTypeVis {get; set;}
		@AuraEnabled public Date datDateTour {get; set;}
		@AuraEnabled public String dtmStartTime {get; set;}
		@AuraEnabled public String dtmEndTime {get; set;}
		@AuraEnabled public String strStatus {get; set;}
		@AuraEnabled public String strSubStatus {get; set;}

		//Builder.
		public visitEvents (String sap, Decimal vis, String typ, Date dat, String st, String en, String sta, String sub) {
			this.strClienteSAP = sap;
			this.decVisitId = vis;
			this.strTypeVis = typ;
			this.datDateTour = dat;
			this.dtmStartTime = st;
			this.dtmEndTime = en;
			this.strStatus = sta;
			this.strSubStatus = sub;
		}
	}

	/**
	 * Data of Visits.
	 *
	 * @param strTourId String.
	 * @param strClientId String.
	 * @return Lista vistEvents.
	 */
	@AuraEnabled
	public static List<visitEvents> fetchClientVis(String strTourId, String strClientId) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		visitEvents result;
		List<Event> lstEvnt = new List<Event>();
		List<visitEvents> lstVisEvnt = new List<visitEvents>();

		//Fetch Visit data.
		if(String.isNotBlank(strTourId) && String.isNotEmpty(strClientId)) {
			strQuery = AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_ALTERNATE_NAME + strTourId + AllMobileStaticVariablesClass.STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_AND_VISIT + strClientId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
			lstEvnt = Database.query(strQuery);
			if(!lstEvnt.isEmpty()) {
				for(Event objEventIt : lstEvnt) {
					result = new visitEvents(objEventIt.CustomerId__c, objEventIt.Sequence__c, objEventIt.VisitList__r.RecordType.DeveloperName, objEventIt.VisitList__r.ONTAP__TourDate__c, objEventIt.StartDateTime.format(AllMobileStaticVariablesClass.STRING_DATE_FORMAT_DD_MM_H_MM_A), objEventIt.EndDateTime.format(AllMobileStaticVariablesClass.STRING_DATE_FORMAT_DD_MM_H_MM_A), objEventIt.ONTAP__Estado_de_visita__c, objEventIt.EventSubestatus__c);
					lstVisEvnt.add(result);
				}
			}
		}
		return lstVisEvnt;
	}

	/**
	 * Data of Cliente Sales.
	 *
	 * @param strTourId String
	 * @param strClientId String
	 * @return Event List
	 */
	@AuraEnabled
	public static List<Event> fetchClientSales(String strTourId, String strClientId) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<Event> lstEvnt = new List<Event>();

		//Fetch client.
		if((String.isNotEmpty(strTourId)) && (String.isNotEmpty(strClientId))) {
			strQuery = AllMobileStaticVariablesClass.STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR + strTourId + AllMobileStaticVariablesClass.STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_WHERE_CLIENT + strClientId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
		}
		lstEvnt = Database.query(strQuery);
		return lstEvnt;
	}

	/**
	 * Data of Client Balance.
	 *
	 * @param strTourId String
	 * @param strClientId String
	 * @return Event List
	 */
	@AuraEnabled
	public static List<Event> fetchClientBalance(String strTourId, String strClientId) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		List<Event> lstEvnt = new List<Event>();

		//Fetch client.
		if((String.isNotEmpty(strTourId)) && (String.isNotEmpty(strClientId))){
			strQuery = AllMobileStaticVariablesClass.STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR + strTourId + AllMobileStaticVariablesClass.STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_WHERE_CLIENT + strClientId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE;
		}
		lstEvnt = Database.query(strQuery);
		return lstEvnt;
	}

	/**
	 * Returns the status list for tours.
	 *
	 * @return List String
	 */
	@AuraEnabled
	public static List<String> getEstatus() {

		//Variables.
		List<String> lstStr= new List<String>();

		//Get status of Tour.
		Schema.DescribeFieldResult fieldResult = ONTAP__Tour__c.ONTAP__TourStatus__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			lstStr.add(pickListVal.getLabel());
		}
		return lstStr;
	}

	/**
	 * Class for Doc CSV info.
	 */
	public class DocCSV {

		//Get set variables.
		@AuraEnabled public String strOrgId {get; set;}
		@AuraEnabled public String strOrgName {get; set;}
		@AuraEnabled public String strOffId {get; set;}
		@AuraEnabled public String strOffName {get; set;}
		@AuraEnabled public String strRouId {get; set;}
		@AuraEnabled public String strTouId {get; set;}
		@AuraEnabled public String strDocSAP {get; set;}
		@AuraEnabled public String strTipoDoc {get; set;}
		@AuraEnabled public String strEstatus {get; set;}

		//Builder.
		public DocCSV (String orgI, String orgN, String offI, String offN, String rouI, String touI, String sap, String tip, String est) {
			this.strOrgId = orgI;
			this.strOrgName = orgN;
			this.strOffId = offI;
			this.strOffName = offN;
			this.strRouId = rouI;
			this.strTouId = touI;
			this.strDocSAP = sap;
			this.strTipoDoc = tip;
			this.strEstatus = est;
		}
	}

	/**
	 * Generate the CSV Doc inserting the info of tours documents.
	 *
	 * @param strVisualLevel String
	 * @param strIdLevelField String
	 * @param strTourStatus String
	 * @param strExecutionDate String
	 * @return List of DocCSV objects
	 */
	@AuraEnabled
	public static List<DocCSV> generateCSVDoc (String strVisualLevel, String strIdLevelField, String strTourStatus, String strExecutionDate) {

		//Variables.
		String strQuery = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		Integer intTypeVisual = 0;
		Date datExecutionDate;
		DocCSV docCSVresult;
		Set<String> setStrTourID = new Set<String>();
		List<DocCSV> lstDocCSV = new List<DocCSV>();
		List<ONTAP__tour__c> lstOntapTour = new List<ONTAP__tour__c>();

		//Convert date.
		if(String.isNotEmpty(strExecutionDate)) {
			datExecutionDate = Date.valueOf(strExecutionDate);
		}

		//IF IS DRV.
		if(strVisualLevel == System.Label.CockpitVisualizationLevelDRV) {
			intTypeVisual = 0;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				intTypeVisual = 5;
			}

		//IF IS UEN
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelSalesOrg) {
			intTypeVisual = 1;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				intTypeVisual = 6;
			}

		//IF IS OFFICE
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelSalesOffice) {
			intTypeVisual = 2;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				intTypeVisual = 7;
			}

		//IF IS ROUTE
		} else if(strVisualLevel == System.Label.CockpitVisualizationLevelRoute) {
			intTypeVisual = 3;
			if((strTourStatus != AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK) && (strTourStatus != AllMobileStaticVariablesClass.STRING_OPTION)) {
				intTypeVisual = 8;
			}

		//IF IS TOUR ID
		} else if(strVisualLevel == AllMobileStaticVariablesClass.STRING_VISUAL_LEVEL_TOUR_ID) {
			intTypeVisual = 4;
		}
		lstOntapTour = CockpitStatusLogicClass.getONTAPTourToSF(strIdLevelField, intTypeVisual, datExecutionDate, strTourStatus);
		if(!lstOntapTour.isEmpty()) {
			for(ONTAP__tour__c objTourIter : lstOntapTour) {
				setStrTourID.add(objTourIter.ONTAP__TourId__c);
			}
		}
		if((setStrTourID != null) && (!setStrTourID.isEmpty()) && (setStrTourID.size() > 0)) {
			strQuery = AllMobileStaticVariablesClass.STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_SELECT + AllMobileStaticVariablesClass.STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_FROM + AllMobileStaticVariablesClass.STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE;
			List<CockpitEventDocument__c> lstEvntDoc = Database.query(strQuery);
			for(CockpitEventDocument__c item : lstEvntDoc) {
				docCSVresult = new DocCSV(item.CockpitTourLK__r.OrgId__c, item.CockpitTourLK__r.SalesOrg__c, item.CockpitTourLK__r.SalesOffice__c, item.CockpitTourLK__r.SalesOfficeName__c, item.CockpitTourLK__r.ONTAP__RouteId__c, item.CockpitTourId__c, item.Name, item.CockpitDocumentType__c, item.CockpitErrorDescription__c);
				lstDocCSV.add(docCSVresult);
			}
			return lstDocCSV;
		} else {
			return null;
		}
	}
}