@isTest
private class ISSM_ProfileSelector_tst {
	
	static testMethod void testselectById()
	{

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Set<Id> idSet = new Set<Id>();
		idSet.add(profileId.Id);

		Test.startTest();		
		List<Profile> result = ISSM_ProfileSelector_cls.newInstance().selectById(idSet);		
		Test.stopTest();
		
		system.assertEquals(1,result.size());
		
	}

	static testMethod void testselectByIdSingle()
	{

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		
		Test.startTest();		
		Profile result = ISSM_ProfileSelector_cls.newInstance().selectById(profileId.Id);		
		Test.stopTest();
		
		system.assertNotEquals(result, null);
		system.assertEquals(profileId.Id,result.Id);
		
	}

	static testMethod void testselectByIdString()
	{

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		
		Test.startTest();		
		List<Profile> result = ISSM_ProfileSelector_cls.newInstance().selectById(profileId.Id+'');		
		Test.stopTest();
		
		system.assertEquals(1,result.size());
		
	}
	
}