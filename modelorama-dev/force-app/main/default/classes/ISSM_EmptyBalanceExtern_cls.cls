/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to get external empties

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_EmptyBalanceExtern_cls implements ISSM_ObjectExternInterface_cls {

	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setSapCustomerId set of sap customer id to request
	 * @param  Boolean isDeleted if the record must be deleted
	 */
	public static List<salesforce_ontap_emptybalance_c__x> getList(Set<String> setSapCustomerId, Boolean isDeleted){
		if(!isDeleted){
			if(test.isRunningTest()){
				List<salesforce_ontap_emptybalance_c__x> empties_lst = new List<salesforce_ontap_emptybalance_c__x>();
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003117',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003118',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003119',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=false, ontap_sapcustomerid_c__c='1029120920192'));
				return empties_lst;
			} else {
				return [SELECT Id, ExternalId, account_ontap_sapcustomerid_c__c, createddate__c, 
				id__c, isdeleted__c, issm_salesoffid_c__c, issm_sap_number_c__c, 
				issm_sapordernumber_c__c, name__c, ontap_balance_c__c, ontap_account_c__c, 
				ontap_duedate_c__c, ontap_emptybalancekey_c__c, ontap_product_c__c, 
				ontap_product_c_ontap_productid_c__c, ontap_productid_c__c, systemmodstamp__c, 
				ontap_sapcustomerid_c__c FROM salesforce_ontap_emptybalance_c__x 
				WHERE ontap_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted and systemmodstamp__c >= YESTERDAY];
			}
		} else {
			if(test.isRunningTest()){
				List<salesforce_ontap_emptybalance_c__x> empties_lst = new List<salesforce_ontap_emptybalance_c__x>();
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003117',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003118',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003119',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				empties_lst.add(new salesforce_ontap_emptybalance_c__x(ontap_emptybalancekey_c__c = '0003003120',isdeleted__c=true, ontap_sapcustomerid_c__c='1029120920192'));
				return empties_lst;
			} else {
				return [SELECT Id, ExternalId, account_ontap_sapcustomerid_c__c, createddate__c, 
				id__c, isdeleted__c, issm_salesoffid_c__c, issm_sap_number_c__c, 
				issm_sapordernumber_c__c, name__c, ontap_balance_c__c, ontap_account_c__c, 
				ontap_duedate_c__c, ontap_emptybalancekey_c__c, ontap_product_c__c, 
				ontap_product_c_ontap_productid_c__c, ontap_productid_c__c, 
				ontap_sapcustomerid_c__c FROM salesforce_ontap_emptybalance_c__x 
				WHERE ontap_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted];
			}
		}
	}

	/**
	 * Get a set of external id for the object		                             
	 * @param  List<sObject> sObjectList list of objects to return the set
	 */
	public static Set<String> getSetExternalId(List<sObject> sObjectList){
		Set<String> emptiesSet = new Set<String>();
		salesforce_ontap_emptybalance_c__x accountEmpties; 
		for(Integer i = 0; i < sObjectList.size(); i++){
				accountEmpties = (salesforce_ontap_emptybalance_c__x)sObjectList.get(i);
		        emptiesSet.add(accountEmpties.ontap_emptybalancekey_c__c);
		}
		return emptiesSet;
	}

}