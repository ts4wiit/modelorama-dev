/**
 * @Created by: Alfonso de la Cuadra Martinez (Everis)
 * @Description: Test class for V360 controller. This methods insert all the necessary information to execute Test Methods. 		
 */

@isTest(seeAllData=false)
public class MDRM_ModeloramaFullViewCtrlTest {
    
    @testSetup static void test() {
        UserRole urDrv = new UserRole(Name='DRV Modeloramas Test');
        insert urDrv;
        UserRole urSup = new UserRole(Name='Supervisor Modeloramas Test', ParentRoleId=urDrv.Id);
        insert urSup;
        UserRole urBdr = new UserRole(Name='BDR Modeloramas Test', ParentRoleId=urSup.Id);
        insert urBdr;
        
        User uDrv = new User(
             ProfileId = [SELECT Id, Name, UserType FROM Profile WHERE PermissionsViewAllData = true LIMIT 1].Id,
             LastName = 'last',
             Email = urDrv.Id + '@modeloramas.mx.com',
             Username = urDrv.Id + '@modeloramas.mx.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = urDrv.Id
        );
        insert uDrv;
        
        User uBdr = new User(
            ProfileId = [SELECT Id, Name, UserType FROM Profile WHERE Name = 'System Administrator' OR Name = 'Administrador del sistema' LIMIT 1].Id,
            LastName = 'last',
            Email = urBdr.Id + '@modeloramas.mx.com',
            Username = urBdr.Id + '@modeloramas.mx.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = urBdr.Id,
            MDRM_Corporative_Approver__c = uDrv.Id,
            MDRM_Legal_Approver__c = uDrv.Id,
            MDRM_LiderGerenteApprover__c = uDrv.Id,
            MDRM_Procurement_Approver__c = uDrv.Id,
            MDRM_UEN_Approver__c = uDrv.Id,
            MDRM_SupervisorApprover__c = uDrv.Id,
            EmployeeNumber = '010101010101'
        );
        Insert uBdr;
        
        MDRM_ModeloramaHelperTest.createFullViewInfo(uBdr.Id);
    }
    
    @isTest static void testPicklistLabels() {
        MDRM_ModeloramaFullViewCtrl.fetchPicklistLabels('MDRM_Sale_Summary__c', 'MDRM_Month__c');
    }
    
    @isTest static void testGetV360Permission() {
        MDRM_ModeloramaFullViewCtrl.getEditPermission();
    }
    
    @isTest static void testGetCommercialInfo() {
        Account acc = [SELECT Name FROM Account WHERE Name = 'Modelorama'];
        
        MDRM_ModeloramaFullViewCtrl.getSummary(acc.Id, 'ASC');
    }
    
    @isTest static void testGetBusinessman() {
        Account acc = [SELECT Name FROM Account WHERE Name = 'Modelorama'];
        
        MDRM_ModeloramaFullViewCtrl.getAccount(acc.Id);
        MDRM_ModeloramaFullViewCtrl.getUser(acc.Id);
    }
    
    @isTest static void testGetDrv() {
        UserRole ur = [SELECT Id, Name FROM UserRole WHERE Name = 'BDR Modeloramas Test' LIMIT 1];
        
        MDRM_ModeloramaFullViewCtrl.getDRV(ur.Id);
    }
    
    @isTest static void testGetBusinessmanRelated() {
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Modelorama'];
        
        MDRM_ModeloramaFullViewCtrl.getBusinessman(acc.Id);
    }

    @isTest static void testGetDocuments() {
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Modelorama'];
        
        MDRM_ModeloramaFullViewCtrl.fetchDocuments(acc.Id, new List<String>{'MDRM_License'});
    }
    
    @isTest static void testBusinessmanSurveys() {
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Modelorama'];
        
        MDRM_ModeloramaFullViewCtrl.getBusinessmanSurveys('', acc.Id);
    }
}