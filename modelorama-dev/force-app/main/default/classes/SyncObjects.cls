/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class with Class Objects to be Sichronized with Heroku, and methods to callout heroku web services 

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       16-08-2017        Andrés Garrido               Creation Class
****************************************************************************************************/
public class SyncObjects {
	/*
		Class with list of tour objects to sinchronize with heroku
	*/
	public class SyncTourObject{
		//List of tours
		public list<TourObject> tours;
		//Constructor method
		public SyncTourObject(){
			tours = new list<TourObject>();
		}
	}
	
	/*
		Class with list of tour objects to sinchronize with heroku
	*/
	public class UpdateTourObject{
		//List of tours
		public list<ObjTourUpdate> tours;
		//Constructor method
		public UpdateTourObject(){
			tours = new list<ObjTourUpdate>();
		}
	}
	
	/*
		Class with Date and list of tours to obtains related events
	*/
	public class GetTourObject{
		//DateTime to 
		public DateTime systemmodstamp;
    	//List of tours
    	public set<String> tours;
    	//Constructor method
	    public GetTourObject(){
			tours = new set<String>();
		}
	}
	
	/*
		Class with list of events that will be delete
	*/
	public class DeleteTourObject{
		public list<ObjTourDelete> tours;
		
		public DeleteTourObject(){
			tours = new list<ObjTourDelete>();
		}
	}
	
	/*
		Class with list of events objects to sinchronize with heroku
	*/
	public class SyncEventObject{
    	//List of events
    	public list<EventObject> events;
    	//Constructor method
	    public SyncEventObject(){
			events = new list<EventObject>();
		}
	}
	
	/*
		Class with list of tour objects to sinchronize with heroku
	*/
	public class UpdateEventObject{
		//List of tours
		public list<ObjEventUpdate> events;
		//Constructor method
		public UpdateEventObject(){
			events = new list<ObjEventUpdate>();
		}
	}
	
	/*
		Class with Date and list of tours to obtains related events
	*/
	public class GetEventObject{
		//DateTime to 
		public DateTime systemmodstamp;
    	//List of events
    	public set<String> tours;
    	//Constructor method
	    public GetEventObject(){
			tours = new set<String>();
		}
	}
	
	/*
		Class with list of events that will be delete
	*/
	public class DeleteEventObject{
		public list<ObjEventDelete> events;
		
		public DeleteEventObject(){
			events = new list<ObjEventDelete>();
		}
	}
	
	/*
		Class with Tour Object atributes
	*/	
    public class TourObject {
		public String	tourid;
		public DateTime	actualend;
		public DateTime actualstart;
		public Decimal	distance;
		public Decimal 	endmile;
		public String	endtime;
		public Date		estimateddeliverydate;
		public Decimal	exceptiontime;
		public Boolean	isactive;
		public DateTime lastmodifydate;
		public Decimal	percentageoffroute;
		public String	rfc;
		public String	routedescription;
		public String 	routeid;
		public String	salesagent;
		public String	salesforceid;
		public String 	salesofficename;
		public String	salesofficeid;
		public String	salesorgname;
		public String	salesorgid;
		public String	shippingcity;
		public String 	shippingstreet;
		public Decimal	startmile;
		public String	starttime;
		public DateTime	systemmodstamp;
		public String	timedifference;
		public String	tourstatus;
		public String	toursubstatus;
		public Date		tourdate;
		public String	vehicleid;
		public String	visitplantype;
	}
	
	public class ObjTourUpdate{
		public String	salesforceid;
		public String	tourstatus;
		public String	toursubstatus;
		public Boolean	isactive;
	}
	
	public class ObjEventUpdate{
		public String	salesforceid;
		public String	id;
		public Integer 	sequence;
	}
	
	/*
		Class with Event Object atributes
	*/
	public class EventObject{
    	public DateTime	controlfin;
    	public DateTime	controlinicio;
    	public String 	customerid;
    	public DateTime	endtime;
    	public String	id;
    	public Boolean	isoffroute;
    	public String	latitude;
    	public String	longitude;
    	public Decimal	numbercartons;
    	public String	orderdetail;
    	public String	razonnoventa;
    	public String	razonnovisita;
    	public Decimal	realsequence;
    	public String	salesagent;
    	public String	salesforceid;
    	public Decimal	sequence;
    	public DateTime	starttime;
    	public String	tourid;
    	public String	visitid;
    	public String	visitstatus;
    	public String	visitsubestatus;
    }
    
    public class ObjEventDelete{
    	public String salesforceid;
    }
    
    public class ObjTourDelete{
    	public String salesforceid;
    }
	
	/**
    * Method that callout the web service in heroku to insert the tours
    * @params:
      1.-objSync: Object with all tours to insert in Heroku
    * @return void
    **/
	public static String calloutInsertHerokuTours(SyncTourObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('InsertHerokuTours');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonTour = Json.serialize(objSync);
		System.debug('\nJson Tours Insert: '+strJsonTour);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonTour);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes: '+res.getBody());
		
		return res.getStatusCode()+'';
    }
    
    /**
    * Method that callout the web service in heroku to update the tours
    * @params:
      1.-objSync: Object with all tours to update in Heroku
    * @return void
    **/
	public static String calloutUpdateHerokuTours(UpdateTourObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('UpdateHerokuTours');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonTour = Json.serialize(objSync);
		System.debug('\nJson Tours Update: '+strJsonTour);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonTour);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes: '+res.getBody());
		
		return res.getStatusCode()+'';
    }
    
    /**
    * Method that callout the web service in heroku to get the tours in SFDC
    * @params:
      1.-objSync: Object with all tours to get in Heroku
    * @return void
    **/
/*    public static SyncTourObject calloutGetHerokuTours(GetTourObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('GetHerokuTours');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson GET Tours: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes GET Tours: '+res.getBody());
		
		if(res.getStatusCode() == 200){
			SyncTourObject objResp = (SyncTourObject)Json.deserialize(res.getBody(), SyncObjects.SyncTourObject.class);
			return objResp;
		}
		else
			return null;
    }
*/    
    /**
    * Method that callout the web service in heroku to delete the tours in SFDC
    * @params:
      1.-objSync: Object with all tours to delete in Heroku
    * @return void
    **/
    public static string calloutDeleteHerokuTours(DeleteTourObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('DeleteHerokuTours');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson Delete Tours: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes Delete Tours: '+res.getBody());
		
		return res.getStatusCode()+'';
    }
    
    /**
    * Method that callout the web service in heroku to insert the events in SFDC
    * @params:
      1.-objSync: Object with all events to insert in Heroku
    * @return void
    **/
    public static string calloutInsertHerokuEvents(SyncEventObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('InsertHerokuEvents');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson Events: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes: '+res.getBody());
		
		return res.getStatusCode()+'';
    }
    
    /**
    * Method that callout the web service in heroku to update the events
    * @params:
      1.-objSync: Object with all events to update in Heroku
    * @return void
    **/
    public static string calloutUpdateHerokuEvents(UpdateEventObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('UpdateHerokuEvents');
    	System.debug('ObjConfig==>'+objConf);
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson Update Events: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes: '+res.getBody());
		
		return res.getStatusCode()+'';	
    }
    
    /**
    * Method that callout the web service in heroku to insert the events in SFDC
    * @params:
      1.-objSync: Object with all events to insert in Heroku
    * @return void
    **/
    public static SyncEventObject calloutGetHerokuEvents(GetEventObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('GetHerokuEvents');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson GET Events: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes GET Events: '+res.getBody());
		
		if(res.getStatusCode() == 200){
			SyncEventObject objResp = (SyncEventObject)Json.deserialize(res.getBody(), SyncObjects.SyncEventObject.class);
			return objResp;
		}
		else
			return null;
    }
    
    /**
    * Method that callout the web service in heroku to delete the events in SFDC
    * @params:
      1.-objSync: Object with all events to delete in Heroku
    * @return void
    **/
    public static string calloutDeleteHerokuEvents(DeleteEventObject objSync){
    	ISSM_PriceEngineConfigWS__c objConf = ISSM_PriceEngineConfigWS__c.getAll().get('DeleteHerokuEvents');
    	String authorizationHeader = objConf.ISSM_AccessToken__c;
		String strJsonEvents = Json.serialize(objSync);
		System.debug('\nJson Delete Events: '+strJsonEvents);
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(objConf.ISSM_EndPoint__c);
		req.setMethod(objConf.ISSM_Method__c);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', objConf.ISSM_HeaderContentType__c);
		req.setBody(strJsonEvents);
		
		HttpResponse res = h.send(req);
		System.debug('\nRes: '+res.getBody());
		
		return res.getStatusCode()+'';
    }
}