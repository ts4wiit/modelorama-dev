/*********************************************************************************************************************************
    General Information
    -------------------
    author:     Cindy Fuentes
    email:      c.fuentes.tapia@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Class for site control

    Information about changes (versions)
    ==============================================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------------------------------------
    1.0       05/05/2018      Cindy Fuentes		           Batch Class created to Mass Update accounts for Mass Reject tool
    ==============================================================================================================================
*********************************************************************************************************************************/
global class MDRM_MassReject_btch implements Database.Batchable<sObject>, database.stateful{
private String query;
string accstate;
double lowlimit;
double highlimit;
integer accnumber;
public integer count = 0;
    
    public MDRM_MassReject_btch(string accs, double lowlimits, double highlimits, integer accnumbers){
        //Method to search for the accounts to update.
        accstate = accs;
        lowlimit = lowlimits;
        highlimit = highlimits;
        accnumber = accnumbers;
        query = 'select name,id, ONTAP__Account_Status__c, MDRM_Substatus__c from account where ONTAP__Province__c = \''+accstate+'\' AND MDRM_of_Attachment__c >=: lowlimit AND MDRM_of_Attachment__c <=: highlimit AND ONTAP__Account_Status__c != \'Rejected Prospect\' AND MDRM_Substatus__c !=\'In % of Attachment\' AND MDRM_Stage__c = \'Prospect\'';
    }

global Database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    //Method to update the accounts with a batch. 
    List<Account> accns = new List<Account>();
   for(sObject s : scope){
       Account a = (Account)s;
			a.ONTAP__Account_Status__c='Rejected Prospect';
            a.MDRM_Substatus__c='In % of Attachment';
	
            accns.add(a);
            
        }

update accns;

}
global void finish(Database.BatchableContext BC){
    // Method to run the final actions for the batch.
    string idusuario = system.UserInfo.getUserId();
        	user usuario = [select id, MDRM_emails_sent__c, name, MDRM_UpdateDate__c from user where id=:idusuario limit 1];
    if(usuario.MDRM_UpdateDate__c == date.today()){
        usuario.MDRM_emails_sent__c =usuario.MDRM_emails_sent__c + accnumber;
        update usuario;
    }else{
        usuario.MDRM_UpdateDate__c = date.today();
        usuario.MDRM_emails_sent__c = accnumber;
        update usuario;
    }

}
}