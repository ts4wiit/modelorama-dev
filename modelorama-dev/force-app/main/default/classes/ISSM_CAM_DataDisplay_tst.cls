/********************************************************************************** 
    Company:            Avanxo México
    Author:             Oscar Alvarez Garcia
    Customer:           AbInbev - CAM
    Descripción:        Class test for class controller - ISSM_CAM_DataDisplay_ctr

    Information about changes (versions)
===================================================================================
    No.    Date                 Author                      Description
    1.0    06-Nobiembre-2018    Oscar Alvarez Garcia           Creation
**********************************************************************************/
@isTest
private class ISSM_CAM_DataDisplay_tst {

	@testSetup static void loadData() {

		Account acc 					=  new Account();
            acc.Name 					= 'ACCTEST1';
            acc.ontap__sapcustomerid__c	= 'TEST';
            acc.ontap__salesoffid__c	= 'OFF';
            acc.ontap__salesogid__c		= 'ORG';
            acc.ONTAP__SAP_Number__c	= '0100ACTEST';
            acc.ISSM_Is_Blacklisted__C	= False;
            acc.ONTAP__Email__c			= 'test@est.com';
            acc.ONTAP__SAP_Number__c    = '1234567890';
	        acc.ONTAP__LegalName__c     = 'Razon social del cliente';
	        acc.ONTAP__Street__c		= 'Didirección del cliente';
	        acc.ONTAP__Street_Number__c	= '234';
	        acc.ONCALL__Postal_Code__c	= '01250';
		    acc.ONTAP__Province__c		= 'Crovince test';
	        acc.ONTAP__SalesOgId__c		= '3116';
	        acc.ONTAP__Colony__c		= 'Colony test';
	        acc.ONTAP__Municipality__c	= 'Municipality test';
	        acc.ISSM_RFC__c				= 'XAXX010101000';
        insert acc; 

		ISSM_Asset__c asset 				= new ISSM_Asset__c();
			asset.name						= 'Unassignation';
			asset.Equipment_Number__c		= '0000UNASSIGNATION_';
			asset.ISSM_Serial_Number__c		= 'UNASSIGNATION_';
			asset.ISSM_Material_number__c	= '8000300';
			asset.ISSM_Status_SFDC__c		= 'Free Use';
			asset.ISSM_CutOverReason__c		= 'Lost Unit';
        insert asset;

		ONTAP__Case_Force__c caseForce = new  ONTAP__Case_Force__c();
			caseForce.ONTAP__Subject__c				='Retiro';
			caseForce.ISSM_Asset_CAM__c 			= asset.id;
			caseForce.ONTAP__Account__c 			= acc.id;
			caseForce.ISSM_TypificationLevel1__c 	= 'Refrigeración';
			caseForce.ISSM_ClassificationLevel2__c 	= 'Retiro de equipo';
			caseForce.ISSM_ApprovalStatus__c		= 'Approved';
        	caseForce.ISSM_SAPOrderDate__c			= date.today().toStartOfWeek()+7;
			caseForce.ISSM_Printed_times__c			= 1;
        insert caseForce;
	}
	
	private static testMethod void downloadPDFTEST() {
		PageReference pageRef = Page.ISSM_CAM_PDFComodato_pag;
		Test.setCurrentPage(pageRef);
		Test.startTest();
			ISSM_CAM_DataDisplay_ctr dataDisplay = new ISSM_CAM_DataDisplay_ctr();
            	dataDisplay.idCaseForce = [SELECT Id FROM ONTAP__Case_Force__c LIMIT 1].Id;
				dataDisplay.downloadPDF();
		Test.stopTest();
	}
}