/**************************************************************************************
Nombre de la Clase Appex: ISSM_AdminCuentasATM_sch
Versión : 1.0
Fecha de Creación : 11 Abril 2018
Funcionalidad : Clase scheduled para ejecutar el batch ISSM_AdminCuentasATM_bch
Clase de Prueba: ISSM_AdminCuentasATM_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        11 - Abril - 2018      Versión Original
*************************************************************************************/
global with sharing class ISSM_AdminCuentasATM_sch implements Schedulable {
    global void execute(SchedulableContext sc) {
        database.executebatch(new ISSM_AdminCuentasATM_bch(), 200);
    }
}