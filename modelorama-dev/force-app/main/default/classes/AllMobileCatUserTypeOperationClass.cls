/**
 * This class contains the operations to synchronize the AllMobileCatUserType records with Heroku.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileCatUserTypeOperationClass implements Queueable {

    //Variables.
    String strEventTriggerFlag;
    List<AllMobileCatUserType__c> lstAllMobileCatUserType = new List<AllMobileCatUserType__c>();

    //Constructor.
    public AllMobileCatUserTypeOperationClass(List<AllMobileCatUserType__c> lstAllMobileCatUserTypeTrigger, String strEventTriggerFlagTrigger) {
        lstAllMobileCatUserType = lstAllMobileCatUserTypeTrigger;
        strEventTriggerFlag = strEventTriggerFlagTrigger;
    }

    /**
     *This method initiate a List of CatUserType objects in Salesforce into Heroku cat_user_type table using POST method (insert).
     *
     * @param lstAllMobileCatUserType   List<AllMobileCatUserType__c>
     */
    public static void syncInsertAllMobileCatUserTypeToExternalObject(List<AllMobileCatUserType__c> lstAllMobileCatUserType) {
        Set<Id> setIdAllMobileCatUserType = new Set<Id>();
        if(!lstAllMobileCatUserType.isEmpty()) {
            setIdAllMobileCatUserType = getSetIdAllMobileCatUserTypeSFFromListAllMobileCatUserTypeSF(lstAllMobileCatUserType);
            AllMobileCatUserTypeOperationClass.insertExternalAllMobileCatUserTypeWS(setIdAllMobileCatUserType);
        }
    }

    /**
     * This method prepares a List of CatUserType objects in Salesforce to communicate asynchronously and insert into Heroku cat_user_type table.
     *
     * @param setIdAllMobileCatUserType Set<Id>
     */
    @future(callout = true)
    public static void insertExternalAllMobileCatUserTypeWS(Set<Id> setIdAllMobileCatUserType) {
        AllMobileCatUserTypeOperationClass.insertExternalAllMobileCatUserType(setIdAllMobileCatUserType);
    }

    /**
     * This method sync a List of CatUserType objects from  Salesforce into a Heroku cat_user_type table using POST method (insert).
     *
     * @param setIdAllMobileCatUserType Set<Id>
     */
    public static Boolean insertExternalAllMobileCatUserType(Set<Id> setIdAllMobileCatUserType) {

        //Variables.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToInsertInHeroku = new List<AllMobileCatUserType__c>();
        AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass();

        //Get List of CatUserType in SF.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeSF = [SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileCatUserType__c WHERE Id =: setIdAllMobileCatUserType];

        //Get List of All CatUserType in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileAllCatUserTypeHerokuObjectClass = getAllMobileAllCatUserTypeHeroku();

        //Compare both lists to avoid insert CatUserType SF that already exist in HK. Generates new List to be inserted in Heroku.
        for(AllMobileCatUserType__c objAllMobileCatUserTypeSF: lstAllMobileCatUserTypeSF) {
            Integer intContainedCatUserTypeSFInCatUserTypeHeroku = 0;
            for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClassIterate : lstAllMobileAllCatUserTypeHerokuObjectClass) {
                if((objAllMobileCatUserTypeSF.Name == (objAllMobileCatUserTypeHerokuObjectClassIterate.user_type).trim()) || (objAllMobileCatUserTypeSF.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c == (objAllMobileCatUserTypeHerokuObjectClassIterate.permisos).trim())) {
                    intContainedCatUserTypeSFInCatUserTypeHeroku++;
                }
            }
            if(intContainedCatUserTypeSFInCatUserTypeHeroku == 0) {
                lstAllMobileCatUserTypeToInsertInHeroku.add(objAllMobileCatUserTypeSF);
            }
        }

        //Insert the Final List of CatUserType (SF) into Heroku.
        if(!lstAllMobileCatUserTypeToInsertInHeroku.isEmpty()) {
            for(AllMobileCatUserType__c objAllMobileCatUserType: lstAllMobileCatUserTypeToInsertInHeroku) {
                objAllMobileCatUserTypeHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass();
                objAllMobileCatUserTypeHerokuObjectClass.user_type = objAllMobileCatUserType.Name;
                objAllMobileCatUserTypeHerokuObjectClass.user_type_desc = objAllMobileCatUserType.AllMobileDeviceUserUserTypeDescription__c;
                objAllMobileCatUserTypeHerokuObjectClass.admon_externa = objAllMobileCatUserType.AllMobileDeviceUserExternalManagement__c;
                objAllMobileCatUserTypeHerokuObjectClass.permisos = objAllMobileCatUserType.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c;
                objAllMobileCatUserTypeHerokuObjectClass.softdeleteflag = objAllMobileCatUserType.AllMobileDeviceUserSoftDeleteFlag__c;
                objAllMobileCatUserTypeHerokuObjectClass.dtlastmodifieddate = objAllMobileCatUserType.LastModifiedDate;

                //Rest callout to insert CatUserType in Heroku.
                String strStatusCodeInsert = AllMobileSyncObjectsClass.calloutInsertCatUserTypeIntoHeroku(objAllMobileCatUserTypeHerokuObjectClass);
            }
        }
        return false;
    }

    /**
     * This method initiate a List of CatUserType objects in Salesforce into Heroku cat_user_type table using PUT method (update).
     *
     * @param lstAllMobileCatUserType   List<AllMobileCatUserType__c>
     */
    public static void syncUpdateAllMobileCatUserTypeToExternalObject(List<AllMobileCatUserType__c> lstAllMobileCatUserType) {
        Set<Id> setIdAllMobileCatUserType = new Set<Id>();
        if(!lstAllMobileCatUserType.isEmpty()) {
            setIdAllMobileCatUserType = getSetIdAllMobileCatUserTypeSFFromListAllMobileCatUserTypeSF(lstAllMobileCatUserType);
            AllMobileCatUserTypeOperationClass.updateExternalAllMobileCatUserTypeWS(setIdAllMobileCatUserType);
        }
    }

    /**
     * This method prepares a List of CatUserType objects in Salesforce to communicate asynchronously and update into Heroku cat_user_type table.
     *
     * @param setIdAllMobileCatUserType Set<Id>
     */
    @future(callout = true)
    public static void updateExternalAllMobileCatUserTypeWS(Set<Id> setIdAllMobileCatUserType) {
        AllMobileCatUserTypeOperationClass.updateExternalAllMobileCatUserType(setIdAllMobileCatUserType);
    }

    /**
     * This method sync a List of CatUserType objects from  Salesforce into a Heroku cat_user_type table using PUT method (update).
     *
     * @param setIdAllMobileCatUserType Set<Id>
     */
    public static Boolean updateExternalAllMobileCatUserType(Set<Id> setIdAllMobileCatUserType) {

        //Variables.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToUpdateInHeroku = new List<AllMobileCatUserType__c>();
        AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass();

        //Get List of CatUserType in SF.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeSF = [SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileCatUserType__c WHERE Id =: setIdAllMobileCatUserType];

        //Get List of All CatUserType in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileAllCatUserTypeHerokuObjectClass = getAllMobileAllCatUserTypeHeroku();

        //Compare both lists to avoid update CatUserType SF that do not exist in HK. Generates new List to be updated in Heroku.
        for(AllMobileCatUserType__c objAllMobileCatUserTypeSF : lstAllMobileCatUserTypeSF) {
            Integer intContainedCatUserTypeSFInCatUserTypeHeroku = 0;
            for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClassIterate : lstAllMobileAllCatUserTypeHerokuObjectClass) {
                if((objAllMobileCatUserTypeSF.Name == (objAllMobileCatUserTypeHerokuObjectClassIterate.user_type).trim()) || (objAllMobileCatUserTypeSF.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c == (objAllMobileCatUserTypeHerokuObjectClassIterate.permisos).trim())) {
                    intContainedCatUserTypeSFInCatUserTypeHeroku++;
                }
            }
            if(intContainedCatUserTypeSFInCatUserTypeHeroku > 0) {
                lstAllMobileCatUserTypeToUpdateInHeroku.add(objAllMobileCatUserTypeSF);
            }
        }

        //Update the Final List of CatUserType (SF) into Heroku.
        if(!lstAllMobileCatUserTypeToUpdateInHeroku.isEmpty()) {
            for(AllMobileCatUserType__c objAllMobileCatUserType: lstAllMobileCatUserTypeToUpdateInHeroku) {
                objAllMobileCatUserTypeHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass();
                objAllMobileCatUserTypeHerokuObjectClass.user_type = objAllMobileCatUserType.Name;
                objAllMobileCatUserTypeHerokuObjectClass.user_type_desc = objAllMobileCatUserType.AllMobileDeviceUserUserTypeDescription__c;
                objAllMobileCatUserTypeHerokuObjectClass.admon_externa = objAllMobileCatUserType.AllMobileDeviceUserExternalManagement__c;
                objAllMobileCatUserTypeHerokuObjectClass.permisos = objAllMobileCatUserType.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c;
                objAllMobileCatUserTypeHerokuObjectClass.softdeleteflag = objAllMobileCatUserType.AllMobileDeviceUserSoftDeleteFlag__c;
                objAllMobileCatUserTypeHerokuObjectClass.dtlastmodifieddate = objAllMobileCatUserType.LastModifiedDate;

                //Rest callout to update CatUserType in Heroku.
                String strStatusCode = AllMobileSyncObjectsClass.calloutUpdateCatUserTypeIntoHeroku(objAllMobileCatUserTypeHerokuObjectClass);
            }
        }
        return false;
    }

    /**
     * This method delete a List of CatUserType objects from  Salesforce according to a Heroku device_user table using DELETE method.
     *
     * @param lstAllMobileCatUserTypeInSF   List<AllMobileCatUserType__c>
     */
    public static void syncDeleteAllMobileCatUserTypeInSF(List<AllMobileCatUserType__c> lstAllMobileCatUserTypeInSF) {

        //Inner variables.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToBeDeletedInSF = new List<AllMobileCatUserType__c>();
        List<String> lstUserTypeAndPermisosFromCatUserTypeHeroku = new List<String>();

        //Get List of All CatUserType in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileAllCatUserTypeHerokuObjectClass = getAllMobileAllCatUserTypeHeroku();

        //Get List of Id & UserName of CatUserType Heroku to compare.
        for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass : lstAllMobileAllCatUserTypeHerokuObjectClass) {
            lstUserTypeAndPermisosFromCatUserTypeHeroku.add((objAllMobileCatUserTypeHerokuObjectClass.user_type).trim() + (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim());
        }

        //Identify CatUserType SF to be Deleted.
        for(AllMobileCatUserType__c objAllMobileCatUserTypeInSF : lstAllMobileCatUserTypeInSF) {
            if(!lstUserTypeAndPermisosFromCatUserTypeHeroku.contains(objAllMobileCatUserTypeInSF.Name + objAllMobileCatUserTypeInSF.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c)) {
                lstAllMobileCatUserTypeToBeDeletedInSF.add(objAllMobileCatUserTypeInSF);
            }
        }
        if(!lstAllMobileCatUserTypeToBeDeletedInSF.isEmpty() && lstAllMobileCatUserTypeToBeDeletedInSF != NULL) {
            delete lstAllMobileCatUserTypeToBeDeletedInSF;
        }
    }

    /**
     * This method insert and update a List of CatUserType objects from  Salesforce according to a Heroku cat_user_type table using POST and PUT methods.
     *
     * @param lstAllMobileCatUserTypeInSF   List<AllMobileCatUserType__c>
     */
    public static void syncInsertAndUpdateAllMobileCatUserTypeInSFFromCatUserTypeHeroku(List<AllMobileCatUserType__c> lstAllMobileCatUserTypeInSF) {

        //Inner variables.
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToBeInsertedInSF = new List<AllMobileCatUserType__c>();
        AllMobileCatUserType__c objAllMobileCatUserTypeToBeInsertedInSF = new AllMobileCatUserType__c();
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileCatUserTypeHerokuObjectClassToBeInsertedInSF = new List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass>();
        List<String> lstCatUserTypeUserTypeAndPermisosFromAllMobileCatUserTypeSF = new List<String>();

        //Get List of All CatUserType in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileAllCatUserTypeHerokuObjectClass = getAllMobileAllCatUserTypeHeroku();

        //Get List of CatUserType Name & AllMobileDeviceUserPermissionsLK__c from AllMobileCatUserType SF.
        for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileCatUserTypeInSF) {
            lstCatUserTypeUserTypeAndPermisosFromAllMobileCatUserTypeSF.add(objAllMobileCatUserType.Name/* + objAllMobileCatUserType.AllMobileDeviceUserPermissionsLK__r.Name */);
        }

        //Identify CatUserTypes From Heroku to be Inserted in SF.
        for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass : lstAllMobileAllCatUserTypeHerokuObjectClass) {
            if(!lstCatUserTypeUserTypeAndPermisosFromAllMobileCatUserTypeSF.contains((objAllMobileCatUserTypeHerokuObjectClass.user_type).trim() /* + (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim() */)) {
                lstAllMobileCatUserTypeHerokuObjectClassToBeInsertedInSF.add(objAllMobileCatUserTypeHerokuObjectClass);
            }
        }

        //Validate if the Permission binary code exists in SF. If it doesn´t exist, it will be created.

        //Get All AllMobilePermissionCategoryUserTyper in SF.
        List<AllMobilePermissionCategoryUserType__c> lstAllMobileAllPermissionCategoryUserTyper = new List<AllMobilePermissionCategoryUserType__c>();
        lstAllMobileAllPermissionCategoryUserTyper = [SELECT Id, Name, AllMobileDeviceUserPermissionDescription__c, AllMobileDeviceUserPermissionName__c, AllMobileDeviceUserBinaryPermission__c FROM AllMobilePermissionCategoryUserType__c];
        List<AllMobilePermissionCategoryUserType__c> lstAllMobilePermissionCategoryUserTypeToBeInsertedInSF = new List<AllMobilePermissionCategoryUserType__c>();
        AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTyperToBeInsertedInSF = new AllMobilePermissionCategoryUserType__c();
        List<String> lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF = new List<String>();

        if(!lstAllMobileAllPermissionCategoryUserTyper.isEmpty()) {
            for(AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeInSF : lstAllMobileAllPermissionCategoryUserTyper) {
                lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF.add(objAllMobilePermissionCategoryUserTypeInSF.AllMobileDeviceUserBinaryPermission__c);
            }
        }

        //Validate if the permission binary code exist in AllMobileAllPermissionCatergoryUserType.
        if(!lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF.isEmpty() && !lstAllMobileCatUserTypeHerokuObjectClassToBeInsertedInSF.isEmpty()) {
            for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass : lstAllMobileCatUserTypeHerokuObjectClassToBeInsertedInSF) {
                if(!lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF.contains((objAllMobileCatUserTypeHerokuObjectClass.permisos).trim())) {
                    objAllMobilePermissionCategoryUserTyperToBeInsertedInSF = new AllMobilePermissionCategoryUserType__c();
                    objAllMobilePermissionCategoryUserTyperToBeInsertedInSF.Name = (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim();
                    objAllMobilePermissionCategoryUserTyperToBeInsertedInSF.AllMobileDeviceUserBinaryPermission__c = (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim();
                    objAllMobilePermissionCategoryUserTyperToBeInsertedInSF.AllMobileDeviceUserPermissionName__c = (objAllMobileCatUserTypeHerokuObjectClass.user_type).trim();
                    lstAllMobilePermissionCategoryUserTypeToBeInsertedInSF.add(objAllMobilePermissionCategoryUserTyperToBeInsertedInSF);
                }
            }
        }

        if(!lstAllMobilePermissionCategoryUserTypeToBeInsertedInSF.isEmpty()) {
            insert lstAllMobilePermissionCategoryUserTypeToBeInsertedInSF;
        }

        List<AllMobilePermissionCategoryUserType__c> lstAllMobileAllPermissionCategoryUserTyperAfterInsertNewPermissionsFromHeroku = new List<AllMobilePermissionCategoryUserType__c>();
        lstAllMobileAllPermissionCategoryUserTyperAfterInsertNewPermissionsFromHeroku = [SELECT Id, Name, AllMobileDeviceUserPermissionDescription__c, AllMobileDeviceUserPermissionName__c, AllMobileDeviceUserBinaryPermission__c FROM AllMobilePermissionCategoryUserType__c];

        //Convert CatUserType From Heroku to AllMobileCatUserType in SF.
        for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass : lstAllMobileCatUserTypeHerokuObjectClassToBeInsertedInSF) {
            for(AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTyper : lstAllMobileAllPermissionCategoryUserTyperAfterInsertNewPermissionsFromHeroku) {
                if(objAllMobilePermissionCategoryUserTyper.AllMobileDeviceUserBinaryPermission__c == (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim()) {
                    objAllMobileCatUserTypeToBeInsertedInSF = new AllMobileCatUserType__c();
                    objAllMobileCatUserTypeToBeInsertedInSF.AllMobileDeviceUserPermissionsLK__c = objAllMobilePermissionCategoryUserTyper.Id;
                    objAllMobileCatUserTypeToBeInsertedInSF.Name = objAllMobilePermissionCategoryUserTyper.AllMobileDeviceUserPermissionName__c;
                    objAllMobileCatUserTypeToBeInsertedInSF.AllMobileDeviceUserUserTypeDescription__c = objAllMobileCatUserTypeHerokuObjectClass.user_type_desc;
                    objAllMobileCatUserTypeToBeInsertedInSF.AllMobileDeviceUserExternalManagement__c = objAllMobileCatUserTypeHerokuObjectClass.admon_externa;
                    objAllMobileCatUserTypeToBeInsertedInSF.AllMobileDeviceUserSoftDeleteFlag__c = objAllMobileCatUserTypeHerokuObjectClass.softdeleteflag;
                    lstAllMobileCatUserTypeToBeInsertedInSF.add(objAllMobileCatUserTypeToBeInsertedInSF);
                }
            }
        }
        if(!lstAllMobileCatUserTypeToBeInsertedInSF.isEmpty() && lstAllMobileCatUserTypeToBeInsertedInSF != NULL) {
            insert lstAllMobileCatUserTypeToBeInsertedInSF;
        }

        //UPDATING
        List<AllmobileCatUserType__c> lstAllMobileCatUserTypeToBeUpdatedInSF = new List<AllmobileCatUserType__c>();
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstAllMobileCatUserTypeHerokuObjectClassExistingInSF = new List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass>();
        List<AllmobileCatUserType__c> lstAllmobileCatUserTypePosibleBeUpdated = [SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c, AllMobileDeviceUserPermissionsLK__r.Name, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileCatUserType__c];
        List<AllMobilePermissionCategoryUserType__c> lstAllMobilePermissionCategoryUserTyperToBeUpdated = new List<AllMobilePermissionCategoryUserType__c>();

        AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTyperToBeUpdatedInSF = new AllMobilePermissionCategoryUserType__c();


        //Identify CatUserType From Heroku to be Updated in SF.
        for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass : lstAllMobileAllCatUserTypeHerokuObjectClass) {
            if(lstCatUserTypeUserTypeAndPermisosFromAllMobileCatUserTypeSF.contains((objAllMobileCatUserTypeHerokuObjectClass.user_type).trim()/* + (objAllMobileCatUserTypeHerokuObjectClass.permisos).trim()*/)) {
                lstAllMobileCatUserTypeHerokuObjectClassExistingInSF.add(objAllMobileCatUserTypeHerokuObjectClass);
            }
        }

        //Query the Permission CategoryUserType.
        lstAllMobileAllPermissionCategoryUserTyper = [SELECT Id, Name, AllMobileDeviceUserPermissionDescription__c, AllMobileDeviceUserPermissionName__c, AllMobileDeviceUserBinaryPermission__c FROM AllMobilePermissionCategoryUserType__c];
        lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF = new List<String>();

        if(!lstAllMobileAllPermissionCategoryUserTyper.isEmpty()) {
            for(AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeInSF : lstAllMobileAllPermissionCategoryUserTyper) {
                lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF.add(objAllMobilePermissionCategoryUserTypeInSF.AllMobileDeviceUserBinaryPermission__c);
            }
        }


        //Generate List of CatUserType to be updated.
        for(AllMobileCatUserType__c objAllMobileCatUserTypeToCompareToUpdate : lstAllmobileCatUserTypePosibleBeUpdated) {
            for(AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate : lstAllMobileCatUserTypeHerokuObjectClassExistingInSF) {

                //Validate the record to be updated. Match the records with de same Name in AllMobileCatUserType__c with user_type in Herkou.
                if(objAllMobileCatUserTypeToCompareToUpdate.Name == (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.user_type).trim()) {

                    //Detecting if there are differences in any field in AllMobileCatUserTpye in SF, excepting the AllMobileUserPermission field.
                    if((objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserUserTypeDescription__c != (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.user_type_desc).trim()) || (objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserExternalManagement__c != objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.admon_externa) || (objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserSoftDeleteFlag__c != objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.softdeleteflag)) {
                        objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserUserTypeDescription__c = (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.user_type_desc).trim();
                        objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserExternalManagement__c = objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.admon_externa;
                        objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserSoftDeleteFlag__c = objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.softdeleteflag;
                    }

                    //Detecting if there are any change in AllMobileUserPermission field.
                    if(objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c != (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.permisos).trim()) {

                        //Detecting if the new value already exists in AllMobilePermissionCategoryUserType__c, if it already exists, just assing it, else, create the new AllMobilePermissionCategoryUserType__c and assign it.
                        //Updating the new value.
                        if(!lstBinaryCodeFromAllMobilePermissionCategoryUserTypeInSF.contains((objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.permisos).trim())) {
                            for(AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTyper : lstAllMobileAllPermissionCategoryUserTyper) {
                                if(objAllMobilePermissionCategoryUserTyper.AllMobileDeviceUserPermissionName__c == (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.user_type).trim()) {
                                    objAllMobilePermissionCategoryUserTyper.AllMobileDeviceUserBinaryPermission__c = (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.permisos).trim();
                                    lstAllMobilePermissionCategoryUserTyperToBeUpdated.add(objAllMobilePermissionCategoryUserTyper);
                                }
                            }

                        //Assigning the existing value.
                        } else {
                            for(AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTyper : lstAllMobileAllPermissionCategoryUserTyper) {
                                if(objAllMobilePermissionCategoryUserTyper.AllMobileDeviceUserBinaryPermission__c == (objAllMobileCatUserTypeHerokuObjectClassToCompareToUpdateIterate.permisos).trim()) {
                                    objAllMobileCatUserTypeToCompareToUpdate.AllMobileDeviceUserPermissionsLK__c = objAllMobilePermissionCategoryUserTyper.Id;
                                }
                            }
                        }
                    }
                    lstAllMobileCatUserTypeToBeUpdatedInSF.add(objAllMobileCatUserTypeToCompareToUpdate);
                }
            }
        }
        if(!lstAllMobilePermissionCategoryUserTyperToBeUpdated.isEmpty() && lstAllMobilePermissionCategoryUserTyperToBeUpdated != NULL) {
            update lstAllMobilePermissionCategoryUserTyperToBeUpdated;
        }
        if(!lstAllMobileCatUserTypeToBeUpdatedInSF.isEmpty() && lstAllMobileCatUserTypeToBeUpdatedInSF != NULL) {
            update lstAllMobileCatUserTypeToBeUpdatedInSF;
        }
    }

    /**
     * This method initiate a remote action to insert and update a List of CatUserType objects of Salesforce according to a Heroku cat_user_type table using POST AND PUT methods.
     */
    @RemoteAction
    webservice static void syncInsertUpdateCatUserTypeFromHeroku() {
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeInSF = [SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c, AllMobileDeviceUserPermissionsLK__r.Name, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileCatUserType__c];
        syncInsertAndUpdateAllMobileCatUserTypeInSFFromCatUserTypeHeroku(lstAllMobileCatUserTypeInSF);
    }

    /**
     * This method get and set a List of CatUserType Ids of Salesforce
     *
     * @param lstAllMobileCatUserType   List<AllMobileCatUserType__c>
     * @return setIdAllMobileCatUserType
     */
    public static Set<Id> getSetIdAllMobileCatUserTypeSFFromListAllMobileCatUserTypeSF(List<AllMobileCatUserType__c> lstAllMobileCatUserType) {
        Set<Id> setIdAllMobileCatUserType = new Set<Id>();
        if(!lstAllMobileCatUserType.isEmpty()) {
            for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileCatUserType) {
                setIdAllMobileCatUserType.add(objAllMobileCatUserType.Id);
            }
        }
        return setIdAllMobileCatUserType;
    }

    /**
     * This method get a List of all CatUserType records of Heroku.
     *
     * @return List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass>
     */
    public static List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> getAllMobileAllCatUserTypeHeroku() {
        String strJsonSerializeAllCatUserTypeHeroku = AllMobileSyncObjectsClass.calloutGetAllCatUserTypeFromHeroku();
        List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass> lstJsonDeserializeAllMobileAllCatUserTypeHerokuObjectClass = (List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass>)JSON.deserialize(strJsonSerializeAllCatUserTypeHeroku, List<AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass>.class);
        return lstJsonDeserializeAllMobileAllCatUserTypeHerokuObjectClass;
    }

    /**
     * This method execute a queueable trigger which insert and update a List of CatUserType Ids of Salesforce.
     *
     * @param objQueueableContext   QueueableContext
     */
    public void execute(QueueableContext objQueueableContext) {
        if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_INSERT) {
            syncInsertAllMobileCatUserTypeToExternalObject(lstAllMobileCatUserType);
        } else if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_UPDATE) {
            syncUpdateAllMobileCatUserTypeToExternalObject(lstAllMobileCatUserType);
        }
    }
}