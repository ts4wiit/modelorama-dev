/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 22: Validation for KATR2 field, when this is equal to '30' then fields AUFSD , LIFSD and FAKSD must to be with the blocked code('01').
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       16-05-2018   	Iván Neria    			Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0025_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0025_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        Boolean ruleHasError = false;
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            
            for(MDM_FieldByRule__c field : uniqueFields(rule.MDM_Fields_by_Rule__r).Values()){
                //Validation for start proccess when the record is SH 
                    if(field.MDM_IsActive__c && toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z001) {
                        //This map contains SH Interlocutors (in Values map) and in the same method "getInterlocutors()" a set<String> called "KUNN2r25" is charged with
                        //KUNN2 field. This helps to call multiple times to MDM_Account_cls and get the same set of KUNN2 by SH Interlocutors associated to this record.
                        //Then the method getKUNN2r25() only returns this set values to check if "isZ019()" method contains any value of the set.
                        Map<String, CDM_Temp_Interlocutor__c> mapInter = MDM_Account_cls.getInterlocutors(setsGranularity, Label.CDM_Apex_SH);
                        for(CDM_Temp_Interlocutor__c inter :mapInter.values()){
                            if(inter.MDM_Temp_Account__c == toEvaluate.get(Label.CDM_Apex_Id) && MDM_Account_cls.isZ019(MDM_Account_cls.getKUNN2r25()).contains(inter.KUNN2__c)){
                                  fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        		  validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c);
                        		  validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                                  validationResult.put(Label.CDM_Apex_CDM_Temp_Interlocutor_c, inter.Id);
                        		  ruleHasError = true;
                            }
                        }
                }
                if(ruleHasError) validationResults.add((MDM_Validation_Result__c)validationResult);
                validationResult = new MDM_Validation_Result__c();
                validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));  
                ruleHasError = false;
            }
        }
        return validationResults;
    }
}