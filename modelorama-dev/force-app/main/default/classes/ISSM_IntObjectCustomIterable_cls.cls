public class ISSM_IntObjectCustomIterable_cls implements Iterator<SObject> {

  SObject[] results { get;set; }
  Integer index { get;set; }
  String query;
  String[] classList;
  String[] nDaysList;
  String[] recordTypeIdList;
  
  public ISSM_IntObjectCustomIterable_cls(String nameConfig, String fieldParam, String valueFieldParam ) {
    index = 0;
    results = new List<SObject>();


    // Retrieve semicolon delimited list of strategies from a Custom Setting empties, openitems, kpis
    ISSM_IntObject__c gv = ISSM_IntObject__c.getInstance(nameConfig);

    if(gv != null && gv.Objects__c != null) classList = gv.Objects__c.split(';');
    if(gv != null && gv.NDays__c != null) nDaysList = gv.NDays__c.split(';');
    if(gv != null && gv.RecordType__c != null) recordTypeIdList = gv.RecordType__c.split(';');

    if(classList != null){
      for(Integer i= 0; i < classList.size(); i++){
        if( recordTypeIdList != null && recordTypeIdList[i] != null && recordTypeIdList[i] != ''){
          if(fieldParam != null && valueFieldParam != null){
            results.addAll(Database.query('SELECT Id FROM '+ classList[i] +' where RecordTypeId =\'' + recordTypeIdList[i] + '\' and '+ fieldParam +' = \'' + valueFieldParam +'\' and CreatedDate <= LAST_N_DAYS:'+nDaysList[i]));
          } else {
            results.addAll(Database.query('SELECT Id FROM '+ classList[i] +' where RecordTypeId =\'' + recordTypeIdList[i] + '\' and CreatedDate <= LAST_N_DAYS:'+nDaysList[i]));
          }
        } else {
          if(fieldParam != null && valueFieldParam != null){
            results.addAll(Database.query('SELECT Id FROM '+ classList[i] +' where '+ fieldParam +' = \'' + valueFieldParam +'\' and CreatedDate <= LAST_N_DAYS:'+nDaysList[i]));
          }else {
            results.addAll(Database.query('SELECT Id FROM '+ classList[i] +' where CreatedDate <= LAST_N_DAYS:'+nDaysList[i]));
          }
        }
        ISSM_Debug_cls.debugList(ISSM_IntObjectCustomIterable_cls.class.getName(), 'ISSM_IntObjectCustomIterable_cls', '14', 'Resultado consulta '+ classList[i], results);
      }
    }
  }

  public Boolean hasNext(){ 
   return results != null && !results.isEmpty() && index < results.size(); 
  }    

  public SObject next() { 
    return results[index++];
  }    

}