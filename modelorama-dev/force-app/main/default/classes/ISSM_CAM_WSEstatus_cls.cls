/****************************************************************************************************
General Information
-------------------
author:     Daniel Rosales
email:      drosales@avanxo.com
company:    Avanxo México
Project:    CAM
Customer:   Grupo Modelo

Description: 
1. Calls out a WS (Salesforce - Mulesoft - SAP) to update the status of a cooler. A equipment_number
and its status is sent to SAP.
2. Modified to receive a map of coolers (EQUNR | Status) and sent it to SAP. Receives also a JSON with 1:N
coolers and their status.

Information about changes (versions)
================================================================================================
Number    Dates                 Author                       Description          
------    -------------         --------------------------   -----------------------------------
1.0       16th-July-2018         Daniel Rosales               Class Creation
2.0 	  08 - Agosto -2018      Daniel Rosales               Modification: 
***** Modified to accept and send a list of coolers and status
================================================================================================
****************************************************************************************************/

public class ISSM_CAM_WSEstatus_cls {
    public static boolean IsIntegration = FALSE;
    
    static{ IsIntegration = 
        boolean.valueOf(
            Test.isRunningTest()?
            !Test.isRunningTest():
            [SELECT Name FROM Profile  WHERE Id = :userinfo.getProfileId()].Name == 'ISSM_Integration'); //ISSM_Integration //System Administrator
          }
    
    class cls_tg_cam_sta {
        public String equnr;	//000000000000000001
        public String stat;		//COMO
        public String resp;		//002
        public String message;	//No se ha efectuado ninguna modificación en el equipo 1
    }
    public class EquiposEstatusResponse{
        public cls_tg_cam_sta[] tg_cam_sta;       
    }
    
    public static void wsEstatus(map<String, String> mEquiposEstatusSFDC){
        
        Map<String, String> mEstatus_SFDC_SAP = new Map<String, String>();
        Map<String, String> mEstatus_SAP_SFDC = new Map<String, String>();
        for(ISSM_CAM_Estatus_SFDC_SAP__c e : [SELECT ISSM_Status_SAP__c, ISSM_Status_SFDC__c
                                              FROM ISSM_CAM_Estatus_SFDC_SAP__c
                                              WHERE ISSM_Send_to_SAP__c = true]) {
                                                  mEstatus_SFDC_SAP.put(e.ISSM_Status_SFDC__c, e.ISSM_Status_SAP__c);
                                                  mEstatus_SAP_SFDC.put(e.ISSM_Status_SAP__c, e.ISSM_Status_SFDC__c);
                                              }
        
        String Json_str = '';
        Json_str += '{';
        Json_str += '"camItems":[';
        
        for(String Equipo : mEquiposEstatusSFDC.keySet()){
            Json_str += '{';
            Json_str += '"serialNumber":' + '"' + Equipo.leftPad(18 , '0') + '",'; //Enviamos el Numero de Equipo 
            Json_str += '"SAPStatusCode":' + '"' + mEstatus_SFDC_SAP.get(mEquiposEstatusSFDC.get(Equipo))  + '"'; // Y el estatus SAP usando el estatus SFDC 
            Json_str += '},';
        }
        
        Json_str = Json_str.removeEnd(',')    ;
        Json_str += ']';
        Json_str += '}';
        
        system.debug('JSON str: ' + json_str);
        
        // ======================== WS REQUEST START =========================================
        Map<String, ISSM_PriceEngineConfigWS__c> configuracionWS_map = ISSM_PriceEngineConfigWS__c.getAll();
        String configName_str = 'ConfigCAMWSEstatus';
        if(Test.isRunningTest()){
            system.debug('TEST DENTRO Estatus');
            Test.setMock(HttpCalloutMock.class, new ISSM_CAM_WSGenMockHttpResponse_cls());
        }
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(configuracionWS_map.get(configName_str).ISSM_EndPoint__c);
        request.setMethod(configuracionWS_map.get(configName_str).ISSM_Method__c);
        request.setTimeout(60000);
        request.setHeader('Content-Type', configuracionWS_map.get(configName_str).ISSM_HeaderContentType__c);
        request.setBody(Json_str);
        
        try {
            HttpResponse response = http.send(request);
            // ======================== WS REQUEST END =========================================
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                // Get ResponseMap
                String responseBody = response.getBody();
                
                system.debug('WS Estatus: responseBody: ' + responseBody);
                
                EquiposEstatusResponse lstResponse = (ISSM_CAM_WSEstatus_cls.EquiposEstatusResponse)JSON.deserialize(responseBody, ISSM_CAM_WSEstatus_cls.EquiposEstatusResponse.class);
                
                system.debug('response: ' + lstResponse);
                system.debug('response inner: ' + lstResponse.tg_cam_sta.size());
                Boolean estatusOK ;
                list<ISSM_Asset_CAM_Log__c> ListAssetLog = new List<ISSM_Asset_CAM_Log__c>();
                
                for(cls_tg_cam_sta Equipo : lstResponse.tg_cam_sta){
                    estatusOK = Boolean.valueOf(Equipo.resp=='001');
                    system.debug('Equipo: ' + equipo);
                    system.debug('Equipo equnr: ' + equipo.equnr);
                    system.debug('Equipo stat: ' + equipo.stat);
                    system.debug('Equipo resp: ' + equipo.resp);
                    system.debug('Equipo message: ' + equipo.message);
                    
                    ISSM_Asset__c a = new ISSM_Asset__c(Equipment_number__c=Equipo.equnr);
                    ISSM_Asset_CAM_Log__c al = 
                        new ISSM_Asset_CAM_Log__c(
                            ISSM_Activity_Date_time__c		= system.now(),
                            ISSM_Asset_CAM__r 				= a,
                            Integration_Status__c 			= estatusOK?'Success':'Failed',
                            Integration_Status_Message__c 	= Equipo.message,
                            Status_SAP__c 					= Equipo.stat,                        
                            Status_SFDC__c 					= mEquiposEstatusSFDC.get(Equipo.equnr) ,                        
                            Name 							= mEquiposEstatusSFDC.get(Equipo.equnr) ,
                            ISSM_Short_description__c 		= 'Cambio en estatus.'
                        );
                    system.debug('WSEstatus al: ' + al);
                    ListAssetLog.add(al);
                }
                
                List<Database.SaveResult> srList = new List<Database.SaveResult>();
                srList = Database.insert(ListAssetLog, false);               
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) { // Operation failed, so get all errors
                        for(Database.Error err : sr.getErrors()) {
                            //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, '[' + filelines[j] + ']' + err.getMessage() + ' ' + err.getStatusCode()));
                            system.debug('***ISSM CAM: Error inserting log for #EQUNR: ' + err.getMessage() + ' ' + err.getStatusCode() );
                        }
                    }   
                }    
                system.debug('WSEstatus: INSERTADO');
            } else { //response statyus code != 200
                system.debug('Error en Response: ' + response.getStatusCode() + System.Label.ISSM_EmptyOrderId);
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_EmptyOrderId));
            }
        } catch(System.CalloutException e) { 
            system.debug('ERROR MESSAGE: ' + e.getMessage() + ' ### ' + 'LINE NUMBER: ' + e.getLineNumber() + 'STACK: ' + e.getStackTraceString());
            throw e;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'TimeOut'));}
        }
    }
        
    public static List<ISSM_Asset__c> setEstatusSFDC(List<ISSM_Asset__c> lstCAMAssets){
        // logica para settear el estatus SFDC usando el estatus SAP
        for(ISSM_Asset__c refri:lstCAMAssets){
            system.debug('Dentro de lógica, estatus SAP: ' + refri.ISSM_Status_SAP__c);
            if(refri.ISSM_Status_SAP__c == 'ALMA')
                refri.ISSM_Status_SFDC__c = refri.Asset_Number__C!=NULL?'Free use':'Available';
            if(refri.ISSM_Status_SAP__c == 'ASAE' )
                refri.ISSM_Status_SFDC__c='To deliver';
        }
        return lstCAMAssets;
        
    }
}