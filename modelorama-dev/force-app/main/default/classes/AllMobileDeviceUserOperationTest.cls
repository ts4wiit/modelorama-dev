/**
 * Test class for AllMobileDeviceUserOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserOperationTest {

	/**
	 * Test method to test syncInsertUpdateDeviceUserFromHerokuRemoteAction method.
	 */
	static testMethod void testSyncInsertUpdateDeviceUserFromHerokuRemoteAction() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();

		//Inserting data.
		callFutureRemoteAction();

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test syncDeleteAllMobileDeviceUserInSF method.
	 */
	static testMethod void testSyncDeleteAllMobileDeviceUserInSF() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();

		//Inserting data.
		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c();
		objAllMobileDeviceUser.AllMobileDeviceUserUserName__c = 'Gustavo Leon';
		objAllMobileDeviceUser.AllMobileDeviceUserId__c = '9452344021';
		insert objAllMobileDeviceUser;

		//Variables.
		List<AllMobileDeviceUser__c> lstAllMobileDeviceUserInHeroku = [SELECT Id, Name, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserId__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserName__c, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserIsActive__c FROM AllMobileDeviceUser__c];
		Set<Id> setIdAllMobileDeviceUserInHK = new Set<Id>();
		if(!lstAllMobileDeviceUserInHeroku.isEmpty()) {
			for(AllMobileDeviceUser__c objAMDeviceUserIterated : lstAllMobileDeviceUserInHeroku) {
				setIdAllMobileDeviceUserInHK.add(objAMDeviceUserIterated.Id);
			}
		}

		//Testing syncDeleteAllMobileDeviceUserInSF method.
		if(!setIdAllMobileDeviceUserInHK.isEmpty()) {
			callFutureSyncDeleteAllMobileDeviceUserInSF(setIdAllMobileDeviceUserInHK);
		}

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku.
	 */
	static testMethod void testSyncInsertAndUpdateAllMobileDeviceUserInSF() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileDeviceUser__c objAllMobileDeviceUser3 = new AllMobileDeviceUser__c(AllMobileDeviceUserUserName__c = 'Griselda Sandoval', AllMobileDeviceUserPasswordText__c = 'GrissaSmile');
		objAllMobileDeviceUser3.AllMobileDeviceUserUserName__c = 'Griselda García';
		objAllMobileDeviceUser3.AllMobileDeviceUserId__c = '77788999';
		objAllMobileDeviceUser3.AllMobileDeviceUserPasswordText__c = 'Memento1';
		objAllMobileDeviceUser3.AllMobileDeviceUserEmail__c = 'grissasmile@gmail.com';
		objAllMobileDeviceUser3.AllMobileDeviceUserLdapId__c = '459628345';
		objAllMobileDeviceUser3.AllMobileDeviceUserPasswordEncrypted__c = '4573ea562c1e909376a37d06ea8285e1';
		insert objAllMobileDeviceUser3;

		//Variables.
		List<AllMobileDeviceUser__c> lstInAndUpAllMobileDeviceUserInSF = [SELECT Id, Name, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserId__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserName__c, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserIsActive__c FROM AllMobileDeviceUser__c] ;
		Set<Id> setIdLstInAndUpAllMobileDeviceUserInSF = new Set<Id>();

		//Creating set ID.
		if(!lstInAndUpAllMobileDeviceUserInSF.isEmpty()) {
			for(AllMobileDeviceUser__c objAMDeviceUserIterated : lstInAndUpAllMobileDeviceUserInSF) {
				setIdLstInAndUpAllMobileDeviceUserInSF.add(objAMDeviceUserIterated.Id);
			}
		}

		//Testing SyncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku method.
		if(!setIdLstInAndUpAllMobileDeviceUserInSF.isEmpty()) {
			callFutureSyncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(setIdLstInAndUpAllMobileDeviceUserInSF);
		}

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku.
	 */
	static testMethod void testSyncInsertAndUpdateAllMobileDeviceUserInSFAlter() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorAlter());

		//Start Test.
		Test.startTest();

		//Inserting data.
		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c();
		objAllMobileDeviceUser.AllMobileDeviceUserUserName__c = 'Gustavo Leon';
		objAllMobileDeviceUser.AllMobileDeviceUserId__c = '9452344021';
		insert objAllMobileDeviceUser;

		AllMobileDeviceUser__c objAllMobileDeviceUser2 = new AllMobileDeviceUser__c();
		insert objAllMobileDeviceUser2;

		//Variables.
		List<AllMobileDeviceUser__c> lstInAndUpAllMobileDeviceUserInSFA = [SELECT Id, Name, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserId__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserName__c, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserIsActive__c FROM AllMobileDeviceUser__c];
		Set<Id> setIdLstInAndUpAllMobileDeviceUserInSFA = new Set<Id>();

		//Creating set ID.
		if(!lstInAndUpAllMobileDeviceUserInSFA.isEmpty()) {
			for(AllMobileDeviceUser__c objAMDeviceUserIterated : lstInAndUpAllMobileDeviceUserInSFA) {
				setIdLstInAndUpAllMobileDeviceUserInSFA.add(objAMDeviceUserIterated.Id);
			}
		}

		//Testing syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku method.
		if(!setIdLstInAndUpAllMobileDeviceUserInSFA.isEmpty()) {
			callFutureSyncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(setIdLstInAndUpAllMobileDeviceUserInSFA);
		}

		//Stop Test.
		Test.stopTest();
	}

	@future(callout=true)
	public static void callFutureRemoteAction() {

		//Testing method.
		AllMobileDeviceUserOperationClass.syncInsertUpdateDeviceUserFromHeroku();
	}

	@future(callout=true)
	public static void callFutureSyncDeleteAllMobileDeviceUserInSF(Set<Id> setIdAllMobileDeviceUserInHK) {
		List<AllMobileDeviceUser__c> lstAllMobileDeviceUserInHeroku = [SELECT Id, Name, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserId__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserName__c, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserIsActive__c FROM AllMobileDeviceUser__c WHERE Id =: setIdAllMobileDeviceUserInHK];
		if(!lstAllMobileDeviceUserInHeroku.isEmpty()) {
			AllMobileDeviceUserOperationClass.syncDeleteAllMobileDeviceUserInSF(lstAllMobileDeviceUserInHeroku);
		}
	}

	@future(callout=true)
	public static void callFutureSyncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(Set<Id> setIdLstInAndUpAllMobileDeviceUserInSF) {
		List<AllMobileDeviceUser__c> lstInAndUpAllMobileDeviceUserInSF = [SELECT Id, Name, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserId__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserName__c, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserIsActive__c FROM AllMobileDeviceUser__c WHERE Id =: setIdLstInAndUpAllMobileDeviceUserInSF];
		if(!lstInAndUpAllMobileDeviceUserInSF.isEmpty()) {
			AllMobileDeviceUserOperationClass.syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(lstInAndUpAllMobileDeviceUserInSF);
		}
	}
}