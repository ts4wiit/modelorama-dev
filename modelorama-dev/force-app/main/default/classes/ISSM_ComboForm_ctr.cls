/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows searches for PickList and Get info of user
    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    17-April-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class ISSM_ComboForm_ctr {
    
    /**
    * @description  Method that allows to obtain the list of values ​​of a selection list
    * 
    * @param    objObject    Object on which the search will be carried out
    * @param    strFld       Field that contains the list of values ​​to search
    * 
    * @return   Return a list of type 'String'
    */	
	@AuraEnabled
    public static String[] getselectOptions(sObject objObject, string strFld) {
        return ISSM_UtilityFactory_cls.getSelectOptions(objObject,strFld);
    }

    /**
    * @description  get user info 
    * 
    * @param    userId    Id of User
    * 
    * @return   Return a list of User
    */
    @AuraEnabled
    public static User[] getUserInfo(String userId) {
        User[] LstUsr = [SELECT Id, Name FROM User WHERE Id=: userId];
        return LstUsr;
    }
}