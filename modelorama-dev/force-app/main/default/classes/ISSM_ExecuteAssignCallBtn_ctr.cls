/****************************************************************************************************
    General Information
    -------------------
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
  
   
    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       06-09-2017      Hector Diaz                 Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_ExecuteAssignCallBtn_ctr {
   
    WebService static String validate(String objCall) {
        String result = System.label.ISSM_AssignCallSuccess;
        PageReference acctPage;
        Decimal numberCallsMade;
        ONCALL__Call__c objCallUpdate = new ONCALL__Call__c();
        List<ONCALL__Call__c> onCall_lst = new List<ONCALL__Call__c>();
        if (String.isNotBlank(objCall)) {
            objCallUpdate.Id = objCall;
            objCallUpdate.OwnerId = UserInfo.getUserId();
            objCallUpdate.ISSM_Call_Assigned__c = true;
            
            onCall_lst = [SELECT Id, ISSM_NumberCallsMade__c FROM ONCALL__Call__c WHERE Id =: objCall];
            if (onCall_lst.size() > 0) {
                for (ONCALL__Call__c reg : onCall_lst) {
                    if ((reg.ISSM_NumberCallsMade__c == null) || (reg.ISSM_NumberCallsMade__c == 0)) {
                        numberCallsMade = 0;
                    } else {
                        numberCallsMade = reg.ISSM_NumberCallsMade__c;
                    }
                }
            }
            
            objCallUpdate.ISSM_NumberCallsMade__c = numberCallsMade + 1;
            if (!Test.isRunningTest()) { update objCallUpdate; }
        }
        return result;
    }
}