global with sharing class MDM_ExecuteBatchRules_ws {
    @AuraEnabled
    WebService static string ExecuteProcessRules(Integer lmtQry, String[] KTOKD, String VKORG, String VKBUR, String SPART, String VTWEG){
        MDM_ProcessRules_bch b = new MDM_ProcessRules_bch(lmtQry, KTOKD, VKORG, VKBUR, SPART, VTWEG);
        ID batchprocessid = Database.executeBatch(b, 100); 
        return batchprocessid;
   }
     @AuraEnabled
    WebService static string ExecuteRelationships(){
        CDM_UpdateRelaionships_bch b = new CDM_UpdateRelaionships_bch();
        ID batchprocessid = Database.executeBatch(b, 200); 
        return batchprocessid;
   } 
}