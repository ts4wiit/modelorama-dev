@isTest
private class ISSM_OrderEraseBch_tst {
	
	@testSetup
	private static void testSetup(){
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}

		Id RecordTypeAccountId;
		try{
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		// create Accounts
		List<Account> accounts_lst = new List<Account>{	};
		for(Integer i = 0;i<10;i++){
			accounts_lst.add(new Account(Name='Test Account '+i,ISSM_MainContactA__c=true,recordTypeId = RecordTypeAccountId,ONTAP__SAPCustomerId__c = i<10 ? '000000000' + i : '00000000' + i));
		}
		insert accounts_lst;
		// create Products
		List<ONTAP__Product__c> product_lst = new List<ONTAP__Product__c>{
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003117',
				ONTAP__ProductType__c = 'FERT',
				ONTAP__DivisionName__c = 'Agua',
				ONTAP__ProductShortName__c = 'AG-Frt',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003118',
				ONTAP__ProductType__c = 'CRN',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Cr',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003119',
				ONTAP__ProductType__c = 'VTR',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Vct',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL'),
			new ONTAP__Product__c(ONTAP__ProductId__c = '0003003120',
				ONTAP__ProductType__c = 'STLL',
				ONTAP__DivisionName__c = 'Cerveza',
				ONTAP__ProductShortName__c = 'CV-Stl',
				ISSM_UnitPrice__c = 15,
				ISSM_Discount__c = 5,
				ONTAP__UnitofMeasure__c = 'HL')
		};
		insert product_lst;

		// create Orders For Accounts
		List<ONTAP__Order__c> orders_lst = new List<ONTAP__Order__c>();
		Integer reps = 5;
		for(Integer i = 0; i< accounts_lst.size() ; i++){
			if(i>0) reps = 10;
			for(Integer j = 0;j<reps;j++){
				orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(i),
					ONTAP__OrderAccount__c = accounts_lst[i].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'Telesales',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = j<10 ? i+'000000010' + j : i+'0000010' + j
				));
			}
			for(Integer j = 0;j<reps;j++){
				orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(i),
					ONTAP__OrderAccount__c = accounts_lst[i].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'B2B',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = j<10 ? i+'000000020' + j : i+'0000020' + j
				));
			}
		}
		orders_lst.add(new ONTAP__Order__c(ONTAP__DeliveryDate__c = DateTime.now().addDays(0),
					ONTAP__OrderAccount__c = accounts_lst[0].Id,
					ONTAP__OrderStatus__c = 'Open',
					ONCALL__OnCall_Status__c = 'Draft',
					ONCALL__Origin__c = 'B2B',
					recordTypeId = devRecordTypeId,
					ONCALL__SAP_Order_Number__c = '0100152874'
				));
		insert orders_lst;

		/*List<ONTAP__Order_Item__c> orderItem_lst = new List<ONTAP__Order_Item__c>();
		for(Integer i = 0; i<orders_lst.size(); i++){
			for(Integer j = 0; j < product_lst.size(); j++){
				orderItem_lst.add(new ONTAP__Order_Item__c(
						ONTAP__ItemProduct__c = product_lst[j].Id,
						ONCALL__OnCall_Order__c = orders_lst[i].Id
					));
			}
		}
		insert orderItem_lst;*/
	}
	
	@isTest static void testEraseOrders() {
		test.startTest();
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		system.debug('id ' + devRecordTypeId);

		AggregateResult[] groupedResults=[SELECT count(Id) cant,ONTAP__OrderAccount__c accountId FROM ONTAP__Order__c where  RecordTypeId =:devRecordTypeId GROUP BY ONTAP__OrderAccount__c HAVING COUNT(Id) > 8  ];
		System.debug(groupedResults);
		Set<String> setId = new Set<String>();
		for (AggregateResult ar : groupedResults)  {
		    if(ar.get('accountId')!= null){
		    	setId.add((String)ar.get('accountId'));
		    }
		}
		System.debug(setId);
		Database.executeBatch(new ISSM_OrderErase_bch(setId), 500);
		test.stopTest();
	}

	
}