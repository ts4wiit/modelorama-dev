/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for schedule class TRM_ZPV2InSettlementPeriod_sch
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_ZPV2InSettlementPeriod_tst {
	
	@testSetup static void loadData() {		
		// insert TRM_SettlementPeriods__c
		TRM_SettlementPeriods__c settlementPeriods 		= new TRM_SettlementPeriods__c();
			settlementPeriods.TRM_ConditionClas__c		= 'ZPV2';
			settlementPeriods.TRM_Description__c		= 'Período de Liquidación TEST';
			settlementPeriods.TRM_EffectiveDate__c		= System.now().Date();
			settlementPeriods.TRM_EndTime__c 			= '19:00:00';
			settlementPeriods.TRM_RecordVolume__c 		= 10000;
			settlementPeriods.TRM_StartTime__c			= '15:00:00';
        Insert settlementPeriods;

        //insert TRM_ConditionClass__c
		TRM_ConditionClass__c  conditionClass1 			= new TRM_ConditionClass__c();
			conditionClass1.TRM_StartDate__c			= System.now().Date().addDays(2);
			conditionClass1.TRM_EndDate__c				= System.now().Date().addDays(5);
			conditionClass1.TRM_ConditionClass__c		= 'ZPV2';
			conditionClass1.TRM_AccessSequence__c		= '973';
			conditionClass1.TRM_Status__c				= 'TRM_Approved';
			conditionClass1.TRM_ConditionUnit__c		= 'MXN';
			conditionClass1.TRM_Description__c			= 'Prueba 01 - ZPV2 973';
			conditionClass1.TRM_MarkedLiquidation__c	= true;
			conditionClass1.TRM_DistributionChannel__c	= '01';
		insert conditionClass1;

		TRM_ConditionClass__c  conditionClass2 			= new TRM_ConditionClass__c();
			conditionClass2.TRM_StartDate__c			= System.now().Date().addDays(2);
			conditionClass2.TRM_EndDate__c				= System.now().Date().addDays(5);
			conditionClass2.TRM_ConditionClass__c		= 'ZPV2';
			conditionClass2.TRM_AccessSequence__c		= '973';
			conditionClass2.TRM_Status__c				= 'TRM_Cancelled';
			conditionClass2.TRM_ConditionUnit__c		= 'MXN';
			conditionClass2.TRM_Description__c			= 'Prueba 01 - ZPV2 973';
			conditionClass2.TRM_MarkedLiquidation__c	= true;
			conditionClass2.TRM_DistributionChannel__c	= '01';
		insert conditionClass2;
	}
	@isTest static void tstExecuteSchedule() {		
		 Test.startTest();
			List<TRM_SettlementPeriods__c> lstSettlementPeriod = [SELECT TRM_ConditionClas__c
																		,TRM_Description__c
																		,TRM_EffectiveDate__c
																		,TRM_EndTime__c
																		,TRM_RecordVolume__c
																		,TRM_StartTime__c
																  FROM TRM_SettlementPeriods__c];
			TRM_ZPV2InSettlementPeriod_sch.executeSchedule(lstSettlementPeriod);
			
        	// NO se incluyen 'assert' debido a que los metodos son 'void'

        Test.stopTest();
	}
	
}