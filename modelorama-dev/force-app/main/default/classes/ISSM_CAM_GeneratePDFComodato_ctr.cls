/******************************************************************************** 
    Company:            Avanxo México
    Author:             Oscar Alvarez Garcia
    Customer:           AbInbev - CAM
    Descripción:        Class that allows to generate the PDF of the Comodato

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    20-Octubre-2018    Oscar Alvarez Garcia           Creation
    ******************************************************************************* */
public with sharing class ISSM_CAM_GeneratePDFComodato_ctr {
	public ISSM_CAM_GeneratePDFComodato_ctr() {}

    /**
    * @description  Method that consults all the information of the indicated case force
    * 
    * @param    IdCaseForce     Id of the case force to check
    * @return   sObject         Result of the query 
    */
	@AuraEnabled
	public static sObject getCaseForce(String idCaseForce){ 
		return getRecordById('ONTAP__Case_Force__c',idCaseForce);
	}
    
    /**
    * @description: Method to get a record by id, the Type of the object is specified in the parameter objectType.
    *               All standard and custom fields are retrieved.
    * 
    * @param    	objectType      String with the Object API Name of the record to retrieve
    * @param    	id              Salesforce Id of the record to retrieve
    * @return   	Return a sObject record of the type specified by the parameter objectType
    */
    public static sObject getRecordById(String objectType, String id) {
        sObject record;
        List<sObject> resultList = new List<sObject>();
        if( id != null ){
            String queryString = 'SELECT ';
                    queryString += String.join(new List<String>(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().keySet()),', ');
                    queryString += ' FROM ' + objectType;
                    queryString += ' WHERE Id = \'' + id + '\'';
                    queryString += ' LIMIT 1';
            resultList = executeQuery(queryString);
        }
        if(resultList.size() == 1){
            record = resultList[0];
        }
        return record;
    }
    /**
    * @description  Method that executes the query
    * 
    * @param        strQuery    Contains the sentence to execute
    * @return       Return a sObject[] with All Values of return Database.query
    */
    public static sObject[] executeQuery(String strQuery){
        sObject[] lstReturn   = new List<sObject>();
        sObject[] lstOfRecords = Database.query(strQuery);
        for (sObject obj: lstOfRecords) {
            lstReturn.add(obj);
        }
        return lstReturn;
    }
}