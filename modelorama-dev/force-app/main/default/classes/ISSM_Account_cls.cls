/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Account basic operations

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       05-06-2017      Carlos Duque                  Class create
    2.0       19-07-2017      Carlos Duque                  Class modify. add new methods.
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_Account_cls {
	
	/**
	 * Class constructor		                             
	 */
	public ISSM_Account_cls() {}

	/**
	 * Get account by id		                             
	 * @param  Id idAccount account id that you need search
	 */
	public Account getAccount(Id idAccount){
	    /*try{
	      return [SELECT Id,  
	      ISSM_BillingManager__c, ISSM_TradeMarketing__c, ISSM_SalesOffice__c,OwnerId,ISSM_RegionalSalesDivision__c,ISSM_SalesOffice__r.ISSM_BillingManager__c,ISSM_SalesOffice__r.ISSM_TradeMarketing__c
	        FROM Account where Id =: idAccount Limit 1];
	    } catch(Exception e){
	      System.debug('Error getting typication Matrix');
	      return null; 
	    }*/
	   	try{
	      return ISSM_AccountSelector_cls.newInstance().selectById(idAccount, 'ISSM_SalesOffice__r');
	    } catch(Exception e){
	      System.debug('Error getting Account');
	      return null; 
	    }
  	}

  	/**
	 * Get map of accounts with ONTAP__SAPCustomerId__c as a key		                             
	 * @param  List<sObject> accounts list of accounts that you need to retrieve the map
	 */
	public Map<String, Account> getMapAccountSapCustomerId(List<sObject> accounts){
/*		Map<String, Account> accountMap = new Map<String, Account>();
		Account account; 
		for(Integer i = 0; i < accounts.size(); i++){
				account = (Account)accounts.get(i);
		        accountMap.put(account.ONTAP__SAPCustomerId__c, account);
		}
		return accountMap;*/
		return getMapAccountSpecificField(accounts, 'ONTAP__SAPCustomerId__c');
	}

	/**
	 * Get map of accounts with Id as a key		                             
	 * @param  List<sObject> accounts list of accounts that you need to retrieve the map
	 */
	public Map<Id, Account> getMapAccountId(List<sObject> accounts){
		Map<Id, Account> accountMap = new Map<Id, Account>();
		Account account; 
		for(Integer i = 0; i < accounts.size(); i++){
				account = (Account)accounts.get(i);
		        accountMap.put(account.Id, account);
		}
		return accountMap;
		
	}

	public Map<Id, Account> getMapAccountId(Set<Id> idsAccount){
		return new Map<Id, Account>(ISSM_AccountSelector_cls.newInstance().selectById(idsAccount, 'ISSM_SalesOffice__r'));

	}

	/**
	 * Get map of accounts with Id as a key		                             
	 * @param  List<sObject> accounts list of accounts that you need to retrieve the map
	 */
	public Map<String, Account> getMapAccountSpecificField(List<sObject> accounts, String field){
		Map<String, Account> accountMap = new Map<String, Account>();
		Account account; 
		for(Integer i = 0; i < accounts.size(); i++){
				account = (Account)accounts.get(i);
		        accountMap.put((String)account.get(field), account);
		}
		return accountMap;
	}

	public Set<String> getSetSpecificField(List<sObject> accounts, String field){
		Set<String> fieldSet = new Set<String>();
		Account account;
		for(Integer i = 0; i < accounts.size(); i++){
			account = (Account)accounts.get(i);
		    fieldSet.add((String)account.get(field));
		}
		return fieldSet;
	}

	/**
	 * Get partner account		                             
	 * @param  Id idAccount
	 */
	public Account getParnertAccount(Id idAccount){
	   /* try{
	      Account OAcc = new Account();
	      OAcc = [Select Id, Name, ParentId from Account Where Id = : idAccount limit 1];

	      return [SELECT Id,ISSM_BillingManager__c, ISSM_TradeMarketing__c, ISSM_SalesOffice__c,OwnerId,ISSM_RegionalSalesDivision__c,ISSM_SalesOffice__r.ISSM_BillingManager__c,ISSM_SalesOffice__r.ISSM_TradeMarketing__c
	        FROM Account where Id =: OAcc.ParentId Limit 1];
	    } catch(Exception e){
	      System.debug('Error getting typication Matrix');
	      return null;
	    }*/
	    try{
	      Account OAcc = getAccount(idAccount);
	      return getAccount(OAcc.ParentId); 
	    } catch(Exception e){
	      System.debug('Error getting Account');
	      return null;
	    }
	}

	

    public List<Account> getDRVs(){
    	String recordTypeRegionalSalesDivision = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_RegionalSalesDivision_RecordType);
        return getAccounts(recordTypeRegionalSalesDivision, null);
    }

    public List<Account> getAccountsByParentId(Id parentAccountId, Boolean salesorg){
    	String recordTypeSalesOfficetId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOffice_RecordType);
        String recordTypeSalesOrg = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOrg_RecordType);
        if(salesorg){
        	return getAccounts(recordTypeSalesOrg, parentAccountId);
       	} else{
       		return getAccounts(recordTypeSalesOfficetId, parentAccountId);
       	}
        
    }    

	public List<Account> getAccounts(Id recordTypeId, Id parentAccountId){
		if(parentAccountId == null){
			return ISSM_AccountSelector_cls.newInstance().selectByRecordType(recordTypeId);
			//return [Select Id, Name, ParentId, ISSM_ParentAccount__c from Account Where RecordTypeId =: recordTypeId];
		} else {
			return ISSM_AccountSelector_cls.newInstance().selectByRecordTypeByParentAccount(recordTypeId, parentAccountId);
			//return [Select Id, Name, ParentId, ISSM_ParentAccount__c from Account Where RecordTypeId =: recordTypeId and ISSM_ParentAccount__c=:parentAccountId];
		}
	}
}