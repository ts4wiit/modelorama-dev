/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_UpdateAccountRshp_bch". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_UpdateAccountRshpBch_tst {
    //Get RecordTypeId
    public static list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account'];
    public static map<String, RecordType> mapRT = new map<String, RecordType>();
    
    @testSetup static void createData(){
        for(RecordType rt : lstRT)
          mapRT.put(rt.DeveloperName, rt); 
        
         //create sales Organization 
        Account salesOrg 							= new Account();
        salesOrg.RecordTypeId         		= mapRT.get('SalesOrg').Id;
        salesOrg.Name 						= 'CMM Hidalgo';
        salesOrg.ONTAP__SAPCustomerId__c 	= '3101';
        insert salesOrg;
        
        //Create sales Office
        Account salesOffice 						= new Account();
        salesOffice.RecordTypeId         	= mapRT.get('SalesOffice').Id;
        salesOffice.Name 					= 'CMM Actopan';
        salesOffice.ONTAP__SalesOgId__c 	= '3101';
        salesOffice.ONTAP__SAPCustomerId__c	= 'GC01';
        salesOffice.ISSM_ParentAccount__c	= salesOrg.Id;
        insert salesOffice;  
        
        //Create Supplier Center
        SupplierCenter__c supplierCenter = new SupplierCenter__c();
        supplierCenter.Name				= 'SAG OCAMPO';
        supplierCenter.Code__c			= 'GB16';
        insert supplierCenter;
        
        //Create DistributionChannel
        Integer auxCont = 0;
        Set<String> lstSetChannel = new Set<String>{'TRADICIONAL', 'MODERNO', 'INTERCOMPAÑIA','EXPORTACIONES','AGENCIA PARTICULAR','COMPAÑIA RELACIONADA','EMPLEADOS','CLIENTES VARIOS','MATERIA PRIMA','INTRAGRUPO'};
		List<DistributionChannel__c> listInsertChannel = new List<DistributionChannel__c>();        
        for (String nameChannel : lstSetChannel){
            DistributionChannel__c channel = new DistributionChannel__c(name = nameChannel, Code__c = auxCont > 9 ?  String.valueOf(auxCont) : '0' + String.valueOf(auxCont++));
            listInsertChannel.add(channel);
        }
        upsert listInsertChannel;
        
        //Create Specific CDM_Temp_Interlocutor__c
        CDM_Temp_Interlocutor__c cdmTempInterlocutor = new CDM_Temp_Interlocutor__c();
        cdmTempInterlocutor.KUNN2__c='100000281';
        cdmTempInterlocutor.KUNNR__c='100039570';
        cdmTempInterlocutor.PARVW__c='SP';
        cdmTempInterlocutor.SPART__c='00';
        cdmTempInterlocutor.VKORG__c='3101';
        cdmTempInterlocutor.VTWEG__c='00';        
        insert cdmTempInterlocutor;
        
        RecordType rt = [SELECT Id,DeveloperName  FROM RecordType WHERE DeveloperName = 'Z010'];
        MDM_Account__c mdmAccount = new MDM_Account__c();

        mdmAccount.KTOKD__c='Z010';
        mdmAccount.NAME1__c='BODEGA BOD JARD DE LA PRIMAVERA';
        mdmAccount.PARVW__c='100039570';
        mdmAccount.RecordTypeId=rt.Id;
        mdmAccount.Unique_Id__c='40000734531400200';
        mdmAccount.VKBUR__c='3140';
        mdmAccount.VKORG__c='3101';
        mdmAccount.SPART__c='00';
        mdmAccount.VTWEGId__c = listInsertChannel.get(0).Id;
        mdmAccount.VTWEG__c='00';
        mdmAccount.VWERKId__c = supplierCenter.Id;
        mdmAccount.VWERK__c='G015';
        insert mdmAccount;
    }
    static testmethod void testBatch() {
        Test.startTest();
        CDM_UpdateAccountRshp_bch cdmUpdateAccountRshp = new CDM_UpdateAccountRshp_bch();
        Database.executeBatch(cdmUpdateAccountRshp);
        Test.stopTest();
        MDM_Account__c MDMacc = [select Id  from MDM_Account__c ];
        CDM_Temp_Interlocutor__c inter =[SELECT Id, MDM_Account__c FROM CDM_Temp_Interlocutor__c];
        //Assert validation: CDM_Temp_Interlocutor__c records must to be updated in the lookup field with MDM_Account__c records that has the same sales information.
        System.assertEquals(MDMacc.Id, inter.MDM_Account__c);
    }
}