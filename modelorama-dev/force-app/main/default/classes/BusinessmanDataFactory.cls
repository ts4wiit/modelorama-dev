public class BusinessmanDataFactory {
    
    public static Account createBusinessman(){
        Id businessmanRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MDRM_Businessman').getRecordTypeId();
        
        Account businessman = new Account(Name = 'Ricardo', 
                                         MDRM_LastName__c = 'Rojas', 
                                         ONTAP__PostalCode__c = '12345',
                                         ONTAP__Colony__c = 'COLONIA 1', 
                                         ONTAP__Province__c = 'SI', 
                                         ONTAP__Municipality__c = 'SI', 
                                         ONTAP__Street__c = 'street', 
                                         ONTAP__Street_Number__c = '23', 
                                         ONTAP__Email__c = 'ric@acc.com',
                                         MDRM_Stage__c = 'Link to shop (Z001 to Z019)',
                                         RecordTypeId = businessmanRt);   
        insert businessman;
        return businessman;
    }
    
    public static Account createBusinessmanEmpty(){
        
        
        Account businessmanEmpty = new Account(Name = 'Ricardo', 
                                         MDRM_LastName__c = 'Rojas', 
                                         ONTAP__PostalCode__c = '',
                                         ONTAP__Colony__c = '', 
                                         ONTAP__Province__c = null, 
                                         ONTAP__Municipality__c = '', 
                                         ONTAP__Street__c = '', 
                                         ONTAP__Street_Number__c = '', 
                                         ONTAP__Email__c = 'ric@acc.com');   
        insert businessmanEmpty;
        return businessmanEmpty;
    }
    
     public static Account createBusinessmanIncorrectNumbers(){
        
        
        Account businessmanNumbers = new Account(Name = 'Ricardo', 
                                         MDRM_LastName__c = 'Rojas', 
                                         ONTAP__PostalCode__c = '987656789876',
                                         ONTAP__Colony__c = 'COLONIA 1', 
                                         ONTAP__Province__c = '', 
                                         ONTAP__Municipality__c = '', 
                                         ONTAP__Street__c = '', 
                                         ONTAP__Street_Number__c = '', 
                                         ONTAP__Email__c = 'ric@acc.com');   
        insert businessmanNumbers;
        return businessmanNumbers;
    }
    
    public static Account createModelorama(){
        
        Account modeloramas = new Account(z019__c = '55555', 
                                          Name = 'HERON MODELORAMA', 
                                          ONTAP__Province__c ='VERACRUZ DE IGNACIO DE LA LLAVE', 
                                          ONTAP__PostalCode__c = '12345', 
                                          ONTAP__Municipality__c = 'VER', 
                                          ONTAP__Colony__c = 'SELECCIONE UNA OPCIÓN:', 
                                          ONTAP__Street__c = 'CALLE', 
                                          ONTAP__Street_Number__c = '1234567890');
        
               insert modeloramas;
        	   return modeloramas;                              
    
    }
    
    public static Account createModeloramaDos(){
        
        Account newModeloramas = new Account(z019__c = '55555', 
                                          Name = 'NUEVO MDRM', 
                                          ONTAP__Province__c ='VERACRUZ DE IGNACIO DE LA LLAVE', 
                                          ONTAP__PostalCode__c = '12345', 
                                          ONTAP__Municipality__c = 'VER', 
                                          ONTAP__Colony__c = 'SELECCIONE UNA OPCIÓN:', 
                                          ONTAP__Street__c = 'CALLE', 
                                          ONTAP__Street_Number__c = '1234567890');
               insert newModeloramas;
        	   return newModeloramas;                              
    
    }
    
    public static Account createModeloramaBussy(){
        
        Account modeloramasBussy = new Account(z019__c = '55555', 
                                          Name = 'HERON MODELORAMA', 
                                          ONTAP__Province__c ='VERACRUZ DE IGNACIO DE LA LLAVE', 
                                          ONTAP__PostalCode__c = '12345', 
                                          ONTAP__Municipality__c = 'VER', 
                                          ONTAP__Colony__c = 'SELECCIONE UNA OPCIÓN:', 
                                          ONTAP__Street__c = 'CALLE', 
                                          ONTAP__Street_Number__c = '1234567890',
                                          MDRM_Used__c = true);
               insert modeloramasBussy;
        	   return modeloramasBussy;                              
    
    }
    
    public static MDRM_Employee__c createEmployee(Id VinculoId){
         Date myDateEmployee = date.newinstance(1960, 2, 17);
        
        MDRM_Employee__c employee = new MDRM_Employee__c(Name = 'Roberto',
                                               MDRM_LastName__c = 'Lopez',
                                               MDRM_MotherSurname__c = 'Perez',
                                               MDRM_Birthdate__c = myDateEmployee,
                                               MDRM_Gender__c = 'M',
                                               MDRM_VinculoMB__c = VinculoId);
               insert employee;
        	   return employee;                              
    
    }
    
    /*public static MDRM_Employee__c createEmployeeLook(){
         Date myDateEmployee = date.newinstance(1960, 2, 17);
        
        MDRM_Employee__c employee = new MDRM_Employee__c(MDRM_VinculoMB__c = '',
            								   Name = 'Roberto',
                                               MDRM_LastName__c = 'Lopez',
                                               MDRM_MotherSurname__c = 'Perez',
                                               MDRM_Birthdate__c = myDateEmployee,
                                               MDRM_Gender__c = 'M');
               insert employee;
        	   return employee;                              
    
    }*/
    
    /*public static MDRM_Employee__c createEmployeeEmpty(){
        
        MDRM_Employee__c employeeEmpty = new MDRM_Employee__c(Name = 'Test',
                                               MDRM_LastName__c = '',
                                               MDRM_MotherSurname__c = '',
                                               MDRM_Birthdate__c = null,
                                               MDRM_Gender__c = '');
               insert employeeEmpty;
        	   return employeeEmpty;                              
    
    }*/
    public static MDRM_Employee__c createEmployeeEmptyError(Id vinculoId){
        
        MDRM_Employee__c employeeEmpty = new MDRM_Employee__c(Name = 'Test',
                                                               MDRM_LastName__c = '',
                                                               MDRM_MotherSurname__c = '',
                                                               MDRM_Birthdate__c = null,
                                                               MDRM_Gender__c = '',
                                                               MDRM_VinculoMB__c = vinculoId);
               insert employeeEmpty;
        	   return employeeEmpty;                              
    
    }
    
    public static MDRM_Employee__c employeeEmpty(){
    MDRM_Employee__c employeeEmpty = new MDRM_Employee__c(Name = '',
                                               MDRM_LastName__c = '',
                                               MDRM_MotherSurname__c = '',
                                               MDRM_Birthdate__c = null,
                                               MDRM_Gender__c = '');
        return employeeEmpty; 
    }
    
    public static MDRM_Vinculo__c createVinculoMdrmEnterprise(Account Businessman, Account Modeloramas){
        
        MDRM_Vinculo__c newVinculo = new MDRM_Vinculo__c(MDRM_Businessman__c = Businessman.Id, 
                                               MDRM_Expansor__c = Modeloramas.Id);
        
        insert newVinculo;
        return newVinculo;
    }
    
    public static MDRM_Vinculo__c createVinculoMdrmEnterpriseDos(Account Businessman, Account ModeloramasDos){
        
        MDRM_Vinculo__c newVinculoDos = new MDRM_Vinculo__c(MDRM_Businessman__c = Businessman.Id, 
                                               MDRM_Expansor__c = ModeloramasDos.Id);
        
        insert newVinculoDos;
        return newVinculoDos;
    }
    public static MDRM_Businessman_Expansor__c createExpansorMdrmEnterprise(Account Businessman, Account Modeloramas){
        
        MDRM_Businessman_Expansor__c newExpasor = new MDRM_Businessman_Expansor__c(MDRM_Businessman__c = Businessman.Id, 
                                               									   MDRM_Expansor__c = Modeloramas.Id);
        
        insert newExpasor;
        return newExpasor;
    }
    
    public static List<MDRM_form__c> createSurvey(Id BusinessmanId){
        
        List<MDRM_form__c> newSurvey = new  List<MDRM_form__c>();
        MDRM_form__c survey = new MDRM_form__c(MDRM_Account_form__c =BusinessmanId,
            										   MDRM_How_did_you_hear_about__c = 'Facebook', 
                                                       MDRM_How_did_you_hear_about_comment__c = 'No',
                                               		   MDRM_Grade_of_schooling__c = 'Primaria',
                                                       MDRM_Marital_status__c = 'Married',
                                                       MDRM_People_financially_dependent__c = 'Any',
                                                       MDRM_Currently_Employed__c = 'Yes',
                                                       MDRM_Current_employment_full_time__c = 'Yes',
                                                       MDRM_Current_Employment__c = 'Housewife',
                                                       MDRM_Current_employment_comment__c = 'No',
                                                       MDRM_Activities_last_employment__c = 'Construction activities',
                                                       MDRM_Activities_last_employment_comment__c = 'No',
                                                       MDRM_Computer_domain_level__c = 'Advanced: Advanced Programming Functions',
                                                       MDRM_Experience_operating_similar_store__c = 'No',
                                                       MDRM_Have_capital_necessary__c = '$10,000 to $29,999',
                                                       MDRM_Former_or_retired_employee__c = 'Yes',
                                                       MDRM_Retired_military__c = 'Yes',
                                                       MDRM_Availability_To_Start_Operating__c = 'More than 2 months',
                                                       MDRM_Facebook_Account__c = 'No',
                                                       MDRM_Why_interested_entrepreneur__c = 'Entrepreneur');
			   newSurvey.add(survey);       		
               insert newSurvey;
        	   return newSurvey;                              
    
    }
    
    public static List<MDRM_form__c> createSurveyError(){
        
        List<MDRM_form__c> newSurvey = new  List<MDRM_form__c>();
        MDRM_form__c survey = new MDRM_form__c(MDRM_How_did_you_hear_about__c = 'Facebook', 
                                                       MDRM_How_did_you_hear_about_comment__c = 'No',
                                               		   MDRM_Grade_of_schooling__c = 'Primaria',
                                                       MDRM_Marital_status__c = 'Married',
                                                       MDRM_People_financially_dependent__c = 'Any',
                                                       MDRM_Currently_Employed__c = 'Yes',
                                                       MDRM_Current_employment_full_time__c = 'Yes',
                                                       MDRM_Current_Employment__c = 'Housewife',
                                                       MDRM_Current_employment_comment__c = 'No',
                                                       MDRM_Activities_last_employment__c = 'Construction activities',
                                                       MDRM_Activities_last_employment_comment__c = 'No',
                                                       MDRM_Computer_domain_level__c = 'Advanced: Advanced Programming Functions',
                                                       MDRM_Experience_operating_similar_store__c = 'No',
                                                       MDRM_Have_capital_necessary__c = '$10,000 to $29,999',
                                                       MDRM_Former_or_retired_employee__c = 'Yes',
                                                       MDRM_Retired_military__c = 'Yes',
                                                       MDRM_Availability_To_Start_Operating__c = 'More than 2 months',
                                                       MDRM_Facebook_Account__c = '',
                                                       MDRM_Why_interested_entrepreneur__c = '');
			   newSurvey.add(survey);       		
               insert newSurvey;
        	   return newSurvey;                              
    
    }
    
    public static Contact contactBusinessman(Id BusinessmanId){
        
    Contact businessmanContact = new Contact(AccountId = BusinessmanId,
        									 LastName = 'Juan',
                                             Phone = '0987654321',
                                             MDRM_Gender__c = 'M');
        insert businessmanContact;
        return businessmanContact; 
    }
    
    public static Contact contactBusinessmanNumbers(Id BusinessmanId){
        
    Contact businessmanContactNum = new Contact(AccountId = BusinessmanId,
        									 LastName = 'Juan',
                                             Phone = '8789',
                                             MDRM_Gender__c = 'M');
        insert businessmanContactNum;
        return businessmanContactNum; 
    }
    
    public static Contact contactBusinessmanEmpty(Id BusinessmanId){
        
    Contact businessmanContactEmp = new Contact(AccountId = BusinessmanId,
        									 LastName = 'Juan',
                                             Phone = '',
                                             MDRM_Gender__c = 'M');
        insert businessmanContactEmp;
        return businessmanContactEmp; 
    }
    
    public static MDRM_Form__c businessmanFacebookUser(Id BusinessmanId){
        
    MDRM_Form__c businessmanFacebook = new MDRM_Form__c(MDRM_Account_Form__c = BusinessmanId,
                                             			MDRM_Facebook_Account__c = 'Rick');
        insert businessmanFacebook;
        return businessmanFacebook; 
    }
    
    // Wrap BusinessmanControllerTest
    public static MDRM_AccountWrapped wrapBusinessman(Account accbus) {
        MDRM_AccountWrapped accWrap = new MDRM_AccountWrapped();

        accWrap.Id = accbus.Id;
        accWrap.Name = accbus.Name;
        accWrap.MDRM_LastName = accbus.MDRM_LastName__c;
        accWrap.ONTAP_PostalCode = accbus.ONTAP__PostalCode__c;
        accWrap.ONTAP_Colony = accbus.ONTAP__Colony__c;
        accWrap.ONTAP_Province = accbus.ONTAP__Province__c;
        accWrap.ONTAP_Municipality = accbus.ONTAP__Municipality__c;
        accWrap.ONTAP_Street = accbus.ONTAP__Street__c;
        accWrap.ONTAP_Street_Number = accbus.ONTAP__Street_Number__c;
        accWrap.ONTAP_Email = accbus.ONTAP__Email__c;

        accWrap.ContactsId = accbus.Contacts[0].Id;
        accWrap.ContactsPhone = accbus.Contacts[0].Phone;
        accWrap.ContactsBirthdate = accbus.Contacts[0].Birthdate;
        accWrap.ContactsGender = accbus.Contacts[0].MDRM_Gender__c;

        accWrap.FormsId = accbus.Forms__r[0].Id;
        accWrap.FormsFacebookAccount = accbus.Forms__r[0].MDRM_Facebook_Account__c;

        return accWrap;
    }
    
}