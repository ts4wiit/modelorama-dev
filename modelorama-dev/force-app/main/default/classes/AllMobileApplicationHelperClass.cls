/**
 * This class serves as helper class for AllMobileApplicationTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileApplicationHelperClass {

	/**
	 * This method enqueue a Job to initiate the synchronization to Heroku of Application object when insert or update.
	 *
	 * @param lstAllMobileApplication	List<AllMobileApplication__c>
	 * @param strEventTriggerFlagApplication	String
	 */
	public static void syncApplicationWithHeroku(List<AllMobileApplication__c> lstAllMobileApplication, String strEventTriggerFlagApplication) {
		AllMobileApplicationOperationClass objAllMobileApplicationOperationClass = new AllMobileApplicationOperationClass(lstAllMobileApplication, strEventTriggerFlagApplication);
		Id IdEnqueueJob = System.enqueueJob(objAllMobileApplicationOperationClass);
	}
}