/**************************************************************************************
Nombre de la clase: ISSM_CAM_WSEnvioPedidos_helper
Versión : 1.0
Fecha de Creación : 20 Julio 2018
Funcionalidad : Clase helper ISSM_WSEnvioPedidos_ctr
Clase de Prueba: ISSM_WSEnvioPedidos_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Julio - 2018      Versión Original
*************************************************************************************/
public class ISSM_CAM_WSEnvioPedidos_helper {
    //public ISSM_CAM_WSEnvioPedidos_helper() { }
    
	public static Boolean SaveOrderId(String orderId, Set<String> setCaseForce_set) {
        List<ONTAP__Case_Force__c> CaseForce_lst = new List<ONTAP__Case_Force__c>();
        List<ONTAP__Case_Force__c> UpdCaseForce_lst = new List<ONTAP__Case_Force__c>();
        CaseForce_lst = [SELECT Id, Name, ISSM_CAM_SAP_Order_Number__c FROM ONTAP__Case_Force__c WHERE Id IN: setCaseForce_set LIMIT 10000];
        if (CaseForce_lst.size() > 0) {
            for (ONTAP__Case_Force__c reg : CaseForce_lst) {
                reg.ISSM_CAM_SAP_Order_Number__c = orderId;
                UpdCaseForce_lst.add(reg);
            }
        }
        if (UpdCaseForce_lst.size() > 0) {
            update UpdCaseForce_lst;
            return true;} else { return false; }
    }
}