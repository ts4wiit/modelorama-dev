public interface ISSM_IOrderSelector_cls {

	List<ONTAP__Order__c> selectById(Set<ID> idSet);
	ONTAP__Order__c selectById(ID orderID);
	List<ONTAP__Order__c> selectById(String orderID);
}