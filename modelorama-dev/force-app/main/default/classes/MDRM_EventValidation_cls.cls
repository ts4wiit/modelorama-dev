public class MDRM_EventValidation_cls {
    public static Map<String, MDRM_EventValidationSettings__mdt> VALIDATION_SETTINGS;
    public static Map<String,Set<String>> APPLY_TO_PROFILE_NAME_ID;
    public static Map<String,Set<String>> APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME;
    public static Map<String,Set<String>> APPLY_TO_EVENT_RECORDTYPE_ID;
    static {
        VALIDATION_SETTINGS = new Map<String, MDRM_EventValidationSettings__mdt>();
        APPLY_TO_PROFILE_NAME_ID = new Map<String,Set<String>>{'AccountCompleteData'=>new Set<String>(), 'RepeatedAccountVisitInTime'=>new Set<String>()};
        APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME = new Map<String,Set<String>>{'AccountCompleteData'=>new Set<String>(), 'RepeatedAccountVisitInTime'=>new Set<String>()};
        APPLY_TO_EVENT_RECORDTYPE_ID = new Map<String,Set<String>>{'AccountCompleteData'=>new Set<String>(), 'RepeatedAccountVisitInTime'=>new Set<String>()};
        
        for(MDRM_EventValidationSettings__mdt setting : [SELECT DeveloperName, MDRM_AccountRecordTypeApplied__c, Event_RecordTypeApplied__c,MDRM_FieldsTovalidate__c,MDRM_UserProfileApplyTo__c FROM MDRM_EventValidationSettings__mdt]){
            VALIDATION_SETTINGS.put(setting.DeveloperName, setting);
        }
        for(Profile prof : [SELECT Id, Name FROM Profile]){
            for(String names :VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_UserProfileApplyTo__c.split(',')){
                if(prof.name == names){
                    APPLY_TO_PROFILE_NAME_ID.get('AccountCompleteData').add(prof.Id);
                    break;
                }
            }
            for(String names :VALIDATION_SETTINGS.get('RepeatedAccountVisitInTime').MDRM_UserProfileApplyTo__c.split(',')){
                if(prof.name == names){
                    APPLY_TO_PROFILE_NAME_ID.get('RepeatedAccountVisitInTime').add(prof.Id);
                    break;
                }
            }
        }
        for(RecordType rt:[select Id, developerName from RecordType where sobjecttype='Account' or sobjecttype='Event']){
            for(String names :VALIDATION_SETTINGS.get('AccountCompleteData').Event_RecordTypeApplied__c.split(',')){
                if(rt.developername == names){
                    APPLY_TO_EVENT_RECORDTYPE_ID.get('AccountCompleteData').add(rt.Id);
                    break;
                }
            }
            for(String names :VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_AccountRecordTypeApplied__c.split(',')){
                if(rt.developername == names){
                    APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME.get('AccountCompleteData').add(rt.DeveloperName);
                    break;
                }
            }
            for(String names :VALIDATION_SETTINGS.get('RepeatedAccountVisitInTime').Event_RecordTypeApplied__c.split(',')){
                if(rt.developername == names){
                    APPLY_TO_EVENT_RECORDTYPE_ID.get('RepeatedAccountVisitInTime').add(rt.Id);
                    break;
                }
            }
            for(String names :VALIDATION_SETTINGS.get('RepeatedAccountVisitInTime').MDRM_AccountRecordTypeApplied__c.split(',')){
                if(rt.developername == names){
                    APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME.get('RepeatedAccountVisitInTime').add(rt.DeveloperName);
                    break;
                }
            }
        } 
    }
	   
    public static List<Event> filterEventsByWhatId(List<Event>events,String prefix){
        List<Event> eventos = new List<Event>();
        for(Event evt : events){
            if(String.ValueOf(evt.WhatId).startsWith(prefix)){
                eventos.add(evt);
            }
        }
        return eventos;
    }
    
	public static void validateAccountCompleteData(List<Event>events){
        Set<Id> ids = new Set<Id>();
        Set<Id> idAccts = new Set<Id>();
        String query = 'SELECT {0},RecordType.DeveloperName FROM Account WHERE Id IN:ids', fields = '';
        Boolean isComplete = true;
                
        query = String.format(query, new List<String> {VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_FieldsTovalidate__c});
        System.debug(':::: query '+query);
        for(Event evt : events){
            ids.add(evt.WhatId);
        }
        for(Account acct : Database.query(query))
        {
            System.debug(':::: acct ID '+ acct.Id);
            System.debug(':::: isComplete BEFORE'+ isComplete);
            for(String fld:VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_FieldsTovalidate__c.split(',')){
                isComplete &= acct.get(fld)!=null;
            }
            System.debug(':::: isComplete AFTER'+ isComplete);
            if(!isComplete && (APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME.get('AccountCompleteData').contains(acct.RecordType.DeveloperName))){
                idAccts.add(acct.Id);
            }
            isComplete = true;
        }
        
        for(Event evt : events){
            if(idAccts.contains(evt.WhatId)){
                if(APPLY_TO_EVENT_RECORDTYPE_ID.get('AccountCompleteData').contains(evt.RecordTypeId))
                	evt.addError(Label.MDRM_IncompleteDataInAccount);
            }
        }
    }
    
    public static void validateRepeatedAccountVisitInTime(List<Event> events){
        System.debug(':::: validateRepeatedAccountVisitInTime');
        System.debug('events: '+events);
        Set<Id> ids = new Set<Id>();
        Set<Event> eventID = new Set<Event>();
        for(Event evt : events){
            ids.add(evt.WhatId);
        }
        Date startOfWeek, endOfWeek;
        startOfWeek = System.today().toStartOfWeek();
        endOfWeek = startOfWeek.addDays(6);
        for(Event evt : [SELECT Id, StartDateTime FROM Event WHERE WhatId IN:ids 
                         AND StartDateTime >=: startOfWeek
                         AND RecordTypeId IN:APPLY_TO_EVENT_RECORDTYPE_ID.get('RepeatedAccountVisitInTime')]){
            eventID.add(evt);
        }
        System.debug(':::: eventID '+eventID);
        for(Event evt : events){
            startOfWeek = evt.StartDateTime.date().toStartOfWeek();
        	endOfWeek = startOfWeek.addDays(6);
            if(evt.StartDateTime >= startOfWeek && evt.StartDateTime <= endOfWeek){
                for(Event prevEv : eventID){
                    if(prevEv.StartDateTime >= startOfWeek && prevEv.StartDateTime <= endOfWeek){
                        evt.addError(Label.MDRM_DuplicatedVisits);
                        break;
                    }
                }
            }
        }
    }
}