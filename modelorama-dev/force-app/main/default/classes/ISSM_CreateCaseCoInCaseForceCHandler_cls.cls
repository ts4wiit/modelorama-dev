/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: Clase que inserta el comentario del caso Standard en el objeto ONTAP__Case_Force_Comment__c  
---------------------------------------------------------------------------------
No.     Fecha            Autor                    Descripción
------ ----------      ----------------------  -----------------------------------------
1.0     22-Junio-2017    Hector Diaz (HD)         Creador.
***********************************************************************************/
public without sharing class ISSM_CreateCaseCoInCaseForceCHandler_cls {
    ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    /**
    * Descripcion :  Metodo el cual obtiene el ISSM_CaseForceNumber__c del Case y con este valor se realiza la relacion 
                    con el objeto CUSTOM en el campo ONTAP__Case_Force__c
    * @param  lstDataCaseCommentNew =  Datos que se obtienen antes del crear el caso en objeto Standard CaseComment
    * @return none  
    **/
    public void CreateCaseCommentInCaseForceComment(List <CaseComment> lstDataCaseCommentNew ) {
        System.debug('#####ISSM_CreateCaseCoInCaseForceCHandler_cls -- CreateCaseCommentInCaseForceComment');
        ONTAP__Case_Force_Comment__c objCaseForceComment= new ONTAP__Case_Force_Comment__c();
        List<Case> lstCaseComment = objCSQuerys.QueryCase(lstDataCaseCommentNew[0].ParentId); 
                              
        for(CaseComment  objDataCaseComment : lstDataCaseCommentNew){
            objCaseForceComment.ONTAP__Comment__c =  objDataCaseComment.CommentBody;
            objCaseForceComment.ONTAP__Case_Force__c = lstCaseComment[0].ISSM_CaseForceNumber__c; 
        }       
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseCommentTrigger); //Se inactiva el trigger ISSM_CaseCommentTrigger_tgr para evitar que se cicle
        if (String.isNotBlank(objCaseForceComment.ONTAP__Comment__c) && String.isNotBlank(objCaseForceComment.ONTAP__Case_Force__c)) { insert objCaseForceComment; }    
    }    
}