/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Class to schedule KPI batch. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Class to schedule KPI batch.
==============================================================================================================================
*********************************************************************************************************************************/
global class MDRM_BatchScheduler implements schedulable{

    global void execute(SchedulableContext sc)
    {
      MDRM_UpsertPerformance b = new MDRM_UpsertPerformance(); 
        database.executebatch(b);
    }

}