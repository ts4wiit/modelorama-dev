/**
 * This class serves as helper class for AllMobileRouteAppVersionTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileRouteAppVersionHelperClass {

	/**
	 * This method enqueue a Job to initiate the synchronization of RouteAppVersion object to Heroku when insert or update.
	 *
	 * @param lstAllMobileRouteAppVersion	List<AllMobileRouteAppVersion__c>
	 * @param strEventTriggerFlagApplication	String
	 */
	public static void syncRouteAppVersionWithHeroku(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion, String strEventTriggerFlag) {
		AllMobileRouteAppVersionOperationClass objAllMobileRouteAppVersionOperationClass = new AllMobileRouteAppVersionOperationClass(lstAllMobileRouteAppVersion, strEventTriggerFlag);
		ID IdEnqueueJob = System.enqueueJob(objAllMobileRouteAppVersionOperationClass);
	}
}