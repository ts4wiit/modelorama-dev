/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for schedule class TRM_StartSchedulers_sch
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_StartSchedulers_tst {
	@testSetup static void loadData() {		
		TRM_SettlementPeriods__c settlementPeriods 		= new TRM_SettlementPeriods__c();
			settlementPeriods.TRM_ConditionClas__c		= 'ZMIW';
			settlementPeriods.TRM_Description__c		= 'Período de Liquidación TEST';
			settlementPeriods.TRM_EffectiveDate__c		= System.now().Date().addDays(1);
			settlementPeriods.TRM_EndTime__c 			= '19:00:00';
			settlementPeriods.TRM_RecordVolume__c 		= 10000;
			settlementPeriods.TRM_StartTime__c			= '15:00:00';
        Insert settlementPeriods;
    }	
	@isTest static void tstScheduleIt() {
		 Test.startTest();
        	String jobId = TRM_StartSchedulers_sch.executeSchedule(); 
        	CronTrigger ct = [SELECT Id, CronExpression FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(TRM_StartSchedulers_sch.CRON_EXPR, ct.CronExpression);
        Test.stopTest();
	}
	
}