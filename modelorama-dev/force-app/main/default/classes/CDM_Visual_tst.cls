/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_Visual_ctr". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_Visual_tst {
    static testMethod void testMethodCtr(){
        String order ='01';
        test.startTest();
        //Assert validation for method 'getOrderOne(Order)' must returns CDM_Execution_Order__mdt records due order that receive as parameter
        System.assertEquals(order,CDM_Visual_ctr.getOrderOne(order).get(0).CDM_Order__c ); 
        test.stopTest();
    }

}