/**
 * Developed by:       Avanxo México
 * Author:             Oscar Alvarez
 * Project:            AbInbev - Trade Revenue Management
 * Description:		   Apex Controller class of the Lightning component 'TRM_ApprovalConditions_lcp', for the approval massive process, cancellation and synchronization retries
 *
 *  No.        Date            Author                 Description
 * 1.0    06-Agosto-2018     Oscar Alvarez             CREATION
 */
public with sharing class TRM_ApprovalConditions_ctr {
    public static List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
    /**
    * @description  According to the operation (R- Retry, C- create and B- Marked to erase). Carry out the following processes: 
    *               1.- Extract the existing settlement periods 
    *               2.- Validate if the condition classes enter a settlement period 
    *               3.- If the operation is 'C' or 'B': 
    *                   3.1 - Execute the method 'sendConditionClass' 
    *               4.- If the operation is 'R': 
    *                   4.1 - execute the method 'forwardingToSAP' 
    * @param    strCondition    String that contains the records to CREATE, CANCEL or RETRY SYNCHRONIZED
    * @param    operation       R- Retry, C- create and B- Marked to delete
    * @param    typeCondition   Brings the type of condition under which the approval, cancellation or retry process will be applied
    * 
    * @return   no returns
    */
    @AuraEnabled
    public static void appprovalForIntegrate(String strCondition,String operation,String typeCondition){
        System.debug('### START APPROVAL: '+ strCondition);
        Set<String> lstNameCondWithPeriod = new Set<String>();
        set<String> lstIdConditions = new set<String>();
        boolean isSettlementPeriod = false;
        Integer numberLots;
        List<TRM_ConditionClass__c> lstConditions = new List<TRM_ConditionClass__c>();
        lstConditions = (List<TRM_ConditionClass__c>)JSON.deserialize(strCondition, List<TRM_ConditionClass__c>.class); 
        Map<String, TRM_SettlementPeriods__c> mapSettlementPeriod = new Map<String, TRM_SettlementPeriods__c>();
        List<TRM_SettlementPeriods__c> settlementPeriods = [SELECT TRM_ConditionClas__c
                                                                  ,TRM_RecordVolume__c
                                                                  ,TRM_EffectiveDate__c
                                                                  ,TRM_StartTime__c
                                                                  ,TRM_EndTime__c
                                                            FROM TRM_SettlementPeriods__c];        
        for(TRM_SettlementPeriods__c period: settlementPeriods) mapSettlementPeriod.put(period.TRM_ConditionClas__c,period);            
        
        lstNameCondWithPeriod = mapSettlementPeriod.keySet();
        // valida si es un periodo de liquidación
        if(lstNameCondWithPeriod.contains(typeCondition) && settlementPeriods.size() > 0){
            isSettlementPeriod = isInTheDateTimePeriod(mapSettlementPeriod.get(typeCondition));  
            numberLots = (Integer) [SELECT TRM_RecordVolume__c 
                                    FROM TRM_SettlementPeriods__c 
                                    WHERE TRM_ConditionClas__c =:typeCondition][0].TRM_RecordVolume__c;
        }
        if(operation.equals(Label.TRM_RetryCC)) { 
            forwardingToSAP(lstConditions,isSettlementPeriod,numberLots);
        }else{
            sendConditionClass(lstConditions,isSettlementPeriod,numberLots,operation,true);
        }        
    }
    /**
    * @description  This method is exclusively for synchronization retries of all those who failed due to connection issues with mulesoft: 
    *  1.- From the list of condition classes that it receives, it separates according to the status 'TRM_Approved' or 'TRM_Cancelled'. 
    *      To do it he sent first of the approved ones and later the canceled ones
    * 
    * @param    lstConditions           List of business conditions to retry synchronized with SAP
    * @param    isSettlementPeriod      Indicate whether or not it is in a settlement period
    * @param    numberLots              Number of lots that mulesoft processes in each loop
    * 
    * @return   no returns
    */
    public static void forwardingToSAP( List<TRM_ConditionClass__c> lstConditions, Boolean isSettlementPeriod, Integer numberLots){ 
        List<TRM_ConditionClass__c> lstCondApproval = new List<TRM_ConditionClass__c>();
        List<TRM_ConditionClass__c> lstCondCancel = new List<TRM_ConditionClass__c>();

        for(TRM_ConditionClass__c conditionClass : lstConditions){
                if(conditionClass.TRM_Status__c.equals('TRM_Approved')) lstCondApproval.add(conditionClass);
                if(conditionClass.TRM_Status__c.equals('TRM_Cancelled')) lstCondCancel.add(conditionClass);
        }
        
         sendConditionClass(lstCondApproval,isSettlementPeriod,numberLots,Label.TRM_CreateCC,false);
         sendConditionClass(lstCondCancel,isSettlementPeriod,numberLots,Label.TRM_CancelCC,false);
    }
    /**
    * @description  This method is exclusively for APPROVE or CANCEL Condition Classes 
    *   1.- Go through the list of Condition Classes and validate 
    *       1.1.- If the transaction is B, enter a settlement period, turn on the flag of 'TRM_MarkedLiquidation__c' to indicate that this record will not travel to SAP until the end of the period. settlement 
    *       1.2.- Leave the flag TRM_FailedSendtoSap__c in FALSE and TRM_IsMassiveApproval__c in TRUE, to control retries 
    *       1.3.- Execute an update on the list 
    *   2.- In case there has not been a settlement period the records travel to SAP
    * 
    * @param    lstConditions           List of business conditions to retry synchronized with SAP
    * @param    isSettlementPeriod      Indicate whether or not it is in a settlement period
    * @param    numberLots              Number of lots that mulesoft processes in each loop
    * @param    operation               C- create and B- Marked to delete
    * @param    applyApprovalProcess    Flag, controls whether to apply an approval process or not
    * 
    * @return   no returns
    */
    public static void sendConditionClass(List<TRM_ConditionClass__c> lstConditions,boolean isSettlementPeriod,Integer numberLots,String operation,Boolean applyApprovalProcess){
            String strJSON;
            List<TRM_ConditionClass__c> lstCondForCancelSAP = new List<TRM_ConditionClass__c>();
            for(TRM_ConditionClass__c conditionClass : lstConditions){
                conditionClass.TRM_FailedSendtoSap__c = false;
                conditionClass.TRM_MarkedLiquidation__c = false;
                conditionClass.TRM_IsMassiveApproval__c = true;
                if(isSettlementPeriod && operation == Label.TRM_CreateCC)  conditionClass.TRM_MarkedLiquidation__c = true;    
                if(operation == Label.TRM_CancelCC){
                    conditionClass.TRM_Status__c = 'TRM_Cancelled';
                    if(Integer.valueOf(conditionClass.TRM_SynchronizeSAP__c) > 0 || Test.isRunningTest()){
                        if(isSettlementPeriod) conditionClass.TRM_MarkedLiquidation__c = true;
                        
                        lstCondForCancelSAP.add(conditionClass);
                    }                
                }
            }
            update lstConditions;
            // VALIDA: si es creación (C) entra al proceso de aprobación
            if(operation == Label.TRM_CreateCC && applyApprovalProcess) 
                for(TRM_ConditionClass__c conditionClass : lstConditions) ISSM_ApprovalProcess_cls.submitAndProcessApprovalRequest(conditionClass.Id);
            // VALIDA: Si esta en un periodo de liquidación "NO" hace el envio a SAP
            if (!isSettlementPeriod){
                if(operation == Label.TRM_CancelCC && lstCondForCancelSAP.size() > 0){
                    strJSON = generateJson(lstCondForCancelSAP, operation, (numberLots != null ? numberLots : Integer.valueOf(Label.TRM_NumberLotsProcessed)));
                    TRM_SendClassConditionToSAP_cls.sendClassCondFuture(strJSON,operation);
                }
                if(operation == Label.TRM_CreateCC){
                    strJSON = generateJson(lstConditions, operation, (numberLots != null ? numberLots : Integer.valueOf(Label.TRM_NumberLotsProcessed)));
                    TRM_SendClassConditionToSAP_cls.sendClassCondFuture(strJSON,operation);
                }
            }
    }
    /**
    * @description  Valid if a certain settlement period is valid vs. the current time 
    *   @param      settlementPeriods       Object of type of liquidation periods
    *   @return     boolean                 TRUE if it is in a settlement period window or FALSE if it is not
    */
    public static boolean isInTheDateTimePeriod(TRM_SettlementPeriods__c settlementPeriods){ 
        boolean response = false;
        Date dtPeriod   = (Date) settlementPeriods.TRM_EffectiveDate__c;
        Date dtToday    = (Date) Datetime.Now().Date();
        if(dtToday >= dtPeriod || Test.isRunningTest()){
            DateTime runtime = getLocalDateTime('',settlementPeriods);
            DateTime runtime2 = getLocalDateTime('START',settlementPeriods);
            DateTime runtime3 = getLocalDateTime('END',settlementPeriods);
            if(runtime >= runtime2 && runtime <= runtime3) response = true;
        }
        return response;
    }
    /**
    * @description  Convert a data type String to DataTime according to the date / time to be evaluated. 
    *   @param      typeEvaluate            Indicate which date you will evaluate (START - Start time, END - end time or a NULL that indicates that you will evaluate the current time).
    *   @param      settlementPeriods       Object of type of liquidation periods
    * 
    *   @return     Datetime                Returns a date in DateTime format
    */
    public static Datetime getLocalDateTime(String typeEvaluate, TRM_SettlementPeriods__c settlementPeriods){   
        Time t;
        String[] strTime;

        if(String.isNotBlank(typeEvaluate)) strTime =  typeEvaluate == 'START' ? settlementPeriods.TRM_StartTime__c.split(':') : settlementPeriods.TRM_EndTime__c.split(':');
        if(strTime != null ) t = time.newInstance(Integer.valueOf(strTime[0]),Integer.valueOf(strTime[1]), Integer.valueOf(strTime[2]), 0);

        DateTime dt = Datetime.Now();
        Datetime dTime = dt.Date();
        dTime = dTime.addHours(t != null ? t.hour() : dt.hour());
        dTime = dTime.addMinutes(t != null ? t.minute() :dt.minute());
        dTime = dTime.addSeconds(t != null ? t.second() :dt.second());
        
        return dTime;
    }
    /**
    * @description  Generates the structure of the JSON that will be sent to SAP 
    *   @param      lstConditionClass   Indicate which date you will evaluate (START - Start time, END - end time or a NULL that indicates that you will evaluate the current time).
    *   @param      operation           C- create and B- Marked to delete
    *   @param      numberLots          Number of lots that mulesoft processes in each loop
    *
    *   @return     String              Return the structure of the JSON to send to Mulesoft
    */
    public static String generateJson(List<TRM_ConditionClass__c> lstConditionClass, String operation, Integer numberLots) {
        Set<String> lstExternalId = new Set<String>();
        for(TRM_ConditionClass__c conditionClass: lstConditionClass){
            lstExternalId.add(conditionClass.TRM_ExternalId__c);
        }
        String strQueryC = 'SELECT COUNT()'; 
        strQueryC       += ' FROM  TRM_ConditionRecord__c';
        strQueryC       += ' WHERE TRM_ConditionClass__r.TRM_ExternalId__c IN :lstExternalId'; 
        String strQueryB = 'SELECT COUNT()';
        strQueryB       += ' FROM  TRM_ConditionRecord__c';
        strQueryB       += ' WHERE TRM_SynchronizedSAP__c = true'; 
        strQueryB       += ' AND TRM_SAPId__c <> null';
        strQueryB       += ' AND TRM_ConditionClass__r.TRM_ExternalId__c IN :lstExternalId'; 
        Integer numConditionRecord  = database.countQuery(operation == Label.TRM_CreateCC ? strQueryC : strQueryB);
        Integer numberLoops         = (Integer) numConditionRecord / numberLots;
        Integer reminder            = math.mod(numConditionRecord, numberLots);
        numberLoops                 = reminder > 0 ? NumberLoops + 1 : NumberLoops; 
        String strJSON = Json.serialize(new TRM_SendClassConditionToSAP_cls.WrapperConditions(operation,lstExternalId,numberLots,numberLoops));

        return strJSON;
     }
     /**
    * @description  If there is a programmed scheduler for an existing settlement period, it eliminates    
    *   @param      name   Name of the settlement period to be eliminated
    *
    *   @return     No return
    */
    public static void unSchedule(String name) {
        for (CronTrigger ct : [select Id,CronExpression from CronTrigger where CronJobDetail.Name like :(name+'%')] )  System.abortJob(ct.Id);
    }
    /**
    * @description  Receives a serie of parameters to create a SOQL query. The keyWord parameter indicates the word to search in the specific fields
    *
    * @param    objectType              API Name of the Object to search for
    * @param    fieldOrderByList        API Name of fields to Order By
    * @param    numberOfRowsToReturn    Max number of records to query
    * @param    keyWord                 String variable with the search box input
    * @param    recordType              Record Type Developer Name to query on records
    * @param    typeCondition           type of condition to filter
    * @param    fieldSetName            Fieldset nombre for columns
    * @param    operation               Operation to perform C = Create and M = Modify
    * 
    * @return   Return a list of type 'DataTableResponse' with columns and data
    */
    @AuraEnabled
    public static DataTableResponse searchByKeyWord(String objectType, 
                                                    String fieldOrderByList, 
                                                    String numberOfRowsToReturn, 
                                                    String keyWord, 
                                                    String recordType,
                                                    String typeCondition,
                                                    String fieldSetName,
                                                    String operation){
        
        DataTableResponse response = new DataTableResponse();
        String createdQuery = createQuery(objectType
                                          , fieldOrderByList
                                          , numberOfRowsToReturn
                                          , keyWord != null ? keyWord : ''
                                          , recordType
                                          , typeCondition
                                          , fieldSetName
                                          , operation);
       	System.debug('####START createdQuery:  '+ createdQuery); 
        response.lstDataTableColumns = lstDataColumns;
        response.lstDataTableData = Database.query(createdQuery);
        System.debug('#### response.lstDataTableData: ' + response.lstDataTableData);
        System.debug('#### CPUTime: ' + Limits.getCpuTime() );
        System.debug('#### LimitCPUTime: ' + Limits.getLimitCpuTime());
        return response;
    }
    
    /**
    * @description  Receives a serie of parameters to create a SOQL query string and get the columns of the datatable 
    * 
    * @param    objectType              API Name of the Object to search for
    * @param    fieldOrderByList        API Name of fields to Order By
    * @param    numberOfRowsToReturn    Max number of records to query
    * @param    keyWord                 String variable with the search box input
    * @param    recordType              Record Type Developer Name to query on records
    * @param    typeCondition           type of condition to filter
    * @param    fieldSetName            Fieldset nombre for columns
    * @param    operation               Operation to perform C = Create and M = Modify
    * 
    * @return   Return a string with the SOQL Query created 
    */
    public static String createQuery(String objectType, 
                                     String fieldOrderByList, 
                                     String numberOfRowsToReturn, 
                                     String keyWord,
                                     String recordType,
                                     String typeCondition,
                                     String fieldSetName,
                                     String operation){

        String strStatus = 'TRM_SentApproval';
        String strIdUsuario = UserInfo.getUserId();
        String strStatusProcess = Label.TRM_StatusProcessApproval;
        Set<Id> setProcessInstanceId = new  Set<Id>();
        Set<String> setProcessInstance = new  Set<String>(); 

        lstDataColumns = getColumnsFromFieldSet(fieldSetName, objectType);
        List<String> lstFieldsToQuery = new List<String>(); 
        List<String> lstFieldsLabelToQuery = new List<String>(); 
        for(DataTableColumns column : lstDataColumns){
            lstFieldsToQuery.add(column.fieldName);
            if(column.type.equals('picklist') && !column.fieldName.equals('TRM_Status__c')){
                lstFieldsLabelToQuery.add('toLabel(' + column.fieldName + ')');
            }else{
                lstFieldsLabelToQuery.add(column.fieldName);
            }
        } 

        // los siguientes 2 loop's validan que solo pueda vizualizar Clases condicion
        for(ProcessInstanceWorkitem processInstItem : [SELECT ProcessInstanceId FROM ProcessInstanceWorkitem 
                                                       WHERE Actor.Id =: strIdUsuario  AND ProcessInstanceId IN (SELECT Id 
                                                       FROM ProcessInstance 
                                                       WHERE Status =: strStatusProcess)]){setProcessInstanceId.add(processInstItem.ProcessInstanceId);}
        for(ProcessInstance processInst:[SELECT TargetObjectId FROM ProcessInstance 
                                         WHERE Id IN :setProcessInstanceId]){ setProcessInstance.add('\''+processInst.TargetObjectId+'\'');}        
        List<String> lstProcessInstance = new List<String>(setProcessInstance);
        String sQuery = 'SELECT TRM_SynchronizeSAP__c, ' + String.join(lstFieldsLabelToQuery, ',');
               sQuery += ' FROM ' + objectType;
               sQuery += ' WHERE';
               sQuery += ' TRM_ConditionClass__c = \'' + typeCondition + '\''; 
               
               sQuery += (String.isBlank(recordType)) ? '' : ' AND RecordType.DeveloperName = \'' + recordType + '\'';
                if (operation.equals(Label.TRM_CreateCC)){ // crear
                    sQuery += lstProcessInstance.size() > 0 ? (' AND Id IN (' + String.join(lstProcessInstance, ',' ) +')') :' AND Id IN (\'\')' ;
                    sQuery += ' AND TRM_Status__c = \'' + strStatus + '\''; 
                }
                if (operation.equals(Label.TRM_CancelCC)){ //marcado para borrar
                    sQuery += ' AND TRM_Status__c <> \'' + 'TRM_Cancelled' + '\''; 
                    sQuery += ' AND TRM_Status__c <> \'' + 'TRM_Expired' + '\''; 
                    sQuery += ' AND TRM_Status__c <> \'' + 'TRM_Rejected' + '\''; 
                }
                if(operation.equals(Label.TRM_RetryCC)){//Reintento
                     sQuery += ' AND (TRM_Status__c = \'' + 'TRM_Cancelled' + '\' OR  TRM_Status__c = \'' + 'TRM_Approved' + '\')'; 
                     sQuery += ' AND TRM_FailedSendtoSap__c = true'; 
                }        
        if(String.isNotBlank(keyWord) && !lstFieldsToQuery.isEmpty()){
            sQuery += ' AND (';
            String sKeyWord = '%' + String.escapeSingleQuotes(keyWord) + '%';
            String[] token_list = new List<String>();
            for(String fieldName : lstFieldsToQuery){
                if(!fieldName.equals('TRM_StartDate__c') && !fieldName.equals('TRM_EndDate__c')){
                    String token = fieldName + ' LIKE \'' + sKeyWord + '\'';
                    token_list.add(token);
                }
            }
            sQuery += ' ' + String.join(token_list, ' OR ');
            sQuery += ')';               
        }
        sQuery += ' ORDER BY ' + fieldOrderByList;
        sQuery += ' LIMIT ' + numberOfRowsToReturn;
        return sQuery;
    }
	/**
    * @description  Get the columns of a FieldSet of a certain object 
    * 
    * @param    fieldSetName            API Name of fieldSet
    * @param    ObjectName              API Name of the Object
    * 
    * @return   Return a list of columns of type DataTableColumns (It's wrapper)
    */
    public static List<DataTableColumns> getColumnsFromFieldSet(String fieldSetName, String ObjectName){
        List<Schema.FieldSetMember> fieldSetMemberList =  readFieldSet(fieldSetName, ObjectName); 
        
        for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList){
            DataTableColumns datacolumns = new DataTableColumns(String.valueOf(fieldSetMemberObj.getLabel()) , 
                                                                String.valueOf(fieldSetMemberObj.getFieldPath()), 
                                                                (String.valueOf(fieldSetMemberObj.getType()).toLowerCase() == 'datetime') || (String.valueOf(fieldSetMemberObj.getType()).toLowerCase() == 'date') ? 'string' : String.valueOf(fieldSetMemberObj.getType()).toLowerCase());
            lstDataColumns.add(datacolumns);
        }
        return lstDataColumns;
    }
    /**
    * @description  Get the fields of a FieldSet of a certain object 
    * 
    * @param    fieldSetName            API Name of fieldSet
    * @param    ObjectName              API Name of the Object
    * 
    * @return   Return a list of fields of type Schema.FieldSetMember
    */
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        
        return fieldSetObj.getFields(); 
    }  
    /*
    public class RecordWrapper{
        @AuraEnabled public sObject record {get;set;}
        @AuraEnabled public Boolean selected {get;set;}
        //@AuraEnabled public Boolean activeValue {get;set;}
    }*/
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
}