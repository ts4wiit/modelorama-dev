@isTest
private class ISSM_Kpi_tst {
	
	@testSetup
	private static void testSetup(){
		// create Accounts
		List<Account> accounts_lst = new List<Account>{	};
		for(Integer i = 0;i<10;i++){
			accounts_lst.add(new Account(Name='Test Account '+i,ISSM_MainContactA__c=true,ONTAP__SAPCustomerId__c = i<10 ? '000000000' + i : '00000000' + i));
		}
		insert accounts_lst;
	}

	@isTest
	static void testConstructor(){
		ISSM_Kpi_cls issmKpi = new ISSM_Kpi_cls();

	}


	@isTest
	static void itShould_CreateAnObject(){
		
		Map<String, Account> accouts_map = new Map<String, Account>();
		for(Account acc : getAccounts()){
			accouts_map.put(acc.ONTAP__SAPCustomerId__c, acc);
		}
		system.debug( '\n\n  ****** accouts_map = ' + accouts_map+'\n\n' );
		ISSM_Kpi_cls issmKpi = new ISSM_Kpi_cls();
		issmKpi.createObject(accouts_map);
		issmKpi = new ISSM_Kpi_cls(null, accouts_map);
		issmKpi.createObjectDelete(accouts_map);
		issmKpi.save();
		issmKpi.deleteObjectsFinish();
		issmKpi.deleteObjectsFinishExt();

		
	}
	

	private static List<Account> getAccounts(){return [SELECT Name,ONTAP__SAPCustomerId__c,Id FROM Account];}
	
}