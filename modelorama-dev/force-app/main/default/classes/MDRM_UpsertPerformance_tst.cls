/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Test class for the Performance Upsert. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Test class for the Performance Upsert. 
==============================================================================================================================
*********************************************************************************************************************************/
@istest
public class MDRM_UpsertPerformance_tst {
    static testmethod void createKPISwithProducts(){

         Id rtID = Schema.SObjectType.account.getRecordTypeInfosByName().get('Expansor').getRecordTypeId();
        account acc = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        acc.recordtypeid =rtID;
        acc.Z019__c = '33';
        insert acc;
        account acc2 = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        acc2.recordtypeid =rtID;
        insert acc2;
        datetime today = datetime.now(); 
       decimal themonth = today.month();
        //string thismonth=string.valueof(themonth);
        MDRM_Modelorama_Performance__c perftoupdate = new MDRM_Modelorama_Performance__c(MDRM_Store__c = acc2.id, 
                                                                                         MDRM_Actual_Volume__c = 0,
                                                                                        MDRM_Month__c = themonth);
        string cerveza ='Cerveza';
        Id rtIDProd = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByName().get('Products').getRecordTypeId();
        
        ONTAP__Product__c Product = new ONTAP__Product__c(ONTAP__ProductId__c = '3001123', ISSM_Factor_for_hectoliters__c = 0.54, 
                                                         ISSM_Sector__c =Cerveza, recordtypeid = rtIDProd);
        insert product;
        ONTAP__KPI__c KPI = new ONTAP__KPI__c(ONTAP__Actual__c=5, ONTAP__Kpi_id__c=3001123, 
                                              ONTAP__Account_id__c=acc.id, ONTAP__Categorie__c = 'Volume');
        insert KPI;
		KPI.ONTAP__Actual__c = 120;
        UPDATE kpi;
        
        ONTAP__KPI__c KPI2 = new ONTAP__KPI__c(ONTAP__Kpi_id__c=3001125, ONTAP__Actual__c =10, 
                                              ONTAP__Account_id__c=acc2.id, ONTAP__Categorie__c = 'Volume');
        insert KPI2;
        KPI2.ONTAP__Actual__c = 25;
        
        update kpi2; 
		ONTAP__KPI__c KPI3 = new ONTAP__KPI__c(ONTAP__Kpi_id__c=3001123, ONTAP__Account_id__c=acc2.id, ONTAP__Categorie__c = 'Volume');
        insert KPI3;
        
        KPI3.ONTAP__Actual__c =NULL;
        update KPI3;
		
        
        
        test.startTest();
            Id batchJobId = Database.executeBatch(new MDRM_UpsertPerformance(), 200);
        system.debug('EXTERNAL ID '+ perftoupdate.MDRM_External_ID__c);
        System.debug('VOLUME '+perftoupdate.MDRM_Actual_Volume__c);
        test.stopTest();
            }
}