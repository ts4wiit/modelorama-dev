/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_CreateCampaingTelecollectionBch_tst {
    public static  List<ISSM_AppSetting_cs__c> AppSetting;
    public static  List<ISSM_TriggerFactory__c> AppTriggerFactory;
    
    static testMethod void myUnitTest() {
        
        // Cretae configuracion personalizada 
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo','ISSM_Telecolletion') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSettingTelecollection(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'),mapIdQueue.get('ISSM_Telecolletion'));
        AppTriggerFactory = ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        
        
        //Generamos campaña  Telecollection_Campaign__c
        Telecollection_Campaign__c objCreateCampaign = new Telecollection_Campaign__c();
        objCreateCampaign.Name = 'EjemploCampaignTest';
        objCreateCampaign.Start_Date__c = System.today();
        objCreateCampaign.End_Date__c =  System.today();
        objCreateCampaign.Active__c = true;
        insert objCreateCampaign;
        

        
        //Generamos RegionalSalesdivision 
        String RecordTypeRegional = [SELECT Id FROM RecordType WHERE DeveloperName='ISSM_RegionalSalesDivision' LIMIT 1].Id;        
        Account objCreateRegionalDiv  = new Account();
        objCreateRegionalDiv.Name ='RegionaSales';
        objCreateRegionalDiv.RecordTypeId = RecordTypeRegional;
        insert  objCreateRegionalDiv;
        
        //Generamos SalesOrg
        String RecordTypeSalesOrg = [SELECT Id FROM RecordType WHERE DeveloperName='SalesOrg' LIMIT 1].Id;
        Account objTypeSalesOrg  = new Account();
        objTypeSalesOrg.Name ='SalesOrg';
        objTypeSalesOrg.ISSM_ParentAccount__c = objCreateRegionalDiv.Id;
        objTypeSalesOrg.RecordTypeId =RecordTypeSalesOrg;
         insert objTypeSalesOrg;
        
        //Generamos SalesOffice
        String RecordTypeSalesOffice =  [SELECT Id FROM RecordType WHERE DeveloperName='SalesOffice' LIMIT 1].Id;        
        Account ObjTypeSalesOffice  = new Account();
        ObjTypeSalesOffice.Name ='SalesOffcie';
        ObjTypeSalesOffice.ISSM_ParentAccount__c = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.ParentId = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.RecordTypeId = RecordTypeSalesOffice;
        insert ObjTypeSalesOffice;
        
        //Generamos cuenta para los filtros
        String RecordTypeAccountId = [SELECT Id FROM RecordType WHERE DeveloperName='Account' LIMIT 1].Id;       
        Account objCreateAccount  = new Account();
        objCreateAccount.Name ='EjemploCuenta';
        objCreateAccount.ONTAP__Classification__c ='Botella Abierta';
        objCreateAccount.ISSM_RegionalSalesDivision__c =objCreateRegionalDiv.Id;
        objCreateAccount.ISSM_SalesOffice__c =ObjTypeSalesOffice.Id;
        objCreateAccount.ISSM_SalesOrg__c =objTypeSalesOrg.Id;
        objCreateAccount.RecordTypeId = RecordTypeAccountId;
        insert objCreateAccount;
        
        //Creamos ONTAP__OpenItem__c 
        ISSM_OpenItemB__c objOpenItem = new ISSM_OpenItemB__c();
        objOpenItem.ISSM_Account__c = objCreateAccount.Id;
        objOpenItem.ISSM_DueDate__c = System.today()-35;
        objOpenItem.ISSM_Amounts__c =15000;
        insert objOpenItem;
        
        
        //Generamos ISSM_TelecolletionOrder__c
        List<ISSM_TelecolletionOrder__c> lstTelecolletionOrder = new List<ISSM_TelecolletionOrder__c>();
        ISSM_TelecolletionOrder__c objTelecolletionOrder = new ISSM_TelecolletionOrder__c();
        objTelecolletionOrder.ISSM_POC__c = objCreateAccount.Id;
        objTelecolletionOrder.AccountName__c= 'EjemploCuenta';
        objTelecolletionOrder.CampaingName__c= 'EjemploCampaignTest';
        objTelecolletionOrder.ISSM_OutstandingBalanceAmount__c=100;
        objTelecolletionOrder.ISSM_MaximunDaysOpenItem__c= 75;
        objTelecolletionOrder.ISSM_LastContactDate__c= System.today();
        objTelecolletionOrder.ISSM_LastPaymentPlanDate__c= System.today();
        objTelecolletionOrder.ISSM_LastPromisePaymentDate__c=System.today();
        objTelecolletionOrder.Telecollection_Campaign__c = objCreateCampaign.Id;
        objTelecolletionOrder.OpenItemsId__c= objOpenItem.Id;
        lstTelecolletionOrder.add(objTelecolletionOrder);
        insert lstTelecolletionOrder;
        
        Database.BatchableContext dbBC;
        Test.startTest();
            ISSM_CreateCampaingTelecollection_bch objCreateCampaingTelecollection = new ISSM_CreateCampaingTelecollection_bch(objCreateCampaign.Id,'');
            objCreateCampaingTelecollection.start(dbBC);
            objCreateCampaingTelecollection.execute(dbBC,lstTelecolletionOrder);
        Test.stopTest();
        
    }
}