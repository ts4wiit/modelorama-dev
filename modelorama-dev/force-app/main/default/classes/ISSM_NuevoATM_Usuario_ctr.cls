/**************************************************************************************
Nombre de la Clase Appex: ISSM_NuevoATM_Usuario_ctr
Versión : 1.0
Fecha de Creación : 09 Abril 2018
Funcionalidad : Clase para Administrar la Asignación de Nuevos Usuarios en Equipos de Cuentas
Clase de Prueba: ISSM_NuevoATM_Usuario_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        09 - Abril - 2018      Versión Original
*************************************************************************************/
public class ISSM_NuevoATM_Usuario_ctr {

    /***** VARIABLES *****************************************************************/
    private ApexPages.StandardController controller { get; set; }
    private User user { get; set; }

    public ISSM_NuevoATM_Usuario_ctr(ApexPages.StandardController controller) {
        try {
            // Inicializamos el controller
            this.controller = controller;
            // Cargamos el registro actual
            this.user = (User)controller.getRecord();

        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
}