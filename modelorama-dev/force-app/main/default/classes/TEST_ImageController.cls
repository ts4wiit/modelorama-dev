public class TEST_ImageController {
    
    @AuraEnabled 
    public static List<contentDocument> getImages(){
        
		List<contentDocument> img = [SELECT Id, Title, FileType, CreatedBy.Name, ContentSize 
                                     FROM contentDocument 
                                     WHERE FileType ='PNG' 
                                     OR FileType ='JPG'
                                     LIMIT 21];
        
        return img;
    }
}