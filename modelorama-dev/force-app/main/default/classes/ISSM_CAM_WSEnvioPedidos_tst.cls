/**************************************************************************************
Nombre de la clase: ISSM_CAM_WSEnvioPedidos_tst
Versión : 1.0
Fecha de Creación : 20 Julio 2018
Funcionalidad : Apex test class to send orders to Mulesoft and SAP instead Case Force
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Julio - 2018      Versión Original
* Daniel Rosales                                Class 
*************************************************************************************/
@isTest
public class ISSM_CAM_WSEnvioPedidos_tst {
@testSetup
    static void setup(){
        ISSM_PriceEngineConfigWS__c wsExistencia = new ISSM_PriceEngineConfigWS__c( Name='ConfigCAMWSExistencia', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/disponibilidad',
        ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsExistencia;
        
        ISSM_PriceEngineConfigWS__c wsPedido = new ISSM_PriceEngineConfigWS__c( Name='ConfigCAMWSEnvioPedidos', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/asignacion',
        ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsPedido;
        
        ISSM_PriceEngineConfigWS__c wsEstatus = new ISSM_PriceEngineConfigWS__c(
        Name='ConfigCAMWSEstatus', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/estado',
        ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsEstatus;
        
        //==============================User
        user usrapp1 = new User (username='omar.cruz@gmodelo.com.mx.test', lastname='approval1',email='omar.cruz@gmodelo.com.mx', Alias='app1', 
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp1;
        user usrapp2 = new User (username='rogelio.olvera@gmodelo.com.mx.test',lastname='approval2',email='rogelio.olvera@gmodelo.com.mx', Alias='app2',  
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp2;
        user usrapp3 = new User (username='jefe.frio@gmodelo.com.mx.test',lastname='approval3',email='jefe.frio@gmodelo.com.mx', Alias='app3',  
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp3;
        user usrapp4 = new User (username='sup.trade@gmodelo.com.mx.test',lastname='approval4',email='sup.trad@gmodelo.com.mx', Alias='app4',  
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp4;
        user usrGteOp = new User (username='gte.op@gmodelo.com.mx.test',lastname='gteOp',email='gte.op@gmodelo.com.mx', Alias='gteOp',  
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrGteOp;
        user usrGteAdm = new User (username='gte.adm@gmodelo.com.mx.test',lastname='gteAdm',email='gte.adm@gmodelo.com.mx', Alias='gteAdm',  
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrGteAdm;
        //==============================Recordtype
        map<String, Id> recTypeIds = new map<String, Id>();
        for (recordtype rc : [SELECT Id, DeveloperName FROM RecordType 
                              WHERE DeveloperName IN ('ISSM_RegionalSalesDivision','SalesOrg','SalesOffice', 'Account', 'CAM_Asset_Assignation', 'CAM_Asset_UnAssignation')])
            recTypeIds.put(rc.developername, rc.id);
        //===============================Account
        Account accDRV = new Account(
            Name='DRV', ISSM_RegionalSalesDivision__c=null, ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='', ontap__sapcustomerid__c='DRV', 
            RecordTypeId=recTypeIds.get('ISSM_RegionalSalesDivision') );
        insert accDRV;
        
        Account accSalesOrg  = 
            new Account(
                Name='SalesOrg',ISSM_RegionalSalesDivision__c=accDRV.id, ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',ontap__sapcustomerid__c='ORG', ontap__salesogid__c='ORG',
                RecordTypeId=recTypeIds.get('SalesOrg'), parentId=accDRV.Id);
        insert accSalesOrg;
        
        Account accSalesOff  = 
            new Account(
                Name='SalesOff',ISSM_RegionalSalesDivision__c=accDRV.id, ISSM_SalesOrg__c=accSalesOrg.id, ontap__sapcustomerid__c='OFF', ontap__salesogid__c='ORG',ONTAP__SalesOffId__c='OFF',
                ISSM_BillingManager__c=UserInfo.getUserId(),ONTAP__SalesOfficeDescription__c='OFF', RecordTypeId=recTypeIds.get('SalesOffice'), parentId=accSalesOrg.id);
        insert accSalesOff;
        
        Account acc = 
            new Account(
                Name = 'ACCTEST1',  ISSM_RegionalSalesDivision__c=accDRV.Id, ISSM_SalesOrg__c=accSalesOrg.Id, ISSM_SalesOffice__c = accSalesOff.Id, parentid=accSalesOff.Id,
                ontap__sapcustomerid__c='TEST', ontap__salesoffid__c='OFF', ontap__salesogid__c='ORG', ONTAP__SAP_Number__c='0100ACTEST',
                ISSM_Is_Blacklisted__C=False, recordtypeid=recTypeIds.get('Account'), OwnerId=usrapp1.id, ONTAP__Email__c='test@est.com' );
        insert acc; 
        //===================================Account Team Member
        AccountTeamMember atmsalesorg =  new AccountTeamMember( AccountId= accSalesOrg.id,  userid=usrapp1.id,  TeamMemberRole='Gerente de Trade Marketing');
        insert atmsalesorg;
        AccountTeamMember atmdrv =  new AccountTeamMember( AccountId=AccDRV.id,  userid=usrapp2.id, TeamMemberRole='Gerente Regional de Trade Marketing');
        insert atmdrv;
        AccountTeamMember atmsalesOffJF =  new AccountTeamMember( AccountId= accSalesOff.id,  userid=usrapp3.id,  TeamMemberRole='Supervisor de Mobiliario');
        insert atmsalesOffJF;
        AccountTeamMember atmsalesOff =  new AccountTeamMember( AccountId= accSalesOff.id,  userid=usrapp4.id,  TeamMemberRole='Supervisor de Trade Marketing');
        insert atmsalesOff;

        AccountTeamMember atmGteOp =  new AccountTeamMember( AccountId=AccDRV.id,  userid=usrgteOp.id, TeamMemberRole='Gerente de Operaciones');
        insert atmGteOp;
        AccountTeamMember atmGteAdm =  new AccountTeamMember( AccountId=AccDRV.id,  userid=usrgteAdm.id, TeamMemberRole='Gerente Administrativo');
        insert atmGteAdm;
        //=================================Inventory
        ISSM_Asset_Inventory__c inv =  
            new ISSM_Asset_Inventory__c( Name = 'VNTEST',  ISSM_Stock_SFDC__c = 20,  ISSM_Material_Number__c = '8000300', ISSM_Centre__c='OFF');
        insert inv;
        //=================================Asset
        ISSM_Asset__c assetAssign = new ISSM_Asset__c(ISSM_CutOver__c=true,name='Free_Use', ISSM_Centre__c='OFF',Equipment_Number__c='000000000FREE_USE_', ISSM_Serial_Number__c='FREE_USE_', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Free Use');
        insert assetAssign;
        
        ISSM_Asset__c assetUnassign = new ISSM_Asset__c(ISSM_CutOver__c=true,name='Unassignation', Equipment_Number__c='0000UNASSIGNATION_', ISSM_Serial_Number__c='UNASSIGNATION_', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Free Use');
        insert assetUnassign;
        //=============================== Typification Matrix
        ISSM_TypificationMatrix__c TypMat = new ISSM_TypificationMatrix__c(ISSM_UniqueIdentifier__c='TEST', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo');
        insert TypMat;
        ISSM_TypificationMatrix__c TypMatRetiro = new ISSM_TypificationMatrix__c(ISSM_UniqueIdentifier__c='TESTRETIRO', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Retiro de equipo');
        insert TypMatRetiro;
        
        //=============================== CASE FORCE 
        id recTypIdTestAssign = recTypeIds.get('CAM_Asset_Assignation');
        id recTypIdTestUnAssign = recTypeIds.get('CAM_Asset_UnAssignation');
        
        ONTAP__Case_Force__c cfUnassign =  
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Retiro', ISSM_Asset_CAM__c = assetUnassign.id,
                ONTAP__Account__c = acc.id,  ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Retiro de equipo', 
                ISSM_ApprovalStatus__c='Approved', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7, recordtypeid=recTypIdTestUnAssign );
        insert cfUnassign;
        
        ONTAP__Case_Force__c cfAssign =  
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Solicitud', ISSM_Asset_Inventory__c = inv.id,
                ONTAP__Account__c = acc.id,  ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Entrega de equipo', 
                ISSM_ApprovalStatus__c='Approved', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7, recordtypeid=recTypIdTestAssign );
        insert cfAssign;
        
        //crear un caseforce con caseforceasset relacionado //asignacion
        //crear un caseforce con asset relacionado //desasignación
        ISSM_Case_Force_Asset__c test_caseforceasset = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = cfAssign.id, ISSM_Asset__c = assetAssign.id);
        insert test_caseforceasset;
    }
    static testMethod void runTestCAMWSEnvioPedidos(){
        ONTAP__Case_Force__C testCFAssign=[SELECT id, ONTAP__Account__c, ISSM_CAM_Coolers_by_Case__c, ISSM_Delivery_Date_CAM__c, recordtypeid FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Solicitud'];
        system.debug(testCFAssign);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCFAssign);
        ISSM_CAM_WSEnvioPedidos_ctr testCAMWSEnvioPedido = new ISSM_CAM_WSEnvioPedidos_ctr(sc);
        testCAMWSEnvioPedido.inicializar();        
    }
    static testMethod void runTestCAMWSEnvioRetiro(){
        ONTAP__Case_Force__C testCFUnAssign=[SELECT id, ONTAP__Account__c, ISSM_CAM_Coolers_by_Case__c, ISSM_Delivery_Date_CAM__c, recordtypeid FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Retiro'];
        system.debug(testCFUnAssign);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCFUnAssign);
        ISSM_CAM_WSEnvioPedidos_ctr testCAMWSEnvioPedido = new ISSM_CAM_WSEnvioPedidos_ctr(sc);
        testCAMWSEnvioPedido.inicializar();        
    }
}