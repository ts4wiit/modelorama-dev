/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public class ISSM_CreateCaseForceCInCaseCoHandler_cls {
    /**
    * Descripcion :  Realiza el insert del CaseForceComment en CaseComment
    * @param  none
    * @return none  
    **/
    public void CreateCaseForceCommentInCaseComment(List <ONTAP__Case_Force_Comment__c> lstDataCaseForceCommentNew ) {
        System.debug('#####ISSM_CreateCaseForceCInCaseCoHandler_cls -- CreateCaseForceCommentInCaseComment' );
        List<id> listID = new  List<id>();
        List<id> listIDCaseComment = new  List<id>();
        for(ONTAP__Case_Force_Comment__c otc : lstDataCaseForceCommentNew ){
            listID.add(otc.ONTAP__Case_Force__c);
            listIDCaseComment.add(otc.Id);
        }
        ISSM_CreateCaseForceCInCaseCoHandler_cls.CreateCaseFInCaseStandard(listID,listIDCaseComment);   
    } 
 
    @future 
    Static void CreateCaseFInCaseStandard(List<ID> lstDataCaseForceCommentNew, List<ID> lstIdCaseForceComment) {
    	ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        System.debug('#####FUTURO ISSM_CreateCaseForceCInCaseCoHandler_cl--CreateCaseFInCaseStandard ');
        List<Case> lstCaseObtainId =  objCSQuerys.QueryCaseLst(lstDataCaseForceCommentNew);                                         
        ISSM_CreateCaseForceCInCaseCoHandler_cls.InserCaseForceCommnetInCaseFuturo(lstCaseObtainId , lstIdCaseForceComment);
    }
    
    Static void InserCaseForceCommnetInCaseFuturo(List <Case> lstDataCaseForceCommentNew , List<Id> LstIdsCaseComment ) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        System.debug('#####ISSM_CreateCaseForceCInCaseCoHandler_cl--InserCaseForceCommnetInCaseFuturo ');   
        CaseComment objCaseComment = new CaseComment();
        List<ONTAP__Case_Force_Comment__c> lstqueryCaseComment = objCSQuerys.QueryCaseForceComment(LstIdsCaseComment);
        
        for(ONTAP__Case_Force_Comment__c objDataCaseForceComment : lstqueryCaseComment ){
            objCaseComment.CommentBody =  objDataCaseForceComment.ONTAP__Comment__c;
            objCaseComment.ParentId = lstDataCaseForceCommentNew[0].id;   
        }
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_ONTAPCaseForceCommentTrigger); 
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseCommentTrigger); 
        insert objCaseComment;                                     
    }
}