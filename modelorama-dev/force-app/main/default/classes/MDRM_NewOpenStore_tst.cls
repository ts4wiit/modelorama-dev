/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Test class for the upsert for Opening Goals. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Test class for the upsert for Opening Goals. 
==============================================================================================================================
*********************************************************************************************************************************/
@istest
public class MDRM_NewOpenStore_tst {

    static testmethod void createOpeningGoal(){

        Id rtID = Schema.SObjectType.account.getRecordTypeInfosByName().get('Expansor').getRecordTypeId();
        account acc = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        acc.RecordTypeId =rtID;
        insert acc;

        test.startTest();
       mdrm_newopenstore.UpsertOpeningGoals(New id[]{acc.id});
    test.stopTest();
}
}