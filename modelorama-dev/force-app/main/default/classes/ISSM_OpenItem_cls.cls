/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice Open items

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OpenItem_cls implements ISSM_ObjectInterface_cls{

	public List<ONTAP__OpenItem__c> openItems;
	List<salesforce_ontap_openitem_c__x> openItemsExtDelete;

	/**
	 * Class constructor		                             
	 */
	public ISSM_OpenItem_cls() {
		
	}

	/**
	 * Create Objects to sincronice		                             
	 * Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObject(Map<String, Account> mapAccount){
		openItems = getList(null, mapAccount);
	}

	/**
	 * Create objects to delete		                             
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObjectDelete(Map<String, Account> mapAccount){
		openItemsExtDelete = ISSM_OpenItemExtern_cls.getList(mapAccount.keySet(), true);
	}

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public List<sObject> getList(List<sObject> accountOpenItems, Map<String, Account> mapAccount){
		List<ONTAP__OpenItem__c> openItemsReturn;
		ONTAP__OpenItem__c openItem;
		//Set<String> accountSap =mapAccount.keySet() ;

		// WP 20170930: FIX to filter accounts that don't have active Tours between the last 7 days and in 7 days in future to avoid impact to iRep project
        
        Set<String> accountSap = new Set<String>();
        for(ONTAP__Tour_Visit__c activeTourVisit : [SELECT ONTAP__Account__r.ONTAP__SAPCustomerId__c 
													FROM ONTAP__Tour_Visit__c 
													WHERE ONTAP__Account__r.ONTAP__SAPCustomerId__c in:mapAccount.keySet()
													AND (ONTAP__Tour__r.ONTAP__IsActive__c = TRUE OR 
														ONTAP__Tour__r.ONTAP__TourStatus__c = 'Downloading')
													AND (ONTAP__Tour__r.ONTAP__TourDate__c >= LAST_N_DAYS:7
													AND ONTAP__Tour__r.ONTAP__TourDate__c <= NEXT_N_DAYS:7)
													]){
               accountSap.add(activeTourVisit.ONTAP__Account__r.ONTAP__SAPCustomerId__c);
         }
        
        for(String idAccount : accountSap){
        	mapAccount.remove(idAccount);
        }
		// accountSap = mapAccount.keySet(); 

        // WP 20170930: FIX ENDING
		// =================================================================

		// =================================================================
		// WP 20171003: FIX Filter accounts that are not part of the pending calls or visits on current day
		// Customers with pending calls
		for(ONCALL__Call__c pendingCall : [SELECT Id, OwnerId, Name, RecordTypeId, ONCALL__Call_Status__c, ONCALL__Date__c, ONCALL__POC__c, ONCALL__POC__r.ONTAP__SAPCustomerId__c 
													FROM ONCALL__Call__c
													WHERE ONCALL__Call_Status__c = 'Pending to call' 
													AND ONCALL__Date__c = TODAY
													AND ONCALL__POC__r.ONTAP__SAPCustomerId__c IN :mapAccount.keySet()
													]){
               accountSap.add(pendingCall.ONCALL__POC__r.ONTAP__SAPCustomerId__c);
         }

		 // As the Event object doesn't have access to SAP Customer Id field on Account object, it is necessary to get the Account's record id
		 map<id, string> accountIdBySAPCustomerId = new map<id, string>();
		 for(Account acc : mapAccount.values()){
			accountIdBySAPCustomerId.put(acc.Id, acc.ONTAP__SAPCustomerId__c);
		 }
		
		// Customers with pending BDR Visits
		String strRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
		for(Event pendingVisit : [SELECT Id, OwnerId, WhatId, ActivityDate 
									FROM Event
									WHERE ONTAP__Estado_de_visita__c = :Label.ISSM_Pending //'Pending' 
									AND ActivityDate = TODAY
									AND Subject = :Label.ISSM_Visit_Subject //'Visit'
									AND RecordTypeId = :strRecordTypeId // ('0122F0000008Xd2QAE') 
									AND WhatId IN :accountIdBySAPCustomerId.keySet()
								 ]){
               accountSap.add(accountIdBySAPCustomerId.get(pendingVisit.WhatId));
         }

		 // If customer was not found in the list of customers with pending calls or BDR visits for the day, then it must be remove from the set of accounts to load 360 data
		for(String strAccountSAPCustId : mapAccount.keySet()){
        	if(!accountSap.contains(strAccountSAPCustId))
				mapAccount.remove(strAccountSAPCustId);
        }

		// WP 20171003: END OF FIX
		// =================================================================
		
		openItemsReturn = new List<ONTAP__OpenItem__c>();
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByName().get('ONTAP_Compact').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByName().get('ONTAP_Compact').getRecordTypeId();
		}
		if(test.isRunningTest()){
			for(Integer i = 0;i<15;i++){
				salesforce_ontap_openitem_c__x openItemExt = new salesforce_ontap_openitem_c__x(
					ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',
					isdeleted__c=false, 
					issm_sapcustomerid_c__c='000000000'+i,
					systemmodstamp__c = System.today());

				openItemsReturn.add(getOpenItem(mapAccount , openItemExt,devRecordTypeId));
			}
		} else{
			for(SObject openItemExt: [SELECT systemmodstamp__c, issm_sapcustomerid_c__c,
			ontap_sapidassigndocfiscalyrkey_c__c, issm_sap_number_c__c, ontap_amount_c__c, 
			ontap_assignment_c__c, ontap_divisionid_c__c, ontap_documentid_c__c, ontap_documenttype_c__c,
			ontap_duedate_c__c, ontap_fiscalyear_c__c, ontap_start_date_c__c,issm_investmentcontnumber_c__c FROM salesforce_ontap_openitem_c__x 
			where issm_sapcustomerid_c__c in :accountSap and isdeleted__c = false and systemmodstamp__c >= YESTERDAY]){		
				openItemsReturn.add(getOpenItem(mapAccount , openItemExt,devRecordTypeId));
			}
		}
		System.debug('### ISSM_OpenItem_cls ListSize: ' + openItemsReturn.size());
		return openItemsReturn;
	}


	/**
	 * Get getOpenItem record		                             
	 * @param  Map<String, Account> mapAccount
	 * @param  sObject accountAssetExt
	 * @param  Id devRecordTypeId
	 */
	private ONTAP__OpenItem__c getOpenItem(Map<String, Account> mapAccount , sObject openItemExt, Id devRecordTypeId){
		ONTAP__OpenItem__c openItem;
		openItem = new ONTAP__OpenItem__c();
		try{
			openItem.ONTAP__Account__c = mapAccount.get(((salesforce_ontap_openitem_c__x)openItemExt).issm_sapcustomerid_c__c).Id;
			openItem.ONTAP__SAPCustomerId__c = ((salesforce_ontap_openitem_c__x)openItemExt).issm_sap_number_c__c;
			openItem.ONTAP__Amount__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_amount_c__c;
			openItem.ONTAP__Assignment__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_assignment_c__c;
			openItem.ONTAP__DivisionId__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_divisionid_c__c;		
			openItem.ONTAP__DocumentId__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_documentid_c__c;
			openItem.ONTAP__DocumentType__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_documenttype_c__c;
			openItem.ONTAP__DueDate__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_duedate_c__c;
			openItem.ONTAP__FiscalYear__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_fiscalyear_c__c;
			openItem.ONTAP__SAPIdAssignDocFiscalYrKey__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_sapidassigndocfiscalyrkey_c__c;
			openItem.ONTAP__Start_Date__c = ((salesforce_ontap_openitem_c__x)openItemExt).ontap_start_date_c__c;
			openItem.ISSM_InvestmentContNumber__c = ((salesforce_ontap_openitem_c__x)openItemExt).issm_investmentcontnumber_c__c;
			openItem.ISSM_IsMigrated__c = true; // WP 20171002: Flag to determine if the source of the record is this upload process
			openItem.RecordTypeId = devRecordTypeId;
		} catch(NullPointerException ne){
			system.debug( '\n\n  ****** ne = ' + ne+'\n\n' );
		}
		return openItem;
	}

	/**
	 * Save the result of the sincronization process		                             
	 */
	public void save(){
		System.debug('### ISSM_OpenItem_cls openitems: ' +openItems);
		if(openItems != null){
			try {
	        	upsert openItems ONTAP__SAPIdAssignDocFiscalYrKey__c;
	        	System.debug('### ISSM_OpenItem_cls openitems: ' +openItems);
	    	} catch (DmlException e) {
	        	System.debug(e.getMessage());
	    	}

		}
	}

	/**
	 * Delete internal records that has the flag isdeleted on true		                             
	 */
	public Boolean deleteObjectsFinish(){
		if(openItemsExtDelete != null){
			System.debug('### ISSM_OpenItem_cls openItemsExtDelete deleteObjectsFinish: ' + openItemsExtDelete.size());
			Set<String> openItemsSet = ISSM_OpenItemExtern_cls.getSetExternalId(openItemsExtDelete);
			List<ONTAP__OpenItem__c> toDeleteOpenItem_lst = getOpenItems(openItemsSet);
			Boolean success = true;
			try{
				System.debug('### ISSM_OpenItem_cls: toDeleteOpenItem_lst size ('+toDeleteOpenItem_lst.size()+')'); 
				System.debug('### ISSM_OpenItem_cls: toDeleteOpenItem_lst ('+toDeleteOpenItem_lst+')'); 
				Database.DeleteResult[] drList = Database.delete(toDeleteOpenItem_lst);
				system.debug( '\n\n  ****** drList = ' + drList+'\n\n' );
				for(Database.DeleteResult dr : drList) {
				    success &= dr.isSuccess();
				}
				system.debug( '\n\n  ****** Borrados = ' + +'\n\n' );
				return success;
			} catch(Exception e){
				System.debug('### ISSM_OpenItem_cls: removeExistingOpenItems (DELETE FAILED)'); 
				return false;
			}
		}
		return false;
	}


	/**
	 * Get list of ONTAP__OpenItem__c based on ONTAP__SAPIdAssignDocFiscalYrKey__c		                             
	 * @param  Set<String> assetsSet
	 */
	private List<ONTAP__OpenItem__c> getOpenItems(Set<String> emptiesSet){
		return [SELECT Id FROM ONTAP__OpenItem__c WHERE ONTAP__SAPIdAssignDocFiscalYrKey__c IN : emptiesSet  ];
	}

	/**
	 * Delete external records that has the flag isdeleted on true		                             
	 */
	public void deleteObjectsFinishExt(){
		if(openItemsExtDelete != null){
			System.debug('### ISSM_OpenItem_cls openItemsExtDelete: ' + openItemsExtDelete.size());
			try{
				
				Database.DeleteResult[] drListExt = Database.deleteAsync(openItemsExtDelete);
				system.debug( '\n\n  ****** drListExt = ' + drListExt +'\n\n' );
			} catch(Exception e){
				System.debug('### ISSM_OpenItem_cls: removeExistingOpenItems (DELETE FAILED)'); 
				
			}
		}
	}

}