Public Class Agri_Attachments_CTRL{

    
  public List<ContentDocumentLink> lstOfAccounts {get; set;}

  public Agri_Attachments_CTRL (ApexPages.StandardController stdController) 
  {         
         lstOfAccounts = findAccounts(); 
  }

  private List<ContentDocumentLink> findAccounts() 
  {

    Agri_Contract__c Contract = [Select id,Agri_Farmer__c from Agri_Contract__c where id=:ApexPages.currentPage().getParameters().get('id')];
    system.debug('Contrato' + Contract);
    Account AgriAccount = [Select id,ONTAP__SAP_Number__c from Account where Id=:Contract.Agri_Farmer__c];
    system.debug('Cuenta' + AgriAccount);
    Agri_Supplier__c Supplier = [Select id, name, Agri_SAP_Number__c from Agri_Supplier__c where Agri_SAP_Number__c=:AgriAccount.ONTAP__SAP_Number__c Limit 1];
    
    return [SELECT ContentDocumentId,ContentDocument.Title FROM ContentDocumentLink
            where LinkedEntityId=:Supplier.Id];         
  }
 

   public PageReference viewAttach()
    {
        ID lid=apexpages.currentpage().getparameters().get('ViewAttach');
        return new Pagereference ('/'+lid);
    }

}