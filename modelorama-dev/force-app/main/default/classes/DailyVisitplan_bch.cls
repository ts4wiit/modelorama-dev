/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Batch for generate visit plans

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       05-07-2017        Nelson Sáenz Leal (NSL)  Creation Class
****************************************************************************************************/
global with sharing class DailyVisitplan_bch implements Database.Batchable<sObject>, Database.Stateful {

    public map<Id, set<string>> mapIdAccServModels   =   new map<Id, set<string>>();
    public Set<String> setModels;
    global string  queryVisitPlanByAcc = '';
    //private list<VisitPlan__c> lstVisitPlan = new list<VisitPlan__c>();
    private List<VisitPlanWrapper> lstVisitPlan = new List<VisitPlanWrapper>();
    public set<id> setIdRoutes     =   new set<Id>();

    // Visit Plan Settings
    private VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
    private Integer VISIT_PERIOD 	  = Integer.valueOf(SyncHerokuParams__c.getAll().get('SyncToursEvents').VisitPeriodConfig__c);
    private Integer VISIT_PERIOD_CONF = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;

    private DateTime dtToday         =   DateTime.now().addDays(VISIT_PERIOD_CONF);
    private String strDtvisit        =   dtToday.format('yyyy-MM-dd');
    private Datetime dt              =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF), Time.newInstance(0, 0, 0, 0));
    private String dayOfWeek         =   dt.format('EEEE');
    private string strFielddayOfWeek = ''+ dayOfWeek + '__c';

    /**
        * @description Batch builder arms dynamic query to query visits and accounts configured for a given day
        * @author Nelson Sáenz Leal
    **/
    global DailyVisitplan_bch()
    {
        String fieldsQuery      =   'Id, Route__r.RouteManager__c, Route__r.OwnerId, Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOgId__c, Route__r.Id, ';
        fieldsQuery             +=  ' Route__r.ONTAP__SalesOffice__r.StartTime__c, Route__r.ONTAP__SalesOffice__r.EndTime__c, VisitPlanType__c,';
        fieldsQuery             +=  ' ( SELECT Id FROM Tours__r WHERE ONTAP__TourDate__c = '+ strDtvisit +' ) ';

        queryVisitPlanByAcc     +=  'SELECT '+fieldsQuery+ ' FROM VisitPlan__c ';
        queryVisitPlanByAcc     +=      ' WHERE ' + strFielddayOfWeek + ' = true';
        queryVisitPlanByAcc 	+=  	' AND ExecutionDate__c <= ' + strDtvisit;
        queryVisitPlanByAcc 	+=  	' AND EffectiveDate__c >= ' + strDtvisit;
        queryVisitPlanByAcc     +=      ' ORDER BY VisitPlanType__c DESC ';
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('queryVisitPlanByAcc===>>>'+queryVisitPlanByAcc);
        return Database.getQueryLocator(queryVisitPlanByAcc);
    }
    /**
        * @description Get all service models from the list of visits found
        * @author Nelson Sáenz Leal
    **/
    global void execute(Database.BatchableContext BC, list<VisitPlan__c> lstVisitPlanByAcc)
    {
        System.debug('lstVisitPlanByAcc  ==============>'+lstVisitPlanByAcc);
        if(lstVisitPlanByAcc != null && lstVisitPlanByAcc.size() > 0)
        {
            // Ejecutar consulta de Cuentas por Plan de Visita
            String strAccsByPlanQuery = 'SELECT Id, Account__c, EffectiveDate__c, Account__r.ONTAP__SAPCustomerId__c, ';
            strAccsByPlanQuery += 'ExecutionDate__c,Friday__c,Monday__c,PlanType__c,';
            strAccsByPlanQuery += 'Saturday__c,Sequence__c,Sunday__c,Thursday__c, ';
            strAccsByPlanQuery += 'Tuesday__c,VisitDays__c,VisitPlan__c, Account__r.ONTAP__SAP_Number__c,';
            strAccsByPlanQuery += 'Wednesday__c,WeeklyPeriod__c, LastVisitDate__c ';
            strAccsByPlanQuery += 'FROM AccountByVisitPlan__c';
            strAccsByPlanQuery +=  ' WHERE VisitPlan__c IN :lstVisitPlanByAcc';
            strAccsByPlanQuery +=  ' AND ' + strFielddayOfWeek + ' = true';
            strAccsByPlanQuery +=  ' AND Account__r.CustomerFlagLegal__c = false ';   // Cliente en Jurídico
            strAccsByPlanQuery +=  ' AND Account__r.OrderBlockFlag__c = false ';      // Bloqueo Pedido
            strAccsByPlanQuery +=  ' AND Account__r.DeletionRequestFlag__c = false '; // Petición de Borrado
            strAccsByPlanQuery +=  ' AND ExecutionDate__c <= ' + strDtvisit;
            strAccsByPlanQuery +=  ' AND EffectiveDate__c >= ' + strDtvisit;
            
            System.debug('\nstrAccsByPlanQuery => '+strAccsByPlanQuery);

            AccountByVisitPlan__c[] lstAccsByVisitPlan = Database.query(strAccsByPlanQuery);

            // Recorrer planes de visita y generar Wrappers (Contiene plan de visita y lista de cuentas)
            List<VisitPlanWrapper> lstPlanWrappers = new List<VisitPlanWrapper>();
            for(VisitPlan__c objVisitPlan : lstVisitPlanByAcc)
            {
                // Crear Wrapper de Plan de Visita
                VisitPlanWrapper visitPlanWrapper = new VisitPlanWrapper();
                visitPlanWrapper.objVisitPlan = objVisitPlan;
                visitPlanWrapper.lstAccounts = new List<AccountByVisitPlan__c>();

                // Agregar planes de visita a la cuenta correspondiente
                for (AccountByVisitPlan__c acc: lstAccsByVisitPlan) {
                    if (acc.VisitPlan__c == objVisitPlan.Id) {
                        visitPlanWrapper.lstAccounts.add(acc);
                    }
                }
                lstPlanWrappers.add(visitPlanWrapper);

                //System.debug(LoggingLevel.DEBUG, '*** objVisitPlan.AccountsByVisitPlan__r: ' + objVisitPlan.AccountsByVisitPlan__r);

                // Recorrer planes de Visita
                for(AccountByVisitPlan__c objAccByVisit: visitPlanWrapper.lstAccounts)
                {
                    setModels = mapIdAccServModels.get(objAccByVisit.Account__c);
                    if(setModels == null)
                    {
                        setModels = new Set<String>();
                        mapIdAccServModels.put(objAccByVisit.Account__c, setModels);
                    }
                    setModels.add(objVisitPlan.VisitPlanType__c);
                }
                setIdRoutes.add(objVisitPlan.Route__r.Id);
            }
            lstVisitPlan.addAll(lstPlanWrappers);
        }
        else
        {
            system.debug('<<GenerateDailyVisitplan_bch.execute>> La lista esta vacia.');
        }
    }
    /**
        * @description Call method to validate service models, generate visits and schedule the batch for the next day at the same time
        * @author Nelson Sáenz Leal
    **/
    global void finish(Database.BatchableContext BC)
    {
        System.debug(LoggingLevel.DEBUG, '*** VISIT_PLAN_WRAPPERS: ' + lstVisitPlan);
         DailyVisitplan_cls.generateDailyVisitplan(lstVisitPlan, mapIdAccServModels, setIdRoutes);
         programBatch();
    }
    /**
        * @description Method to automatically program the batch
        * @author Nelson Sáenz Leal
    **/
    public static void programBatch()
    {
        Datetime dtHoraActual       =   System.now();
        Datetime dtNextExecution    =   dtHoraActual.addDays(1);

        String strTime  =   dtNextExecution.second() +' ';
        strTime         +=  dtNextExecution.minute() +' ';
        strTime         +=  dtNextExecution.hour() +' ';
        strTime         +=  dtNextExecution.day() +' ';
        strTime         +=  dtNextExecution.month()+' ';
        strTime         +=  '? ';
        strTime         +=  dtNextExecution.year()+' ';

        String strDailyVisitplan    =   'DailyVisitplan_sch';
        try
        {
            list<CronTrigger> lstCronTrigger    =   [SELECT Id
                                                        FROM  CronTrigger
                                                        WHERE CronJobDetail.Name =: strDailyVisitplan];

            if(lstCronTrigger.size() == 0)
            {
                String jobId = System.schedule(strDailyVisitplan, strTime, new DailyVisitplan_sch() );
            }

        }catch(Exception e){ System.debug('\n ERROR ===========>>>>'+e.getMessage()); }
    }

}