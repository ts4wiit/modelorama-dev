public class VinculoTriggerHandler extends TriggerHandler
{
    
    private Map<Id, MDRM_Vinculo__c> newMap;
    private Map<Id, MDRM_Vinculo__c> oldMap;
    private List<MDRM_Vinculo__c> newList;
    private List<MDRM_Vinculo__c> oldList;
    
    public VinculoTriggerHandler()
    {
        this.newMap = (Map<Id, MDRM_Vinculo__c>) Trigger.newMap;
        this.oldMap = (Map<Id, MDRM_Vinculo__c>) Trigger.oldMap;
        this.newList = (List<MDRM_Vinculo__c>) Trigger.new;
        this.oldList = (List<MDRM_Vinculo__c>) Trigger.old;
    }
    
    public override void afterDelete()
    {
        DeactivateMDRM();
    }
    
    public void DeactivateMDRM()
    {
        List<ID> IDsMDRMtoDeactivate = new List<ID>();
        System.debug('Old '+oldList);
        for(MDRM_Vinculo__c DeletedVinculo : oldList)
        {
            IDsMDRMtoDeactivate.add(DeletedVinculo.MDRM_Expansor__c);
        }
        List<Account> MDRMtoDeactivate = new List<Account>();
        for(Account MDRM : [SELECT Id,MDRM_used__c FROM Account WHERE ID IN :IDsMDRMtoDeactivate])
        {
            MDRM.MDRM_used__c=FALSE;
            MDRMtoDeactivate.add(MDRM);
        }
        if(!MDRMtoDeactivate.isEmpty())
        {
        update MDRMtoDeactivate;
        }
    }
}