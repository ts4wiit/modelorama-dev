/****************************************************************************************************
    General Information
    -------------------
    author:     Andrés Morales
    email:      lmorales@avanxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Handler for Trigger under Attachment object

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                        Description          
    ------    --------        --------------------------    -----------------------------------------
    1.0       26/10/2017      Andrés Morales                Created
    ================================================================================================
****************************************************************************************************/

public class AttachmentTriggerHandler //extends TriggerHandler
{
    //@TestVisible static Boolean bypassTrigger = false;

    final List<attachment> newRecords;
    final Map<Id, attachment> oldMap;

    public AttachmentTriggerHandler(List<attachment> newRecords, Map<Id, attachment> oldMap)
    {
        this.newRecords = newRecords;
        this.oldMap = oldMap;
    }

    public void afterInsert()
    {
        AttachmentCountService_cls.AttachmentCountInsert(newRecords);
    }

    /*public void afterUpdate()
    {
        //if (bypassTrigger) return;
        AttachmentCountService_cls.AttachmentCount(oldMap);
    }*/

    public void afterDelete()
    {        
        AttachmentCountService_cls.AttachmentCount(oldMap);
    }

    public void afterUndelete()
    {
        AttachmentCountService_cls.AttachmentCountInsert(newRecords);
    }
}