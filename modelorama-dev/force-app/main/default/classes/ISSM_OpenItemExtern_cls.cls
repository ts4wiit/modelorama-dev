/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to get external open items

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OpenItemExtern_cls implements ISSM_ObjectExternInterface_cls{

	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setSapCustomerId set of sap customer id to request
	 * @param  Boolean isDeleted if the record must be deleted
	 */
	public static List<salesforce_ontap_openitem_c__x> getList(Set<String> setSapCustomerId, Boolean isDeleted){
		if(!isDeleted){
			if(test.isRunningTest()){
				List<salesforce_ontap_openitem_c__x> openItem_lst = new List<salesforce_ontap_openitem_c__x>();
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				return openItem_lst;
			} else {
				return [SELECT Id, ExternalId, account_ontap_sapcustomerid_c__c, 
				createddate__c, id__c, isdeleted__c, issm_investmentcontnumber_c__c, 
				issm_salesoffid_c__c, issm_sap_number_c__c, name__c, ontap_account_c__c, 
				ontap_amount_c__c, ontap_assignment_c__c, ontap_documentid_c__c, 
				ontap_divisionid_c__c, ontap_documenttype_c__c, ontap_duedate_c__c, 
				ontap_fiscalyear_c__c, ontap_start_date_c__c, systemmodstamp__c, issm_sapcustomerid_c__c,
				ontap_sapidassigndocfiscalyrkey_c__c FROM salesforce_ontap_openitem_c__x 
				where issm_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted and systemmodstamp__c >= YESTERDAY];
			}
		} else {
			if(test.isRunningTest()){
				List<salesforce_ontap_openitem_c__x> openItem_lst = new List<salesforce_ontap_openitem_c__x>();
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				openItem_lst.add(new salesforce_ontap_openitem_c__x(ontap_sapidassigndocfiscalyrkey_c__c = 'iwiofuioufd88373847sfhdjf',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				return openItem_lst;
			} else {
				return [SELECT Id, ExternalId, account_ontap_sapcustomerid_c__c, 
				createddate__c, id__c, isdeleted__c, issm_investmentcontnumber_c__c, 
				issm_salesoffid_c__c, issm_sap_number_c__c, name__c, ontap_account_c__c, 
				ontap_amount_c__c, ontap_assignment_c__c, ontap_documentid_c__c, 
				ontap_divisionid_c__c, ontap_documenttype_c__c, ontap_duedate_c__c, 
				ontap_fiscalyear_c__c, ontap_start_date_c__c, issm_sapcustomerid_c__c,
				ontap_sapidassigndocfiscalyrkey_c__c FROM salesforce_ontap_openitem_c__x 
				where issm_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted];
			}
		}	
	}

	/**
	 * Get a set of external id for the object		                             
	 * @param  List<sObject> sObjectList list of objects to return the set
	 */
	public static Set<String> getSetExternalId(List<sObject> sObjectList){
		Set<String> optenItemsSet = new Set<String>();
		salesforce_ontap_openitem_c__x accountOpenItems; 
		for(Integer i = 0; i < sObjectList.size(); i++){
				accountOpenItems = (salesforce_ontap_openitem_c__x)sObjectList.get(i);
		        optenItemsSet.add(accountOpenItems.ontap_sapidassigndocfiscalyrkey_c__c);
		}
		return optenItemsSet;
	}
}