/****************************************************************************************************
    Información general
    -------------------
    author:     Ernesto Gutiérrez
    email:      egutierrez@avanxo.com
    company:    Avanxo México
    Project:    OnCall Project
    Customer:   Grupo Modelo
 
    Description:
    Clase de prueba para el controlador ISSM_FinishOrder_cls

    Information about changes (versions)
    ================================================================================================
    Number    Dates               Author                       Description              Cobertura
    ------    --------            --------------------------   -----------------------------------------
    1.0       05-Septiembre-2018   Ernesto Gutiérrez           Creación de la Clase     91%
    ================================================================================================
****************************************************************************************************/

@isTest
public class ISSM_FinishOrder_tst { 

    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Order__c ObjOrder2{get;set;}
    static ONTAP__Product__c ObjProduct{get;set;}
    static ONTAP__Product__c ObjProduct2{get;set;}
    static ONTAP__Product__c ObjProduct3{get;set;}
    static Account ObjAccount{get;set;}//Cuentas
    static ONCALL__Call__c ObjCall{get;set;}//Llamadas
    static ONTAP__Order_Item__c ObjOI{get;set;}
    static ONTAP__Order_Item__c ObjOI2{get;set;}
    static ONTAP__Order_Item__c[] LstOI{get;set;}
    static ONTAP__Product__c[] LstProduc{get;set;}
    static ApexPages.StandardController sc{get;set;}
    static String IdOrderr{get;set;}
    static String IdAccnt{get;set;}
    static ONTAP__Order_Item__c[] lstIds{get;set;}
    static ISSM_Bonus__c ObjBns{get;set;}
    static ISSM_Bonus__c ObjBns2{get;set;}
    static ISSM_Bonus__c[] LstBns{get;set;}
    static EmptyBalanceB__c ObjEB{get;set;}
    static EmptyBalanceB__c[] LstObjEB2{get;set;}
    static EmptyBalanceB__c[] LstEB{get;set;}
    static ISSM_NewOrder_ctr.PriceCondition[] LstPC{get;set;}
    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
    static Map<Integer,String> ZTPMMap = new Map<Integer,String>();
    static ISSM_ProductByOrg__c ObjPBO{get;Set;}
    static Account ObjAccOrg{get;set;}//Cuentas
    public static ISSM_CreateOrder_ctr createOrder = new ISSM_CreateOrder_ctr();
    public static ISSM_PricingMotor_cls priceMotor = new ISSM_PricingMotor_cls();

    public static void StartValues(){
        //LstAccount = Test.loadData(Account.sObjectType, 'ISSM_AccountDataTst');

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = Label.ISSM_EPPricingEngine,
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        LstObjEB2 = new List<EmptyBalanceB__c>();
        LstOI = new List<ONTAP__Order_Item__c>();
        LstBns = new List<ISSM_Bonus__c>();
        LstEB = new List<EmptyBalanceB__c>();
        LstPC = new List<ISSM_NewOrder_ctr.PriceCondition>();

        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;

        ObjAccOrg = new Account(Name = 'Cuenta Org',
            ONTAP__Main_Phone__c= '12345678',
            ONTAP__SalesOgId__c = '3116',
            RecordTypeId = CTRSOQL.getRecordTypeId('Account','SalesOrg'));
        insert ObjAccOrg;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;

        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ISSM_Empties_Material__c    = ObjProduct2.Id,
            ISSM_BoxRack__c             = ObjProduct3.Id

        );
        insert ObjProduct;


        ObjPBO = new ISSM_ProductByOrg__c(
            ISSM_RelatedAccount__c      =   ObjAccOrg.Id,
            ISSM_AssociatedProduct__c   =   ObjProduct.Id,
            ISSM_Empties_Material__c    =   ObjProduct2.Id,
            ISSM_EmptyRack__c           =   ObjProduct3.Id,
            RecordTypeId                =   CTRSOQL.getRecordTypeId('ISSM_ProductByOrg__c','ISSM_OnCallProduct'));

        insert ObjPBO;


        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;


        ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder2;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_ComboNumber__c = '8768:8768'
        );
        insert ObjOI;


        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c = true
        );

        insert ObjOI2;

        ObjBns = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct.Id,
                            ISSM_SAPOrderNumber__c  = '300',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;


        ObjBns2 = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct2.Id,
                            ISSM_SAPOrderNumber__c  = '200',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns2;


        ObjEB = new EmptyBalanceB__c(
                            Account__c         = ObjAccount.Id,
                            SAPOrderNumber__c  = '1',
                            DueDate__c         = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c  = 2,
                            MaterialProduct__c = 'ENVASE',
                            EmptyBalanceKey__c = '321',
                            Product__c         = ObjProduct.Id,
                            Process__c         = false
        );

        LstObjEB2.add(new EmptyBalanceB__c(
                            Account__c = ObjAccount.Id,
                            SAPOrderNumber__c = '2',
                            DueDate__c = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c = 2,
                            MaterialProduct__c = 'CAJA',
                            EmptyBalanceKey__c = '4321',
                            Product__c = ObjProduct.Id,
                            Process__c = false
        ));
        insert ObjEB;

        LstBns.add(ObjBns);
        LstEB.add(ObjEB);
        LstOI.add(ObjOI);
        LstPC.add(new ISSM_NewOrder_ctr.PriceCondition(Label.ISSM_ZPVM,5.0,10));
        Apexpages.currentPage().getParameters().put(Label.ISSM_IdOrder, ObjOrder.Id);
        lstIds  = new List<ONTAP__Order_Item__c>();
        lstIds.add(ObjOI);
        ZTPMMap.put(3000005, '');

    }

    public static void StartValuesA(){
        //LstAccount = Test.loadData(Account.sObjectType, 'ISSM_AccountDataTst');

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = 'http://A',
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        LstObjEB2 = new List<EmptyBalanceB__c>();
        LstOI = new List<ONTAP__Order_Item__c>();
        LstBns = new List<ISSM_Bonus__c>();
        LstEB = new List<EmptyBalanceB__c>();
        LstPC = new List<ISSM_NewOrder_ctr.PriceCondition>();

        Id IdRecType = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId    = IdRecType,
            ISSM_ValidateOrder__c = false
        );
        insert ObjCall;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='2000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;


        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='3000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0,
            ISSM_Empties_Material__c = ObjProduct2.Id,
            ISSM_BoxRack__c = ObjProduct3.Id

        );
        insert ObjProduct;


        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;


        ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder2;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_ComboNumber__c = '9000000000'
        );
        insert ObjOI; 


        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c = true
        );

        insert ObjOI2;

        ObjBns = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct.Id,
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;


        ObjEB = new EmptyBalanceB__c(
                            Account__c      = ObjAccount.Id,
                            SAPOrderNumber__c   = '1',
                            DueDate__c      = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c   = 2,
                            MaterialProduct__c = 'TST',
                            EmptyBalanceKey__c = '321',
                            Product__c  = ObjProduct.Id
        );

        LstObjEB2.add(new EmptyBalanceB__c(
                            Account__c = ObjAccount.Id,
                            SAPOrderNumber__c = '2',
                            DueDate__c = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c = 2,
                            MaterialProduct__c = 'TST',
                            EmptyBalanceKey__c = '4321',
                            Product__c = ObjProduct.Id
        ));

        insert ObjEB;

        LstBns.add(ObjBns);
        LstEB.add(ObjEB);
        LstOI.add(ObjOI);
        LstPC.add(new ISSM_NewOrder_ctr.PriceCondition(Label.ISSM_ZPVM,5.0,10));
        Apexpages.currentPage().getParameters().put(Label.ISSM_IdOrder, ObjOrder.Id);
        lstIds  = new List<ONTAP__Order_Item__c>();
        lstIds.add(ObjOI);
        ZTPMMap.put(3000005, '');

    }


    public static void StartValuesB(){
        //LstAccount = Test.loadData(Account.sObjectType, 'ISSM_AccountDataTst');

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = 'http://B',
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        LstObjEB2 = new List<EmptyBalanceB__c>();
        LstOI = new List<ONTAP__Order_Item__c>();
        LstBns = new List<ISSM_Bonus__c>();
        LstEB = new List<EmptyBalanceB__c>();
        LstPC = new List<ISSM_NewOrder_ctr.PriceCondition>();

        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId    = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='2000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;


        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='3000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0,
            ISSM_Empties_Material__c = ObjProduct2.Id,
            ISSM_BoxRack__c = ObjProduct3.Id

        );
        insert ObjProduct;
        

        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;


        ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder2;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_MaterialAvailable__c  = 1000
        );
        insert ObjOI;


        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c = true
        );

        insert ObjOI2;

        ObjBns = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct.Id,
                            ISSM_SAPOrderNumber__c  = '200',
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;


        ObjEB = new EmptyBalanceB__c(
                            Account__c      = ObjAccount.Id,
                            SAPOrderNumber__c   = '1',
                            DueDate__c      = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c   = 2,
                            MaterialProduct__c = 'TST',
                            EmptyBalanceKey__c = '321',
                            Product__c  = ObjProduct.Id
        );

        LstObjEB2.add(new EmptyBalanceB__c(
                            Account__c = ObjAccount.Id,
                            SAPOrderNumber__c = '2',
                            DueDate__c = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c = 2,
                            MaterialProduct__c = 'TST',
                            EmptyBalanceKey__c = '4321',
                            Product__c = ObjProduct.Id
        ));

        insert ObjEB;

        LstBns.add(ObjBns);
        LstEB.add(ObjEB);
        LstOI.add(ObjOI);
        LstPC.add(new ISSM_NewOrder_ctr.PriceCondition(Label.ISSM_ZPVM,5.0,10));
        Apexpages.currentPage().getParameters().put(Label.ISSM_IdOrder, ObjOrder.Id);
        lstIds  = new List<ONTAP__Order_Item__c>();
        lstIds.add(ObjOI);
        ZTPMMap.put(3000005, '');

    }

    @isTest static void TestOrderTaker(){
        //GET RECORDS VALUES
        StartValues();
        ISSM_EmptyBalanceProcess_cls.WrpEmptyBalance WrpEB;
        ISSM_EmptyBalanceProcess_cls.WrpEmptyBalance WrpEB2;
        ISSM_BoxRackBalance_cls.WrpBoxRackBalance WrpBoxRackBalance;
        ISSM_NewOrder_ctr CTR = new ISSM_NewOrder_ctr();
        String StrPay = ISSM_NewOrder_ctr.StrPayMethod;
                    
        Test.startTest();

            List<SelectOption> SelOpts  = CTR.LevelList;
            List<SelectOption> SelOpts2 = CTR.getCallResultValues;
            List<SelectOption> SelOpts3 = CTR.getCallLaterValues;
            String result_Str1 = CTR.getAllPicklistValues();
            String result_Str2 = CTR.getAllSelectedValues();
            System.assertEquals(true,SelOpts.size()>0);
            System.assertEquals(true,SelOpts2.size()>0);
            System.assertEquals(true,SelOpts3.size()>0);

            CTR.StrCallResult     = String.valueOf(SelOpts2[0].getValue());
            CTR.StrSgtTimeCalling = String.valueOf(SelOpts3[0].getValue());
            System.assertNotEquals(null, CTR.StrCallResult);
            System.assertNotEquals(null, CTR.StrSgtTimeCalling);


            /////
            String s1 = CTR.StrCondition = 's1';
            String s2 = ISSM_NewOrder_ctr.StrTotal;
            String s3 = ISSM_NewOrder_ctr.StrTotalDesc;
            String s4 = ISSM_NewOrder_ctr.StrTotalIVA;
            /////

            ONTAP__Order_Item__c[] OBJPRODI = ISSM_NewOrder_ctr.getOrderItem(ObjOrder.Id);
            System.assertEquals(false,OBJPRODI.size()>0);

            ISSM_FillRate__c[] OBJFR = ISSM_NewOrder_ctr.getFillRate(lstIds, lstIds, lstIds, ObjOrder.Id, ObjAccount.Id);
            System.assertEquals(true,OBJFR.size()>0);

            ISSM_Bonus__c[] OBJPRODBNS      = ISSM_NewOrder_ctr.readBonuses(ObjAccount.Id);
            System.assertEquals(true,OBJPRODBNS.size()>0);
            
            ISSM_Bonus__c[] OBJPRODBNS2     = ISSM_NewOrder_ctr.processBonusSelected(OBJPRODBNS,Double.valueOf(1000.10),Double.valueOf(100.20),String.valueOf(ObjOrder.Id),'');
            System.assertEquals(true,OBJPRODBNS2.size()>0);
            
            LstProduc =  new List<ONTAP__Product__c>();
            WrpEB2 = ISSM_NewOrder_ctr.getEmptyBalance(LstOI,OBJPRODBNS2,ObjAccount.Id,ObjOrder.Id,LstProduc);
            System.assertEquals(true,WrpEB2 != null);
            
            //***************** WrpBoxRackBalance
            WrpBoxRackBalance = ISSM_NewOrder_ctr.getBoxRackBalance(LstOI,OBJPRODBNS2,ObjAccount.Id,ObjOrder.Id,new List<ONTAP__Product__c>());
            //System.assertEquals(true,WrpBoxRackBalance != null);
            //***************** WrpBoxRackBalance
            
            //ONTAP__Product__c[] OBJPROD = ISSM_NewOrder_ctr.select_getProducts(ObjAccount.id, '3116', 'FF000100734672');
            //System.assertEquals(false,OBJPROD.size()>0);

            CTR.updateAccount();
            CTR.noAction();
            ONTAP__Product__c[] OBJPROD2 = ISSM_NewOrder_ctr.select_getProductsONTAP(ObjAccount.ONTAP__SalesOgId__c);
            ONTAP__Product__c[] OBJPROD3 = ISSM_NewOrder_ctr.select_getProductsInvType(ObjAccount.ONTAP__SalesOgId__c,'Y029',ObjAccount.Id,'EXTRA');

            ObjAccount = ISSM_NewOrder_ctr.updateAccountAJS(ObjAccount.Id,'COMENTARIO PRUEBA','5555555555','5555555555',
                                                           '5555555555','tst@emailtst.com',true,true,true,true,true, new List<String>());
            String StrCall    = ISSM_NewOrder_ctr.updateCall(ObjCall.Id,CTR.StrCallResult,'','COMENTARIO PRUEBA');

            ISSM_NewOrder_ctr.RefreshOrder(ObjOrder.Id); 
            
            String[] LstStr = ISSM_NewOrder_ctr.endCallWithoutOrder(ObjOrder.Id,ObjCall.id,'5');
            System.assertEquals(true,!LstStr.isEmpty());
            
            String[] LstStr2= ISSM_NewOrder_ctr.endCallWithoutOrderOnTap(ObjOrder2.Id);
            System.assertEquals(true,!LstStr2.isEmpty());

            String[] LstStr3 = ISSM_NewOrder_ctr.endCallModernChannel(ObjOrder2.Id, ObjAccount.Id, ObjCall.Id,LstOI, '5','COMENTARIO PRUEBA',
                                                                     '12345QAZ','Y013','5555555555','5555555555','5555555555',
                                                                     'tst@emailtst.com',true,true,true,true,true, new List<String>(),'1');
            System.assertEquals(true,!LstStr3.isEmpty());
            
        Test.stopTest();
    }
    
    @isTest static void TestEndCall(){
        StartValues();
        ISSM_NewOrder_ctr.WrpOrder WrpOrder;
        
        
        ISSM_Wrapper_cls objWrap = new ISSM_Wrapper_cls();
        ISSM_FilRate_cls objFR = new ISSM_FilRate_cls ();
        
        ISSM_FinishOrder_cls FO = new ISSM_FinishOrder_cls();

        WrpOrder = ISSM_NewOrder_ctr.processSelected(lstIds,ObjOrder.Id,ObjAccount.Id,ObjAccount.ONTAP__SalesOgId__c,'Y064'); 

        EmptyBalanceB__c[] LstEBGroup = new List<EmptyBalanceB__c>();
        ISSM_DeserializePricingMotor_cls.OrderItems[] LstDealsReceived = new List<ISSM_DeserializePricingMotor_cls.OrderItems>();   

        ISSM_DeserializePricingMotor_cls.OrderConditions OBJOC = new ISSM_DeserializePricingMotor_cls.OrderConditions();
        ISSM_DeserializePricingMotor_cls.OrderConditions[] LSTOBJOC = new List<ISSM_DeserializePricingMotor_cls.OrderConditions>();
        ISSM_DeserializePricingMotor_cls.CashAmounts OBJCA = new ISSM_DeserializePricingMotor_cls.CashAmounts();
        ISSM_DeserializePricingMotor_cls.OrderItems OBJOI = new ISSM_DeserializePricingMotor_cls.OrderItems();
        ISSM_DeserializePricingMotor_cls.OrderItems[] LSTOII = new List<ISSM_DeserializePricingMotor_cls.OrderItems>();

        OBJOC.conditionType = 'ZPD1';
        OBJOC.amount = 12.21;
        LSTOBJOC.add(OBJOC);

        OBJCA.totalPrice=1.1;
        OBJCA.unitPrice=2.2;
        OBJCA.discount=3.3;
        OBJCA.taxes=4.4;
        OBJCA.orderConditions=LSTOBJOC;

        OBJOI.itemPosition='1';
        OBJOI.quantity=2;
        OBJOI.materialNumber=3000005;
        OBJOI.promoNo='123QWE';
       //   OBJOI.promoFlag='X';
        OBJOI.cashAmounts=OBJCA;
        OBJOI.creditAmounts=OBJCA;
        LSTOII.add(OBJOI);

        Test.startTest(); 
            String[] LstStr = ISSM_NewOrder_ctr.EndCall(ObjOrder.Id, ObjAccount.Id, ObjCall.Id, LstOI, LstPC, '5', LstBns, 'C',
                                                       LstEBGroup,WrpOrder.LstDealsCashCredit,WrpOrder.LstOrderItemsDeals,'B',
                                                       null,null,'COMENTARIO PRUEBA',ZTPMMap,null,'Y029',
                                                       '5555555555','5555555555','5555555555','tst@emailtst.com',
                                                       true,true,true,true,true, new List<String>(),'1','1','1');
            LstStr = ISSM_NewOrder_ctr.EndCall(ObjOrder.Id, ObjAccount.Id, ObjCall.Id, LstOI, LstPC, '5', LstBns, 'A',
                                               LstEBGroup,null,null,null,null,null,'COMENTARIO PRUEBA',ZTPMMap,null,'Y064',
                                               '5555555555','5555555555','5555555555','tst@emailtst.com',
                                               true,true,true,true,true, new List<String>(),'1','1','1');
            ISSM_Wrapper_cls.WrpDealsMaterial objwrapper = new ISSM_Wrapper_cls.WrpDealsMaterial ('ZPD1',111.1,'3000005',2,10.1,'1A2B3C');
            ISSM_Wrapper_cls.WrpDealsMaterial[] LstDeal = new List<ISSM_Wrapper_cls.WrpDealsMaterial>();
            LstDeal.add(objwrapper);
            LstStr = ISSM_NewOrder_ctr.EndCall(ObjOrder.Id, ObjAccount.Id, ObjCall.Id, LstOI, LstPC, '5', LstBns, 'B',
                                               LstEBGroup,WrpOrder.LstDealsCredit,WrpOrder.LstOrderItemsDeals,null,null,null,
                                               'COMENTARIO PRUEBA',ZTPMMap,null,'Y029', '5555555555','5555555555','5555555555',
                                               'tst@emailtst.com',true,true,true,true,true, new List<String>(),'1','1','1');

            Boolean BlnDeletedOI = ISSM_NewOrder_ctr.deleteOrderItems(lstIds,ObjOrder.Id);

            ISSM_NewOrder_ctr.DealsProcessInfo(LSTOII,LSTOII,'A',LSTOII,LSTOII);
            ISSM_NewOrder_ctr.DealsProcessInfo(LSTOII,LSTOII,'B',LSTOII,LSTOII);
            ISSM_Wrapper_cls.WrpDealsSend objwrappersend = new ISSM_Wrapper_cls.WrpDealsSend ('1A2B3C4D',LstDeal);
            System.assertEquals(true,!LstStr.isEmpty());
        Test.stopTest();
            
    }


    @isTest static void TestEndCallwithoutDEALS(){
        StartValues();
        ISSM_FinishOrder_cls FO = new ISSM_FinishOrder_cls();
        EmptyBalanceB__c[] LstEBGroup = new List<EmptyBalanceB__c>();

        ISSM_DeserializePricingMotor_cls.OrderConditions OBJOC = new ISSM_DeserializePricingMotor_cls.OrderConditions();
        ISSM_DeserializePricingMotor_cls.OrderConditions[] LSTOBJOC = new List<ISSM_DeserializePricingMotor_cls.OrderConditions>();
        ISSM_DeserializePricingMotor_cls.CashAmounts OBJCA = new ISSM_DeserializePricingMotor_cls.CashAmounts();
        ISSM_DeserializePricingMotor_cls.OrderItems OBJOI = new ISSM_DeserializePricingMotor_cls.OrderItems();
        ISSM_DeserializePricingMotor_cls.OrderItems[] LSTOII = new List<ISSM_DeserializePricingMotor_cls.OrderItems>();

        OBJOC.conditionType = 'ZPD1';
        OBJOC.amount = 12.21;
        LSTOBJOC.add(OBJOC);

        OBJCA.totalPrice=1.1;
        OBJCA.unitPrice=2.2;
        OBJCA.discount=3.3;
        OBJCA.taxes=4.4;
        OBJCA.orderConditions=LSTOBJOC;

        OBJOI.itemPosition='1';
        OBJOI.quantity=2;
        OBJOI.materialNumber=3000005;
        OBJOI.promoNo='123QWE';
        //OBJOI.promoFlag='X';
        OBJOI.cashAmounts=OBJCA;
        OBJOI.creditAmounts=OBJCA; 
        LSTOII.add(OBJOI);

        Test.startTest();
            String[] LstStr = ISSM_NewOrder_ctr.EndCall(ObjOrder.Id, ObjAccount.Id, ObjCall.Id, LstOI, LstPC, '5', LstBns, 'C',
                                                        LstEBGroup,LSTOII,LSTOII,'B',null,null,'COMENTARIO PRUEBA',ZTPMMap,null,'Y029',
                                                        '5555555555','5555555555','5555555555','tst@emailtst.com',
                                                        true,true,true,true,true, new List<String>(),'1','2','1');
        Test.stopTest(); 
            
    }
    
}