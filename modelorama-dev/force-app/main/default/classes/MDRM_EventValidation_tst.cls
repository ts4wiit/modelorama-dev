@isTest
public class MDRM_EventValidation_tst {
	private static testmethod void testValidationsIncompleteData(){
        User userTest = new User(
            ProfileId = (new List<String>(MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('AccountCompleteData')))[0],
            LastName = 'Test',
            Email = 'asdasdasd@amamama.com',
            Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST2',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert userTest;
        String recordTypeName = (new List<String>(MDRM_EventValidation_cls.APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME.get('AccountCompleteData')))[0];
        Id recordtypeID = [Select Id from RecordType WHERE DeveloperName =: recordTypeName][0].Id;
        System.runAs(userTest){
            //with complete data
            Account acctWData = new Account(Name='testAcc1', RecordTypeId = recordtypeID);
            for(String fld:MDRM_EventValidation_cls.VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_FieldsTovalidate__c.split(',')){
                if(fld.toLowerCase().contains('phone')){
                    acctWData.put(fld, '12312332');
                }else if(fld.toLowerCase().contains('email')){
                    acctWData.put(fld, '123@xx.xom');
                }else{
                    acctWData.put(fld, 'test123');
                }
            }
            insert acctWData;
            Account acctWOData = new Account(Name='testAcc2', RecordTypeId = recordtypeID);
            insert acctWOData;
            List<Event> events = new List<Event>();
            String whatID_tst = '';
            for(Integer i = 0; i<2;i++){
                if(i==0){
                    whatID_tst = acctWData.Id;
                }else{
                    whatID_tst = acctWOData.Id;
                }
                events.add(new Event(RecordTypeId=(new list<String>(MDRM_EventValidation_cls.APPLY_TO_EVENT_RECORDTYPE_ID.get('AccountCompleteData')))[0],
                                     WhatId = whatID_tst, startDateTime = System.now(), DurationInMinutes=12));
            }
            insert events[0];
            try{
				insert events[1];
				System.assert(false);
            }catch(Exception e){
                System.assert(true);
            }
        }
    }
    private static testmethod void testValidationsDuplicatedVisit(){
        User userTest = new User(
         ProfileId = (new List<String>(MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('RepeatedAccountVisitInTime')))[0],
         LastName = 'Test',
         Email = 'asdasdasd@amamama.com',
         Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST2',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert userTest;
        System.Runas(userTest){
            String recordTypeName = (new List<String>(MDRM_EventValidation_cls.APPLY_TO_ACCOUNT_RECORDTYPE_DEVNAME.get('RepeatedAccountVisitInTime')))[0];
            Id recordtypeID = [Select Id from RecordType WHERE DeveloperName =: recordTypeName][0].Id;
            Account acctWData = new Account(Name='testAcc1', RecordTypeId = recordtypeID);
            for(String fld:MDRM_EventValidation_cls.VALIDATION_SETTINGS.get('AccountCompleteData').MDRM_FieldsTovalidate__c.split(',')){
                if(fld.toLowerCase().contains('phone')){
                    acctWData.put(fld, '12312332');
                }else if(fld.toLowerCase().contains('email')){
                    acctWData.put(fld, '123@xx.xom');
                }else{
                    acctWData.put(fld, 'test123');
                }
            }
            insert acctWData;
            List<Event> events = new List<Event>();
            for(Integer i = 0; i<2;i++){
                events.add(new Event(RecordTypeId=(new list<String>(MDRM_EventValidation_cls.APPLY_TO_EVENT_RECORDTYPE_ID.get('RepeatedAccountVisitInTime')))[0],
                                     WhatId = acctWData.Id, startDateTime = System.now(),DurationInMinutes=12));
            }
            insert events[0];
            try{
				insert events[1];
				System.assert(false);
            }catch(Exception e){
                System.assert(true);
            }
            
        }
    }
}