/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 19: Validation for KATR2 field, when this is equal to '17' .....
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-04-2018   Iván Neria    Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0019_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0019_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setPARVW_BP){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        Boolean ruleHasError = false;
        
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            if(toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z019 && (toEvaluate.get(Label.CDM_Apex_KATR2_c)==Label.CDM_Apex_17 || toEvaluate.get(Label.CDM_Apex_KATR2_c)==Label.CDM_Apex_16 || toEvaluate.get(Label.CDM_Apex_KATR2_c)==Label.CDM_Apex_15)){
                for(CDM_Temp_Interlocutor__c inter :(MDM_Account_cls.getInterlocutors(setPARVW_BP, Label.CDM_Apex_BP)).values())  { 
                    if(inter.MDM_Temp_Account__c == toEvaluate.get(Label.CDM_Apex_Id)){
                        if(inter.KUNN2__c == inter.KUNNR__c){
                           	validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+Label.CDM_Apex_KUNN2_c);
                        	validationResult.put((Label.CDM_Apex_DevName2 + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c );
                        	validationResult.put((Label.CDM_Apex_DevName2 + HAS_ERROR_SUFIX), true);
                        	validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        	ruleHasError = true; 
                        }
                    }
                }
            }
            if(ruleHasError) validationResults.add((MDM_Validation_Result__c)validationResult);
                validationResult = new MDM_Validation_Result__c();
                validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
                ruleHasError = false;
        }
        return validationResults;
    }
}