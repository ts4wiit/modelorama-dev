/****************************************************************************************************
    General Information
    -------------------
    Author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Class to calculate the new Credit Amount in the Account when the Open Items related
    						to the Client has changes.
		Events for the Trigger: after insert, after update, after undelete, before delete.

    Related classes: ISSM_OnCallQueries_cls, xOpenItemCreditAmount_thr, ISSM_TriggerManager_cls
    Related triggers: xOpenItem_tgr 

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       10-April-2018          Marco Zúñiga                Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
private class xOpenItemCreditAmount_tst {
	static ISSM_OnCallQueries_cls CTRL = new ISSM_OnCallQueries_cls();
	static Id recTypeIdOI = Schema.SObjectType.ISSM_OpenItemB__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_ONTAPCompact).getRecordTypeId();
	static Id recTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();

	static Account ObjAcc             = new Account();
	static ISSM_OpenItemB__c ObjOI    = new ISSM_OpenItemB__c();
	static ISSM_OpenItemB__c[] OI_lst = new ISSM_OpenItemB__c[]{};

	@testSetup static void createData1() {

		ObjAcc.Name                    = 'Account 1';
		ObjAcc.ISSM_CreditLimit__c     = 1000;
		ObjAcc.ONTAP__SAP_Number__c    = '0123456789';
		ObjAcc.RecordTypeId            = recTypeIdAcc;
		ObjAcc.ONTAP__Credit_Amount__c = 10;
		insert ObjAcc;

		ObjOI.ISSM_Account__c        = ObjAcc.Id;
		ObjOI.ISSM_SAPCustomerId__c  = ObjAcc.ONTAP__SAP_Number__c;
		ObjOI.ISSM_Debit_Credit__c   = Label.ISSM_S;
		ObjOI.RecordTypeId           = recTypeIdOI;
		ObjOI.ISSM_DueDate__c        = Date.today() + 1;
		ObjOI.ISSM_Amounts__c        = 100;
		insert ObjOI;

		for(Integer i = 0; i < 10; i++){
	      ISSM_OpenItemB__c ObjOI2      = new ISSM_OpenItemB__c();
	      ObjOI2.ISSM_Account__c        = ObjAcc.Id;
	      ObjOI2.ISSM_SAPCustomerId__c  = ObjAcc.ONTAP__SAP_Number__c;
	      ObjOI2.ISSM_Debit_Credit__c   = Label.ISSM_H;
	      ObjOI2.RecordTypeId           = recTypeIdOI;
	      ObjOI2.ISSM_DueDate__c        = Date.today() + 1;
	      ObjOI2.ISSM_Amounts__c        = 100*Math.random();  
	      OI_lst.add(ObjOI2);
	    }
	}


	@isTest static void updateOIAmount(){
		Test.startTest();
			createData1();
			System.assertEquals(100,ObjOI.ISSM_Amounts__c,'Initial value for ISSM_Amounts__c field is changed.');
			ObjOI.ISSM_Amounts__c = 200;
			update ObjOI;

			System.assertEquals(200,ObjOI.ISSM_Amounts__c,'The value for ISSM_Amounts__c was not updated.');	
		Test.stopTest();
	}

	@isTest static void upsertDeleteOIAmount(){
		Test.startTest();
			System.assert(OI_lst.size() == 0 ,'There are at least 1 Open Item(s) already inserted.');
			createData1();
			upsert OI_lst;
			System.assert(OI_lst.size() > 0 ,'There are not values of Open Items to modify.');

			for(ISSM_OpenItemB__c OI_obj : OI_lst){
				OI_obj.ISSM_Amounts__c = 200;
			}
			update OI_lst;
			System.assertEquals(200,OI_lst.get(3).ISSM_Amounts__c,'The value for ISSM_Amounts__c was not updated.');

			delete OI_lst;
			System.assertNotEquals(0,OI_lst.size(),'There are at least 1 Open Item(s) already inserted.');

		Test.stopTest();
	}

	@isTest static void queryTest_tst(){
		Test.startTest();
			System.assert(OI_lst.size() == 0 ,'There are at least 1 Open Item(s) already inserted.');
			createData1();
			upsert OI_lst;
			System.assert(OI_lst.size() > 0 ,'There are not values of Open Items to modify.');

			List<Account> Acc_lst = [SELECT Id, RecordTypeId FROM Account WHERE Name = 'Account 1' LIMIT 1];

			Set<String> AccId_Set = new Set<String>();
			AccId_Set.add(String.valueOf(Acc_lst[0].Id));		
			System.assert(AccId_Set.size() == 1,'There are not Accounts to be query');	

			List<AggregateResult> OITest2_lst= CTRL.getSumOpenItemB(AccId_Set,recTypeIdOI);
			System.assertNotEquals(0,OI_lst.size(),'There are at least 1 Open Item(s) already inserted.');
		Test.stopTest();
	}

}