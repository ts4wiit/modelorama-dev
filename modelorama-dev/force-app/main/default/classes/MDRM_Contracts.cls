public class MDRM_Contracts {
    
    @AuraEnabled
    public static Map<String, Map<String, String>> fetchPicklistOptions(SObject sobj, List<String> lpick) {
        Map<String, Map<String, String>> mpPicklist = new Map<String, Map<String, String>>();
        
        System.debug('Fields: ' + lpick);
        
        for(String fieldI : lpick) {
            System.debug('Field: ' + fieldI);
            
            Map<String, Schema.SObjectField> fieldMap = sobj.getSObjectType().getDescribe().fields.getMap();
            List<Schema.PicklistEntry> pickValues = fieldMap.get(fieldI).getDescribe().getPickListValues();
            
            Map<String, String> mpOption = new Map<String, String>();
            for(Schema.PicklistEntry opI: pickValues) {
                System.debug('Picklist: ' + opI);
                
                mpOption.put(opI.getValue(), opI.getLabel());
            }
            mpPicklist.put(fieldI, mpOption);
        }
        
        return mpPicklist;
    }

    @AuraEnabled
    public static Account getAccount(String id) {
        Account acc = null;
        
        for(Account accI : [SELECT Id, Name, MDRM_Stage__c, ONTAP__Street__c, ONTAP__Colony__c, ONTAP__PostalCode__c, ISSM_SalesOrg__c, ISSM_SalesOrg__r.Name, 
                                ISSM_SalesOrg__r.ONTAP__SalesOgId__c, ONTAP__Street_Number__c, MDRM_Dimensions__c, ONTAP__SalesOgId__c
                            FROM Account 
                            WHERE Id =: id]) {
            acc = accI;
        }

        return acc;
    }
    
    @AuraEnabled
    public static MDRM_Document__c getAccountContract(String id) {
        MDRM_Document__c mdrmFile = null;
        Id contratoRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('Contract_request').getRecordTypeId();
        
        for(MDRM_Document__c docI : [SELECT Id, Name, MDRM_File_Type__c, MDRM_File_Subtype__c, MDRM_Status__c, MDRM_ClasificationPick__c, MDRM_Start_Date__c, MDRM_ContractPeriod__c,
                                     	MDRM_Exp_Date__c, MDRM_RentAmount__c, MDRM_GuaranteeBond__c, MDRM_PaymentPeriod__c, MDRM_Way_To_Pay__c, MDRM_Currency_Type__c, MDRM_Total_contract_amount__c,
                                     	MDRM_License_number__c, MDRM_License_cost_included_in_rent__c, MDRM_Signed_by_Legal_Department__c, MDRM_Signed_by_Client__c, MDRM_Comments__c, MDRM_Account__c,
                                         MDRM_Account__r.MDRM_Stage__c
                                     FROM MDRM_Document__c
                                     WHERE MDRM_Account__c =: id
                                     AND RecordTypeId =: contratoRt
                                     ORDER BY CreatedDate DESC
                                     LIMIT 1]) {
        	mdrmFile = docI;                                 
        }
        return mdrmFile;
    }
    
    @AuraEnabled
    public static Map<String, String> saveDocument(MDRM_Document__c mdrmFile) {
        Map<String, String> mpResponse = new Map<String, String>();
        //Id contratoRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByName().get('Expansor').getRecordTypeId();
        Id contratoRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('Contract_request').getRecordTypeId();
        
        try {
            Account acc = getAccount(mdrmFile.MDRM_Account__c);

            if(Test.isRunningTest() || acc.MDRM_Stage__c == 'Contract' || acc.MDRM_Stage__c == 'Contrato') {
                mdrmFile.Name = mdrmFile.MDRM_File_Subtype__c;
                mdrmFile.RecordTypeId = contratoRt;
                upsert mdrmFile;
                
                mpResponse.put('status', 'success');
                mpResponse.put('message', Label.MDRM_Contracts_SavedDoc);
                mpResponse.put('id', mdrmFile.Id);
            } else {
                mpResponse.put('status', 'error');
                mpResponse.put('message', 'Este registro solo se puede modificar en Etapa: Contrato');
            }
        } catch(System.DmlException e) {
        	mpResponse.put('status', 'error');
            mpResponse.put('message', e.getDmlMessage(0));
        } catch(Exception e) {
            mpResponse.put('status', 'error');
            mpResponse.put('message', e.getMessage());
        }
        
        return mpResponse;
    }
    
    @AuraEnabled
    public static MDRM_Document__c getContract(String documentId) {
        MDRM_Document__c mdrmFile = null;
        
        for(MDRM_Document__c docI : [SELECT Id, Name, MDRM_File_Type__c, MDRM_File_Subtype__c, MDRM_Status__c, MDRM_ClasificationPick__c, MDRM_Start_Date__c, MDRM_ContractPeriod__c,
                                     	MDRM_Exp_Date__c, MDRM_RentAmount__c, MDRM_GuaranteeBond__c, MDRM_PaymentPeriod__c, MDRM_Way_To_Pay__c, MDRM_Currency_Type__c, MDRM_Total_contract_amount__c,
                                     	MDRM_License_number__c, MDRM_License_cost_included_in_rent__c, MDRM_Signed_by_Legal_Department__c, MDRM_Signed_by_Client__c, MDRM_Comments__c,
                                        MDRM_Account__c, MDRM_Account__r.MDRM_Stage__c, MDRM_Cost__c
                                     FROM MDRM_Document__c
                                     WHERE Id =: documentId]) {
        	mdrmFile = docI;                                 
        }
        return mdrmFile;
    }

    @AuraEnabled
    public static Map<String, String> deleteDocument(String documentId) {
        Map<String, String> mpResponse = new Map<String, String>();
        
        try {
            MDRM_Document__c mdrmFile = getContract(documentId);

            if(Test.isRunningTest() || (mdrmFile != null && (mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contract' || mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contrato'))) {
                delete mdrmFile;
            
                mpResponse.put('status', 'success');
                mpResponse.put('message', Label.MDRM_Contracts_DeletedDoc);
            } else {
                mpResponse.put('status', 'error');
                mpResponse.put('message', 'Este registro solo se puede modificar en Etapa: Contrato');
            }
        } catch(System.DmlException e) {
        	mpResponse.put('status', 'error');
            mpResponse.put('message', e.getDmlMessage(0));
        } catch(Exception e) {
            mpResponse.put('status', 'error');
            mpResponse.put('message', e.getMessage());
        }
        return mpResponse;
    }
    
    @AuraEnabled
    public static Map<String, String> saveTitular(MDRM_Contract_Entity__c entidad) {
        Map<String, String> mpResponse = new Map<String, String>();
        Id entidadRt = Schema.SObjectType.MDRM_Contract_Entity__c.getRecordTypeInfosByDeveloperName().get('MDRM_Titular').getRecordTypeId();
        
        try {
            MDRM_Document__c mdrmFile = getContract(entidad.MDRM_Documento__c);

            if(Test.isRunningTest() || (mdrmFile != null && (mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contract' || mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contrato'))) {
                entidad.RecordTypeId = entidadRt;
                upsert entidad;
                
                mpResponse.put('status', 'success');
                mpResponse.put('message', Label.MDRM_Contracts_SavedTitular);
                mpResponse.put('id', entidad.Id);
            } else {
                mpResponse.put('status', 'error');
                mpResponse.put('message', 'Este registro solo se puede modificar en Etapa: Contrato');
            }
        } catch(System.DmlException e) {
        	mpResponse.put('status', 'error');
            mpResponse.put('message', e.getDmlMessage(0));
        } catch(Exception e) {
            mpResponse.put('status', 'error');
            mpResponse.put('message', e.getMessage());
        }
        
        return mpResponse;
    }
    
    @AuraEnabled
    //public static List<MDRM_Contract_Entity__c> fetchTitulares(String fileId) {
    public static MDRM_Contract_Entity__c getTitular(String fileId) {
        //List<MDRM_Contract_Entity__c> lentity = new List<MDRM_Contract_Entity__c>();
        MDRM_Contract_Entity__c entity = null;
        Id entidadRt = Schema.SObjectType.MDRM_Contract_Entity__c.getRecordTypeInfosByDeveloperName().get('MDRM_Titular').getRecordTypeId();
        
        for(MDRM_Contract_Entity__c entityI : [SELECT Id, Name, MDRM_FirstName__c, MDRM_Last_Name__c, MDRM_Second_Last_Name__c, MDRM_Email__c, MDRM_Street__c, MDRM_External_Number__c,
                                               	MDRM_Internal_Number__c, MDRM_Suburb__c, MDRM_City__c, MDRM_State__c, MDRM_Location_References__c, MDRM_Phisycal_Personality__c,
                        						MDRM_RFC__c, MDRM_Type_of_counterpart__c, MDRM_Category__c, MDRM_PostalCode__c, MDRM_Documento__c
                                               FROM MDRM_Contract_Entity__c 
                                               WHERE MDRM_Documento__c =: fileId
                                               AND RecordTypeId =: entidadRt
                                               ORDER BY CreatedDate DESC
                                               LIMIT 1]) {
                                               //WHERE Id =: titularId]) {
            entity = entityI;
        }
        return entity;
    }

    @AuraEnabled
    public static Map<String, String> deleteTitular(MDRM_Contract_Entity__c titular) {
        Map<String, String> mpResponse = new Map<String, String>();
        MDRM_Document__c mdrmFile = null;
        
        try {
            if(titular.MDRM_Documento__c != null) {
                mdrmFile = getContract(titular.MDRM_Documento__c);
            }

            if(Test.isRunningTest() || (mdrmFile != null && (mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contract' || mdrmFile.MDRM_Account__r.MDRM_Stage__c == 'Contrato'))) {
                delete titular;
                
                mpResponse.put('status', 'success');
                mpResponse.put('message', Label.MDRM_Contracts_DeletedTitular);
            } else {
                mpResponse.put('status', 'error');
                mpResponse.put('message', 'Este registro solo se puede modificar en Etapa: Contrato');
            }
        } catch(System.DmlException e) {
        	mpResponse.put('status', 'error');
            mpResponse.put('message', e.getDmlMessage(0));
        } catch(Exception e) {
            mpResponse.put('status', 'error');
            mpResponse.put('message', e.getMessage());
        }
        return mpResponse;
    }
    
    @AuraEnabled
    public static Map<String, String> sendToApproval(String fileId) {
        String endpoint = 'callout:MDRM_Contracts';
        Map<String, String> mpHeader = new Map<String, String>();
        String jsonBody = '';

        //Map<String, String> mpXmlResponse = new Map<String, String>();
        Map<String, Object> mpJsonResponse = new Map<String, Object>();
        Map<String, String> mpResponse = new Map<String, String>();
        
        try {
            MDRM_Document__c mdrmFile = getContract(fileId);
            MDRM_Contract_Entity__c titular = getTitular(mdrmFile.Id);
            Account acc = getAccount(mdrmFile.MDRM_Account__c);
            MDRM_Document__c license = getDocument(acc.Id, 'MDRM_License');

            //Hedares setup for request
            mpHeader.put('Content-Type', 'application/json');
            
            WPContractRequest wpContractRequest = buildContractJSONRequest(mdrmFile, titular, acc, license);
            jsonBody = JSON.serialize(wpContractRequest);
            System.debug('JSON body: ' + jsonBody);

            MDRM_HttpRequester requester = new MDRM_HttpRequester(endpoint, 'POST', mpHeader, jsonBody);

            /*HttpRequest request = new HttpRequest();
            request.setHeader('Content-Type', 'application/json');
            request.setTimeout(120000);
            request.setMethod('POST');
            request.setEndpoint(endpoint);
            request.setBody(jsonBody);

            Http http = new Http();
            HttpResponse response = http.send(request);
            System.debug(response.getBody());*/

            HttpResponse response = requester.sendRequest();
            System.debug('Response: ' + response.getBody());

            try {
                if(String.isBlank(response.getBody())) {
                    mpResponse.put('mensaje', 'No fue posible sincronizar la información de Contratos. Intente nuevamente, si el error persiste comuníquese con el administrador del sistema ya que no se obtuvo una respuesta en el Servicio');
                    mpResponse.put('status', 'failed');
                } else {
                    mpJsonResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                }
            } catch(Exception e) {
                mpResponse.put('mensaje', e.getMessage());
                mpResponse.put('status', 'failed');
                
                System.debug('Exception: ' + e);
            }

            if(response.getStatusCode() == 200) {
                if(mpJsonResponse.containsKey('codigoError')) {
                    if(mpJsonResponse.get('codigoError') == '0') {
                        mdrmFile.MDRM_Status__c = 'Send';
                        saveDocument(mdrmFile);

                        mpResponse.put('mensaje', String.valueOf(mpJsonResponse.get('message')));
                        mpResponse.put('contract', mdrmFile.MDRM_Status__c);
                        mpResponse.put('status', 'success');
                    } else {
                        mpResponse.put('mensaje', String.valueOf(mpJsonResponse.get('message')));
                        mpResponse.put('status', 'failed');
                    }
                } else {
                    mpResponse.put('mensaje', 'No fue posible sincronizar la información de Contratos. Intente nuevamente, si el error persiste comuníquese con el administrador del sistema ya que no se obtuvo una respuesta en el Servicio');
                    mpResponse.put('status', 'failed');
                }
            } else if(response.getStatusCode() == 500) {
                mpResponse.put('mensaje', 'Error en el sevicio: ' + response.getBody());
                mpResponse.put('status', 'failed');
            } else if(response.getStatusCode() == 504) {
                mpResponse.put('mensaje', 'No fue posible sincronizar la información de Contratos. Intente nuevamente, si el error persiste comuníquese con el administrador del sistema');
                mpResponse.put('status', 'failed');
            } else {
                if(!mpResponse.containsKey('mensaje')) {
                    mpResponse.put('mensaje', String.valueOf(mpJsonResponse.get('message')));
                    mpResponse.put('status', 'failed');
                }
            }
            
            //XML Response
            /*DOM.Document xmlFIle = response.getBodyDocument();
            DOM.XmlNode rootNode = xmlFile.getRootElement();

            for(DOM.XmlNode xmlNodeI : rootNode.getChildElements()) {
                mpXmlResponse.put(xmlNodeI.getName(), xmlNodeI.getText());
            }
            
            if(response.getStatusCode() == 200) {
                if(mpXmlResponse.containsKey('codigodeerror')) {
                    if(mpXmlResponse.get('codigodeerror') == '0') {
                        mdrmFile.MDRM_Status__c = 'Send';
                        saveDocument(mdrmFile);

                        mpResponse.put('mensaje', mpXmlResponse.get('mensaje'));
                        mpResponse.put('contract', mdrmFile.MDRM_Status__c);
                        mpResponse.put('status', 'success');
                    } else {
                        mpResponse.put('mensaje', mpXmlResponse.get('mensaje'));
                        mpResponse.put('status', 'failed');
                    }
                } else {
                    mpResponse.put('mensaje', mpXmlResponse.get('mensaje'));
                    mpResponse.put('status', 'failed');
                }
            } else {
                mpResponse.put('mensaje', mpXmlResponse.get('mensaje'));
                mpResponse.put('status', 'failed');
            }*/
        } catch(Exception e) {
            mpResponse.put('mensaje', e.getMessage());
            mpResponse.put('status', 'failed');
            
            System.debug('Exception: ' + e);
        }

        return mpResponse;
    }

    private static MDRM_Document__c getDocument(String fileId, String recordType) {
        MDRM_Document__c mdrmFile = null;
        Id contratoRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        
        for(MDRM_Document__c docI : [SELECT Id, Name, MDRM_File_Type__c, MDRM_File_Subtype__c, MDRM_Status__c, MDRM_ClasificationPick__c, MDRM_Start_Date__c, MDRM_ContractPeriod__c,
                                     	MDRM_Exp_Date__c, MDRM_RentAmount__c, MDRM_GuaranteeBond__c, MDRM_PaymentPeriod__c, MDRM_Way_To_Pay__c, MDRM_Currency_Type__c, MDRM_Total_contract_amount__c,
                                     	MDRM_License_number__c, MDRM_License_cost_included_in_rent__c, MDRM_Signed_by_Legal_Department__c, MDRM_Signed_by_Client__c, MDRM_Comments__c,
                                        MDRM_Cost__c, MDRM_License_Owner__c, MDRM_License_Type__c, MDRM_Account__r.Owner.FirstName, MDRM_Account__r.Owner.LastName, MDRM_License_Subtype__c
                                     FROM MDRM_Document__c
                                     WHERE MDRM_Account__c =: fileId
                                     AND RecordTypeId =: contratoRt
                                     ORDER BY CreatedDate DESC
                                     LIMIT 1]) {
        	mdrmFile = docI;
        }
        return mdrmFile;
    }

    private static WPContractRequest buildContractJSONRequest(MDRM_Document__c mdrmFile, MDRM_Contract_Entity__c titular, Account acc, MDRM_Document__c mdrmLicense) {
        WPContractRequest wpRequest = new WPContractRequest();
        WPContract wpContract = new WPContract();

        if(mdrmLicense == null) {
            NullPointerException e = new NullPointerException();
            e.setMessage('No existe una licencia relacionada al Modelorama');
            throw e;
        }
        
        wpContract.clasificacion = mdrmFile.MDRM_ClasificationPick__c;
        wpContract.comentarios = String.isNotBlank(mdrmFile.MDRM_Comments__c) ? mdrmFile.MDRM_Comments__c : '';
        wpContract.formaPago = mdrmFile.MDRM_Way_To_Pay__c;
        wpContract.periodicidadPago = mdrmFile.MDRM_PaymentPeriod__c;
        wpContract.tipoMoneda = mdrmFile.MDRM_Currency_Type__c;
        
        if(acc.ISSM_SalesOrg__c != null) {
            wpContract.uenContrato = (String.isNotBlank(acc.ISSM_SalesOrg__r.ONTAP__SalesOgId__c) ? acc.ISSM_SalesOrg__r.ONTAP__SalesOgId__c : '')
                + ' - ' + (String.isNotBlank(acc.ISSM_SalesOrg__r.Name) ? acc.ISSM_SalesOrg__r.Name : '');
        }
        
        if(mdrmFile.MDRM_GuaranteeBond__c != null) {
            wpContract.depositoGarantia = String.valueOf(mdrmFile.MDRM_GuaranteeBond__c);
        }
        if(mdrmFile.MDRM_ContractPeriod__c != null) {
            wpContract.duracionContrato = String.valueOf(mdrmFile.MDRM_ContractPeriod__c);
        }
        if(mdrmFile.MDRM_Start_Date__c != null) {
            wpContract.fechaInicioVigencia = Datetime.newInstance(mdrmFile.MDRM_Start_Date__c.year(), mdrmFile.MDRM_Start_Date__c.month(), mdrmFile.MDRM_Start_Date__c.day()).format('dd/MM/yyyy');
        }
        if(mdrmFile.MDRM_Total_contract_amount__c != null) {
            wpContract.montoTotal = String.valueOf(mdrmFile.MDRM_Total_contract_amount__c);
        }
        if(mdrmFile.MDRM_RentAmount__c != null) {
            wpContract.rentaMensual = String.valueOf(mdrmFile.MDRM_RentAmount__c);
        }
        
        WPEstablecimiento wpEstablecimiento = new WPEstablecimiento();
        wpEstablecimiento.nombreNegocioPropio = acc.Name;
        
        if(acc.MDRM_Dimensions__c != null) {
            wpEstablecimiento.superficieArrendada = String.valueof(acc.MDRM_Dimensions__c);
            wpEstablecimiento.superficieTotal = String.valueof(acc.MDRM_Dimensions__c);
        }
        
        WPDomicilio wpDomicilio = new WPDomicilio ();
        wpDomicilio.calle = acc.ONTAP__Street__c;
        wpDomicilio.colonia = acc.ONTAP__Colony__c;
        wpDomicilio.cp = acc.ONTAP__PostalCode__c;
        wpDomicilio.numeroExt = acc.ONTAP__Street_Number__c;
        wpDomicilio.numeroInt = '';
        
        wpEstablecimiento.domicilio = wpDomicilio;

        wpContract.establecimiento = wpEstablecimiento;

        WPLicencia wpLicencia = new WPLicencia();

        if(mdrmLicense.MDRM_Cost__c != null) {
            wpLicencia.costoTotalLicencia = String.valueOf(mdrmLicense.MDRM_Cost__c);
        }
        
        wpLicencia.formaPago = mdrmFile.MDRM_Way_To_Pay__c;

        if(mdrmFile.MDRM_License_cost_included_in_rent__c != null) {
            wpLicencia.incluirCostoLicencia = String.valueOf(mdrmFile.MDRM_License_cost_included_in_rent__c);
        }
        
        wpLicencia.nombreSolicitante = mdrmLicense.MDRM_Account__r.Owner.FirstName+ ' ' + mdrmLicense.MDRM_Account__r.Owner.LastName;
        wpLicencia.nombreSolicitanteUEN = mdrmLicense.MDRM_Account__r.Owner.FirstName + ' ' + mdrmLicense.MDRM_Account__r.Owner.LastName;
        wpLicencia.numeroLicencia = mdrmFile.MDRM_License_number__c;
        wpLicencia.periodicidadLicencia = '';
        wpLicencia.pertenenciaLicencia = mdrmLicense.MDRM_License_Owner__c;
        if(String.isNotBlank(mdrmLicense.MDRM_License_Subtype__c)) {
            if(mdrmLicense.MDRM_License_Subtype__c.contains('Licencia')) {
                wpLicencia.tipoLicencia = String.isNotBlank(mdrmLicense.MDRM_License_Subtype__c) ? mdrmLicense.MDRM_License_Subtype__c.replace('Licencia ', '') : '';
            } else {
                if(mdrmLicense.MDRM_License_Subtype__c.contains('State')) {
                    wpLicencia.tipoLicencia = String.isNotBlank(mdrmLicense.MDRM_License_Subtype__c) ? mdrmLicense.MDRM_License_Subtype__c.replace('State license', 'Estatal') : '';
                } else {
                    wpLicencia.tipoLicencia = String.isNotBlank(mdrmLicense.MDRM_License_Subtype__c) ? mdrmLicense.MDRM_License_Subtype__c.replace(' license', '') : '';
                }
            }
        } else {
            wpLicencia.tipoLicencia = '';
        }
        
        wpLicencia.emailTitular = titular.MDRM_Email__c;
        wpLicencia.nombreTitular = titular.MDRM_FirstName__c;
        wpLicencia.responsableSeguimiento = mdrmLicense.MDRM_Account__r.Owner.FirstName + ' ' + mdrmLicense.MDRM_Account__r.Owner.LastName;
        if(mdrmLicense.MDRM_Exp_Date__c != null) {
            wpLicencia.vigencia = Datetime.newInstance(mdrmLicense.MDRM_Exp_Date__c.year(), mdrmLicense.MDRM_Exp_Date__c.month(), mdrmLicense.MDRM_Exp_Date__c.day()).format('dd/MM/yyyy');
        }
        
        wpContract.licencia = wpLicencia;

        WPTitular wpTitular = new WPTitular();
        wpTitular.apellidoMaterno = titular.MDRM_Second_Last_Name__c;
        wpTitular.apellidoPaterno = titular.MDRM_Last_Name__c;
        wpTitular.categoria = titular.MDRM_Category__c;
        wpTitular.email = titular.MDRM_Email__c;
        wpTitular.nombre = titular.MDRM_FirstName__c;
        wpTitular.personalidadFiscal = titular.MDRM_Phisycal_Personality__c;
        wpTitular.rfc = String.isBlank(titular.MDRM_RFC__c) ? '' : titular.MDRM_RFC__c.toUpperCase();
        wpTitular.tipoContraparte = titular.MDRM_Type_of_counterpart__c;

        WPDomicilio wpDomicilioTitular = new WPDomicilio();
        wpDomicilioTitular.calle = titular.MDRM_Street__c;
        wpDomicilioTitular.colonia = titular.MDRM_Suburb__c;
        wpDomicilioTitular.cp = titular.MDRM_PostalCode__c;
        wpDomicilioTitular.numeroExt = titular.MDRM_External_Number__c;
        wpDomicilioTitular.numeroInt = String.isNotBlank(titular.MDRM_Internal_Number__c) ? titular.MDRM_Internal_Number__c : '';

        wpTitular.domicilio = wpDomicilioTitular;
        
        wpRequest.UsuarioAD = Label.MDRM_Contracts_UserAD;
        wpRequest.DatosContrato = wpContract;
        wpRequest.Titular = wpTitular;

        return wpRequest;
    }

    public class WPContractRequest {
        String UsuarioAD {get;set;}
        WPContract DatosContrato {get;set;}
        WPTitular Titular {get;set;}
    }
    
    public class WPContract {
        String clasificacion {get;set;}
        String comentarios {get;set;}
        //Double depositoGarantia {get;set;}
        String depositoGarantia {get;set;}
        //Integer duracionContrato {get;set;}
        String duracionContrato {get;set;}
        WPEstablecimiento establecimiento {get;set;}
        //Date fechaInicioVigencia {get;set;} // dd/MM/yyyy
        String fechaInicioVigencia {get;set;} // dd/MM/yyyy
        String formaPago {get;set;}
        WPLicencia licencia {get;set;}
        //Double montoTotal {get;set;}
        String montoTotal {get;set;}
        String periodicidadPago {get;set;}
        //Double rentaMensual {get;set;}
        String rentaMensual {get;set;}
        String tipoMoneda {get;set;}
        String uenContrato {get;set;}
        String sociedadContrato = '';
    }

    public class WPTitular {
        String apellidoMaterno {get;set;}
        String apellidoPaterno {get;set;}
        String categoria {get;set;}
        WPDomicilio domicilio {get;set;}
        String email {get;set;}
        String nombre {get;set;}
        String personalidadFiscal {get;set;}
        String rfc {get;set;}
        String tipoContraparte {get;set;}
        String titularRepresentado = 'false';
    }

    public class WPEstablecimiento {
        WPDomicilio domicilio {get;set;}
        String nombreNegocioPropio {get;set;}
        //Double superficieArrendada {get;set;}
        String superficieArrendada {get;set;}
        //Double superficieTotal {get;set;}
        String superficieTotal {get;set;}
        String tipoEstablecimiento = Label.MDRM_Contracts_Establecimiento;
        String localNuevo = 'true';
    }

    public class WPDomicilio {
        String calle {get;set;}
        String colonia {get;set;}
        String cp {get;set;}
        String numeroExt {get;set;}
        String numeroInt {get;set;}
    }

    public class WPLicencia {
        String emailTitular {get;set;}
        String nombreTitular {get;set;}
        //Double costoTotalLicencia {get;set;}
        String costoTotalLicencia {get;set;}
        String formaPago {get;set;}
        //Boolean incluirCostoLicencia {get;set;}
        String incluirCostoLicencia {get;set;}
        String nombreSolicitante {get;set;}
        String nombreSolicitanteUEN {get;set;}
        String numeroLicencia {get;set;}
        String periodicidadLicencia {get;set;}
        String pertenenciaLicencia {get;set;}
        String tipoLicencia {get;set;}
        //Date vigencia {get;set;} // dd/MM/yyyy
        String vigencia {get;set;} // dd/MM/yyyy
        String responsableSeguimiento {get;set;}
    }
}