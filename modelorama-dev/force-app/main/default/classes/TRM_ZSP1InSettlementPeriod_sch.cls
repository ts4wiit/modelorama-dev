/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Andrea Cedillo
* Project           :   AbInbev - Trade Revenue Management
* Descripción       :   Schedule responsible for the mass shipment of condition classes that were not sent online due to the settlement period | ZSP1
*                       it is executed every day from the date and time specified in the settlement period record
*
*  No.        Date                 Author                      Descripción
* 1.0    16-Noviembre-2018      Andrea Cedillo                   CREATION
*******************************************************************************/
global class TRM_ZSP1InSettlementPeriod_sch implements Schedulable {
    /**
    * @description  Method that invokes the Process builder 'TRM_SettlementPeriod'.    
    *   @param      lstSettlementPeriod   List of objects of type 'TRM_SettlementPeriods__c'
    * 
    *   @return     No return
    */
    @InvocableMethod 
    global static void executeSchedule(List<TRM_SettlementPeriods__c> lstSettlementPeriod) {
        programSchedule(lstSettlementPeriod[0],false);
    }
    /**
    * @description  Send canceled that were glued for the settlement period, and Send created that were glued for the settlement period.    
    * 
    *   @return     No return
    */   
    global void execute(SchedulableContext sc) {
       String typeConditionClass = Label.TRM_ZSP1;
    //Send canceled that were glued for the settlement period
        List<TRM_ConditionClass__c> lstConditionCancel =[SELECT Name
                                                               ,TRM_ExternalId__c
                                                               ,TRM_MarkedLiquidation__c 
                                                        FROM TRM_ConditionClass__c 
                                                        WHERE TRM_Status__c = 'TRM_Cancelled' 
                                                            AND TRM_ConditionClass__c =:typeConditionClass
                                                            AND TRM_MarkedLiquidation__c = true];
        if(lstConditionCancel.size() > 0){
            String operation         = Label.TRM_CancelCC;
            Integer numberLots       = (Integer)[SELECT TRM_RecordVolume__c FROM TRM_SettlementPeriods__c WHERE TRM_ConditionClas__c =:typeConditionClass][0].TRM_RecordVolume__c;
            String strJSON           = TRM_ApprovalConditions_ctr.generateJson(lstConditionCancel, operation, numberLots);            
            TRM_SendClassConditionToSAP_cls.sendClassCondFuture(strJSON, operation);
            for(TRM_ConditionClass__c conditionClass: lstConditionCancel) conditionClass.TRM_MarkedLiquidation__c = false;

            update lstConditionCancel;
        }
    //Send created that were glued for the settlement period
        List<TRM_ConditionClass__c> lstConditionApprove =[SELECT Name
                                                                ,TRM_ExternalId__c
                                                                ,TRM_MarkedLiquidation__c  
                                                        FROM TRM_ConditionClass__c 
                                                        WHERE TRM_Status__c = 'TRM_Approved' 
                                                            AND TRM_ConditionClass__c =:typeConditionClass
                                                            AND TRM_MarkedLiquidation__c = true];
        if(lstConditionApprove.size() > 0){
            String operation    = Label.TRM_CreateCC;
            Integer numberLots  = (Integer)[SELECT TRM_RecordVolume__c FROM TRM_SettlementPeriods__c WHERE TRM_ConditionClas__c =:typeConditionClass][0].TRM_RecordVolume__c;
            String strJSON      = TRM_ApprovalConditions_ctr.generateJson(lstConditionApprove, operation, numberLots);
            TRM_SendClassConditionToSAP_cls.sendClassCondFuture(strJSON, operation);
            for(TRM_ConditionClass__c conditionClass: lstConditionApprove) conditionClass.TRM_MarkedLiquidation__c = false;

            update lstConditionApprove;
        }
    }
    /**
    * @description  Program the scheduler with respect to the end date of the settlement period of the selected condition class.    
    *   @param      settlementPeriod    Settlement period to be programmed | TRM_SettlementPeriods__c
    *   @param      scheduleNow         flag, which indicates if the scheduler was programmed or not at the moment that the creation or modification of a settlement price is detected
    * 
    *   @return     No return
    */
    public static void programSchedule(TRM_SettlementPeriods__c settlementPeriod,boolean scheduleNow) {
        String name= settlementPeriod.TRM_ConditionClas__c;
        Datetime runtime = (Datetime)settlementPeriod.TRM_EffectiveDate__c;
        String[] strTime = settlementPeriod.TRM_EndTime__c.split(':');
        Time t = time.newInstance(Integer.valueOf(strTime[0]),Integer.valueOf(strTime[1]), Integer.valueOf(strTime[2]), 0);
        runtime = runtime.addHours(t.hour());
        runtime = runtime.addMinutes(t.minute());
        runtime = runtime.addSeconds(t.second()); 
        String strHour = runtime.format('yyyy-MM-dd\'T\'HH:mm:ss','GMT').substring(11, 13);
        TRM_ApprovalConditions_ctr.unSchedule(name);
        if((runtime.Date() == System.now().Date() && runtime.Date() < System.now().Date().addDays(1)) || scheduleNow){
            String cron = runtime.second() + ' ' + runtime.minute() + ' ' + strHour + ' ' + ' 1/1  * ? *';
            System.debug('cron: '+cron);
            System.schedule(name +' - ' + System.now(), cron, new TRM_ZSP1InSettlementPeriod_sch());
        }
    }

}