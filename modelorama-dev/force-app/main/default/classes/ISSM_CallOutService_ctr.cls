/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que gestiona el request y response de los web services

    Information about changes (versions)
    ========================================= =======================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       12-Julio-2017   Luis Licona                  Creación de la Clase
    2.0        25.Julio-2017   Hector Diaz                  Modificacion request
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_CallOutService_ctr {
    /**
    * class for simulate request of test class
    * @param   StrSerializeJson:  Cadena json que se envia en el body del request
    * @param   ConfigName:  Nombre de la configuración personalizada
    * @return  List<WprResponseService>:  Lista donde se guarda la respuesta del servicio
    **/ 
    public List<WprResponseService> SendRequest(String StrSerializeJson,String ConfigName){
        CalloutClass ctr = new CalloutClass();
        WprResponseService[] lstWprResponseService = new List<WprResponseService>();
        HttpResponse response;
        String strDescriptionError;
    //    try{
            if(!Test.isRunningTest()){
                response = ctr.getInfoFromExternalService(StrSerializeJson,ConfigName);
                if(response.getStatusCode() != 200){     
                   strDescriptionError  =String.valueOf(response);
                   lstWprResponseService.add(new WprResponseService(null,strDescriptionError));
                }else  
                   lstWprResponseService.add(new WprResponseService(response.getBody(),null));
                 
            }else if(Test.isRunningTest()){
                // Set mock callout class 
                System.debug(loggingLevel.Error, '*** Paso 1: ' );
                Test.setMock(HttpCalloutMock.class, new ISSM_MockHttpResponseGenerator());  
                response = ctr.getInfoFromExternalService(StrSerializeJson,ConfigName);
                lstWprResponseService.add(new WprResponseService(response.getBody(),null));
           
            }//else if(Test.isRunningTest() && ConfigName == 'ConfigWSCancelOrders'){
            /*if(Test.isRunningTest() && ConfigName == Label.ISSM_ConfigWSCancelOrders){
                System.debug(loggingLevel.Error, '*** Paso 2: '  );
                 Test.setMock(HttpCalloutMock.class, new ISSM_MockHttpResponseGenerator());  
                 response = ctr.getInfoFromExternalService(StrSerializeJson,ConfigName);
                lstWprResponseService.add(new WprResponseService(response.getBody(),null));
            }*/

        //}catch(Exception exc){

        //    exc.setMessage('Exception: '+exc); throw exc; lstWprResponseService.add(new WprResponseService(null,exc.getMessage()));
        //}
        return lstWprResponseService;
    }


    public class CalloutClass{
        Map<String, ISSM_PriceEngineConfigWS__c> MapConfiguracionWs = ISSM_PriceEngineConfigWS__c.getAll();

        /**
        * class for simulate request of test class
        * @param   StrSerializeJson:  Cadena json que se envia en el body del request
        * @param   ConfigName:  Nombre de la configuración personalizada
        * @return  HttpResponse:  Respuesta genereada por el servicio
        **/ 
        public HttpResponse getInfoFromExternalService(String StrSerializeJson,String ConfigName) {
            System.debug('ConfigName---> '+ConfigName);

            Http http = new Http();
            HttpRequest request = new HttpRequest();

            //String strendpoint = ConfigName == 'ConfigCreateOrder' ? 'https://www.noexiste.com' : MapConfiguracionWs.get(ConfigName).ISSM_EndPoint__c;
            //request.setEndpoint(strendpoint);
            
            request.setEndpoint(MapConfiguracionWs.get(ConfigName).ISSM_EndPoint__c);
            request.setMethod(MapConfiguracionWs.get(ConfigName).ISSM_Method__c);
            request.setHeader(Label.ISSM_ContentType, MapConfiguracionWs.get(ConfigName).ISSM_HeaderContentType__c);
            
            if(ConfigName == Label.ISSM_ConfigWSCancelOrders || ConfigName == Label.TRM_CustomConfigCC){
                Blob headerValue = Blob.valueOf(MapConfiguracionWs.get(ConfigName).ISSM_Username__c + ':' + MapConfiguracionWs.get(ConfigName).ISSM_Password__c);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                request.setHeader('Authorization', authorizationHeader);
                request.setTimeout(Integer.valueOf(Label.ISSM_SetTime2)); 
            }
            if(ConfigName == Label.ISSM_ConfigWSSuggestedOrder){
                String strAccesTokenSuggedted = ISSM_SuggestedProducts_cls.requestAutentication();
                request.setHeader('Authorization', 'JWT '+strAccesTokenSuggedted);  
            }
            request.setBody(StrSerializeJson);
            request.setTimeout(Integer.valueOf(Label.ISSM_SetTime));
           
            HttpResponse response = http.send(request);

            return response;
        }
    }
    
    /** 
    * class for response service
    * @param   none
    * @return  none
    **/ 
    public class WprResponseService{
        public String strBodyService;
        public String strMessageError;
        
        public WprResponseService(String strBodyService,String strMessageError){
            this.strBodyService = strBodyService;
            this.strMessageError =strMessageError;
        } 
    }
}