/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_AdminATM_Usuario_tst
Versión : 1.0
Fecha de Creación : 16 Abril 2018
Funcionalidad : Clase de prueba para el controlador ISSM_AdminATM_Usuario_ctr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        16 - Abril - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_AdminATM_Usuario_tst {
    static testmethod void staticResourceLoad() {

        // List<SObject> SalesOffice_lst = Test.loadData(Account.sObjectType,'ISSM_SalesOfficeLoad_Test');
        // List<SObject> Account_lst = Test.loadData(Account.sObjectType,'ISSM_AccountLoad_Test');
        // List<SObject> ATM_Mirror_lst = Test.loadData(ISSM_ATM_Mirror__c.sObjectType,'ISSM_ATM_MirrorLoad_Test');
        // List<SObject> ATM_lst = Test.loadData(AccountTeamMember.sObjectType,'ISSM_ATMLoad_Test');
        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();

        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
        insert user;

        Id RT_SalesOffice_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account salesOffice = new Account();
        salesOffice.Name = 'Sales Office 1';
        salesOffice.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice;

        Id RT_Account_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        Account account = new Account();
        account.Name = 'Account 1';
        account.RecordTypeId = RT_Account_id;
        insert account;

        ISSM_ATM_Mirror__c atmMirror = new ISSM_ATM_Mirror__c();
        atmMirror.Oficina_de_ventas__c = salesOffice.Id;
        atmMirror.Usuario_relacionado__c = user.Id;
        atmMirror.Rol__c = 'Supervisor';
        atmMirror.Estatus__c = 'Activo';
        // ATM_Mirror_lst.add(atmMirror);
        insert atmMirror;

        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = account.Id;
        atm.UserId = user.Id;
        atm.TeamMemberRole = 'Supervisor';
        insert atm;
        
        // System.assertEquals(SalesOffice_lst.size(), 5);
        // System.assertEquals(Account_lst.size(), 5);

        test.startTest();

            PageReference pageRef = Page.ISSM_AdminATM_Usuario_pag;
            Test.setCurrentPage(pageRef);

            ApexPages.StandardController sc = new ApexPages.standardController(user);
            ISSM_AdminATM_Usuario_ctr controller = new ISSM_AdminATM_Usuario_ctr(sc);

            controller.nuevo();

            PageReference pageRefEliminar = controller.eliminar();
            PageReference expPageEliminar = new PageReference('/apex/issm_adminatm_usuario_pag');

            PageReference pageRefGuardar = controller.guardar();
            PageReference expPageGuardar = new PageReference('/apex/ISSM_NuevoATM_Usuario_pag');

            PageReference pageRefCancelar = controller.cancelar();
            PageReference expPageCancelar = new PageReference('/apex/ISSM_AdminATM_Usuario_pag');

            System.assertEquals(null,controller.nuevo());
            // System.assertEquals(expPageEliminar.getUrl(), pageRefEliminar.getUrl());
            // System.assertEquals(expPageGuardar.getUrl(), pageRefGuardar.getUrl());
            System.assertEquals(expPageCancelar.getUrl(), pageRefCancelar.getUrl());

        test.stopTest();

    }
}