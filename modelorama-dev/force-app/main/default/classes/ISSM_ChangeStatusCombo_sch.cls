/******************************************************************************* 
* Desarrollado por  :   Avanxo México
* Autor             :   Oscar Alvarez
* Proyecto          :   AbInbev - Trade Revenue Management
* Descripción       :   Schedulable for the execution of work flows (change of status).
*                       Run every day at 11:30:00 p.m.
*
* No.       Fecha              Autor                      Descripción
* 1.0    15-Mayo-2018      Oscar Alvarez                   Creación
* 2.0    16-Noviembre-2018 Andrea Cedillo                  Modificado
*******************************************************************************/
global class ISSM_ChangeStatusCombo_sch implements Schedulable {
  /*
   * Run every day at 11:30:00 p.m.
   * 
  */
  @TestVisible
  public static final String CRON_EXPR = Label.TRM_CronExprChangeCancelCombo;    
  /*
    Call this from Anonymous Apex to schedule at the default regularity
  */
  global static String scheduleIt() {
    ISSM_ChangeStatusCombo_sch job = new ISSM_ChangeStatusCombo_sch();
    return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchStatusCmb + System.now()) : Label.TRM_NameSchStatusCmb), CRON_EXPR, job);
  }
   /*
    * Execution of the flow, for the change of status of the combo. Change status:
    *   -ISSM_Active to Cancel when combo finish
  */
  global void execute(SchedulableContext sc) {
    Map<String, Object> params = new Map<String, Object>();
    //Flow for ISSM_Cancel
        Flow.Interview.ISSM_ChangeStatusCombo flowCombo = new Flow.Interview.ISSM_ChangeStatusCombo(params);
        if(!Test.isRunningTest()){flowCombo.start();}
  }
}