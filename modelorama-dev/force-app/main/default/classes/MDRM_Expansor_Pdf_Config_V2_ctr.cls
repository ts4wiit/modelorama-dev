/****************************************************************************************************
General Information
-------------------
author:     Fernando Engel
email:      fernando.engel.funes@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Permite clasificar las imágenes para determinar donde se mostrarán en el PDF

Information about changes (versions)
================================================================================================
Number    Dates           Author                        Description          
------    --------        --------------------------    -----------------------------------------
1.0       29/06/2018      Fernando Engel                Class created
1.1       30/06/2019      Alfonso de la Cuadra          User messages, code coverage increased
================================================================================================
****************************************************************************************************/

public with sharing class MDRM_Expansor_Pdf_Config_V2_ctr {
    private final Account acc;
    public Map<String,Documento> mapDocument {get;set;}
    public List<Documento> lstDocumento {get;set;}
    public Id cvID {get;set;}
    public Integer numPDFImg {get;set;}
    public Map<String, Integer> mapCounters {get;set;}
    public Map<String, String> mapFileSubTypeRecordType {get;set;}
    public Integer totalCounters {get;set;}
    public MDRM_Document__c bc  {get;set;}
    public String strDocument{get{return 'Documento';}}
    public String strAccount {get{return 'Account';}}
    public String strImage {get{return 'Imagen';}}
    public List<SelectOption> soCounters {get;set;}
    public Boolean isDone {get;set;}
    
    public MDRM_Expansor_Pdf_Config_V2_ctr(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
        this.mapCounters = new Map<String, Integer>();
        this.mapFileSubTypeRecordType = new Map<String, String>();
        
		    this.mapFileSubTypeRecordType.put(Label.MDRM_BusinessCase,Label.MDRM_BusinessCase);
        this.mapFileSubTypeRecordType.put('Comprobante de Domicilio','Proof of Residence');
        this.mapFileSubTypeRecordType.put('Comprobante del Pago de Agua','Proof of Water Payment');
        this.mapFileSubTypeRecordType.put('Contrato de Arrendamiento','Contract');
        this.mapFileSubTypeRecordType.put('Estado de Cuenta','Bank Statement');
        this.mapFileSubTypeRecordType.put('Identificación Oficial','ID');
        this.mapFileSubTypeRecordType.put('Licencia','License');
        this.mapFileSubTypeRecordType.put('Otros','Others');
        this.mapFileSubTypeRecordType.put('Predial','Property Tax');
        this.mapFileSubTypeRecordType.put('Protección Civil','Civil Protection');
        this.mapFileSubTypeRecordType.put('Refrendo','Endorsement');
        this.mapFileSubTypeRecordType.put('RFC','RFC');
        this.mapFileSubTypeRecordType.put('Título de Propiedad','Property Title');
        this.mapFileSubTypeRecordType.put('Uso de Suelo','Land Use');
        this.mapFileSubTypeRecordType.put('Cotización Final','Final Quote');
        this.isDone = false;
        init();
    }
    
    public void init(){
        //get all Images
        this.mapDocument = new Map<String,Documento>();
        this.soCounters = new List<SelectOption>();
        this.lstDocumento = new List<Documento>();
        this.numPDFImg = 0;
        set<Id> lstContentDocumentId = new set<Id>();
        Map<Id,Id> mapCDtoLE = new Map<Id,Id>();
        id accId = this.acc.Id;
        this.bc = new MDRM_Document__c();
        
        this.mapCounters.put('Croquis (Imagen del Poc Compass)' , 0);
        this.mapCounters.put('Estimación del Volumen (Imagen del Poc Compass)' , 0);
        this.mapCounters.put('Zonas Potenciales (Imagen del Poc Compass)' , 0);
        this.mapCounters.put('Capas N.S.E. (Imagen del Poc Compass)' , 0);
        this.mapCounters.put('Reporte (Imagen del Poc Compass)' , 0);
        this.mapCounters.put('Entorno de la zona y Nivel Socioeconómico' , 0);
        this.mapCounters.put('Fotos de frente, costados y vista desde el local' , 0);
        this.mapCounters.put('Levantamiento proporcionado por el Contratista' , 0);
        this.mapCounters.put('Layout proporcionado por el Contratista' , 0);
        this.mapCounters.put('Render o fotomontaje proporcionado por el Contratista' , 0);
        this.mapCounters.put('Captura de pantalla del Resumen de la Cotización' , 0);
        this.mapCounters.put('Local operando (2 fotos exteriores y 2 interiores)' , 0);
        
        this.totalCounters = this.mapCounters.size();
        
        if( accId != null ){
            try{
                try{
                    //Get infmation from Business Case
                    this.bc = [SELECT Id, MDRM_Send_for_Approval__c
                               FROM MDRM_Document__c 
                               WHERE MDRM_Account__c =: accId 
                               AND Name =: Label.MDRM_BusinessCase
                               ORDER BY Name ASC LIMIT 1];
                }catch(Exception ex){
                    system.debug('ERROR: '+ex);
                }
                
                //get all documents related with current Account
                map<Id,MDRM_Document__c> mapDocs = new map<Id,MDRM_Document__c>([SELECT Id, Name, MDRM_Account__c, MDRM_File_Type__c,MDRM_Alcohol_license2__c,
                                                                                 MDRM_File_Subtype__c, MDRM_Exp_Date__c,MDRM_Status__c, MDRM_Other_License_Owner__c,
                                                                                 MDRM_Send_for_Approval__c,MDRM_Comments__c,MDRM_Expiration_Date__c,
                                                                                 MDRM_DocumentStatus__c,MDRM_License_Type__c,MDRM_License_Subtype__c,
                                                                                 MDRM_License_Owner__c,MDRM_Cost__c,MDRM_Process_Start_Date__c,
                                                                                 MDRM_Signed_by_Legal_Department__c,MDRM_Signed_by_Client__c
                                                                                 FROM MDRM_Document__c 
                                                                                 WHERE MDRM_Account__c =: accId  ORDER BY Name ASC ]);
                
                List<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>([SELECT ContentDocumentId,Id,LinkedEntityId 
                                                                                  FROM ContentDocumentLink 
                                                                                  WHERE LinkedEntityId =: accId]);
                
                set<id> sdocsid = mapDocs.keySet();
                //get all Content Document Ids from related account and document
                if( !mapDocs.keySet().isEmpty() ){
                    List<ContentDocumentLink> lstDocCDL = new List<ContentDocumentLink>([SELECT ContentDocumentId,Id,LinkedEntityId 
                                                                                         FROM ContentDocumentLink 
                                                                                         WHERE LinkedEntityId IN: sdocsid ]);
                    lstCDL.addAll(lstDocCDL);
                }
                //generate a index to search LinkedEntityId using ContentDocumentId
                for(ContentDocumentLink cdl : lstCDL){
                    lstContentDocumentId.add(cdl.ContentDocumentId);
                    mapCDtoLE.put(cdl.ContentDocumentId,cdl.LinkedEntityId);
                }
                
                List<ContentVersion> lstCV = new List<ContentVersion>([SELECT ContentBodyId,VersionNumber,ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension,ContentUrl,IsLatest
                                                                       FROM ContentVersion 
                                                                       WHERE ContentDocumentId IN: lstContentDocumentId
                                                                       AND IsLatest = TRUE
                                                                       ORDER BY Description,Title ASC NULLS FIRST	]);
                this.acc.MDRM_Business_case_files__c = '';
                
                for(ContentVersion cv: lstCV){
                    MDRM_Document__c doc = new MDRM_Document__c();
                    Id LinkedEntityId = mapCDtoLE.get(cv.ContentDocumentId);
                    
                    if(mapDocs.get( LinkedEntityId ) != null){
                        doc = mapDocs.get( LinkedEntityId );
                    }
                    
                    //Count images to PDF document founded
                    Documento documento = new Documento(cv, doc, LinkedEntityId);
                    
                    if(documento.Parent == this.strAccount && cv.Description != '' && mapCounters.containsKey(doc.MDRM_File_Subtype__c) ){
                        Integer cont = this.mapCounters.get(doc.MDRM_File_Subtype__c) + 1;
                        this.mapCounters.put(doc.MDRM_File_Subtype__c,cont);
                        
                        this.acc.MDRM_Business_case_files__c = this.acc.MDRM_Business_case_files__c +';'+ doc.MDRM_File_Subtype__c;
                    }
                    
                    
                    this.mapDocument.put(cv.id, documento);
                    lstDocumento.add(documento);
                }
                
                for( String key : mapCounters.keySet() ){
                    this.soCounters.add( new SelectOption( key+': '+mapCounters.get(key), key+': '+mapCounters.get(key) ) );
                    if(mapCounters.get(key) >0) this.numPDFImg++;
                }
                
            }catch(Exception ex){ 
                System.debug('ERROR: '+ex);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            }
        }
    }
    
    public void save_clasification() {
        string idacc = this.acc.id;
        String docName = Label.MDRM_BusinessCase;
        MDRM_Document__c newDoc;
        PageReference pdf = Page.MDRM_expansor_pdf_V2_pag;
        
        pdf.getParameters().put('id', idacc);
        
        try{
            try{
                newDoc = [SELECT Id,Name,MDRM_File_Subtype__c FROM MDRM_Document__c WHERE MDRM_Account__c = :idacc AND MDRM_File_Subtype__c = :docName LIMIT 1];
            }catch(Exception ex){
                system.debug('ERROR: '+ex);
                newDoc = new MDRM_Document__c();
            }
            
            If(newDoc.id == null){
                newDoc = new MDRM_Document__c(Name = docName,
                                              MDRM_File_Type__c = this.strDocument,
                                              MDRM_File_Subtype__c = docName,
                                              RecordTypeId = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByName().get(Label.MDRM_BusinessCase).getRecordTypeId(),
                                              MDRM_Account__c = idacc);
                Insert newDoc;
                ContentVersion cv = new ContentVersion();
                cv.VersionData   =  Test.isRunningTest() ?  EncodingUtil.base64Decode('Unit Test Attachment Body') : pdf.getContentAsPdf();
                cv.Title = docName;
                cv.PathOnClient  = docName+'.pdf';
                cv.Description = newDoc.MDRM_File_Subtype__c;
                Insert cv;
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
                cdl.LinkedEntityId = newDoc.id;
                cdl.ShareType = 'V';
                insert cdl;
                this.isDone = true;
            }else{
                Id ContentDocumentId = [SELECT ContentDocumentId,Id,LinkedEntityId 
                                        FROM ContentDocumentLink 
                                        WHERE LinkedEntityId = :newDoc.id 
                                        LIMIT 1].ContentDocumentId;
                
                ContentVersion cv = new ContentVersion();
                cv.ContentDocumentId = ContentDocumentId;
                cv.ReasonForChange = 'Update';
                cv.VersionData   =  Test.isRunningTest() ?  EncodingUtil.base64Decode('Unit Test Attachment Body') : pdf.getContentAsPdf();
                cv.Title = docName;
                cv.PathOnClient  = docName+'.pdf';
                cv.Description = newDoc.MDRM_File_Subtype__c;
                Insert cv;
                this.isDone = true;
            }
        }catch(System.DMLException ex){
            System.debug('Error: '+ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getDMLMessage(0)));
            this.isDone = false;
        }catch(Exception e) {
            System.debug('Error: '+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            this.isDone = false;
        }
        init();
    }
    
    public void saveChanges(){
        
       try{
            
            id cvID = this.cvID;
           
           //MDRM_File_Subtype__c is required
           if(this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c == null){
               
               this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c.addError(Label.MDRM_RquiredField);
                this.isDone = false;
               
           }
           if(this.mapDocument.get(this.cvID).doc.MDRM_Expiration_Date__c == null 
               && this.mapDocument.get(this.cvID).doc.MDRM_File_Type__c == this.strDocument 
               && this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c != Label.MDRM_BusinessCase ){
                   
                   this.mapDocument.get(this.cvID).doc.MDRM_Expiration_Date__c.addError(Label.MDRM_RquiredField);
                    this.isDone = false;
           }else{
               if(this.mapDocument.get(this.cvID).doc.MDRM_File_Type__c == this.strDocument){
                   if(this.mapDocument.get(this.cvID).doc.id == null){
                       
                       //Change Document name to File Subtype
                       ContentDocumentLink cdl = new ContentDocumentLink();
                       cdl.ContentDocumentId = [SELECT Id, ContentDocumentId 
                                                FROM ContentVersion 
                                                WHERE Id =: This.cvID 
                                                AND IsLatest = TRUE 
                                                LIMIT 1].ContentDocumentId;
                       ContentDocumentLink oldCDL = [SELECT ID
                                                     FROM ContentDocumentLink 
                                                     WHERE LinkedEntityId  =: this.acc.id 
                                                     AND ContentDocumentId =: cdl.ContentDocumentId
                                                     LIMIT 1];
                       cdl.ShareType = 'V';
                       
                       If(this.mapDocument.get(this.cvID).doc.Name != null){
                           this.mapDocument.get(this.cvID).doc.Name = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                       }
                       
                       String strRecordType = mapFileSubTypeRecordType.get(this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c);
                       Id recordtType = [SELECT ID FROM RecordType  WHERE name =:strRecordType LIMIT 1].id;
                       this.mapDocument.get(this.cvID).doc.RecordTypeId = recordtType;
                       this.mapDocument.get(this.cvID).doc.MDRM_Account__c = this.acc.id;
                       this.mapDocument.get(this.cvID).doc.name = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                       
                       //move attachment to new document
                       
                       //Insert newDoc;
                       Insert this.mapDocument.get(this.cvID).doc;
                       cdl.LinkedEntityId = this.mapDocument.get(this.cvID).doc.id;
                       Insert cdl;
                       
                       ContentVersion cv = [SELECT ContentBodyId,ContentDocumentId,FileExtension,FileType,Id,IsLatest,Title,Description,ContentSize
                                            FROM ContentVersion 
                                            WHERE ContentDocumentId =: cdl.ContentDocumentId 
                                            LIMIT 1];
                       cv.Description = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                       cv.Title = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                       
                       //add to multipicklist the documents selected
                       If(this.mapDocument.get(this.cvID).doc.MDRM_File_type__c == this.strImage){
                           this.acc.MDRM_Business_case_files__c = this.acc.MDRM_Business_case_files__c +';'+cv.Title;
                           update this.acc;
                       }
                       
                       update cv;
                       Delete oldCDL;
                       this.isDone = true;
                       //update inforamtion is exist
                   }else{
                       Update this.mapDocument.get(this.cvID).doc;
                       this.isDone = true;
                   }
               }else if(this.mapDocument.get(this.cvID).doc.MDRM_File_Type__c == this.strImage){
                   //Add Subtype like attachment description
                   this.mapDocument.get(this.cvID).cv.Description = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                   //Change Document name to File Subtype
                   this.mapDocument.get(this.cvID).cv.Title = this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c;
                   update this.mapDocument.get(this.cvID).cv;
                   this.isDone = true;
               }
               
               init();
               update this.acc;
           }
       }catch(System.DMLException ex){
           System.debug('ERROR: '+ex);
           //if(!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getDMLMessage(0)));
              this.isDone = false;
       }catch(Exception e) {
            System.debug('ERROR: '+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            this.isDone = false;
       }
        
    }
    
    public void deleteAtt(){
      this.isDone = true;
        try{
            Set<Id> lstContentDocumentId = new Set<Id>();
            Map<String,String> mapLinkedEntity = new Map<String,String>();
            String title;
            
            List<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>([SELECT ContentDocumentId,Id,LinkedEntityId,ShareType
                                                                              FROM ContentDocumentLink  
                                                                              WHERE ContentDocumentId =: this.mapDocument.get(this.cvID).cv.ContentDocumentId  
                                                                              ORDER BY ShareType ASC                                
                                                                              LIMIT 1]);
            
            for( ContentDocumentLink cdl : lstCDL ){
                lstContentDocumentId.add(cdl.ContentDocumentId);
                mapLinkedEntity.put(cdl.LinkedEntityId.getSObjectType().getDescribe().getName(), cdl.LinkedEntityId);
            }
            
            List<ContentDocument> lstCD = new List<ContentDocument>([SELECT Description,FileExtension,FileType,Id,Title FROM ContentDocument WHERE id IN: lstContentDocumentId ]);
            title = lstCD[0].Title;
            
            //Only will delete document are not in approval process
            if(mapLinkedEntity.get('MDRM_Document__c') != null ){
                MDRM_Document__c doc = [SELECT ID, MDRM_Status__c,MDRM_Send_for_Approval__c FROM MDRM_Document__c WHERE ID =: mapLinkedEntity.get('MDRM_Document__c') LIMIT 1];
                if(doc.MDRM_Send_for_Approval__c != Label.MDRM_Send_for_Approval_YES){
                    Delete lstCD;
                    Delete doc;
                }
                //Delete only images related with the current account
            }else if(mapLinkedEntity.get(this.strAccount) != null ){
                delete lstCD;
            }

            this.acc.MDRM_Business_case_files__c = this.acc.MDRM_Business_case_files__c.replace(title,'');
            update this.acc;
            init();
        }catch(Exception ex){
            System.debug('ERROR: '+ex);
            //if(!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
              this.isDone = false;
        }
    }
    
    public void sendDocToApproval(){
        try{
            
            //Validation before sent to approval
            boolean passValidations = TRUE;
            /*if(this.mapDocument.get(this.cvID).doc.MDRM_File_Type__c == 'Documento' && this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c == 'Contrato de Arrendamiento'){
                if(this.mapDocument.get(this.cvID).doc.MDRM_Signed_by_Legal_Department__c == FALSE){
                    this.mapDocument.get(this.cvID).doc.MDRM_Signed_by_Legal_Department__c.addError(Label.MDRM_signedAlert);
                    passValidations = FALSE;
                }
                if(this.mapDocument.get(this.cvID).doc.MDRM_Signed_by_Client__c == FALSE){
                    this.mapDocument.get(this.cvID).doc.MDRM_Signed_by_Client__c.addError(Label.MDRM_signedAlert);
                    passValidations = FALSE;
                }
            }*/
            if(this.mapDocument.get(this.cvID).doc.MDRM_File_Type__c == 'Documento' && this.mapDocument.get(this.cvID).doc.MDRM_File_Subtype__c == 'Licencia'){
                 if(this.mapDocument.get(this.cvID).doc.MDRM_Exp_Date__c == null){
                    this.mapDocument.get(this.cvID).doc.MDRM_Exp_Date__c.addError(Label.MDRM_RquiredField);
                    passValidations = FALSE;
                }
            }
            
            
            if(passValidations){
                if(this.mapDocument.get(this.cvID).doc.id == this.bc.id){
                    this.bc.MDRM_Send_for_Approval__c = Label.MDRM_Send_for_Approval_YES;
                }
                
                this.mapDocument.get(this.cvID).doc.MDRM_Send_for_Approval__c = Label.MDRM_Send_for_Approval_YES;
                MDRM_Document__c doc = this.mapDocument.get(this.cvID).doc;
                Update doc;
                this.isDone = true;
            }
        }catch(Exception ex){
            System.debug('ERROR: '+ex);
            if(!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            this.isDone = false;
        }
    }
    
    
    public class Documento{
        public ContentVersion cv {get;set;}
        public MDRM_Document__c doc {get;set;}
        public String Parent {get;set;}
        public Id LinkedEntityId {get;set;}
        
        public Documento(ContentVersion cv,MDRM_Document__c doc, Id LinkedEntityId){
            this.cv = cv;
            this.doc = doc;
            this.LinkedEntityId = LinkedEntityId;
            
            this.Parent = LinkedEntityId.getSObjectType().getDescribe().getName();
            if(this.Parent == 'MDRM_Document__c'){
                this.doc.MDRM_File_Type__c = 'Documento';
            }else if(this.Parent == 'Account' && this.cv.Description != null ){
                this.doc.MDRM_File_Type__c = 'Imagen';
            }
            this.doc.MDRM_File_Subtype__c = this.cv.Description;
        }
    }
    
}