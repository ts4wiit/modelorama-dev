/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
Comentarios ":
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Agosto-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
public class ISSM_AssignProvider_cls {
    
    public static void AssignProvider(List <Case> lstDataCaseNew,String strTypificationLevel3,String strTypificationLevel4){ 
        System.debug('#####ENTRO  ISSM_AssignProvider_cls - AssignProvider ' +lstDataCaseNew);
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        List<Account> lstAccounts = new List<Account>();
        List<User> lstUserGestor = new List<User>();
        List<Account> lstAccountsSalesOffice = new List<Account>();
        //String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(System.label.ISSM_Provider_RecordType).getRecordTypeId();
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Provider_RecordType);
        lstAccountsSalesOffice = objCSQuerys.QueryAccountSalesOffice(lstDataCaseNew);
        lstAccounts = objCSQuerys.QueryAccountList(RecordTypeAccountId,strTypificationLevel3,strTypificationLevel4,lstAccountsSalesOffice);
        System.debug('#####SELECt lstAccounts : '+lstAccounts);
        
        if(lstAccounts!= null && !lstAccounts.isEmpty()){ 
            lstUserGestor = objCSQuerys.QueryUser(lstAccounts);
            for(Case objCaseTrigger : lstDataCaseNew){
                for(Account objAccount : lstAccounts){
                    if(objAccount.ISSM_BossRefrigeration__c != null && String.IsnotBlank(objAccount.ISSM_BossRefrigeration__c)){
                         objCaseTrigger.ISSM_BossRefrigeration__c = objAccount.ISSM_BossRefrigeration__c;
                         objCaseTrigger.ISSM_RefrigerationRegional__c = lstUserGestor[0].ManagerId;
                    }
                    objCaseTrigger.ISSM_Provider__c = objAccount.Id;
                    objCaseTrigger.ISSM_ProviderEmail__c  =  objAccount.ONTAP__Email__c;
                    objCaseTrigger.ISSM_EmailProvider2__c  = objAccount.ISSM_Additional_Email__c;
                    objCaseTrigger.ISSM_EmailProvider3__c  = objAccount.ONCALL__Preferred_Contact_Email__c;
                    objCaseTrigger.EntitlementId  = objAccount.ISSM_ProviderEntitlement__c;
                    
                    System.debug('#####objCaseTrigger : '+objCaseTrigger);
                }
            }
            
        }else{
             List<ISSM_AppSetting_cs__c> lstIdQueueWithoutOwner =  objCSQuerys.QueryAppSettings();
            for(Case objCaseTrigger : lstDataCaseNew){
                objCaseTrigger.ISSM_Provider__c = lstIdQueueWithoutOwner[0].ISSM_IdProviderWithOutMail__c;
            }   
        }                                                           
    }   
}