/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex class to get the batches jobs of CDM and show it on a VF called 'CDM_ExecutionLog_vf'
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
public without sharing class CDM_ExecutionLog_ctr {
	Public static List<AsyncApexJob> lstJobs;
    Public static List<CDM_Logs__c> lstLogs;
    //Returns Current Jobs in the system (last 10)
    public static List<AsyncApexJob> getJobs(){
        lstJobs=[SELECT Id, CreatedBy.Name, Status,CreatedDate, CompletedDate, NumberOfErrors, ExtendedStatus, JobItemsProcessed,TotalJobItems, ApexClass.Name FROM AsyncApexJob WHERE (ApexClass.Name=:Label.CDM_Apex_MDM_ProcessRules_bch OR ApexClass.Name=:Label.CDM_Apex_CDM_UpdateRelaionships_bch OR ApexClass.Name=:Label.CDM_Apex_CDM_ReprocessBatch_bch OR ApexClass.Name=:Label.CDM_Apex_CDM_MassDeleteTaxes_bch)AND JobType=:Label.CDM_Apex_BatchApex ORDER BY CreatedDate DESC LIMIT 10];
        return lstJobs;
    }
    //Returns last 10 records of logs in CDM_Logs to show in the Visualforce
    public static List<CDM_Logs__c> getLogs(){
        lstLogs=[SELECT Id,Name, CDM_State__c,Credopor__c, CDM_StartDate__c, CDM_EndDate__c,CDM_Failure__c, CDM_Total__c, CDM_BatchProc__c,CDM_Details__c FROM CDM_Logs__c  ORDER BY CDM_StartDate__c DESC LIMIT 10];
        return lstLogs;
    }
    //Returns Last 10 records of logs to refresh the data in the VF
    public static set<String> oldLoglsts(){
        lstLogs = !Test.isRunningTest() ? [SELECT Id,Name,BatchId__c, CDM_State__c, Credopor__c, CDM_StartDate__c,CDM_EndDate__c,CDM_Total__c,CDM_BatchProc__c,CDM_Details__c FROM CDM_Logs__c ] : lstLogs;
       	set<String> setBatchId = new set<String>();
        for(CDM_Logs__c log: lstLogs){
          setBatchId.add(String.valueOf(log.BatchId__c) ); 
        }
        return setBatchId;
    }
    //Returns all logs to update them status
    public static List<CDM_Logs__c> oldLogs(){
        lstLogs=[SELECT Id,Name,BatchId__c, CDM_State__c, Credopor__c, CDM_StartDate__c,CDM_EndDate__c,CDM_Failure__c,CDM_Total__c,CDM_BatchProc__c,CDM_Details__c FROM CDM_Logs__c ];
        return lstLogs;
    }
    //Method to update Visualforce Data
    public static void save(){
       List<AsyncApexJob> lstJobs = !Test.isRunningTest() ? getJobs(): lstJobs;
       List<CDM_Logs__c> lstLogs  = new List<CDM_Logs__c>();
       Set<String> oldBatchs  	  = oldLoglsts(); 
       List<CDM_Logs__c> oldLogs  = oldLogs();
        for(AsyncApexJob i: lstJobs){
            String className='';
            String errorMsg='';
            if(i.ApexClass.Name==Label.CDM_Apex_MDM_ProcessRules_bch){
                className = Label.CDM_RuleExecution;
            } else if(i.ApexClass.Name== Label.CDM_Apex_CDM_UpdateRelaionships_bch){
                className = Label.CDM_Relate;
            } else if(i.ApexClass.Name== Label.CDM_Apex_CDM_ReprocessBatch_bch){
                className = Label.CDM_Reprocess;
            } else if(i.ApexClass.Name== Label.CDM_Apex_CDM_MassDeleteTaxes_bch){
                className = Label.CDM_MassDelete;
            }
            
            if(i.ExtendedStatus!=null){
             if(i.ExtendedStatus== Label.CDM_Apex_Error1){
                errorMsg= Label.CDM_ErrorMsg1;
             }	else if(i.Status!= Label.CDM_Apex_Error3){
                errorMsg=Label.CDM_ErrorMsg3;
             } else if (i.Status!= Label.CDM_Apex_Error2){
                errorMsg=Label.CDM_ErrorMsg2;
            }   
            }
            
            if(oldBatchs.size()>0){
                    if(!oldBatchs.contains(i.Id)){
                         	CDM_Logs__c obj = new CDM_Logs__c();
            				obj.BatchId__c = i.Id;
                        	obj.Name		=className ;
            				obj.CDM_State__c= i.Status;
            				obj.Credopor__c = i.CreatedBy.Name;
            				obj.CDM_StartDate__c = String.valueOf(i.CreatedDate);
            				obj.CDM_EndDate__c = String.valueOf(i.CompletedDate);
            				obj.CDM_Total__c = String.valueOf(i.TotalJobItems);
                        	obj.CDM_Failure__c = String.valueOf(i.NumberOfErrors);
            				obj.CDM_BatchProc__c = String.valueOf(i.JobItemsProcessed);
            				obj.CDM_Details__c = errorMsg;
            				lstLogs.add(obj);
                    } else{
                        for(CDM_Logs__c log:oldLogs){
                            if(i.Id== log.BatchId__c){
                              	log.BatchId__c = i.Id;
                                log.Name		=className;
            					log.CDM_State__c= i.Status;
            					log.Credopor__c = i.CreatedBy.Name;
            					log.CDM_StartDate__c = String.valueOf(i.CreatedDate);
            					log.CDM_EndDate__c = String.valueOf(i.CompletedDate);
            					log.CDM_Total__c = String.valueOf(i.TotalJobItems);
                                log.CDM_Failure__c = String.valueOf(i.NumberOfErrors);
            					log.CDM_BatchProc__c = String.valueOf(i.JobItemsProcessed);
            					log.CDM_Details__c = errorMsg; 
                            }
                        }
                        
                    }
            } else{
                CDM_Logs__c obj = new CDM_Logs__c();
            				obj.BatchId__c = i.Id;
                			obj.Name		=className;
            				obj.CDM_State__c= i.Status;
            				obj.Credopor__c = i.CreatedBy.Name;
            				obj.CDM_StartDate__c = String.valueOf(i.CreatedDate);
            				obj.CDM_EndDate__c = String.valueOf(i.CompletedDate);
            				obj.CDM_Total__c = String.valueOf(i.TotalJobItems);
            				obj.CDM_BatchProc__c = String.valueOf(i.JobItemsProcessed);
            				obj.CDM_Details__c = errorMsg;
            				lstLogs.add(obj);
            }  
        }
        
        if(lstLogs.size()>0){
            try{
                insert lstLogs;
            }  catch (DmlException e) {
                
            }
        }
            update oldLogs;
    }
}