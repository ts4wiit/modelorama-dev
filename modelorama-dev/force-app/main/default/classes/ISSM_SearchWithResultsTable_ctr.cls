/**
 * Desarrollado por:	Avanxo México
 * Autor:				Carlos Pintor / cpintor@avanxo.com
 * Proyecto:			AbInbev - Trade Revenue Management
 * Descripción:			Clase Controlador Apex del componente Lightning 'ISSM_SearchWithResultsTable_lcp'.
 * 						Incluye métodos para obtener la información del servidor.
 *
 * No.    Fecha            Autor                      Descripción
 * 1.0    07-Mayo-2018     Carlos Pintor              Creación
 *
 */
public with sharing class ISSM_SearchWithResultsTable_ctr {
    /**
    * @description  Receives a serie of parameters to create a SOQL query in order to retrieve records from the server, 
    *               the results are returned in 'RecordWrapper' type
    * 
    * @param    deselectedRecordIds     List of Salesforce Ids of records already deselected in the front end (LEx Component)
    * @param    fieldList               API Name of fields to Select from the server
    * @param    objectType              API Name of the Object to search for
    * @param    fieldOrderByList        API Name of fields to Order By
    * @param    numberOfRowsToReturn    Max number of records to query
    * @param    recordType              Record Type Developer Name to query on records
    * @param    parentRecordFieldName   API Name of the field with the relationship to the parent record
    * @param    parentRecordIdList      List of Salesforce Ids of parent records to match
    * @param    priceGroupList			List of MDM_Parameter__c records selected for segments
    * 
    * @return   Return a list of type 'RecordWrapper' with pairs of boolean (selected) and sObject record
    */
    @AuraEnabled
    public static List<RecordWrapper> getRecordWrapperList(List<String> deselectedRecordIds,
                                                            String fieldList, 
                                                            String objectType, 
                                                            String fieldOrderByList, 
                                                            String numberOfRowsToReturn, 
                                                            String recordType, 
                                                            String parentRecordFieldName, 
                                                            String[] parentRecordIdList, 
                                                            MDM_Parameter__c[] priceGroupList ){
        System.debug('####START getRecordWrapperList');
        String sParentRecordIdList = '';
        if( parentRecordIdList.size() > 0 ){
            sParentRecordIdList = '(' + String.join( parentRecordIdList, ',' ) +')';
        }
        //String createdQuery = createQuery(fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, '', false, recordType, parentRecordFieldName, parentRecordIdList, priceGroupList);
        String createdQuery = createQuery(fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, '', false, recordType, parentRecordFieldName, sParentRecordIdList, parentRecordIdList, priceGroupList);
        
        List<RecordWrapper> returnList = new List<RecordWrapper>();
        List<sObject> records = ISSM_UtilityFactory_cls.executeQuery(createdQuery);
        for(sObject o : records){
            RecordWrapper recWpr = new RecordWrapper();
            recWpr.record = o;
            
            //if( deselectedRecordIds.size() > 0 ){
                
                Boolean found = false;
                for(String idString : deselectedRecordIds){
                if(!found){
                    found = o.Id == idString ? true : false;
                }
            }
            recWpr.selected = found == true ? false : true;
            
            returnList.add(recWpr);
        }
        System.debug('####returnList.size:' + returnList.size());
        return returnList;
    }
    
    /**
    * @description  Receives a serie of parameters to create a SOQL query in order to retrieve records from the server, 
    *               the keyWord parameter indicates the word to search in the specific fields (fieldList),
    *               if retrieved record ids are in list 'deselectedRecordIds', then the boolean 'selected' is set to false,
    *               the results are returned in 'RecordWrapper' type
    * 
    * @param    deselectedRecordIds     List of Salesforce Ids of records already deselected in the front end (LEx Component)
    * @param    fieldList               API Name of fields to Select from the server
    * @param    objectType              API Name of the Object to search for
    * @param    fieldOrderByList        API Name of fields to Order By
    * @param    numberOfRowsToReturn    Max number of records to query
    * @param    keyWord                 String variable with the search box input
    * @param    searchByList			Boolean variable to indicate if the user is searching a list of Ids
    * @param    recordType              Record Type Developer Name to query on records
    * @param    parentRecordFieldName   API Name of the field with the relationship to the parent record
    * @param    parentRecordIdList      List of Salesforce Ids of parent records to match
    * @param    priceGroupList			List of MDM_Parameter__c records selected for segments
    * 
    * @return   Return a list of type 'RecordWrapper' with pairs of boolean (selected) and sObject record
    */
    @AuraEnabled
    public static List<RecordWrapper> searchByKeyWord(List<String> deselectedRecordIds, 
                                                        String fieldList, 
                                                        String objectType, 
                                                        String fieldOrderByList, 
                                                        String numberOfRowsToReturn, 
                                                        String keyWord, 
                                                        Boolean searchByList,
                                                        String recordType, 
                                                        String parentRecordFieldName, 
                                                        String[] parentRecordIdList, 
                                                        MDM_Parameter__c[] priceGroupList) {
        System.debug('####START searchByKeyWord');
        List<RecordWrapper> returnList = new List<RecordWrapper>();
        Boolean doQuery = searchByList == false && keyWord != null && keyWord.length() > Integer.valueOf(Label.TRM_KeywordMaxLength) ? false : true;
        String sParentRecordIdList = '';
        if( parentRecordIdList.size() > 0 ){
            sParentRecordIdList = '(' + String.join( parentRecordIdList, ',' ) +')';
        }
        if(keyWord != null && doQuery){

            String createdQuery = createQuery(fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, keyWord, searchByList, recordType, parentRecordFieldName, sParentRecordIdList, parentRecordIdList, priceGroupList );
        
            List<sObject> records = ISSM_UtilityFactory_cls.executeQuery(createdQuery);
            for(sObject o : records){
                Boolean found = false;
                RecordWrapper recWpr = new RecordWrapper();
                recWpr.record = o;
                if(!searchByList){
                for(String idString : deselectedRecordIds){
                    if(!found){
                        found = o.Id == idString ? true : false;
                    }
                }
                }
                recWpr.selected = found == true ? false : true;
                returnList.add(recWpr);
            }
            System.debug('#### CPUTime: ' + Limits.getCpuTime() );
            System.debug('#### LimitCPUTime: ' + Limits.getLimitCpuTime());

        }
        
        System.debug('####returnList.size:' + returnList.size());
        return returnList;
    }
    
    /**
    * @description  Receives a serie of parameters to create a SOQL query string 
    * 
    * @param    fieldList               API Name of fields to Select from the server
    * @param    objectType              API Name of the Object to search for
    * @param    fieldOrderByList        API Name of fields to Order By
    * @param    numberOfRowsToReturn    Max number of records to query
    * @param    keyWord                 String variable with the search box input
    * @param    searchByList            Boolean variable to indicate if the user is searching a list of Ids
    * @param    recordType              Record Type Developer Name to query on records
    * @param    parentRecordFieldName   API Name of the field with the relationship to the parent record
    * @param    sParentRecordIdList     String with the list of Salesforce Ids of parent records to match
    * @param    parentRecordIdList      List of Salesforce Ids of parent records to match
    * @param    priceGroupList			List of MDM_Parameter__c records selected for segments
    * 
    * @return   Return a string with the SOQL Query created 
    */
    public static String createQuery(
        String fieldList, 
        String objectType, 
        String fieldOrderByList, 
        String numberOfRowsToReturn, 
        String keyWord,
        Boolean searchByList,
        String recordType,
        String parentRecordFieldName,
        String sParentRecordIdList,
        String[] parentRecordIdList,
        MDM_Parameter__c[] priceGroupList
    ){
        Boolean firstConditionAdded = false;
        String sQuery = 'SELECT Id, ISSM_SegmentCode__c, ISSM_SalesOffice__c, ISSM_SalesOrg__c, ISSM_RegionalSalesDivision__c, ISSM_ParentAccount__c, ISSM_ParentAccount__r.ISSM_ParentAccount__c, ' + fieldList;
               sQuery += ' FROM ' + objectType;
        	   sQuery += ' WHERE';
        //Record Type filter
        if( !recordType.equals('') ){//optional
               sQuery += ' RecordType.DeveloperName = \'' + recordType + '\'';
               firstConditionAdded = true;
        }
        //Here was the Parent field filter
        //Search word filter
        if( !keyWord.equals('') && !fieldList.equals('') ){
            if( searchByList == true ){
                List<String> keyWordList = String.escapeSingleQuotes(keyWord).split(' ');
                List<String> sKeyWordList = new List<String>();
                for(String kw : keyWordList){
                    String s = '\''+ kw +'\'';
                    sKeyWordList.add(s);
                }
                //sQuery += ' AND ONTAP__SAP_Number__c IN ' + sKeyWordList;
                sQuery += ' AND ONTAP__SAP_Number__c IN (' + String.join(sKeyWordList, ',') + ')';
            } else {
                sQuery += ' AND (';
                String sKeyWord = '%' + String.escapeSingleQuotes(keyWord) + '%';
                List<String> field_list = fieldList.split(',');
                List<String> token_list = new List<String>();
                for(String fieldName : field_list){
                    String token = fieldName + ' LIKE \'' + skeyWord + '\'';
                    token_list.add(token);
                }
                sQuery += ' ' + String.join(token_list, ' OR ');
                sQuery += ')';
            }
               
        }
        //Parent field filter
        if( !String.isEmpty(sParentRecordIdList) ){
            if( firstConditionAdded ){
                sQuery += ' AND';
            }
                sQuery += ' ' + parentRecordFieldName + ' IN ' + sParentRecordIdList;
        }
        //Price group filter
        List<String> priceGroupIdList = ISSM_UtilityFactory_cls.createLstItems(priceGroupList,'Id');
        System.debug('####priceGroupIdList:'+priceGroupIdList);
        if( priceGroupIdList.size() > 0 ){
            String sPriceGroupIdList = '(' + String.join( priceGroupIdList, ',' ) +')';
               sQuery += ' AND ISSM_SegmentCode__c IN ' + sPriceGroupIdList;
        }
        
        	   sQuery += ' ORDER BY ' + fieldOrderByList;
        	   sQuery += ' LIMIT ' + numberOfRowsToReturn;
        System.debug('####createdQuery: ' + sQuery);
        return sQuery;
    }
    
    public class RecordWrapper{
        @AuraEnabled public sObject record {get;set;}
        @AuraEnabled public Boolean selected {get;set;}
        //@AuraEnabled public Boolean activeValue {get;set;}
    }

}