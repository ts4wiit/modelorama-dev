/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 * 
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_ReadAttachmentCloseCase_tst {

    static testMethod void RecordSucces() {  
    	String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
       	//Insertamos en el objeto ISSM_MassCloseCase__c
        ISSM_MassCloseCase__c objMassCloseCase = new  ISSM_MassCloseCase__c();
		objMassCloseCase.ISSM_Status__c=null;
		List<ISSM_MassCloseCase__c > lstMassCloseCase = new List<ISSM_MassCloseCase__c >();
		lstMassCloseCase.add(objMassCloseCase);      
        insert lstMassCloseCase;
		
		//Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;   

        //Creamos Cuenta
        Account  objAccount = new Account(); 
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
            insert objAccount;
		//Creamos el caso para que podamos asociar a la tarea
        Case objCase = new Case();
                objCase.Subject = 'Subject';
                objCase.Description = 'Description'; 
                objCase.Status = 'new';
                objCase.ISSM_TypificationLevel1__c='Refrigeración';
                objCase.ISSM_TypificationLevel2__c='Mantenimiento';
                objCase.ISSM_TypificationLevel3__c ='Barril';
                objCase.ISSM_TypificationLevel4__c ='Tanque CO2';
                objCase.ISSM_TypificationLevel5__c = null;
                objCase.ISSM_TypificationLevel6__c = null;
                objCase.AccountID = objAccount.Id;
                insert objCase; 
                List<Case>lstcase =[SELECT id,CaseNumber FROM Case WHERE id =: objCase.Id];
                System.debug('CaseNumber : '+lstcase[0].CaseNumber);
                
        String stAttachment = '﻿Numero de caso,Fecha y hora de cierre,Descripcion,Cierre Efectivo\n'+
        						lstcase[0].CaseNumber+',2017-10-04 11:12:00,HDH1,SERVICIO CONCLUIDO';
		blob blbCSVBlobRecordOk = Blob.valueOf(stAttachment); 
		//Creamos el attachment
		Attachment objAttachment = new Attachment();
		objAttachment.Name =  'ReocrdSuccess';
		objAttachment.ParentId = lstMassCloseCase[0].id;
		objAttachment.body = blbCSVBlobRecordOk;
		insert objAttachment;
		
		
        Test.startTest();
        	SchedulableContext sc = null;
            ISSM_ReadAttachmentCloseCase_sh obj = new ISSM_ReadAttachmentCloseCase_sh();
            obj.execute(sc);
             
            Database.BatchableContext bc = null;
            ISSM_ReadAttachmentCloseCase_cls objReadAttachmentCloseCase = new ISSM_ReadAttachmentCloseCase_cls();
            objReadAttachmentCloseCase.execute(bc,lstMassCloseCase);
        Test.stopTest();
        
    }
    
    static testMethod void RecordWidthError() {  
       	//Insertamos en el objeto ISSM_MassCloseCase__c
        ISSM_MassCloseCase__c objMassCloseCase = new  ISSM_MassCloseCase__c();
		objMassCloseCase.ISSM_Status__c=null;
		List<ISSM_MassCloseCase__c > lstMassCloseCase = new List<ISSM_MassCloseCase__c >();
		lstMassCloseCase.add(objMassCloseCase);      
        insert lstMassCloseCase;

        String stAttachment = '﻿Numero de caso,Fecha y hora de cierre,Descripcion,Cierre Efectivo\n'+
        						'372671,2017-10-04 11:12:00,HDH1,SERVICIO CONCLUIDO\n'+
        						',2017-10-04 11:12:00,Hector 1,SERVICIO CONCLUIDO\n'+
								'3726xx,2017-10-04 11:12:00,Hecto2,SERVICIO CONCLUIDO';
		blob blbCSVBlobRecordOk = Blob.valueOf(stAttachment); 
		//Creamos el attachment
		Attachment objAttachment = new Attachment();
		objAttachment.Name =  'RecordWidthError';
		objAttachment.ParentId = lstMassCloseCase[0].id;
		objAttachment.body = blbCSVBlobRecordOk;
		insert objAttachment;
		
		
        Test.startTest();
        	SchedulableContext sc = null;
            ISSM_ReadAttachmentCloseCase_sh obj = new ISSM_ReadAttachmentCloseCase_sh();
            obj.execute(sc);
             
            Database.BatchableContext bc = null;
            ISSM_ReadAttachmentCloseCase_cls objReadAttachmentCloseCase = new ISSM_ReadAttachmentCloseCase_cls();
            objReadAttachmentCloseCase.execute(bc,lstMassCloseCase);
        Test.stopTest();
        
    }
        static testMethod void RecordErrorinDate() {  
       	//Insertamos en el objeto ISSM_MassCloseCase__c
        ISSM_MassCloseCase__c objMassCloseCase = new  ISSM_MassCloseCase__c();
		objMassCloseCase.ISSM_Status__c=null;
		List<ISSM_MassCloseCase__c > lstMassCloseCase = new List<ISSM_MassCloseCase__c >();
		lstMassCloseCase.add(objMassCloseCase);      
        insert lstMassCloseCase;
		
        String stAttachment = '﻿Numero de caso,Fecha y hora de cierre,Descripcion,Cierre Efectivo\n'+
        						'372465,2017/10/04 11:12:00,Hector 1,SERVICIO CONCLUIDO\n'+
								'372465,2017/10/04 11:12:00,Hecto2,SERVICIO CONCLUIDO';
		blob blbCSVBlobRecordOk = Blob.valueOf(stAttachment); 
		//Creamos el attachment
		Attachment objAttachment = new Attachment();
		objAttachment.Name =  'RecordErrorinDate';
		objAttachment.ParentId = lstMassCloseCase[0].id;
		objAttachment.body = blbCSVBlobRecordOk;
		insert objAttachment;
		
		
        Test.startTest();
        	SchedulableContext sc = null;
            ISSM_ReadAttachmentCloseCase_sh obj = new ISSM_ReadAttachmentCloseCase_sh();
            obj.execute(sc);
             
            Database.BatchableContext bc = null;
            ISSM_ReadAttachmentCloseCase_cls objReadAttachmentCloseCase = new ISSM_ReadAttachmentCloseCase_cls();
            objReadAttachmentCloseCase.execute(bc,lstMassCloseCase);
        Test.stopTest();
      
        
    } 
	static testMethod void RecordErrorinColumn() {  
       	//Insertamos en el objeto ISSM_MassCloseCase__c
        ISSM_MassCloseCase__c objMassCloseCase = new  ISSM_MassCloseCase__c();
		objMassCloseCase.ISSM_Status__c=null;
		List<ISSM_MassCloseCase__c > lstMassCloseCase = new List<ISSM_MassCloseCase__c >();
		lstMassCloseCase.add(objMassCloseCase);      
        insert lstMassCloseCase;

        String stAttachment = '﻿NumeroCaso,Fechhora de cierre,Descripcion,Cierre Efectivo\n'+
        						'372465,2017-10-04 11:12:00,Hector 1,SERVICIO CONCLUIDO\n'+
								'372465,2017-10-04 11:12:00,Hecto2,SERVICIO CONCLUIDO';
		blob blbCSVBlobRecordOk = Blob.valueOf(stAttachment); 
		//Creamos el attachment
		Attachment objAttachment = new Attachment();
		objAttachment.Name =  'RecordErrorinColumn';
		objAttachment.ParentId = lstMassCloseCase[0].id;
		objAttachment.body = blbCSVBlobRecordOk;
		insert objAttachment;
		
		
        Test.startTest();
        	SchedulableContext sc = null;
            ISSM_ReadAttachmentCloseCase_sh obj = new ISSM_ReadAttachmentCloseCase_sh();
            obj.execute(sc);
             
            Database.BatchableContext bc = null;
            ISSM_ReadAttachmentCloseCase_cls objReadAttachmentCloseCase = new ISSM_ReadAttachmentCloseCase_cls();
            objReadAttachmentCloseCase.execute(bc,lstMassCloseCase);
        Test.stopTest();
        
    }
}