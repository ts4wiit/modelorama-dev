/**
 * Test Class for AllMobileVersionHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileVersionHelperTest {

	/**
	 * Method to setup data.
	 */
	public static testMethod void setup() {

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;

		//Creating an Application Autoventa.
		AllMobileApplication__c objAllMobileApplication2 = AllMobileUtilityHelperTest.createAllMobileApplicationObject('AUTOVENTA', '2');
		insert objAllMobileApplication2;

		//Creating a Version Preventa.
		AllMobileVersion__c objAllMobileVersion = AllMobileUtilityHelperTest.createAllMobileVersionObject('1.0.0', objAllMobileApplication);
		insert objAllMobileVersion;

		//Updating version.
		AllMobileVersion__c objAllMobileVersion2 = [SELECT Id, Name, AllMobileApplicationMD__c FROM AllMobileVersion__c WHERE Id =: objAllMobileVersion.Id];
		objAllMobileVersion2.AllMobileApplicationMD__c = objAllMobileApplication2.Id;
		update objAllMobileVersion2;
	}
}