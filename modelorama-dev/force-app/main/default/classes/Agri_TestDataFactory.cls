@isTest
public class Agri_TestDataFactory {

    public static testMethod Agri_Material__c getMaterial() {
        Agri_Material__c am = new Agri_Material__c();
        am.Name = 'Test';
        am.Agri_Type__c = 'Semilla';
        return am;
    }
    
    public static testMethod Account getAgricultor() {
        Account ag = new Account();
        ag.Name = 'Test';
        ag.ONTAP__Email__c = 'test@domain.com';
        return ag;
    }
    
    public static testMethod Agri_Warehouse__c getAlmacen() {
    	Agri_Warehouse__c aw = new Agri_Warehouse__c();
        aw.Name = 'Almacen 1';
        aw.Agri_ls_warehouseCode__c = 'IASA';
        aw.Agri_ls_warehouseState__c = 'Activo';
        aw.Agri_tx_centre__c = 'IA01';
        aw.Agri_nu_totalCapacity__c = 400;
        aw.Agri_nu_totalDelivered__c = 1;
        aw.Agri_nu_contractedCapacity__c = 100;
        aw.Agri_fm_percentageSurplus__c = 10;
        return aw;
    }
    
    public static testMethod Agri_Contract__c getContrato() {
        Agri_Contract__c ac = new Agri_Contract__c();
        ac.Agri_Bank_Account_Modelo__c = '12345678901';
        ac.Agri_Name_of_Bank_Account_Modelo__c = '021 HSBC';
        ac.Agri_Bank_Reference_Number__c = '12345678901';
        ac.Agri_Contract_Number_SAP__c = '45000000123';
        ac.Agri_Tons_to_Produce__c = 50;
        return ac;
    }
    
    public static testMethod Delivery_Order__c getOrdenEntrega() {
        Delivery_Order__c dor = new Delivery_Order__c();
		dor.Agri_Surface__c = 120;
        dor.Agri_Cycle__c = 'OI18';
        dor.Agri_Estimated_weight__c = 10;
        dor.Agri_Category__c = 'Declarada';
        dor.Agri_Delivery_date__c = Date.today();
        dor.Agri_Observations__c = 'obs';
        dor.Agri_ls_status__c = 'Nueva';
        return dor;
    }
    
    public static testMethod Agri_receiving_orders__c getEntrega() {
        Agri_receiving_orders__c ro = new Agri_receiving_orders__c();
        ro.Name = 'Folio';
        ro.Agri_ls_status__c = 'Recibido';
        return ro;
    }
    
    public static testMethod Agri_Quality_Control__c getQA() {
        Agri_Quality_Control__c qa = new Agri_Quality_Control__c();
        qa.Agri_ls_Result_of_quality_analysis__c = 'Aprobado';
        qa.Agri_nu_grossWeight__c = 2;
        qa.Agri_nu_tare__c = 1;
        qa.Agri_nu_netWeight__c = 1;
        return qa;
    }
    
}