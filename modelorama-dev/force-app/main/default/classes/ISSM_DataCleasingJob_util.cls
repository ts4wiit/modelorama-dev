/**************************************************************************************
Name apex class: ISSM_DataCleasingJob_util
Version: 1.0
Createdate: 29 June 2018
Functionality: Class to process methods of utilities
Test class: ISSM_DataCleasingJob_tst
Modifications history:
-----------------------------------------------------------------------------
* Developer            -       Date       -        Description
* ----------------------------------------------------------------------------
* Carlos Duque         26 - September - 2017      Original version
* Leopoldo Ortega        29 - June - 2018         Add methods "sendEmail" and "parseArrayRecordType"
*************************************************************************************/
public class ISSM_DataCleasingJob_util {
    // Method to process objects of the custom settings to delete data
    public static void sendEmail(String email_str, String process_str, String error_str) {
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        String[] sendingTo_str = new String[]{email_str};
            semail.setToAddresses(sendingTo_str);
        semail.setSubject(System.Label.ISSM_EmailSubject);
        semail.setPlainTextBody(System.Label.ISSM_EmailBody + ': ' + process_str + ' - Error: ' + error_str);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
    }
    
    // Method to process array and convert to string
    public static String parseArrayRecordType(String[] arrayRecordType) {
        String string_str = '';
        Set<String> string_set = new Set<String>();
        if (arrayRecordType != null) {
            for (String reg : arrayRecordType) {
                string_str = string_str += '\'' + reg + '\',';
                string_set.add(reg);
            }
            string_str = string_str.removeEnd(',');
        }
        // Return string of record type found
        return string_str;
    }
}