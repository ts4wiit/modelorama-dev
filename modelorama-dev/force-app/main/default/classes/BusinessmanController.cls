public class BusinessmanController {
    
    ///////// ///////// ///////// ///////// ///////// ///////// ///////// GETTERS & SETTERS///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// 
    
    public List<SelectOption> genderlist{get; set;}
    
    public List<MDRM_Vinculo__c> listaModeloramas{get; set;}
    public List<MDRM_Vinculo__c> empresarioConModeloramas{get;set;}
    public List<MDRM_Businessman_Expansor__c> listaModeloramasExpansor{get;set;}
    public List<Account> model{get;set;}
    public List<MDRM_Employee__c> empleados{get;set;}
    public List<MDRM_form__c> survey{get;set;}
    public List<Account> verModelorama{get;set;}
    public Account empresario{get;set;}
    public MDRM_Employee__c employee { get; set;}
    
    public MDRM_AccountWrapped accWrap {get; set;}
    public MDRM_AccountWrapped accountWrap {get; set;}
    public List<MDRM_VinculoWrapped> wrapListaModelorama{get;set;}
    public MDRM_AccountWrapped busWrap {get; set;}
    
    public String sapid {get; set;}
    public String codigoModelorama {get; set;}
    
    public Boolean isEditable {get; private set;}
    public Boolean isNewValue {get; private set;}
    public Boolean isNewMdrm {get; private set;}
    public Boolean miModelorama {get; private set;}
    public Boolean miEmpleado {get; private set;}
    public Boolean mostrarMdrm {get; private set;}
    public Boolean IsDisabled {get; set;}
    
    public String inputTextErrorMessage {get; set;}
    public String inputTextErrorMessageBussy {get; set;}
    public String inputTextErrorMessageGenero {get; set;}
    public String inputTextErrorMessageFecha{get; set;}
    public String inputTextErrorTotal {get; set;}
    public String inputTextErrorLastModelorama {get; set;}
    public String parametroModelorama {get; set{parametroModelorama = value;}}
    public String parametroEmpleado {get; set;}
    public Boolean validationFields {get; private set;}
    
    public String PhoneError{get; set;}
    public String PostalCodeError{get; set;}
    public String RequieredProvinceError{get; set;}
    public String RequieredMunicipalityError{get; set;}
    public String RequieredColonyError{get; set;}
    public String RequieredStreetError{get; set;}
    public String RequieredStreetNumberError{get; set;}
    
    public List<Contact> contactBusinessman{get; set;}
    public List<MDRM_Form__c> userBusinessman{get; set;}
    
    //////////////////////////Survey////////////////////////////////////
    
    public String preguntaUno {get; set;}
    public String preguntaDos {get; set;}
    public String preguntaTres {get; set;}
    public String preguntaCuatro {get; set;}
    public String preguntaCinco {get; set;}
    public String preguntaCincoUno {get; set;}
    public String preguntaCincoDos {get; set;}
    public String labelCincoDos {get; set;}
    public String preguntaCincoTres {get; set;}
    public String labelCincoTres {get; set;}
    public String preguntaSeis {get; set;}
    public String preguntaSiete {get; set;}
    public String preguntaOcho {get; set;}
    public String preguntaNueve {get; set;}
    public String preguntaDiez {get; set;}
    public String preguntaOnce {get; set;}
    public String preguntaDoce {get; set;}
    public String preguntaTrece {get; set;}
    
    ///////// ///////// ///////// ///////// ///////// ///////// /////////CLASS VALUES ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// /////////    
    
    public BusinessmanController(){
        
        Schema.DescribeFieldResult fieldResult = MDRM_Employee__c.MDRM_Gender__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        genderlist = new List<SelectOption>();
        for (Schema.PicklistEntry f: ple) {
            genderlist.add( new SelectOption(f.getLabel(), f.getValue()) );
        }
        
        employee = new MDRM_Employee__c();
        isEditable = true;
        isNewValue = true;
        inputTextErrorMessage = '';
        isNewMdrm = true;
        miModelorama = true;
        miEmpleado = true;
        mostrarMdrm = true;
        validationFields = true;
        IsDisabled=true;
        
    }
    
    ///////// ///////// ///////// ///////// ////////// //////////PAGE BUTTON REFERENCES AND METHODS/ ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////////////////////////////////////////
    
    public pageReference buscarMiModelorama()
    {
        clearMessages();
        if(codigoModelorama == null || codigoModelorama == '')
            codigoModelorama = apexpages.currentPage().getParameters().get('MDRM_Z019');
            //codigoModelorama = apexpages.currentPage().getHeaders().get('MDRM_Z019');
        if(codigoModelorama != null && codigoModelorama !=''){
            String newEmail = apexpages.currentPage().getParameters().get('MDRM_Email');
            //String newEmail = apexpages.currentPage().getHeaders().get('MDRM_Email');
            
            empresarioConModeloramas= new List<MDRM_Vinculo__c>([SELECT MDRM_Businessman__r.Id FROM MDRM_Vinculo__c WHERE MDRM_Expansor__r.z019__c =: codigoModelorama]);
            if(!empresarioConModeloramas.isEmpty() || empresario != null)
            {
                contactBusinessman = new List<Contact>(); 
                userBusinessman = new List<MDRM_Form__c>();
                
                if(!empresarioConModeloramas.isEmpty() && empresario == null){
                    empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                                  ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                                  ONTAP__Email__c,
                                  (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                                  FROM Account 
                                  WHERE Id =: empresarioConModeloramas[0].MDRM_Businessman__c];
                }
                if(empresario != null){
                    empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                                  ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                                  ONTAP__Email__c,
                                  (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                                  FROM Account 
                                  WHERE Id =: empresario.Id];
                    
                    if(newEmail != null && newEmail != ''){
                        empresario.ONTAP__Email__c = newEmail;
                        update empresario;
                    }
                    // Llamada al metodo del wrapper
                    accWrap = wrapBusinessman(empresario);
                    
                    contactBusinessman = empresario.Contacts;
                    userBusinessman = empresario.Forms__r;
                    
                    listaModeloramas= new List<MDRM_Vinculo__c>([SELECT MDRM_Expansor__r.z019__c, MDRM_Expansor__r.Name, MDRM_Expansor__r.ONTAP__Province__c, 
                                                                 MDRM_Expansor__r.ONTAP__PostalCode__c, MDRM_Expansor__r.ONTAP__Municipality__c, 
                                                                 MDRM_Expansor__r.ONTAP__Colony__c, MDRM_Expansor__r.ONTAP__Street__c, 
                                                                 MDRM_Expansor__r.ONTAP__Street_Number__c
                                                                 FROM MDRM_Vinculo__c 
                                                                 WHERE MDRM_Businessman__r.Id =: empresario.id
                                                                 AND MDRM_Expansor__c != null]);
                    // Llmamada al metodo wraper
                    wrapListaModelorama = wrapListaModelorama(listaModeloramas);
                }
            }
            else {
                
                listaModeloramasExpansor= new List<MDRM_Businessman_Expansor__c>([SELECT MDRM_Businessman__c, MDRM_Expansor__c
                                                                                  FROM MDRM_Businessman_Expansor__c  
                                                                                  WHERE MDRM_Expansor__r.z019__c =: codigoModelorama ]);
                
                if(listaModeloramasExpansor.isEmpty())
                {
                    PageReference pr = Page.MDRM_ErrorPage; 
                    return pr;
                }
                
                List<MDRM_Vinculo__c> oldVinculos = new List<MDRM_Vinculo__c>();
                
                if(listaModeloramasExpansor.size() > 0){
                    if(listaModeloramasExpansor[0].MDRM_Businessman__c != null){
                        oldVinculos= [SELECT Id FROM MDRM_Vinculo__c WHERE  MDRM_Businessman__r.Id =: listaModeloramasExpansor[0].MDRM_Businessman__c];
                    }
                }
                
                if(!oldVinculos.isEmpty()){
                    PageReference pr = Page.MDRM_ErrorPage; 
                    return pr;
                }
                
                List<MDRM_Vinculo__c> newVinculos = new List<MDRM_Vinculo__c>();
                List<Id> modeloramasIds = new List<Id>();
                List<MDRM_Businessman_Expansor__c> expansors = new List<MDRM_Businessman_Expansor__c>([SELECT MDRM_Expansor__r.Name, MDRM_Expansor__c 
                                                                                                       FROM MDRM_Businessman_Expansor__c 
                                                                                                       WHERE MDRM_Businessman__c =: listaModeloramasExpansor[0].MDRM_Businessman__c]);
                for(MDRM_Businessman_Expansor__c expansor : expansors){
                    modeloramasIds.add(expansor.MDRM_Expansor__c);
                    MDRM_Vinculo__c newVinculo = new MDRM_Vinculo__c(MDRM_Businessman__c = listaModeloramasExpansor[0].MDRM_Businessman__c, MDRM_Expansor__c = expansor.MDRM_Expansor__c );
                    newVinculos.add(newVinculo);
                }
                
                List<Account> modeloramasUpdate = new List<Account>([SELECT MDRM_Used__c, Id 
                                                                     FROM Account 
                                                                     WHERE Id IN: modeloramasIds]); 
                for(Account up : modeloramasUpdate){
                    up.MDRM_Used__c = true;
                }
                
                update modeloramasUpdate;
                insert newVinculos;
                
                backPageEnterpriseProfile();
            }  
            
            inputTextErrorMessage = null;
            return null;
        }
        PageReference pr = Page.MDRM_ErrorPage; 
        return pr;   
    }        
    
    public void changeEditable(){
        isEditable = isEditable ? false:true;
        isNewMdrm = true;
        isNewValue = true;
        inputTextErrorMessageBussy = '';
        clearMessages();
    }
    
    public void newMdrm(){
        isNewMdrm= isNewMdrm ? false:true;
        isNewValue = true;
        inputTextErrorTotal = '';
        inputTextErrorMessageBussy = '';
        clearMessages();
    }
    
    public void isMineMdrm(){
        miModelorama= miModelorama ? false:true;
    }
    
    public void isMineEmployee(){
        miEmpleado= miEmpleado ? false:true;
    }
    
    public void mostrarModelorama(){
        mostrarMdrm= mostrarMdrm ? false:true;
    }
    
    public PageReference pagetomdrm()
    {
        PageReference pr = Page.Employ_Profile; 
        return pr;
    }
    
    public PageReference employNew()
    {
        verModelorama = new List<Account>([SELECT z019__c FROM Account WHERE z019__c =: parametroModelorama]);
        
        PageReference pr = Page.Employ_Profile_New;
        
        inputTextErrorMessageFecha = ' ';
        inputTextErrorMessageGenero =' ';
        return pr;
    }
    
    public PageReference pagetoModelorama()
    {
        clearMessages();
        inputTextErrorMessageBussy = '';
        isNewValue = false;
        isNewValue = true;
        verModelorama = new List<Account>([SELECT z019__c, Id, Name, ONTAP__Province__c, 
                                           ONTAP__PostalCode__c, ONTAP__Municipality__c, 
                                           ONTAP__Colony__c, ONTAP__Street__c, 
                                           ONTAP__Street_Number__c
                                           FROM Account 
                                           WHERE z019__c =: parametroModelorama]);
        // Llamado al wrap verModelorama
        accountWrap = wrapVerModelorama(verModelorama);
        
        empleados = new List<MDRM_Employee__c>([SELECT Name, MDRM_LastName__c, MDRM_MotherSurname__c, MDRM_Birthdate__c, MDRM_Gender__c, MDRM_VinculoMB__c 
                                                FROM MDRM_Employee__c
                                                WHERE MDRM_VinculoMB__r.MDRM_Expansor__r.z019__c =: parametroModelorama]);
        
        PageReference pr = Page.Mdrm_Profile;
        miModelorama= miModelorama = false;
        isEditable = true;
        return pr;
    }
    
    public PageReference deleteMdrm()
    { 
        clearMessages();
        inputTextErrorMessageBussy = '';
        if(listaModeloramas.size() != 1){
            List<Account> setFalse = new List<Account>([SELECT z019__c,MDRM_Used__c FROM Account WHERE z019__c =:parametroModelorama]);
            
            List <MDRM_Vinculo__c> deleteModelorama = new List <MDRM_Vinculo__c> ();
            deleteModelorama = new List<MDRM_Vinculo__c> ([SELECT Name, MDRM_Businessman__c, MDRM_Id_SAP_Expansor__c, MDRM_Expansor__c FROM MDRM_Vinculo__c WHERE MDRM_Expansor__r.z019__c =: parametroModelorama]);
            delete deleteModelorama;
            
            for(Account dm : setFalse){
                if(dm.z019__c == parametroModelorama){
                    dm.MDRM_Used__c = false;
                }
            }
            update setFalse;
            
            listaModeloramas= new List<MDRM_Vinculo__c>([SELECT MDRM_Expansor__r.z019__c, MDRM_Expansor__r.Name, MDRM_Expansor__r.ONTAP__Province__c, 
                                                         MDRM_Expansor__r.ONTAP__PostalCode__c, MDRM_Expansor__r.ONTAP__Municipality__c, 
                                                         MDRM_Expansor__r.ONTAP__Colony__c, MDRM_Expansor__r.ONTAP__Street__c, 
                                                         MDRM_Expansor__r.ONTAP__Street_Number__c
                                                         FROM MDRM_Vinculo__c 
                                                         WHERE MDRM_Businessman__r.Id =:empresario.Id
                                                         AND MDRM_Expansor__c != null]);
            
            // Llmamada al metodo wraper lista modelorama
            wrapListaModelorama = wrapListaModelorama(listaModeloramas);
            
            return null;
        }
        else{
            inputTextErrorLastModelorama = System.Label.MDRM_BP_LastModelorama;
            isNewValue = false;
            return null;
        }
    }
    
    public void deleteEmpleado()
    {	
        empleados = new List<MDRM_Employee__c>([SELECT Id, Name, MDRM_LastName__c, MDRM_MotherSurname__c, MDRM_Birthdate__c, MDRM_Gender__c, MDRM_VinculoMB__c 
                                                FROM MDRM_Employee__c
                                                WHERE Id =: parametroEmpleado]);
        delete empleados;
        
        empleados = new List<MDRM_Employee__c>([SELECT Name, MDRM_LastName__c, MDRM_MotherSurname__c, MDRM_Birthdate__c, MDRM_Gender__c, MDRM_VinculoMB__c 
                                                FROM MDRM_Employee__c
                                                WHERE MDRM_VinculoMB__r.MDRM_Expansor__r.z019__c =: parametroModelorama]);
    }
    
    public PageReference savetoNewEmployee()
    {      
        validationFields = true;
        inputTextErrorMessageFecha = '';
        inputTextErrorMessageGenero = '';
        if(employee.Name == null || employee.Name ==''){
            employee.Name.addError(System.Label.MDRM_BP_NameRequired);
            validationFields = false;
        }
        if(employee.MDRM_LastName__c == null || employee.MDRM_LastName__c ==''){
            employee.MDRM_LastName__c.addError(System.Label.MDRM_BP_LastNameRequired);
            validationFields = false;
        }
        if(employee.MDRM_MotherSurname__c == null || employee.MDRM_MotherSurname__c ==''){
            employee.MDRM_MotherSurname__c.addError(System.Label.MDRM_BP_MotherSurnameRequired);
            validationFields = false;
        }
        if(employee.MDRM_Birthdate__c == null){
            inputTextErrorMessageFecha = ' '+System.Label.MDRM_BP_BithdayRequired;
            validationFields = false;
        }
        if(validationFields == false){
            return null;
        }
        else{
            IsDisabled=false;
            List<MDRM_Vinculo__c> addEmployee = new List<MDRM_Vinculo__c>([SELECT MDRM_Expansor__r.z019__c FROM MDRM_Vinculo__c WHERE MDRM_Expansor__r.z019__c =: parametroModelorama]);
            
            MDRM_Employee__c empleados = new MDRM_Employee__c(MDRM_VinculoMB__c = addEmployee[0].id, Name = employee.Name,MDRM_LastName__c = employee.MDRM_LastName__c,
                                                              MDRM_MotherSurname__c =employee.MDRM_MotherSurname__c, MDRM_Gender__c = employee.MDRM_Gender__c,
                                                              MDRM_Birthdate__c = employee.MDRM_Birthdate__c);
            insert empleados;
            
            employee.Name = '';
            employee.MDRM_LastName__c = '';
            employee.MDRM_MotherSurname__c = '';
            employee.MDRM_Birthdate__c = null;
            employee.MDRM_Gender__c = '';
            
            pagetoModelorama();
        }
        inputTextErrorMessageFecha = '';
        inputTextErrorMessageGenero = '';
        PageReference pr = Page.Mdrm_Profile;
        IsDisabled=true;
        return pr;
    }
    
    public PageReference pagetoEmploy()
    {
        empleados = new List<MDRM_Employee__c>([SELECT Id, Name, MDRM_LastName__c, MDRM_MotherSurname__c, MDRM_Birthdate__c, MDRM_Gender__c 
                                                FROM MDRM_Employee__c 
                                                WHERE Id =: parametroEmpleado]);
        PageReference pr = Page.Employ_Profile;
        inputTextErrorMessageFecha = '';
        return pr;
    }
    
    public PageReference saveEmployee()
    {
        IsDisabled=true;
        validationFields = true;
        inputTextErrorMessageFecha = '';
        inputTextErrorMessageGenero = '';
        if(empleados[0].Name == null || empleados[0].Name ==''){
            empleados[0].Name.addError(System.Label.MDRM_BP_NameRequired);
            validationFields = false;
        }
        if(empleados[0].MDRM_LastName__c == null || empleados[0].MDRM_LastName__c ==''){
            empleados[0].MDRM_LastName__c.addError(System.Label.MDRM_BP_LastNameRequired);
            validationFields = false;
        }
        if(empleados[0].MDRM_MotherSurname__c == null || empleados[0].MDRM_MotherSurname__c ==''){
            empleados[0].MDRM_MotherSurname__c.addError(System.Label.MDRM_BP_MotherSurnameRequired);
            validationFields = false;
        }
        if(empleados[0].MDRM_Birthdate__c == null){
            inputTextErrorMessageFecha = ' '+System.Label.MDRM_BP_BithdayRequired;
            validationFields = false;
        }
        if(validationFields == false){
            return null;
        }
        else{
            upsert empleados;
        }   
        inputTextErrorMessageFecha = '';
        inputTextErrorMessageGenero = '';
        PageReference pr = Page.Employ_Profile; 
        isEditable = isEditable ? false:true;
        IsDisabled=false;
        
        return pr;
    }
    
    public PageReference editProfile()
    {
        clearMessages();
        inputTextErrorMessageBussy = '';
        validationFields = true;
        empresario = unwrapBusinessman(accWrap);
        if(accWrap.ContactsPhone == null || accWrap.ContactsPhone ==''){
            PhoneError = System.Label.MDRM_BP_PhoneRequired;
            validationFields = false;
        }
        else{
            String telefonoFijo = accWrap.ContactsPhone;
            Integer cLength = telefonoFijo.length();
            if(telefonoFijo.length() != 10){
                PhoneError = System.Label.MDRM_BP_PhoneNumberOfDigits;
                validationFields = false;
            }
        }
        if(empresario.ONTAP__PostalCode__c == null || empresario.ONTAP__PostalCode__c ==''){
            PostalCodeError = System.Label.MDRM_BP_PostalCodeRequired;
            validationFields = false;
        }
        else{
            String postalCode = empresario.ONTAP__PostalCode__c;
            Integer postal = postalCode.length();
            if(postalCode.length() != 5){
                PostalCodeError = System.Label.MDRM_BP_PostalCodeNumberOfDigits;
                validationFields = false;
            }
        }
        System.debug('Error estado: '+ System.Label.MDRM_BP_ProvinceRequired);
        if(empresario.ONTAP__Province__c == null || empresario.ONTAP__Province__c ==''){
            RequieredProvinceError = System.Label.MDRM_BP_ProvinceRequired;
            validationFields = false;
        }
        if(empresario.ONTAP__Municipality__c == null || empresario.ONTAP__Municipality__c ==''){
            RequieredMunicipalityError = System.Label.MDRM_BP_MunicipalityRequired;
            validationFields = false;
        }
        if(empresario.ONTAP__Colony__c == null || empresario.ONTAP__Colony__c ==''){
            RequieredColonyError = System.Label.MDRM_BP_ColonyRequired;
            validationFields = false;
        }
        if(empresario.ONTAP__Street__c == null || empresario.ONTAP__Street__c ==''){
            RequieredStreetError = System.Label.MDRM_BP_StreetRequired;
            validationFields = false;
        }
        if(empresario.ONTAP__Street_Number__c == null || empresario.ONTAP__Street_Number__c ==''){
            RequieredStreetNumberError = System.Label.MDRM_BP_StreetNumberRequired;
            validationFields = false;
        }
        
        if(validationFields == false){
            inputTextErrorTotal = System.Label.MDRM_BP_PageError;
            return null;
        }
        else{
            
            if(contactBusinessman.size() > 0)
            	contactBusinessman[0].Phone = accWrap.ContactsPhone;
           	if(userBusinessman.size() > 0)
            	userBusinessman[0].MDRM_Facebook_Account__c = accWrap.FormsFacebookAccount;
            List<sObject> objectsToUpsert = new List<sObject>();
            objectsToUpsert.addAll(userBusinessman);
            objectsToUpsert.addAll(contactBusinessman);
            objectsToUpsert.add(empresario);
            if(!objectsToUpsert.isEmpty())
                update objectsToUpsert;
            inputTextErrorTotal ='';
        }
        PageReference pr = Page.Enterprise_Profile; 
        isEditable = isEditable ? false:true;
        return pr;
    }
    
    
    public PageReference modeloramaSAP()
    {
        clearMessages();
        inputTextErrorMessageBussy = '';
        isNewValue = false;
        isNewValue = true;
        validationFields = true;
        if(sapid == null || sapid ==''){
            inputTextErrorMessage = System.Label.MDRM_BP_CodeRequired;
            return null;
        }
        
        if(validationFields == false){
            return null;
        }
        else{
            sapid=sapid.trim();
            model = new List<Account>([SELECT z019__c, Name, ONTAP__Province__c, ONTAP__PostalCode__c, ONTAP__Municipality__c, ONTAP__Colony__c, ONTAP__Street__c, ONTAP__Street_Number__c
                                       FROM Account 
                                       WHERE z019__c =:sapid]);
            
            if(!model.isEmpty()) {
                // Llamado al wrap buscar
                busWrap = wrapBuscarModelorama(model);
            } else {   
                inputTextErrorMessage = System.Label.MDRM_BP_WrongCode;
                return null;
            }
        }
        
        inputTextErrorMessage ='';    
        PageReference pr = Page.Mdrm_Profile_New;
        miModelorama = true;
        isEditable = true;
        return pr;
    }
    
    
    public PageReference savetoEnterprise()
    {
        sapid=sapid.trim();
        List<MDRM_Vinculo__c> newVin = new List<MDRM_Vinculo__c>();
        
        List<Account> modeloramaAccount = new List<Account>([SELECT z019__c,MDRM_Used__c, Name, ONTAP__Province__c, 
                                                             ONTAP__PostalCode__c, ONTAP__Municipality__c, 
                                                             ONTAP__Colony__c, ONTAP__Street__c, 
                                                             ONTAP__Street_Number__c
                                                             FROM Account 
                                                             WHERE z019__c =:sapid]);
        for(Account used: modeloramaAccount){
            
            if(used.MDRM_Used__c == false){
                MDRM_Vinculo__c expansor = new MDRM_Vinculo__c(MDRM_Businessman__c = empresario.Id, MDRM_Expansor__c = modeloramaAccount[0].Id );
                newVin.add(expansor);
                
                used.MDRM_Used__c = true;
            }
            else if(used.MDRM_Used__c == true){
                inputTextErrorMessageBussy = System.Label.MDRM_BP_UnavailableModelorama;
                return messageError();
            }
            
        }
        if(!newVin.isEmpty())
            insert newVin;
        if(!modeloramaAccount.isEmpty())
            upsert modeloramaAccount;
        
        miModelorama= miModelorama ? false:true;
        PageReference pr = Page.Mdrm_Profile_New;
        return pr;
    }
    public PageReference messageError(){
        
        PageReference pr = Page.Enterprise_profile; 
        isNewMdrm = false;
        return pr;
    }
    
    public PageReference backPageEnterpriseProfile()
    {
        empresarioConModeloramas= new List<MDRM_Vinculo__c>([SELECT MDRM_Businessman__r.Id FROM MDRM_Vinculo__c WHERE MDRM_Expansor__r.z019__c =: codigoModelorama ]);
        contactBusinessman = new List<Contact>(); 
        userBusinessman = new List<MDRM_Form__c>();
        empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                      ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                      ONTAP__Email__c,
                      (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                      FROM Account 
                      WHERE Id =: empresarioConModeloramas[0].MDRM_Businessman__c];
        
        contactBusinessman = empresario.Contacts;
        userBusinessman = empresario.Forms__r;
        
        listaModeloramas= new List<MDRM_Vinculo__c>([SELECT MDRM_Expansor__r.z019__c, MDRM_Expansor__r.Name, MDRM_Expansor__r.ONTAP__Province__c, 
                                                     MDRM_Expansor__r.ONTAP__PostalCode__c, MDRM_Expansor__r.ONTAP__Municipality__c, 
                                                     MDRM_Expansor__r.ONTAP__Colony__c, MDRM_Expansor__r.ONTAP__Street__c, 
                                                     MDRM_Expansor__r.ONTAP__Street_Number__c
                                                     FROM MDRM_Vinculo__c 
                                                     WHERE MDRM_Businessman__r.Id =: empresario.id
                                                     AND MDRM_Expansor__c != null]);
        
        PageReference pr = Page.Enterprise_profile; 
        isNewMdrm = true;
        inputTextErrorMessage = '';
        inputTextErrorMessageFecha = '';
        inputTextErrorMessageGenero = '';
        return pr;
    }
    
    public PageReference EnterpriseProfile(){
        
        PageReference pr = Page.Enterprise_profile; 
        isNewMdrm = true;
        isEditable = true;
        inputTextErrorMessage = '';
        return pr;
    }
    
    public PageReference pageSurvey()
    {
        isNewValue = true;
        clearMessages();
        inputTextErrorMessageBussy = '';
        survey =new List<MDRM_form__c>([SELECT MDRM_Account_form__r.Id, 
                                        MDRM_How_did_you_hear_about__c, 
                                        MDRM_How_did_you_hear_about_comment__c,
                                        MDRM_Grade_of_schooling__c,
                                        MDRM_Marital_status__c,
                                        MDRM_People_financially_dependent__c,
                                        MDRM_Currently_Employed__c,
                                        MDRM_Current_employment_full_time__c,
                                        MDRM_Current_Employment__c,
                                        MDRM_Current_employment_comment__c,
                                        MDRM_Activities_last_employment__c,
                                        MDRM_Activities_last_employment_comment__c,
                                        MDRM_Computer_domain_level__c,
                                        MDRM_Experience_operating_similar_store__c,
                                        MDRM_Have_capital_necessary__c,
                                        MDRM_Former_or_retired_employee__c,
                                        MDRM_Retired_military__c,
                                        MDRM_Availability_To_Start_Operating__c,
                                        MDRM_Facebook_Account__c,
                                        MDRM_Why_interested_entrepreneur__c
                                        FROM MDRM_form__c 
                                        WHERE MDRM_Account_form__r.Id =: empresario.Id]);
        if(!survey.isEmpty()){
            preguntaUno = survey[0].MDRM_How_did_you_hear_about__c;
            preguntaDos = survey[0].MDRM_Grade_of_schooling__c;
            preguntaTres = survey[0].MDRM_Marital_status__c;
            preguntaCuatro = survey[0].MDRM_People_financially_dependent__c;
            preguntaCinco = survey[0].MDRM_Currently_Employed__c;
            preguntaCincoUno = survey[0].MDRM_Current_employment_full_time__c;
            preguntaCincoDos = survey[0].MDRM_Current_Employment__c;
            labelCincoDos = survey[0].MDRM_Current_employment_comment__c;
            preguntaCincoTres = survey[0].MDRM_Activities_last_employment__c;
            labelCincoTres = survey[0].MDRM_Activities_last_employment_comment__c;
            preguntaSeis = survey[0].MDRM_Computer_domain_level__c;
            preguntaSiete = survey[0].MDRM_Experience_operating_similar_store__c;
            preguntaOcho = survey[0].MDRM_Have_capital_necessary__c;
            preguntaNueve = survey[0].MDRM_Former_or_retired_employee__c;
            preguntaDiez = survey[0].MDRM_Retired_military__c;
            preguntaOnce = survey[0].MDRM_Availability_To_Start_Operating__c;
            preguntaDoce = survey[0].MDRM_Why_interested_entrepreneur__c;
            preguntaTrece = survey[0].MDRM_Facebook_Account__c;
        }
        
        PageReference pr = Page.Businessman_survey; 
        return pr;
    }
    
    public PageReference updateSurvey()
    {
        validationFields = true;
        if(survey[0].MDRM_Why_interested_entrepreneur__c == null || survey[0].MDRM_Why_interested_entrepreneur__c ==''){
            survey[0].MDRM_Why_interested_entrepreneur__c.addError(System.Label.MDRM_BP_RequiredField);
            validationFields = false;
            inputTextErrorMessage = System.Label.MDRM_BP_PageError;
        }
        if(survey[0].MDRM_Facebook_Account__c == null || survey[0].MDRM_Facebook_Account__c ==''){
            survey[0].MDRM_Facebook_Account__c.addError(System.Label.MDRM_BP_RequiredField);
            validationFields = false;
            inputTextErrorMessage = System.Label.MDRM_BP_PageError;
        }
        if(validationFields == false){
            return null;
        }
        else{
            
            for(MDRM_form__c s: survey){
                
                s.MDRM_How_did_you_hear_about__c = preguntaUno;
                s.MDRM_Grade_of_schooling__c = preguntaDos;
                s.MDRM_Marital_status__c = preguntaTres;
                s.MDRM_People_financially_dependent__c = preguntaCuatro;
                s.MDRM_Currently_Employed__c = preguntaCinco;
                s.MDRM_Current_employment_full_time__c = preguntaCincoUno;
                
                s.MDRM_Current_Employment__c = preguntaCincoDos;
                s.MDRM_Current_employment_comment__c = labelCincoDos;
                
                s.MDRM_Activities_last_employment__c = preguntaCincoTres;
                s.MDRM_Activities_last_employment_comment__c = labelCincoTres;
                
                s.MDRM_Computer_domain_level__c = preguntaSeis;
                s.MDRM_Experience_operating_similar_store__c = preguntaSiete;
                s.MDRM_Have_capital_necessary__c = preguntaOcho;
                s.MDRM_Former_or_retired_employee__c = preguntaNueve;
                s.MDRM_Retired_military__c = preguntaDiez;
                s.MDRM_Availability_To_Start_Operating__c = preguntaOnce;
                s.MDRM_Why_interested_entrepreneur__c = survey[0].MDRM_Why_interested_entrepreneur__c;
                s.MDRM_Facebook_Account__c = survey[0].MDRM_Facebook_Account__c;
            }
            upsert survey;
        }
        isNewMdrm = true;
        isEditable = true;
        inputTextErrorMessage = '';
        PageReference pr = Page.Enterprise_profile; 
        return pr;
    }
    
    public void clearMessages(){
        inputTextErrorTotal = '';
        inputTextErrorMessage = '';
        inputTextErrorLastModelorama='';
        PhoneError='';
        PostalCodeError='';
        RequieredProvinceError='';
        RequieredMunicipalityError='';
        RequieredColonyError='';
        RequieredStreetError='';
        RequieredStreetNumberError='';
    }
    
    //Wrapp Businessman
    public static MDRM_AccountWrapped wrapBusinessman(Account accbus) {
        MDRM_AccountWrapped accWrap = new MDRM_AccountWrapped();
        
        accWrap.Id = accbus.Id;
        accWrap.Name = accbus.Name;
        accWrap.MDRM_LastName = accbus.MDRM_LastName__c;
        accWrap.ONTAP_PostalCode = accbus.ONTAP__PostalCode__c;
        accWrap.ONTAP_Colony = accbus.ONTAP__Colony__c;
        accWrap.ONTAP_Province = accbus.ONTAP__Province__c;
        accWrap.ONTAP_Municipality = accbus.ONTAP__Municipality__c;
        accWrap.ONTAP_Street = accbus.ONTAP__Street__c;
        accWrap.ONTAP_Street_Number = accbus.ONTAP__Street_Number__c;
        accWrap.ONTAP_Email = accbus.ONTAP__Email__c;
        
        if(!accbus.Contacts.isEmpty()){
            accWrap.ContactsId = accbus.Contacts[0].Id;
            accWrap.ContactsPhone = accbus.Contacts[0].Phone;
            accWrap.ContactsBirthdate = accbus.Contacts[0].Birthdate;
            accWrap.ContactsGender = accbus.Contacts[0].MDRM_Gender__c;
        }
        
        if(!accbus.Forms__r.isEmpty()){
        	accWrap.FormsId = accbus.Forms__r[0].Id;
        	accWrap.FormsFacebookAccount = accbus.Forms__r[0].MDRM_Facebook_Account__c;
        }
        
        return accWrap;
    }
    
    // unwrap Businessman
    public static Account unwrapBusinessman(MDRM_AccountWrapped mdrmacc) {
        
        Account acc = new Account();
        Contact cnt = new Contact();
        acc.Id = mdrmacc.Id;
        acc.Name = mdrmacc.Name;
        acc.MDRM_LastName__c = mdrmacc.MDRM_LastName;
        acc.ONTAP__PostalCode__c = mdrmacc.ONTAP_PostalCode;
        acc.ONTAP__Colony__c = mdrmacc.ONTAP_Colony;
        acc.ONTAP__Province__c = mdrmacc.ONTAP_Province;
        acc.ONTAP__Municipality__c = mdrmacc.ONTAP_Municipality;
        acc.ONTAP__Street__c = mdrmacc.ONTAP_Street;
        acc.ONTAP__Street_Number__c = mdrmacc.ONTAP_Street_Number;
        acc.ONTAP__Email__c = mdrmacc.ONTAP_Email;
        
        return acc;
    }
    
    //wrap lista modelorama
    public static List<MDRM_VinculoWrapped> wrapListaModelorama(List<MDRM_Vinculo__c> vinlist) {
        
        List<MDRM_VinculoWrapped> listvinc = new List<MDRM_VinculoWrapped>();
        
        for(MDRM_Vinculo__c lv : vinlist) {
            
            MDRM_VinculoWrapped vinculoWrap = new MDRM_VinculoWrapped();
            
            vinculoWrap.z019 = lv.MDRM_Expansor__r.z019__c;
            vinculoWrap.Name = lv.MDRM_Expansor__r.Name;
            vinculoWrap.ONTAP_Province = lv.MDRM_Expansor__r.ONTAP__Province__c;
            vinculoWrap.ONTAP_PostalCode = lv.MDRM_Expansor__r.ONTAP__PostalCode__c;
            vinculoWrap.ONTAP_Municipality = lv.MDRM_Expansor__r.ONTAP__Municipality__c;
            vinculoWrap.ONTAP_Colony = lv.MDRM_Expansor__r.ONTAP__Colony__c;
            vinculoWrap.ONTAP_Street = lv.MDRM_Expansor__r.ONTAP__Street__c;
            vinculoWrap.ONTAP_Street_Number = lv.MDRM_Expansor__r.ONTAP__Street_Number__c;
            
            
            listvinc.add(vinculoWrap);
        }
        
        return listvinc;
    }
    
    //wrap ver modelorama
    public static MDRM_AccountWrapped wrapVerModelorama(List<Account> accvm) {
        
        MDRM_AccountWrapped accountWrap = new MDRM_AccountWrapped();
        
        accountWrap.z019 = accvm[0].z019__c;
        accountWrap.Id = accvm[0].Id;
        accountWrap.Name = accvm[0].Name;
        accountWrap.ONTAP_Province = accvm[0].ONTAP__Province__c;
        accountWrap.ONTAP_PostalCode = accvm[0].ONTAP__PostalCode__c;
        accountWrap.ONTAP_Municipality = accvm[0].ONTAP__Municipality__c;
        accountWrap.ONTAP_Colony = accvm[0].ONTAP__Colony__c;
        accountWrap.ONTAP_Street = accvm[0].ONTAP__Street__c;
        accountWrap.ONTAP_Street_Number = accvm[0].ONTAP__Street_Number__c;
        
        return accountWrap;
    }
    
    //wrap buscar modelorama
    public static MDRM_AccountWrapped wrapBuscarModelorama(List<Account> busmod){
        
        MDRM_AccountWrapped busWrap = new MDRM_AccountWrapped();
        
        busWrap.z019 = busmod[0].z019__c;
        busWrap.Name = busmod[0].Name;
        busWrap.ONTAP_Province = busmod[0].ONTAP__Province__c;
        busWrap.ONTAP_PostalCode = busmod[0].ONTAP__PostalCode__c;
        busWrap.ONTAP_Municipality = busmod[0].ONTAP__Municipality__c;
        busWrap.ONTAP_Colony = busmod[0].ONTAP__Colony__c;
        busWrap.ONTAP_Street = busmod[0].ONTAP__Street__c;
        busWrap.ONTAP_Street_Number = busmod[0].ONTAP__Street_Number__c;
        
        return busWrap;
    }
}