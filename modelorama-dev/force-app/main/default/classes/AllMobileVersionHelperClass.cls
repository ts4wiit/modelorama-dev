/**
 * This class serves as helper class for AllMobileVersionTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileVersionHelperClass {

	/**
	 * This method initiates the insertion of version.
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 */
	public static void initiateInsertVersion(List<AllMobileVersion__c> lstAllMobileVersion) {
		assignApplicationIdAndGenerateVersionId(lstAllMobileVersion);
	}

	/**
	 * This method assign Application Id to Version.
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 */
	public static void assignApplicationIdAndGenerateVersionId(List<AllMobileVersion__c> lstAllMobileVersion) {
		Set<Id> setIdAllMobileApplication = new Set<Id>();
		for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersion) {
			setIdAllMobileApplication.add(objAllMobileVersion.AllMobileApplicationMD__c);
		}
		List<AllMobileApplication__c> lstAllMobileApplication = [SELECT Id, Name, AllMobileApplicationId__c FROM AllMobileApplication__c WHERE Id =: setIdAllMobileApplication];
		for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersion) {
			for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplication) {
				if(objAllMobileVersion.AllMobileApplicationMD__c == objAllMobileApplication.Id) {
					objAllMobileVersion.AllMobileApplicationId__c = objAllMobileApplication.AllMobileApplicationId__c;
				}
			}
		}
	}

	/**
	 * This method enqueue a Job to synchronize AllMobileVersions to Heroku.
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 * @param strEventTriggerFlag	String
	 */
	public static void syncVersionWithHeroku(List<AllMobileVersion__c> lstAllMobileVersion, String strEventTriggerFlag) {
		AllMobileVersionOperationClass objAllMobileOperationClass = new AllMobileVersionOperationClass(lstAllMobileVersion, strEventTriggerFlag);
		Id IdEnqueueJob = System.enqueueJob(objAllMobileOperationClass);
	}
}