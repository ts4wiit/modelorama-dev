/**
 * This class serves as batch class for AllMobileRouteAppVersionOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileRouteAppVersionBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	/**
	 * This method returns a list with RouteAppVersion records to be deleted.
	 *
	 * @param objBatchableContext	Database.BatchableContext
	 */
	global Database.QueryLocator start(Database.BatchableContext objBatchableContext) {
		return Database.getQueryLocator(AllMobileStaticVariablesClass.STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_ROUTE_APP_VERSIONS);
	}

	/**
	 * This method execute a batch to delete the RouteAppVersion records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 * @param List<AllMobileRouteAppVersion__c>
	 */
	global void execute(Database.BatchableContext objBatchableContext, List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionSF) {
		AllMobileRouteAppVersionOperationClass.syncDeleteAllMobileRouteAppVersionInSF(lstAllMobileRouteAppVersionSF);
	}

	/**
	 * This method finish the execution of batch which deletes the RouteAppVersion records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 */
	global void finish(Database.BatchableContext objBatchableContext) {
	}
}