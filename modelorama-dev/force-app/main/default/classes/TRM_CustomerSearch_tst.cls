@isTest
private class TRM_CustomerSearch_tst {
	
	@isTest static void testGetDataTableColumnsByFieldset() {
		TRM_CustomerSearch_ctr.getDataTableColumnsByFieldset('TRM_ConditionClass__c','TRM_Default');
	}

	@isTest static void testGetAccounts() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		List<String> lstfilterId = new List<String>();
		String strValue = '\'5tdS4l3sf0rc31d\'';
		lstfilterId.add(strValue);
		TRM_CustomerSearch_ctr.getAccounts(true, 'FK00', 'ONTAP__ExternalKey__c, Name', 'Account', 
			'Account', 'ONTAP__ExternalKey__c', '3000', lstfilterId, true, 'Segmento-40, Segmento-41, Segmento-42',
			true, '01, 06', true, '5tdS4l3sf0rc31d', true, '3116', true, '[1, [2');
        
        TRM_CustomerSearch_ctr.getAccounts(false, 'FK00', 'ONTAP__ExternalKey__c, Name', 'Account', 
			'Account', 'ONTAP__ExternalKey__c', '3000', lstfilterId, true, 'Segmento-40, Segmento-41, Segmento-42',
			true, '01, 06', true, '5tdS4l3sf0rc31d', true, '3116', true, '[1, [2');
	}
    
    @isTest static void testGetAccountsByIdList() {
        String RecordTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecordTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
            
        //Creamos Cuenta con SalesOrg
        Account  objAccountSalesOrg = new Account();
            objAccountSalesOrg.Name = 'Name Sales Org';
            objAccountSalesOrg.RecordTypeId = RecordTypeAccountSalesOrg;
            objAccountSalesOrg.ONTAP__SalesOgId__c = 'OR01';
            insert objAccountSalesOrg;
        
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
        	objAccount.ONTAP__SAP_Number__c = '0123456789';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ONTAP__SalesOffId__c = 'SO01';
            objAccount.ONTAP__SalesOgId__c ='OR01';
            insert objAccount;
        List<String> customerIdList = new List<String>();
        customerIdList.add('\'' + objAccount.Id + '\'');
		TRM_CustomerSearch_ctr.getAccountsByIdList('ONTAP__ExternalKey__c, Name', 'Account', customerIdList, '3000');
	}
	
	@isTest static void testGetRecordById() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		TRM_CustomerSearch_ctr.getRecordById('TRM_ConditionClass__c', trm.id);
	}
	
}