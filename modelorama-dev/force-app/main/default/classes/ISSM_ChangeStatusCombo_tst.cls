/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Test class for schedule class ISSM_ChangeStatusCombo_sch
*
* No.       Fecha              Autor                      Descripción
* 1.0    24-Mayo-2018      Oscar Alvarez                   Creación
*******************************************************************************/
@isTest
private class ISSM_ChangeStatusCombo_tst{
    
    static testmethod void testsScheduleIt() {
        Test.startTest();
        	ISSM_ChangeStatusCombo_sch.scheduleIt(); 
        Test.stopTest();
    }
    
    static testmethod void testEjecuteFlow() {
        Test.startTest();
            String jobId = System.schedule(Label.TRM_NameSchStatusCmb + System.now(),ISSM_ChangeStatusCombo_sch.CRON_EXPR,new ISSM_ChangeStatusCombo_sch());
            System.debug('jobId--> '+jobId);
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(ISSM_ChangeStatusCombo_sch.CRON_EXPR, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}