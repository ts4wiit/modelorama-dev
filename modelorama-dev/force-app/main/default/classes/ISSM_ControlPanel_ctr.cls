/****************************************************************************************************
    General Information
    -------------------
    author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Class to show the report for the Telesales on OnCall

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       1-November-2017       Marco Zúñiga                 Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_ControlPanel_ctr {
    public String CurrentAccId{get;set;}
    public List<ONTAP__KPI__c> LstKPIYear{get;set;}
    public List<ONTAP__Order_Item__c> LstMonthOI{get;set;}
    public List<ONTAP__Order_Item__c> LstMonthWaterOI{get;set;}
    public List<ONTAP__Order_Item__c> LstMonthEnergOI{get;set;}
    public List<ONTAP__Order_Item__c> LstMonthSodasOI{get;set;}
    public List<ISSM_TradeIniciative__c> LstITrade{get;set;}
    public List<ISSM_Goal__c> LstGoals{get;set;}
    public List<ONTAP__Account_Asset__c> LstAccAssets{get;set;}
   
    
    public List<String> LstCoronaMega{get;set;}
    public List<String> LstVictoriaMega{get;set;}
    public List<String> LstLeonMega{get;set;}
    public List<String> LstVictoriaLaton{get;set;}
    public List<String> LstCoronaLaton{get;set;}
    public List<String> LstModeloLaton{get;set;}
    public List<String> LstFamiliar{get;set;}
    public List<String> LstVictoriaMediana{get;set;}
    public List<String> LstCoronaMediana{get;set;}
    public List<String> LstCFM{get;set;}
    public List<String> LstStella{get;set;}
    public List<String> LstBudLight{get;set;}
    public List<String> LstCoronaLight{get;set;}
    public List<String> LstMichelob{get;set;}
    public List<String> LstBudweiser{get;set;}
    public List<String> LstMedia{get;set;}
    public List<String> LstCraft{get;set;}

    public Integer CoronaMegaCov{get;set;}
    public Integer VictoriaMega{get;set;}
    public Integer LeonMega{get;set;}
    public Integer VictoriaLaton{get;set;}
    public Integer CoronaLaton{get;set;}
    public Integer ModeloLaton{get;set;}
    public Integer Familiar{get;set;}
    public Integer VictoriaMediana{get;set;}
    public Integer CoronaMediana{get;set;}
    public Integer CobFamMod{get;set;}
    public Integer Int1Dot5Lts{get;set;}
    public Integer Stella{get;set;}
    public Integer BudLight{get;set;}
    public Integer CoronaLight{get;set;}
    public Integer Michelob{get;set;}
    public Integer Budweiser{get;set;}
    public Integer Media{get;set;}
    public Integer Craft{get;set;}

    public Double BeerGoalHL{get;set;}
    public Integer BeerGoalBoxes{get;set;}
    public Double EDGoalHL{get;set;}
    public Integer EDGoalBoxes{get;set;}
    public Double SodaGoalHL{get;set;}
    public Integer SodaGoalBoxes{get;set;}
    public Double WaterGoalHL{get;set;}
    public Integer WaterGoalBoxes{get;set;}
    public Integer SumRefrigerators{get;set;}
    public Decimal Effectiveness{get;set;}
    public Double SumHLOI{get;set;}
    public Integer SumBoxesKPIY{get;set;}
    public Double PrcLastYearKPI{get;set;}
    public Double SumPrcLastYearKPI{get;set;}
    public Double NumBoxesLYKPI{get;set;}
    public Integer SumBoxesQtyOIM{get;set;} 
    public Integer SumBoxesQtyOIY{get;set;}
    public Integer SumBoxesQtyOILastY{get;set;}
    public Integer SumWater{get;set;}
    public Integer SumEnerg{get;set;}
    
    public Integer SumSodas{get;set;}
    public Integer SalesByB2B{get;set;}
    public Integer SalesByTel{get;set;}
    public Integer SalesByPresales{get;set;}
    public Account ObjAccount{get;set;}
    public String StrTrade{get;set;}
    public String StrTradeValue{get;set;}
    public Integer IntBeerGap1{get;set;}
    public Integer IntBeerGap2{get;set;}
    public Integer IntBeerGap3{get;set;}
    public Integer IntResidueGap1{get;set;}
    public Integer IntResidueGap2{get;set;}
    public Integer IntResidueGap3{get;set;}
    public Integer IntNumRecords{get;set;}
    public Double AverBeerBoxes{get;set;}
  	public String showDetail{get;set;}
  	public String showAlertNoData{get;set;}
	public Boolean blockThisMonth{get;set;}
   	public Boolean blockLastMonth{get;set;}
   	public String strMonthSelected{get;set;}
   	public String strYearSelected{get;set;}
    
    public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();

    public ISSM_ControlPanel_ctr(ApexPages.StandardController sc) {
        CurrentAccId = sc.getId();
        SumHLOI              = 0.0;
        SumBoxesKPIY         = 0;
        PrcLastYearKPI       = 0.0;
        SumPrcLastYearKPI    = 0.0;
        NumBoxesLYKPI        = 0.0;
        SumBoxesQtyOIM       = 0;
        SumBoxesQtyOIY       = 0;
        SumBoxesQtyOILastY   = 0;
        SumWater             = 0;
        SumEnerg             = 0;

        SumSodas             = 0;
        SalesByB2B           = 0;
        SalesByTel           = 0;
        SalesByPresales      = 0;
        IntResidueGap1       = 0;
        IntResidueGap2       = 0;
        IntResidueGap3       = 0;
        IntBeerGap1          = 0;
        IntBeerGap2          = 0;
        IntBeerGap3          = 0;
        BeerGoalHL           = 0.0;
        BeerGoalBoxes        = 0;
        EDGoalHL             = 0.0;
        EDGoalBoxes          = 0;
        SodaGoalHL           = 0.0;
        SodaGoalBoxes        = 0;
        WaterGoalHL          = 0.0;
        WaterGoalBoxes       = 0;
        SumRefrigerators     = 0;
        Effectiveness        = 0;

        CoronaMegaCov        = 0;
        VictoriaMega         = 0;
        LeonMega             = 0;
        VictoriaLaton        = 0; 
        CoronaLaton          = 0;
        ModeloLaton          = 0;

        Familiar             = 0;
        VictoriaMediana      = 0;
        CoronaMediana        = 0;
        CobFamMod            = 0;
        Int1Dot5Lts          = 0;
        Stella               = 0;
        BudLight             = 0;
        CoronaLight          = 0;
        Michelob             = 0;
        Budweiser            = 0;
        Media                = 0;
        Craft                = 0;  
   		showDetail      ='inline';
   		showAlertNoData = 'none';
   		blockThisMonth = true;
   		blockLastMonth = false;
   		strMonthSelected = ''; 
   		strYearSelected = '';      
   		LstGoals =  new List<ISSM_Goal__c>(); 
    }

    public void getValidateLastMonth(){  
		blockThisMonth = false;
   		blockLastMonth = true;
    	Date ShowMonth;
 		Date ShowMonth2;
		Date ShowMonthcomplete;
 	 
 		ShowMonthcomplete= Date.valueOf(System.now()).addMonths(-1);
    	ShowMonth= ShowMonthcomplete.toStartOfMonth();
    	ShowMonth2 = Date.newinstance(System.now().year(), System.now().month()-1, Date.daysInMonth(System.now().year(), System.now().month()-1));
    	System.debug('ValidateLastMonth  INICO LAST  ' + ShowMonth); 
    	System.debug('ValidateLastMonth  FIN  LAST ' + ShowMonth2); 
    	strMonthSelected = getMonth(ShowMonthcomplete.month());
    	strYearSelected = String.valueOf(ShowMonthcomplete.year());
    	getValidateMonth(ShowMonth,ShowMonth2);    	    	 
    }
	
	public void getValidateThisMonth(){  
    	blockThisMonth = true;
   		blockLastMonth = false;
    	Date ShowMonth;
 		Date ShowMonth2;
 		
    	ShowMonth = Date.valueOf(System.now()).toStartOfMonth();
    	ShowMonth2 = Date.newinstance(System.now().year(), System.now().month(), Date.daysInMonth(System.now().year(), System.now().month()));
    	System.debug('ValidateTHISMonth  INICO  THIS ' + ShowMonth); 
    	System.debug('ValidateTHISMonth  FIN THIS ' + ShowMonth2); 
    	strMonthSelected = getMonth(ShowMonth.month());
    	strYearSelected = String.valueOf(ShowMonth.year());
    	getValidateMonth(ShowMonth,ShowMonth2);
    	
    }
    public String getMonth(Integer numMonth){

    	List<String> month = new List<String>(13);			
    	month.add(0,'Ninguno');
		month.add(1,Label.ISSM_360ViewJanuary);
		month.add(2,Label.ISSM_360ViewFebruary);
		month.add(3,Label.ISSM_360ViewMarch);
		month.add(4,Label.ISSM_360ViewApril);
		month.add(5,Label.ISSM_360ViewMay);
		month.add(6,Label.ISSM_360ViewJune);
		month.add(7,Label.ISSM_360ViewJuly);
		month.add(8,Label.ISSM_360ViewAugust);
		month.add(9,Label.ISSM_360ViewSeptember);
		month.add(10,Label.ISSM_360ViewOctober); 
		month.add(11,Label.ISSM_360ViewNovember);
		month.add(12,Label.ISSM_360ViewDecember);
		return month[numMonth];		
    }
        
    
    public void getValidateMonth(Date ShowMonth, Date ShowMonth2){   
       
        //DateTime d = ShowMonth;
        //System.debug('** D : '+d);
        List<String> ValueMonth =  String.valueOf(ShowMonth).split('-'); 
        String StrMonthName=getMonth(Integer.valueOf(ValueMonth[1])); 
        String StrYear = String.valueOf(ShowMonth.year());
		
        LstCoronaMega      = Label.ISSM_CoronaMega      != null ? Label.ISSM_CoronaMega.split(';')      : new List<String>();
        LstVictoriaMega    = Label.ISSM_VictoriaMega    != null ? Label.ISSM_VictoriaMega.split(';')    : new List<String>();
        LstLeonMega        = Label.ISSM_LeonMega        != null ? Label.ISSM_LeonMega.split(';')        : new List<String>(); 
        LstVictoriaLaton   = Label.ISSM_VictoriaLaton   != null ? Label.ISSM_VictoriaLaton.split(';')   : new List<String>();
        LstCoronaLaton     = Label.ISSM_CoronaLaton     != null ? Label.ISSM_CoronaLaton.split(';')     : new List<String>();
        LstModeloLaton     = Label.ISSM_ModeloLaton     != null ? Label.ISSM_ModeloLaton.split(';')     : new List<String>();
        LstFamiliar        = Label.ISSM_Familiar        != null ? Label.ISSM_Familiar.split(';')        : new List<String>();
        LstVictoriaMediana = Label.ISSM_VictoriaMediana != null ? Label.ISSM_VictoriaMediana.split(';') : new List<String>();
        LstCoronaMediana   = Label.ISSM_CoronaMediana   != null ? Label.ISSM_CoronaMediana.split(';')   : new List<String>();
        LstCFM             = Label.ISSM_CobFamMod       != null ? Label.ISSM_CobFamMod.split(';')       : new List<String>();
        LstStella          = Label.ISSM_Stella          != null ? Label.ISSM_Stella.split(';')          : new List<String>();
        LstBudLight        = Label.ISSM_BudLight        != null ? Label.ISSM_BudLight.split(';')        : new List<String>();
        LstCoronaLight     = Label.ISSM_CoronaLight     != null ? Label.ISSM_CoronaLight.split(';')     : new List<String>();
        LstMichelob        = Label.ISSM_Michelob        != null ? Label.ISSM_Michelob.split(';')        : new List<String>();
        LstBudweiser       = Label.ISSM_Budweiser       != null ? Label.ISSM_Budweiser.split(';')       : new List<String>();
        LstMedia           = Label.ISSM_Media           != null ? Label.ISSM_Media.split(';')           : new List<String>();
        LstCraft           = Label.ISSM_Craft           != null ? Label.ISSM_Craft.split(';')           : new List<String>();
 		 		    
        Date DfirstDateM  = ShowMonth;
        Date DsecondDateM = ShowMonth2;
        Date DfirstDateY  = Date.newInstance(DfirstDateM.year(),  1,  1);
        Date DsecondDateY = Date.newinstance(DfirstDateM.year(), 12, 31);
	
        LstMonthOI      = CTRSOQL.getOrdersItems(CurrentAccId, DfirstDateM, DsecondDateM, Label.ISSM_Beer);     
        LstMonthWaterOI = CTRSOQL.getOrdersItems(CurrentAccId, DfirstDateM, DsecondDateM, Label.ISSM_Water);
        LstMonthEnergOI = CTRSOQL.getOrdersItems(CurrentAccId, DfirstDateM, DsecondDateM, Label.ISSM_EnergyDrink);
        LstMonthSodasOI = CTRSOQL.getOrdersItems(CurrentAccId, DfirstDateM, DsecondDateM, Label.ISSM_Sodas);
        IntNumRecords   = CTRSOQL.getNumOIRecordsPerAccount(CurrentAccId);
        
        System.debug(' LstMonthOI: '+LstMonthOI);
        System.debug('LstMonthWaterOI : '+LstMonthWaterOI);
       	System.debug(' LstMonthEnergOI: '+LstMonthEnergOI );
        System.debug(' LstMonthSodasOI: '+ LstMonthSodasOI);
       	System.debug(' IntNumRecords: '+IntNumRecords );
       
       if(LstMonthOI != null && !LstMonthOI.isEmpty() || LstMonthWaterOI != null && !LstMonthWaterOI.isEmpty() || LstMonthEnergOI != null && !LstMonthEnergOI.isEmpty()|| LstMonthSodasOI != null && !LstMonthSodasOI.isEmpty() ){
       	SumHLOI              = 0.0;
        SumBoxesKPIY         = 0;
        PrcLastYearKPI       = 0.0;
        SumPrcLastYearKPI    = 0.0;
        NumBoxesLYKPI        = 0.0;
        SumBoxesQtyOIM       = 0;
        SumBoxesQtyOIY       = 0;
        SumBoxesQtyOILastY   = 0;
        SumWater             = 0;
        SumEnerg             = 0;

        SumSodas             = 0;
        SalesByB2B           = 0;
        SalesByTel           = 0;
        SalesByPresales      = 0;
        IntResidueGap1       = 0;
        IntResidueGap2       = 0;
        IntResidueGap3       = 0;
        IntBeerGap1          = 0;
        IntBeerGap2          = 0;
        IntBeerGap3          = 0;
        BeerGoalHL           = 0.0;
        BeerGoalBoxes        = 0;
        EDGoalHL             = 0.0;
        EDGoalBoxes          = 0;
        SodaGoalHL           = 0.0;
        SodaGoalBoxes        = 0;
        WaterGoalHL          = 0.0;
        WaterGoalBoxes       = 0;
        SumRefrigerators     = 0;
        Effectiveness        = 0;

        CoronaMegaCov        = 0;
        VictoriaMega         = 0;
        LeonMega             = 0;
        VictoriaLaton        = 0; 
        CoronaLaton          = 0;
        ModeloLaton          = 0;

        Familiar             = 0;
        VictoriaMediana      = 0;
        CoronaMediana        = 0;
        CobFamMod            = 0;
        Int1Dot5Lts          = 0;
        Stella               = 0;
        BudLight             = 0;
        CoronaLight          = 0;
        Michelob             = 0;
        Budweiser            = 0;
        Media                = 0;
        Craft                = 0;  
       	showDetail      ='inline';
   		showAlertNoData = 'none';

        Boolean IsTelesales = false;
        
        for(ONTAP__Order_Item__c ObjOI : LstMonthOI){
            SumHLOI         += ObjOI.ISSM_Hectoliters__c != null ? ObjOI.ISSM_Hectoliters__c : 0;
            SumBoxesQtyOIM  += ObjOI.ONCALL__OnCall_Quantity__c != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0;
            SalesByB2B      += ObjOI.ONTAP__ActualQuantity__c != null && ObjOI.ONTAP__CustomerOrder__r.ISSM_OrderOrigin__c == Label.ISSM_MiModelo ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0;
            SalesByTel      += ObjOI.ONCALL__OnCall_Quantity__c != null && ObjOI.ONTAP__CustomerOrder__r.ISSM_OrderOrigin__c == Label.ISSM_Televenta ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0;
            SalesByPresales += ObjOI.ONTAP__ActualQuantity__c != null && ObjOI.ONTAP__CustomerOrder__r.ISSM_OrderOrigin__c == Label.ISSM_Preventa ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0;            

            IsTelesales = ObjOI.ONTAP__CustomerOrder__r.ISSM_OrderOrigin__c == Label.ISSM_Televenta ? true : false;

            for(String s : LstCoronaMega){
                CoronaMegaCov   += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstVictoriaMega){
                VictoriaMega    += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstLeonMega){
                LeonMega        += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstVictoriaLaton){
                VictoriaLaton   += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstCoronaLaton){
                CoronaLaton     += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstModeloLaton){
                ModeloLaton     += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }            
            for(String s : LstFamiliar){
                Familiar        += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstVictoriaMediana){
                VictoriaMediana += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstCoronaMediana){
                CoronaMediana   += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstCFM){
                CobFamMod       += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstStella){
                Stella          += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstBudLight){
                BudLight        += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstCoronaLight){
                CoronaLight     += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstMichelob){
                Michelob        += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstBudweiser){
                Budweiser       += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstMedia){
                Media           += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
            for(String s : LstCraft){
                Craft           += IsTelesales == true ? (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Number__c).equals(s) && Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);
            }
        }               

        Integer IntNumOrders = CTRSOQL.getOrdersQty(CurrentAccId, DfirstDateM, DsecondDateM);
        AverBeerBoxes = IntNumOrders != 0 ? Decimal.valueOf(Double.valueOf(SalesByB2B + SalesByTel + SalesByPresales) / Double.valueOf(IntNumOrders)).round(System.RoundingMode.CEILING) : 0;

        for(ONTAP__Order_Item__c ObjOI : LstMonthWaterOI){
            SumWater += String.valueOf(ObjOI.ISSM_Sector__c) == Label.ISSM_Water ? (ObjOI.ONCALL__OnCall_Quantity__c != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (ObjOI.ONTAP__ActualQuantity__c != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);  
            Int1Dot5Lts += String.valueOf(ObjOI.ONCALL__OnCall_Product__r.ONCALL__Material_Name__c).containsIgnoreCase(Label.ISSM_1dot5) && ObjOI.ISSM_Sector__c == Label.ISSM_Water ? (ObjOI.ONCALL__OnCall_Quantity__c != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (ObjOI.ONTAP__ActualQuantity__c != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);            
        }

        for(ONTAP__Order_Item__c ObjOI : LstMonthEnergOI){
            SumEnerg += String.valueOf(ObjOI.ISSM_Sector__c) == Label.ISSM_EnergyDrink ? (ObjOI.ONCALL__OnCall_Quantity__c != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (ObjOI.ONTAP__ActualQuantity__c != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);  
        }

        for(ONTAP__Order_Item__c ObjOI : LstMonthSodasOI){
            SumSodas += String.valueOf(ObjOI.ISSM_Sector__c) == Label.ISSM_Sodas ? (ObjOI.ONCALL__OnCall_Quantity__c != null ? Integer.valueOf(ObjOI.ONCALL__OnCall_Quantity__c) : 0) : (ObjOI.ONTAP__ActualQuantity__c != null ? Integer.valueOf(ObjOI.ONTAP__ActualQuantity__c) : 0);  
        }

        LstKPIYear = CTRSOQL.getKPIs(CurrentAccId, 1, DfirstDateM, DsecondDateM);   
        
        if(!LstKPIYear.isEmpty() && LstKPIYear.size() > 0){
            for(ONTAP__KPI__c ObjKPI : LstKPIYear){
                SumBoxesKPIY      += ObjKPI.ONTAP__Actual__c != null ? Integer.valueOf(ObjKPI.ONTAP__Actual__c) : 0;
                
                NumBoxesLYKPI     += (ObjKPI.ONTAP__Percentage_increase_lastyear__c != null) ? (Double.valueOf(ObjKPI.ONTAP__Actual__c) - ((Double.valueOf(ObjKPI.ONTAP__Actual__c) * Double.valueOf(ObjKPI.ONTAP__Percentage_increase_lastyear__c)) / 100.0)) : Double.valueOf(ObjKPI.ONTAP__Actual__c);
            }   
            SumBoxesKPIY = Integer.valueOf(Decimal.valueOf(NumBoxesLYKPI).round(System.RoundingMode.CEILING));
            PrcLastYearKPI = SumBoxesKPIY != 0 ? Decimal.valueOf(((Double.valueOf(SalesByTel) - (Double.valueOf(SumBoxesKPIY))) / Double.valueOf(SumBoxesKPIY)) * 100).round(System.RoundingMode.CEILING): 0.0;            
        }

        ObjAccount    = CTRSOQL.getAccountbyId(CurrentAccId);
        StrTrade      = ObjAccount.ISSM_TradeIniciative__c != null ? ObjAccount.ISSM_TradeIniciative__c : '';
        StrTradeValue = ObjAccount.ISSM_TradeIniciativeValue__c != null ? ObjAccount.ISSM_TradeIniciativeValue__c : '';

        LstITrade =  (ObjAccount.ISSM_TradeIniciativeValue__c != null || ObjAccount.ISSM_TradeIniciativeValue__c != '') ? CTRSOQL.getTradeInitiative(ObjAccount.ISSM_TradeIniciativeValue__c) : new List<ISSM_TradeIniciative__c>();
        
        if(!LstITrade.isEmpty() && LstITrade.size() > 0){
            IntBeerGap1 = LstITrade[0].ISSM_BeerGap1__c != null ? Integer.valueOf(LstITrade[0].ISSM_BeerGap1__c) : 0;
            IntBeerGap2 = LstITrade[0].ISSM_BeerGap2__c != null ? Integer.valueOf(LstITrade[0].ISSM_BeerGap2__c) : 0;
            IntBeerGap3 = LstITrade[0].ISSM_BeerGap3__c != null ? Integer.valueOf(LstITrade[0].ISSM_BeerGap3__c) : 0;

            IntResidueGap1 = (SalesByB2B + SalesByTel + SalesByPresales) - IntBeerGap1;
            IntResidueGap2 = (SalesByB2B + SalesByTel + SalesByPresales) - IntBeerGap2;
            IntResidueGap3 = (SalesByB2B + SalesByTel + SalesByPresales) - IntBeerGap3;
        }        

        LstAccAssets = CTRSOQL.getAccAssPerCustomer(CurrentAccId);

		if(!LstAccAssets.isEmpty() && LstAccAssets.size() > 0){
            SumRefrigerators = LstAccAssets.size();
            Effectiveness = (SumRefrigerators * 10) != null ? (SumRefrigerators * 10) : 0;
		}  
	}else{
       	SumHLOI              = 0.0;
        SumBoxesKPIY         = 0;
        PrcLastYearKPI       = 0.0;
        SumPrcLastYearKPI    = 0.0;
        NumBoxesLYKPI        = 0.0;
        SumBoxesQtyOIM       = 0;
        SumBoxesQtyOIY       = 0;
        SumBoxesQtyOILastY   = 0;
        SumWater             = 0;
        SumEnerg             = 0;
		
        SumSodas             = 0;
        SalesByB2B           = 0;
        SalesByTel           = 0;
        SalesByPresales      = 0;
        IntResidueGap1       = 0;
        IntResidueGap2       = 0;
        IntResidueGap3       = 0;
        IntBeerGap1          = 0;
        IntBeerGap2          = 0;
        IntBeerGap3          = 0;
        BeerGoalHL           = 0.0;
        BeerGoalBoxes        = 0;
        EDGoalHL             = 0.0;
        EDGoalBoxes          = 0;
        SodaGoalHL           = 0.0;
        SodaGoalBoxes        = 0;
        WaterGoalHL          = 0.0;
        WaterGoalBoxes       = 0;
        SumRefrigerators     = 0;
        Effectiveness        = 0;

        CoronaMegaCov        = 0;
        VictoriaMega         = 0;
        LeonMega             = 0;
        VictoriaLaton        = 0; 
        CoronaLaton          = 0;
        ModeloLaton          = 0;

        Familiar             = 0;
        VictoriaMediana      = 0;
        CoronaMediana        = 0;
        CobFamMod            = 0;
        Int1Dot5Lts          = 0;
        Stella               = 0;
        BudLight             = 0;
        CoronaLight          = 0;
        Michelob             = 0;
        Budweiser            = 0;
        Media                = 0;
        Craft                = 0;  
       	showDetail      ='inline';
   		//showAlertNoData = 'inline';
       }
        String StrRecTypeIdGoals = CTRSOQL.getRecordTypeId('ISSM_Goal__c','ISSM_OnCallAccount');
		System.debug('CurrentAccId : ' +CurrentAccId);
		System.debug('StrMonthName : '+StrMonthName);
		System.debug('StrYear : '+StrYear);
		System.debug('StrRecTypeIdGoals : '+StrRecTypeIdGoals);
        LstGoals = CTRSOQL.getGoalsPerCustomer(CurrentAccId, StrMonthName, StrYear, StrRecTypeIdGoals);
		System.debug('LstGoals : '+LstGoals);
        if(!LstGoals.isEmpty() && LstGoals.size() >0){
            BeerGoalHL     = LstGoals[0].ISSM_BeerMonthlyGoal__c != null ? LstGoals[0].ISSM_BeerMonthlyGoal__c : 0;
            BeerGoalBoxes  = Integer.valueOf(LstGoals[0].ISSM_BeerMonthlyGoalBox__c) != null ? Integer.valueOf(LstGoals[0].ISSM_BeerMonthlyGoalBox__c) : 0;
            EDGoalHL       = LstGoals[0].ISSM_EnergyDrinkMonthlyGoal__c != null ? LstGoals[0].ISSM_EnergyDrinkMonthlyGoal__c : 0;
            EDGoalBoxes    = Integer.valueOf(LstGoals[0].ISSM_EnergyDrinkMonthlyGoalBox__c) != null ? Integer.valueOf(LstGoals[0].ISSM_EnergyDrinkMonthlyGoalBox__c) : 0;
            SodaGoalHL     = LstGoals[0].ISSM_SodaMonthlyGoal__c != null ? LstGoals[0].ISSM_SodaMonthlyGoal__c : 0;
            SodaGoalBoxes  = Integer.valueOf(LstGoals[0].ISSM_SodaMonthlyGoalBox__c) != null ? Integer.valueOf(LstGoals[0].ISSM_SodaMonthlyGoalBox__c) : 0;         
            WaterGoalHL    = LstGoals[0].ISSM_WaterMonthlyGoal__c != null ? LstGoals[0].ISSM_WaterMonthlyGoal__c : 0;
            WaterGoalBoxes = Integer.valueOf(LstGoals[0].ISSM_WaterMonthlyGoalBox__c) != null ? Integer.valueOf(LstGoals[0].ISSM_WaterMonthlyGoalBox__c) : 0;
        }else{
         	BeerGoalHL           = 0.0;
	        BeerGoalBoxes        = 0;
	        EDGoalHL             = 0.0;
	        EDGoalBoxes          = 0;
	        SodaGoalHL           = 0.0;
	        SodaGoalBoxes        = 0;
	        WaterGoalHL          = 0.0;
	        WaterGoalBoxes       = 0;

        }
    }
}