/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_ComboDeleteDetail_tst {
	static ONTAP__Product__c ObjProduct2{get;set;}

@isTest static void testgetProductsDetailxCombination() {
       
       //	Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        //String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        
        ObjProduct2 = new ONTAP__Product__c(
			//RecordTypeId 				= StrRecType,
			ONTAP__MaterialProduct__c 	= 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
			ONCALL__Material_Number__c	= '2000020',
			ONTAP__UnitofMeasure__c  	= Label.ISSM_BOX,
			ISSM_Is_returnable__c  		= true,
			ISSM_Discount__c 			= 5.0
			
		);
		insert ObjProduct2;
		
        Test.startTest();        
        	ISSM_ComboDeleteDetail_cls.getProductsDetailxCombination('["'+ObjProduct2.Id +':6:9000000001:4:C1","a1Jg0000006N0lHEAS:5:9000000001:4:C1","a1Jg0000006N0lFEAS:6:9000000001:20:C2","a1Jg0000006N0l6EAC:5:9000000001:20:C2"]','9000000001');
        Test.stopTest();
    }  
    
     
@isTest static void testWprDetailCombinationCombo() {
        
        
        Test.startTest();  
        	ISSM_ComboDeleteDetail_cls.WprDetailCombinationCombo objClas = new ISSM_ComboDeleteDetail_cls.WprDetailCombinationCombo('String1', new List<ONTAP__Product__c>(),'String2','String3',0);      
        Test.stopTest();
    }  
}