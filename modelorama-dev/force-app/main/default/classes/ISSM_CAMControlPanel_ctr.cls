/****************************************************************************************************
    General Information
    -------------------
    author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall / CAM
    Customer:   Grupo Modelo

    Description: Class to show KPIs for CAM 

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       1-November-2017       Marco Zúñiga                 Class Creation
    1.1       21-May-2018           Daniel Rosales               Rip-off from original ISSM_ControlPanel_ctr, 
        adjustments for selected KPI category values for current month on Accounts for CAM
    1.2       01-Jun-2018           Daniel Rosales               Added Customer Category based on Custom Settings intervals
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_CAMControlPanel_ctr {
    public String CurrentAccId{get;set;}
    
    public List<ONTAP__KPI__c> lstKPIByCatCurrMonth{get;set;}
    public List<ONTAP__Account_Asset__c> lstAccAssets{get;set;}

    public Integer intKPI01BeerCurrMonth{get;set;}
    public Integer intKPI02WaterCurrMonth{get;set;}
    public Integer intKPI25EnergyCurrMonth{get;set;}
    public Integer intBeerBoxesThreshold{get;set;}
    public Integer sumRefrigerators{get;set;}    
    public string strCustomerCategory{get;set;}

    public ISSM_CAMControlPanel_ctr(ApexPages.StandardController sc) {
        CurrentAccId = sc.getId();
        intBeerBoxesThreshold = Integer.valueof(Label.ISSM_CAM_BeerBoxesThreshold);
        integer[] intKPICAtegories = new Integer[]{1, 2, 25};
        integer intKPIByCatCurrMonth = 0;
        intKPI01BeerCurrMonth   = 0;
        intKPI02WaterCurrMonth  = 0;
        intKPI25EnergyCurrMonth = 0;
        sumRefrigerators        = 0;
        strCustomerCategory     = '';
        
        for (integer intKPICategory: intKPICAtegories ){
            lstKPIByCatCurrMonth = 
                [SELECT ontap__kpi_name__c, ONTAP__Actual__c, ONTAP__Kpi_start_date__c
                FROM ONTAP__KPI__c 
                WHERE ONTAP__Account_id__c = :CurrentAccId 
                AND ONTAP__Kpi_id__c = :intKPICategory
                AND ontap__kpi_start_date__C = THIS_MONTH];
                
            if(!lstKPIByCatCurrMonth.isEmpty() && lstKPIByCatCurrMonth.size() > 0){
                for(ONTAP__KPI__c ObjKPI : lstKPIByCatCurrMonth){
                    intKPIByCatCurrMonth      += ObjKPI.ONTAP__Actual__c != null ? Integer.valueOf(ObjKPI.ONTAP__Actual__c) : 0;
                }
                if (intKPICategory == 1) { intKPI01BeerCurrMonth  = intKPIByCatCurrMonth; } else 
                if (intKPICategory == 2) { intKPI02WaterCurrMonth = intKPIByCatCurrMonth; } else 
                if (intKPICategory == 25) { intKPI25EnergyCurrMonth = intKPIByCatCurrMonth;}   
            }
        }

       lstAccAssets = [SELECT Id, ONTAP__Asset_Code__c, ISSM_AssetType__c, ONTAP__Account__c
                FROM ONTAP__Account_Asset__c
                WHERE ONTAP__Account__c =: CurrentAccId
                AND ISSM_AssetType__c =: Label.ISSM_R];
        if(!LstAccAssets.isEmpty() && LstAccAssets.size() > 0){
            sumRefrigerators = LstAccAssets.size();
        }

        List<ISSM_Customer_Category__c> lstCustomerCategory = 
            new List<ISSM_Customer_Category__c>
                ([SELECT Name, Category_Status__c, Final_Volume_Beer_Boxes__c
                FROM ISSM_Customer_Category__c
                WHERE Category_Status__c = TRUE
                ORDER BY Final_Volume_Beer_Boxes__c DESC]);
        
        for(ISSM_Customer_Category__c cc:lstCustomerCategory){
            if(intKPI01BeerCurrMonth >= cc.Final_Volume_Beer_Boxes__c){
                strCustomerCategory = cc.Name;
                break;
            }
        }
    }
}