/**
 * This class provides methods that generate reusable creation of data.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public with sharing class AllMobileUtilityHelperTest {

	/**
	 * Method to create a AllMobileVersionControl object.
	 */
	public static AllMobileVersionControl__c createVersionControlObject(Date datTValidStartDate, Date datTValidEndDate, Boolean blnSoftDeleteFlag, RecordType objVersionControlRecordType, AllMobileApplication__c objAllMobileApplication, AllMobileVersion__c objAllMobileVersion, ONTAP__Route__c objONTAPRoute, Account objAccountSalesOffice, Account objAccountSalesOrg) {
		AllMobileVersionControl__c objAllMobileVersionControl = new AllMobileVersionControl__c(RecordTypeId = objVersionControlRecordType.Id, AllMobileApplicationLK__c = objAllMobileApplication.Id, AllMobileVersionLK__c = objAllMobileVersion.Id, AllMobileRouteLK__c = objONTAPRoute.Id, AllMobileValidStartDate__c = datTValidStartDate, AllMobileValidEndDate__c = datTValidEndDate, AllMobileSoftDeleteFlag__c = blnSoftDeleteFlag, AllMobileSalesOfficeLK__c = objAccountSalesOffice.Id, AllMobileSalesOrgLK__c = objAccountSalesOrg.Id);
		return objAllMobileVersionControl;
	}

	/**
	 * Method to create a AllMobileApplication object.
	 */
	public static AllMobileApplication__c createAllMobileApplicationObject(String strApplicationName, String strApplicationId) {
		AllMobileApplication__c objAllMobileApplication = new AllMobileApplication__c(Name = strApplicationName, AllMobileApplicationId__c = strApplicationId);
		return objAllMobileApplication;
	}

	/**
	 * Method to create a AllMobileVersion object.
	 */
	public static AllMobileVersion__c createAllMobileVersionObject(String strVersionId, AllMobileApplication__c objAllMobileApplication) {
		AllMobileVersion__c objAllMobileVersion = new AllMobileVersion__c(AllMobileApplicationMD__c = objAllMobileApplication.Id, AllMobileVersionId__c = strVersionId);
		return objAllMobileVersion;
	}

	/**
	 * Method to get RecordType object.
	 */
	public static RecordType getRecordTypeObject(String strRecordTypeDeveloperName) {
		RecordType objRecordType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName =: strRecordTypeDeveloperName LIMIT 1];
		return objRecordType;
	}

	/**
	 * Method to create Ontap__Route object.
	 */
	public static ONTAP__Route__c createOntapRouteObject(RecordType objRecordType, User objUserAgent,  String strServiceModel, String strRouteId, String strRouteName, Account objAccountSalesOffice, ONTAP__Vehicle__c objOnTapVehicle) {
		ONTAP__Route__c objONTAPRoute = new ONTAP__Route__c(RecordTypeId = objRecordType.Id, RouteManager__c = objUserAgent.Id, ServiceModel__c = strServiceModel, ONTAP__RouteId__c = strRouteId, ONTAP__RouteName__c = strRouteName, ONTAP__SalesOffice__c = objAccountSalesOffice.Id, Vehicle__c = objOnTapVehicle.Id);
		return objONTAPRoute;
	}

	/**
	 * Method to create SalesOffice object.
	 */
	public static Account createSalesOfficeObject(RecordType objRecordType, String strSalesOfficeName, String strONTAPSalesOffId, Account objAccountSalesOrg) {
		Account objSalesOffice = new Account(RecordTypeId = objRecordType.Id, Name = strSalesOfficeName, ONTAP__SalesOffId__c = strONTAPSalesOffId, ISSM_SalesOrg__c = objAccountSalesOrg.Id);
		return objSalesOffice;
	}

	/**
	 * Method to create SalesOrg object.
	 */
	public static Account createSalesOrgObject(RecordType objRecordType, String strName) {
		Account objSalesOrg = new Account(RecordTypeId = objRecordType.Id, Name = strName);
		return objSalesOrg;
	}

	/**
	 * Method to create UserRole object.
	 */
	public static UserRole createUserRoleObject(String strName, String strDeveloperName) {
		UserRole objUserRole = new UserRole(Name = strName, DeveloperName = strDeveloperName);
		return objUserRole;
	}

	/**
	 * Method to get Profile object.
	 */
	public static Profile getProfileObject(String strProfileName) {
		Profile objProfile = [SELECT Id, Name FROM Profile WHERE Name =: strProfileName];
		return objProfile;
	}

	/**
	 * Method to create User object.
	 */
	public static User createUserObject(Profile objProfile, String strUserName, String strAlias, String strEmail, String strEmailEncodingKey, String strLastName, String strLanguageLocaleKey, String strLocaleSidKey, String strTimeZoneSidKey) {
		User objUser = new User(UserName = strUserName, Alias = strAlias, Email = strEmail, EmailEncodingKey = strEmailEncodingKey, LastName = strLastName, LanguageLocaleKey = strLanguageLocaleKey,
										LocaleSidKey=strLocaleSidKey, ProfileId = objProfile.Id, TimeZoneSidKey = strTimeZoneSidKey );
		return objUser;
	}

	/**
	 * Method to create a Vehicle.
	 */
	public static ONTAP__Vehicle__c createOnTapVehicle(String strVehicleName, Account objAccountSalesOffice, String strVehicleId, String strPlateNumber, Boolean boolOutOfService, Decimal decOdometer, String strFleetNumber) {
		ONTAP__Vehicle__c objOnTapVehicle = new ONTAP__Vehicle__c(ONTAP__VehicleName__c = strVehicleName, ONTAP__SalesOffice__c = objAccountSalesOffice.Id, ONTAP__SalesOfficeId__c = objAccountSalesOffice.ONTAP__SalesOffId__c, ONTAP__VehicleId__c = strVehicleId, ONTAP__PlateNumber__c = strPlateNumber, ONTAP__OutofService__c = boolOutOfService, ONTAP__Odometer__c = decOdometer, ONTAP__FleetNumber__c = strFleetNumber);
		return objOnTapVehicle;
	}

	/**
	 * Method to create a RouteAppVersion.
	 */
	public static ALlMobileRouteAppVersion__c createAllMobileRouteAppVersion(String strRouteId, String strVersionId, Decimal decApplicationId, Date datValidStartDate, Date datValidEndDate, Boolean boolSoftDeleteFlag) {
		ALlMobileRouteAppVersion__c objALlMobileRouteAppVersion = new ALlMobileRouteAppVersion__c(AllMobileRouteId__c = strRouteId, AllMobileVersionId__c = strVersionId, AllMobileApplicationId__c = decApplicationId, AllMobileValidStartDate__c = datValidStartDate, AllMobileValidEndDate__c = datValidEndDate, AllMobileSoftDeleteFlag__c = boolSoftDeleteFlag);
		return objALlMobileRouteAppVersion;
	}

	/**
	 * Method to crate a Custom Settings.
	 */
	public static AllMobileEndPointConfigurationsWS__c createAllMobileCustomSetting(String strName, String strEndPoint, String strHeaderAccessTokenKeyName, String strHeaderAccessTokenValue, String strHeaderContentTypeKeyName, String strHeaderContentTypeValue, String strMethod) {
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = new AllMobileEndPointConfigurationsWS__c(Name = strName, AllMobileEndPoint__c = strEndPoint, AllMobileHeaderAccessTokenKeyName__c = strHeaderAccessTokenKeyName, AllMobileHeaderAccessTokenValue__c = strHeaderAccessTokenValue, AllMobileHeaderContentTypeKeyName__c = strHeaderContentTypeKeyName, AllMobileHeaderContentTypeValue__c = strHeaderContentTypeValue, AllMobileMethod__c = strMethod);
		return objAllMobileEndPointConfigurationsWS;
	}

	/**
	 * Method to crate a Custom Settings.
	 */
	public static ISSM_PriceEngineConfigWS__c createISSMPriceEngineConfigWSCustomSetting(String strName, String strEndPoint, String strHeaderAccessTokenValue, String strHeaderContentTypeValue, String strMethod) {
		ISSM_PriceEngineConfigWS__c objISSMPriceEngineConfigWS = new ISSM_PriceEngineConfigWS__c(Name = strName, ISSM_EndPoint__c = strEndPoint, ISSM_AccessToken__c = strHeaderAccessTokenValue, ISSM_HeaderContentType__c = strHeaderContentTypeValue, ISSM_Method__c = strMethod);
		return objISSMPriceEngineConfigWS;
	}

	/**
	 * Method to insert AllMobile Custom Settings.
	 */
	public static void insertAllMobileCustomSetting() {
		List<AllMobileEndPointConfigurationsWS__c> lstAllMobileEndPointConfigurationsWS = new List<AllMobileEndPointConfigurationsWS__c>();
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionReadAll = createAllMobileCustomSetting('AllMobileRouteAppVersionReadAll', 'https://abimx-allmobile-ext.herokuapp.com/routeAppVersion/all', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'GET');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionReadAll);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileVersionReadAll = createAllMobileCustomSetting('AllMobileVersionReadAll', 'https://abimx-allmobile-ext.herokuapp.com/version/all', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'GET');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileVersionReadAll);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileApplicationReadAll = createAllMobileCustomSetting('AllMobileApplicationReadAll', 'https://abimx-allmobile-ext.herokuapp.com/application/all', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'GET');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileApplicationReadAll);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionInsert = createAllMobileCustomSetting('AllMobileRouteAppVersionInsert', 'https://abimx-allmobile-ext.herokuapp.com/routeAppVersion/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionInsert);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileVersionInsert = createAllMobileCustomSetting('AllMobileVersionInsert', 'https://abimx-allmobile-ext.herokuapp.com/version/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileVersionInsert);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileApplicationInsert = createAllMobileCustomSetting('AllMobileApplicationInsert', 'https://abimx-allmobile-ext.herokuapp.com/application/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileApplicationInsert);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionUpdate = createAllMobileCustomSetting('AllMobileRouteAppVersionUpdate', 'https://abimx-allmobile-ext.herokuapp.com/routeAppVersion/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'PUT');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileRouteAppVersionUpdate);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileVersionUpdate = createAllMobileCustomSetting('AllMobileVersionUpdate', 'https://abimx-allmobile-ext.herokuapp.com/version/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'PUT');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileVersionUpdate);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileApplicationUpdate = createAllMobileCustomSetting('AllMobileApplicationUpdate', 'https://abimx-allmobile-ext.herokuapp.com/application/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'PUT');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileApplicationUpdate);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserReadAll = createAllMobileCustomSetting('AllMobileDeviceUserReadAll', 'https://abimx-allmobile-ext.herokuapp.com/deviceUser/all', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'GET');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserReadAll);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserInsert = createAllMobileCustomSetting('AllMobileDeviceUserInsert', 'https://abimx-allmobile-ext.herokuapp.com/deviceUser/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserInsert);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserUpdate = createAllMobileCustomSetting('AllMobileDeviceUserUpdate', 'https://abimx-allmobile-ext.herokuapp.com/deviceUser/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'PUT');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserUpdate);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserEncrypt = createAllMobileCustomSetting('AllMobileDeviceUserEncrypt', 'https://abimx-allmobile-ext.herokuapp.com/encrypt/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'text/plain', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileDeviceUserEncrypt);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeReadAll = createAllMobileCustomSetting('AllMobileCatUserTypeReadAll', 'https://abimx-allmobile-ext.herokuapp.com/catUserType/all', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'GET');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeReadAll);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeInsert = createAllMobileCustomSetting('AllMobileCatUserTypeInsert', 'https://abimx-allmobile-ext.herokuapp.com/catUserType/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'POST');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeInsert);
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeUpdate = createAllMobileCustomSetting('AllMobileCatUserTypeUpdate', 'https://abimx-allmobile-ext.herokuapp.com/catUserType/', 'Access-Token', 'VkSuwKufXZEf9A75Dukv', 'Content-Type', 'application/json', 'PUT');
		lstAllMobileEndPointConfigurationsWS.add(objAllMobileEndPointConfigurationsWS_AllMobileCatUserTypeUpdate);
		insert lstAllMobileEndPointConfigurationsWS;

		//ISSM End Point Configurations.
		List<ISSM_PriceEngineConfigWS__c> lstISSMPriceEngineConfigWS = new List<ISSM_PriceEngineConfigWS__c>();
		ISSM_PriceEngineConfigWS__c objISSMPriceEngineConfigWS_InsertHerokuTours = createISSMPriceEngineConfigWSCustomSetting('InsertHerokuTours', 'https://mex-issm-int-api-uat.herokuapp.com/api/v1/tours', 'Basic RDM5N0ZDN1hBVjhnekFxUjd2NnJXUkdybjRzU3hBejVyOFpSWUdxZzpOZnVhdlhIUUVVeXlLYnpqQWtqR2NCVHlVcVYzMlhnc1pGWUNHUjlQ', 'application/json; charset=UTF-8', 'POST');
		lstISSMPriceEngineConfigWS.add(objISSMPriceEngineConfigWS_InsertHerokuTours);
		ISSM_PriceEngineConfigWS__c objISSMPriceEngineConfigWS_InsertHerokuEvents = createISSMPriceEngineConfigWSCustomSetting('InsertHerokuEvents', 'https://mex-issm-int-api-uat.herokuapp.com/api/v1/events', 'Basic RDM5N0ZDN1hBVjhnekFxUjd2NnJXUkdybjRzU3hBejVyOFpSWUdxZzpOZnVhdlhIUUVVeXlLYnpqQWtqR2NCVHlVcVYzMlhnc1pGWUNHUjlQ', 'application/json; charset=UTF-8', 'POST');
		lstISSMPriceEngineConfigWS.add(objISSMPriceEngineConfigWS_InsertHerokuEvents);
		insert lstISSMPriceEngineConfigWS;
	}

	/**
	 * Method to create RouteAppVersion Heroku object.
	 */
	public static AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass createRouteAppVersionHerokuObjectClass(Integer intRouteAppVersionId, String strRoute, String strVersionId, Date datValidStartDate, Date datValidEndDate, Boolean boolSoftDeleteFlag, DateTime datTLastModifiedDate) {
		AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass();
		objAllMobileRouteAppVersionHerokuObjectClass.routeappversionid = intRouteAppVersionId;
		objAllMobileRouteAppVersionHerokuObjectClass.route = strRoute;
		objAllMobileRouteAppVersionHerokuObjectClass.version_id = strVersionId;
		objAllMobileRouteAppVersionHerokuObjectClass.validstart = datValidStartDate;
		objAllMobileRouteAppVersionHerokuObjectClass.validend = datValidEndDate;
		objAllMobileRouteAppVersionHerokuObjectClass.softdeleteflag = boolSoftDeleteFlag;
		objAllMobileRouteAppVersionHerokuObjectClass.dtlastmodifieddate = datTLastModifiedDate;
		return objAllMobileRouteAppVersionHerokuObjectClass;
	}

	/**
	 * Method to create Version Heroku object.
	 */
	public static AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass createVersionHerokuObjectClass(String strVersionId, Integer intApplicationId, Boolean boolSoftDeleteFlag, DateTime datTLastModifiedDate) {
		AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass();
		objAllMobileVersionHerokuObjectClass.version_id = strVersionId;
		objAllMobileVersionHerokuObjectClass.application_id = intApplicationId;
		objAllMobileVersionHerokuObjectClass.softdeleteflag = boolSoftDeleteFlag;
		objAllMobileVersionHerokuObjectClass.dtlastmodifieddate = datTLastModifiedDate;
		return objAllMobileVersionHerokuObjectClass;
	}

	/**
	 * Method to create Application Heroku object.
	 */
	public static AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass createApplicationHerokuObjectClass(Integer intApplicationId,String strName, String strShipType, Integer intLevelValidation, Boolean boolSoftDeleteFlag, DateTime datTLastModifiedDate) {
		AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass();
		objAllMobileApplicationHerokuObjectClass.application_id = intApplicationId;
		objAllMobileApplicationHerokuObjectClass.name = strName;
		objAllMobileApplicationHerokuObjectClass.ship_type = strShipType;
		objAllMobileApplicationHerokuObjectClass.levelvalidation = intLevelValidation;
		objAllMobileApplicationHerokuObjectClass.softdeleteflag = boolSoftDeleteFlag;
		objAllMobileApplicationHerokuObjectClass.dtlastmodifieddate = datTLastModifiedDate;
		return objAllMobileApplicationHerokuObjectClass;
	}

	/**
	 * Method to create DeviceUser Heroku object.
	 */
	public static AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass createDeviceUserHerokuObjectClass(String strId, String strUserType, String strVkbur, String strUserName, Boolean boolIsActive, String strRoute, String strEmail, String strLdapId, String strPassword, Boolean boolSoftDeleteFlag, DateTime datTLastModifiedDate) {
		AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass();
		objAllMobileDeviceUserHerokuObjectClass.id = strId;
		objAllMobileDeviceUserHerokuObjectClass.user_type = strUserType;
		objAllMobileDeviceUserHerokuObjectClass.vkbur = strVkbur;
		objAllMobileDeviceUserHerokuObjectClass.user_name = strUserName;
		objAllMobileDeviceUserHerokuObjectClass.isactive = boolIsActive;
		objAllMobileDeviceUserHerokuObjectClass.route = strRoute;
		objAllMobileDeviceUserHerokuObjectClass.email = strEmail;
		objAllMobileDeviceUserHerokuObjectClass.ldap_id = strLdapId;
		objAllMobileDeviceUserHerokuObjectClass.password = strPassword;
		objAllMobileDeviceUserHerokuObjectClass.softdeleteflag = boolSoftDeleteFlag;
		objAllMobileDeviceUserHerokuObjectClass.dtlastmodifieddate = datTLastModifiedDate;
		return objAllMobileDeviceUserHerokuObjectClass;
	}

	/**
	 * Method to create CatUserType Heroku object.
	 */
	public static AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass createCatUserTypeHerokuObjectClass(String strUserType, String strUserTypeDescription, Boolean boolAdmonExterna, String strPermisos, Boolean boolSoftDeleteFlag, DateTime datTLastModifiedDate) {
		AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass();
		objAllMobileCatUserTypeHerokuObjectClass.user_type = strUserType;
		objAllMobileCatUserTypeHerokuObjectClass.user_type_desc = strUserTypeDescription;
		objAllMobileCatUserTypeHerokuObjectClass.admon_externa = boolAdmonExterna;
		objAllMobileCatUserTypeHerokuObjectClass.permisos = strPermisos;
		objAllMobileCatUserTypeHerokuObjectClass.softdeleteflag = boolSoftDeleteFlag;
		objAllMobileCatUserTypeHerokuObjectClass.dtlastmodifieddate = datTLastModifiedDate;
		return objAllMobileCatUserTypeHerokuObjectClass;
	}

	/**
	 * Method to create AllMobilePermissions.
	 */
	public static AllMobilePermission__c createAllMobilePermission(String strPermissionKey, String strPermissionName, Integer intBinaryPosition) {
		AllMobilePermission__c objAllMobilePermission = new AllMobilePermission__c(AllMobileDeviceUserPermissionKey__c = strPermissionKey, AllMobileDeviceUserPermissionName__c = strPermissionName, AllMobileDeviceUserBinaryPosition__c = intBinaryPosition);
		return objAllMobilePermission;
	}

	/**
	 * Method to create AllMobilePermissionCategoryUserType.
	 */
	public static AllMobilePermissionCategoryUserType__c createAllMobilePermissionCategoryUserType(String strName, String strPermissionDescription, String strPermissionName, String strBinaryPermission) {
		AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserType = new AllMobilePermissionCategoryUserType__c(Name = strName, AllMobileDeviceUserPermissionDescription__c = strPermissionDescription, AllMobileDeviceUserPermissionName__c = strPermissionName, AllMobileDeviceUserBinaryPermission__c = strBinaryPermission);
		return objAllMobilePermissionCategoryUserType;
	}

	/**
	 * Method to create AllMobileCatUserType.
	 */
	public static AllMobileCatUserType__c createAllMobileCatUserType(String strRoleIdentifierName, Boolean boolExternalManagement, Boolean boolSoftDeleteFlag, String strUserTypeDescription, String strUserType, AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserType) {
		AllMobileCatUserType__c objAllMobileCatUserType = new AllMobileCatUserType__c(Name = strRoleIdentifierName, AllMobileDeviceUserExternalManagement__c = boolExternalManagement, AllMobileDeviceUserSoftDeleteFlag__c = boolSoftDeleteFlag, AllMobileDeviceUserUserTypeDescription__c = strUserTypeDescription, AllMobileDeviceUserUserType__c = strUserType, AllMobileDeviceUserPermissionsLK__c = objAllMobilePermissionCategoryUserType.Id);
		return objAllMobileCatUserType;
	}

	/**
	 * Method to create AllMobileDeviceUser.
	 */
	public static AllMobileDeviceUser__c createAllMobileDeviceUser(Boolean boolSoftDeleteFlag, String strPasswordText, String strPasswordEncrypted, String strEmail, String strLdapId, String strUserId, AllMobileCatUserType__c objAllMobileCatUserType, String strUserName, Account objAccountSalesOffice, ONTAP__Route__c objONTAPRoute, Boolean boolIsActive) {
		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c(AllMobileDeviceUserSoftDeleteFlag__c = boolSoftDeleteFlag, AllMobileDeviceUserPasswordText__c = strPasswordText, AllMobileDeviceUserPasswordEncrypted__c = strPasswordEncrypted, AllMobileDeviceUserEmail__c = strEmail, AllMobileDeviceUserLdapId__c = strLdapId, AllMobileDeviceUserId__c = strUserId, AllMobileDeviceUserUserTypeLK__c = objAllMobileCatUserType.Id, AllMobileDeviceUserUserName__c = strUserName, AllMobileDeviceUserVkburLK__c = objAccountSalesOffice.Id, AllMobileDeviceUserRouteLK__c = objONTAPRoute.Id, AllMobileDeviceUserIsActive__c = boolIsActive);
		return objAllMobileDeviceUser;
	}
}