/****************************************************************************************************
    General Information
    -------------------
    author:     Joseph Ceron
    email:      jceron@gmail.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Class for site control   

    Information about changes (versions) 
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       15/08/2017      Joseph Ceron  JC              Batch create for calls creation
    2.0       15/09/2017      Hector Diaz   HD              Class created decorate class implemented --> ISSM_ProcessOpenItemTelecollection_bch
    ================================================================================================
****************************************************************************************************/
global class ISSM_LaunchCampaingTelecolletion_bch implements Database.Batchable<sObject> {
    public Map<String,ONCALL__Call__c> MapCall;
    public ISSM_AppSetting_cs__c oIdQueueWithoutOwner;
    String query;
    String strOrder;
    String strCampaing;
    String openItemsId;
    Date startDate;
    Date endDate;
    Boolean active;
    String idCampaign;
    String soqlFilters;
    
    global ISSM_LaunchCampaingTelecolletion_bch(String strCampaignName, Date startDateParam, Date endDateParam, Boolean activeParam, String idCampaignParam, String soqlFiltersParam) {
        startDate = startDateParam;
        endDate = endDateParam;
        active = activeParam;
        strCampaing = strCampaignName;
        idCampaign = idCampaignParam;
        soqlFilters = soqlFiltersParam;
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Account_RecordType);
        query = 'select Id,Name,ONCALL__Risk_Category__c,ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c, ISSM_LastPromisePaymentDate__c,' +
                    '(Select id,ISSM_Account__c,ISSM_DocumentId__c, ISSM_Amounts__c, ISSM_ExpirationDays__c,ISSM_Debit_Credit__c From Open_Items__r)' +
                    'FROM Account ' +
                    'WHERE Id IN(Select ISSM_Account__c From ISSM_OpenItemB__c) AND ' +
                    'ISSM_FilterCondicion__c = true ' +
                    'AND RecordTypeId=\''+ RecordTypeAccountId +'\'  '+ soqlFilters +'  Limit 10000';
        System.debug('--query____>>>>>'+query);
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
            return Database.getQueryLocator(query);
        } catch(Exception e) { System.debug(e.getMessage()); return null; }
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
           
        Decimal DecSumOutStand = 0;
        Decimal DecMaxVal = 0;
        String Strdate = '';
        String StrRiskCat = '';
        Decimal StrMinInterval = 0;
        Decimal StrMaxInterval = 0;
        Double dblDebit = 0.0;  //DEBE debe-haber
        Double dblCredit = 0.0; //HABER 
        Double dbAmount = 0.0;
        ISSM_TelecolletionOrder__c telecollectionOrder;
        List<String> lstDocumentTypeOrderI =  new List<String>();
        Set<Account> setAccount =  new Set<Account>();
        
        Decimal SaldoVencidoMayorA_dec = 0;
        Decimal RangoMinimoAceptado_dec = 0;
        Decimal RangoMaximoAceptado_dec = 0;
        
        /*Valida que la cuenta tenga open item y que si por lo menos uno es mayoa 90 lo elimina del set y si la open item en la Deuda('S') es < 7 tambien lo elimina */
        for (Account objAccount : (List<Account>)scope) { 
            try {
                setAccount.add(objAccount);
                
                List<ISSM_ParametersOpenItem__c> paramOI_lst = new List<ISSM_ParametersOpenItem__c>();
                paramOI_lst = [SELECT Id, ISSM_SaldoVencidoMayorA__c, ISSM_RangoMinimoAceptado__c, ISSM_RangoMaximoAceptado__c, ISSM_Active__c
                               FROM ISSM_ParametersOpenItem__c
                               WHERE ISSM_Active__c = true];
                if (paramOI_lst.size() > 0) {
                    for (ISSM_ParametersOpenItem__c paramOI : paramOI_lst) {
                        SaldoVencidoMayorA_dec = paramOI.ISSM_SaldoVencidoMayorA__c;
                        RangoMinimoAceptado_dec = paramOI.ISSM_RangoMinimoAceptado__c;
                        RangoMaximoAceptado_dec = paramOI.ISSM_RangoMaximoAceptado__c;
                    }
                }
                
                if (!objAccount.Open_Items__r.isEmpty()) {
                    
                    for (ISSM_OpenItemB__c objOpenI : objAccount.Open_Items__r) {
                       
                        if (objOpenI.ISSM_ExpirationDays__c > RangoMaximoAceptado_dec) { setAccount.remove(objAccount); break; }
                        
                        if (objOpenI.ISSM_ExpirationDays__c < RangoMinimoAceptado_dec && objOpenI.ISSM_Debit_Credit__c == System.label.ISSM_Debit) { setAccount.remove(objAccount); break; }
                        
                        if (objOpenI.ISSM_Amounts__c <= SaldoVencidoMayorA_dec) { setAccount.remove(objAccount); break; }
                    }
                    
                // Si la cuenta no tiene Open Items elimina el registro sel SET
                } else { setAccount.remove(objAccount); }   
            
            } catch(QueryException QE) { setAccount.remove(objAccount); }
            
        } 
 
        Set<ID> idsAccounts = new ISSM_Account_cls().getMapAccountId((List<Account>)scope).keySet();
        System.debug('Lista Final de Cuentas: ' + idsAccounts);
       
        //Configuracion ISSM_IntervalsByCountry
       /* String IdUser = UserInfo.getUserId(); 
       String CStrCountry = [Select Id,Name,Country from user Where id = : IdUser limit 1].Country;
        for(ISSM_IntervalsByCountry__c oCustomIntervals : [Select id,ISSM_Country__c,ISSM_maxInterval__c,ISSM_minInterval__c FROM ISSM_IntervalsByCountry__c WHERE ISSM_Country__c = :CStrCountry]){
            if(oCustomIntervals.ISSM_minInterval__c!= null && oCustomIntervals.ISSM_maxInterval__c!= null){
                StrMinInterval = Decimal.valueOf(oCustomIntervals.ISSM_minInterval__c);
                StrMaxInterval = Decimal.valueOf(oCustomIntervals.ISSM_maxInterval__c);
            }
        }*/
        //Configuracion ISS_DocumentTypeOpenItems__c
        
        for (ISS_DocumentTypeOpenItems__c objDocumentTypeOrderI : [SELECT Id, Name FROM ISS_DocumentTypeOpenItems__c WHERE ISSM_Active__c = true]) { lstDocumentTypeOrderI.add(objDocumentTypeOrderI.Name); }
        List<ISSM_OpenItemB__c> lstOpenItem = new ISSM_CustomerServiceQuerys_cls().getOpenItems(idsAccounts, lstDocumentTypeOrderI);
        List<ISSM_TelecolletionOrder__c> telecollectionOrderList = new List<ISSM_TelecolletionOrder__c>();
        for (Account account : setAccount) {
                    
            DecSumOutStand = 0;
            DecMaxVal = 0;
            openItemsId = '';
            dblDebit = 0.0;
            dblCredit = 0.0; 

            for (ISSM_OpenItemB__c objIteraOI : lstOpenItem) {
                dbAmount = 0.0;
                
                if (objIteraOI.ISSM_Account__c == account.ID) {
                    DecSumOutStand += objIteraOI.ISSM_Amounts__c;
                    if (objIteraOI.ISSM_Debit_Credit__c == System.label.ISSM_Debit ) { DecMaxVal = (objIteraOI.ISSM_ExpirationDays__c > DecMaxVal)? objIteraOI.ISSM_ExpirationDays__c: DecMaxVal; }
                    openItemsId += objIteraOI.ID + ';';
                }
            }
                 
             /*  
                if(objIteraOI.ISSM_Debit_Credit__c == 'D' ){
                    dblDebit = dblDebit + objIteraOI.ONTAP__Amount__c;
                    System.debug('dblDebit = '+dblDebit);
                }
                if(objIteraOI.ISSM_Debit_Credit__c == 'H') {
                    dblCredit = dblCredit + objIteraOI.ONTAP__Amount__c;
                    System.debug('dblCredit = '+dblCredit);
                }   
                
                if(dblDebit > 0.0 &&  dblCredit > 0.0 ){
                    System.debug('EXISTEN LOS " VALORES : DEBE / HABER '+ dblDebit  +'---'+dblCredit );
                    dbAmount = dblDebit-dblCredit;
                    System.debug('MONTO TOTAL A DEBER  : '+dbAmount);
                }
           
                
                if(dblDebit > 0.0 && dblCredit==0.0){
                    dbAmount = dblDebit;
                    System.debug('SOLO EXISTE DEBE : dblDebit '+dblDebit );
                }
            */
         
            openItemsId = openItemsId.removeEnd(';');
            //if(dbAmount > 0 || DecMaxVal > 0) {
                telecollectionOrder = new ISSM_TelecolletionOrder__c();
                telecollectionOrder.Telecollection_Campaign__c = idCampaign;
                telecollectionOrder.Active__c = active;
                telecollectionOrder.Start_Date__c = startDate;
                telecollectionOrder.End_Date__c = endDate;
                telecollectionOrder.ISSM_POC__c = account.Id;
                telecollectionOrder.AccountName__c = account.Name;
                telecollectionOrder.CampaingName__c = strCampaing;
                telecollectionOrder.ISSM_OutstandingBalanceAmount__c = DecSumOutStand;//dbAmount;
                telecollectionOrder.ISSM_MaximunDaysOpenItem__c = DecMaxVal;
                telecollectionOrder.ISSM_LastContactDate__c = account.ISSM_LastContactDate__c;
                telecollectionOrder.ISSM_LastPaymentPlanDate__c = account.ISSM_LastPaymentPlanDate__c;
                telecollectionOrder.ISSM_LastPromisePaymentDate__c = account.ISSM_LastPromisePaymentDate__c;
                telecollectionOrder.OpenItemsId__c = openItemsId;
                telecollectionOrder.ISSM_RiskCategory__c = (account.ONCALL__Risk_Category__c != null) ? account.ONCALL__Risk_Category__c : '';
                telecollectionOrderList.add(telecollectionOrder);
            //}

        }
        Insert telecollectionOrderList;
        
        DecSumOutStand = 0;

    }
    
    global void finish(Database.BatchableContext BC) {
        ISSM_CreateCampaingTelecollection_bch generateCampaingBch = new ISSM_CreateCampaingTelecollection_bch(idCampaign, soqlFilters); 
        database.executeBatch(generateCampaingBch, 150);
    }
    
}