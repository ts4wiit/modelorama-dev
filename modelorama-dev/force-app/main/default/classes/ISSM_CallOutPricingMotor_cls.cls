///****************************************************************************************************
//    General Information
//    -------------------
//    author:     Luis Licona
//    email:      llicona@avanxo.com
//    company:    Avanxo México
//    Project:    Salesforce Implementatio 
//    Customer:   Grupo Modelo

//    Description:
//    Class that makes the call of the service for price engine, 
//    resulting in the list of prices and discounts of the products selected by the customer

//    Information about changes (versions)
//    ================================================================================================
//    Number    Dates           Author                       Description          
//    ------    --------        --------------------------   -----------------------------------------
//    1.0       19/06/2017      Luis Licona                  Class creation
//    1.1       04/07/2017      Luis Licona                  Add method for test class
//    ================================================================================================
//****************************************************************************************************/
public with sharing class ISSM_CallOutPricingMotor_cls {

//	public WrpOrderItem[] LstWrpOI 	  = new List<WrpOrderItem>();
//	public WrpPricingMotor[] LstWrpPM = new List<WrpPricingMotor>();
//	public Map<String,ONTAP__Order_Item__c> MapWS = new Map<String,ONTAP__Order_Item__c>();
//	public Account ObjAcc = new Account();
//	public ISSM_DeserializePricingMotor_cls ObjDeserealized{get;set;}


//	/**
//    * It makes the call to the motor service of prices, 
//    * resulting in the list of prices and discounts of the products
//    * @param   StrIdAcc: Id Account  
//    * @param   LstProd:  List of Products
//    * @param   MapProd:  Map contain id and quantity of product
//    * @return  ISSM_DeserializePricingMotor_cls: return list of Products convert in Order Item
//    * with your pricing and total
//    **/
//	public ISSM_DeserializePricingMotor_cls CallOutPricingMotor(String StrIdAcc, ONTAP__Product__c[] LstProd,Map<Id, Decimal> MapProd){
//		//get values of map
//        Map<String, ISSM_PriceEngineConfigWS__c> MapConfiguracionWs = ISSM_PriceEngineConfigWS__c.getAll();   
//    	String StrToken = MapConfiguracionWs.get('Configuration WS').ISSM_AccessToken__c;  
		
//		for(ONTAP__Product__c objProd : LstProd){
//            LstWrpOI.add(new WrpOrderItem(objProd.ONCALL__Material_Number__c,Integer.valueOf(MapProd.get(objProd.Id))));
//		}
		
//		if(!LstWrpOI.isEmpty()){
//			ObjAcc = GetInfoAccount(StrIdAcc);
//			LstWrpPM.add(new WrpPricingMotor(StrToken,ObjAcc.ONTAP__SAPCustomerId__c,ObjAcc.ONTAP__SalesOrgId__c,ObjAcc.ONTAP__ChannelId__c,LstWrpOI));
//		}

//		String StrSerializeJson = JSON.serializePretty(LstWrpPM[0]);
//		System.debug('StrSerializeJson-->'+StrSerializeJson);

//        String MessageWs ='';
//        if(!Test.isRunningTest()){
//            Http http = new Http();
//            HttpRequest request = new HttpRequest();
//            request.setEndpoint(MapConfiguracionWs.get('Configuration WS').ISSM_EndPoint__c);
//            request.setMethod(MapConfiguracionWs.get('Configuration WS').ISSM_Method__c);
//            request.setHeader('Content-Type',MapConfiguracionWs.get('Configuration WS').ISSM_HeaderContentType__c);
//            request.setBody(StrSerializeJson);
//            HttpResponse response = http.send(request);
            
//            if(response.getStatusCode() != 200)
//            	MessageWs = response.getBody();       
//            else
//            	ObjDeserealized = ISSM_DeserializePricingMotor_cls.parse(response.getBody());
           
//        	System.debug('RESPONSE-->'+response.getBody());
//        	System.debug('MessageWs-->'+MessageWs);
            
//        }else if(Test.isRunningTest()){
//        	// Set mock callout class 
//        	Test.setMock(HttpCalloutMock.class, new ISSM_MockHttpResponseGenerator());	
//       		CalloutClass ctr = new CalloutClass();

//       		HttpResponse res = ctr.getInfoFromExternalService();
//        	// Parse the JSON response
//            ObjDeserealized  = ISSM_DeserializePricingMotor_cls.parse(res.getBody());
//        }
//  		return ObjDeserealized;
//	}

//    /**
//    * Method for obtaining account information
//    * @param   StrIdAcc: Id Account  
//    * @return  Object of Account
//    **/
//	public Account GetInfoAccount(String StrIdAcc){
//		return [SELECT ONTAP__SAPCustomerId__c,ONTAP__SalesOrgId__c,ONTAP__ChannelId__c FROM Account WHERE Id=: StrIdAcc LIMIT 1];
//	}



//    /**
//    * Wrapper for Request
//    * @param   none
//    * @return  none
//    **/
//  	public class WrpPricingMotor{
//		public String Token;
//		public String customerNumber;
//		public String organizacionVentas;
//		public String canalDistribucion;
//		public List<WrpOrderItem> orderItem;

//        //constructor
//		public WrpPricingMotor(String Token, String customerNumber,String organizacionVentas,String canalDistribucion,List<WrpOrderItem> orderItem){
//			this.Token=Token;
//			this.customerNumber=customerNumber;
//			this.organizacionVentas=organizacionVentas;
//			this.canalDistribucion=canalDistribucion;
//			this.orderItem=orderItem;
//		}
//	}



//    /**
//    * Wrapper for Request
//    * @param   none
//    * @return  none
//    **/
//	public class WrpOrderItem {
//		public String materialNumber;
//		public Integer quantity;

//        //constructor
//		public WrpOrderItem(String materialNumber,Integer quantity){
//			this.materialNumber=materialNumber;
//			this.quantity=quantity;
//		}
//	}



//    /**
//    * class for simulate request of test class
//    * @param   none
//    * @return  none
//    **/
//	public class CalloutClass{

//    	public HttpResponse getInfoFromExternalService() {
//        	Http http = new Http();
//        	HttpRequest request = new HttpRequest();
//        	request.setEndpoint('https://abi-pricing-engine-dev.herokuapp.com/obtenPrecio');
//        	request.setMethod('POST');
//        	request.setHeader('Content-Type', 'application/json');
//        	request.setBody('{ "Token" : "4098237329980327982310483277032840982740717239847230473870317093","organizacionVentas" : "3116","orderItem" : [ {"quantity" : 10,"materialNumber" : "000000000003000006"} ],"customerNumber" : "100153987","canalDistribucion" : "1"}');
//        	HttpResponse response = http.send(request);

//        	return response;
//    	}
//	}
}