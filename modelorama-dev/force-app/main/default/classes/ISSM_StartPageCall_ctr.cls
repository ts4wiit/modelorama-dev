/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que gestiona la pagina a mostrar al ingresar a la llamada

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------------------------------------
    1.0       05-Junio-2017   Luis Licona                  Creación de la Clase
  	1.2       04-Septiembre-2017   Hector Diaz             Se agregara funcion para no dejar crear pedido fuera de horario 
    ================================================================================================
    ****************************************************************************************************/
    global class ISSM_StartPageCall_ctr {

    public ONTAP__Order__c[] LstOrder{get;set;} 
    public List<ONTAP__Order__c> OrdersToday_lst{get;set;} 
    public ONCALL__Call__c CallInstance{get;set;} 
    public Boolean BlnHaveOrder{get;set;} 
    public String IdCall{get;set;} 
    public ONTAP__Order__c OrderInstance{get;set;}
    public String[] lstSucc{get;set;}
    public String[] canceled_lst{get;set;}
    public Account[] ObjAcc{get;set;}
    public String[] LstMissedFields{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();
    public ISSM_ValidateRequestedFields_cls CTRVFR = new ISSM_ValidateRequestedFields_cls();
    public ONTAP__Order_Item__c[] LstOItems = new List<ONTAP__Order_Item__c>();
    public ONTAP__Order__c[] LstOrderTom = new List<ONTAP__Order__c>();
    public PermissionSetAssignment[] LstPSA = new List<PermissionSetAssignment>();
    public Boolean FlgUTS{get;set;}

    public ISSM_StartPageCall_ctr(ApexPages.StandardController controller){
        BlnHaveOrder    = false; //Indica que dio clic al boton star call
        FlgUTS          = false; //Universal Telesales Route
        lstSucc         = new List<String>();
        canceled_lst    = new List<String>();
        LstOrder        = new List<ONTAP__Order__c>();
        OrdersToday_lst = new List<ONTAP__Order__c>();
        IdCall          = controller.getId();
        LstMissedFields = new List<String>();      
        CallInstance    = CTRSOQL.getCallbyId(IdCall);
        ObjAcc = CTRSOQL.getAccountListbyId(CallInstance.ONCALL__POC__c);
    }

    /**
    * Realiza el redireccionamiento de la pagina de inicio de la llamada
    * @param  null
    * @return  PageReference: pr indica la pagina que se mostrara al ingresar a la llamada
    **/
    public PageReference startPage(){
        PageReference pr;
        Datetime StrDate    = System.now();
        DateTime dT         = CallInstance.ONCALL__Date__c;
        Date CallDate       = date.newinstance(dT.year(), dT.month(), dT.day());
        DateTime DtDD       = Datetime.parse(StrDate.format());
        Date CurrentDate    = date.newinstance(DtDD.year(), DtDD.month(), DtDD.day());
        Id RecTypeOrder     = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
        Id RecTypeEmergency = CTRSOQL.getRecordTypeId('ONTAP__Order__c','Emergency');

        //si la fecha de llamada es DEL DIA DE HOY
        
        if(CallDate == CurrentDate){
			
			Boolean ValidateCheckOut  = CTRVFR.CheckOutValidate(ObjAcc);
            if(ValidateCheckOut){ 
				LstMissedFields.add(Label.ISSM_CallCheckOut);
			} 
			
            LstOrderTom = CTRSOQL.GetOrderTomorrow(ObjAcc[0].Id, RecTypeOrder, RecTypeEmergency, Label.ISSM_CanceledStatus);
            //Si existe una llamada para mañana, valida si el creador fue Televenta Universal
            if(!LstOrderTom.isEmpty()){
                for(PermissionSetAssignment ObjPSA : CTRSOQL.getPermissionSets(LstOrderTom[0].OwnerId)){
                    if(ObjPSA.PermissionSet.Name == Label.ISSM_PermisionSett)
                        FlgUTS=true;
                }
            }
            //si hay pedido generado por televenta Universal cambia estatus de llamada y muestra alerta con el Name del pedido realizado
            if(FlgUTS && CallInstance.ISSM_CallUT__c && (CallInstance.ISSM_CallResult__c != Label.ISSM_OrderWOSAP)){
                CallInstance.ISSM_CallResult__c = Label.ISSM_CallStatus1;
                update CallInstance;    
                LstMissedFields.add(Label.ISSM_OrderMnsg8+' '+LstOrderTom[0].Name);
            }

            //si no hay llamada ni pedido por televenta Universal, toma flujo normal
            if(!FlgUTS){
                String[] LstMFA = CTRVFR.checkAccountRequiredValues(ObjAcc);
                //si falta algun campo de la cuenta muestra mensajes de los campos faltantes
                if(LstMFA.size() > 0){
                    LstMissedFields.add(Label.ISSM_MissedFieldsA);
                    LstMissedFields.addAll(LstMFA);
                }

                String[] LstMFC = CTRVFR.checkCallList(CallInstance);
                //si falta la lista de llamadas o esta no contiene el Sap User Id Muestra mensaje
                if(LstMFC.size() > 0){
                    LstMissedFields.addAll(LstMFC);
                }

                String[] LstMFR = CTRVFR.checkRoute(CallInstance);
                if(LstMFR.size() > 0){
                    LstMissedFields.add(Label.ISSM_Route);
                    LstMissedFields.addAll(LstMFR);
                }

                OrdersToday_lst = CTRSOQL.getOrdersByAccount(CallInstance.ONCALL__POC__c);
                System.debug(OrdersToday_lst);
                if(OrdersToday_lst.size() > 0){
                    LstMissedFields.clear();
                    for(ONTAP__Order__c currentOrder : OrdersToday_lst){
                        if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Reparto){
                            LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            CallInstance.ISSM_CallResult__c = Label.ISSM_OrderByRep;
                        }
                        if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_MiModelo){
                            LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            CallInstance.ISSM_CallResult__c = Label.ISSM_OrderByMiModelo;
                        }
                        if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Preventa){
                            LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            CallInstance.ISSM_CallResult__c = Label.ISSM_OrderByPresales;
                        }
                        if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Autoventa){
                            LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            CallInstance.ISSM_CallResult__c = Label.ISSM_OrderByAutosales;
                        }
                    }     
                    if(LstMissedFields.size() > 0){
                        update CallInstance;
                    }
                }

                //Valida si existe una orden relacionada a la llamada
                LstOrder = CTRSOQL.getOrderListbyId(IdCall);

                System.debug('LstOrder = ' + LstOrder);

                Integer count_Int = 0;
                for(Integer i = 0; i < LstOrder.size(); i++)
                    count_Int = LstOrder.get(i).ONTAP__OrderStatus__c == Label.ISSM_CanceledStatus ? count_Int + 1 : count_Int;

                Boolean CanceledFlag_Bln = count_Int == LstOrder.size() ? true : false;                
                
                //si no hay una orden actualiza el campo para indicarle que es su primer pedido
                if(((LstOrder.isEmpty() || CanceledFlag_Bln) && CallInstance.ISSM_ValidateOrder__c && LstMissedFields.isEmpty()) || Test.isRunningTest()){
                    OrderInstance = new ONTAP__Order__c();
                    OrderInstance.ONCALL__OnCall_Account__c = CallInstance.ONCALL__POC__c;
                    OrderInstance.ONCALL__Call__c           = IdCall;
                    OrderInstance.ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus2;
                    OrderInstance.ONCALL__Origin__c         = Label.ISSM_OriginCall;
                    OrderInstance.ONTAP__DeliveryDate__c    = Datetime.parse(StrDate.format())+1;
                    OrderInstance.ONTAP__BeginDate__c       = Datetime.parse(StrDate.format());
                    insert OrderInstance;
                    
                    //indica hora de inicio de la llamada
                    CallInstance.ISSM_StartCallTime__c = Datetime.parse(StrDate.format());
                    update CallInstance;

                    BlnHaveOrder=true;
                    pr = new PageReference(Label.ISSM_PageReference1);
                    pr.getParameters().put(Label.ISSM_IdOrder,OrderInstance.Id);
                    pr.setRedirect(true);
                
                //Si existe un pedido
                }else if(!LstOrder.isEmpty() && (!CallInstance.ISSM_ValidateOrder__c || CallInstance.ISSM_ValidateOrder__c)){
                    LstOItems = CTRSOQL.getOrderItemById(LstOrder[0].Id);
                    //si el pedido ya tiene items manda mensaje que ya tiene un pedido generado
                    if(!LstOItems.isEmpty() && !CanceledFlag_Bln){ 
                        lstSucc.add(Label.ISSM_OrderMnsg2);
                    }else if(!LstOItems.isEmpty() && count_Int > 0){
                        canceled_lst.add(Label.ISSM_CanceledOrders + count_Int);
                    //sino elimina el pedido para dejar disponible la llamada
                    }else{
                        delete LstOrder;
                        CallInstance.ISSM_ValidateOrder__c = false;
                        update CallInstance;
                    }
                }
            }
             
 		
        //si la fecha de llamada es diferente al dia de hoy muestra alerta    
        }else{
            LstMissedFields.add(Label.ISSM_DateDifferent);
        }    
    return pr;
    }
}