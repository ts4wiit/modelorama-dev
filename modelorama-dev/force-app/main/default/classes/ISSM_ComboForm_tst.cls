/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Test Class for 'ISSM_ComboForm_ctr'

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    31-Mayo-2018    	Luis Licona                 Creation
******************************************************************************* */
@isTest
private class ISSM_ComboForm_tst {
	
	@isTest static void tstMethod_getselectOptions() {
		Test.startTest();
			String[] LstString  = ISSM_ComboForm_ctr.getselectOptions(new ISSM_Combos__c(),
																	 'ISSM_TypeApplication__c');
			System.assertEquals(!LstString.isEmpty(),true);
		Test.stopTest();
	}
	
	@isTest static void tstMethod_getUserInfo() {
		Test.startTest();
			User[] LstUsr = ISSM_ComboForm_ctr.getUserInfo(UserInfo.getUserId());
			System.assertEquals(LstUsr[0].Id == UserInfo.getUserId(),true);
		Test.stopTest();
	}
}