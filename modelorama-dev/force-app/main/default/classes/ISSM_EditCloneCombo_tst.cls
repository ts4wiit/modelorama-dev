/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Test Class for ISSM_EditCloneCombo_ctr

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    15-Mayo-2018     Luis Licona                 Creation
    ******************************************************************************* */
@isTest
private class ISSM_EditCloneCombo_tst {
	
	@testSetup
	static void setupData(){
		String RecTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        String ISSM_ComboByCustomerMX = Schema.getGlobalDescribe().get('ISSM_ComboByAccount__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboByCustomerMx').getRecordTypeId();

		MDM_Parameter__c MDMPARAM 		= new MDM_Parameter__c();
	        MDMPARAM.Name				= 'MDMPARAMETER';
	        MDMPARAM.Active__c    		= true;
	        MDMPARAM.Active_Revenue__c	= true;
	        MDMPARAM.Code__c 			= '40';
	        MDMPARAM.Catalog__c 		= 'Segmento';
	        MDMPARAM.Description__c		= 'MDMPARAMETER01';
        Insert MDMPARAM;

        MDM_Parameter__c MDMPARAM1 		= new MDM_Parameter__c();
	        MDMPARAM1.Name				= 'MDMPARAMETER02';
	        MDMPARAM1.Active__c    		= true;
	        MDMPARAM1.Active_Revenue__c	= true;
	        MDMPARAM1.Code__c 			= '50';
	        MDMPARAM1.Catalog__c 		= 'GrupoMateriales1';
	        MDMPARAM1.Description__c	= 'MDMPARAMETER02';
        Insert MDMPARAM1;

        ONTAP__Product__c PRODUCT = new ONTAP__Product__c();
	        PRODUCT.ONTAP__ProductId__c 		= '3000005';
	        PRODUCT.ONTAP__MaterialProduct__c 	= '3000005';
	        PRODUCT.ONTAP__ProductShortName__c 	= 'VICTORIA';
	        PRODUCT.ONTAP__ProductType__c 		= 'FERT';
        Insert PRODUCT;

        Account  ACC = new Account();
        ACC.Name 		 = 'Name SalesOffice Parent';
        ACC.RecordTypeId = RecTypeAccountSalesOffice;
        Insert ACC;

        Account  ACCSO = new Account();
        ACCSO.Name 					= 'Name SalesOffice';
        ACCSO.RecordTypeId 			= RecTypeAccountSalesOffice;
        ACCSO.ONTAP__SalesOffId__c 	= 'SO01';
        ACCSO.ParentId 				= ACC.Id;
        Insert ACCSO;

        ISSM_ComboLimit__c CMBLIMIT = new ISSM_ComboLimit__c();
        CMBLIMIT.ISSM_ComboLevel__c 	= 'ISSM_SalesOffice';
		CMBLIMIT.ISSM_SalesStructure__c = ACC.Id;
		CMBLIMIT.ISSM_AllowedCombos__c  = 12;
		CMBLIMIT.ISSM_IsActive__c  		= true;
		Insert CMBLIMIT;

		ISSM_Combos__c COMBO = new ISSM_Combos__c();
        COMBO.ISSM_StartDate__c 		= Date.today() + 2;
        COMBO.ISSM_EndDate__c			= Date.today() + 30;
        COMBO.ISSM_LongDescription__c	= 'Long description for the new combo';
        COMBO.ISSM_ShortDescription__c	= 'ShortDescr';
        COMBO.ISSM_NumberSalesOffice__c	= 10;
        COMBO.ISSM_NumberByCustomer__c	= 1;
        COMBO.ISSM_Currency__c			= 'MXN';
        COMBO.ISSM_ComboLimit__c		= CMBLIMIT.Id;
        COMBO.ISSM_TypeApplication__c	= 'ALLMOBILE;TELESALES;B2B;BDR';
        COMBO.RecordTypeId				= Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        COMBO.ISSM_PriceGrouping__c 	= MDMPARAM1.Code__c;
        Insert COMBO;

		ISSM_ComboByProduct__c CBYPROD = new ISSM_ComboByProduct__c();
		CBYPROD.ISSM_ActiveValue__c		= true;
		CBYPROD.ISSM_QuantityProduct__c = 2;
		CBYPROD.ISSM_UnitPriceTax__c 	= 120;
		CBYPROD.ISSM_Product__c 		= PRODUCT.Id;
		CBYPROD.ISSM_Type__c 			= 'Product';
		CBYPROD.ISSM_ComboNumber__c		= COMBO.Id;
		Insert CBYPROD;

		ISSM_ComboByAccount__c CBYACC = new ISSM_ComboByAccount__c();
        CBYACC.ISSM_SalesOffice__c 	= ACCSO.Id;
        CBYACC.RecordTypeId			= ISSM_ComboByCustomerMX;
        CBYACC.ISSM_ComboNumber__c  = COMBO.Id;
        Insert CBYACC;
		
		User PPMREGNL  = new User();
            PPMREGNL.ProfileId 			= [SELECT Id FROM Profile WHERE Name = 'Price Promotion Manager' LIMIT 1].Id;
            PPMREGNL.LastName 			= 'Price Promotion Manager';
            PPMREGNL.Email 				= 'ppmreg@abi.com';
            PPMREGNL.Username 			= 'ppmreg@abi.com';
            PPMREGNL.CompanyName 		= 'ABI';
            PPMREGNL.Title 				= 'PPM Reg';
            PPMREGNL.Alias 				= 'ppmReg';
            PPMREGNL.TimeZoneSidKey 	= 'America/Los_Angeles';
            PPMREGNL.EmailEncodingKey 	= 'UTF-8';
            PPMREGNL.LanguageLocaleKey 	= 'en_US';
            PPMREGNL.LocaleSidKey 		= 'en_US';
        Insert PPMREGNL;

        User PPMLOCAL  = new User();
            PPMLOCAL.ProfileId 			= [SELECT Id FROM Profile WHERE Name = 'Price Promotion Manager' LIMIT 1].Id;
            PPMLOCAL.LastName 			= 'Price Promotion Manager';
            PPMLOCAL.Email 				= 'ppmlocal@abi.com';
            PPMLOCAL.Username 			= 'ppmlocal@abi.com';
            PPMLOCAL.CompanyName 		= 'ABI';
            PPMLOCAL.Title 				= 'PPM Local';
            PPMLOCAL.Alias 				= 'ppmLocal';
            PPMLOCAL.TimeZoneSidKey 	= 'America/Los_Angeles';
            PPMLOCAL.EmailEncodingKey 	= 'UTF-8';
            PPMLOCAL.LanguageLocaleKey 	= 'en_US';
            PPMLOCAL.LocaleSidKey 		= 'en_US';
            PPMLOCAL.ManagerId 			= PPMREGNL.Id;
        Insert PPMLOCAL;
	}

	@isTest static void test_getLimitRecord() {
		ISSM_ComboLimit__c[] LSTCMBLIMIT = [SELECT Id FROM ISSM_ComboLimit__c LIMIT 1];
		Test.startTest();
			ISSM_EditCloneCombo_ctr.getLimitRecord(LSTCMBLIMIT[0].Id);
		Test.stopTest();
	}
	
	@isTest static void test_getComboByProduct() {
		ISSM_Combos__c[] COMBO = [SELECT Id FROM ISSM_Combos__c LIMIT 1];
		Test.startTest();
			ISSM_MultiSelectLookUp_cls.WrapperClass[] lst =	ISSM_EditCloneCombo_ctr.getComboByProduct(COMBO[0].Id);
		Test.stopTest();
	}

	@isTest static void test_getComboByAccount() {
		ISSM_Combos__c[] COMBO = [SELECT Id FROM ISSM_Combos__c LIMIT 1];
		Test.startTest();
			ISSM_ComboByAccount__c[] lstCBA = ISSM_EditCloneCombo_ctr.getComboByAccount(COMBO[0].Id);
		Test.stopTest();
	}

	@isTest static void test_getPriceGroup() {
		String[] LstStr = new List<String>{'40'};
		Test.startTest();
			MDM_Parameter__c[] lst = ISSM_EditCloneCombo_ctr.getPriceGroup(LstStr);
		Test.stopTest();
	}

	@isTest static void test_upsertComboByProductList() {
		ISSM_ComboByProduct__c[] CBPL = [SELECT Id,ISSM_ComboNumber__c FROM ISSM_ComboByProduct__c];
		Test.startTest();
			ISSM_EditCloneCombo_ctr.upsertComboByProductList(CBPL,CBPL[0].ISSM_ComboNumber__c);
		Test.stopTest();
	}

	@isTest static void test_saveComboByAccountList() {
		ISSM_ComboByAccount__c[] CBPL = [SELECT Id,ISSM_ComboNumber__c FROM ISSM_ComboByAccount__c];
		ISSM_Combos__c[] COMBO = [SELECT Id FROM ISSM_Combos__c];
		Test.startTest();
			ISSM_EditCloneCombo_ctr.saveComboByAccountList(CBPL,CBPL[0].ISSM_ComboNumber__c);
			CBPL = new List<ISSM_ComboByAccount__c>();
			ISSM_EditCloneCombo_ctr.saveComboByAccountList(CBPL,COMBO[0].Id);
		Test.stopTest();
	}

	@isTest static void test_saveCombo() {
		ISSM_ComboLimit__c[] LSTCMBLIMIT = [SELECT Id FROM ISSM_ComboLimit__c LIMIT 1];

		ISSM_Combos__c COMBO = new ISSM_Combos__c();
        COMBO.ISSM_StartDate__c 		= Date.today() + 2;
        COMBO.ISSM_EndDate__c			= Date.today() + 30;
        COMBO.ISSM_LongDescription__c	= 'Long description for the new combo';
        COMBO.ISSM_ShortDescription__c	= 'ShortDescr';
        COMBO.ISSM_NumberSalesOffice__c	= 10;
        COMBO.ISSM_NumberByCustomer__c	= 1;
        COMBO.ISSM_Currency__c			= 'MXN';
        COMBO.ISSM_ComboLimit__c		= LSTCMBLIMIT[0].Id;
        COMBO.ISSM_TypeApplication__c	= 'ALLMOBILE;TELESALES;B2B;BDR';
        COMBO.RecordTypeId				= Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();

		Test.startTest();
			Id StrId1 	= ISSM_EditCloneCombo_ctr.saveCombo(COMBO);
		Test.stopTest();
	}

	@isTest static void test_getStructureforComboByAccount() {
		ISSM_ComboLimit__c[] CMBLIM = [SELECT Id,ISSM_ComboLevel__c,ISSM_SalesStructure__c,ISSM_SalesStructure__r.RecordType.DeveloperName 
									   FROM ISSM_ComboLimit__c LIMIT 1];

		MDM_Parameter__c[] MDMP2 	= [SELECT Id FROM MDM_Parameter__c WHERE Catalog__c =: 'GrupoMateriales1'];
		Test.startTest();
			ISSM_ComboByAccount__c[] lst = ISSM_EditCloneCombo_ctr.getStructureforComboByAccount(CMBLIM[0].Id,MDMP2);		
		Test.stopTest();
	} 

	@isTest static void test_submitForApprovalManager() {
		User PPLOCAL 			= [SELECT Id FROM User WHERE Username = 'ppmlocal@abi.com' LIMIT 1];
        ISSM_Combos__c[] COMBO 	= [SELECT Id FROM ISSM_Combos__c LIMIT 1];
		Test.startTest();
			String processName 	= 'ISSM_ApprovalCombo';
        	ISSM_EditCloneCombo_ctr.submitForApproval(COMBO[0].Id,'Send for approval',processName,PPLOCAL.Id);
		Test.stopTest();
	}

	@isTest static void test_submitForApproval() {
		User PPLOCAL 			= [SELECT Id FROM User WHERE Username = 'ppmreg@abi.com' LIMIT 1];
        ISSM_Combos__c[] COMBO 	= [SELECT Id FROM ISSM_Combos__c LIMIT 1];
		Test.startTest();
			String processName 	= 'ISSM_ApprovalCombo';
        	ISSM_EditCloneCombo_ctr.submitForApproval(COMBO[0].Id,'Send for approval',processName,PPLOCAL.Id);
		Test.stopTest();
	}		
    
 //   @isTest static void test_validateManager() {
	//	User PPLOCAL 			= [SELECT Id FROM User WHERE Username = 'ppmlocal@abi.com' LIMIT 1];
	//	Test.startTest();
 //       System.runAs(PPLOCAL){
 //           ISSM_EditCloneCombo_ctr.validateManager();
 //       }
	//	Test.stopTest();
	//}
	
}