/******************************************************************************* 
* Developed by      :   Everis México
* Author      :   Andrea Cedillo
* Project      :   AbInBev - Modeloramas
* Description    :   Test class for schedule class MDRM_ExecuteSendEmailsFlow_sch
*
* No.         Date               Author                      Description
* 1.0    	  13-June-2019       Andrea Cedillo              Create
*******************************************************************************/
@isTest
private class MDRM_ExecuteSendEmailsFlow_tst{
    static testmethod void testsScheduleIt() {
        Test.startTest();
          MDRM_ExecuteSendEmailsFlow_sch.scheduleIt(); 
        Test.stopTest();
    }
}