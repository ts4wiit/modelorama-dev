/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Interface use tu define the way to sincronice external objects

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public interface ISSM_ObjectInterface_cls {

	/**
	 * Create Objects to sincronice		                             
	 * Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	void createObject(Map<String, Account> mapAccount);

	/**
	 * Create objects to delete		                             
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	void createObjectDelete(Map<String, Account> mapAccount);

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	List<sObject> getList(List<sObject> accountAssetsExt, Map<String, Account> mapAccount);

	/**
	 * Save the result of the sincronization process		                             
	 */
	void save();

	/**
	 * Delete internal records that has the flag isdeleted on true		                             
	 */
	Boolean deleteObjectsFinish();

	/**
	 * Delete external records that has the flag isdeleted on true		                             
	 */
	void deleteObjectsFinishExt();

}