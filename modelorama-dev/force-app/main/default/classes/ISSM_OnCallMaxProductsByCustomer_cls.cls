/****************************************************************************************************
    General Information
    -------------------
    Author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   ABInBev

    Description:
    Controller to asign the maximum to buy by the Customer for a specific SKU
 
    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       08-August-2018  Marco Zúñiga                 Class Creation
    ================================================================================================
****************************************************************************************************/

public with sharing class ISSM_OnCallMaxProductsByCustomer_cls {
	public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();

	public Map<Id,Integer> getMaxBySKUByCustomer(String AccId_Str, List<ONTAP__Product__c> products_lst){
		Map<Id,Integer> maxQtyBySKU_map = new Map<Id,Integer>();
		Id recTypeCounterByCustomerBySKU_Id = Schema.SObjectType.ISSM_CounterByCustomerBySKU__c.getRecordTypeInfosByDeveloperName().get('ISSM_CounterByCustomer').getRecordTypeId();

		List<ISSM_CounterByCustomerBySKU__c> cbcbSKU_lst = CTRSOQL.getMaximumByCustomer(recTypeCounterByCustomerBySKU_Id,
																						AccId_Str,
																						products_lst);

		//System.debug('cbcbSKU_lst = ' + cbcbSKU_lst);

		for(ISSM_CounterByCustomerBySKU__c cbcbSKU_Obj : cbcbSKU_lst){
			maxQtyBySKU_map.put(cbcbSKU_Obj.ISSM_RelatedProduct__r.ISSM_AssociatedProduct__c, Integer.valueOf(cbcbSKU_Obj.ISSM_MaximumQuantity__c));
		}
		//System.debug('maxQtyBySKU_map = ' + maxQtyBySKU_map);
		
		return maxQtyBySKU_map;
	} 
}