global class ISSM_InactivateCallTelecollection_bch implements Database.Batchable<sObject> {
    
    String query;
    String strOrder;
    String strSoqlFilters;
    List<String> lstCampaignName = new List<String>();
    List<Telecollection_Campaign__c> setCampaigns = new List<Telecollection_Campaign__c>();
    String strCampaing;
    String openItemsId;
    Date startDate;
    Date endDate;
    Boolean active;
    String idCampaign;
    String soqlFilters;
    Boolean refreshCampaign;
     
    global ISSM_InactivateCallTelecollection_bch(String strOrderParam) {
        strOrder = strOrderParam;
        
        String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
        query = 'Select Id,ISSM_Active__c,ISSM_CampaingName__c from ONCALL__Call__c where ISSM_Active__c = true and  RecordTypeId=\''+RecordTypeCallId+'\' And ISSM_Country__c =\''+System.label.Country_CallOpenItem+'\'';
        strSoqlFilters = ''; 
    } 


    global ISSM_InactivateCallTelecollection_bch(String strCampaignName, Date startDateParam, Date endDateParam, Boolean activeParam, String idCampaignParam, String soqlFiltersParam, Boolean generateCampaing) {
        startDate = startDateParam;
        endDate = endDateParam;
        active = activeParam;
        strCampaing = strCampaignName;
        idCampaign = idCampaignParam;
        soqlFilters = soqlFiltersParam;
        refreshCampaign = generateCampaing;
        String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
        query = 'Select Id,ISSM_Active__c,ISSM_CampaingName__c,ONCALL__Call_Status__c from ONCALL__Call__c where ISSM_Active__c = true and RecordTypeId=\''+RecordTypeCallId+'\' and Telecollection_Campaign__c =\''+idCampaign+'\'';
        strSoqlFilters = ''; 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('##### Result Query : '+scope);
        List<ONCALL__Call__c> callToDelete = new List<ONCALL__Call__c>();
        List<ONCALL__Call__c> callToUpdate = new List<ONCALL__Call__c>();
        for(ONCALL__Call__c callUnAct : (List<ONCALL__Call__c>)scope){
            
            if(callUnAct.ONCALL__Call_Status__c == 'Incomplete' && refreshCampaign){
                callToDelete.add(callUnAct);
            } else{
                callUnAct.ISSM_Active__c = false;
                callToUpdate.add(callUnAct);
            }
        }
        if(!callToDelete.isEmpty()){
            System.debug('CALL *** TO DELETE : '+callToDelete);
            delete callToDelete;
        }
        if(!callToUpdate.isEmpty()){
            System.debug('CALL *** UPDATE : '+callToUpdate);
            update callToUpdate;
        }
    } 
    
    global void finish(Database.BatchableContext BC) {
        System.debug('##### ENTRO A GENERAR LA CAMPAÑA DE NUEVO');
        if(refreshCampaign){        
            ISSM_LaunchCampaingTelecolletion_bch BtchProces = new ISSM_LaunchCampaingTelecolletion_bch(strCampaing, startDate, endDate, active, idCampaign, soqlFilters); 
            database.executeBatch(BtchProces,150);
        }
        System.debug('##### SALIO DE GENERAR LA CAMPAÑA DE NUEVO');
    }
}