/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface Comparable and sort orders

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OrderWrapper_cls implements Comparable{

	public ONTAP__Order__c order;

    /**
     * Class constructor                                     
     * @param  ONTAP__Order__c orderParam
     */
	public ISSM_OrderWrapper_cls(ONTAP__Order__c orderParam) {
        order = orderParam;
    }

    /**
     * Compare two orders by ONTAP__BeginDate__c                                   
     * @param  Object compareTo
     */
	public Integer compareTo(Object compareTo) {
        ISSM_OrderWrapper_cls compareToOrder = (ISSM_OrderWrapper_cls)compareTo;
        Integer returnValue = 0;
        if (order.ONTAP__BeginDate__c < compareToOrder.order.ONTAP__BeginDate__c) {
            returnValue = 1;
        } else if (order.ONTAP__BeginDate__c > compareToOrder.order.ONTAP__BeginDate__c) {
            returnValue = -1;
        }
        return returnValue;      
	}

}