/**************************************************************************************
Nombre de la clase apex: ISSM_SurveyInvitation_thr
Versión : 1.0
Fecha de Creación : 06 Septiembre 2018
Funcionalidad : Clase que procesa el trigger ISSM_SurveyInvitation_tgr
Clase de Prueba: ISSM_SurveyInvitation_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega     06 - Septiembre - 2018      Versión Original
*************************************************************************************/
public class ISSM_SurveyInvitation_thr {
    public static void setBoolean(List<SurveyInvitation> dataSurveyInv_lst) {
        
        List<SurveyInvitation> surveyInv_lst = new List<SurveyInvitation>();
        List<SurveyInvitation> surveyInvUpd_lst = new List<SurveyInvitation>();
        surveyInv_lst = [SELECT Id, ISSM_LigaEncuesta__c, InvitationLink FROM SurveyInvitation WHERE Id =: dataSurveyInv_lst[0].Id];
        
        if (surveyInv_lst.size() > 0) {
            for (SurveyInvitation reg : surveyInv_lst) {
                reg.OptionsAllowGuestUserResponse = true;
                reg.ISSM_LigaEncuesta__c = reg.InvitationLink;
                surveyInvUpd_lst.add(reg);
            }
        }
        
        if (surveyInvUpd_lst.size() > 0) {
            if (!Test.isRunningTest()) { update surveyInvUpd_lst; }
        }
    }
}