@istest
public class MDRM_AssistanceListctr_tst {
    static testmethod void testlist(){
        date somedate = date.parse('05/11/2018');
        datetime startdate = DateTime.parse('05/11/2018 10:00 AM');
        datetime enddate = DateTime.parse('05/11/2018 11:00 AM');
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user theuser = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='Testing',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        insert theuser;
        
        account acc = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        insert acc;
        
        contact uencontact = new contact(lastname = 'Testing', firstname = 'thistest', email = 'testing9922@test.com', account = acc);
        insert uencontact;
        
        account assistant = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        insert assistant;
         mdrm_informative_session__c session = new mdrm_informative_session__c(MDRM_UEN_Contact__c = uencontact.id,  MDRM_Start__c = startdate,
                                                                             MDRM_end__c = enddate, mdrm_date__c = somedate); 
        insert session;
        event ev = new event(whatid = assistant.id, MDRM_UENContact__c = uencontact.id, Subject ='visit', StartDateTime = startdate,
                            EndDateTime =enddate, ActivityDate = somedate, mdrm_informative_session__C = session.id);
   
        test.startTest();
        MDRM_AssitanceList_ctr controlador = new MDRM_AssitanceList_ctr(new ApexPages.StandardController(session));
        test.stopTest();
    }
}