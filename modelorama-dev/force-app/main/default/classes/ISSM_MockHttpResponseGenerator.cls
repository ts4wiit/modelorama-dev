/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Mock class for simulate response of service pricing motor

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       20/06/2017      Luis Licona                  CLass created
    2.0       18/07/2018      Hector Diaz                  Class Modified Cancel Order ISSM_EndPointCancelOrder
    ================================================================================================
****************************************************************************************************/
@isTest
global class ISSM_MockHttpResponseGenerator implements HttpCalloutMock {
    

    /**
    * simulate response of service pricing motor
    * @param   req: contain Request of service  
    * @return  HTTPResponse: response of service simulate
    **/
    global HTTPResponse respond(HTTPRequest req) {
        
        String StrBody = req.getEndpoint() == Label.ISSM_EndPointCancelOrder ? Label.ISMM_responseCancelOrder :
        				(req.getEndpoint() == Label.ISSM_EPPricingEngine ? Label.ISSM_ResponsePricingEngine+Label.ISSM_ResponsePricingEngine2 : 
        				(req.getEndpoint() == 'http://A' ? Label.ISSM_ResponsePricingEngineA1+Label.ISSM_ResponsePricingEngineA2 :
                        (req.getEndpoint() == 'http://B' ? Label.ISSM_ResponsePricingEngineB1+Label.ISSM_ResponsePricingEngineB2 :
                        (req.getEndpoint() == Label.ISSM_EPMaterialAvailable ? Label.ISSM_ResponseMaterialAvailable : 
                        (req.getEndpoint() == Label.ISSM_EPAuthSuggestedProds ? Label.ISSM_ResponseAuthSuggested : 
                        (req.getEndpoint() == Label.ISSM_EPsuggestedProducts ? Label.ISSM_ResponseSuggestedProducts : 
                        (req.getEndpoint() == Label.ISSM_EndPointDeals ? Label.ISSM_EndPointDeals : Label.ISSM_ResponseCreateOrder)))))));
        // Create a test response
        System.debug('StrBody-----> '+StrBody);
        HttpResponse res = new HttpResponse();
        res.setHeader(Label.ISSM_ContentType, Label.ISSM_HeaderContentTypeTst);
        res.setBody(StrBody);
        res.setStatusCode(200);
        return res;
    }
}