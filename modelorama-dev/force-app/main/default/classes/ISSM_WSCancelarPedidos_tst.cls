/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_WSCancelarPedidos_tst
Versión : 1.0
Fecha de Creación : 21 Mayo 2018
Funcionalidad : Clase de prueba para la clase ISSM_WSCancelarPedidos
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        21 - Mayo - 2018      Versión Original
  Hector Diaz            18 - Julio - 2018      Modificacion de clase
*************************************************************************************/
@isTest
public class ISSM_WSCancelarPedidos_tst {

    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
     static ONTAP__Order_Item__c ObjOI{get;set;}
     static ONTAP__Product__c ObjProduct{get;set;}
    static testmethod void processWebService() {
        // VARIABLES
        Id RT_Emergency_id;
        Id RT_GPOrder_id;
        List<RecordType> RecordType_lst = new List<RecordType>();

        Profile profile = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam']; 
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
        insert user;

        // Obtenemos el Record Type "Emergency"
      
        Id stExam = Schema.getGlobalDescribe().get('ONTAP__Order__c').getDescribe().getRecordTypeInfosByDeveloperName().get('Emergency').getRecordTypeId();
        
        RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
      
       
        

        // Obtenemos el Record Type "GP_Order"
       Id stExam1 = Schema.getGlobalDescribe().get('ONTAP__Order__c').getDescribe().getRecordTypeInfosByDeveloperName().get('GP_Order').getRecordTypeId();
        RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'GP_Order' LIMIT 1];
        for (RecordType rt : RecordType_lst) { RT_GPOrder_id = rt.Id; }

       Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;  
        
        ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '123456';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'Open';
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONCALL__Order_Created_Date_Time__c = System.today()-1;
        orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
       orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergency2;


        ISSM_PriceEngineConfigWS__c config = new ISSM_PriceEngineConfigWS__c();
        config.Name = 'ConfigWSCancelOrders';
        config.ISSM_EndPoint__c = 'http://mx-issm-v360-order-api-service.cloudhub.io/api/client/order';
        config.ISSM_Method__c = 'PUT';
        config.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8';
        config.ISSM_Username__c = 'test';
        config.ISSM_Password__c = 'test';
        insert config;
        
        
        
        
      
      	
      /*  controller.Orders_lst=New List <ONTAP__Order__c>();
        controller.Orders_lst.add(orderEmergency);
        controller.Orders_lst.add(orderGPOrder1);
        */
        
        test.startTest();
			
			ApexPages.StandardController sc = new ApexPages.standardController(orderEmergency2);
      		ISSM_WSCancelaPedidos controller = new ISSM_WSCancelaPedidos(sc);
      	
          
          
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            	Test.setCurrentPage(pageRef);
				PageReference pageRefInicializar = controller.inicializar();
           // ApexPages.StandardController sc = new ApexPages.standardController(user);
           // ISSM_WSCancelaPedidos controller = new ISSM_WSCancelaPedidos(sc);
        

           
        
        test.stopTest();
    }
    
     static testmethod void processWebService2() {
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
     	  
     	Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;  
        
     	ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '123456';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'In progress';
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONCALL__Order_Created_Date_Time__c = System.today()-1;
        orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
       orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergency2;
          Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecTypeProd = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        
        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'AGUA 1.5 L',
            ONCALL__Material_Number__c  = '3000005',
            ONCALL__Material_Name__c    = 'AGUA 1.5 L',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = false,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '02'
            /*ISSM_Sector__c              = Label.ISSM_Water*/
        );
        insert ObjProduct;
        
		ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = orderEmergency2.Id,
            ONCALL__OnCall_Product__c        = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = true,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000009',
            ISSM_EmptyMaterial__c            = '2000005',
            ISSM_EmptyRack__c                = '4000005',
            ISSM_OrderItemSKU__c             = '4000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10,
            ISSM_IdOrderSAP__c 				= '1234567789'
        );
     	
     	
     	 test.startTest();
			

      		
      		ApexPages.StandardController sc2 = new ApexPages.standardController(orderEmergency2);
      		ISSM_WSCancelaPedidos controller2 = new ISSM_WSCancelaPedidos(sc2);
          
          
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            Test.setCurrentPage(pageRef);
			PageReference pageRefInicializar = controller2.inicializar();
	
     
           
        
        test.stopTest();
     	
     	
     }
     	
     	@isTest 
        static  void processWebService22() {


     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
         Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
     	
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

     	ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '1234567';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'In progress'; //'Open';
        orderEmergency2.ONCALL__Origin__c= Label.ISSM_OriginCall;
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONTAP__DeliveryDate__c= System.today()+1;
      	orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
        orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        
        insert orderEmergency2;
        
        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecTypeProd = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        
        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'AGUA 1.5 L',
            ONCALL__Material_Number__c  = '3000005',
            ONCALL__Material_Name__c    = 'AGUA 1.5 L',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = false,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '02'
            /*ISSM_Sector__c              = Label.ISSM_Water*/
        );
        insert ObjProduct;
        
		ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = orderEmergency2.Id,
            ONCALL__OnCall_Product__c        = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = true,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000009',
            ISSM_EmptyMaterial__c            = '2000005',
            ISSM_EmptyRack__c                = '4000005',
            ISSM_OrderItemSKU__c             = '4000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10,
            ISSM_IdOrderSAP__c 				= '1234567789'
        );
        insert ObjOI;
     	
     	 test.startTest();
			

      		ApexPages.StandardController sc2 = new ApexPages.standardController(orderEmergency2);

      		ISSM_WSCancelaPedidos.WprSaleOrder ho=new ISSM_WSCancelaPedidos.WprSaleOrder('ejemplo1','ejemplo2');
      		ISSM_WSCancelaPedidos.WprSaleOrderSAP ho2=new ISSM_WSCancelaPedidos.WprSaleOrderSAP(ho,ho);
      		
      		//ISSM_WSCancelaPedidos controller2 = new ISSM_WSCancelaPedidos(sc2);
      		//controller2.WprSaleOrder obkWpr= new 	controller2.WprSaleOrder('t','t');
        test.stopTest();
     	
     	
     }
     
     @isTest 
        static  void StatusOrderTest() {
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;

        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116', 
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

     	ONTAP__Order__c orderEmergencyStatus = new ONTAP__Order__c();
        orderEmergencyStatus.ONCALL__SAP_Order_Number__c = '1234568';
        orderEmergencyStatus.ISSM_IdEmptyBalance__c = '67890';
        orderEmergencyStatus.ONTAP__TotalAmount__c = 10;
        orderEmergencyStatus.ONTAP__OrderStatus__c = 'Open';
        orderEmergencyStatus.RecordTypeId = RT_Emergency_id;
        orderEmergencyStatus.ONCALL__Order_Created_Date_Time__c = System.today();
        orderEmergencyStatus.ONTAP__BeginDate__c = System.today();
        orderEmergencyStatus.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergencyStatus;
        
     	
     	
     	 test.startTest();
			
			ApexPages.StandardController orderStatus = new ApexPages.standardController(orderEmergencyStatus);
      		ISSM_WSCancelaPedidos controllerstatus = new ISSM_WSCancelaPedidos(orderStatus);
          
          
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            Test.setCurrentPage(pageRef);
			PageReference pageRefInicializar = controllerstatus.inicializar();
	
        test.stopTest();
     	
     	
     }
     
          
        @isTest  static  void StatusCancelOrderTest() {
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
     	
     	 Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '1234567';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'Open';
        orderEmergency2.ONCALL__Origin__c= Label.ISSM_OriginCall;
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONTAP__DeliveryDate__c= System.today()+1;
        orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
        orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergency2;

     	 test.startTest();
			
			ApexPages.StandardController orderStatus = new ApexPages.standardController(orderEmergency2);
      		ISSM_WSCancelaPedidos controllerstatus = new ISSM_WSCancelaPedidos(orderStatus);
      		
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            Test.setCurrentPage(pageRef);
			PageReference pageRefInicializar = controllerstatus.inicializar();
	
        test.stopTest();
     	
     	
     }
     
      @isTest  static  void StatusClosedOrderTest() {
     	
    
     	   insert new ISSM_PriceEngineConfigWS__c(
			Name = 'ConfigWSCancelOrders' ,
			ISSM_EndPoint__c = 'https://mx-issm-v360-order-api-service.cloudhub.io/api/client/order',
			ISSM_HeaderContentType__c = 'application/json; charset=UTF-8',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = 'v360rderAcc2017',
			ISSM_Username__c = 'Mx0rderMgDev'
			); 
			
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
     	  
        
         Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '1234567';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'Open';
        orderEmergency2.ONCALL__Origin__c= Label.ISSM_OriginCall;
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONTAP__DeliveryDate__c= System.today()+1;
        orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
        orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergency2;
        
     	
     	
     	 test.startTest();		
			
			ApexPages.StandardController orderStatusClose = new ApexPages.standardController(orderEmergency2);
      		ISSM_WSCancelaPedidos controllerstatus = new ISSM_WSCancelaPedidos(orderStatusClose);
      		
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            Test.setCurrentPage(pageRef);
			PageReference pageRefInicializar = controllerstatus.inicializar();
			
		
	
        test.stopTest();
     	
     	
     }
       
		@isTest  static  void CloseOrderHelperTest() {
			
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
     	
         Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        Account ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1',
            ISSM_DistributionCenterLU__c = null
        );
        insert ObjAccount;

        ONCALL__Call__c ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ONTAP__Order__c orderEmergency2 = new ONTAP__Order__c();
        orderEmergency2.ONCALL__SAP_Order_Number__c = '1234567';
        orderEmergency2.ISSM_IdEmptyBalance__c = '67890';
        orderEmergency2.ONTAP__TotalAmount__c = 10;
        orderEmergency2.ONTAP__OrderStatus__c = 'Open';
        orderEmergency2.ONCALL__Origin__c= Label.ISSM_OriginCall;
        orderEmergency2.RecordTypeId = RT_Emergency_id;
        orderEmergency2.ONTAP__DeliveryDate__c= System.today()+1;
        orderEmergency2.ONTAP__BeginDate__c = System.today()-1;
        orderEmergency2.ONCALL__Call__c = ObjCall.Id;
        insert orderEmergency2;

        List<ONTAP__Order__c> lstOntapOrder = new List<ONTAP__Order__c>();
     	
     	 test.startTest();		
			
			ISSM_WS_CancelaPedidos_helper objhelper = new  ISSM_WS_CancelaPedidos_helper();
			ISSM_WS_CancelaPedidos_helper.UpdateStatusOrder(null,'',''); 
			ISSM_WS_CancelaPedidos_helper.UpdateStatusOrder(lstOntapOrder,'',null); 
			
	
        test.stopTest();
     	
     	
     }
       
     
	
}