/**
 * This class serves as helper class for AllMobileCatUserTypeTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileCatUserTypeHelperClass {

    /**
     * This method enqueue a Job to initiate the synchronization of Cat User Type object to Heroku when insert or update.
     *
     * @param lstAllMobileCatUserType   List<AllMobileCatUserType__c>
     * @param strEventTriggerFlagApplication    String
     */
    public static void syncCatUserTypeWithHeroku(List<AllMobileCatUserType__c> lstAllMobileCatUserType, String strEventTriggerFlag) {
        if(lstAllMobileCatUserType != NULL && !lstAllMobileCatUserType.isEmpty()) {
            AllMobileCatUserTypeOperationClass objAllMobileCatUserTypeOperationClass = new AllMobileCatUserTypeOperationClass(lstAllMobileCatUserType, strEventTriggerFlag);
            ID IdEnqueueJob = System.enqueueJob(objAllMobileCatUserTypeOperationClass);
        }
    }

    /**
     * This method validates duplucation in the Name of the Cat User Type object.
     *
     * @param lstAllMobileCatUserType   List<AllMobileCatUserType__c>
     */
    public static void validateCatUserTypeNameDuplication(List<AllMobileCatUserType__c> lstAllMobileCatUserType) {
        List<String> lstStrNameCatUserType = new List<String>();
        for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileCatUserType) {
            lstStrNameCatUserType.add(objAllMobileCatUserType.Name);
        }
        List<AllMobileCatUserType__c> lstAllMobileCatUserTypeExistingName = [SELECT Id, Name FROM AllMobileCatUserType__c WHERE Name =: lstStrNameCatUserType];

        if(lstAllMobileCatUserTypeExistingName != null) {
            for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileCatUserType) {
                for(AllMobileCatUserType__c objAllMobileCatUserTypeExistingName : lstAllMobileCatUserTypeExistingName) {
                    if(objAllMobileCatUserType.Name == objAllMobileCatUserTypeExistingName.Name) {
                        objAllMobileCatUserType.Name.addError(System.Label.AllMobileCatUserTypeDuplicatedRoleIdentifier);
                    }
                }
            }
        }
    }
}