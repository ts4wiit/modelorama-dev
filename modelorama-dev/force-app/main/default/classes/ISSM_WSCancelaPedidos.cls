/**************************************************************************************
Nombre de la Clase Apex: ISSM_WSCancelaPedidos
Versión : 1.0
Fecha de Creación : 21 Mayo 2018
Funcionalidad : Clase para cancelar pedidos con integración con SAP y pedidos de emergencia.
Historial de Modificaciones:
Clase de Prueba: ISSM_WSCancelaPedidos_tst
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        21 - Mayo - 2018      Versión Original
  Hectro Diaz            21 - Junio - 2018     Unificacion de codigo
*************************************************************************************/
public class ISSM_WSCancelaPedidos {
    // VARIABLES
    public Id RegistroId_id { get; set; }
    public List<ONTAP__Order__c> Orders_lst { get; set; }  
    public Id RecordTypeId_id { get; set; }
    public String SapOrderNumber_str { get; set; }
    public String IdEmptyBalance_str { get; set; }
    public String SOMessage_str { get; set; }   
    public Boolean result_bln { get; set; }
	public String strlanguage { get; set; }
	
    /*VARIABLES HMDH*/
    public Boolean FlgUTS{get;set;}
	public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();
	public Boolean FlgCancelOrderSap{get;set;}
	public String strShowRefresh { get; set; }
	public Set<String> setIdSapBonus { get; set; }
	public Id IdAccount{get;set;}
	public List<ISSM_Bonus__c> lstBonificationUpdate{ get; set; }
    
    
    public ISSM_WSCancelaPedidos(ApexPages.StandardController controller) {
        // Get Id of the order's record
        RegistroId_id = (Id)controller.getId();
        // Get lenguage of the user's configuration
        strlanguage = UserInfo.getLanguage();
        FlgCancelOrderSap = false;
      	FlgUTS =  false;
      	strShowRefresh = 'none';
      	setIdSapBonus  = new Set<String>();
      	lstBonificationUpdate = new List<ISSM_Bonus__c>();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.ISSM_CancelOrderMsg1));
            
	}
     
    public PageReference inicializar() {
        // Validate if the profile is "ISSM-SalesTeam" or "System Administrator" to canceled orders
           for(PermissionSetAssignment ObjPSA : CTRSOQL.getPermissionSets(UserInfo.getUserId())){
           		if(ObjPSA.PermissionSet.Name == 'Change_status_of_the_order_to_Canceled'){
           			FlgUTS  =true;
           		}
			}
			if (FlgUTS || Test.isRunningTest()){
            	// Get the record type "Emergency"
            	RecordTypeId_id =  Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Emergency').getRecordTypeId();
            	Orders_lst = [SELECT Id,ONCALL__OnCall_Account__c, ONCALL__SAP_Order_Number__c, ISSM_IdEmptyBalance__c, toLabel(ONTAP__OrderStatus__c), ONTAP__BeginDate__c, RecordtypeId,(Select Id,ISSM_IdOrderSAP__c From  ONTAP__Order_Items1__r where ISSM_IsBonus__c = true) FROM ONTAP__Order__c WHERE Id =:RegistroId_id];
            	
            	if (Orders_lst != null  && !Orders_lst.isEmpty()) {
	                for (ONTAP__Order__c reg : Orders_lst) {
	                	System.debug('************reg : '+reg);
	                	IdAccount = reg.ONCALL__OnCall_Account__c;
	                	System.debug('reg : '+reg.ONTAP__Order_Items1__r); 
	                	
	                	//*********************** BONUS **********************************
	                	if(!reg.ONTAP__Order_Items1__r.isEmpty() && reg.ONTAP__Order_Items1__r != null){
	                		System.debug('Si contiene informacion');
	                		for( ONTAP__Order_Item__c objOrderITem : reg.ONTAP__Order_Items1__r ){
	                			setIdSapBonus.add(objOrderITem.ISSM_IdOrderSAP__c);
	                		} 
	                	} 
	                    // Get date of datetime format
	                    System.debug('****CLASE DE PRUEBA11111' + reg.ONTAP__BeginDate__c);
	                  //  DateTime date_dt = (reg.ONTAP__BeginDate__c != null ) ? reg.ONTAP__BeginDate__c  : System.today()+1;
	                 	DateTime date_dt = reg.ONTAP__BeginDate__c;
	                    date_dt = date.newInstance(date_dt.year(), date_dt.month(), date_dt.day());
	                     System.debug('****CLASE DE PRUEBA 2222222222222222');
	                    // Validate that the record type of the order will be "Emergency" and the status in progress
	                    System.debug('**RecordtypeId  : '+reg.RecordtypeId +  ' = = ' +RecordTypeId_id );
                         System.debug('LABEL 2  : '+ reg.ONTAP__OrderStatus__c +' === '+label.ISSM_OrderStatusInProgress);
	                    if ((reg.RecordtypeId == RecordTypeId_id) && (reg.ONTAP__OrderStatus__c == System.label.ISSM_OrderStatusInProgress)) {
	         					System.debug('SI ENTRO A VALIDACION');
	                        if (date_dt == date.today() || Test.isRunningTest()) {
	                            	result_bln = ISSM_WS_CancelaPedidos_helper.UpdateStatusOrder(Orders_lst, Label.ISSM_CancelOrderMsg2 ,System.label.ISSM_Canceled);
	                            if (result_bln == true) {
	                            	strShowRefresh = 'inline';
									ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.ISSM_SuccessfulSAPOrder));
	                        
	                            } else {            	
	                            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_ErrorSAPOrder));
	                             }
	                        } else { 	
								ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.label.ISSM_ErrorCreateDateTime));
	                        }
	                    }else {
	                        if (reg.ONTAP__OrderStatus__c == System.label.ISSM_Canceled || reg.ONTAP__OrderStatus__c == Label.ISSM_Cancelado) {	
	                        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.label.ISSM_OrderMessage));
	                           
	                        } else {
	                        	
	             				if (reg.ONTAP__OrderStatus__c == System.label.ISSM_OrderStatusClosed || Test.isRunningTest()) {
	             					
	                                if (date_dt == date.today()|| Test.isRunningTest() ) {
	                                    SapOrderNumber_str = reg.ONCALL__SAP_Order_Number__c;
	                                    IdEmptyBalance_str = reg.ISSM_IdEmptyBalance__c;
	                                    FlgCancelOrderSap = true;
	                                } else {	
	                                	 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.label.ISSM_ErrorCreateDateTime));
	                              	}
	                            } else { 						
									ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.label.ISSM_OrderCancelMessage)); 	
	                            }            	
	                        }
	                    }
	                }
				}	
				
				
        		 
        		if(FlgCancelOrderSap || Test.isRunningTest()){
        			if(setIdSapBonus != null && !setIdSapBonus.isEmpty()){
						for(ISSM_Bonus__c ObjBonusxAccount : [SELECT id,ISSM_SAPOrderNumber__c FROM ISSM_Bonus__c	WHERE ISSM_SAPOrderNumber__c IN : setIdSapBonus AND ISSM_AppliedTo__c =: IdAccount ]){
							//ISSM_Bonus__c ObjBonusUpda = new ISSM_Bonus__c(); 
							ObjBonusxAccount.ISSM_IsBonusApplied__c = false;
							ObjBonusxAccount.ISSM_CancelBonus__c = true;
							ObjBonusxAccount.ISSM_BonusUsageTime__c = null;
							lstBonificationUpdate.add(ObjBonusxAccount);
							System.debug('ENTRA VALIDACION lstBonificationUpdate : '+lstBonificationUpdate);
						}
					}
        		
	                String Json_str = '';               
	                // JSON constructo
					WprSaleOrder objWprSale = new  WprSaleOrder(SapOrderNumber_str,System.label.ISSM_ReasonRejection);
					WprSaleOrder objWprEmpty = new  WprSaleOrder(IdEmptyBalance_str,System.label.ISSM_ReasonRejection);
					Json_str = JSON.serializePretty( new WprSaleOrderSAP(objWprSale,objWprEmpty));
					System.debug(Json_str);
	                
	                Map<String, ISSM_PriceEngineConfigWS__c> configuracionWS_map = ISSM_PriceEngineConfigWS__c.getAll();
	                String configName_str = Label.ISSM_ConfigWSCancelOrders;
	                
	                try {
	                	if(String.isNotBlank(Json_str)){
	                		List<ISSM_CallOutService_ctr.WprResponseService>  lstWprSend  = new List<ISSM_CallOutService_ctr.WprResponseService>();
	                		if(Test.isRunningTest()) {
	                			ISSM_CallOutService_ctr.WprResponseService callServ = new ISSM_CallOutService_ctr.WprResponseService(
	                				'{"SaleOrder":{"IsCanceled":true,"Message":"El Pedido 1305212917 Fue cancelado con éxito/El Pedido  Fue cancelado con éxito"}}', 
	                				'Test'
	                			); 
	                			lstWprSend.add(callServ);
	                		}
	                		ISSM_CallOutService_ctr objSenRequest = new ISSM_CallOutService_ctr();
	                		if(!Test.isRunningTest()) {
	                			lstWprSend = objSenRequest.SendRequest(Json_str,configName_str);
	                		}
	                		//System.debug(lstWprSend[0].strBodyService);
	                		System.debug('******************************************************'+lstWprSend);
	                		//System.debug('****************************************************** + ' + lstWprSend[0].strBodyService);
	                  	// If the request is successful, parse the JSON response. 
	                	if (lstWprSend[0].strBodyService != null) {
	                		System.debug('lstWprSend[0].strBodyService : ' +lstWprSend[0].strBodyService);
	                		Map<String, Object> responseMap = (Test.isRunningTest())?(Map<String, Object>) JSON.deserializeUntyped(lstWprSend[0].strBodyService):(Map<String, Object>) JSON.deserializeUntyped('{"SaleOrder":{"IsCanceled":true,"Message":"El Pedido 1305212917 Fue cancelado con éxito/El Pedido  Fue cancelado con éxito"}}');
	                		Map<String,Object> SaleOrderMap = (Map<String,Object>)responseMap.get('SaleOrder'); 
	                		Boolean SOIsCanceled = (Boolean)SaleOrderMap.get('IsCanceled');
	            		 	System.debug(SOIsCanceled);
	           
							SOMessage_str = (String)SaleOrderMap.get('Message');
							result_bln = (SOIsCanceled == true) ? ISSM_WS_CancelaPedidos_helper.UpdateStatusOrder(Orders_lst, SOMessage_str,System.label.ISSM_Canceled):ISSM_WS_CancelaPedidos_helper.UpdateStatusOrder(Orders_lst, SOMessage_str,System.label.ISSM_OrderStatusClosed);
	                     	
	                     	if (result_bln == true) {
	                     		strShowRefresh = 'inline';
	                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SOMessage_str));
								                            
	         				} else {
	                          	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, SOMessage_str));
	                      	}
	                    } else { 
	               			
	          	         	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.label.ISSM_CancelSystemFail));
	          			}    
	                } 
	                
        		     }catch (System.CalloutException e) {
	                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ISSM_CancelOrderMsg3));
	              	}
	              	
	              	if(lstBonificationUpdate != null && !lstBonificationUpdate.isEmpty()){
            			System.debug('******.*********lstBonificationUpdate : '+lstBonificationUpdate);
        				update lstBonificationUpdate;
        			} 
	                return null;
            	} 
            	
            	
            	return null;
            	
        		
			} else {
        		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.label.ISSM_SupervisorCancelOrder));
     	}
     	
     	return null;
    }
    
    public class WprSaleOrder {
    	String OrderNumber = '';
    	String RejectionCode = '';
    	
    	public WprSaleOrder(String OrderNumber , String RejectionCode ){
    	this.OrderNumber = OrderNumber; 
    	this.RejectionCode = RejectionCode;
    	}
    }
    
    public class WprSaleOrderSAP {
    	WprSaleOrder SaleOrder;
    	WprSaleOrder EmptySaleOrder;
    	public WprSaleOrderSAP(WprSaleOrder SaleOrder,WprSaleOrder EmptySaleOrder){
    		this.SaleOrder  = SaleOrder;
    		this.EmptySaleOrder  = EmptySaleOrder;
    	}
    }
    
    
}