/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_InactivateCallTelecollectionBch_tst {
  	public static  List<ISSM_AppSetting_cs__c> AppSetting;
    static testMethod void myUnitTest() {
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
          
        // Cretae configuracion personalizada 
         List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }  
        //Generamos RegionalSalesdivision 
        String RecordTypeRegional = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();        
        Account objCreateRegionalDiv  = new Account();
        objCreateRegionalDiv.Name ='RegionaSales';
        objCreateRegionalDiv.RecordTypeId = RecordTypeRegional;
        insert  objCreateRegionalDiv;
        
        //Generamos SalesOrg
        String RecordTypeSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();        
        Account objTypeSalesOrg  = new Account();
        objTypeSalesOrg.Name ='SalesOrg';
        objTypeSalesOrg.ISSM_ParentAccount__c = objCreateRegionalDiv.Id;
        objTypeSalesOrg.RecordTypeId =RecordTypeSalesOrg;
         insert objTypeSalesOrg;
        
        //Generamos SalesOffice
        String RecordTypeSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        Account ObjTypeSalesOffice  = new Account();
        ObjTypeSalesOffice.Name ='SalesOffcie';
        ObjTypeSalesOffice.ISSM_ParentAccount__c = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.ParentId = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.RecordTypeId = RecordTypeSalesOffice;
        insert ObjTypeSalesOffice;
        
        //Generamos cuenta para los filtros
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();        
        Account objCreateAccount  = new Account();
        objCreateAccount.Name ='EjemploCuenta';
        objCreateAccount.ONCALL__Risk_Category__c = '';
        objCreateAccount.ONTAP__Classification__c ='Botella Abierta';
        objCreateAccount.ISSM_LastContactDate__c = null;
        objCreateAccount.ISSM_LastPaymentPlanDate__c= null;
        objCreateAccount.ISSM_LastPromisePaymentDate__c= null;
        objCreateAccount.ISSM_RegionalSalesDivision__c =objCreateRegionalDiv.Id;
        objCreateAccount.ISSM_SalesOffice__c =ObjTypeSalesOffice.Id;
        objCreateAccount.ISSM_SalesOrg__c =objTypeSalesOrg.Id;
        objCreateAccount.RecordTypeId = RecordTypeAccountId;
        insert objCreateAccount; 
         
        //Generamos campaña  Telecollection_Campaign__c
        Telecollection_Campaign__c objCreateCampaign = new Telecollection_Campaign__c();
        objCreateCampaign.Name = 'EjemploCampaignTest';
        objCreateCampaign.Start_Date__c = System.today();
        objCreateCampaign.End_Date__c =  System.today();
        objCreateCampaign.Active__c = true;
        objCreateCampaign.SoqlFilters__c='';
        insert objCreateCampaign;
        
       //Generamos LLamadas 
       List<ONCALL__Call__c> LstCall = new List<ONCALL__Call__c>();
       String RecordTypeCallTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();        
       ONCALL__Call__c objCall = new ONCALL__Call__c ();
       objCall.ONCALL__POC__c =objCreateAccount.Id;
       objCall.RecordTypeId = RecordTypeCallTelecollection;
       objCall.ISSM_Active__c = true;
       objCall.ISSM_CampaingName__c = 'EjemploCampaignTest';
       objCall.ONCALL__Call_Status__c = 'Incomplete';
       LstCall.add(objCall);
       insert LstCall;
       
          //Generamos LLamadas2 
       List<ONCALL__Call__c> LstCall2 = new List<ONCALL__Call__c>();
       ONCALL__Call__c objCall2 = new ONCALL__Call__c ();
       objCall2.ONCALL__POC__c =objCreateAccount.Id;
       objCall2.RecordTypeId = RecordTypeCallTelecollection;
       objCall2.ISSM_Active__c = true;
       objCall2.ISSM_CampaingName__c = 'EjemploCampaignTest';
       objCall2.ONCALL__Call_Status__c = 'Complete';
       LstCall2.add(objCall2);
       insert LstCall2;
       AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
       Database.BatchableContext dbBC;
       Test.startTest(); 
            ISSM_InactivateCallTelecollection_bch objInactivateCallTelecollection = new ISSM_InactivateCallTelecollection_bch('');
            ISSM_InactivateCallTelecollection_bch objInactivateCallTelecollectionInactive = new ISSM_InactivateCallTelecollection_bch(
            objCreateCampaign.Name,
            objCreateCampaign.Start_Date__c,
            objCreateCampaign.End_Date__c,
            objCreateCampaign.Active__c,
            objCreateCampaign.Id,
            objCreateCampaign.SoqlFilters__c,
            true);
            
            ISSM_InactivateCallTelecollection_bch objInactivateCallTelecollectionInactiveFalse = new ISSM_InactivateCallTelecollection_bch(
            objCreateCampaign.Name,
            objCreateCampaign.Start_Date__c,
            objCreateCampaign.End_Date__c,
            objCreateCampaign.Active__c,
            objCreateCampaign.Id,
            objCreateCampaign.SoqlFilters__c,
            true);
        
            objInactivateCallTelecollectionInactiveFalse.start(dbBC);
            objInactivateCallTelecollectionInactiveFalse.execute(dbBC,LstCall);
            objInactivateCallTelecollectionInactiveFalse.execute(dbBC,LstCall2);
            objInactivateCallTelecollectionInactiveFalse.finish(dbBC);
       Test.stopTest();
        
    }
}