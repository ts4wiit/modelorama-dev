public interface ISSM_IProfileSelector_cls {

	List<Profile> selectById(Set<ID> idSet);
	Profile selectById(ID profileID);
	List<Profile> selectById(String profileID);
}