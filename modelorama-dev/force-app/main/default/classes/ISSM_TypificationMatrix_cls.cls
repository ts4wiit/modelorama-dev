/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public with sharing class ISSM_TypificationMatrix_cls {
	public ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    public ISSM_TypificationMatrix_cls() {
        
    }
    
    public ISSM_TypificationMatrix__c getTypificationMatrix(Id idTypificationMatrix) {
        try { 
            return objCSQuerys.QueryTypificationMatrixLimit(idTypificationMatrix);
        } catch(Exception e) { return null; }
    }
    
    public List<ISSM_TypificationMatrix__c> getTypificationMatrix(String strOptionSelectedLevel1, 
                                                                  String strOptionSelectedLevel2, String strOptionSelectedLevel3, 
                                                                  String strOptionSelectedLevel4, String strOptionSelectedLevel5, 
                                                                  String strOptionSelectedLevel6){
                                                                      return objCSQuerys.QueryTypificationMatrixAllLevels(strOptionSelectedLevel1,strOptionSelectedLevel2,strOptionSelectedLevel3,
                                                                                                                          strOptionSelectedLevel4,strOptionSelectedLevel5,strOptionSelectedLevel6);
                                                                      
                                                                  }
    
    public List<ISSM_TypificationMatrix__c> getTypificationMatrixByChannel(String strOptionSelectedLevel1, String strOptionSelectedLevel2, String channel_str) {
        return objCSQuerys.QueryTypificationMatrixAllLevelsByChannel(strOptionSelectedLevel1, strOptionSelectedLevel2, channel_str);
    }
}