/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that runs at the end or save and exit the process of generation of conditions. 
                        This allows to determine through the parameters sent by which method to enter as well as the 
                        execution of two batch processes to eliminate and create new records

    Information about changes (versions)
    ===============================================================================
    No.    Date             	Author                      Description
    1.0    06-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class TRM_ConditionDetail_ctr {
    /**
    * @description  Method that is executed at the end of the Generation of price conditions process
    * 
    * @param    lstProducts          List of selected products
    * @param    lstConditionScales   List of scales by material selected
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * @param    lstCustomer          List of selected Customers
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * 
    * @return   none
    */
    @AuraEnabled
    public static void mainSaveConditions( ONTAP__Product__c[] lstProducts,
                                           TRM_ProductScales__c[] lstConditionScales, 
                                           Map<String,String> mapStructures,
                                           Map<String,String> mapCatalogs,
                                           Account[] lstCustomer,
                                           TRM_ConditionClass__c condClssRec,
                                           TRM_ConditionRelationships__mdt mtdRelation,
                                           TRM_ConditionsManagement__mdt mtdMngRecord
                                           ){

        //Execute batch for delete Condition Records
        if(condClssRec.TRM_TotalConditionRecords__c > 0){
            TRM_DeleteConditions_bch bchDeleteProcess = new TRM_DeleteConditions_bch(lstProducts,
                                                                                    mapStructures,
                                                                                    mapCatalogs,
                                                                                    lstCustomer,
                                                                                    condClssRec,
                                                                                    mtdRelation,
                                                                                    mtdMngRecord,
                                                                                    lstConditionScales);
            ID batchDeleteProcessId  = Database.executeBatch(bchDeleteProcess);
            System.debug('###batchDeleteProcessId### '+batchDeleteProcessId);
        }else{
            TRM_GenerateConditions_bch bchGenerateProcess = new TRM_GenerateConditions_bch(lstProducts,
                                                                                         mapStructures,
                                                                                         mapCatalogs,
                                                                                         lstCustomer,
                                                                                         condClssRec,
                                                                                         mtdRelation,
                                                                                         mtdMngRecord,
                                                                                         lstConditionScales);
            ID batchGenerateProcessId  = Database.executeBatch(bchGenerateProcess);
            System.debug('###batchGenerateProcessId### '+batchGenerateProcessId);  
        }                
    }
    

    /**
    * @description  Main method to determine the type of condition that is going to be generated
    * 
    * @param    lstProducts          List of selected products
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * @param    lstCustomer          List of selected Customers
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    * 
    * @return  TRM_ConditionRecord__c[] List of generated condition records
    */
    public static TRM_ConditionRecord__c[] generatorOfConditions(ONTAP__Product__c[] lstProducts, 
                                                                Map<String,String> mapStructures,
                                                                Map<String,String> mapCatalogs,
                                                                Account[] lstCustomer,
                                                                TRM_ConditionClass__c condClssRec,
                                                                TRM_ConditionRelationships__mdt mtdRelation,
                                                                TRM_ConditionsManagement__mdt mtdMngRecord){
        
        TRM_GenerateConditions_cls CTR = new TRM_GenerateConditions_cls();
        TRM_ConditionRecord__c[] lstConditionRec = new List<TRM_ConditionRecord__c>();
        String StrGenerateBy = mtdMngRecord.TRM_GenerateBy__c;
        String StrIdCndClss  = condClssRec.Name;
        String[] lstPriZones = (String.isBlank(condClssRec.TRM_StatePerZone__c)) ? null : condClssRec.TRM_StatePerZone__c.split(';');
        String[] lstSalesOrg = (String.isBlank(condClssRec.TRM_SalesOrg__c)) ? null : condClssRec.TRM_SalesOrg__c.split(';');
        String[] lstSalesOff = (String.isBlank(condClssRec.TRM_SalesOffice__c)) ? null : condClssRec.TRM_SalesOffice__c.split(';');
        String[] lstSegments = (String.isBlank(condClssRec.TRM_Segment__c)) ? null : condClssRec.TRM_Segment__c.split(';');

        switch on StrGenerateBy {
            when 'TRM_SearchByCustomer' {
                System.debug('BLOCK SearchByCustomer');
                lstConditionRec = CTR.SearchByCustomer(lstProducts,
                                                       lstCustomer,
                                                       mtdRelation,
                                                       mtdMngRecord,
                                                       condClssRec,
                                                       mapStructures,
                                                       mapCatalogs);
            }
            when 'TRM_SearchByProduct' {
                System.debug('BLOCK SearchByProduct');
                lstConditionRec = CTR.SearchByProduct(lstProducts,
                                                      mtdRelation,
                                                      mtdMngRecord,
                                                      condClssRec,
                                                      mapStructures,
                                                      mapCatalogs); 
            }
            when 'TRM_SearchBySegmentZone' {
                System.debug('BLOCK SearchBySegmentZone');
                lstConditionRec = CTR.SearchBySegmentZone(lstProducts,
                                                          lstSegments,
                                                          lstPriZones,
                                                          condClssRec,
                                                          mtdRelation,
                                                          mtdMngRecord,
                                                          mapStructures,
                                                          mapCatalogs); 
            }
            when 'TRM_SearchBySegmentOffice' {
                System.debug('BLOCK SearchBySegmentOffice');
                lstConditionRec = CTR.SearchBySegmentOffice(lstProducts,
                                                            lstSegments,
                                                            lstSalesOff,
                                                            condClssRec,
                                                            mtdRelation,
                                                            mtdMngRecord,
                                                            mapStructures,
                                                            mapCatalogs);
            }
            when 'TRM_SearchByZone' {
                System.debug('BLOCK SearchByZone');
                lstConditionRec = CTR.SearchByZone(lstProducts,
                                                    lstPriZones,
                                                    condClssRec,
                                                    mtdRelation,
                                                    mtdMngRecord,
                                                    mapStructures,
                                                    mapCatalogs);
            }
            when 'TRM_SearchByOrganization' {
                System.debug('BLOCK SearchByOrganization');
                lstConditionRec = CTR.SearchByOrganization(lstProducts,
                                                           lstSalesOrg,
                                                           condClssRec,
                                                           mtdRelation,
                                                           mtdMngRecord,
                                                           mapStructures,
                                                           mapCatalogs);
            }
            when 'TRM_SearchByOffice' {
                System.debug('BLOCK SearchByOffice');
                lstConditionRec = CTR.SearchByOffice(lstProducts,
                                                      lstSalesOff,
                                                      condClssRec,
                                                      mtdRelation,
                                                      mtdMngRecord,
                                                      mapStructures,
                                                      mapCatalogs);
            }
            when else {
                System.debug('BLOCK default');
            }
        }

        return lstConditionRec;
    }
    @AuraEnabled
    public static String getUserLogged(){
      Id userId  = UserInfo.getUserId();
      return[SELECT ManagerId FROM User WHERE Id =: userId].ManagerId;
    }

}