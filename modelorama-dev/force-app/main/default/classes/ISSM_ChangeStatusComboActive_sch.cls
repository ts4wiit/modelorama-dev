/******************************************************************************* 
* Desarrollado por  :   Avanxo México
* Autor             :   Andrea Cedillo
* Proyecto          :   AbInbev - Trade Revenue Management
* Descripción       :   Schedulable for the execution of work flows (change of status).
*                       Run every day at 12:00:00 a.m.
*
* No.       Fecha              Autor                      Descripción
* 1.0    16-Noviembre-2018      Andrea Cedillo                   Creación
*******************************************************************************/
global class ISSM_ChangeStatusComboActive_sch implements Schedulable {
  /*
   * Run every day at 12:00:00 a.m.
   * 
  */
  @TestVisible
  public static final String CRON_EXPR = Label.TRM_CronExpr;    
  /*
    Call this from Anonymous Apex to schedule at the default regularity
  */
  global static String scheduleIt() {
    ISSM_ChangeStatusComboActive_sch job = new ISSM_ChangeStatusComboActive_sch();
    return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchActiveCmb + System.now()) : Label.TRM_NameSchActiveCmb), CRON_EXPR, job);
  }
   /*
    * Execution of the flow, for the change of status of the combo. Change status:
    *   -ISSM_Actived
  */
  global void execute(SchedulableContext sc) {
    Map<String, Object> params = new Map<String, Object>();
    //Flow for ISSM_Actived
        Flow.Interview.ISSM_ChangeStatusActivo flowComboActivo = new Flow.Interview.ISSM_ChangeStatusActivo(params);
        if(!Test.isRunningTest()){flowComboActivo.start();}
  }
}