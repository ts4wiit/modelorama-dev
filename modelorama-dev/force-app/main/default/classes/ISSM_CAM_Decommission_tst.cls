@IsTest
public class ISSM_CAM_Decommission_tst {
    @testSetup static void setup() {
        ISSM_PriceEngineConfigWS__c wsEstatus = new ISSM_PriceEngineConfigWS__c(
        Name='ConfigCAMWSEstatus', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/estado',
        ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsEstatus;
        id recTypeCAMDecommission = [SELECT id  FROM Recordtype WHERE SobjectType = 'ONTAP__Case_Force__c' 
                                     AND DeveloperName  = 'CAM_Asset_Decommission'].id;        
        
        user usrapp1 = new User (username='omar.cruz@gmodelo.com.mx.test', lastname='approval1',email='omar.cruz@gmodelo.com.mx', Alias='app1', 
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp1;
        ISSM_TypificationMatrix__c TypMat = new ISSM_TypificationMatrix__c(ISSM_UniqueIdentifier__c='TEST', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Baja');
        insert TypMat;
        ISSM_CAM_Approvers__c camapprovers = 
            new ISSM_CAM_Approvers__c (Name='CAM_Asset_Decommission', Approver1__C='omar.cruz@gmodelo.com.mx.test', Approver2__C='omar.cruz@gmodelo.com.mx.test',
                                       Approver3__C='omar.cruz@gmodelo.com.mx.test', Approver4__C='omar.cruz@gmodelo.com.mx.test');
        insert camapprovers;
        ONTAP__Case_Force__c cf_sol =  
            new ONTAP__Case_Force__c(ontap__subject__C='Decommission', ONTAP__Description__c='Description decommission', ONTAP__Status__c='Open', 
                                     ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Baja', recordtypeid=recTypeCAMDecommission );
        insert cf_sol;
        
        ISSM_Asset__c asset = 
            new ISSM_Asset__c(name='Test Asset', ISSM_Serial_Number__c='ESUNASERIEDEPRUEBA', Equipment_Number__c='ESUNASERIEDEPRUEBA', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Validation');
        insert asset;
    }
    
    static testmethod void runTestCAMDecommission(){
        List<ISSM_Case_Force_Asset__c> tstlstbaja = new List<ISSM_Case_Force_Asset__c>();
        ONTAP__Case_Force__c cf_decomm = [
            SELECT id FROM ONTAP__Case_Force__c WHERE ontap__subject__C='Decommission'];
        try{
            ApexPages.StandardController sc = new ApexPages.StandardController(cf_decomm);
            ISSM_CAM_Decommission_ctr tstcamdecomm = new ISSM_CAM_Decommission_ctr(sc);
           // tstlstbaja = tstcamdecomm.getlstDecommission();
            tstcamdecomm.ReadFile(); 
            tstcamdecomm.contentFile=Blob.valueOf('ESUNASERIEDEPRUEBA');
            tstcamdecomm.ReadFile();
           // tstlstbaja = tstcamdecomm.getlstDecommission();
          //  system.assert(tstlstbaja.size()>0,'La lista no contiene elementos');       
        }catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }       
    }
}