/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
global without sharing class ISSM_TriggerManager_cls {
    private static Set<String> inactivatedTriggers = new Set<String>();

    public static boolean isInactive(String triggername){
    System.debug('inactivatedTriggers>>>>>'+inactivatedTriggers);
        if(inactivatedTriggers.contains(triggername)){
            return true;
        }
        else{
            return false;
        }
    }

    public static void inactivate(String triggername) {
        inactivatedTriggers.add(triggername);
    }
    
    public static void Activate() {
        inactivatedTriggers= new Set<String>();
    }
  
    /*static testMethod void TriggerManagerTest(){
        System.assertEquals(TriggerManager.isInactive('test'),false);
        TriggerManager.inactivate('test');
        System.assertEquals(TriggerManager.isInactive('test'),true);
    } */
}