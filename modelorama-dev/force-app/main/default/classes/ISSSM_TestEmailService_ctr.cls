/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  Clase que es usada por EMAIL SERVICE
Comentarios ":
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 12-Julio-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
global with sharing class ISSSM_TestEmailService_ctr implements Messaging.InboundEmailHandler {
    public String NumberCaseSubject {get;set;}
    public Case CaseEdit {get;set;}
    public Boolean blnCreateCaseComment {get;set;}
    public ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {      
        blnCreateCaseComment = false;
        Database.SaveResult[] srListInsertCaseCo=null;
        Attachment attachment = new Attachment();
        List<String> listaValuesSubject = new List<String>();
        List<String> listaValuesBody = new List<String>();
        List<Attachment> lstattachment = new List<Attachment>();
    
        //ASUNTO DEL CORREO 
        if(String.isNotBlank(email.subject)){
            for(String  valuesSubject  :email.subject.split(' ')){
            listaValuesSubject.add(valuesSubject);
            }
        }
        // VALIDADMOS LOS DATOS DE LA LISTA listaValuesSubject
        if(!listaValuesSubject.isEmpty()){
            for(String valuesSubject :listaValuesSubject ){
                if(valuesSubject.isNumeric()){
                    NumberCaseSubject =  valuesSubject;
                }
            }  
        }
        //SI EXISTE EL NUMERO DE CASO LO CONSULTA DE LO CONTRARIO MANDA MENSAJE EN LA CONSOLA
        if(String.isNotBlank(NumberCaseSubject)){
            if(NumberCaseSubject.length() == 8){
                CaseEdit =   objCSQuerys.QueryCaseCaseNumber(NumberCaseSubject);
                blnCreateCaseComment = true;
            } 
        }else{
            System.debug('##### NO SE PUEDE CONSULTAR EL CASO YA QUE EL ASUNTO NO CONTIENE  EL NUMERO DE CASO');
        }
 
        //CUERPO DE CORREO QUITAMOS SALTOS DE LINEA Y GUARDAMOS EN 
        if(String.isNotBlank(email.plainTextBody)){
            for(String  ValuesBody  :email.plainTextBody.split('\n')){
                listaValuesBody.add(ValuesBody);
            }   
        }
         
        if(String.isNotBlank(email.plainTextBody) && blnCreateCaseComment){
            List <CaseComment> lstCaseComentInsert = new List <CaseComment>();
            CaseComment ObjCaseComment = new CaseComment();
             Boolean FlagEscribio = false;
                         
            //***** VALOR PARA REENVIO  *****//
            Integer  fincadenaReenvio =  email.plainTextBody.indexOf(System.label.ISSM_EmailServiceForwarded);   
            if(fincadenaReenvio >=1){
                FlagEscribio = true;
                ObjCaseComment.CommentBody = email.plainTextBody.substring(0,fincadenaReenvio);
            }else{
                 ObjCaseComment.CommentBody = email.plainTextBody;
            }
            //***** VALOR PARA ESCRIBIO  *****//
            if(FlagEscribio == false){
                Integer fincadenaEscribio =  email.plainTextBody.indexOf(System.label.ISSM_EmailServiceResponse);   
                if(fincadenaEscribio >=1){
                    ObjCaseComment.CommentBody = email.plainTextBody.substring(0,fincadenaEscribio);
                }else{
                    ObjCaseComment.CommentBody = email.plainTextBody;
                }
            }
            
            
            ObjCaseComment.ParentId     =  CaseEdit.Id;
            lstCaseComentInsert.add(ObjCaseComment);
            //Insertamos el Case Comment
          	srListInsertCaseCo = Database.insert(lstCaseComentInsert, false);
            
	      	for (Database.SaveResult sr : srListInsertCaseCo) {
	      	//Actualizamos el STATUS DEL CASO  a EmailResponse
		      	if(sr.isSuccess()){
		      		    /*Case ObjCase = new Case();
		            	ObjCase.id = CaseEdit.Id;*/
		                
		             	if(CaseEdit.status !=System.label.ISSM_StatusCaseEmailService){
                            CaseEdit.Status = System.label.ISSM_StatusCaseEmailService;
                            CaseEdit.ISSM_ProviderCloseCase__c  = System.now();
                            ISSM_Debug_cls.debug(ISSSM_TestEmailService_ctr.class.getName(), 'handleInboundEmail', '96', CaseEdit.ISSM_ProviderCloseCase__c + '');
		             		update CaseEdit;
		               }
		               	
		  		}else{
		         	System.debug('#### NO SE PUEDE REALIZAR EL UPDATE PARA STATUS YA QUE HUBO PROBLEMAS AL NSERTAR EL CASECOMMENT : '+sr.getErrors());  
		      	}
	 		}
        }else{
            /*POdemos meter el reenvio de correo para responder*/ 
            System.debug('##### PARA PODER SOLUCIONAR EL PROBLEMA ES NECESARIO QUE EL ASUNTO DEL CORREO CONTENGA EL  *CASE - NUMBER* O QUE EL *CASE NUMBER* INGRESADO ESTE INCORRECTO'+listaValuesBody + '--- '+blnCreateCaseComment );
        }           
        //ARCHIVO ADJUNTO 
        
        if(String.isNotBlank(String.valueOf(email.binaryAttachments))){
            for(Messaging.InboundEmail.BinaryAttachment BodyAttachment : email.binaryAttachments){  
                attachment.Name = BodyAttachment.fileName;
                attachment.Body = BodyAttachment.body;
                attachment.ParentId = CaseEdit.Id;
                lstattachment.add(attachment);
                attachment = new Attachment();//Realiza el limpiado y evita que mande error de duplicidad 
            } 
        }else{
            System.debug('##### NO SE CUENTA CON ARCHIVOS ADJUNTOS EN EL CORREO');
        }
        if(!lstattachment.isEmpty()){
            insert lstattachment;
        }
    
        return null; 
    }
        
}