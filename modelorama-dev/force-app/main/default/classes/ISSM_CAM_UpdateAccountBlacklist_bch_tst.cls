@IsTest
public class ISSM_CAM_UpdateAccountBlacklist_bch_tst {
    @testSetup
    static void setup(){
        
        List<Account> accs = new List<Account>();
        List<ONTAP__KPI__C> kpis = new List<ONTAP__KPI__c>();
        
        Id recTypeAccount = [SELECT id FROM Recordtype WHERE SobjectType ='Account' AND DeveloperName = 'Account' LIMIT 1].Id;
        
        for (integer i=0; i<5; i++) accs.add(new Account(Name='Accounti'+i, recordtypeid=recTypeAccount, ISSM_Is_Blacklisted__c=False, ISSM_AccountToDel__c=False ));
        for (integer j=0; j<5; j++) accs.add(new Account(Name='Accountj'+j, recordtypeid=recTypeAccount, ISSM_Is_Blacklisted__c=True,  ISSM_AccountToDel__c=True ));
        insert accs;
        
        for (Account acc: [SELECT id FROM Account WHERE ISSM_Is_Blacklisted__c=True])
            kpis.add(new ONTAP__KPI__c(ONTAP__Kpi_id__c = 1, ONTAP__Actual__c=500, ontap__kpi_name__c = 'Cerveza', ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY()-32, ONTAP__Account_id__c = acc.id));
        insert kpis;
    }
    
    static testMethod void runTestUpdateAccountBlacklist(){
        
        Test.startTest();
        system.debug('BEFORE ASSERT BEFORE BATCH');
        system.debug([SELECT id, ISSM_Is_Blacklisted__c, ISSM_AccountToDel__c FROM Account]);
        for(Account acc:[SELECT id, ISSM_Is_Blacklisted__c, ISSM_AccountToDel__c FROM Account]) 
            system.assert(acc.ISSM_Is_Blacklisted__c == acc.ISSM_AccountToDel__c,'UpdateAccountBlacklist: Error en los datos iniciales');
        
        ISSM_CAM_UpdateAccountBlacklist_bch bl = new ISSM_CAM_UpdateAccountBlacklist_bch();
        Id batchId = Database.executeBatch(bl);
        Test.stopTest();
        
        system.debug('BEFORE ASSERT AFTER BATCH');
        system.debug([SELECT id, ISSM_Is_Blacklisted__c, ISSM_AccountToDel__c FROM Account]);
        for(Account acc:[SELECT id, ISSM_Is_Blacklisted__c, ISSM_AccountToDel__c FROM Account ]) 
            system.assert(acc.ISSM_Is_Blacklisted__c != acc.ISSM_AccountToDel__c,'UpdateAccountBlacklist: Error después del Batch!');
        system.debug('AFTER ASSERT AFTER BATCH');
        
    }
}