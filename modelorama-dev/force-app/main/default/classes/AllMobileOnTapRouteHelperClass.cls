/**
 * This is a helper class for the Route_tgr trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileOnTapRouteHelperClass {

	/**
	 * This method assign the Application Id to every Route in Salesforce, this happens when a Route is created or updated.
	 *
	 * @param lstOnTapRoute List<ONTAP__Route__c>
	 */
	public static void assignApplicationIdToRoute(List<ONTAP__Route__c> lstOnTapRoute) {

		//Variables.
		AllMobileApplication__c objAllMobileApplicationPresales = new AllMobileApplication__c();
		AllMobileApplication__c objAllMobileApplicationAutosales = new AllMobileApplication__c();
		AllMobileApplication__c objAllMobileApplicationIRep = new AllMobileApplication__c();
		RecordType objRecordTypeSales = new RecordType();

		//Get RecordType Sales and Applications: Preventa, Autoventa, Reparto.
		List<RecordType> lstRecordTypeSales = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName =: AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_ONTAP_ROUTE_SALES LIMIT 1];
		if(!lstRecordTypeSales.isEmpty() && lstRecordTypeSales != NULL) {
			objRecordTypeSales = lstRecordTypeSales[0];
		}
		List<AllMobileApplication__c> lstAllMobileApplicationPresales = [SELECT Id, Name, AllMobileApplicationId__c FROM AllMobileApplication__c WHERE Name =: AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_APPLICATION_NAME_PREVENTA LIMIT 1];
		if(!lstAllMobileApplicationPresales.isEmpty() && lstAllMobileApplicationPresales != NULL) {
			objAllMobileApplicationPresales = lstAllMobileApplicationPresales[0];
		}
		List<AllMobileApplication__c> lstAllMobileApplicationAutosales = [SELECT Id, Name, AllMobileApplicationId__c FROM AllMobileApplication__c WHERE Name =: AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_APPLICATION_NAME_AUTOVENTA LIMIT 1];
		if(!lstAllMobileApplicationAutosales.isEmpty() && lstAllMobileApplicationAutosales != NULL) {
			objAllMobileApplicationAutosales = lstAllMobileApplicationAutosales[0];
		}
		List<AllMobileApplication__c> lstAllMobileApplicationIRep = [SELECT Id, Name, AllMobileApplicationId__c FROM AllMobileApplication__c WHERE Name =: AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_APPLICATION_NAME_REPARTO LIMIT 1];
		if(!lstAllMobileApplicationIRep.isEmpty() && lstAllMobileApplicationIRep != NULL) {
			objAllMobileApplicationIRep = lstAllMobileApplicationIRep[0];
		}

		//Assign the Application id depending on service model.
		if(!lstOnTapRoute.isEmpty() && lstOnTapRoute != NULL  && objRecordTypeSales.Id != NULL) {
			for(ONTAP__Route__c objOntapRoute: lstOnTapRoute) {
				if(objOntapRoute.ServiceModel__c == AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_SALES_MODE_PRESALES && objOntapRoute.RecordTypeId == objRecordTypeSales.Id) {
					objOntapRoute.AllMobileApplicationNameLK__c = objAllMobileApplicationPresales.Id;
				} else if(objOntapRoute.ServiceModel__c == AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_SALES_MODE_AUTOSALES  && objOntapRoute.RecordTypeId == objRecordTypeSales.Id) {
					objOntapRoute.AllMobileApplicationNameLK__c = objAllMobileApplicationAutosales.Id;
				} else if((objOntapRoute.ServiceModel__c == NULL) || (objOntapRoute.RecordTypeId == NULL)) {
					objOntapRoute.AllMobileApplicationNameLK__c = objAllMobileApplicationIRep.Id;
				} else {
					objOntapRoute.AllMobileApplicationNameLK__c = NULL;
				}
			}
		}
	}

	/**
	 * This method update the Record Type depending on Service Model (Autosales) and RecordType (Null). Routes from Heroku.
	 */
	public static void updateAutosalesRecordType(List<ONTAP__Route__c> lstOnTapRoute) {

		//Variables.
		RecordType objRecordTypeSales = new RecordType();

		//Get RecordType Sales.
		List<RecordType> lstRecordTypeSales = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName =: AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_ONTAP_ROUTE_SALES LIMIT 1];

		//Record Type Sales.
		if(!lstRecordTypeSales.isEmpty() && lstRecordTypeSales != NULL) {
			objRecordTypeSales = lstRecordTypeSales[0];
		}

		//Assigning Record Tpye Sales if Service Model is 'Autosales' and RecordType is null.
		if(!lstOnTapRoute.isEmpty() && lstOnTapRoute != NULL && objRecordTypeSales.Id != NULL) {
			for(ONTAP__Route__c objOntapRoute: lstOnTapRoute) {
				if((objOntapRoute.ServiceModel__c == AllMobileStaticVariablesClass.STRING_ONTAP_ROUTE_SALES_MODE_AUTOSALES) && (objOntapRoute.RecordTypeId == NULL)) {
					objOntapRoute.RecordTypeId = objRecordTypeSales.Id;
				}
			}
		}
	}
}