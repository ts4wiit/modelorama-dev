@isTest
public class MDRM_BusinessmanStage_tst {
   
    static testMethod void DRM_BusinessmanStage_ext() {
        
        Id devRecordTypeId = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE DeveloperName = 'MDRM_Businessman' AND SobjectType = 'Account' limit 1].id;
        Account acc = new Account (name='Test', MDRM_Stage__c = 'Prospect', RecordTypeId = devRecordTypeId);
        insert acc; 
        List<MDRM_AccountStages__c> lstaccS = new List<MDRM_AccountStages__c>();
        lstaccS.add( new MDRM_AccountStages__c(name = 'Prospect', MDRM_Stage__c = 'Prospect', Position__c = 1) );
        lstaccS.add( new MDRM_AccountStages__c(name = 'Basic Filter ', MDRM_Stage__c = 'Basic Filter (Age, schooling, investment amount, PC Management)', Position__c = 2) );
        insert lstaccS;
        PageReference pageRef = Page.MDRM_BusinessmanStage;
        
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardController sc = new Apexpages.StandardController(acc);
        MDRM_BusinessmanStage_ext ext = new  MDRM_BusinessmanStage_ext(sc); 
        ext.switchState();
        ext.editStage();
        ext.cancelStage();
        ext.saveStage();
        ext.assignStage();
        ext.loadInicialData();
        ext.getPicklistValuesOpAccountStages();
    }
  
}