@istest
public class MDRM_NewAttachmentProcessBuilder_tst {
    static testmethod void Attachment(){
        contact thiscontact = new contact(MDRM_Active_UEN_Contact__c = true, lastname= 'testing');
        insert thiscontact;
        MDRM_Informative_Session__c Session = new MDRM_Informative_Session__c( MDRM_Sendemail__C = false, 
                                                                              MDRM_Attachment_Generated__c = false,
                                                                             MDRM_UEN_CONTACT__C = THISCONTACT.id);
        insert session;
        id idsession = session.id;
        
        pagereference page = page.mdrm_assistance_list_pdf;
        page.getParameters().put('id', session.id);
        
        string soontobeblob = 'blob test';
        blob thisblob = blob.valueOf(soontobeblob);
        attachment att = new attachment(body = thisblob, parentid = idsession, contenttype ='application/pdf', name = 'Assistance List');
        insert att;
        
        Session.MDRM_Attachment_Generated__c = true;
        session.MDRM_SendEmail__c =true;
        update session;
        
        Test.startTest();
         mdrm_newattachmentprocessbuilder.createattachment(New id[]{session.id});
        list<attachment> newlstAtt = new list<attachment>([select id from attachment where parentid =: idsession]);
        integer newlstsize = newlstatt.size();
        
        system.assertEquals(1, newlstsize);
        Test.stopTest();
    }
}