/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Batch to delete all records in the org. (CDM_Temp_Taxes__c) 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-Mayo-2018   Iván Neria			   Initial version
***********************************************************************************/
global class CDM_MassDeleteTaxes_bch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT  Id, Name '
        +' FROM CDM_Temp_Taxes__c ';  
        return Database.getQueryLocator(query); 
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        delete scope; 
    }  
    global void finish(Database.BatchableContext BC)
    {
        EmailTemplate template =[SELECT Id FROM EmailTemplate WHERE Name=:Label.CDM_Apex_CDM_Delete_Data];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(template.Id);
        mail.setTargetObjectId(System.UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}