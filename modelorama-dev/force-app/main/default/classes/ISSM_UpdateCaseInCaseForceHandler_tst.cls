@isTest 
private class ISSM_UpdateCaseInCaseForceHandler_tst {
     
    @isTest static void UpdateStatusReopen() {
    
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
        objCaseForce.ONTAP__Description__c = 'Description';
        objCaseForce.ONTAP__Subject__c = 'Subject'; 
        objCaseForce.ONTAP__Status__c = 'New';
        insert objCaseForce;

        Case objCase = new Case();
            objCase.Subject = 'Subject';
            objCase.Description = 'Description';
            objCase.Status = 'New';
            objCase.ISSM_CaseForceNumber__c = objCaseForce.Id;
            insert objCase;
            
      
        Case objCaseUpdate1 = new Case();
            objCaseUpdate1.Id = objCase.Id;
            objCaseUpdate1.Status = 'Closed';
            objCaseUpdate1.ISSM_ReOpenCase__c = false;
            update objCaseUpdate1;
      
        Case objCaseUpdate2 = new Case();
            objCaseUpdate2.Id = objCase.Id;
            objCaseUpdate2.Status = 'Re-Open';
            objCaseUpdate2.ISSM_ReOpenCase__c = false;
            update objCaseUpdate2;
   
        Case objCaseUpdate3 = new Case();
            objCaseUpdate3.Id = objCase.Id;
            objCaseUpdate3.ISSM_SolutionName__c = 'Solucion update';
            objCaseUpdate3.ISSM_SolutionNote__c ='Solucion Name';  
            update objCaseUpdate3;
  
        Case objCaseUpdate4 = new Case();
            objCaseUpdate4.Id = objCase.Id;
            objCaseUpdate4.Status = 'Closed'; 
            objCaseUpdate4.ISSM_ReOpenCase__c = true;
            objCaseUpdate4.ISSM_SolutionName__c = 'Solucion update';
            objCaseUpdate4.ISSM_SolutionNote__c ='Solucion Name';  
     
        Solution objSolution2 = new Solution (
            SolutionName = objCaseUpdate4.ISSM_SolutionName__c,
            SolutionNote =  objCaseUpdate4.ISSM_SolutionNote__c,
            Status =  'Solucion Cerrada'
        ); 
        insert objSolution2;
    
        CaseSolution objCaseSolution = new CaseSolution(
            SolutionId = objSolution2.Id,
            CaseId = objCaseUpdate3.Id
        );
        insert objCaseSolution;
    
    Test.startTest();
        Map<Id, Case>  MapSend  = new Map<Id, Case>();
        ISSM_UpdateCaseInCaseForceHandler_cls obj = new ISSM_UpdateCaseInCaseForceHandler_cls ();
        
        MapSend.put(objCase.Id,objCase);
        obj.UpdateCaseInCaseForce(MapSend,MapSend);
        MapSend = new Map<Id, Case>();
        MapSend.put(objCaseUpdate4.Id,objCaseUpdate4);
        obj.UpdateCaseInCaseForce(MapSend,MapSend);
    
       Test.stopTest(); 
    }   
}