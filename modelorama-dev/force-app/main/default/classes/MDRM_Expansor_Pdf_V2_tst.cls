@isTest
private class MDRM_Expansor_Pdf_V2_tst {	

	//static Account newAccount;

	@isTest static void general_test() {		
		//Crear Cuenta
		Account acc = createAccount();
		//Crear formulario
		MDRM_Form__c form = createForm(acc);
		//Crear estudio de mercado
		MDRM_Market_Research__c mResearch = createResearch(acc);
		//Crear Attachment
		Attachment atti = createAttachment(mResearch.Id);

		//Contact ctc = createContact(true);		


		test.StartTest();

		
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);

		MDRM_Expansor_Pdf_V2_ctr EPDF = new MDRM_Expansor_Pdf_V2_ctr(sc);

		//EPDF.getData();

		//System.assertEquals(1, 1);

		
		test.StopTest();
	}

	public static Account createAccount() {
	    Account account = new Account();
	    account.Name = 'Prueba Expansor Nombre';
	    insert account;
	    
	    return account;
	}

	public static MDRM_Form__c createForm(Account accf) {
	    MDRM_Form__c form = new MDRM_Form__c();
	    form.MDRM_Account_Form__c = accf.Id;
	    form.MDRM_Corner_Type__c = 'No Corner';
	    insert form;
	    
	    return form;
	}

	public static MDRM_Market_Research__c createResearch(Account accm) {
	    MDRM_Market_Research__c mResearch = new MDRM_Market_Research__c();
	    mResearch.MDRM_Account_Market_Research__c = accm.Id;
	    mResearch.MDRM_Comparative_Local__c = 'Comparative Local 1';
	    mResearch.MDRM_Address__c = 'Calle Falsa 123';
	    mResearch.MDRM_Area__c = 50;
	    mResearch.MDRM_Income__c = 200;
	    mResearch.MDRM_Phone__c = '100200';
	    insert mResearch;
	    
	    return mResearch;
	}

	public static Attachment createAttachment(Id id) {
	    Attachment att = new Attachment();
	    att.ParentId = id;
	    att.Name = 'Prueba Attachment';
	    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
	    att.Body = bodyBlob;
	    att.ContentType = 'image';
	    insert att;
	    
	    return att;
	}
	
}