/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_UpdateRelaionships_bch". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_UpdateRelaionshipsBch_tst {
    @testSetup static void createData(){
		//Load Static resources 
        List<sObject> ls = Test.loadData(MDM_Temp_Account__c.sObjectType, 'MDMTempAccount');
        List<sObject> lsInt = Test.loadData(CDM_Temp_Interlocutor__c.sObjectType, 'CDMTempInterlocutores');
        List<sObject> lsTax = Test.loadData(CDM_Temp_Taxes__c.sObjectType, 'CDMTempTaxes');
        List<sObject> lsParams = Test.loadData(MDM_Parameter__c.sObjectType, 'CDMparams');
        
        //Create specific Record CDM_Temp_interlocutor for specific MDM_Temp_Account record.
        CDM_Temp_Interlocutor__c cdmTempInterlocutor = new CDM_Temp_Interlocutor__c();
        cdmTempInterlocutor.KUNN2__c='0100000281';
        cdmTempInterlocutor.KUNNR__c='0100160410';
        cdmTempInterlocutor.PARVW__c='SP';
        cdmTempInterlocutor.SPART__c='00';
        cdmTempInterlocutor.VKORG__c='3116';
        cdmTempInterlocutor.VTWEG__c='01';
        insert cdmTempInterlocutor;
         
    }
    static testmethod void testBatch() {
        Test.startTest();
        CDM_UpdateRelaionships_bch cdmUpdateRelaionships = new CDM_UpdateRelaionships_bch();
        Database.executeBatch(cdmUpdateRelaionships);
        Test.stopTest();
        // Assert validation: Validate the lookup relationship between CDM_Temp_Tax & MDM_Temp_Account be correct.
        CDM_Temp_Taxes__c tx = [SELECT Id FROM CDM_Temp_Taxes__c WHERE TATYP__c='MWST' AND KUNNR__c='0100160410'];
        MDM_Temp_Account__c tmpAcc = [SELECT Id, TAXKD01Id__c FROM MDM_Temp_Account__c WHERE PARVW__c = '0100160410' LIMIT 1];
        System.assertEquals(tx.Id,tmpAcc.TAXKD01Id__c);
        // Assert Validation: Validate the lookup relationship between CDM_Temp_Interlocutor & MDM_Temp_Account be correct.
        CDM_Temp_Interlocutor__c inter = [SELECT Id, MDM_Temp_Account__c FROM CDM_Temp_Interlocutor__c WHERE KUNNR__c='0100160410'];
        System.assertEquals(tmpAcc.Id,inter.MDM_Temp_Account__c);
    }
}