/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Test class for schedule class ISSM_IntegrateCmbCancelMulsoft_sch
*
* No.       Fecha              Autor                      Descripción
* 1.0    22-Junio-2018      Oscar Alvarez                   Creación
*******************************************************************************/
@isTest
private class ISSM_IntegrateCmbCancelMulsoft_tst{
    @testSetup static void loadData() {
        //create SEGMENTO
        MDM_Parameter__c segmento01 = new MDM_Parameter__c();
        segmento01.Active_Revenue__c= true;
        segmento01.Active__c        = true;
        segmento01.Catalog__c       = 'Segmento';
        segmento01.Code__c          = '42';
        segmento01.Description__c   = 'Foco (Tradicional)';
        segmento01.ExternalId__c    = 'Segmento-42';
        segmento01.Name             = 'Foco (Tradicional)';
        insert segmento01;
        
        Account grupoModeloAccount01 = new Account();
        grupoModeloAccount01.IsExcludedFromRealign=false;
        grupoModeloAccount01.ISSM_AccountToDel__c=false;
        grupoModeloAccount01.ISSM_AttachmentProcessed__c=false;
        grupoModeloAccount01.ISSM_EmptyCreditQty__c=false;
        grupoModeloAccount01.ISSM_HasBonus__c=false;
        grupoModeloAccount01.ISSM_MainContactA__c=false;
        grupoModeloAccount01.ISSM_MainContactB__c=false;
        grupoModeloAccount01.ISSM_MainContactC__c=false;
        grupoModeloAccount01.ISSM_MainContactD__c=false;
        grupoModeloAccount01.ISSM_MainContactE__c=false;
        grupoModeloAccount01.ISSM_SegmentCode__c=segmento01.Id;
        grupoModeloAccount01.ISSM_StartCall__c=false;
        grupoModeloAccount01.ISSM_ValidateOrder__c=false;
        grupoModeloAccount01.Name='Grp Mdl';
        grupoModeloAccount01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        grupoModeloAccount01.ONCALL__Ignore_Objective_Flag__c=false;
        grupoModeloAccount01.ONCALL__No_Call__c=false;
        grupoModeloAccount01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        grupoModeloAccount01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        grupoModeloAccount01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        grupoModeloAccount01.ONCALL__PO_Required__c=false;
        grupoModeloAccount01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        grupoModeloAccount01.ONCALL__Seasonal__c=false;
        grupoModeloAccount01.ONTAP__Classification__c='Botella Cerrada';
        grupoModeloAccount01.ONTAP__Decision_Maker__c=false;
        grupoModeloAccount01.ONTAP__Is_Wholesaler__c=false;
        grupoModeloAccount01.ONTAP__Loan_Applied__c=false;
        grupoModeloAccount01.ONTAP__Not_Interested__c=false;
        grupoModeloAccount01.ONTAP__SAPCustomerId__c='ABINBEV';
        grupoModeloAccount01.ONTAP__Segment__c='Botella Cerrada';
        grupoModeloAccount01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName = 'Account'].get(0).Id;
        insert grupoModeloAccount01;
        
        //Inser Division general ventas
        Account drv01 = new Account();
        drv01.IsExcludedFromRealign=false;
        drv01.ISSM_AccountToDel__c=false;
        drv01.ISSM_AttachmentProcessed__c=false;
        drv01.ISSM_EmptyCreditQty__c=false;
        drv01.ISSM_HasBonus__c=false;
        drv01.ISSM_MainContactA__c=false;
        drv01.ISSM_MainContactB__c=false;
        drv01.ISSM_MainContactC__c=false;
        drv01.ISSM_MainContactD__c=false;
        drv01.ISSM_MainContactE__c=false;
        drv01.ISSM_ParentAccount__c=grupoModeloAccount01.Id;
        drv01.ISSM_StartCall__c=false;
        drv01.ISSM_ValidateOrder__c=false;
        drv01.Name='DRV Centro Sur';
        drv01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        drv01.ONCALL__Ignore_Objective_Flag__c=false;
        drv01.ONCALL__No_Call__c=false;
        drv01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        drv01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        drv01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        drv01.ONCALL__PO_Required__c=false;
        drv01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        drv01.ONCALL__Seasonal__c=false;
        drv01.ONTAP__Decision_Maker__c=false;
        drv01.ONTAP__Is_Wholesaler__c=false;
        drv01.ONTAP__Loan_Applied__c=false;
        drv01.ONTAP__Negotiation_Status__c='Nuevo';
        drv01.ONTAP__Not_Interested__c=false;
        drv01.ONTAP__SAPCustomerId__c='GM00004';
        drv01.RecordTypeId=[SELECT id FROM RecordType where DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;
        insert drv01;
        
        Account salesOrg01 = new Account();
        salesOrg01.IsExcludedFromRealign=false;
        salesOrg01.ISSM_AccountToDel__c=false;
        salesOrg01.ISSM_AttachmentProcessed__c=false;
        salesOrg01.ISSM_EmptyCreditQty__c=false;
        salesOrg01.ISSM_HasBonus__c=false;
        salesOrg01.ISSM_MainContactA__c=false;
        salesOrg01.ISSM_MainContactB__c=false;
        salesOrg01.ISSM_MainContactC__c=false;
        salesOrg01.ISSM_MainContactD__c=false;
        salesOrg01.ISSM_MainContactE__c=false;
        salesOrg01.ISSM_ParentAccount__c=drv01.Id;
        salesOrg01.ISSM_SegmentCode__c=segmento01.Id;
        salesOrg01.ISSM_StartCall__c=false;
        salesOrg01.Name='CMM Acapulco';
        salesOrg01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        salesOrg01.ONCALL__Ignore_Objective_Flag__c=false;
        salesOrg01.ONCALL__No_Call__c=false;
        salesOrg01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        salesOrg01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        salesOrg01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        salesOrg01.ONCALL__PO_Required__c=false;
        salesOrg01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        salesOrg01.ONCALL__Seasonal__c=false;
        salesOrg01.ONTAP__Decision_Maker__c=false;
        salesOrg01.ONTAP__Is_Wholesaler__c=false;
        salesOrg01.ONTAP__Loan_Applied__c=false;
        salesOrg01.ONTAP__Negotiation_Status__c='Nuevo';
        salesOrg01.ONTAP__Not_Interested__c=false;
        salesOrg01.ONTAP__SalesOgId__c='3120';
        salesOrg01.ONTAP__SAPCustomerId__c='3120';
        salesOrg01.RecordTypeId= [SELECT Id FROM RecordType where DeveloperName= 'SalesOrg'].get(0).Id;
        insert salesOrg01;        
        
        Account salesOffice01 = new Account();
        salesOffice01.EndTime__c='0.833333333333333';
        salesOffice01.IsExcludedFromRealign=false;
        salesOffice01.ISSM_AccountToDel__c=false;
        salesOffice01.ISSM_AttachmentProcessed__c=false;
        salesOffice01.ISSM_EmptyCreditQty__c=false;
        salesOffice01.ISSM_HasBonus__c=false;
        salesOffice01.ISSM_MainContactA__c=false;
        salesOffice01.ISSM_MainContactB__c=false;
        salesOffice01.ISSM_MainContactC__c=false;
        salesOffice01.ISSM_MainContactD__c=false;
        salesOffice01.ISSM_MainContactE__c=false;
        salesOffice01.ISSM_ParentAccount__c=salesOrg01.Id;
        salesOffice01.ISSM_SalesOrg__c=salesOrg01.Id;
        salesOffice01.ISSM_SegmentCode__c=segmento01.Id;
        salesOffice01.ISSM_StartCall__c=false;
        salesOffice01.ISSM_ValidateOrder__c=false;
        salesOffice01.Name='CMM Ometepec';
        salesOffice01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        salesOffice01.ONCALL__Ignore_Objective_Flag__c=false;
        salesOffice01.ONCALL__No_Call__c=false;
        salesOffice01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        salesOffice01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        salesOffice01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        salesOffice01.ONCALL__PO_Required__c=false;
        salesOffice01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        salesOffice01.ONCALL__Seasonal__c=false;
        salesOffice01.ONTAP__Decision_Maker__c=false;
        salesOffice01.ONTAP__Is_Wholesaler__c=false;
        salesOffice01.ONTAP__Loan_Applied__c=false;
        salesOffice01.ONTAP__Negotiation_Status__c='Nuevo';
        salesOffice01.ONTAP__Not_Interested__c=false;
        salesOffice01.ONTAP__SalesOfficeDescription__c='CMM Acapulco';
        salesOffice01.ONTAP__SalesOffId__c='FD10';
        salesOffice01.ONTAP__SalesOgId__c='3120';
        salesOffice01.ONTAP__SAPCustomerId__c='FD10';
        salesOffice01.ParentId=salesOrg01.Id;
        salesOffice01.PercentageOffRoute__c=30;
        salesOffice01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName= 'SalesOffice'].get(0).Id;
        salesOffice01.TimeDifference__c='1';
        insert salesOffice01;
        
        ISSM_ComboLimit__c comboLimit01 = new ISSM_ComboLimit__c();  
        comboLimit01.ISSM_AllowedCombos__c=50;
        comboLimit01.ISSM_ComboLevel__c='ISSM_SalesOffice';
        comboLimit01.ISSM_IsActive__c=true;
        comboLimit01.ISSM_SalesStructure__c=salesOffice01.Id;
        comboLimit01.RecordTypeId= [SELECT Id FROM RecordType where DeveloperName = 'ISSM_LimitComboMx'].get(0).Id;
        insert comboLimit01;
        
        	sObject objObject = new ISSM_Combos__c();
            String strFld = 'ISSM_CancellationsReasons__c';
            List<testPickListValues> lstPickListValues = new List<testPickListValues>();
            Schema.sObjectType objType = objObject.getSObjectType();// Get the object type of the SObject.
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();// Describe the SObject using its object type.
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();// Get a map of fields for the SObject
        	Schema.PicklistEntry[] values = fieldMap.get(strFld).getDescribe().getPickListValues();
        
        ISSM_Combos__c  combo01 = new ISSM_Combos__c();
        combo01.ISSM_ComboLimit__c=comboLimit01.Id;
        combo01.ISSM_ComboType__c='Mixed';
        combo01.ISSM_Currency__c='MXN';
        combo01.ISSM_EndDate__c=System.Today();
        combo01.ISSM_ExternalKey__c='9000000430';
        combo01.ISSM_LongDescription__c='Combo Off';
        combo01.ISSM_NumberByCustomer__c=1;
        combo01.ISSM_NumberSalesOffice__c=1000;
        combo01.ISSM_ShortDescription__c='Combo Off';
        combo01.ISSM_StartDate__c=System.Today();
        combo01.ISSM_StatusCombo__c='ISSM_Cancelled';
        combo01.ISSM_CancellationDate__c = System.Today();
        combo01.ISSM_CancellationsReasons__c = values[0].getValue();
        combo01.ISSM_SynchronizedWithSAP__c=false;
        combo01.ISSM_TypeApplication__c='ALLMOBILE;TELESALES;B2B;BDR';
        combo01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName= 'ISSM_ComboMx'].get(0).Id;
        insert combo01;
        List<ISSM_Combos__c> lstCanceledCmbs = [SELECT ISSM_ExternalKey__c,ISSM_CancellationDate__c FROM ISSM_Combos__c WHERE ISSM_SynchronizedWithSAP__c = false AND ISSM_StatusCombo__c = 'ISSM_Cancelled' AND ISSM_CancellationDate__c = TODAY];
        System.debug('lstCanceledCmbs --->' + lstCanceledCmbs.size()+' Detail -------------->' + lstCanceledCmbs);
    }
    
    static testmethod void testExecuteScheduleCancel() {
        Test.startTest();
        	ISSM_IntegrateCmbCancelMulsoft_sch.executeScheduleCancel(); 
        Test.stopTest();
    }
    
    static testmethod void testEjecuteFlow() {
        Test.startTest();
            String jobId = System.schedule(Label.TRM_NameSchApexIntgrCmbCancel + System.now(),ISSM_IntegrateCmbCancelMulsoft_sch.CRON_EXPR,new ISSM_IntegrateCmbCancelMulsoft_sch());
            System.debug('jobId--> '+jobId);
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(ISSM_IntegrateCmbCancelMulsoft_sch.CRON_EXPR, ct.CronExpression);
        Test.stopTest();
    }
    public class testPickListValues{
        public String value;
        public String label;
        
    }
}