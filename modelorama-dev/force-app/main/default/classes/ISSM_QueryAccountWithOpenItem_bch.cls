/****************************************************************************************************
    General Information
    -------------------
    author:     Hecto Diaz
    email:      hdiaz@avaxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Class for site control   

    Information about changes (versions) 
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       20/Nov/2017      Hector Diaz  HD              Class created decorate class implemented
    ================================================================================================
****************************************************************************************************/
global class ISSM_QueryAccountWithOpenItem_bch implements Database.Batchable<sObject>{
    global String query='';
    String RecordTypeAccountId='';
    global ISSM_QueryAccountWithOpenItem_bch(){
        RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Account_RecordType);
        query = 'SELECT Id,Name,ONCALL__Risk_Category__c,ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c, ISSM_LastPromisePaymentDate__c '+
                ',(Select id,ISSM_Amounts__c, ISSM_DocumentType__c,ISSM_Account__c,ISSM_Debit_Credit__c From Open_Items__r) '+
                'FROM Account WHERE RecordTypeId=\''+ RecordTypeAccountId +'\'  '+
                'And Id IN(Select ISSM_Account__c From ISSM_OpenItemB__c)'; 
 
    }
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try{
            return Database.getQueryLocator(query);
        }catch(Exception e){
            System.debug(' Exception : '+e.getMessage());
            return null;
        }
            
    }
      
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Account objAccountUpdate = new Account();
        List<Account> lstAccountUpdate = new List<Account>();
        Set<Account> setAccountWithOpeI = new Set<Account>();
        setAccountWithOpeI.addAll((List<Account>)scope);
        Double dblDebit = 0.0;  //DEBE debe-haber
        Double dblCredit = 0.0; //HABER 
        Double dbAmount = 0.0;
        for(Account objIteraSetAccout :  setAccountWithOpeI){
            try{
                objAccountUpdate = new Account();
                dblDebit = 0.0;
                dblCredit = 0.0;
                dbAmount = 0.0;
                System.debug('Cuentas : '+objIteraSetAccout); 
                for(ISSM_OpenItemB__c objOpenI : objIteraSetAccout.Open_Items__r){
                    System.debug('OPEN ITEM : '+objIteraSetAccout); 
                    if(objOpenI.ISSM_Debit_Credit__c == System.label.ISSM_Debit ){
                        dblDebit = dblDebit + objOpenI.ISSM_Amounts__c;
                        System.debug('dblDebit = '+dblDebit);
                    }
                    if(objOpenI.ISSM_Debit_Credit__c == System.label.ISSM_CreditOI) {
                        dblCredit = dblCredit + objOpenI.ISSM_Amounts__c;
                        System.debug('dblCredit = '+dblCredit);
                    }
                }   
                
                if(dblDebit > 0.0 &&  dblCredit > 0.0 ){
                    System.debug('EXISTEN LOS " VALORES : DEBE / HABER '+ dblDebit  +'---'+dblCredit );
                    dbAmount = dblDebit-dblCredit;
                    System.debug('MONTO TOTAL A DEBER  : '+dbAmount);
                }
                if(dblDebit > 0.0 && dblCredit==0.0){
                    dbAmount = dblDebit;
                    System.debug('SOLO EXISTE DEBE : dblDebit '+dblDebit );
                }
                /*if(dblDebit == 0.0 && dblCredit > 0.0){
                    dbAmount = dblCredit;
                    System.debug('SOLO EXISTE DEBE : HABER  '+dblCredit );
                }
                */
                objAccountUpdate.Id =  objIteraSetAccout.Id; 
                objAccountUpdate.ISSM_TotalDebt__c = dbAmount;
                lstAccountUpdate.add(objAccountUpdate);    
            }catch(QueryException QE){
                System.debug('*** QueryException : '+QE);
            }     
        }
        
        if(!lstAccountUpdate.isEmpty()){
            System.debug('Entro update Account');
            update lstAccountUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        system.debug('Finish');
    } 
}