@isTest(seeAllData=false)
private class MDRM_Expansor_Pdf_Config_V2_tst {

    /**
     * Description. Create an Expansor Account type.
     */
    @testSetup static void setup() {
        Profile profileId = [SELECT Id FROM Profile WHERE UserType = 'Standard' LIMIT 1];

        User usr = new User(LastName = 'expansor',
             FirstName='Modelorama',
             Alias = 'mdrmexp',
             Email = 'profile.expansor@mdrm.com',
             Username = 'profile.expansor@mdrm.com',
             ProfileId = profileId.id,
             TimeZoneSidKey = 'GMT',
             LanguageLocaleKey = 'en_US',
             EmailEncodingKey = 'UTF-8',
             LocaleSidKey = 'en_US'
        );
        insert usr;

        User currentUser = [SELECT Name FROM User WHERE Id =: Userinfo.getUserId()];
        currentUser.MDRM_Corporative_Approver__c = usr.Id;
        currentUser.MDRM_Legal_Approver__c = usr.Id;
        currentUser.MDRM_UEN_Approver__c = usr.Id;
        currentUser.MDRM_LiderGerenteApprover__c = usr.Id;
        currentUser.MDRM_SupervisorApprover__c = usr.Id;
        update currentUser;

        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'MDRM_Expansor'];
        System.assertNotEquals(null, rt, 'Did not fetch some Recordtype');

        Account acc = new Account(
            Name = 'Expansor test',
            MDRM_Stage__c = 'Negotiation',
            MDRM_Substage__c = 'N/A',
            RecordTypeId = rt.Id
        );
        insert acc;
    }
    
    @isTest static void test_Document_save_clasificationt() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();

        ctr.save_clasification();

        test.stopTest();
    }   
    
    
    @isTest static void test_Documen_save_clasificationDoble() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();

        ctr.save_clasification();
        ctr.save_clasification();

        test.stopTest();
    }   
    
    @isTest static void test_Document_deleteAtt() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();

        ctr.save_clasification();
        ctr.deleteAtt();

        test.stopTest();
    }   
    
    
    @isTest static void test_Document_saveChanges() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();

        ctr.save_clasification();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.saveChanges();
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Documento';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'RFC';
        ctr.lstDocumento[0].doc.MDRM_Send_for_Approval__c = 'YES';
        ctr.lstDocumento[0].doc.MDRM_Exp_Date__c = Date.today()-1;
        ctr.saveChanges();
        
        test.stopTest();
    }
    
    @isTest static void test_Document_sendDocToApproval() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();

        ctr.save_clasification();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.sendDocToApproval();

        test.stopTest();
    }   
    
    
    @isTest static void test_Documen_deleteOnApprovalProcess() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();
        
        ctr.save_clasification();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.sendDocToApproval();
        
        ctr.lstDocumento[0].doc.MDRM_Status__c = 'Enviado a Aprobar';
        ctr.saveChanges();
        
        try{
            delete ctr.lstDocumento[0].doc;
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains( Label.MDRM_DocumentDMLWarning ) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
        
        test.stopTest();
    }   
    
    
    @isTest static void test_Image() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        string idacc = acc.id;
        String docName = 'RFC';

        for(Integer i = 1 ; i <=2 ; i++) {
            ContentVersion cv = new ContentVersion();
            cv.VersionData   = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv.Title = docName;
            cv.PathOnClient  = docName+'.JPG';
            Insert cv;
        
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
            cdl.LinkedEntityId = acc.id;
            cdl.ShareType = 'V';
            insert cdl;
        }

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        test.startTest();
        
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.mapDocument.put(ctr.cvID, ctr.lstDocumento[0]);
        ctr.save_clasification();
        ctr.saveChanges();
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Documento';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'RFC';
        ctr.saveChanges();
        ctr.deleteAtt();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Imagen';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'Croquis (Imagen del Poc Compass)';
        ctr.mapDocument.put(ctr.cvID, ctr.lstDocumento[0]);
        ctr.saveChanges();
        ctr.sendDocToApproval();
        
        test.stopTest();
    }

    @isTest static void test_Image_Contrato() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        string idacc = acc.id;
        String docName = 'RFC';
        
        for(Integer i = 1 ; i <=2 ; i++) {
            ContentVersion cv = new ContentVersion();
            cv.VersionData   = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv.Title = docName;
            cv.PathOnClient  = docName+'.JPG';
            Insert cv;
        
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
            cdl.LinkedEntityId = acc.id;
            cdl.ShareType = 'V';
            insert cdl;
        }
        
        test.startTest();
        
        ctr.init();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.lstDocumento[0].doc.Name = 'Contrato';
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Documento';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'Contrato de Arrendamiento';
        ctr.mapDocument.put(ctr.cvID, ctr.lstDocumento[0]);
        ctr.saveChanges();
        ctr.sendDocToApproval();
        
        test.stopTest();
    }

    @isTest static void test_Image_Licencia() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        string idacc = acc.id;
        String docName = 'RFC';
        
        for(Integer i = 1 ; i <=2 ; i++) {
            ContentVersion cv = new ContentVersion();
            cv.VersionData   = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv.Title = docName;
            cv.PathOnClient  = docName+'.JPG';
            Insert cv;
        
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
            cdl.LinkedEntityId = acc.id;
            cdl.ShareType = 'V';
            insert cdl;
        }
        
        test.startTest();
        
        ctr.init();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Documento';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'Licencia';
        ctr.saveChanges();
        ctr.sendDocToApproval();
        
        test.stopTest();
    }

    @isTest static void testSaveChanges() {
        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        System.assertNotEquals(null, acc, 'Account did not fetch');

        ApexPages.currentPage().getParameters().put('id',acc.id);
        ApexPages.StandardController stdacc = new ApexPages.StandardController(acc);
        MDRM_Expansor_Pdf_Config_V2_ctr ctr  = new MDRM_Expansor_Pdf_Config_V2_ctr(stdacc);
        
        string idacc = acc.id;
        String docName = 'Licencia';
        
        for(Integer i = 1 ; i <=2 ; i++) {
            ContentVersion cv = new ContentVersion();
            cv.VersionData   = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv.Title = docName;
            cv.PathOnClient  = docName+'.JPG';
            Insert cv;
        
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
            cdl.LinkedEntityId = acc.id;
            cdl.ShareType = 'V';
            insert cdl;
        }
        
        test.startTest();
        
        ctr.init();
        ctr.cvID = ctr.lstDocumento[0].cv.id;
        ctr.lstDocumento[0].doc.MDRM_File_Type__c = 'Documento';
        ctr.lstDocumento[0].doc.MDRM_File_SubType__c = 'Licencia';
        ctr.lstDocumento[0].doc.MDRM_Expiration_Date__c = '2050-01-01';
        ctr.saveChanges();
        ctr.sendDocToApproval();
        
        test.stopTest();
    }

}