/**************************************************************************************
Nombre de la clase de prueba: ISSM_SurveyInvitation_tst
Versión : 1.0
Fecha de Creación : 06 Septiembre 2018
Funcionalidad : Clase que procesa el trigger ISSM_SurveyInvitation_tgr y la clase de apex ISSM_SurveyInvitation_thr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega     06 - Septiembre - 2018      Versión Original
*************************************************************************************/
@isTest (SeeAllData = true)
public class ISSM_SurveyInvitation_tst {
    static testmethod void principalMethod() {
        User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        Id communityId = [SELECT Id, Name FROM Community].Id;
        Id surveyId = [SELECT Id, Name FROM Survey LIMIT 1].Id;
        String IdNet = Network.getNetworkId();
        
        //String IdSurvey = Survey.getSurveyId();
        
        System.runAs(thisUser) {
            Test.startTest();
            
            SurveyInvitation surveyInv = new SurveyInvitation();
            surveyInv.Name = 'Survey Test 1';
            surveyInv.SurveyId = surveyId;
            surveyInv.CommunityId = IdNet;
            insert surveyInv;
            
            Test.stopTest();
        }  
    }
}