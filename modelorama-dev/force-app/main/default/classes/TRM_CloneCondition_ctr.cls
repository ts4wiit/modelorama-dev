/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows clone the records for condition

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    30-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class TRM_CloneCondition_ctr {
	public TRM_CloneCondition_ctr() {}

    /**
    * @description  Method that allows to clone the selected condition type, later execution is performed
    *               of a batch to create all its records related to the new condition type
    * @param    IdCondClass     Id of Condition class to clone
    * @return   String          Id of New condition class
    */
	@AuraEnabled
	public static String getConditions(String IdCondClass){
        TRM_ConditionClass__c NewCCRec = new TRM_ConditionClass__c();
		TRM_ConditionClass__c CCRec = (TRM_ConditionClass__c) ISSM_UtilityFactory_cls.getRecordById('TRM_ConditionClass__c',IdCondClass);
  
        NewCCRec = CCRec.clone();
        NewCCRec.TRM_IsMassiveApproval__c   = false;
        NewCCRec.TRM_MarkedLiquidation__c   = false;
        NewCCRec.TRM_ReadyApproval__c       = false;
        NewCCRec.TRM_FailedSendtoSap__c     = false;
        NewCCRec.TRM_Status__c              = 'TRM_Pending';
        NewCCRec.TRM_ReadyForEdit__c 	    = false;
        NewCCRec.TRM_IsClone__c             = true;
        NewCCRec.TRM_ExternalId__c          = null; 
        NewCCRec.TRM_EndDate__c             = Date.today() + NewCCRec.TRM_StartDate__c.daysBetween(NewCCRec.TRM_EndDate__c);
        NewCCRec.TRM_StartDate__c           = Date.today() + Integer.valueOf(System.Label.TRM_StartDateConditionClass);
        
        insert NewCCRec;

        //Execute batch for Clone Condition Records
        TRM_CloneCondition_bch bchCloneProcess = new TRM_CloneCondition_bch(IdCondClass,NewCCRec.Id);
        Id bchCloneProcessId  = Database.executeBatch(bchCloneProcess);
        System.debug('###bchCloneProcessId### '+bchCloneProcessId);

		return NewCCRec.Id;
	}
}