/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice order items

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    1.1       04-01-2018      Wilmer Piedrahita             Adjustment to ONTAP__Product__c query for map collection key value change.
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OrderItem_cls {
	ONTAP__Order_Item__c orderItem_obj;
	public List<ONTAP__Order_Item__c> orderItem_lst;

	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  Map<String, Account> mapAccountParam
	 */
	public ISSM_OrderItem_cls(List<salesforce_ontap_order_item_c__x> orderItemExt, Map<String, ONTAP__Order__c> mapOrders) {
		orderItem_lst = getList(orderItemExt, mapOrders);
		
	}

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public List<sObject> getList(List<sObject> orderItemExt, Map<String, ONTAP__Order__c> mapOrders){
		List<ONTAP__Order_Item__c> orderItemsReturn;

		Map<String, ONTAP__Product__c> mapProducts = new Map<String, ONTAP__Product__c>();
		Set<String> idsProducts = new Set<String>();

		for(SObject orderItm_obj : orderItemExt){
			idsProducts.add(((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_productid_c__c);
		}

		for(ONTAP__Product__c  prod_obj : [SELECT Id,ONTAP__ProductId__c,ONCALL__Material_Number__c FROM ONTAP__Product__c WHERE ONCALL__Material_Number__c IN: idsProducts]){
			//mapProducts.put(prod_obj.ONCALL__Material_Number__c,prod_obj);
            mapProducts.put(prod_obj.ONTAP__ProductId__c,prod_obj); // WP 20180104: Add new key value field to match products with Order Items. Fixes a reported issue on 01/04/2018 by Marco Ramírez.
		}

		if(orderItemExt != null && orderItemExt.size() > 0){
			orderItemsReturn = new List<ONTAP__Order_Item__c>();
			for(SObject orderItm_obj : orderItemExt){
				orderItem_obj = new ONTAP__Order_Item__c();
				try{
					orderItem_obj.ONTAP__ActualQuantity__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_actualquantity_c__c;
					orderItem_obj.ONTAP__CustomerOrder__c = mapOrders.get(((salesforce_ontap_order_item_c__x)orderItm_obj).oncall_sap_order_number_c__c).Id;
					orderItem_obj.ISSM_MaterialProduct__c  = ((salesforce_ontap_order_item_c__x)orderItm_obj).issm_itemproductname_c__c;
					orderItem_obj.ISSM_ProductName__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).issm_itemproductname_c__c;
					orderItem_obj.ONTAP__LineAmount__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_lineamount_c__c;
					orderItem_obj.ONTAP__NetValue__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_netvalue_c__c;
					orderItem_obj.ONCALL__OnCall_Quantity__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).oncall_oncall_quantity_c__c;
					orderItem_obj.ONCALL__SAP_Order_and_Order_Item_Number__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).issm_uniqueid_c__c;
					orderItem_obj.ONTAP__ItemId__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_itemid_c__c;
					orderItem_obj.ISSM_ProductDesc__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_product_description_c__c;
					orderItem_obj.ISSM_UnitofMeasure__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).issm_presentation_c__c;
					orderItem_obj.ISSM_UnitPrice__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_lineamount_c__c;
					orderItem_obj.ONTAP__ProductId__c = mapProducts.get(((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_productid_c__c).Id;
					orderItem_obj.ONCALL__OnCall_Product__c = mapProducts.get(((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_productid_c__c).Id;
					orderItem_obj.ONTAP__ItemProduct__c = mapProducts.get(((salesforce_ontap_order_item_c__x)orderItm_obj).ontap_productid_c__c).Id;
				} catch(NullPointerException ne){
					system.debug( '\n\n  ****** Error buscando producto = ' + +'\n\n' );
				}
				
				orderItem_obj.ISSM_TaxAmount__c = ((salesforce_ontap_order_item_c__x)orderItm_obj).issm_taxamount_c__c;

				orderItemsReturn.add(orderItem_obj);
			}
		}
		return orderItemsReturn;
	}
	/**
	 * Save the result of the sincronization process		                             
	 */
	public void save(){
		try{
			upsert orderItem_lst ONCALL__SAP_Order_and_Order_Item_Number__c;
		}catch(Exception e){
			System.debug('### ISSM_OrderItem_cls [ERROR] NO orderitems were saved'); 
			System.debug('### ISSM_OrderItem_cls [ERROR DESC]'+e.getMessage()); 
		}
	}

}