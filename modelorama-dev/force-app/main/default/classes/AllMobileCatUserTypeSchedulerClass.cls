/**
 * This class is the schedule for the AllMobileCatUserTypeBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileCatUserTypeSchedulerClass implements Schedulable {

	/**
	 * Execute AllMobileCatUserTypeBatchClass class.
	 */
	global void execute(SchedulableContext objSchedulableContext) {
		AllMobileCatUserTypeBatchClass objAllMobileCatUserTypeBatchClass = new AllMobileCatUserTypeBatchClass();
		Database.executeBatch(objAllMobileCatUserTypeBatchClass);
	}
}