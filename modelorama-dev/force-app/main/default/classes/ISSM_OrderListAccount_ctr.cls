/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Clase que consulta los ultimos 8 pedidos realizados por el cliente

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       04-Septiembre-2017    Luis Licona                  Class Creation
    ================================================================================================
****************************************************************************************************/

public with sharing class ISSM_OrderListAccount_ctr {
	
    public ONTAP__Order__c[] OrderList{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();
    
    /*
        Metodo constructor
    */
    public ISSM_OrderListAccount_ctr(ApexPages.StandardController controller){ 

        Datetime Dt = Datetime.parse(System.now().format());
        OrderList   = CTRSOQL.getLastOrders(controller.getId(),Dt);
    }
}