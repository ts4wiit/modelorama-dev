public with sharing class ISSM_Constants_cls {
	public static final String USER = System.label.ISSM_AssignCaseUser;
    public static final String QUEUE = System.label.ISSM_AssignCaseQueue;
    public static final String SUPERVISOR =System.label.ISSM_AssignCaseSupervisor;
    public static final String BILLINGMANAGER = System.label.ISSM_AssignCaseBillingManager;
    public static final String TRADEMARKETING = System.label.ISSM_AssignCaseTradeMarketing; 
    public static final String MODELORAMA = System.label.ISSM_AssignCaseModelorama;
    public static final String REPARTO = System.label.ISSM_AssignCaseReparto;
    public static final String BOSSREFRIGERATION = System.label.ISSM_AssignCaseBossRefrigeration;
}