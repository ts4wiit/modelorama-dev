@isTest
private class MDRM_Expansor_Pdf_Config_tst {
	
	@isTest static void test_save() {
		//Crear Cuenta
		Account acc = createAccount();
		//Crear Imagenes Attachment
		Attachment atti = createImage(acc.Id, 'Volumen');


		test.StartTest();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		
		MDRM_Expansor_Pdf_Config_ctr EPDFConfig = new MDRM_Expansor_Pdf_Config_ctr(sc);		

		EPDFConfig.save_clasification();
		
		test.StopTest();
	}
	
	@isTest static void test_generate() {
		//Crear Cuenta
		Account acc = createAccount();
		//Crear Imagenes Attachment
		Attachment atti = createImage(acc.Id, '123');


		test.StartTest();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		
		MDRM_Expansor_Pdf_Config_ctr EPDFConfig = new MDRM_Expansor_Pdf_Config_ctr(sc);		

		EPDFConfig.PDF_generate();
		
		test.StopTest();
	}

	public static Account createAccount() {
	    Account account = new Account();
	    account.Name = 'Prueba Expansor Nombre';
	    insert account;
	    
	    return account;
	}

	public static Attachment createImage(Id id, String sDesc) {
	    Attachment att = new Attachment();
	    att.ParentId = id;
	    att.Name = 'Prueba Attachment';
	    att.Description = sDesc;
	    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
	    att.Body = bodyBlob;
	    att.ContentType = 'image';
	    insert att;
	    
	    return att;
	}	
	
}