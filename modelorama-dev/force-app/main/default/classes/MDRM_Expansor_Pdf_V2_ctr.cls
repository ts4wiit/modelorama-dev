/****************************************************************************************************
    General Information
    -------------------
    author:     Fernando Engel 
    email:      fernando.engel.funes@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Generar información del Expansor en formato PDF

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                        Description          
    ------    --------        --------------------------    -----------------------------------------
    1.0       29/06/2018      Fernando Engel                Class created
    ================================================================================================
****************************************************************************************************/

public with sharing class MDRM_Expansor_Pdf_V2_ctr {
    private List<MDRM_Form__c> lForms {get;set;}
    private List<User> lUser {get;set;}
    public List<MDRM_Market_Research__c> lEstudios_Precios {get;set;}
    public List<MDRM_Modelorama_Payback__c> lPayback {get;set;}

    public MDRM_Form__c FormAccount {get;set;}
    public User oUser {get;set;}
    public MDRM_Modelorama_Payback__c oPayback {get;set;}
    

    public list<ContentVersion> imgCroquis {get;set;}
    public list<ContentVersion> imgVolumen {get;set;}
    public list<ContentVersion> imgZPotenciales {get;set;}
    public list<ContentVersion> imgNivelSE {get;set;}
    public list<ContentVersion> imgReporte {get;set;}
    public list<ContentVersion> imgEntorno {get;set;}
    public list<ContentVersion> imgVistas {get;set;}
    public list<ContentVersion> imgLevantamiento {get;set;}

    public list<ContentVersion> imgLayout {get;set;}
    //public list<ContentVersion> imgFachada {get;set;}
    public list<ContentVersion> imgRender {get;set;}
    public list<ContentVersion> imgPresupuesto {get;set;}
    public list<ContentVersion> imgOperando {get;set;}

    private final Account acct;

    public MDRM_Expansor_Pdf_V2_ctr(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        getData();
        getImages();
    }   

    public String getData()
    {      
        //Obtener Id del Owner de la Cuenta
        List<Account> lAccountOwner = [SELECT OwnerId FROM Account WHERE Id=: acct.Id];
        String sOwnerId;
        //Validar que retorne registros
        if(lAccountOwner!=null && !lAccountOwner.isEmpty()) {
            sOwnerId = lAccountOwner[0].OwnerId;    
        }


        //  *****  Obtener datos del Formulario (Cuestionario)
        lForms = [SELECT MDRM_Location_Analysis__c, MDRM_Corner_Type__c, MDRM_Local_Area__c,
                        MDRM_Employment_Generator__c, MDRM_Population_Density__c, MDRM_Activity_Generator__c, 
                        MDRM_Visibility__c, MDRM_Street_Type__c, MDRM_Parking__c, 
                        MDRM_Speed_Bumps__c, MDRM_Street_Way__c, MDRM_Ridge__c, 
                        MDRM_Average_Foot_Traffic__c, MDRM_Average_Vehicular_Traffic__c,
                        MDRM_ABInBev_50__c, MDRM_ABInBev_50_100__c, MDRM_ABInBev_100__c,
                        MDRM_Heineken_50__c, MDRM_Heineken_50_100__c, MDRM_Heineken_100__c
                    FROM MDRM_Form__c
                    WHERE MDRM_Account_Form__c =: acct.Id
                    ORDER BY CreatedDate DESC NULLS LAST];

        //Validar que retorne registros
        if(lForms!=null && !lForms.isEmpty()) {
            FormAccount = lForms[0];
        }

        //  *****  Obtener datos de Owner
        lUser = [SELECT Name, Division, ONTAP__Business_Unit__c
                    FROM User
                    WHERE Id =: sOwnerId];

        //Validar que retorne registros
        if(lUser!=null && !lUser.isEmpty()) {
            oUser = lUser[0];
        }

        //  *****  Obtener datos de Estudios de precios (Este es un detalle de Cuenta)
        List<MDRM_Market_Research__c> lEstudios_Preciostmp = [SELECT Id, MDRM_Address__c, MDRM_Area__c, MDRM_Income__c,
                                                                MDRM_Contact__c, MDRM_Phone__c, MDRM_Photo__c, MDRM_Square_meter_mkt__c                                 
                                                            FROM MDRM_Market_Research__c
                                                            WHERE MDRM_Account_Market_Research__c =: acct.Id
                                                            ORDER BY CreatedDate DESC
                                                            LIMIT 2];      

        for (MDRM_Market_Research__c EP : lEstudios_Preciostmp){
            //Obtener imagen asignada en attachments
            List<Attachment> imgEstudios = [SELECT Id, Name, Description
                                            FROM Attachment 
                                            WHERE ParentId =: EP.Id
                                            AND ContentType LIKE 'image%'                                       
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1];

            if(imgEstudios!=null && !imgEstudios.isEmpty()) {                
                EP.MDRM_Photo__c = imgEstudios[0].Id;
            }
        }

        lEstudios_Precios = lEstudios_Preciostmp;

        //this.imgReporte = imgReporte;

        //  *****  Obtener datos del Formulario (Cuestionario)
        lPayback = [SELECT MDRM_Estimated_monthly_sales_cartons__c, MDRM_Estimated_Monthly_Sales_Hectolitres__c,
                            MDRM_Average_Price_Box__c, MDRM_Commission_Percentage__c,
                            MDRM_Electrical_Bill_Month__c, MDRM_Wages_Month__c,
                            MDRM_Maintenance_Month__c, MDRM_Others_Month__c,
                            MDRM_Building_Work__c, MDRM_License__c
                    FROM MDRM_Modelorama_Payback__c
                    WHERE MDRM_Account__c =: acct.Id
                    ORDER BY CreatedDate DESC
                    Limit 1];

        //Validar que retorne registros
        if(lPayback!=null && !lPayback.isEmpty()) {
            oPayback = lPayback[0];
        }

        return 'Ok';
    }

    public void getImages(){
        if(this.acct.Id != null){
            set<Id> lstContentDocumentId = new set<Id>();
            Id  accId = this.acct.Id;
            
            List<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>([SELECT ContentDocumentId,Id,LinkedEntityId 
                                                                              FROM ContentDocumentLink 
                                                                              WHERE LinkedEntityId = :accId]);
            
            
            for(ContentDocumentLink cdl : lstCDL){
                lstContentDocumentId.add(cdl.ContentDocumentId);
            }
            
            List<ContentVersion> lstCV = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                   FROM ContentVersion 
                                                                   WHERE ContentDocumentId IN: lstContentDocumentId
                                                                   AND IsLatest = TRUE]);
            this.imgCroquis = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Croquis (Imagen del Poc Compass)'
                                                                        AND IsLatest = TRUE]);
            this.imgVolumen = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Estimación del Volumen (Imagen del Poc Compass)'
                                                                        AND IsLatest = TRUE]);
            this.imgZPotenciales = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Zonas Potenciales (Imagen del Poc Compass)'
                                                                        AND IsLatest = TRUE]);
            this.imgNivelSE = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Capas N.S.E. (Imagen del Poc Compass)'
                                                                        AND IsLatest = TRUE]);
            this.imgReporte = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Reporte (Imagen del Poc Compass)'
                                                                        AND IsLatest = TRUE]);
            this.imgEntorno = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Entorno de la zona y Nivel Socioeconómico'
                                                                        AND IsLatest = TRUE]);
            this.imgVistas = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Fotos de frente, costados y vista desde el local'
                                                                        AND IsLatest = TRUE]);
            this.imgLevantamiento = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Levantamiento proporcionado por el Contratista'
                                                                        AND IsLatest = TRUE]);
            this.imgLayout = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Layout proporcionado por el Contratista'
                                                                        AND IsLatest = TRUE]);
            this.imgRender = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Render o fotomontaje proporcionado por el Contratista'
                                                                        AND IsLatest = TRUE]);
            this.imgPresupuesto = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Captura de pantalla del Resumen de la Cotización'
                                                                        AND IsLatest = TRUE]);
            this.imgOperando = new List<ContentVersion>([SELECT ContentDocumentId,Id,Description,ContentSize,FileType,Title,FileExtension 
                                                                        FROM ContentVersion 
                                                                        WHERE ContentDocumentId IN: lstContentDocumentId
                                                                        AND Description = 'Local operando (2 fotos exteriores y 2 interiores)'
                                                                        AND IsLatest = TRUE]);
        }
        
        
    }
}