/****************************************************************************************************
    General Information
    -------------------
    author:     Fernando Engel 
    email:      fernando.engel.funes@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Class for site control

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       28/05/2018      Fernando Engel FE            Class created decorate class implemented
    ================================================================================================
****************************************************************************************************/
global class MDRM_Site_ctr {
    global String section{get;set;}
    global contact OContact {get;set;}
    public MDRM_Site_ctr(){
        section = apexpages.currentpage().getparameters().get('section');
        
        if(section == 'vision' || section == 'bonus'){
            init();
        }else if(section == 'form'){
            OContact = new Contact();
            OContact.Id = null;
        }
        
        
    }
/** Form code   **/ 

    @RemoteAction
    global static List<WrpQuestion> lstQuestion()
    {   
        MDRM_Form__c oForm = new MDRM_Form__c();
        list<WrpQuestion> LstWrpQues = new list<WrpQuestion>();
        LstWrpQues.add(getQuestionWrp('MDRM_How_did_you_hear_about__c',true,'MDRM_How_did_you_hear_about_comment__c','block','1','1',''));
        //LstWrpQues.add(getQuestionWrp('MDRM_How_did_you_hear_about_comment__c',false,'',true,'','',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Grade_of_schooling__c',false,'','block','2','2',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Marital_status__c',false,'','block','3','3',''));
        LstWrpQues.add(getQuestionWrp('MDRM_People_financially_dependent__c',false,'','block','4','4',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Currently_Employed__c',false,'','block','5','5','true'));
        LstWrpQues.add(getQuestionWrp('MDRM_Current_employment_full_time__c',false,'','none','','6',''));
        //LstWrpQues.add(getQuestionWrp('MDRM_Current_Employment__c',true,'MDRM_Activities_last_employment__c','none','','7',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Current_Employment__c',true,'MDRM_Current_Employment_comment__c','none','','7',''));
        //LstWrpQues.add(getQuestionWrp('MDRM_Current_employment_comment__c',false,'',true,'','',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Activities_last_employment__c',true,'MDRM_Activities_last_employment_comment__c','block','6','',''));
        //LstWrpQues.add(getQuestionWrp('MDRM_Activities_last_employment_comment__c',false,'','block','7','9',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Computer_domain_level__c',false,'','block','7','8',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Experience_operating_similar_store__c',false,'','block','8','9',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Have_capital_necessary__c',false,'','block','9','10',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Former_or_retired_employee__c',false,'','block','10','11',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Retired_military__c',false,'','block','11','12',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Availability_To_Start_Operating__c',false,'','block','12','13',''));        
        LstWrpQues.add(getQuestionWrp('MDRM_Why_interested_entrepreneur__c',false,'','block','13','14',''));
        LstWrpQues.add(getQuestionWrp('MDRM_Facebook_Account__c',false,'','block','14','15',''));

        return LstWrpQues;
    }

    public static WrpQuestion getQuestionWrp( String FielQuestionName, Boolean Getrecomendatios, String ApifieldFollow,String StrDisplay, String StrNormalNum,String StrJobNum, String SpecialCondiP)
        {
            Schema.sObjectType objType = MDRM_Form__c.getSObjectType(); 
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
            WrpQuestion WrpQre = new WrpQuestion(
                                                fieldMap.get(FielQuestionName).getDescribe().getInlineHelpText().ToUpperCase(),
                                                FielQuestionName,
                                                FielQuestionName+'Q',
                                                FielQuestionName+'A',
                                                Getrecomendatios,
                                                ApifieldFollow,
                                                getPicklistLabels(FielQuestionName),
                								getPicklistValues(FielQuestionName),
                                                StrDisplay,
                                                StrNormalNum,
                                                StrJobNum,
                                                SpecialCondiP
                                                );
            return WrpQre;
        }

    @RemoteAction
    global static WrpPostalCode ValidatePostalCode(String StrPostalCode){
        WrpPostalCode wrpPostal;
        System.debug('<<<<<<StrPostalCode>>>>>> '+StrPostalCode);
        List<MDRM_Modelorama_Postal_Code__c> LstModelo = new List<MDRM_Modelorama_Postal_Code__c>();
        LstModelo = [select Id, Name,MDRM_Municipality__c,MDRM_Postal_Code__c,MDRM_State__c
                        from MDRM_Modelorama_Postal_Code__c
                        where MDRM_Postal_Code__c =: StrPostalCode];
        List<String> LstColonia = new List<String>();
        System.debug('<<<<<<LstModelo>>>>>> '+LstModelo);

        for(MDRM_Modelorama_Postal_Code__c oPostalCode : LstModelo)
        {
            LstColonia.add(oPostalCode.Name);
        }
        System.debug('<<<<<<LstColonia>>>>>> '+LstColonia);
        if(LstModelo!=null && !LstModelo.isEmpty()) {
            wrpPostal = new WrpPostalCode(LstModelo[0].MDRM_Postal_Code__c+'', LstModelo[0].MDRM_State__c, LstModelo[0].MDRM_Municipality__c,LstColonia);
        }
        else{
            wrpPostal = new WrpPostalCode('', '', '',LstColonia);           
        }       
        return wrpPostal;
    }

    @RemoteAction
    global static Boolean GetAccountExist(String PersonalEmail){
        Map<String,Contact> mapStrAccexit = new Map<String,Contact>();
        //and Account.MDRM_UpdateNumber__c <> 3
        for(Contact oAccExi : [Select Id,Name,Phone,MobilePhone,Email,AccountId,Account.ONTAP__PostalCode__c,Account.ONTAP__Province__c,Account.ONTAP__Municipality__c, Account.ONTAP__Colony__c,Account.ONTAP__Street__c ,Account.ONTAP__Street_Number__c
                                From Contact
                                Where Email =: PersonalEmail
                                ])
        {
            mapStrAccexit.put(oAccExi.Email , oAccExi);
        }
        return mapStrAccexit.containsKey(PersonalEmail);
    }

    @RemoteAction
    global static Contact GetAccountInfo(String PersonalEmail, String Password){
        Map<String,Contact> mapStrAccexit = new Map<String,Contact>();
        //and Account.MDRM_UpdateNumber__c <> 3
        for(Contact oAccExi : [Select Id,Name,Phone,MobilePhone,Email,AccountId,Account.ONTAP__PostalCode__c,Account.ONTAP__Province__c,Account.ONTAP__Municipality__c, Account.ONTAP__Colony__c,Account.ONTAP__Street__c ,Account.ONTAP__Street_Number__c,Account.MDRM_Password__c
                                From Contact
                                Where Email =: PersonalEmail
                                and Account.MDRM_Password__c = :Password
                                ])
        {
            mapStrAccexit.put(oAccExi.Email , oAccExi);
        }
        return mapStrAccexit.get(PersonalEmail);
    }

    @RemoteAction
    global static Boolean ToSendEmailPass(String PersonalEmail){
        Map<String,Contact> mapStrAccexit = new Map<String,Contact>();
        //and Account.MDRM_UpdateNumber__c <> 3
        for(Contact oAccExi : [Select Id,Name,Phone,MobilePhone,Email,AccountId,Account.ONTAP__PostalCode__c,Account.ONTAP__Province__c,Account.ONTAP__Municipality__c, Account.ONTAP__Colony__c,Account.ONTAP__Street__c ,Account.ONTAP__Street_Number__c,Account.MDRM_Password__c
                                From Contact
                                Where Email =: PersonalEmail
                                ])
        {
            mapStrAccexit.put(oAccExi.Email , oAccExi);
        }

        Account ObjAccount = new Account();
        ObjAccount.Id = mapStrAccexit.get(PersonalEmail).AccountId;
        ObjAccount.MDRM_SendPassword__c = true;

        Database.SaveResult srListCont = Database.Update(ObjAccount, false);
        if (!srListCont.isSuccess()){   
            return false;
        }
        else
        {
            return true;
        }
    }

    @RemoteAction
    global static String InsertForm(String JsonAccount, String JsonContact, String JsonForm, string StrBirthDay){
        system.debug('Begin Insert');
        Account ObjAccount = new Account();
        Contact ObjContact = new Contact();
        MDRM_Form__c ObjForm = new MDRM_Form__c();      

        ObjAccount = (Account)JSON.deserialize(JsonAccount, Account.class);
        ObjContact = (Contact)JSON.deserialize(JsonContact,Contact.class);
        ObjContact.Birthdate = Date.newInstance(Integer.valueOf(StrBirthDay.split('/')[2]), Integer.valueOf(StrBirthDay.split('/')[1]),Integer.valueOf(StrBirthDay.split('/')[0]));
        ObjForm = (MDRM_Form__c)JSON.deserialize(JsonForm,MDRM_Form__c.class);
		
        //ObjForm.MDRM_How_did_you_hear_about__c = 'Newspaper';

        system.debug(UserInfo.getLocale());
        system.debug(UserInfo.getUserId());


        ObjAccount.ISSM_MainContactA__c = true;
        ObjAccount.MDRM_UpdateNumber__c = 0;
        //ObjAccount.RecordtypeId = (User//Progfile Admin)? Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId() : null;
        ObjAccount.RecordtypeId = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'MDRM_Businessman'].id;
        
        Database.SaveResult srListAcc = Database.insert(ObjAccount, false);
        if(srListAcc.isSuccess())
        {
            ObjContact.AccountId = ObjAccount.Id;
            Database.SaveResult srListCont = Database.insert(ObjContact, false);
            if(srListCont.isSuccess())
            {
                ObjForm.MDRM_Account_Form__c = ObjAccount.Id;
                Database.SaveResult srListForm = Database.insert(ObjForm, true);
                if(srListForm.isSuccess())
                {
                    return 'true';
                }
                else
                {
                    for(Database.Error err : srListForm.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Form fields that affected this error: ' + err.getFields());
                    }
                    return 'malform';
                }
            }
            else
            {
                for(Database.Error err : srListCont.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Contact fields that affected this error: ' + err.getFields());
                }
                return 'malContact';
            }

        }
        else
        {
            for(Database.Error err : srListAcc.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Account fields that affected this error: ' + err.getFields());
            }
            return 'malAccount';
        }

    }

    @RemoteAction
    global static String UpdateForm(String JsonAccount, String JsonContact, String JsonForm){
        Account ObjAccount = new Account();
        Contact ObjContact = new Contact();
        MDRM_Form__c ObjForm = new MDRM_Form__c();
        Id IdForm;

        //System.debug('JsonAccount____>>>>> '+JsonAccount);
        //System.debug('JsonContact____>>>>> '+JsonContact);
        //System.debug('JJsonForm____>>>>> '+JsonForm);

        ObjAccount = (Account)JSON.deserialize(JsonAccount, Account.class);
        ObjContact = (Contact)JSON.deserialize(JsonContact, Contact.class);
        ObjForm = (MDRM_Form__c)JSON.deserialize(JsonForm, MDRM_Form__c.class);

        System.debug('ObjAccount____>>> '+ObjAccount);
        System.debug('ObjContact____>>> '+ObjContact.id);
        System.debug('ObjForm_______>>> '+ObjForm);
        //ObjAccount.MDRM_Stage__c = 'Prospect';
        
        String Idseccion = '';
        //Contact oCont = [select Id,Name,AccountId,Account.MDRM_UpdateNumber__c from Contact Where Id =:ObjContact.Id Limit 1];
        Contact oCont = [select Id,Name,AccountId from Contact Where Id =:ObjContact.Id Limit 1];
        //System.debug('oCont_______>>> '+oCont);       

        Id IdAccount = oCont.AccountId;

        Account oAcc = [Select MDRM_UpdateNumber__c, ONTAP__Account_Status__c from Account where Id =: IdAccount and MDRM_UpdateNumber__c != null limit 1];

        MDRM_Form__c oForm = [Select Id from MDRM_Form__c where MDRM_Account_Form__c =: IdAccount limit 1];
        if(oForm != null){
            IdForm = oForm.Id;
        }
        //}
        
        //ObjAccount.MDRM_UpdateNumber__c = oCont.Account.MDRM_UpdateNumber__c + 1;
        if(oAcc != null){
            ObjAccount.MDRM_UpdateNumber__c = oAcc.MDRM_UpdateNumber__c + 1;

            //Validar estados permitidos para modificacion
            if(oAcc.ONTAP__Account_Status__c <> 'Prospect Businessman' &&
                oAcc.ONTAP__Account_Status__c <> 'Unreachable' &&
                oAcc.ONTAP__Account_Status__c <> 'In waiting list' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Basic Filter' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Telephone interview' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Information Session' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Compliance' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Psychometric Tests' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Leader interview' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Document Request' &&
                oAcc.ONTAP__Account_Status__c <> 'Rejected Contract'){
                return 'BadStatus';
            }
        }else{
            ObjAccount.MDRM_UpdateNumber__c = 1;
        }

        System.debug('Times_______>>> '+ObjAccount.MDRM_UpdateNumber__c + ', Account: ' + IdAccount);
        //Validar hasta 3 modificaciones
        if(ObjAccount.MDRM_UpdateNumber__c >= 4){
            return 'TooManyTimes';
        }

        //Form data corrections
        if(ObjForm.MDRM_Currently_Employed__c == 'Yes' || ObjForm.MDRM_Currently_Employed__c == 'Si'){
            ObjForm.MDRM_Activities_last_employment__c = null;
            ObjForm.MDRM_Activities_last_employment_comment__c = null;
        }else{
            ObjForm.MDRM_Current_employment_full_time__c = null;
            ObjForm.MDRM_Current_Employment__c = null;
            ObjForm.MDRM_Current_employment_comment__c = null;
        }


        //Update Account
        ObjAccount.Id = IdAccount;
        Database.SaveResult srListAcc = Database.Update(ObjAccount, false);
        if (!srListAcc.isSuccess()){    
            return 'badAccount';
        }

        //Update Contact
        Database.SaveResult srListCont = Database.Update(ObjContact, false);
        if (!srListCont.isSuccess()){   
            return 'badContact';
        }

        //Update Form
        ObjForm.Id = IdForm;
        Database.SaveResult srListForm = Database.Update(ObjForm, false);
        if (!srListForm.isSuccess()){   
            return 'badform';
        }

        return 'true';
    }
    

    public static list<String> getPicklistLabels( String fld)
    {
        // Get the object type of the SObject.
        Schema.sObjectType objType = MDRM_Form__c.getSObjectType(); 
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
        list<String> lstSort = new list<String>();
        // Add these values to the sort list.
        for (Schema.PicklistEntry a : values)
            lstSort.add( a.getLabel() );

        return lstSort;
    }
    
    public static list<String> getPicklistValues( String fld)
    {
        // Get the object type of the SObject.
        Schema.sObjectType objType = MDRM_Form__c.getSObjectType(); 
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
        list<String> lstSort = new list<String>();
        // Add these values to the sort list.
        for (Schema.PicklistEntry a : values)
            lstSort.add( a.getValue() );

        return lstSort;
    }


    global class WrpQuestion{
        public string StrQuestionName{get;set;}
        public String StrAPIFieldQ{get;set;}
        public String StrIdQuestion{get;set;}
        public String StrIdAnswer{get;set;}
        public Boolean BlnHaveaListener{get;set;}
        public String StrNameFlow{get;set;}
        public List<String> LstAnswer{get;set;}
        public List<String> LstValues{get;set;}
        public String StrFirstshow{get;Set;}
        public String NormalNumber{get;Set;}
        public String WithJobNumber{get;Set;}
        public String SpecialCondicion{get;Set;}
		

        public WrpQuestion()
        {
            StrQuestionName = '';
            StrAPIFieldQ = '';
            StrIdQuestion = '';
            StrIdAnswer = '';
            StrNameFlow ='';
            BlnHaveaListener = false;
            LstAnswer = new list<String>();
            LstValues = new list<String>();
            StrFirstshow = '';
            NormalNumber = '';
            WithJobNumber = '';
            SpecialCondicion = '';
        }
        public WrpQuestion(     String StrPQuestionName, 
                                    String StrPAPIFieldQ,
                                    String StrPIdQuestion ,
                                    String StrPIdAnswer,
                                    Boolean BlnPHaveaListener,
                                    String StrPNameFlow,
                                    List<String> LstPAnswer,
                           			List<String> LstPValues,
                                    String StrPFirstshow,
                                    String PNormalNumber,
                                    String PWithJobNumber,
                                    String PSpecialCondicion
                                    )
        {
            LstAnswer = new List<String>();
            LstValues = new list<String>();
            StrQuestionName     = StrPQuestionName;
            StrAPIFieldQ        = StrPAPIFieldQ;
            StrIdQuestion       = StrPIdQuestion;
            StrIdAnswer         = StrPIdAnswer;
            BlnHaveaListener    = BlnPHaveaListener;
            StrNameFlow         = StrPNameFlow;
            LstAnswer.addAll(LstPAnswer);
            LstValues.addAll(LstPValues);
            StrFirstshow = StrPFirstshow;
            NormalNumber = PNormalNumber;
            WithJobNumber = PWithJobNumber;
            SpecialCondicion = PSpecialCondicion;
        }
    }

    global class WrpPostalCode{
        public string StrPostalCode{get;set;}
        public String StrEstado{get;set;}
        public String StrMunicipio{get;set;}
        public list<String> LstColonia{get;Set;}

        public WrpPostalCode()
        {
            StrPostalCode = '';
            StrEstado = '';
            StrMunicipio = '';
            LstColonia = new List<String>();
        }

        public WrpPostalCode(String StrPPostalCode, String StrPEstado, String StrPMunicipio, List<String> LstPColonia)
        {
            StrPostalCode = StrPPostalCode;
            StrEstado = StrPEstado;
            StrMunicipio = StrPMunicipio;
            LstColonia = new List<String>();
            LstColonia.addAll(LstPColonia);
        }
    }
/** End form code   **/ 
    
/** Vision code   **/     
    public Account Expansor {get;set;}
    public Account Businessman {get;set;}
    public Contact Con {get;set;}
    public List<MDRM_Modelorama_Performance__c> lstModelorama_Performance {get;set;}
    public List<MDRM_Modelorama_Performance__c> lstModelorama_PerformanceFault {get;set;}
    public Map<Decimal, dataPerformance> mapDataPerformance {get;set;}
    public List<dataPerformance> lstDataPerformance {get;set;}
    public boolean noError{get;set;}
    public String selectedYear {get;set;}
    public String selectedZ001 {get;set;}
    public String selectedZ019 {get;set;}
    public List<SelectOption> lstZ019 {get;set;}
        
    
    public void init(){
        noError = false;
        Businessman = new Account();        
        lstZ019 = new List<SelectOption>();
        lstZ019.add( genPickListLabeldefaultVal( SObjectType.MDRM_Businessman_Expansor__c.getLabel() ));
        
        /*
        selectedZ001 = apexpages.currentpage().getparameters().get('MDRM_Z001');
        selectedZ019 = apexpages.currentpage().getparameters().get('MDRM_Z019');
        selectedYear = apexpages.currentpage().getparameters().get('MDRM_Year');
		*/
        
        //selectedZ001 = apexpages.currentpage().getHeaders().get('MDRM_Z001');
        selectedZ019  = apexpages.currentpage().getHeaders().get('MDRM_Z019');
        //selectedYear  = apexpages.currentpage().getHeaders().get('MDRM_Year');
        
       
        if(selectedZ019 != null){
            genInfoBusinessman();
            genConInfo();
            genInformation();
            genZ019PickList();
        }
        
    }
    
    public void genZ019PickList(){
        lstZ019 = new list<SelectOption>();
        try{
            for(Account z019 : [SELECT Z019__c FROM Account WHERE ParentId =: Businessman.id ORDER BY Z019__c ASC  ]){
                lstZ019.add(new SelectOption( String.valueOf(z019.Z019__c) ,String.valueOf(z019.Z019__c) ));
            }
        }catch(Exception ex){
            system.debug('ERROR: '+ex);
        }
        lstZ019.add(new SelectOption( label.MDRM_All , label.MDRM_All ));
        selectedZ019 = '';
    }
    
    public void genInfoBusinessman(){
        try{
            String strOrignData = selectedZ019 == Label.MDRM_All ? ' WHERE Parent.Z01__c =: selectedZ01 ' : ' WHERE Z019__c =: selectedZ019 ' ;
            Expansor = database.query(' SELECT ID,'+
                                      ' Z019__c, '+
                                      ' Parent.Z001__c, '+         
                                      ' ParentId '+
                                      ' FROM Account  '+
                                      strOrignData+
                                      ' LIMIT 1');
            
            Businessman = [SELECT ID,
                           Name, 
                           Z001__c,
                           MDRM_RFC__c,
                           Agri_CURP__c,
                           ONTAP__Street__c,
                           ONTAP__Street_Number__c,
                           ONTAP__PostalCode__c,
                           ONTAP__Province__c,
                           ONTAP__Municipality__c,
                           ONTAP__Email__c 
                           FROM Account 
                           WHERE id =: Expansor.ParentId 
                           LIMIT 1];
            selectedZ001 = Businessman.Z001__c;            
        }catch(Exception ex){
            system.debug('ERROR: '+ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.MDRM_UserNotFound ));
            noError = false;
        }
        
    }
    
    public void genConInfo(){
        try{
            Con = [SELECT Id,Name,Phone FROM Contact WHERE AccountId =: Businessman.id  LIMIT 1];
        }catch(Exception ex){
            System.debug('Contact ERROR: '+ex);
            con = new Contact();
        }
    }
    
    public void genInformation(){
        try{
            lstModelorama_Performance = new List<MDRM_Modelorama_Performance__c>();
            lstModelorama_PerformanceFault = new List<MDRM_Modelorama_Performance__c>();
            mapDataPerformance = new Map<Decimal, dataPerformance>();
            lstDataPerformance = new List<dataPerformance>();
            
            if(  selectedYear != null && selectedYear.isNumeric() ){
                
                integer intSelectedYear = selectedYear == '' ? 0 : Integer.valueOf( selectedYear );
                String BusinessmanId = Businessman.id;
                String strQuery = 'SELECT id,'+
                    'Name,'+
                    'MDRM_Actual_Volume__c,'+
                    'MDRM_Bond__c,'+
                    'MDRM_Businessman_Expansor__c,'+
                    'MDRM_Businessman_Expansor__r.MDRM_Expansor__r.Z019__c,'+
                    'MDRM_Challenges_Monthly__c,'+
                    'MDRM_Execution__c,'+
                    'MDRM_Month__c,'+
                    'MDRM_Month_fx__c,'+
                    'MDRM_No_Selling_Alcohol_to_Minors__c,'+
                    'MDRM_Share__c,'+
                    'MDRM_Year__c '+
                    'FROM MDRM_Modelorama_Performance__c '+
                    'WHERE MDRM_Businessman_Expansor__r.MDRM_Expansor__r.ParentId =: BusinessmanId ' +
                    'AND MDRM_Year__c =: intSelectedYear  ';
                String strOrderQuery = 'ORDER  BY MDRM_Z019__c ASC, MDRM_Month__c ASC  NULLS LAST LIMIT 12';
                String strSelectedZ019 = selectedZ019 == label.MDRM_All ? '' : 'AND MDRM_Businessman_Expansor__r.MDRM_Expansor__r.Z019__c =: selectedZ019 ' ;
                
                for(Decimal i=1; i<=12; i++){
                    mapDataPerformance.put(i, new dataPerformance( i , 0 ));
                }
                
                lstModelorama_Performance = Database.query( strQuery + strSelectedZ019 + strOrderQuery );
                
                
                lstModelorama_PerformanceFault = Database.query( strQuery + strSelectedZ019 + 'AND MDRM_No_Selling_Alcohol_to_Minors__c != null  ' + strOrderQuery );
                
                for(MDRM_Modelorama_Performance__c MP : lstModelorama_Performance ){
                    mapDataPerformance.put( MP.MDRM_Month__c , new dataPerformance( MP.MDRM_Month__c, mapDataPerformance.get(MP.MDRM_Month__c).Bonus + MP.MDRM_Bond__c) ) ;
                }
                
                
                for(dataPerformance DP : mapDataPerformance.values() ){
                    lstDataPerformance.add( DP );
                }
            }
            noError = true;	
        }catch(Exception ex){
            system.debug('ERROR: '+ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.MDRM_UserNotFound ));
            noError = false;
        }
        
    }
    
    public Class dataPerformance{
        public Decimal Month {get;set;}
        public Decimal Bonus {get;set;}
        public String txtMonth {get;set;}
        
        public dataPerformance(Decimal Month, Decimal Bonus){
            this.Month = Month;
            this.Bonus = Bonus;
            this.txtMonth = monthDecimalToText(Month);
        }
        public String monthDecimalToText(Decimal month){
            if(month == 1){
                return Label.MDRM_January;
            }else if(month == 2){
                return Label.MDRM_February;
            }else if(month == 3){
                return Label.MDRM_March;
            }else if(month == 4){
                return Label.MDRM_Abril;
            }else if(month == 5){
                return Label.MDRM_May;
            }else if(month == 6){
                return Label.MDRM_June;
            }else if(month == 7){
                return Label.MDRM_July;
            }else if(month == 8){
                return Label.MDRM_August;
            }else if(month == 9){
                return Label.MDRM_September;
            }else if(month == 10){
                return Label.MDRM_October;
            }else if(month == 11){
                return Label.MDRM_November;
            }else if(month == 12){
                return Label.MDRM_December;
            }
            return '';
        }
    }
    
    public SelectOption genPickListLabeldefaultVal(String val){
        String defaultVal = '-- ' + Label.MDRM_Select + ' ' + val + ' --';
        return new SelectOption( '' , defaultVal );
    }
    
    public List<SelectOption> getYears() {
        List<SelectOption> options = new List<SelectOption>();
        options.add( genPickListLabeldefaultVal(Schema.SObjectType.MDRM_Modelorama_Performance__c.fields.getMap().get('MDRM_Year__c').getDescribe().getLabel()) );
        Integer Year = system.today().year();
        for( Integer y = Year; y > Year-5; y-- ){
            options.add(new SelectOption( String.valueOf(y) ,String.valueOf(y)));
        }
        return options;
    }
/** End vision code   **/  
}