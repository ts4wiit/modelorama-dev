/**
 * Test class for AllMobileDeviceUserBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserBatchTest {

	/**
	 * Method to test AllMobileDeviceUserBatchClass.
	 */
	static testMethod void testAllMobileDeviceUserBatchClass() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();

		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c();
		insert objAllMobileDeviceUser;

		//Testing method.
		AllMobileDeviceUserBatchClass objAllMobileDeviceUserBatchClass = new AllMobileDeviceUserBatchClass();
		Id idObjAllMobileDeviceUserBatchClass = Database.executeBatch(objAllMobileDeviceUserBatchClass);

		//Stop test.
		Test.stopTest();
	}
}