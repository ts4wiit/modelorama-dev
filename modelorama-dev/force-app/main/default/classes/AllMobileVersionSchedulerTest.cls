/**
 * Test class for AllMobileVersionSchedulerClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileVersionSchedulerTest {

	/**
	 * Method to test execute method.
	 */
	static testMethod void testExecute() {

		//Set time.
		String strScheduleTimeVersion = '0 57 * * * ?';

		//Start test.
		Test.startTest();

		//Schedule Version.
		AllMobileVersionSchedulerClass objAllMobileVersionSchedulerClass = new AllMobileVersionSchedulerClass();
		System.schedule('jobVersion', strScheduleTimeVersion, objAllMobileVersionSchedulerClass);

		//Stop test.
		Test.stopTest();
	}
}