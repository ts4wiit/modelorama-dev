/**
 * This class contains Object Classes to be synchronized with Heroku and methods to callout Heroku web services.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileSyncObjectsClass {

	/**
	 * This class contains the attributes for the RouteAppVersion of Heroku.
	 */
	public class AllMobileRouteAppVersionHerokuObjectClass {

		//Fields of RouteAppVersion table of Heroku.
		public Integer routeappversionid;
		public String route;
		public String version_id;
		public Date validstart;
		public Date validend;
		public Boolean softdeleteflag;
		public DateTime dtlastmodifieddate;
	}

	/**
	 * This class contains the attributes to deserialize the RouteAppVersion Heroku object.
	 */
	public class AllMobileRouteAppVersionHerokuObjectClassDeserialize {

		//Fields of RouteAppVersion table of Heroku.
		public Integer routeappversionid;
		public String route;
		public String version_id;
		public DateTime validstart;
		public DateTime validend;
		public Boolean softdeleteflag;
		public DateTime dtlastmodifieddate;
	}

	/**
	 * This class contains the attributes for the Version of Heroku.
	 */
	public class AllMobileVersionHerokuObjectClass {

		//Fields of Version table of Heroku.
		public String version_id;
		public Integer application_id;
		public Boolean softdeleteflag;
		public DateTime dtlastmodifieddate;

	}

	/**
	 * This class contains the attributes for the Application of Heroku.
	 */
	public class AllMobileApplicationHerokuObjectClass {

		//Fields of Version table of Heroku.
		public Integer application_id;
		public String name;
		public String ship_type;
		public Integer levelvalidation;
		public DateTime dtlastmodifieddate;
		public Boolean softdeleteflag;

	}

	/**
	 * This class contains the attributes for the Device User of Heroku.
	 */
	public class AllMobileDeviceUserHerokuObjectClass {

		//Fields of Device User table of Heroku.
		public String id;
		public String user_type;
		public String vkbur;
		public String user_name;
		public Boolean isactive;
		public String route;
		public String email;
		public String ldap_id;
		public String password;
		public Boolean softdeleteflag;
		public DateTime dtlastmodifieddate;
	}

	/**
	 * This class contains the attributes for the Cat User Type of Heroku.
	 */
	public class AllMobileCatUserTypeHerokuObjectClass {

		//Fields of Cat User Type table of Heroku.
		public String user_type;
		public String user_type_desc;
		public Boolean admon_externa;
		public String permisos;
		public Boolean softdeleteflag;
		public DateTime dtlastmodifieddate;
	}

	/**
	 * This method insert a List of RouteAppVersion objects into Heroku routeappversion table.
	 *
	 * @param objAllMobileRouteAppVersionHerokuObjectClass 	AllMobileRouteAppVersionHerokuObjectClass
	 * @return String
	 */
	public static String calloutInsertRouteAppVersionIntoHeroku(AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_INSERT);
		String strJsonObjAllMobileRouteAppVersionHerokuObjectClass = JSON.serialize(objAllMobileRouteAppVersionHerokuObjectClass);

		//Set Http Request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileRouteAppVersionHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method update a List of RouteAppVersion objects into Heroku routeappversion table.
	 *
	 * @param objAllMobileRouteAppVersionHerokuObjectClass	AllMobileRouteAppVersionHerokuObjectClass
	 * @return String.
	 */
	public static String calloutUpdateRouteAppVersionIntoHeroku(AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_UPDATE);
		String strJsonObjAllMobileRouteAppVersionHerokuObjectClass = JSON.serialize(objAllMobileRouteAppVersionHerokuObjectClass);
		String strRouteAppVersionIdHeroku = String.valueOf(objAllMobileRouteAppVersionHerokuObjectClass.routeappversionid);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c + strRouteAppVersionIdHeroku);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileRouteAppVersionHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method callout a GET request to return a List of RouteAppVersion records from Heroku.
	 *
	 * @return String.
	 */
	public static String calloutGetAllRouteAppVersionFromHeroku() {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_READ_ALL);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}

	/**
	 * This method insert Version records into Heroku version table.
	 *
	 * @param objAllMobileVersionHerokuObjectClass  AllMobileVersionHerokuObjectClass
	 * @return String.
	 */
	public static String calloutInsertVersionIntoHeroku(AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_INSERT);
		String strJsonObjAllMobileVersionHerokuObjectClass = JSON.serialize(objAllMobileVersionHerokuObjectClass);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileVersionHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method update Version records in Heroku version table.
	 *
	 * @param objAllMobileVersionHerokuObjectClass	AllMobileVersionHerokuObjectClass
	 * @return String.
	 */
	public static String calloutUpdateVersionIntoHeroku(AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_UPDATE);
		String strJsonObjAllMobileVersionHerokuObjectClass = JSON.serialize(objAllMobileVersionHerokuObjectClass);
		String strVersionIdHeroku = String.valueOf(objAllMobileVersionHerokuObjectClass.version_id);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c + strVersionIdHeroku);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileVersionHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method returns a List of Version recods from Heroku version table.
	 *
	 * @return String.
	 */
	public static String calloutReadAllVersionFromHeroku() {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_READ_ALL);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}

	/**
	 * This method insert Application records into Heroku application table.
	 *
	 * @param objAllMobileApplicationHerokuObjectClass	AllMobileApplicationHerokuObjectClass
	 * @return String.
	 */
	public static String calloutInsertApplicationIntoHeroku(AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_INSERT);
		String strJsonObjAllMobileApplicationHerokuObjectClass = JSON.serialize(objAllMobileApplicationHerokuObjectClass);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileApplicationHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method is used to update the Application records in Heroku using the PUT method in the callout.
	 *
	 * @param objAllMobileApplicationHerokuObjectClass  AllMobileApplicationHerokuObjectClass
	 * @return String.
	 */
	public static String calloutUpdateApplicationIntoHeroku(AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_UPDATE);
		String strJsonObjAllMobileApplicationHerokuObjectClass = JSON.serialize(objAllMobileApplicationHerokuObjectClass);
		String strApplicationIdHeroku = String.valueOf(objAllMobileApplicationHerokuObjectClass.application_id);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c + strApplicationIdHeroku);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileApplicationHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method is used to get the Application recorda using the GET method.
	 *
	 * @return String.
	 */
	public static String calloutReadAllApplicationFromHeroku() {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_READ_ALL);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}

	/**
	 * This method insert a List of DeviceUser objects into Heroku device_user table.
	 *
	 * @param objAllMobileDeviceUserHerokuObjectClass 	AllMobileDeviceUserHerokuObjectClass
	 * @return String
	 */
	public static String calloutInsertDeviceUserIntoHeroku(AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_INSERT);
		String strJsonObjAllMobileDeviceUserHerokuObjectClass = JSON.serialize(objAllMobileDeviceUserHerokuObjectClass);

		//Set Http Request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileDeviceUserHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method update a List of DeviceUser objects into Heroku device_user table.
	 *
	 * @param objAllMobileDeviceUserHerokuObjectClass	AllMobileDeviceUserHerokuObjectClass
	 * @return String.
	 */
	public static String calloutUpdateDeviceUserIntoHeroku(AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_UPDATE);
		String strJsonObjAllMobileDeviceUserHerokuObjectClass = JSON.serialize(objAllMobileDeviceUserHerokuObjectClass);
		String strDeviceUserIdHeroku = String.valueOf(objAllMobileDeviceUserHerokuObjectClass.id);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c + strDeviceUserIdHeroku);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileDeviceUserHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method callout a GET request to return a List of DeviceUser records from Heroku.
	 *
	 * @return String.
	 */
	public static String calloutGetAllDeviceUserFromHeroku() {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_READ_ALL);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}

	/**
	 * This method callout a POST request to return a List of String encrypted passwords.
	 *
	 * @return String.
	 */
	public static String encryptPasswordDeviceUser(String strPasswordClearText) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_ENCRYPT);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strPasswordClearText);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}

	/**
	 * This method insert a List of CatUserType objects into Heroku cat_user_type table.
	 *
	 * @param objAllMobileCatUserTypeHerokuObjectClass 	AllMobileCatUserTypeHerokuObjectClass
	 * @return String
	 */
	public static String calloutInsertCatUserTypeIntoHeroku(AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_INSERT);
		String strJsonObjAllMobileCatUserTypeHerokuObjectClass = JSON.serialize(objAllMobileCatUserTypeHerokuObjectClass);

		//Set Http Request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileCatUserTypeHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method update a List of CatUserType objects into Heroku cat_user_type table.
	 *
	 * @param objAllMobileCatUserTypeHerokuObjectClass	AllMobileCatUserTypeHerokuObjectClass
	 * @return String.
	 */
	public static String calloutUpdateCatUserTypeIntoHeroku(AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass) {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_UPDATE);
		String strJsonObjAllMobileCatUserTypeHerokuObjectClass = JSON.serialize(objAllMobileCatUserTypeHerokuObjectClass);
		String strCatUserTypeIdHeroku = String.valueOf(objAllMobileCatUserTypeHerokuObjectClass.user_type);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c + strCatUserTypeIdHeroku);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		objHttpRequest.setBody(strJsonObjAllMobileCatUserTypeHerokuObjectClass);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getStatusCode() + AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
	}

	/**
	 * This method callout a GET request to return a List of CatUserType records from Heroku.
	 *
	 * @return String.
	 */
	public static String calloutGetAllCatUserTypeFromHeroku() {

		//Variables.
		AllMobileEndPointConfigurationsWS__c objAllMobileEndPointConfigurationsWS = AllMobileEndPointConfigurationsWS__c.getAll().get(AllMobileStaticVariablesClass.STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_READ_ALL);

		//Set Http request.
		Http objHttp = new Http();
		HttpRequest objHttpRequest = new HttpRequest();
		objHttpRequest.setEndpoint(objAllMobileEndPointConfigurationsWS.AllMobileEndPoint__c);
		objHttpRequest.setMethod(objAllMobileEndPointConfigurationsWS.AllMobileMethod__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderAccessTokenValue__c);
		objHttpRequest.setHeader(objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeKeyName__c, objAllMobileEndPointConfigurationsWS.AllMobileHeaderContentTypeValue__c);
		HttpResponse objHttpResponse = objHttp.send(objHttpRequest);
		return objHttpResponse.getBody();
	}
}