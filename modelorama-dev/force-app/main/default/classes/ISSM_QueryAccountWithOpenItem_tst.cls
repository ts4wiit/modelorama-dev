/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_QueryAccountWithOpenItem_tst {

    static testMethod void TestValidateDebitCredit() {
       
		//Generamos cuenta para los filtros
		List<Account> lstAccount = new List<Account>();
		String RecordTypeAccountId = [SELECT Id FROM RecordType WHERE DeveloperName='Account' LIMIT 1].Id;       
        Account objCreateAccount  = new Account();
        objCreateAccount.Name ='EjemploCuenta';
        objCreateAccount.ONTAP__Classification__c ='Botella Abierta';
        //objCreateAccount.ISSM_RegionalSalesDivision__c =objCreateRegionalDiv.Id;
        //objCreateAccount.ISSM_SalesOffice__c =ObjTypeSalesOffice.Id;
        //objCreateAccount.ISSM_SalesOrg__c =objTypeSalesOrg.Id;
        objCreateAccount.RecordTypeId = RecordTypeAccountId;
       
        
        
		//Creamos ONTAP__OpenItem__c   con  Deuda
		ISSM_OpenItemB__c objOpenItemDebit = new ISSM_OpenItemB__c();
	        objOpenItemDebit.ISSM_Account__c = objCreateAccount.Id;
	        objOpenItemDebit.ISSM_DueDate__c = System.today()-35;
	        objOpenItemDebit.ISSM_Amounts__c =15000;
	        objOpenItemDebit.ISSM_Debit_Credit__c = 'S';
	        insert objOpenItemDebit;
	    
	   	//Creamos ONTAP__OpenItem__c  con  Abono 
		ISSM_OpenItemB__c objOpenItemCredit = new ISSM_OpenItemB__c();
	        objOpenItemCredit.ISSM_Account__c = objCreateAccount.Id;
	        objOpenItemCredit.ISSM_DueDate__c = System.today()-35;
	        objOpenItemCredit.ISSM_Amounts__c =1500;
	        objOpenItemCredit.ISSM_Debit_Credit__c = 'H';
	        insert objOpenItemCredit;
        
        lstAccount.add(objCreateAccount);
        insert lstAccount;
		Database.BatchableContext dbBC;
        Test.startTest();
			ISSM_QueryAccountWithOpenItem_bch objQueryAccountWithOpenItem = new ISSM_QueryAccountWithOpenItem_bch();
            objQueryAccountWithOpenItem.start(dbBC);
            objQueryAccountWithOpenItem.execute(dbBC,lstAccount); 
            objQueryAccountWithOpenItem.finish(dbBC);
        Test.stopTest();
        
    }
        static testMethod void TestException() {
       
		//Generamos cuenta para los filtros
		List<Account> lstAccount = new List<Account>();
		
		Database.BatchableContext dbBC;
        Test.startTest();
			ISSM_QueryAccountWithOpenItem_bch objQueryAccountWithOpenItem = new ISSM_QueryAccountWithOpenItem_bch();
			objQueryAccountWithOpenItem.query =null;
            objQueryAccountWithOpenItem.start(dbBC);
            objQueryAccountWithOpenItem.execute(dbBC,lstAccount); 
        Test.stopTest();
        
    }
}