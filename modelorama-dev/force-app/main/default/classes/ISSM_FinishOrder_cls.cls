/****************************************************************************************************
    General Information
    -------------------
    Author:     Luis Licona
    email:      llicona@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the End call process for OnCall

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       12-Sept-2017    Luis Licona                  Class Creation
    1.1       13-Febr-2018    Luis Licona                  Generación de Id externo para OrderItems
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_FinishOrder_cls {
    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
    public static ISSM_CreateOrder_ctr CLSWSCO      = new ISSM_CreateOrder_ctr();
    public static Map<String,String> MapAutonumber  = new Map<String,String>();
    public static String[] LstMnsgSucc = new List<String>();
    public static String StrTelesalesRoute = '';
    public static ONTAP__Order_Item__c ObjItem = new ONTAP__Order_Item__c();
    public static String StrRequest;
    public static String StrRequestEB;
    public static Datetime ReqDate;
    public static String StrPreAutNum;
    public ISSM_FinishOrder_cls() {}

    public static List<String> EndCall(String IdOrder, String IdAcc, String IdCall,ONTAP__Order_Item__c[] LstOItem, 
                                        ISSM_NewOrder_ctr.PriceCondition[] LstPC, String CallDuration,
                                        ISSM_Bonus__c[] LstBonus,String StrPMethod,EmptyBalanceB__c[] LstEBGroup,
                                        ISSM_DeserializePricingMotor_cls.OrderItems[] LstDealsReceived,
                                        ISSM_DeserializePricingMotor_cls.OrderItems[] LstOrderItemsDealsReceived,
                                        String strCreditTypeReceived,
                                        ISSM_DeserializePricingMotor_cls.OrderItems[] LstDealsCashCreditReceived,
                                        ISSM_DeserializePricingMotor_cls.OrderItems[] LstDealsCreditReceived,
                                        String StrCustomerComments,Map<Integer,String> ZTPMMap, Map<String,Integer> comboId_map, 
                                        String DocType,String Tel02, String Tel03, String Tel04, String Email01, Boolean ChkTel01, 
                                        Boolean ChkTel02, Boolean ChkTel03, Boolean ChkTel04, 
                                        Boolean ChkEmail, List<String> selectedPicklistValues, String counter,String suggestedCovPercentage,String pushCovPercentage){

        System.debug('--------comboId_map-------- = ' + comboId_map); 

        Integer counter_Int = !String.isBlank(counter) ? Integer.valueOf(counter) : 1;
        List<ONTAP__Order_Item__c> dummy_lst = new List<ONTAP__Order_Item__c>();

        //SI es tipo C dependiendo de la seleccion manda el parametro D (Contado,efectivo) C Crédito
        if(StrPMethod == Label.ISSM_CTC)
            StrPMethod = strCreditTypeReceived == Label.ISSM_CTA ? Label.ISSM_CTD : Label.ISSM_CTC;

        String strResponseDeals = '';
        ISSM_Wrapper_cls.WrpDealsMaterial objDealsWrapp;
        List<ISSM_Wrapper_cls.WrpDealsMaterial> lstWrapper = new List<ISSM_Wrapper_cls.WrpDealsMaterial>();
        ISSM_Wrapper_cls.WrpDealsSend objWrpDealsSend;
        Boolean blnFlagDealsSend = false;
       //************************************************DEALS HMDH **********************************************************
       
        System.debug('**********LstDealsReceived : '+LstDealsReceived);
        System.debug('**********LstOrderItemsDealsReceived : '+LstOrderItemsDealsReceived);

        if(LstDealsReceived!=null  && (!LstDealsReceived.isEmpty() || !LstOrderItemsDealsReceived.isEmpty())){
            System.debug('SI ENTRO DEALS');
            lstWrapper.addAll(ISSM_NewOrder_ctr.DealsProcessInfo(LstDealsReceived,LstOrderItemsDealsReceived,strCreditTypeReceived,LstDealsCashCreditReceived,LstDealsCreditReceived));
            System.debug('*********lstWrapper : '+lstWrapper);
            blnFlagDealsSend = !lstWrapper.isEmpty() && lstWrapper != null ? true : false;
           // blnFlagDealsSend = true;
        }else{
            strResponseDeals = Label.ISSM_DealsMnsg1;
        }
            
        
        //************************************************DEALS HMDH **********************************************************
        String StrIdOrderSap;
        String StrIdOrderSapEB;
        ISSM_DeserializeCreateOrder_cls ObjDesCO;
        ISSM_DeserializeCreateOrder_cls ObjDesCOEB; 
        String StrDocType = '';
        ONTAP__EmptyBalance__c[] LstRetEB = new List<ONTAP__EmptyBalance__c>();
        String StrAutoNumber;
        String StrPurchaseNumber;
        Boolean BlnflgError=false;
        Boolean BlnflgWiD=false;
        try{        
            StrAutoNumber     = DocType == Label.ISSM_Y029 ? InvoicingPONumber(DocType) : '0';
            StrPurchaseNumber = CreatePONumber(IdOrder);
            //StrPurchaseNumber = DocType != Label.ISSM_Y029 ? CreatePONumber(IdOrder)    : StrAutoNumber.leftPad(10, '0');
            //INVOKE SERVICE CREATEORDER
            ObjDesCO    = !LstOItem.isEmpty()   ? CLSWSCO.CallOutCreateOrder(IdOrder,IdAcc,LstOItem,LstPC,StrPMethod,null,null,StrPurchaseNumber,ZTPMMap,comboId_map,counter_Int) : null;
            dummy_lst = CLSWSCO.dummy_Lst;
            System.debug('dummy_lst after asign values = ' + dummy_lst);
            ObjDesCOEB  = !LstEBGroup.isEmpty() && String.isNotBlank(ObjDesCO.orderID) ? CLSWSCO.CallOutCreateOrder(IdOrder,IdAcc,LstOItem,null,StrPMethod,Label.ISSM_DocTypeEB,LstEBGroup,StrPurchaseNumber,null,null,counter_Int) : null;
            System.debug('################## ObjDesCO :' + ObjDesCO);
            System.debug('################## ObjDesCOEB :' + ObjDesCOEB);


        }catch(Exception ex){
            System.debug('ex----> '+String.ValueOf(ex.getMessage()));
            LstMnsgSucc.add(''+ex.getMessage());
            BlnflgError=true;
        }

        if(!Test.isRunningTest() && !BlnflgError){ 
            StrIdOrderSap   = ObjDesCO   != null ? ObjDesCO.orderID      : null;
            StrIdOrderSapEB = ObjDesCOEB != null ? ObjDesCOEB.orderID    : Label.ISSM_NoIdSAP;
            ReqDate         = ObjDesCO.ReqDate != null ? Datetime.parse(ObjDesCO.ReqDate.format()) : Datetime.parse(System.now().format());
        }else if(Test.isRunningTest()){
            StrIdOrderSap    = String.valueOf(Math.round(Math.random()*10000000));
            StrIdOrderSapEB  = String.valueOf(Math.round(Math.random()*10000000));
            ReqDate = Datetime.parse(System.now().format());
        }
        //si no tiene id Sap se prende flag
        BlnflgWiD = String.isBlank(StrIdOrderSap) ? true : false;
        //*************** DEALS ************
        System.debug('*************** blnFlagDealsSend : '+blnFlagDealsSend);
        if(blnFlagDealsSend){
            ISSM_DeserializeCreateOrder_cls ObjDeserealized;
            objWrpDealsSend = new ISSM_Wrapper_cls.WrpDealsSend(StrIdOrderSap,lstWrapper);   
            
            String StrSerializeJsonDeals = JSON.serializePretty(objWrpDealsSend);   
            System.debug('######JSON DEALS : \n '+StrSerializeJsonDeals);
            ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
            List<ISSM_CallOutService_ctr.WprResponseService> response = CTR.SendRequest(StrSerializeJsonDeals, Label.ISSM_ConfigWSDeals);
            System.debug('ANTES DE PONER DEALS EXITSO');
            strResponseDeals = Label.ISSM_DealsMnsg2; 
        }else{
              strResponseDeals = Label.ISSM_DealsMnsg1; 
        }     
        //************** DEALS ***************
        System.debug('###IdCall1### '+IdCall);
        if(String.isNotBlank(IdCall)){
            ONCALL__Call__c ObjCall              = CTRSOQL.getCallbyId(IdCall);
            //Se ejecuta el calculo de la duración de la llamada en caso de fallar el cronometro
            ObjCall.ISSM_StartCallTime__c = ObjCall.ISSM_StartCallTime__c != null ? ObjCall.ISSM_StartCallTime__c : Datetime.parse(System.now().format());
            Decimal CallDurationFF = Decimal.valueOf((Datetime.now().getTime()-ObjCall.ISSM_StartCallTime__c.getTime()) / 1000);
            CallDurationFF = (CallDurationFF/60).round();
            ///////////
            ObjCall.ISSM_EndCallTime__c          = Datetime.parse(System.now().format());
            ObjCall.ISSM_CallDuration__c         = String.isNotBlank(CallDuration) ? Decimal.valueOf(CallDuration) : CallDurationFF;
            StrTelesalesRoute                    = ObjCall.ISSM_SalesRoute__c;
            ObjCall.ISSM_CallEffectiveness__c    = Label.ISSM_CallSubStatus;   
            ObjCall.ISSM_CustomerComments__c     = StrCustomerComments; 
            ObjCall.ISSM_CallResult__c           = BlnflgError || BlnflgWiD ? Label.ISSM_OrderWOSAP : Label.ISSM_EffectiveCall; 
            ObjCall.ISSM_SuggestedTimeCalling__c = null;  
            ObjCall.ISSM_ValidateOrder__c        = true;
            ObjCall.Id=IdCall;  
            update ObjCall;

            System.debug('###IdCall2### '+ObjCall.Id);
            Account objAcc = CTRSOQL.getAccountbyId(IdAcc);
            objAcc.ONCALL__Phone__c                  = Tel02;
            objAcc.ONTAP__Secondary_Phone__c         = Tel03;
            objAcc.ONTAP__Mobile_Phone__c            = Tel04;
            objAcc.ONTAP__Email__c                   = Email01;
            objAcc.ISSM_MainContactA__c              = ChkTel01;
            objAcc.ISSM_MainContactB__c              = ChkTel02;
            objAcc.ISSM_MainContactC__c              = ChkTel03;
            objAcc.ISSM_MainContactD__c              = ChkTel04;
            objAcc.ISSM_MainContactE__c              = ChkEmail;
            objAcc.ISSM_FidelityPercentagePush__c    = Decimal.valueOf(pushCovPercentage);
          	objAcc.ISSM_FidelityPercentageSug__c     = Decimal.valueOf(suggestedCovPercentage);
            
            objAcc.ONTAP__Preferred_Service_Hours__c = String.join(selectedPicklistValues, ';');
            
            if(StrCustomerComments != null && StrCustomerComments != ''){                    
                objAcc.ISSM_CustomerComments__c  = StrCustomerComments + '\n\n' + Label.ISSM_Date +' '+ 
                                                   + System.now().format(Label.ISSM_FormatDate2) 
                                                   + '\n' + Label.ISSM_OriginCall + ': ' + UserInfo.getName();
            }
            if(counter_Int == 1)
                update objAcc;

            System.debug('objAcc = ' + objAcc); 
        }
        
        StrPreAutNum = DocType != Label.ISSM_Y029 ? MapAutonumber.get(StrPurchaseNumber) : StrAutoNumber;
        ONTAP__Order__c ObjOrder             = CTRSOQL.getOrderbyId(IdOrder);
        ObjOrder.ONCALL__SAP_Order_Number__c = StrIdOrderSap;
        ObjOrder.ONTAP__OrderStatus__c       = BlnflgError || BlnflgWiD ? Label.ISSM_OrderStatus3 : Label.ISSM_OrderStatus;
        ObjOrder.ONTAP__DeliveryDate__c      = ReqDate;
        ObjOrder.ONCALL__Call__c             = IdCall;
        ObjOrder.ISSM_PayloadOrder__c        = String.isBlank(String.valueOf(ObjDesCO)) ? String.ValueOf(LstMnsgSucc) : String.valueOf(ObjDesCO);
        ObjOrder.ISSM_PayloadOrderEB__c      = String.valueOf(ObjDesCOEB);
        ObjOrder.ISSM_IdEmptyBalance__c      = StrIdOrderSapEB;
        ObjOrder.ISSM_Autonumber__c          = String.isBlank(ObjOrder.ISSM_Autonumber__c) ? StrPreAutNum : ObjOrder.ISSM_Autonumber__c;
        ObjOrder.ONTAP__PurchaseOrder__c     = String.isBlank(ObjOrder.ONTAP__PurchaseOrder__c) ? StrPurchaseNumber : ObjOrder.ONTAP__PurchaseOrder__c;
        ObjOrder.RecordTypeId                = ObjOrder.Owner.Profile.Name == Label.ISSM_Profile2  && !BlnflgWiD ? CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order') : ObjOrder.RecordTypeId;
        ObjOrder.ISSM_EndOrder__c            = ObjOrder.Owner.Profile.Name == Label.ISSM_Profile2  && !BlnflgWiD ? true : false;
        update ObjOrder;
        
        
        
        if(ObjOrder.ONCALL__SAP_Order_Number__c != null && !LstBonus.isEmpty() && String.isNotBlank(IdCall)){
            Set<String> EBSapOrder = new Set<String>();
            for(ISSM_Bonus__c b : LstBonus)
                EBSapOrder.add(b.ISSM_SAPOrderNumber__c);
            
            ISSM_Bonus__c[] ObjBonus = CTRSOQL.getBonusById(IdAcc,EBSapOrder);

            if(!ObjBonus.isEmpty())
                LstBonus.addall(ObjBonus);
            
            for(ISSM_Bonus__c  ObjB: LstBonus){
                ObjB.ISSM_BonusUsageTime__c = ObjOrder.ONTAP__DeliveryDate__c;                    
                ObjB.ISSM_TelesalesRoute__c = StrTelesalesRoute;
                ObjB.ISSM_IsBonusApplied__c = true;
            }   
            if(counter_Int == 1)
                update LstBonus;
           
        }

        LstOItem.addAll(dummy_lst);
        System.debug('###LISTADOORDERITEMS###');
        System.debug('*****LstOItem----> '+LstOItem);
        
        //se crea id externo para no duplicar items por el área de reparto
        for(ONTAP__Order_Item__c objItem : LstOItem){
            //objItem.ONCALL__SAP_Order_and_Order_Item_Number__c = StrIdOrderSap+objItem.ONCALL__SFDC_Suggested_Order_Item_Number__c+objItem.ONCALL__SAP_Order_Item_Number__c;
            objItem.ONCALL__SAP_Order_and_Order_Item_Number__c = StrIdOrderSap+objItem.ISSM_ItemPosition__c+objItem.ONCALL__SAP_Order_Item_Number__c;                
        }

        if(!LstBonus.isEmpty()){
            System.debug('#####LstBonus#####   '+LstBonus);
            for(ISSM_Bonus__c ObjBonus : LstBonus){
                ObjItem = new ONTAP__Order_Item__c();
                ObjItem.ONCALL__OnCall_Product__c       = ObjBonus.ISSM_Product__c;
                ObjItem.ONCALL__OnCall_Quantity__c      = ObjBonus.ISSM_BonusQuantity__c;
                ObjItem.ONTAP__CustomerOrder__c         = IdOrder;
                ObjItem.ISSM_OrderItemSKU__c            = ObjBonus.ISSM_BonusSKU__c;
                ObjItem.ISSM_ProductDesc__c             = ObjBonus.ISSM_Product__r.ONTAP__ProductShortName__c;
                ObjItem.ISSM_MaterialProduct__c         = ObjBonus.ISSM_MaterialProduct__c;
                ObjItem.ISSM_UnitofMeasure__c           = ObjBonus.ISSM_UnitMeasure__c;
                ObjItem.ONCALL__SAP_Order_Item_Number__c= ObjBonus.ISSM_Material_Number__c;
                ObjItem.ISSM_MaterialAvailable__c       = ObjBonus.ISSM_QuantityAvailable__c;
                ObjItem.ISSM_Uint_Measure_Code__c       = ObjBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c;
                ObjItem.ISSM_BalanceNum__c              = ObjBonus.ISSM_Product__r.ISSM_Empties_Material__r.ONCALL__Material_Number__c;
                ObjItem.ISSM_Is_returnable__c           = ObjBonus.ISSM_Product__r.ISSM_Is_returnable__c;
                ObjItem.ISSM_EmptyMaterial__c           = ObjBonus.ISSM_Product__r.ISSM_Empties_Material__c;
                ObjItem.ISSM_EmptyMaterialProduct__c    = ObjBonus.ISSM_Product__r.ISSM_Empties_Material__r.ONTAP__MaterialProduct__c;
                ObjItem.ISSM_EmptyRack__c               = ObjBonus.ISSM_Product__r.ISSM_BoxRack__c;
                ObjItem.ISSM_RackMaterialProduct__c     = ObjBonus.ISSM_Product__r.ISSM_BoxRack__r.ONTAP__MaterialProduct__c;
                ObjItem.ISSM_IsBonus__c                 = true;
                ObjItem.ISSM_IdOrderSAP__c              = ObjBonus.ISSM_SAPOrderNumber__c;
                //ObjItem.ONCALL__SAP_Order_and_Order_Item_Number__c = StrIdOrderSap+ObjItem.ONCALL__SFDC_Suggested_Order_Item_Number__c+ObjItem.ONCALL__SAP_Order_Item_Number__c;
                LstOItem.add(ObjItem); 
            }
        }
        
        if(counter_Int == 1)
            upsert LstOItem;
        //si se ejecuto el proceso y simplemente no devuelve ID SAP
        if(!BlnflgError && BlnflgWiD){
            LstMnsgSucc.add(Label.ISSM_Error03+' '+ObjOrder.Name+' '+Label.ISSM_Error04+' '+Label.ISSM_Error02 + '|' + ObjDesCO.error);
        //SI EXISTIO ALGUNA EXCEPCION
        }else if(BlnflgError && BlnflgWiD){
            String StrMns = String.ValueOf(LstMnsgSucc);
            System.debug('StrMns: ' + StrMns);
            LstMnsgSucc = new List<String>();
            LstMnsgSucc.add(StrMns);
        }
        //SI TODO SALIO OK
        else{
            System.debug('AQUI SE ARMA EL MENSAJE : '+strResponseDeals);
            LstMnsgSucc.add(Label.ISSM_OrderMnsg1+ ' '+StrIdOrderSap+'  |  '+StrIdOrderSapEB + '  |  '+strResponseDeals +'  |  Si quieres descargar el recibo en PDF de este pedido, ingresa a la sección notas y archivos adjuntos de dicho pedido');   
        }
        System.debug('LstMnsgSucc---> '+LstMnsgSucc);
        System.debug('********ORDEN FINAL : '+ObjOrder.Id);
        GeneratePDF(ObjOrder.Id,StrIdOrderSap);
        System.debug('###IdCall3### '+ObjOrder.ONCALL__Call__c);
        
        return LstMnsgSucc;
    }


    public static List<String> EndCallModernChannel(String IdOrder, String IdAcc, String IdCall,ONTAP__Order_Item__c[] LstOItem, 
                                                    String CallDuration,String StrCustomerComments,String StrPoNum, String StrDocType,
                                                    String Tel02, String Tel03, String Tel04, String Email01, Boolean ChkTel01, 
                                                    Boolean ChkTel02, Boolean ChkTel03, Boolean ChkTel04, Boolean ChkEmail, 
                                                    List<String> selectedPicklistValues, String counter){

        Integer counter_Int = !String.isBlank(counter) ? Integer.valueOf(counter) : 1;

        System.debug('##################ENTRO A END CALL CANAL MODERNO###################### '+StrDocType);
        String StrIdOrderSap;
        ISSM_DeserializeCreateOrder_cls ObjDesCO;
        String StrPurchaseNumber;
        Boolean BlnflgError=false;
        Boolean BlnflgWiD=false;
        try{
            StrPurchaseNumber = InvoicingPONumber(StrDocType);
            StrPoNum = StrDocType == Label.ISSM_Y023 ? StrPurchaseNumber.leftPad(10, '0') : StrPoNum.leftPad(10, '0');
            //INVOKE SERVICE CREATEORDER, envia Y013 pero al crear el pedido le asigna la facturación que contenga el cliente, Y013 O Y023
            //ObjDesCO  = !LstOItem.isEmpty() ? CLSWSCO.CallOutCreateOrder(IdOrder,IdAcc,LstOItem,null,'',StrDocType,null,StrPoNum,null,null,null) : null;
            System.debug('################## ObjDesCO :' + ObjDesCO);
        }catch(Exception ex){
            System.debug('ex----> '+String.ValueOf(ex.getMessage()));
            LstMnsgSucc.add(''+ex.getMessage());
            BlnflgError=true;
        }

            if(!Test.isRunningTest() && !BlnflgError)
                StrIdOrderSap   = ObjDesCO != null ? ObjDesCO.orderID : null;
            else
                StrIdOrderSap   = string.valueOf(Math.round(Math.random()*10000000));

            BlnflgWiD = String.isBlank(StrIdOrderSap) ? true : false;
            if(String.isNotBlank(IdCall)){
                ONCALL__Call__c ObjCall              = CTRSOQL.getCallbyId(IdCall);
                ObjCall.ISSM_EndCallTime__c          = Datetime.parse(System.now().format());
                ObjCall.ISSM_CallDuration__c         = Decimal.valueOf(CallDuration);
                StrTelesalesRoute                    = ObjCall.ISSM_SalesRoute__c;
                ObjCall.ISSM_CallEffectiveness__c    = Label.ISSM_CallSubStatus; 
                ObjCall.ISSM_CustomerComments__c     = StrCustomerComments; 
                ObjCall.ISSM_CallResult__c           = BlnflgError || BlnflgWiD ? Label.ISSM_OrderWOSAP : Label.ISSM_EffectiveCall; 
                ObjCall.ISSM_SuggestedTimeCalling__c = null;
                ObjCall.ISSM_ValidateOrder__c        = true;  
                ObjCall.Id=IdCall;     
                update ObjCall;

                Account objAcc = CTRSOQL.getAccountbyId(IdAcc);
                objAcc.ONCALL__Phone__c                  = Tel02;
                objAcc.ONTAP__Secondary_Phone__c         = Tel03;
                objAcc.ONTAP__Mobile_Phone__c            = Tel04;
                objAcc.ONTAP__Email__c                   = Email01;
                objAcc.ISSM_MainContactA__c              = ChkTel01;
                objAcc.ISSM_MainContactB__c              = ChkTel02;
                objAcc.ISSM_MainContactC__c              = ChkTel03;
                objAcc.ISSM_MainContactD__c              = ChkTel04;
                objAcc.ISSM_MainContactE__c              = ChkEmail;
                objAcc.ONTAP__Preferred_Service_Hours__c = String.join(selectedPicklistValues, ';');

                if(StrCustomerComments != null && StrCustomerComments != ''){                    
                    objAcc.ISSM_CustomerComments__c = StrCustomerComments + '\n\n' + Label.ISSM_Date +' '+ 
                                                      + System.now().format(Label.ISSM_FormatDate2) 
                                                      + '\n' + Label.ISSM_OriginCall + ': ' + UserInfo.getName();
                }
                if(counter_Int == 1)
                    update objAcc; 
            }

            ONTAP__Order__c ObjOrder             = CTRSOQL.getOrderbyId(IdOrder);
            ObjOrder.ONCALL__SAP_Order_Number__c = StrIdOrderSap;
            ObjOrder.ONTAP__OrderStatus__c       = BlnflgError || BlnflgWiD ? Label.ISSM_OrderStatus3 : Label.ISSM_OrderStatus;
            ObjOrder.ONTAP__DeliveryDate__c      = ReqDate;
            ObjOrder.ONCALL__Call__c             = IdCall;
            ObjOrder.ISSM_PayloadOrder__c        = String.isBlank(String.valueOf(ObjDesCO)) ? String.ValueOf(LstMnsgSucc) : String.valueOf(ObjDesCO);
            ObjOrder.ISSM_Autonumber__c          = String.isBlank(ObjOrder.ISSM_Autonumber__c) ? StrPurchaseNumber : ObjOrder.ISSM_Autonumber__c;
            ObjOrder.ONTAP__PurchaseOrder__c     = String.isBlank(ObjOrder.ONTAP__PurchaseOrder__c) ? StrPoNum : ObjOrder.ONTAP__PurchaseOrder__c;
            update ObjOrder;
            
            System.debug('***LstOItem----> '+LstOItem);
            
            if(counter_Int == 1)
                upsert LstOItem;

            if(!BlnflgError && BlnflgWiD){
                LstMnsgSucc.add(Label.ISSM_Error02+'|' + ObjDesCO.error);
            //SI EXISTIO ALGUNA EXCEPCION
            }else if(BlnflgError && BlnflgWiD){
                String StrMns = String.ValueOf(LstMnsgSucc);
                StrMns = StrMns.substring(1, StrMns.indexOf('|'));
                LstMnsgSucc = new List<String>();
                LstMnsgSucc.add(StrMns);
            }
            //SI TODO SALIO OK
            else{
                LstMnsgSucc.add(Label.ISSM_OrderMnsg1+ ' '+StrIdOrderSap);   
            }
            
        return LstMnsgSucc;
    }


    public static List<String> EndCallWithoutOrder(String IdOrder, String IdCall, String CallDuration){
        
        try{        
            ONTAP__Order__c ObjOrder = CTRSOQL.getOrderbyId(IdOrder);
            delete ObjOrder;

            if(String.isNotBlank(IdCall)){
                ONCALL__Call__c ObjCall          = CTRSOQL.getCallbyId(IdCall);           
                ObjCall.ISSM_EndCallTime__c      = Datetime.parse(System.now().format());
                ObjCall.ISSM_CallDuration__c     = Decimal.valueOf(CallDuration);            
                ObjCall.ISSM_ValidateOrder__c    = false;
                ObjCall.ISSM_CallEffectiveness__c= Label.ISSM_CallNotEffective;
                update ObjCall;
            }            
            LstMnsgSucc.add(Label.ISSM_OrderMnsg5);

        }catch(Exception ex){LstMnsgSucc.add(ex.getMessage());}
        
        return LstMnsgSucc;
    }


    public static List<String> EndCallWithoutOrderOnTap(String IdOrder){

        try{        
            ONTAP__Order__c ObjOrder  = CTRSOQL.getOrderbyId(IdOrder);
            ObjOrder.ISSM_EndOrder__c = false;
            update ObjOrder;
            LstMnsgSucc.add(Label.ISSM_OrderMnsg5);
        }catch(Exception ex){LstMnsgSucc.add(ex.getMessage());}
        
        return LstMnsgSucc;
    }   

    public static String CreatePONumber(String IdOrder){
        ONTAP__Order__c CurrentOrd = new ONTAP__Order__c();
        CurrentOrd                 = CTRSOQL.getOrderbyId(IdOrder);
        ONTAP__Route__c ObjRoute   = CTRSOQL.getRoute(CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c);

        Datetime DtDateP                = Datetime.parse(System.now().format());
        String StrPurDat                = DtDateP.format(Label.ISSM_FormatDate);
        StrPurDat                       = StrPurDat.replace('-', '');
        String DtConcat                 = StrPurDat + CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c + '%';
        Integer MaxOrderToday           = CTRSOQL.getMaxOrderToday(DtConcat);
        MaxOrderToday = MaxOrderToday == 0 ? 0 : MaxOrderToday++;        
        String Autonumber               = String.valueOf(MaxOrderToday);

        String PONum = StrPurDat
                     + CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c
                     + Autonumber.leftPad(4, '0')
                     + '|'
                     + (CurrentOrd.RecordType.Name == Label.ISSM_RecTypeCall1 ? CurrentOrd.ONCALL__Call__r.ISSM_SAPUserId__c : ObjRoute.ISSM_SAPUserId__c);

        MapAutonumber.put(PONum,String.valueOf(Autonumber));
        return PONum;
    }

    public static String InvoicingPONumber(String StrDocType){        
        ONTAP__Order__c[] MaxOrderToday = CTRSOQL.getInvoicingMaxOrderToday(StrDocType);        
        String Autonumber = MaxOrderToday.isEmpty() ? '0' : String.valueOf(Integer.valueOf(MaxOrderToday[0].ISSM_Autonumber__c) + 1);        
        return Autonumber;
    }

    public static void GeneratePDF(ID recordID,String StrIdOrderSap){
        String idOrder = String.valueOf(recordID);
        someFutureMethod(idOrder,StrIdOrderSap);
    
    }
    
    @Future(callout=true) 
    public static void someFutureMethod(String recordIds,String StrIdOrderSap) {
        System.debug('************recordIds : '+recordIds);
        pageReference pdfPage = Page.ISSM_OncallCreateOrderPDF_pag;
        pdfPage.getParameters().put('IdOrder',recordIds);
        
        date myDate =  System.today();
        Attachment attach = new Attachment();
        Blob body; 
        body = (!Test.isRunningTest()) ?   pdfPage.getContentAsPDF() :   blob.valueOf('Unit.Test');  
        attach.Body = body; 
        attach.Name = String.valueOf(myDate.year())+ String.valueOf(myDate.month())+String.valueOf(myDate.day())+'-'+StrIdOrderSap+'.pdf';
        attach.IsPrivate = false;
        attach.ParentId = recordIds; 
        insert attach;
        System.debug('************ATTACH :  ' +attach );
    }
    
  

}