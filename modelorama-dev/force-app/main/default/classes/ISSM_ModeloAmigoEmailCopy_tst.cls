/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_ModeloAmigoEmailCopy_tst {
	public static Case caseObj;
    static testMethod void EmailServiceTest() {  
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        caseObj  = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion','Asunto','New',null);
        List<Case> lstCase = [Select id,CaseNumber From Case Where id =: caseObj.Id ];
         //Creamos el Body  

        email.plainTextBody='ejemplo 1 \n'+'Id:'+lstCase[0].Id;
        email.fromAddress ='test@test.com';   
        email.htmlBody='Cuerpo Correo Body';  
        String contactEmail = 'jsmith@salesforce.com';   
        email.ccAddresses = new String[] {contactEmail };
        email.subject = 'Create Task';    
        env.fromAddress = 'hdiaz@avanxo.com';
        env.toAddress = 'hector.diaz93@hotmail.com';
        ISSM_ModeloAmigoEmailCopy_cls  objTestEmailServiceCopy = new ISSM_ModeloAmigoEmailCopy_cls(); 
        System.debug('email'+email + '------ env : '+env); 
        
         
        Test.startTest();
            objTestEmailServiceCopy.handleInboundEmail(email,env);
        Test.stopTest();
        
    }  
}