/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Test class for apex class  TRM_SendComboToMulesoft_bch
*
* No.       Fecha              Autor                      Descripción
* 1.0    10-agosto-2018      Oscar Alvarez                   Creación
*******************************************************************************/

@isTest
private class TRM_SendComboToMulesoftBch_tst {
    @testSetup 
    static void setup() {
        String RecTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        String ISSM_ComboByCustomerMX = Schema.getGlobalDescribe().get('ISSM_ComboByAccount__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboByCustomerMx').getRecordTypeId();

		MDM_Parameter__c MDMPARAM 		= new MDM_Parameter__c();
	        MDMPARAM.Name				= 'MDMPARAMETER';
	        MDMPARAM.Active__c    		= true;
	        MDMPARAM.Active_Revenue__c	= true;
	        MDMPARAM.Code__c 			= '40';
	        MDMPARAM.Catalog__c 		= 'Segmento';
	        MDMPARAM.Description__c		= 'MDMPARAMETER01';
        Insert MDMPARAM;

        MDM_Parameter__c MDMPARAM1 		= new MDM_Parameter__c();
	        MDMPARAM1.Name				= 'MDMPARAMETER02';
	        MDMPARAM1.Active__c    		= true;
	        MDMPARAM1.Active_Revenue__c	= true;
	        MDMPARAM1.Code__c 			= '50';
	        MDMPARAM1.Catalog__c 		= 'GrupoMateriales1';
	        MDMPARAM1.Description__c	= 'MDMPARAMETER02';
        Insert MDMPARAM1;

        ONTAP__Product__c PRODUCT = new ONTAP__Product__c();
	        PRODUCT.ONTAP__ProductId__c 		= '3000005';
	        PRODUCT.ONTAP__MaterialProduct__c 	= '3000005';
	        PRODUCT.ONTAP__ProductShortName__c 	= 'VICTORIA';
	        PRODUCT.ONTAP__ProductType__c 		= 'FERT';
        Insert PRODUCT;

        Account  ACC = new Account();
        ACC.Name 		 = 'Name SalesOffice Parent';
        ACC.RecordTypeId = RecTypeAccountSalesOffice;
        Insert ACC;

        Account  ACCSO = new Account();
        ACCSO.Name 					= 'Name SalesOffice';
        ACCSO.RecordTypeId 			= RecTypeAccountSalesOffice;
        ACCSO.ONTAP__SalesOffId__c 	= 'SO01';
        ACCSO.ParentId 				= ACC.Id;
        Insert ACCSO;

        ISSM_ComboLimit__c CMBLIMIT = new ISSM_ComboLimit__c();
        CMBLIMIT.ISSM_ComboLevel__c 	= 'ISSM_SalesOffice';
		CMBLIMIT.ISSM_SalesStructure__c = ACC.Id;
		CMBLIMIT.ISSM_AllowedCombos__c  = 12;
		CMBLIMIT.ISSM_IsActive__c  		= true;
		Insert CMBLIMIT;

		ISSM_Combos__c COMBO = new ISSM_Combos__c();
        COMBO.ISSM_StartDate__c 		= Date.today() + 2;
        COMBO.ISSM_EndDate__c			= Date.today() + 30;
        COMBO.ISSM_LongDescription__c	= 'Long description for the new combo';
        COMBO.ISSM_ShortDescription__c	= 'ShortDescr';
        COMBO.ISSM_NumberSalesOffice__c	= 10;
        COMBO.ISSM_NumberByCustomer__c	= 1;
        COMBO.ISSM_Currency__c			= 'MXN';
        COMBO.ISSM_ComboLimit__c		= CMBLIMIT.Id;
        COMBO.ISSM_TypeApplication__c	= 'ALLMOBILE;TELESALES;B2B;BDR';
        COMBO.RecordTypeId				= Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        COMBO.ISSM_PriceGrouping__c 	= MDMPARAM1.Code__c;
        COMBO.ISSM_ExternalKey__c='9000000430';
        COMBO.ISSM_StatusCombo__c='ISSM_Approved';
        COMBO.ISSM_SynchronizedWithSAP__c=false;
        COMBO.ISSM_ModifiedCombo__c = true;
        Insert COMBO;

		ISSM_ComboByProduct__c CBYPROD = new ISSM_ComboByProduct__c();
		CBYPROD.ISSM_ActiveValue__c		= true;
		CBYPROD.ISSM_QuantityProduct__c = 2;
		CBYPROD.ISSM_UnitPriceTax__c 	= 120;
		CBYPROD.ISSM_Product__c 		= PRODUCT.Id;
		CBYPROD.ISSM_Type__c 			= 'Product';
		CBYPROD.ISSM_ComboNumber__c		= COMBO.Id;
		Insert CBYPROD;

		ISSM_ComboByAccount__c CBYACC = new ISSM_ComboByAccount__c();
        CBYACC.ISSM_SalesOffice__c 	= ACCSO.Id;
        CBYACC.RecordTypeId			= ISSM_ComboByCustomerMX;
        CBYACC.ISSM_ComboNumber__c  = COMBO.Id;
        Insert CBYACC;
    }
    static testmethod void testBatchCreate() {        
        Test.startTest();
        List<String> lstIdCombo= new List<String>();
        List<ISSM_Combos__c> lstCombo = [SELECT Id FROM ISSM_Combos__c];
        for(ISSM_Combos__c combo : lstCombo) lstIdCombo.add(combo.Id);        
        if(lstIdCombo.size() >0) Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstIdCombo,Label.TRM_OperationCreate), Integer.valueOf(Label.TRM_NumberLotsProcessed));
        Test.stopTest();
    }
     static testmethod void testBatchModify() {        
        Test.startTest();
        List<String> lstIdCombo= new List<String>();
        List<ISSM_Combos__c> lstCombo = [SELECT Id FROM ISSM_Combos__c];
        for(ISSM_Combos__c combo : lstCombo) lstIdCombo.add(combo.Id);        
        if(lstIdCombo.size() >0) Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstIdCombo,Label.TRM_OperationModify), Integer.valueOf(Label.TRM_NumberLotsProcessed));
        Test.stopTest();
    }
    
}