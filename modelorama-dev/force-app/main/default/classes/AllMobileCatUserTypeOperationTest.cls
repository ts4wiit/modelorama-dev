/**
 * Test class for AllMobileCatUserTypeOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileCatUserTypeOperationTest {

	/**
	 * Test method to setup data.
	 */
	@testSetup
	static void setup() {

		//Create a Device User.
        AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1111111110', 'ZZZ', 'ZZZ', '1111111110');
        insert objAllMobilePermissionCategoryUserTypeAll;
        AllMobileCatUserType__c objAllMobileCatUserType = AllMobileUtilityHelperTest.createAllMobileCatUserType('ZZZ', false, false, 'Almacén ZZZ', 'ZZZ', objAllMobilePermissionCategoryUserTypeAll);
		insert objAllMobileCatUserType;
        AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll2 = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1000000000', 'ALL', 'ALL', '1000000000');
        insert objAllMobilePermissionCategoryUserTypeAll2;
        AllMobileCatUserType__c objAllMobileCatUserType2 = AllMobileUtilityHelperTest.createAllMobileCatUserType('ALL', false, false, 'Almacén lleno', 'ALL', objAllMobilePermissionCategoryUserTypeAll);
		insert objAllMobileCatUserType2;
        objAllMobileCatUserType2.AllMobileDeviceUserSoftDeleteFlag__c = true;
        update objAllMobileCatUserType2;
	}

	/**
	 * Test method to test Queueable insertCatUserTypeInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableInsertCatUserTypeInSFAndSendToHeroku() {

		//Variables to enqueue Job.
		String strEventTriggerFlagCatUserTypeAfterInsert = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_INSERT;
		List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToInsertInHeroku = [SELECT Id, Name FROM AllMobileCatUserType__c /*WHERE AllMobileDeviceUserUserName__c IN ('Ricardo Estrada') */] ;
		AllMobileCatUserTypeOperationClass objAllMobileCatUserTypeOperationClassInsert = new AllMobileCatUserTypeOperationClass(lstAllMobileCatUserTypeToInsertInHeroku, strEventTriggerFlagCatUserTypeAfterInsert);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileCatUserTypeOperationClassInsert);

		//End Test.
		Test.stopTest();
	}

    /**
	 * Test method to test Queueable updateCatUserTypeInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableUpdateCatUserTypeInSFAndSendToHeroku() {

		//Variables to enqueue Job.
		String strEventTriggerFlagCatUserTypeAfterUpdate = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_UPDATE;
		List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToUpdateInHeroku = [SELECT Id, Name FROM AllMobileCatUserType__c WHERE Name IN ('ZZZ') LIMIT 1] ;
        lstAllMobileCatUserTypeToUpdateInHeroku[0].Name = 'XXX';
		AllMobileCatUserTypeOperationClass objAllMobileCatUserTypeOperationClassUpdate = new AllMobileCatUserTypeOperationClass(lstAllMobileCatUserTypeToUpdateInHeroku, strEventTriggerFlagCatUserTypeAfterUpdate);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileCatUserTypeOperationClassUpdate);

		//End Test.
		Test.stopTest();
	}

    /**
	 * Test method to test syncDeleteAllMobileCatUserTypeInSF method.
	 */
	static testMethod void testSyncDeleteAllMobileCatUserTypeInSF() {

		//Variables.
		List<AllMobileCatUserType__c> lstAllMobileCatUserTypeToUpdateInHeroku = [SELECT Id, Name, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.Name, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c  FROM AllMobileCatUserType__c];

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileCatUserTypeOperationClass.syncDeleteAllMobileCatUserTypeInSF(lstAllMobileCatUserTypeToUpdateInHeroku);

		//Stop Test.
		Test.stopTest();
	}

    /**
	 * Test method to test SyncInsertAndUpdateAllMobileCatUserTypeInSF method.
	 */
	static testMethod void testSyncInsertAndUpdateAllMobileCatUserTypeInSF() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileCatUserTypeOperationClass.syncInsertUpdateCatUserTypeFromHeroku();

		//Stop Test.
		Test.stopTest();
	}
}