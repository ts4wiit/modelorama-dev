/****************************************************************************************************

    Información general
    -------------------
    author: Luis Enrique Garcia
    email: lgarcia@avanxo.com
    company: Avanxo México
    Project: Implementación Salesforce
    Customer: ABInBev - CAM
    
    Description:
    <Hacer una descripcion completa de que hace este desarrollo>

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------------------------------------
    1.0       28-SEP-2018     Luis Enrique Garcia              Creación de clase
    ================================================================================================

****************************************************************************************************/

public class ISSM_CAM_UpsertInventory_cls {

	/**
    * @author Luis Garcia (legarcia@avanxo.com)
    * @version 1.0
    * @description: method that help a process batch upsert rocord of ISSM_Asset_Inventory__c 
    *               execution everyday at 7 am.
    *
    * 2018-09-20 : Luis Garcia - Original
    **/

    public static void upsertInventory (List<ISSM_Asset_Inventory__c> agrInventoryBch){
        List<ISSM_Asset_Inventory__c> lstInventory = new List<ISSM_Asset_Inventory__c>();
        Map<String, String> mapCount = new Map<String, String>();
        try{
            List<ISSM_Asset_Inventory__c> agrInventory = new List<ISSM_Asset_Inventory__c>();
            for(ISSM_Asset_Inventory__c iteratorInventory: agrInventoryBch) {
                if(iteratorInventory.CreatedDate < system.today()) {
                    agrInventory.add(iteratorInventory);
                }
            }
            System.debug(loggingLevel.Error, '*** agrInventory: ' + agrInventory);
            if(!agrInventory.isEmpty()) {
                List<Database.DeleteResult> dR = Database.delete(agrInventory,true); 
            }

            AggregateResult[] agrInventoryCount
                = [SELECT   ISSM_Sales_Organization__c,
                   ISSM_Sales_Organization__r.ontap__sapcustomerid__c salesogid,
                   ISSM_Centre__c, ISSM_Model_Name__c,
                   Type__c,
                   ISSM_Asset_Description__c,
                   ISSM_Material_number__c,
                   count(id) iCuenta
                   FROM ISSM_Asset__c
                   WHERE ISSM_Status_SFDC__c =: Label.upsertInventoryFreeUse
                   GROUP BY ISSM_Sales_Organization__c, ISSM_Sales_Organization__r.ontap__sapcustomerid__c, ISSM_Centre__c, ISSM_Model_Name__c, Type__c, ISSM_Asset_Description__c, ISSM_Material_number__c
                   ];
            System.debug(loggingLevel.Error, '*** agrInventoryCount.size(): ' + agrInventoryCount.size());
            System.debug(loggingLevel.Error, '*** agrInventoryCount: ' + agrInventoryCount);
            integer intConsecutive = 0001;
            //integer intCount = agrInventory
            for (AggregateResult ar : agrInventoryCount){
                string ExternalId = ar.get('salesogid')
                    + '-' + ar.get('ISSM_Centre__c')
                    + '-' + ar.get('ISSM_Material_number__c')
                    + '-' + intConsecutive;
                System.debug(loggingLevel.Error, '*** ExternalId: ' + ExternalId);
                ISSM_Asset_Inventory__c i =
                    new ISSM_Asset_Inventory__c(
                        Name = (string) ar.get('ISSM_Asset_Description__c'),
                        ISSM_Inventario_External_Id__c = ExternalId,
                        ISSM_UEN__c = (id) ar.get('ISSM_Sales_Organization__c'),
                        ISSM_Centre__c = (string) ar.get('ISSM_Centre__c'),
                        ISSM_Material_Name__c = (string) ar.get('Type__c'),
                        ISSM_Material_Description__c = (string) ar.get('ISSM_Asset_Description__c'),
                        ISSM_Material_Number__c = (string) ar.get('ISSM_Material_number__c'),
                        ISSM_Stock_SAP__c = (integer) ar.get('iCuenta'),
                        ISSM_Stock_SFDC__c = (integer) ar.get('iCuenta'));
                lstInventory.add(i);
                system.debug(i);
                intConsecutive = intConsecutive+1; 
            }
            System.debug(loggingLevel.Error, '*** lstInventory: ' + lstInventory);
        }catch(Exception e){
            system.debug('ERROR MESSAGE: ' + e.getMessage() + ' \n### LINE NUMBER: ' + e.getLineNumber() + '\n### STACK: ' + e.getStackTraceString());
            throw e;
        }


        if(lstInventory.size()>0 && lstInventory!=NULL){
            system.debug(lstInventory.size());

            List<Database.UpsertResult> srList = new List<Database.UpsertResult>();
            try{
                srList = Database.upsert(lstInventory, ISSM_Asset_Inventory__c.ISSM_Inventario_External_Id__c, false);
            }
            catch (exception e){
                system.debug('ERROR MESSAGE: ' + e.getMessage() + ' \n### LINE NUMBER: ' + e.getLineNumber() + '\n### STACK: ' + e.getStackTraceString());
                throw e;
            }
        }
    }
}