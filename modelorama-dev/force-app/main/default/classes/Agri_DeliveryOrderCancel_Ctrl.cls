public class Agri_DeliveryOrderCancel_Ctrl {
    
    public String contractId {get;set;}
    public List<DeliveryOrderWrapper> lOrdersWrapper {get;set;}
    
    public Agri_DeliveryOrderCancel_Ctrl() {
        contractId = ApexPages.currentPage().getparameters().get('contractId');     
        
        set<string> setStatus = new set<String>();
        lOrdersWrapper = new List<DeliveryOrderWrapper>();
        for(Delivery_Order__c deOr : [SELECT Id, Agri_Folio__c, Agri_rb_almacen__r.Name, Agri_Delivery_date__c, Agri_Estimated_weight__c, Agri_ls_status__c 
                                      FROM Delivery_Order__c 
                                      WHERE Agri_Contract__c = : contractId 
                                      AND RecordType.Name = 'Delivery order'
                                      AND (Agri_ls_status__c = 'Nueva' OR Agri_ls_status__c = 'Error en creación SAP' OR Agri_ls_status__c = 'Orden creada en SAP' OR Agri_ls_status__c = 'Error en cancelación SAP')]) 
        {
            lOrdersWrapper.add(new DeliveryOrderWrapper(deOr));
            setStatus.add(deOr.Agri_ls_status__c);
        }      
    }
    
    public void actualizaSeleccionados() {
        System.debug('--> lOrdersWrapper : ' + lOrdersWrapper);
        
        List<Delivery_Order__c> lDOUpd = new List<Delivery_Order__c>();
        
        for(DeliveryOrderWrapper dow : lOrdersWrapper) {
            if(dow.isSelected == true) {
                dow.deliveryOrder.Agri_ls_status__c = 'Cancelada';
                dow.deliveryOrder.Agri_tx_mandante__c = '200';
                lDOUpd.add(dow.deliveryOrder);
            }
        }
        
        if(!lDOUpd.isEmpty()) {
            update lDOUpd;
            System.debug('--> lDOUpd : ' + lDOUpd);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Se cancelaron correctamente las ordenes de entrega seleccionadas'));
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'No se ha seleccionado ningún registro. Las ordenes de entrega con casillas deshabilitadas no se pueden cancelar'));
        }
    }
    
    public PageReference regresar() {
        PageReference myVFPage = null;        
        if(contractId != null && contractId != '') {
            myVFPage = new PageReference('/' + contractId);
        } else {
            myVFPage = new PageReference('/home/home.jsp');
        }        
        return myVFPage;
    }
    
    public class DeliveryOrderWrapper {
        public Delivery_Order__c deliveryOrder {get;set;}
        public Boolean isSelected {get;set;}
        
        public DeliveryOrderWrapper(Delivery_Order__c dOr) {
            deliveryOrder = dor;
            isSelected = false;
        }
    }

}