@isTest // (SeeAllData=true)
public class ISSM_CAM_tst {

    @testSetup static void setup() {
        //=============================== Custom Settins WS
        ISSM_PriceEngineConfigWS__c wsEstatus = new ISSM_PriceEngineConfigWS__c(
            Name='ConfigCAMWSEstatus', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/estado',
            ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsEstatus;
        ISSM_CAM_Approvers__c camapprovers =
            new ISSM_CAM_Approvers__c (Name='CAM_Asset_Assignation', Approver1__C='omar.cruz@gmodelo.com.mx.test', Approver2__C='omar.cruz@gmodelo.com.mx.test',
                                       Approver3__C='omar.cruz@gmodelo.com.mx.test', Approver4__C='omar.cruz@gmodelo.com.mx.test');
        insert camapprovers;
        //==============================User
        user usrapp1 = new User (username='omar.cruz@gmodelo.com.mx.test', lastname='approval1',email='omar.cruz@gmodelo.com.mx', Alias='app1',
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp1;
        user usrapp2 = new User (username='rogelio.olvera@gmodelo.com.mx.test',lastname='approval2',email='rogelio.olvera@gmodelo.com.mx', Alias='app2',
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp2;
        user usrapp3 = new User (username='jefe.frio@gmodelo.com.mx.test',lastname='approval3',email='jefe.frio@gmodelo.com.mx', Alias='app3',
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp3;
        user usrapp4 = new User (username='sup.trade@gmodelo.com.mx.test',lastname='approval4',email='sup.trad@gmodelo.com.mx', Alias='app4',
                                 TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrapp4;

        user usrGteOp = new User (username='gte.op@gmodelo.com.mx.test',lastname='gteOp',email='gte.op@gmodelo.com.mx', Alias='gteOp',
                                  TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrGteOp;
        user usrGteAdm = new User (username='gte.adm@gmodelo.com.mx.test',lastname='gteAdm',email='gte.adm@gmodelo.com.mx', Alias='gteAdm',
                                   TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrGteAdm;

        user usrgteGen = new User (username='gte.gen@gmodelo.com.mx.test',lastname='gteGen',email='gte.gen@gmodelo.com.mx', Alias='gteGen',
                                   TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrgteGen;
        user usrgteRTM = new User (username='gte.rtm@gmodelo.com.mx.test',lastname='gteRTM',email='gte.rtm@gmodelo.com.mx', Alias='gteRTM',
                                   TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es', EmailEncodingKey='ISO-8859-1', ProfileId='00e36000000SCLGAA4', LanguageLocaleKey='es');
        insert usrgteRTM;
        //==============================Recordtype
        map<String, Id> recTypeIds = new map<String, Id>();
        for (recordtype rc : [SELECT Id, DeveloperName FROM RecordType
                              WHERE DeveloperName IN ('ISSM_RegionalSalesDivision','SalesOrg','SalesOffice', 'Account', 'CAM_Asset_Assignation', 'CAM_Asset_UnAssignation', 'CAM_Asset_Decommission')])
            recTypeIds.put(rc.developername, rc.id);
        //===============================Account
        Account accABINBEV = new Account(
            Name='AbInBev', ISSM_RegionalSalesDivision__c=null, ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='', ontap__sapcustomerid__c='ABInBev',
            RecordTypeId=recTypeIds.get('Account') );
        insert accABINBEV;

        Account accDRV = new Account(
            Name='DRV', ISSM_RegionalSalesDivision__c=null, ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='', ontap__sapcustomerid__c='DRV',
            RecordTypeId=recTypeIds.get('ISSM_RegionalSalesDivision') );
        insert accDRV;

        Account accSalesOrg  =
            new Account(
                Name='SalesOrg',ISSM_RegionalSalesDivision__c=accDRV.id, ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',ontap__sapcustomerid__c='ORG', ontap__salesogid__c='ORG',
                RecordTypeId=recTypeIds.get('SalesOrg'), parentId=accDRV.Id);
        insert accSalesOrg;

        Account accSalesOff  =
            new Account(
                Name='SalesOff',ISSM_RegionalSalesDivision__c=accDRV.id, ISSM_SalesOrg__c=accSalesOrg.id, ontap__sapcustomerid__c='OFF', ontap__salesogid__c='ORG',ONTAP__SalesOffId__c='OFF',
                ISSM_BillingManager__c=UserInfo.getUserId(),ONTAP__SalesOfficeDescription__c='OFF', RecordTypeId=recTypeIds.get('SalesOffice'), parentId=accSalesOrg.id);
        insert accSalesOff;

        Account acc =
            new Account(
                Name = 'ACCTEST1',  ISSM_RegionalSalesDivision__c=accDRV.Id, ISSM_SalesOrg__c=accSalesOrg.Id, ISSM_SalesOffice__c = accSalesOff.Id, parentid=accSalesOff.Id,
                ontap__sapcustomerid__c='TEST', ontap__salesoffid__c='OFF', ontap__salesogid__c='ORG',
                ISSM_Is_Blacklisted__C=False, recordtypeid=recTypeIds.get('Account'), OwnerId=usrapp1.id, ONTAP__Email__c='test@est.com' );
        insert acc;
        Account accFallBack =
            new Account(
                Name = 'FALLBACK',
                ontap__sapcustomerid__c='FALLBACK', ontap__salesoffid__c='OFF2',
                ISSM_Is_Blacklisted__C=False, recordtypeid=recTypeIds.get('Account'), OwnerId=usrapp1.id, ONTAP__Email__c='test@est.com' );
        insert accFallBack;

        /*
Account acc2 = new Account( Name = 'ACCTEST2',  ISSM_RegionalSalesDivision__c=accDRV.Id, ISSM_SalesOrg__c=accSalesOrg.Id, ISSM_SalesOffice__c = accSalesOff.Id, parentid=accSalesOff.Id,
ontap__sapcustomerid__c='TEST2', ontap__salesoffid__c='OFF', ontap__salesogid__c='ORG',
ISSM_Is_Blacklisted__C=True, recordtypeid=recTypeIds.get('Account'), OwnerId=usrapp1.id, ONTAP__Email__c='test@est.com' );
insert acc2;
*/
        //===================================Account Team Member
        AccountTeamMember atmsalesOffJF =  new AccountTeamMember( AccountId= accSalesOff.id,  userid=usrapp3.id,  TeamMemberRole='Supervisor de Mobiliario');
        insert atmsalesOffJF;

        AccountTeamMember atmsalesOff =  new AccountTeamMember( AccountId= accSalesOff.id,  userid=usrapp4.id,  TeamMemberRole='Supervisor de Trade Marketing');
        insert atmsalesOff;

        AccountTeamMember atmsalesorg =  new AccountTeamMember( AccountId= accSalesOrg.id,  userid=usrapp1.id,  TeamMemberRole='Gerente de Trade Marketing');
        insert atmsalesorg;

        AccountTeamMember atmdrv =  new AccountTeamMember( AccountId=AccDRV.id,  userid=usrapp2.id, TeamMemberRole='Gerente Regional de Trade Marketing');
        insert atmdrv;
        AccountTeamMember atmGteRTM =  new AccountTeamMember( AccountId=AccSalesOrg.id,  userid=usrgteRTM.id, TeamMemberRole='Gerente Regional de Trade Marketing');
        insert atmGteRTM;
        AccountTeamMember atmGteOp =  new AccountTeamMember( AccountId=AccSalesOrg.id,  userid=usrgteOp.id, TeamMemberRole='Gerente de Operaciones');
        insert atmGteOp;
        AccountTeamMember atmGteAdm =  new AccountTeamMember( AccountId=AccSalesOrg.id,  userid=usrgteAdm.id, TeamMemberRole='Gerente Administrativo');
        insert atmGteAdm;
        AccountTeamMember atmGteGen =  new AccountTeamMember( AccountId=AccSalesOrg.id,  userid=usrgteGen.id, TeamMemberRole='Gerente General');
        insert atmGteGen;

        //=================================Inventory
        ISSM_Asset_Inventory__c inv =
            new ISSM_Asset_Inventory__c( Name = 'VNTEST',  ISSM_Stock_SFDC__c = 20,  ISSM_Material_Number__c = '8000300', ISSM_Centre__c='OFF');
        insert inv;
        ISSM_Asset_Inventory__c inv2 =
            new ISSM_Asset_Inventory__c( Name = 'VNTEST2',  ISSM_Stock_SFDC__c = 10,  ISSM_Material_Number__c = '8000300', ISSM_Centre__c='OFF2');
        insert inv2;
        //===============================Asset CAM
        ISSM_Asset__c assetValidation = new ISSM_Asset__c(name='Validation', ISSM_CutOverReason__c = 'Incorrect Asset Number', ISSM_Centre__c='OFF', Equipment_Number__c='0000000VALIDATION_', ISSM_Serial_Number__c='VALIDATION_', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Validation');
        insert assetValidation;
        ISSM_Asset__c assetFreeUse = new ISSM_Asset__c(name='Free_Use', ISSM_CutOverReason__c = 'Incorrect Asset Number', ISSM_Centre__c='OFF', Equipment_Number__c='000000000FREE_USE_', ISSM_Serial_Number__c='FREE_USE_', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Free Use');
        insert assetFreeUse;
        ISSM_Asset__c assetFreeUseDiffSalesOff = new ISSM_Asset__c(name='DiffSalesOff', ISSM_CutOverReason__c = 'Incorrect Asset Number', Equipment_Number__c='000000DIFFSALESOFF', ISSM_Serial_Number__c='DIFFSALESOFF', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Free Use');
        insert assetFreeUseDiffSalesOff;
        ISSM_Asset__c assetUnassign = new ISSM_Asset__c(name='Unassignation', ISSM_CutOverReason__c = 'Incorrect Asset Number', ISSM_Centre__c='OFF', Equipment_Number__c='0000UNASSIGNATION_', ISSM_Serial_Number__c='UNASSIGNATION_', ISSM_Material_number__c='8000300', ISSM_Status_SFDC__c='Free Use');
        insert assetUnassign;
        //=============================== Typification Matrix
        ISSM_TypificationMatrix__c TypMat =
            new ISSM_TypificationMatrix__c(ISSM_UniqueIdentifier__c='TEST', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo');
        insert TypMat;
        ISSM_TypificationMatrix__c TypMatRetiro =
            new ISSM_TypificationMatrix__c(ISSM_UniqueIdentifier__c='TESTRETIRO', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Retiro de equipo');
        insert TypMatRetiro;
        //============================= Custom Category
        ISSM_Customer_Category__c CustCat = new ISSM_Customer_Category__c(Name='Steel', Category_Status__c=True, Final_Volume_Beer_Boxes__c=80);
        insert CustCat;
        //================================= KPI
        ONTAP__KPI__c kpi01 =
            new ONTAP__KPI__c(ONTAP__Kpi_id__c = 1,  ONTAP__Actual__c=500, ontap__kpi_name__c = 'Cerveza',
                              ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY(), ONTAP__Account_id__c = acc.id);
        insert kpi01;

        /*
* ONTAP__KPI__c kpi02 =
new ONTAP__KPI__c(ONTAP__Kpi_id__c = 1,  ONTAP__Actual__c=500, ontap__kpi_name__c = 'Cerveza',
ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY()-32, ONTAP__Account_id__c = acc2.id);
insert kpi02;
*/
        //=============================== CASE FORCE
        id recTypIdTestAssign = recTypeIds.get('CAM_Asset_Assignation');
        id recTypIdTestDecomm = recTypeIds.get('CAM_Asset_Decommission');
        id recTypIdTestUnAssign = recTypeIds.get('CAM_Asset_UnAssignation');
        List<ontap__case_force__c> lstCaseforce = new List<ontap__case_force__c>();
        ONTAP__Case_Force__c cf_sol =
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Solicitud',
                                     ONTAP__Account__c = acc.id,  ISSM_Asset_Inventory__c = inv.id,  ONTAP__Quantity__c = 1, ONTAP__Send_to_Approval__c=true,
                                     ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Entrega de equipo',
                                     ISSM_ApprovalStatus__c='In Progress', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7, recordtypeid=recTypIdTestAssign );
        lstCaseforce.add(cf_sol);
        ONTAP__Case_Force__c cf_solFallBack =
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Fallback',
                                     ONTAP__Account__c = accFallBack.id,  ISSM_Asset_Inventory__c = inv2.id,  ONTAP__Quantity__c = 1, ONTAP__Send_to_Approval__c=true,
                                     ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Entrega de equipo',
                                     ISSM_ApprovalStatus__c='In Progress', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7, recordtypeid=recTypIdTestAssign );
        //lstCaseforce.add(cf_solFallBack);
        ONTAP__Case_Force__c cf_decomm =
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Decommission1', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Baja', recordtypeid=recTypIdTestDecomm );
        lstCaseforce.add(cf_decomm);

        ONTAP__Case_Force__c cfUnassign =
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Retiro', ISSM_Asset_CAM__c = assetUnassign.id,
                                     ONTAP__Account__c = acc.id,  ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Retiro de equipo',
                                     ISSM_ApprovalStatus__c='Approved', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7, recordtypeid=recTypIdTestUnAssign );
       lstCaseforce.add(cfUnassign);
        insert lstCaseforce;
        //event ev = new event(WhatId=acc.id, OwnerId=[SELECT id FROM USER WHERE id=:usrapp1.id LIMIT 1].id , DurationInMinutes=15, StartDateTime= system.now()); // Estado_de_visita__c = New
        //insert ev;
    }
    static testMethod void runTestCaseForceUnAssignCreation(){
        test.startTest();
        ONTAP__Case_Force__c CFUnAssign = [SELECT id, ISSM_TypificationNumber__c,ISSM_Approver1__r.username, ISSM_Approver2__r.username, recordtypeid FROM ONTAP__Case_Force__c WHERE ONTAP__Subject__c = 'Retiro'];
        //system.assert(CFUnAssign.ISSM_TypificationNumber__c!=NULL);
        //system.assertEquals('omar.cruz@gmodelo.com.mx.test', CFUnAssign.ISSM_Approver1__r.username);
        //system.assertEquals('rogelio.olvera@gmodelo.com.mx.test', CFUnAssign.ISSM_Approver2__r.username);
        test.stopTest();
    }

    static testmethod void runTestCaseForceCreation(){
        test.startTest();
        list<ONTAP__Case_Force__c> lstCF = new list<ONTAP__Case_Force__c>([
            SELECT id, recordtypeid FROM ONTAP__Case_Force__c WHERE ontap__subject__c like 'Solicitud%']);
        //system.assertEquals(lstCF, ISSM_CAM_cls.retCAMRecords(lstCF));
        ontap__case_force__c cf_sol = [SELECT ONTAP__Account__c, ISSM_Asset_Inventory__c, ONTAP__Quantity__c, ISSM_BeerCurrentMonthBoxes__c,  ISSM_Delivery_Date_CAM__c,
                                       ISSM_Approver1__c, ISSM_Approver2__c, ISSM_Approver3__c, ISSM_Approver4__c, ISSM_Approver1__r.username, ISSM_Approver2__r.username, ISSM_Approver3__r.username, ISSM_Approver4__r.username,
                                       ONTAP__Send_to_Approval__c, ISSM_ApprovalStatus__c,
                                       ISSM_TypificationNumber__c, ISSM_TypificationNumber__r.ISSM_UniqueIdentifier__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_ClassificationLevel2__c,
                                       RecordType.DeveloperName, ONTAP__Do_not_Create_Visit__c,
                                       ownerid, owner.username, ONTAP__Case_Close_Date__c, ONTAP__Case_Number__c, ISSM_CaseNumber__c
                                       FROM ONTAP__Case_Force__c
                                       WHERE ONTAP__Subject__c='Solicitud'];

        // system.assert(cf_sol.ISSM_TypificationNumber__c!=NULL);
        //system.assertEquals('omar.cruz@gmodelo.com.mx.test', cf_sol.ISSM_Approver1__r.username);
        //system.assertEquals('rogelio.olvera@gmodelo.com.mx.test', cf_sol.ISSM_Approver2__r.username);
        //system.assert(cf_sol.ISSM_Approver3__r.username!=NULL,'Falta aprobador 3');
        //system.assert(cf_sol.ISSM_Approver4__r.username!=NULL,'Falta aprobador 4');

        //system.assertEquals(500, cf_sol.ISSM_BeerCurrentMonthBoxes__c);

        ISSM_Asset_Inventory__c inv = [SELECT id, name, ISSM_Stock_SFDC__c, ISSM_Material_Number__c from ISSM_Asset_Inventory__c WHERE Name='VNTEST'];
        //system.assertEquals(20, inv.ISSM_Stock_SFDC__c);

        cf_sol.ONTAP__Quantity__c = 10;
        update(cf_sol);
        inv = [SELECT id, name, ISSM_Stock_SFDC__c, ISSM_Material_Number__c from ISSM_Asset_Inventory__c WHERE id=:inv.id];
        //system.assertEquals(10, inv.ISSM_Stock_SFDC__c);

        cf_sol.ONTAP__Quantity__c = 5;
        update(cf_sol);
        inv = [SELECT id, name, ISSM_Stock_SFDC__c, ISSM_Material_Number__c from ISSM_Asset_Inventory__c WHERE id=:inv.id];
        //system.assertEquals(15, inv.ISSM_Stock_SFDC__c);
        /*
        cf_sol.ISSM_ApprovalStatus__c = 'Rejected';
        update(cf_sol);
        inv = [SELECT id, name, ISSM_Stock_SFDC__c, ISSM_Material_Number__c FROM ISSM_Asset_Inventory__c WHERE id=:inv.id];
        system.assertEquals('Rejected', cf_sol.ISSM_ApprovalStatus__c);
        system.assertEquals(20, inv.ISSM_Stock_SFDC__c);
        */
        system.debug('***ISSMCAM FIN DE ASSERTS');
        //VALIDACION CreateCaseForceHandler

        List <ONTAP__Case_Force__c> caseForce_list = new List <ONTAP__Case_Force__c>();
        ONTAP__Case_Force__c objInsertCaseForce =
            new ONTAP__Case_Force__c(
                ONTAP__Description__c ='Description',
                ONTAP__Subject__c = 'Asunto',
                ONTAP__Status__c = 'Open'
            );

        insert objInsertCaseForce;

        ONTAP__Case_Force__c case1 = new ONTAP__Case_Force__c();
        case1.Id =  objInsertCaseForce.Id;
        case1.ONTAP__Status__c = 'Open';
        case1.ONTAP__Description__c = 'Description1';
        case1.ONTAP__Subject__c = 'subjectEdit';
        caseForce_list.add(case1);

        ISSM_CreateCaseForceInCaseHandler_cls handler = new ISSM_CreateCaseForceInCaseHandler_cls();
        //handler.CreateCaseForceInCase(caseForce_list);
        handler.UodateToJSon(caseforce_list,caseforce_list );
        test.stopTest();
    }
		static testMethod void runTestAccountBlacklist(){
            test.startTest();
				List<Account> lstAccs = new List<Account>([SELECT id FROM Account WHERE Name in ('ACCTEST1','ACCTEST2') Limit 1]);
				ISSM_CAM_cls.isBlacklisted(lstAccs);
            test.stopTest();    
			}
    static testMethod void runTestCaseForceCaseValidation(){
        test.startTest();
        ONTAP__Case_Force__C testCF = [SELECT id FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Retiro'];
        ISSM_Asset__c testARecType = new ISSM_Asset__c(ISSM_CutOverReason__c = 'Incorrect Asset Number');
        insert testARecType;
        system.debug('**ISSM CAM: Tipo de registro:');
        ISSM_Case_Force_Asset__c testCFA = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCF.id, ISSM_Asset__c=testARecType.id);
        try{
            insert testCFA;
        }catch(DmlException e){
            system.debug(e.getMessage());
            system.debug(e.getMessage().contains('FIELD_CUSTOM'));
            //system.assert(e.getMessage().contains('FIELD_CUSTOM'), e.getMessage());
            //System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Case_Force__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }

        system.debug('**ISSM CAM: NOT APPROVED:');
        ONTAP__Case_Force__c testCF2 = [SELECT id FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Solicitud'];
        ISSM_Case_Force_Asset__c testCFA2 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCF2.id, ISSM_Asset__C=testARecType.id);
        try{
            insert testCFA2;
        }catch(DmlException e){
            system.debug(e.getMessage());
            // system.assert(e.getMessage().contains('In progress'), e.getMessage());
            // System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Case_Force__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }
        test.stopTest();

    }

    static testMethod void runTestUpserInventory(){
        test.startTest();
        ISSM_CAM_cls.upsertCAMInventory();
        test.stopTest();
    }
    static testMethod void runTestCaseForceAssetValidation(){

        list<ISSM_Case_Force_Asset__c> lstCaseForceAsset = new list<ISSM_Case_Force_Asset__c>();
        test.startTest();
        ISSM_Asset__c testA = [SELECT id, name FROM ISSM_Asset__C WHERE Name='Validation' LIMIT 1];

        system.debug('**ISSM CAM 3: Aprobado');
        ONTAP__Case_Force__c testCFApproved = [SELECT id FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Solicitud'];
        testCFApproved.ISSM_ApprovalStatus__c='Approved';
        update testCFApproved;
        ISSM_Case_Force_Asset__c testCFA3 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCFApproved.id, ISSM_Asset__c = testA.id);
        try{
            insert testCFA3;
        }catch(DmlException e){
            system.debug(e.getMessage());
            // system.assert(e.getMessage().contains('Validation'), e.getMessage());
            // System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Asset__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }

        system.debug('**ISSM CAM bloque: Sales Off  Incorrecto');
        ISSM_Asset__c testADifSalesOff = [SELECT id, ISSM_Centre__c FROM ISSM_Asset__C WHERE Name='DiffSalesOff'];
        ISSM_Case_Force_Asset__c testCFADiffSalesOrg = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCFApproved.id, ISSM_Asset__c = testADifSalesOff.id);
        try{
            insert testCFADiffSalesOrg;
        }catch(DmlException e){
            system.debug(e.getMessage());
            //system.assert(e.getMessage().contains('Sales Office'), e.getMessage());
            // System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Asset__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }


        system.debug('**ISSM CAM 4 bloque: SKU Incorrecto');
        testA.ISSM_Status_SFDC__c='Free Use';
        testA.ISSM_Material_number__c='8000000';
        update testA;
        ISSM_Case_Force_Asset__c testCFA4 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCFApproved.id, ISSM_Asset__c = testA.id);
        try{
            insert testCFA4;
        }catch(DmlException e){
            system.debug(e.getMessage());
            // system.assert(e.getMessage().contains('8000000'), e.getMessage());
            // System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Asset__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }

        system.debug('**ISSM CAM 5 bloque: Estatus To deliver');
        testA.ISSM_Material_number__c='8000300';
        update testA;
        ISSM_Case_Force_Asset__c testCFA5 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCFApproved.id, ISSM_Asset__c = testA.id);
        try{
            insert testCFA5;
            //system.assertEquals('To deliver', [SELECT ISSM_Status_SFDC__c FROM ISSM_Asset__c WHERE id=:testA.Id].ISSM_Status_SFDC__c);
        }catch(DmlException e){
            system.debug(e.getMessage());
        }

        system.debug('**ISSM CAM 6 bloque: RecTYpe Decommission but Cooler not in Validation');
        ONTAP__Case_Force__c testCF3 = [SELECT id FROM ONTAP__Case_Force__C WHERE ONTAP__Subject__c='Decommission1'];
        ISSM_Case_Force_Asset__c testCFA6 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCF3.id, ISSM_Asset__c = testA.id);
        try{
            insert testCFA6;
        }catch(DmlException e){
            system.debug(e.getMessage());
            //system.assert(e.getMessage().contains('To deliver'), e.getMessage());
            //System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Asset__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }

        system.debug('**ISSM CAM 7 bloque: Asset - Validation -> Asset - Decommission Request');
        testA.ISSM_Status_SFDC__c='Validation';
        update testA;
        ISSM_Case_Force_Asset__c testCFA7 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCF3.id, ISSM_Asset__c = testA.id);
        try{
            system.debug('CASO BAJA, ASSET VALIDATION');
            insert testCFA7;
            //system.assertEquals('Decommission request', [SELECT ISSM_Status_SFDC__c FROM ISSM_Asset__c WHERE id=:testA.Id].ISSM_Status_SFDC__c, 'Decommission request FAILED!');
        }catch(DmlException e){
            system.debug(e.getMessage());
        }

        system.debug('**ISSM CAM 8 bloque: Tratar de asignar más coolers de los solicitados');
        ISSM_Asset__c testA2 = [SELECT id, name FROM ISSM_Asset__C WHERE Name='Free_use' LIMIT 1];
        ISSM_Case_Force_Asset__c testCFA8 = new ISSM_Case_Force_Asset__c(ISSM_Case_Force__c = testCFApproved.id, ISSM_Asset__c = testA2.id);
        try{
            insert testCFA8;
        }catch(DmlException e){
            system.debug(e.getMessage());
            //system.assert(e.getMessage().contains('To deliver'), e.getMessage());
            // System.assertEquals(ISSM_Case_Force_Asset__c.ISSM_Asset__c, e.getDmlFields(0)[0]);
            //system.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION', e.getDmlStatusCode(0));
        }
        test.stopTest();
    }

    static testmethod void runassignTypMatrix(){
        test.startTest();
        list<ONTAP__Case_Force__c> lstCF = new list<ONTAP__Case_Force__c>([
            SELECT id, recordtypeid FROM ONTAP__Case_Force__c WHERE ontap__subject__c like 'Solicitud%' LIMIT 1]);
        ISSM_CAM_Approvers__c camapprovers2 =
            new ISSM_CAM_Approvers__c (Name='CAM_Asset_Decommission', Approver1__C='omar.cruz@gmodelo.com.mx.test', Approver2__C='omar.cruz@gmodelo.com.mx.test',
                                       Approver3__C='omar.cruz@gmodelo.com.mx.test', Approver4__C='omar.cruz@gmodelo.com.mx.test');
        insert camapprovers2;
        System.debug(loggingLevel.Error, '*** lstCF.size(): ' + lstCF.size());
        
            ISSM_CAM_cls.retCAMRecords(lstCF);
        test.stopTest();
    }

}