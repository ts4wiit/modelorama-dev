/**
 * Test class for AllMobileApplicationBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileApplicationBatchTest {

	/**
	 * Method to setup data.
	 */
	@testSetup
	static void setup() {

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;
	}

	/**
	 * Method to test AllMobileApplicationBatchClass.
	 */
	static testMethod void testAllMobileApplicationBatchClass() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		AllMobileApplicationBatchClass objAllMobileApplicationBatchClass = new AllMobileApplicationBatchClass();
		Id idObjAllMobileApplicationBatchClass = Database.executeBatch(objAllMobileApplicationBatchClass);

		//Stop test.
		Test.stopTest();
	}
}