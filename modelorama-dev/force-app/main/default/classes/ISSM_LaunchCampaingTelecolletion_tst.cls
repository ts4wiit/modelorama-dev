/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_LaunchCampaingTelecolletion_tst {
    public static  List<ISSM_AppSetting_cs__c> AppSetting;
    public static  List<ISSM_TriggerFactory__c> AppTriggerFactory;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        ApexPages.StandardController sc;
        ISSM_LaunchCampaingTelecolletion_ctr objLaunchCampaingTelecolletion = new ISSM_LaunchCampaingTelecolletion_ctr(sc);
        //User createUser = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        
        //Generamos campaña  Telecollection_Campaign__c
        Telecollection_Campaign__c objCreateCampaign = new Telecollection_Campaign__c();
        objCreateCampaign.Name = 'EjemploCampaignTest';
        objCreateCampaign.Start_Date__c = System.today();
        objCreateCampaign.End_Date__c =  System.today();
        objCreateCampaign.Active__c = true;
        insert objCreateCampaign;
        
        // Cretae configuracion personalizada 
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup) {
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        
        //Generamos RegionalSalesdivision
        String RecordTypeRegional = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();        
        Account objCreateRegionalDiv  = new Account();
        objCreateRegionalDiv.Name ='RegionaSales';
        objCreateRegionalDiv.RecordTypeId = RecordTypeRegional;
        insert  objCreateRegionalDiv;
        
        //Generamos SalesOrg
        String RecordTypeSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();        
        Account objTypeSalesOrg  = new Account();
        objTypeSalesOrg.Name ='SalesOrg';
        objTypeSalesOrg.ISSM_ParentAccount__c = objCreateRegionalDiv.Id;
        objTypeSalesOrg.RecordTypeId =RecordTypeSalesOrg;
         insert objTypeSalesOrg;
        
        //Generamos SalesOffice
        String RecordTypeSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        Account ObjTypeSalesOffice  = new Account();
        ObjTypeSalesOffice.Name ='SalesOffcie';
        ObjTypeSalesOffice.ISSM_ParentAccount__c = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.ParentId = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.RecordTypeId = RecordTypeSalesOffice;
        insert ObjTypeSalesOffice;

        //Generamos cuenta para los filtros
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();        
        Account objCreateAccount  = new Account();
        objCreateAccount.Name = 'Cuenta ejemplo';
        objCreateAccount.ONTAP__Classification__c ='Botella Abierta';
        objCreateAccount.ISSM_RegionalSalesDivision__c = objCreateRegionalDiv.Id;
        objCreateAccount.ISSM_SalesOffice__c = ObjTypeSalesOffice.Id;
        objCreateAccount.ISSM_SalesOrg__c = objTypeSalesOrg.Id;
        objCreateAccount.RecordTypeId = RecordTypeAccountId;
        insert objCreateAccount;
        
        ISSM_OpenItemB__c openItem = new ISSM_OpenItemB__c();
        openItem.ISSM_Account__c = objCreateAccount.Id;
        openItem.ISSM_DueDate__c = date.today().addDays(-50);
        openItem.ISSM_Amounts__c = 100;
        insert openItem;
        
		ISSM_ParametersOpenItem__c paramOI = new ISSM_ParametersOpenItem__c();
        paramOI.Name = 'Config1';
        paramOI.ISSM_SaldoVencidoMayorA__c = 1;
        paramOI.ISSM_RangoMinimoAceptado__c = 8;
        paramOI.ISSM_RangoMaximoAceptado__c = 90;
        paramOI.ISSM_Active__c = true;
        insert paramOI;
        
        //AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        AppTriggerFactory = ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User usr = new User(LastName = 'zxcv', FirstName='asdf', Alias = 'zxca', Email = 'kmkjh@asdf.com', Username = 'kmkjh@aslp.com', ProfileId = profileId.id, TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8', LocaleSidKey = 'en_US', Country = 'MX');
        insert usr;
        
        ISSM_AppSetting_cs__c appSett = new ISSM_AppSetting_cs__c();
        appSett.ISSM_IdQueueTelecolletion__c = usr.Id;
        insert appSett;
        
        Test.startTest();
            ISSM_LaunchCampaingTelecolletion_ctr objlaunch = new ISSM_LaunchCampaingTelecolletion_ctr();
            ISSM_LaunchCampaingTelecolletion_ctr.LstOptionsCustomSet(); 
            //Sin filtro
            ISSM_LaunchCampaingTelecolletion_ctr.GenerateCampaingFinal('EjemploCampaignTest',String.valueOf(System.today()),String.valueOf(System.today()),true,objCreateCampaign.Id,'','','','');
            //con filtro
            ISSM_LaunchCampaingTelecolletion_ctr.GenerateCampaingFinal('EjemploCampaignTest1',String.valueOf(System.today()),String.valueOf(System.today()),true,objCreateCampaign.Id,'ONTAP__Classification__c;List,Botella Abierta¡!',String.valueOf(objCreateRegionalDiv.Id),'','');
            ISSM_LaunchCampaingTelecolletion_ctr.GenerateCampaingFinal('EjemploCampaignTest2',String.valueOf(System.today()),String.valueOf(System.today()),true,objCreateCampaign.Id,'','',String.valueOf(objTypeSalesOrg.Id),'');  
            ISSM_LaunchCampaingTelecolletion_ctr.GenerateCampaingFinal('EjemploCampaignTest3',String.valueOf(System.today()),String.valueOf(System.today()),true,objCreateCampaign.Id,'','','',String.valueOf(ObjTypeSalesOffice.Id));         
            ISSM_LaunchCampaingTelecolletion_ctr.getDRVs(); 
            ISSM_LaunchCampaingTelecolletion_ctr.getAccountsByParentId(ObjTypeSalesOffice.ISSM_ParentAccount__c,true);
            ISSM_LaunchCampaingTelecolletion_ctr.getPicklistValues('ISSM_RegionalSalesDivision__c');
        Test.stopTest();
        
        
    }
}