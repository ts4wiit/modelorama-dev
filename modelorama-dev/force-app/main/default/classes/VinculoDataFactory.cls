@isTest

public class VinculoDataFactory
{
    public static Account createBusinessman(){
        
        Date myDate = date.newinstance(1960, 2, 17);
        
        Account businessman = new Account(Name = 'Ricardo', 
                                         MDRM_LastName__c = 'Rojas', 
                                         ONTAP__PostalCode__c = '12345',
                                         ONTAP__Colony__c = 'COLONIA 1', 
                                         ONTAP__Province__c = 'SI', 
                                         ONTAP__Municipality__c = 'SI', 
                                         ONTAP__Street__c = 'street', 
                                         ONTAP__Street_Number__c = '23', 
                                         ONTAP__Email__c = 'ric@acc.com');   
        insert businessman;
        return businessman;
    }
    
    public static Account createModelorama(){
        
        Account modeloramas = new Account(z019__c = '55555', 
                                          Name = 'HERON MODELORAMA', 
                                          ONTAP__Province__c ='VERACRUZ DE IGNACIO DE LA LLAVE', 
                                          ONTAP__PostalCode__c = '12345', 
                                          ONTAP__Municipality__c = 'VER', 
                                          ONTAP__Colony__c = 'SELECCIONE UNA OPCIÓN:', 
                                          ONTAP__Street__c = 'CALLE', 
                                          ONTAP__Street_Number__c = '1234567890');
               insert modeloramas;
        	   return modeloramas;                              
    
    }
    
    public static MDRM_Employee__c createEmployee(){
         Date myDateEmployee = date.newinstance(1960, 2, 17);
        
        MDRM_Employee__c employee = new MDRM_Employee__c(Name = 'Roberto',
                                               MDRM_LastName__c = 'Lopez',
                                               MDRM_MotherSurname__c = 'Perez',
                                               MDRM_Birthdate__c = myDateEmployee,
                                               MDRM_Gender__c = 'MASCULINO');
               insert employee;
        	   return employee;                              
    
    }
    
    public static MDRM_Employee__c createEmployeeEmpty(){
        
        MDRM_Employee__c employeeEmpty = new MDRM_Employee__c(Name = '',
                                               MDRM_LastName__c = '',
                                               MDRM_MotherSurname__c = '',
                                               MDRM_Birthdate__c = null,
                                               MDRM_Gender__c = '');
               insert employeeEmpty;
        	   return employeeEmpty;                              
    
    }
    
    public static MDRM_Vinculo__c createVinculoMdrmEnterprise(Account Businessman, Account Modeloramas){
        
        MDRM_Vinculo__c newVinculo = new MDRM_Vinculo__c(MDRM_Businessman__c = Businessman.Id, 
                                               MDRM_Expansor__c = Modeloramas.Id);
        
        insert newVinculo;
        return newVinculo;
    }
    
    public static MDRM_Businessman_Expansor__c createExpansorMdrmEnterprise(Account Businessman, Account Modeloramas){
        
        MDRM_Businessman_Expansor__c newExpasor = new MDRM_Businessman_Expansor__c(MDRM_Businessman__c = Businessman.Id, 
                                               									   MDRM_Expansor__c = Modeloramas.Id);
        
        insert newExpasor;
        return newExpasor;
    }
    
}