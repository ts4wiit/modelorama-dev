/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Batch Class that allows clone the records for condition

    Information about changes (versions)
    ===============================================================================
    No.    Date             	Author                      Description
    1.0    30-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
global class TRM_CloneCondition_bch implements Database.Batchable<sObject>, Database.stateful{
	
	global String StrQuery;
	global String IdCondClass;
	global String IdNewCondClass;
	
	//Constructor Method
	global TRM_CloneCondition_bch(String IdCC, String IdNCC) {
		IdCondClass 	= IdCC;
		IdNewCondClass 	= IdNCC;
	}
	
	//Start the cloning run, consulting the condition records to be cloned
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('####################START BATCH: TRM_CloneCondition_bch####################');

		StrQuery= 'SELECT ';
		StrQuery+= String.join(new List<String>(Schema.getGlobalDescribe().get('TRM_ConditionRecord__c').getDescribe().fields.getMap().keySet()),', ');
        StrQuery+=' FROM TRM_ConditionRecord__c';
		StrQuery+=' WHERE TRM_ConditionClass__c = \'' + IdCondClass + '\'';
		
		return Database.getQueryLocator(StrQuery);
	}

	//Execute cloning of condition records and their related scales
   	global void execute(Database.BatchableContext BC, List<TRM_ConditionRecord__c> lstCndRec) {
   		System.debug('####################EXECUTE BATCH: TRM_CloneCondition_bch####################');

   		TRM_ConditionRecord__c[] lstNewCondRce = new List<TRM_ConditionRecord__c>();
   		Map<String,String> MapCR = new Map<String,String>();
   		Integer acum=0;

   		for(TRM_ConditionRecord__c objCR : lstCndRec){
   			TRM_ConditionRecord__c newCR 	= new TRM_ConditionRecord__c();
			newCR.TRM_DummyKey__c			= objCR.Name+'R'+acum++;
			newCR.TRM_AmountPercentage__c 	= objCR.TRM_AmountPercentage__c;
			newCR.TRM_ConditionClass__c		= IdNewCondClass;
			newCR.TRM_Customer__c			= objCR.TRM_Customer__c;
			newCR.TRM_DistributionChannel__c= objCR.TRM_DistributionChannel__c;
			newCR.TRM_PreviousAmount__c 	= objCR.TRM_AmountPercentage__c;
			newCR.TRM_Product__c 			= objCR.TRM_Product__c;
			newCR.RecordTypeId 				= objCR.RecordTypeId;
			newCR.TRM_SalesOffice__c 		= objCR.TRM_SalesOffice__c;
			newCR.TRM_Sector__c 			= objCR.TRM_Sector__c;
			newCR.TRM_Segment__c 			= objCR.TRM_Segment__c;
			newCR.TRM_StatePerZone__c 		= objCR.TRM_StatePerZone__c;
			newCR.TRM_UnitMeasure__c 		= objCR.TRM_UnitMeasure__c;
			newCR.TRM_Utilization__c 		= objCR.TRM_Utilization__c;

			lstNewCondRce.add(newCR);
			MapCR.put(objCR.Id, newCR.TRM_DummyKey__c);
   		}

   		insert lstNewCondRce;

   		TRM_ProductScales__c[] lstCondScales = new List<TRM_ProductScales__c>();
   		TRM_ProductScales__c[] lstNewCondScales = new List<TRM_ProductScales__c>();

        lstCondScales = [SELECT Id, TRM_ConditionRecord__c, TRM_Quantity__c, TRM_AmountPercentage__c 
        				 FROM 	TRM_ProductScales__c 
        				 WHERE 	TRM_ConditionRecord__c IN: MapCR.keySet()];

   		for(TRM_ProductScales__c objCndScal : lstCondScales){   
   			TRM_ProductScales__c newCRS 	= new TRM_ProductScales__c();
   			newCRS.TRM_Quantity__c 			= objCndScal.TRM_Quantity__c;
   			newCRS.TRM_AmountPercentage__c 	= objCndScal.TRM_AmountPercentage__c;
   			newCRS.TRM_ConditionRecord__r 	= new TRM_ConditionRecord__c(TRM_DummyKey__c=MapCR.get(objCndScal.TRM_ConditionRecord__c));
   			lstNewCondScales.add(newCRS);
   		}

   		insert lstNewCondScales;
	}
	
	//Finish the process by lighting a flag that indicates that the record is already available for manipulation
	global void finish(Database.BatchableContext BC) {
		System.debug('####################FINISH BATCH: TRM_CloneCondition_bch####################');
		
		TRM_ConditionClass__c objCnClass = new TRM_ConditionClass__c();
		objCnClass = [SELECT TRM_ReadyForEdit__c FROM TRM_ConditionClass__c WHERE Id=: IdNewCondClass];
		objCnClass.TRM_ReadyForEdit__c = true;
		update objCnClass;
	}
}