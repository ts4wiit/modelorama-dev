@IsTest
public class ISSM_CAM_UpdateInventory_sch_tst {
    static testMethod void testUpdateInventory_sch(){
        String strCron = '0 0 23 * * ?';
        List<AsyncApexJob> jobsBefore = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob];
        System.assertEquals(0, jobsBefore.size(), 'not expecting any asyncjobs');
        
        Test.startTest();
        ISSM_CAM_UpdateInventory_sch sch = new ISSM_CAM_UpdateInventory_sch();
        system.schedule('schUpdateInventory', strCron, sch);
        Test.stopTest();
        
        //// Check schedulable is in the job list
        system.debug([select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob]);
        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('ISSM_CAM_UpdateInventory_sch', jobsScheduled[0].ApexClass.Name, 'expecting specific ScheduledApex');
    }
}