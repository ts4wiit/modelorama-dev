public class MDRM_BusinessmanStage_ext {
    
    public ApexPages.StandardController stdController;
    public Account acc {get;set;}    
    public String MDRM_Stage {get;set;} 
    public String MDRM_StageInitial {get;set;} 
    public String mode {get;set;}   
    public string displayValue {get;set;}
    public String redirectUrl {get;set;}
    public Boolean shouldRedirect {get;set;}
    
    
    
    
    public MDRM_BusinessmanStage_ext(ApexPages.StandardController sc){
        
        stdController = sc;
        acc = (Account)stdController.getRecord();
        
        loadInicialData();
        
        mode = 'maindetail';
        
        shouldRedirect = false;
        displayValue = 'none';
        redirectUrl = stdController.view().getUrl();
    }
    
    public void switchState(){
        mode = mode == 'maindetail'?'edit':'maindetail';   
        displayValue = displayValue == 'none'?'display':'none';
    }
    
    public void editStage(){
        loadInicialData();
        switchState();        
    }
    
    public PageReference cancelStage(){
        //loadInicialData();
        //switchState();  
        shouldRedirect = true;
        redirectUrl = stdController.view().getUrl();
        
        return null;
    }
    
    public PageReference saveStage(){
        try{
            update acc;
            loadInicialData();
            switchState();
            shouldRedirect = true;
            redirectUrl = stdController.view().getUrl();
            
        }catch (exception e){
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            ApexPages.addMessage(msg);
            
        }
        
        return null;
        
    }
    
    public void assignStage(){
        acc.MDRM_Stage__c = MDRM_Stage;
    } 
    
    public void loadInicialData(){
        if(acc.Id != null){
            acc = [SELECT Id, MDRM_Stage__c,MDRM_Substage__c,MDRM_Documents__c,ONTAP__Account_Status__c,MDRM_Substatus__c,MDRM_Psychometry_User__c,MDRM_Psychometry_Password__c FROM Account WHERE Id =: acc.Id  LIMIT 1];
            MDRM_StageInitial = acc.MDRM_Stage__c;
            MDRM_Stage = '';
        }
    }
       
    public List<SelectOption> getPicklistValuesOpAccountStages() {
        
		list<Schema.PicklistEntry> values = getLstPicklistEntry('MDRM_Stage__c');
        List<SelectOption> lstSort = new List<SelectOption>();
        //Get the order and dependencies
        List<MDRM_AccountStages__c> lstMDRM_AccountStages = database.query('SELECT MDRM_Stage__c FROM MDRM_AccountStages__c WHERE MDRM_Stage__c != null  ORDER BY Position__c ASC NULLS FIRST');
        Map<String,String> mapValues = new Map<String,String>();
        for (Schema.PicklistEntry a : values){
            mapValues.put(a.getValue(), a.getLabel());
        }
        
        // Add these values to the sort list.        
        for (integer i=0 ; i<lstMDRM_AccountStages.size() ; i++){  
            if(MDRM_StageInitial == lstMDRM_AccountStages[i].MDRM_Stage__c){
                lstSort.add( new SelectOption(lstMDRM_AccountStages[i].MDRM_Stage__c,mapValues.get(lstMDRM_AccountStages[i].MDRM_Stage__c))  );
                i++;
                if(i<lstMDRM_AccountStages.size()){                    
                    lstSort.add( new SelectOption(lstMDRM_AccountStages[i].MDRM_Stage__c,mapValues.get(lstMDRM_AccountStages[i].MDRM_Stage__c))  );
                } 
            }
        }
        return lstSort;
    }
    
    public static list<Schema.PicklistEntry> getLstPicklistEntry(String fld){
        // Get the object type of the SObject.
        Schema.sObjectType objType = Account.getSObjectType(); 
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();        

        System.debug('No of values: ' + values.size());
        return values;
    } 
   
   
    
}