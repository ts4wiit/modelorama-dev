/****************************************************************************************************
    General Information
    -------------------
    author:     Fernando Engel
    email:      fernando.engel.funes@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Class for site control

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       05/05/2018      Fernando Engel  FE           Class created to control info displayed
    ================================================================================================
****************************************************************************************************/
public class MDRM_Vision360_ctr {
    public Account Expansor {get;set;}
    public Account Businessman {get;set;}
    public Contact Con {get;set;}
    public List<MDRM_Modelorama_Performance__c> lstModelorama_Performance {get;set;}
    public List<MDRM_Modelorama_Performance__c> lstModelorama_PerformanceFault {get;set;}
    public Map<Decimal, dataPerformance> mapDataPerformance {get;set;}
    public List<dataPerformance> lstDataPerformance {get;set;}
    public boolean noError{get;set;}
    public String selectedYear {get;set;}
    public String selectedZ001 {get;set;}
    public String selectedZ019 {get;set;}
    public List<SelectOption> lstZ019 {get;set;}
    
    public MDRM_Vision360_ctr(){
        init();
    }
    
    public void init(){
        noError = false;
        Businessman = new Account();        
        lstZ019 = new List<SelectOption>();
        lstZ019.add( genPickListLabeldefaultVal( SObjectType.MDRM_Businessman_Expansor__c.getLabel() ));
        
        /*
        selectedZ001 = apexpages.currentpage().getparameters().get('MDRM_Z001');
        selectedZ019 = apexpages.currentpage().getparameters().get('MDRM_Z019');
        selectedYear = apexpages.currentpage().getparameters().get('MDRM_Year');
		*/
        
        selectedZ001 = apexpages.currentpage().getHeaders().get('MDRM_Z001');
        selectedZ019  = apexpages.currentpage().getHeaders().get('MDRM_Z019');
        selectedYear  = apexpages.currentpage().getHeaders().get('MDRM_Year');
        
       
        if(selectedZ019 != null){
            genInfoBusinessman();
            genConInfo();
            genInformation();
            genZ019PickList();
        }
        
    }
    
    public void genZ019PickList(){
        for(Account z019 : [SELECT Z019__c FROM Account WHERE ParentId =: Businessman.id ORDER BY Z019__c ASC  ]){
            lstZ019.add(new SelectOption( String.valueOf(z019.Z019__c) ,String.valueOf(z019.Z019__c) ));
        }
        lstZ019.add(new SelectOption( label.MDRM_All , label.MDRM_All ));
        selectedZ019 = '';
    }
    
    public void genInfoBusinessman(){
        try{
            String strOrignData = selectedZ019 == Label.MDRM_All ? ' WHERE Parent.Z01__c =: selectedZ01 ' : ' WHERE Z019__c =: selectedZ019 ' ;
            Expansor = database.query(' SELECT ID,'+
                                      ' Z019__c, '+
                                      ' Parent.Z001__c, '+         
                                      ' ParentId '+
                                      ' FROM Account  '+
                                      strOrignData+
                                      ' LIMIT 1');
            
            Businessman = [SELECT ID,
                           Name, 
                           Z001__c,
                           MDRM_RFC__c,
                           Agri_CURP__c,
                           ONTAP__Street__c,
                           ONTAP__Street_Number__c,
                           ONTAP__PostalCode__c,
                           ONTAP__Province__c,
                           ONTAP__Municipality__c,
                           ONTAP__Email__c 
                           FROM Account 
                           WHERE id =: Expansor.ParentId 
                           LIMIT 1];
            selectedZ001 = Businessman.Z001__c;            
        }catch(Exception ex){
            system.debug('ERROR: '+ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.MDRM_UserNotFound ));
            noError = false;
        }
        
    }
    
    public void genConInfo(){
        try{
            Con = [SELECT Id,Name,Phone FROM Contact WHERE AccountId =: Businessman.id  LIMIT 1];
        }catch(Exception ex){
            System.debug('Contact ERROR: '+ex);
            con = new Contact();
        }
    }
    
    public void genInformation(){
        try{
            lstModelorama_Performance = new List<MDRM_Modelorama_Performance__c>();
            lstModelorama_PerformanceFault = new List<MDRM_Modelorama_Performance__c>();
            mapDataPerformance = new Map<Decimal, dataPerformance>();
            lstDataPerformance = new List<dataPerformance>();
            
            if(  selectedYear != null && selectedYear.isNumeric() ){
                
                integer intSelectedYear = selectedYear == '' ? 0 : Integer.valueOf( selectedYear );
                String BusinessmanId = Businessman.id;
                String strQuery = 'SELECT id,'+
                    'Name,'+
                    'MDRM_Actual_Volume__c,'+
                    'MDRM_Bond__c,'+
                    'MDRM_Businessman_Expansor__c,'+
                    'MDRM_Businessman_Expansor__r.MDRM_Expansor__r.Z019__c,'+
                    'MDRM_Challenges_Monthly__c,'+
                    'MDRM_Execution__c,'+
                    'MDRM_Month__c,'+
                    'MDRM_Month_fx__c,'+
                    'MDRM_No_Selling_Alcohol_to_Minors__c,'+
                    'MDRM_Share__c,'+
                    'MDRM_Year__c '+
                    'FROM MDRM_Modelorama_Performance__c '+
                    'WHERE MDRM_Businessman_Expansor__r.MDRM_Expansor__r.ParentId =: BusinessmanId ' +
                    'AND MDRM_Year__c =: intSelectedYear  ';
                String strOrderQuery = 'ORDER  BY MDRM_Z019__c ASC, MDRM_Month__c ASC  NULLS LAST LIMIT 12';
                String strSelectedZ019 = selectedZ019 == label.MDRM_All ? '' : 'AND MDRM_Businessman_Expansor__r.MDRM_Expansor__r.Z019__c =: selectedZ019 ' ;
                
                for(Decimal i=1; i<=12; i++){
                    mapDataPerformance.put(i, new dataPerformance( i , 0 ));
                }
                
                lstModelorama_Performance = Database.query( strQuery + strSelectedZ019 + strOrderQuery );
                
                
                lstModelorama_PerformanceFault = Database.query( strQuery + strSelectedZ019 + 'AND MDRM_No_Selling_Alcohol_to_Minors__c != null  ' + strOrderQuery );
                
                for(MDRM_Modelorama_Performance__c MP : lstModelorama_Performance ){
                    mapDataPerformance.put( MP.MDRM_Month__c , new dataPerformance( MP.MDRM_Month__c, mapDataPerformance.get(MP.MDRM_Month__c).Bonus + MP.MDRM_Bond__c) ) ;
                }
                
                
                for(dataPerformance DP : mapDataPerformance.values() ){
                    lstDataPerformance.add( DP );
                }
            }
            noError = true;	
        }catch(Exception ex){
            system.debug('ERROR: '+ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.MDRM_UserNotFound ));
            noError = false;
        }
        
    }
    
    public Class dataPerformance{
        public Decimal Month {get;set;}
        public Decimal Bonus {get;set;}
        public String txtMonth {get;set;}
        
        public dataPerformance(Decimal Month, Decimal Bonus){
            this.Month = Month;
            this.Bonus = Bonus;
            this.txtMonth = monthDecimalToText(Month);
        }
        public String monthDecimalToText(Decimal month){
            if(month == 1){
                return Label.MDRM_January;
            }else if(month == 2){
                return Label.MDRM_February;
            }else if(month == 3){
                return Label.MDRM_March;
            }else if(month == 4){
                return Label.MDRM_Abril;
            }else if(month == 5){
                return Label.MDRM_May;
            }else if(month == 6){
                return Label.MDRM_June;
            }else if(month == 7){
                return Label.MDRM_July;
            }else if(month == 8){
                return Label.MDRM_August;
            }else if(month == 9){
                return Label.MDRM_September;
            }else if(month == 10){
                return Label.MDRM_October;
            }else if(month == 11){
                return Label.MDRM_November;
            }else if(month == 12){
                return Label.MDRM_December;
            }
            return '';
        }
    }
    
    public SelectOption genPickListLabeldefaultVal(String val){
        String yaerLabel = val;
        String defaultVal = '-- ' + Label.MDRM_Select + ' ' + yaerLabel + ' --';
        return new SelectOption( '' , defaultVal );
    }
    
    public List<SelectOption> getYears() {
        List<SelectOption> options = new List<SelectOption>();
        options.add( genPickListLabeldefaultVal(Schema.SObjectType.MDRM_Modelorama_Performance__c.fields.getMap().get('MDRM_Year__c').getDescribe().getLabel()) );
        Integer Year = system.today().year();
        for( Integer y = Year; y > Year-5; y-- ){
            options.add(new SelectOption( String.valueOf(y) ,String.valueOf(y)));
        }
        return options;
    }
}