/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_CaseTrigger_tst {
    
    static testMethod void TestCreateCase() {
        ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');         
        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
       
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','New',objCaseForce.Id,1);
        ISSM_MappingFieldCase__c MappingFields = ISSM_CreateDataTest_cls.fn_CreateCaseForceMapping(true,'ISSM_CaseForceNumber__c','id');
        
        Test.startTest();
        ISSM_TriggerManager_cls.Activate();
        Case UpdateCase =  ISSM_CreateDataTest_cls.fn_UpdateCase(true,'DescripcionUpdate','AsuntoUpdate','Escalated',objCase.id);
        update UpdateCase;  
        System.assertEquals(UpdateCase.id,objCase.Id);
        Test.stopTest();
    }

    static testMethod void testAfterInsertUpdateCase_StatusClosed(){
        Test.startTest();
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
        objCaseForce.ONTAP__Description__c = 'Description';
        objCaseForce.ONTAP__Subject__c = 'Subject';
        objCaseForce.ONTAP__Status__c = 'Closed';
        insert objCaseForce;
        
        Case objCase = new Case();
        objCase.Subject = 'Subject';
        objCase.Description = 'Description';
        objCase.Status = 'Closed';
        objCase.ISSM_CaseForceNumber__c = objCaseForce.Id;
        insert objCase;
        
        Case objCase2 = new Case();
        objCase2.Subject = 'Subject2';
        objCase2.Description = 'Description2';
        objCase2.Status = 'Closed';
        objCase2.ISSM_Channel__c = 'B2B';
        objCase2.ISSM_CaseForceNumber__c = objCaseForce.Id;
        insert objCase2;
        
        objCase.Description = 'Description edited';
        update objCase;
     
        Test.stopTest();
    }
    
    static testMethod void testCreateCaseB2B(){
        Test.startTest();
        
        // Insertamos un usuario
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@test1test.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@test1test.com');
        insert user1;
        
        // Insertamos una oficina de ventas
        Id salesOfficeRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOffice'].Id;
        Account so = new Account();
        so.Name = 'Test Acc 1';
        so.RecordTypeId = salesOfficeRecordTypeId;
        insert so;
        
        // Insertamos una cuenta
        Id accountRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Account'].Id;
        Account acc = new Account();
        acc.Name = 'Test Acc 1';
        acc.RecordTypeId = accountRecordTypeId;
        acc.ISSM_SalesOffice__c = so.Id;
        acc.ONTAP__SAP_Number__c = '12345';
        insert acc;
        
        // Insertamos un case force
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
        objCaseForce.ONTAP__Description__c = 'Description';
        objCaseForce.ONTAP__Subject__c = 'Subject';
        objCaseForce.ONTAP__Status__c = 'Closed';
        insert objCaseForce;
        
        // Insertamos un caso
        Case objCase = new Case();
        objCase.Subject = 'Subject';
        objCase.Description = 'Description';
        objCase.Status = 'Closed';
        objCase.ISSM_Channel__c = 'B2B';
        objCase.ISSM_TypificationLevel1__c = 'Refrigeración';
        objCase.ISSM_TypificationLevel2__c = 'Entrega de equipo';
        objCase.AccountId = acc.Id;
        objCase.ISSM_CaseForceNumber__c = objCaseForce.Id;
        insert objCase;
        
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat1 = new ISSM_TypificationMatrix__c();
        objTypMat1.ISSM_UniqueIdentifier__c = 'Test1';
        objTypMat1.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat1.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat1.ISSM_Channel__c = 'B2B';
        objTypMat1.ISSM_AssignedTo__c = 'USER';
        objTypMat1.ISSM_OwnerUser__c = user1.Id;
        insert objTypMat1;
        
        Id queue = [SELECT Id FROM Group WHERE Type = 'Queue' LIMIT 1].Id;
        ISSM_TypificationMatrix__c objTypMat2 = new ISSM_TypificationMatrix__c();
        objTypMat2.ISSM_UniqueIdentifier__c = 'Test2';
        objTypMat2.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat2.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat2.ISSM_Channel__c = 'B2B';
        objTypMat2.ISSM_AssignedTo__c = 'QUEUE';
        objTypMat2.ISSM_OwnerQueue__c = 'ISSM_WithoutOwner';
        objTypMat2.ISSM_IdQueue__c = queue;
        insert objTypMat2;
        
        ISSM_TypificationMatrix__c objTypMat3 = new ISSM_TypificationMatrix__c();
        objTypMat3.ISSM_UniqueIdentifier__c = 'Test3';
        objTypMat3.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat3.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat3.ISSM_Channel__c = 'B2B';
        objTypMat3.ISSM_AssignedTo__c = 'SUPERVISOR';
        insert objTypMat3;
        
        ISSM_TypificationMatrix__c objTypMat4 = new ISSM_TypificationMatrix__c();
        objTypMat4.ISSM_UniqueIdentifier__c = 'Test4';
        objTypMat4.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat4.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat4.ISSM_Channel__c = 'B2B';
        objTypMat4.ISSM_AssignedTo__c = 'BILLINGMANAGER';
        insert objTypMat4;
        
        ISSM_TypificationMatrix__c objTypMat5 = new ISSM_TypificationMatrix__c();
        objTypMat5.ISSM_UniqueIdentifier__c = 'Test5';
        objTypMat5.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat5.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat5.ISSM_Channel__c = 'B2B';
        objTypMat5.ISSM_AssignedTo__c = 'TRADEMARKETING';
        insert objTypMat5;
        
        ISSM_TypificationMatrix__c objTypMat6 = new ISSM_TypificationMatrix__c();
        objTypMat6.ISSM_UniqueIdentifier__c = 'Test6';
        objTypMat6.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat6.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat6.ISSM_Channel__c = 'B2B';
        objTypMat6.ISSM_AssignedTo__c = 'MODELORAMA';
        insert objTypMat6;
        
        ISSM_TypificationMatrix__c objTypMat7 = new ISSM_TypificationMatrix__c();
        objTypMat7.ISSM_UniqueIdentifier__c = 'Test7';
        objTypMat7.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat7.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat7.ISSM_Channel__c = 'B2B';
        objTypMat7.ISSM_AssignedTo__c = 'REPARTO';
        insert objTypMat7;
        
        ISSM_TypificationMatrix__c objTypMat8 = new ISSM_TypificationMatrix__c();
        objTypMat8.ISSM_UniqueIdentifier__c = 'Test8';
        objTypMat8.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat8.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat8.ISSM_Channel__c = 'B2B';
        objTypMat8.ISSM_AssignedTo__c = 'BOSSREFRIGERATION';
        insert objTypMat8;
        
        Test.stopTest();
    }

    static testMethod void testUpdateCaseInCaseForce(){
      Test.startTest();
      ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
      objCaseForce.ONTAP__Description__c = 'Description';
      objCaseForce.ONTAP__Subject__c = 'Subject';
      objCaseForce.ONTAP__Status__c = 'New';
      insert objCaseForce;

      Case objCase = new Case();
      objCase.Subject = 'Subject';
      objCase.Description = 'Description';
      objCase.Status = 'New';
      objCase.ISSM_CaseForceNumber__c = objCaseForce.Id;
      insert objCase;

      objCase.Status = 'Working';
      update objCase;
       Test.stopTest();
    }
    
    /*static testMethod void testUpdateStatusClose(){
        Test.startTest();
        
        Case objCase = new Case();
        objCase.Subject = 'Subject';
        objCase.Description = 'Description';
        objCase.Status = 'Closed';
        objCase.ISSM_TypificationLevel1__c = 'Refrigeración';
        objCase.ISSM_TypificationLevel2__c = 'Entrega de equipo';
        insert objCase;
        
        ISSM_TypificationMatrix__c objTypMat = new ISSM_TypificationMatrix__c();
        objTypMat.ISSM_UniqueIdentifier__c = 'Test1';
        objTypMat.ISSM_TypificationLevel1__c = objCase.ISSM_TypificationLevel1__c;
        objTypMat.ISSM_TypificationLevel2__c = objCase.ISSM_TypificationLevel2__c;
        objTypMat.ISSM_Channel__c = 'B2B';
        insert objTypMat;
        
        Test.stopTest();
    }*/
    
  /*  static testMethod void testBeforeSLAinProvider(){

     Test.startTest(); 
     User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
     ISSM_TypificationMatrix__c  TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Modelorama',null,null,null,null,null,'Trade Marketing',user.id,null);
      Case objCase = new Case();
      objCase.Subject = 'Subject';
      objCase.Description = 'Description'; 
      objCase.Status = 'new';
      objCase.ISSM_TypificationNumber__c = TypificationMatrix.Id; 
      insert objCase;
	  System.debug('objCase : '+objCase);
       Test.stopTest();
    }*/
}