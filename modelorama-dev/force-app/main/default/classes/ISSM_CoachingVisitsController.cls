public with sharing class ISSM_CoachingVisitsController {
    private static final String COACHING_EVENT_RT = DevUtils_cls.getRecordTypes('Event', 'DeveloperName').get('Coaching').Id;
    public String getUserId(){
        return UserInfo.getUserId();
    }

    @AuraEnabled
    public static List<Account> getAccountCoachingEvents(String dDate, Id managerId, String[] selectedAccIdList){
        List<Id> selectedIdList = new List<Id>();
        for(String s : selectedAccIdList){
            selectedIdList.add(ID.valueOf(s));
        }
        Date selectedDate = Date.valueOf(dDate);
        Date weekStart = selectedDate.toStartofWeek();
        Date weekEnd = weekStart.addDays(5);
        List<Account> accList = new List<Account>();

        List<Event> eventResultList = [SELECT Id, WhatId, ActivityDate  
                    FROM Event 
                    WHERE OwnerId =: managerId 
                    AND RecordTypeId =:COACHING_EVENT_RT 
                    AND WhatId IN :selectedAccIdList
                    AND ActivityDate >=: weekStart 
                    AND ActivityDate <=: weekEnd 
                    ORDER BY ActivityDate ASC];
        System.debug('#### eventResultList size:' + eventResultList.size());

        List<Id> idList = new List<Id>();
        for(Event e : eventResultList){
            idList.add(e.WhatId);
        }
        System.debug('#### idList size:' + idList.size());

        Map<Id,Integer> counterMap = new Map<Id,Integer>();
        for(String key : idList) {
            if(!counterMap.containsKey(key)){
            counterMap.put(key,0);
            }
            Integer currentInt=counterMap.get(key)+1;
            counterMap.put(key,currentInt);
        }

        List<Id> accIdList = new List<Id>();
        for(Id key : counterMap.keySet()){
            if(counterMap.get(key) >= Integer.valueOf(Label.ISSM_WeekMaxCoachings)){
                accIdList.add(key);
            }
        }

        accList = [SELECT Id, Name, OwnerId FROM Account WHERE Id IN :accIdList];
        System.debug('#### accList size:' + accList.size());

        return accList;
    }

	@AuraEnabled
    public static List<Account> getToursVisitsFromUser(String tourDate, Id userId) {
        ISSM_Coaching__c coachSett = ISSM_Coaching__c.getValues(Label.ISSM_CoachingSettings);
        List<String> modelServiceValues = new List<String>();
        if(coachSett.ISSM_ApplyToAutosales__c){
            modelServiceValues.add(Label.ISSM_SMAutosales);
        }
        if(coachSett.ISSM_ApplyToBDR__c){
            modelServiceValues.add(Label.ISSM_SMBDR);
        }
        if(coachSett.ISSM_ApplyToNationalAccount__c){
            modelServiceValues.add(Label.ISSM_SMNationalAccount);
        }
        if(coachSett.ISSM_ApplyToPresales__c){
            modelServiceValues.add(Label.ISSM_SMPresales);
        }
        if(coachSett.ISSM_ApplyToTelesales__c){
            modelServiceValues.add(Label.ISSM_SMTelesales);
        }


        Date inputDate = Date.valueOf(tourDate);
        
        List<Account> accountsResult = new List<Account>();
        Set<Id> acctsIds = new Set<Id>();
        ONTAP__Tour__c tour; 
        try{
            tour = [SELECT Id, Route__r.ServiceModel__c FROM ONTAP__Tour__c WHERE 
                    Route__r.RouteManager__c =:userId 
                    AND ONTAP__TourDate__c=:inputDate LIMIT 1];
        }catch(Exception e){
            tour = null;
        }
        if(tour!=null){
            String serviceModel = tour.Route__r.ServiceModel__c;
            Boolean allowedServiceModel = modelServiceValues.contains(serviceModel);
            //if(tour.Route__r.ServiceModel__c == 'BDR'){
            if(allowedServiceModel){
                for(Event evt:[SELECT whatId FROM Event WHERE VisitList__c  =: tour.Id]){
                    if((evt.whatId+'').startsWith('001')){
                        acctsIds.add(evt.whatId);
                    }
                }
            }
            if(!acctsIds.isEmpty()){
                for(Account acct : [SELECT Id, Name, ONTAP__SAPCustomerId__c, ONTAP__SalesOffId__c  FROM Account WHERE Id in:acctsIds]){
                    accountsResult.add(acct);
                }
            }
        }
        System.debug('::::accountsResult '+accountsResult);
        return accountsResult;
    }
    @AuraEnabled
    public static List<Event> saveCoaching(String accountsJSON, String assignTo, String executionDate) {
        System.debug('::::saveCoaching');
        System.debug('accounts: '+accountsJSON);
        system.debug('assignto: '+assignTo);
        System.debug('execution: '+executionDate);
        List<Account> accounts = (List<Account>) JSON.deserialize(accountsJSON, List<Account>.class);
        List<Event> events = new List<Event>();
        Date inputDate = Date.valueOf(executionDate);
        Event tmpEvt;
        for(Account acct : accounts){
            tmpEvt = new Event(WhatId = acct.Id, OwnerId = assignTo, 
                               ActivityDate=inputDate,Subject='Visit', 
                               RecordTypeId = COACHING_EVENT_RT,DurationInMinutes =15, 
                               StartDateTime = Datetime.newInstance(inputDate.year(), inputDate.month(), inputDate.day(), 8, 0, 0), 
                               EndDateTime = Datetime.newInstance(inputDate.year(), inputDate.month(), inputDate.day(), 8, 15, 0) 
                              );
            events.add(tmpEvt);
        }
        insert events;
        return events;
    }
    @AuraEnabled
    public static List<CoachingWrapper> getCoaching(String range, String startDate, String endDate, String userId) {
        //transformation of date values comming from the lightning component
        Date sDate = Date.valueOf(startDate);
        Date eDate = Date.valueOf(endDate);
        
        String query = '';
        Map<String,Event> events = new Map<String,Event>();
        List<Event> allEvents = new List<Event>();
        Set<Id> acctsIds = new Set<Id>();
        //Maps to fill data of the wrapper
        Map<String, Set<Account>> userAccounts = new Map<String, Set<Account>>();
        Map<String, Set<Event>> eventXDate = new Map<String, Set<Event>>();
        Map<String, Boolean> coachingApplied = new Map<String, Boolean>();
        Map<String, String> userData = new Map<String, String>();
        Map<String, ONTAP__Route__c> bdrXRoute = new Map<String, ONTAP__Route__c>();
        Map<String, Account> accountById = new Map<String, Account>();
        //Wrapper to show data from 3 different objects 
        CoachingWrapper wrp;
        List<CoachingWrapper> wrpList = new List<CoachingWrapper>();
        //determine which search will be performed depending on the selection of the user: today, this week, this month or custom (two dates will be provided(sDate & eDate))
        switch on range{
            when 'CUSTOM' {
                query = 'SELECT Id,WhatId, ActivityDate,ONTAP__Estado_de_visita__c  FROM Event WHERE OwnerId =: userId AND RecordTypeId =:COACHING_EVENT_RT AND ActivityDate >=: sDate AND ActivityDate <=: eDate ORDER BY ActivityDate ASC';
            }
            when else {
                query = String.format('SELECT Id,WhatId, ActivityDate,ONTAP__Estado_de_visita__c  FROM Event WHERE OwnerId =: userId AND RecordTypeId =:COACHING_EVENT_RT AND ActivityDate = {0} ORDER BY ActivityDate ASC',
                                      new List<String>{range});
            }
        }
        //get all Events which WhatId is an Account, recordType is Coaching and the Owner is the current user
        for(Event evt : Database.query(query)){
            if(String.valueOf(evt.WhatId).startsWith('001')){
                events.put(evt.WhatId,evt);
                acctsIds.add(evt.WhatId);
                allEvents.add(evt);
            }
        }
        //get Account information from visits created for the manager
        for(Account acct : [SELECT Id, Name, OwnerId, Owner.Name FROM Account WHERE Id IN: acctsIds]){
            if(!userAccounts.containsKey(acct.OwnerId)){
                userAccounts.put(acct.OwnerId, new Set<Account>{acct});
            }else{
                userAccounts.get(acct.OwnerId).add(acct);
            }
            if(!userData.containsKey(acct.OwnerId)){
                userData.put(acct.OwnerId, acct.Owner.Name);
            }
            accountById.put(acct.Id, acct);
        }
        //get route of selected BDRs
        for(ONTAP__Route__c selRoute :[SELECT Id,ONTAP__RouteName__c,Supervisor__c,RouteManager__c FROM ONTAP__Route__c WHERE RouteManager__c IN: userAccounts.keySet() AND ServiceModel__c ='BDR']){
            bdrXRoute.put(selRoute.RouteManager__c, selRoute);
        }
        //group events by date and user: because multiple users can have multiple days but not in the same date
        for(Event evt: allEvents){
            String key = (evt.ActivityDate.format()+'#'+accountById.get(evt.WhatId).OwnerId);
            System.debug('::::'+key);
            if(!eventXDate.containsKey(key)){
                eventXDate.put(key, new Set<Event>{evt});
            }else{
                eventXDate.get(key).add(evt);
            }
        }
        //coachingApplied
        for(String evDt : eventXDate.keySet()){
            for(Event evt : eventXDate.get(evDt)){
                if(evt.ONTAP__Estado_de_visita__c =='Completado'){
                    coachingApplied.put(evDt,true);
                    break;
                }
            }
            if(!coachingApplied.containsKey(evDt)){
                coachingApplied.put(evDt,false);
            }
        }
        
        //eventXDate key is composed by activity date + # + Account Id
        for(String exd: eventXDate.keySet()){
            String myUser = exd.split('#')[1];
            String myDate = (new list<Event>(eventXDate.get(exd))[0]).ActivityDate.format();
            wrp = new CoachingWrapper(
                (new list<Event>(eventXDate.get(exd))[0]).ActivityDate,
            	userData.get(myUser),
                myUser,
                bdrXRoute.get(myUser)==null?'':bdrXRoute.get(myUser).ONTAP__RouteName__c,
                bdrXRoute.get(myUser)==null?'':'/'+bdrXRoute.get(myUser).Id,
               	eventXDate.get(exd).size(),
                coachingApplied.get(exd)?'Aplicado':'No Aplicado'
            );
            wrpList.add(wrp);
        }
        System.debug('::::wrpList'+wrpList);
        wrpList.sort();
        
        return wrpList;
    }
    public class CoachingWrapper implements Comparable{
        @AuraEnabled
        public String bdr {get;set;}
        @AuraEnabled
        public Date coachingDate {get;set;}
        @AuraEnabled
        public String bdrId {get;set;}
        @AuraEnabled
        public String route {get;set;}
        @AuraEnabled
        public String routeId {get;set;}
        @AuraEnabled
        public Integer numberOfVisits {get;set;}
        @AuraEnabled
        public String coachingApplied {get;set;}
        public CoachingWrapper(){}
        public CoachingWrapper(Date coachingDate, String bdr, String bdrId,String route,String routeId,Integer numberOfVisits,String coachingApplied){
            this.coachingDate = coachingDate.addDays(1);
            this.bdr = bdr;
            this.bdrId = bdrId;
            this.route = route;
            this.routeId = routeId;
            this.numberOfVisits = numberOfVisits;
            this.coachingApplied = coachingApplied;
        }
        public Integer compareTo(Object compareTo) {
            CoachingWrapper cWrapper = (CoachingWrapper) compareTo;
            if (Date.valueOf(coachingDate) == Date.valueOf(cWrapper.coachingDate)) return 0;
            if (Date.valueOf(coachingDate) > Date.valueOf(cWrapper.coachingDate)) return 1;
            return -1;        
        }
    }

}