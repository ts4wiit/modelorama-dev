/****************************************************************************************************
    General Information
    -------------------
    author:     Hector Diaz
    email:      hdiaz@gmail.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       29/Sep/2017       Hector Diaz	HD 				
    ================================================================================================
****************************************************************************************************/

global  with sharing class ISSM_ModeloAmigoEmailCopy_cls implements Messaging.InboundEmailHandler{
	
	   global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) { 
	   	System.debug('ENTRO A LA CLASE'+email.plainTextBody);
	   	List<String> listaValuesBody = new List<String>();
	   	List<Task> lstInsertTask= new List<Task>();
	   	String strLineIdCaso ='';
        String strIdCase ='';
        String ccAddresses ='';
        Boolean blncreateTaskCase = false;
		try{
		/*for(String objemail : email){
			
		}*/
	        //Obtenemos el Id del caso al cual se le insertara la tarea
	        if(String.isNotBlank(email.plainTextBody)){
	        	 Integer  fincadenaReenvio =  email.plainTextBody.indexOf('Id:');  
	        	  if(fincadenaReenvio >=1){
	                blncreateTaskCase = true;
	                strLineIdCaso = email.plainTextBody.substring(fincadenaReenvio);
	                strIdCase = strLineIdCaso.substring(3).replaceAll('[( - )\n\r]', '');
	            }
	        }
	      // for(){
	       		if(String.isNotBlank(email.plainTextBody) && blncreateTaskCase && strIdCase!=null & strIdCase!=''){
		        	String RecordTypeId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Standard_Task');
		        	System.debug('RecordTypeId : '+RecordTypeId);
		        	ccAddresses = (email.ccAddresses != null) ?  String.valueOf(email.ccAddresses) : '' ;
		        	String DescriptionTask = 'From :'+email.fromAddress+'\n CC : '+ccAddresses+'\n From Name : '+email.fromName+'\n Subject :'+email.subject+'\n To Adrres :'+email.toAddresses+'\n\n Cuerpo :\n'+email.plainTextBody;
		        	Task objTask = new Task();
		        	objTask.RecordTypeId = RecordTypeId; 
		        	objTask.Subject = email.subject;  
		        	objTask.ONTAP__Estado_de_visita__c = 'Abierto';
		        	objTask.Status = 'Completed';
		        	objTask.Priority = 'Normal';
		        	objTask.WhatId = strIdCase;
		        	objTask.Description=DescriptionTask;
		       		lstInsertTask.add(objTask);
	        	}	       
	     //  }
	       if(lstInsertTask!=null && !lstInsertTask.isEmpty()){
	       		insert lstInsertTask;
	       }
			 			
		}catch(Exception e){
			System.debug('Exception  : '+e);
		}
		
	   	return null;
	   } 
    
}