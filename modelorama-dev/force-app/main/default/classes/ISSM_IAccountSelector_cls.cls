public interface ISSM_IAccountSelector_cls {

	List<Account> selectById(Set<ID> idSet);
	Account selectById(ID accountID);
	List<Account> selectById(String accountID);
	List<Account> selectByMultipleFields(String strRecordTypeAccountId,String strTypificationLevel3,String strTypificationLevel4,String accountsSalesOffice);
	List<Account> selectByRecordType(Id recordTypeId);
	List<Account> selectByRecordTypeByParentAccount(Id recordTypeId, Id parentAccountId);
	List<Account> selectById(Set<ID> idSet, String relationshipFieldPath);
	Account selectById(ID accountID, String relationshipFieldPath);
	List<Account> selectById(String accountID, String relationshipFieldPath);
}