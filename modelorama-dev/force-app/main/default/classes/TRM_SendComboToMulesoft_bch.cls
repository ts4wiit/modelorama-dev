global class TRM_SendComboToMulesoft_bch implements Database.Batchable<sObject>, Database.stateful, Database.AllowsCallouts{
   	global final String externalKeyCombo;
   	global final List<String> lstIdCombos;
    global final String operation;
    public List<ISSM_ComboByAccount__c> lstComboByAccountAll = new List<ISSM_ComboByAccount__c>();    

   global TRM_SendComboToMulesoft_bch(String externalKeycmb, List<String> lstIdCmbs, String oper){
   		externalKeyCombo		= externalKeycmb; 
        lstIdCombos	= lstIdCmbs;
        operation	= oper;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){     
       system.debug('####START startBatch: '+ lstIdCombos.size());
       String srtQueryCustomers = 'SELECT  ISSM_ComboNumber__r.ISSM_ExternalKey__c'
                                       + ',ISSM_RegionalSalesDirection__r.ONTAP__SAPCustomerId__c'
                                       + ',ISSM_SalesOrganization__r.ONTAP__SAPCustomerId__c'
                                       + ',ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c'
                                       + ',ISSM_SegmentCode__c'
                                       + ',ISSM_SAPNumber__c'
                                       + ',ISSM_ExclusionAccount__c'
                               + ' FROM ISSM_ComboByAccount__c'
           					   + ' WHERE ISSM_ComboNumber__c IN :lstIdCombos';
        //srtQueryCustomers += operation == Label.TRM_OperationCreate ? ' WHERE ISSM_ComboNumber__c IN :lstIdCombos' : ' WHERE ISSM_ActiveValue__c = true AND ISSM_ComboNumber__c IN :lstIdCombos';
       system.debug(srtQueryCustomers);
       return Database.getQueryLocator(srtQueryCustomers);
   }

   global void execute(Database.BatchableContext BC, List<ISSM_ComboByAccount__c> scope){
       system.debug('####START executeBatch: '+ scope.size());
       for(ISSM_ComboByAccount__c cmbByAccount : scope){
               if (cmbByAccount != null ) lstComboByAccountAll.add(cmbByAccount);
           }
   }

   global void finish(Database.BatchableContext BC){
       system.debug('####START finishBatch');
       Map<String, List<ISSM_ComboByAccount__c>> mapCmbByAccount = new Map<String, List<ISSM_ComboByAccount__c>>();
       Map<String, List<ISSM_ComboByProduct__c>> mapCmbByProduct = new Map<String, List<ISSM_ComboByProduct__c>>();
       
       List<ISSM_ComboByProduct__c> lstCmbByProductAll = operation == Label.TRM_OperationCreate ? [SELECT  ISSM_ComboNumber__r.ISSM_ExternalKey__c
                                                                                                       ,ISSM_MaterialDescription__c
                                                                                                       ,ISSM_Product__r.ONTAP__ProductId__c
                                                                                                       ,ISSM_ItemCode__c
                                                                                                       ,ISSM_ItemQuota__c
                                                                                                       ,ISSM_QuantityProduct__c
                                                                                                       ,ISSM_UnitMeasure__c
                                                                                                       ,ISSM_MaterialGroup2__c 
                                                                                                       ,ISSM_UnitPriceTax__c
                                                                                                       ,ISSM_UnitPrice__c
                                                                                                       ,ISSM_CurrencyProduct__c
                                                                                                       ,ISSM_TotalPrice__c
                                                                                                       ,ISSM_Position__c
                                                                                                FROM ISSM_ComboByProduct__c 
                                                                                                WHERE ISSM_ComboNumber__c IN :lstIdCombos] : [SELECT   ISSM_ComboNumber__r.ISSM_ExternalKey__c
                                                                                                                                                      ,ISSM_MaterialDescription__c
                                                                                                                                                      ,ISSM_Product__r.ONTAP__ProductId__c
                                                                                                                                                      ,ISSM_ItemCode__c
                                                                                                                                                      ,ISSM_ItemQuota__c
                                                                                                                                                      ,ISSM_QuantityProduct__c
                                                                                                                                                      ,ISSM_UnitMeasure__c
                                                                                                                                                      ,ISSM_MaterialGroup2__c 
                                                                                                                                                      ,ISSM_UnitPriceTax__c
                                                                                                                                                      ,ISSM_UnitPrice__c
                                                                                                                                                      ,ISSM_CurrencyProduct__c
                                                                                                                                                      ,ISSM_TotalPrice__c
                                                                                                                                                      ,ISSM_Position__c
                                                                                                                                              FROM ISSM_ComboByProduct__c 
                                                                                                                                              WHERE ISSM_ActiveValue__c = true AND ISSM_ComboNumber__c IN :lstIdCombos];
       
	List<ISSM_Combos__c> lstComboAll = [SELECT   Id
                                                ,CreatedBy.Name
                                                ,LastModifiedBy.Name
                                                ,ISSM_ApprovalDate__c
                                                ,ISSM_StartDate__c
                                                ,ISSM_EndDate__c
                                                ,ISSM_StatusCombo__c
                                                ,ISSM_LongDescription__c
                                                ,ISSM_ShortDescription__c
                                                ,ISSM_NumberSalesOffice__c
                                                ,ISSM_NumberByCustomer__c
                                                ,ISSM_TypeApplication__c
                                                ,ISSM_TotalAmount__c
                                                ,ISSM_Currency__c
                                                ,ISSM_ExternalKey__c
                                                ,ISSM_SynchronizedWithSAP__c
                                                ,ISSM_ModifiedCombo__c
                                           FROM ISSM_Combos__c
                                           WHERE Id IN :lstIdCombos];
       
       List<ISSM_WebServicesLog__c> lstWebServicesLogAll = new List<ISSM_WebServicesLog__c>();
       for(ISSM_Combos__c sObjCombo : lstComboAll){
           List<ISSM_ComboByAccount__c> lstCmbByAccount = new List<ISSM_ComboByAccount__c>();  
           List<ISSM_ComboByProduct__c> lstCmbByProduct = new List<ISSM_ComboByProduct__c>();
           
           for(ISSM_ComboByProduct__c CmbByProduct : lstCmbByProductAll){
               if(sObjCombo.Id == CmbByProduct.ISSM_ComboNumber__c) lstCmbByProduct.add(CmbByProduct);
           }
           for(ISSM_ComboByAccount__c cmbByAccount : lstComboByAccountAll){
               if (sObjCombo.Id  == cmbByAccount.ISSM_ComboNumber__c) lstCmbByAccount.add(cmbByAccount);
           }
           
           List<ISSM_WebServicesLog__c>  lstWebServicesLog = sendCmbMulesoft(sObjCombo,lstCmbByAccount,lstCmbByProduct,operation);
           
           for (ISSM_WebServicesLog__c webServicesLog : lstWebServicesLog) if(!lstWebServicesLogAll.contains(webServicesLog)) lstWebServicesLogAll.add(webServicesLog);
           
       }
       
       upsert lstWebServicesLogAll;
   }
    public static List<ISSM_WebServicesLog__c> sendCmbMulesoft(ISSM_Combos__c combo, List<ISSM_ComboByAccount__c> lstCmbByAccount, List<ISSM_ComboByProduct__c> lstCmbByProduct,String operation){
        List<ISSM_WebServicesLog__c> lstWebServicesLog = new List<ISSM_WebServicesLog__c>();
        String strSerializeJson = TRM_SendCombo_cls.generateJSONForSend(combo,lstCmbByAccount,lstCmbByProduct,operation);  
        lstWebServicesLog = ISSM_SendComboToMulesoft_cls.SendCmbToMulesoftNotFuture(strSerializeJson,operation);
        return lstWebServicesLog;
    }
}