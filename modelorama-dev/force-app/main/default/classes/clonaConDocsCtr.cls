public class clonaConDocsCtr {

    private ApexPages.StandardController controller {get; set;}
    private agri_contract__c actr {get;set;}
    public ID newRecordId {get;set;}
	List<ContentDocumentLink> docs = new List<ContentDocumentLink>();

    public clonaConDocsCtr(ApexPages.StandardController controller) {
        this.controller = controller;
        actr = (agri_contract__c)controller.getRecord();
    }

    public PageReference copiaContrato() {
         Savepoint sp = Database.setSavepoint();
         agri_contract__c newActr;

         try {
             actr = [select Id, Agri_Place_of_Contract_Signature__c, Agri_Date_Of_Contract_Signature__c, Agri_Farmer__c, Agri_Legal_Personality_Rep__c, Agri_Legal_Personality__c, Agri_Start_Date_of_the_Contract__c, 
                     Agri_End_Date_of_Contract__c, Agri_Billing_date__c, Agri_Maltery__c, Agri_Branch_Rep_del__c, Agri_Use_of_Representative__c, Agri_Variety_Seed__c, Agri_Cycle_Contract__c, Agri_Unit_of_measurement_Seed__c, 
                     Agri_Minimum_Seed_Volume__c, Agri_Seed_volume__c, Agri_Lot_Id__c, Agri_Brand__c, Agri_Payment_method__c, Agri_Seed_Payment_Term__c, Agri_Total_Price_Per_Seed__c, Agri_Year_Cycle__c, Agri_Seed_yield__c, 
                     Agri_Maximum_Seed_Volume__c, Agri_Hectares_to_Be_Planted__c, Agri_Location_of_The_Property__c, Agri_Delivery_Warehouse__c, Agri_Currency__c, Agri_Unit_Price_of_Seed_Kg__c, Agri_Centre_Barley__c, 
                     Agri_Variety_Barley__c, Agri_Unit_of_measurement_Barley__c, Agri_Tons_to_Produce__c, Agri_Total_Sale_of_Barley__c, Agri_Final_Purchase_Price__c, Agri_Term_of_Payment_of_Final_Produc_del__c, 
                     Agri_Estimated_Harvest_Date__c, Agri_Place_of_Delivery_of_Final_Product__c, Agri_Contractual_Penalty__c, Agri_Succesor__c, Agri_Bank_Account_Modelo__c, Agri_Bank_Account_Holder__c, Agri_RFC_Successor__c, 
                     Agri_Name_of_Bank_Account_Modelo__c, Agri_CLABE_Account__c, Agri_Bank_Reference_Number__c,
                     Agri_Name_of_Representative__c, Agri_Last_Name_Rep__c, Agri_Second_Last_Name_Rep__c, Agri_Representative_Phone__c, Agri_Power_of_legal_representative__c, Agri_RFC_Representative__c, Agri_CURP_Rep__c,
                     Agri_State_Rep__c, Agri_City_Rep__c, Agri_Colony_Rep__c, Agri_Street_Rep__c, Agri_Exterior_Number_Rep__c, Agri_Interior_Number_Rep__c, Agri_Zip_Postal_Code_Rep__c, Agri_Email_Rep__c
                     from agri_contract__c 
                     where id = :actr.id];
             newActr = actr.clone(false);
             insert newActr;

             newRecordId = newActr.id;
             
             set<Id> setDocsId = new set<Id>();
             for(ContentDocumentLink conDoc : [SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =: actr.Id]) {
                 setDocsId.add(conDoc.ContentDocumentId);
             }
             System.debug('--> setDocsID : ' + setDocsId);
             
             List<ContentVersion> lCVInsert = new List<ContentVersion>();
             for(ContentVersion oldCV : [SELECT Checksum, ContentDocumentId, ContentLocation, ContentSize, ContentUrl, Description, FileExtension, FileType, FirstPublishLocationId, Id, 
                                         IsAssetEnabled, IsDeleted, Origin, OwnerId, PathOnClient, PublishStatus, RatingCount, ReasonForChange, SharingOption, Title, VersionData, VersionNumber 
                                         FROM ContentVersion 
                                         WHERE ContentDocumentId IN : setDocsId]) {                            
              	  ContentVersion newCV = new ContentVersion();
                  newCV.Title = oldCV.Title;
                  newCV.PathOnClient = oldCV.PathOnClient;
                  newCV.VersionData = oldCV.VersionData;
                  newCV.FirstPublishLocationId = newRecordId; 
                  lCVInsert.add(newCV);
             }
             System.debug('--> lCVInsert : ' + lCVInsert);
             
             if(!lCVInsert.isEmpty()) {
                 insert lCVInsert;
             }
             
         } catch (Exception e){
             Database.rollback(sp);
             ApexPages.addMessages(e);
             return null;
         }
        
        return new PageReference('/'+newActr.id+'/e?retURL=%2F'+newActr.id);                           
    }

}