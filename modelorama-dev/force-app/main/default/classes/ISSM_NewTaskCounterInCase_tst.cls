/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_NewTaskCounterInCase_tst {
    /*
        MEtodo que hace la prueba cuando una tarea es tipo de registro recalles y cuando es diferente de recalled
    */
    static testMethod void TestTaskCreate() {
        String RecordTypeTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Recalled').getRecordTypeId();
        String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
       String RecordTypeTelecollectionTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId(); 
       String RecordTypeTelecollectionCall = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId(); 
        
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        
        //Creamos el usuario y la tipificacion para posterior insertarla en la configuracion personalizada
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix1 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00002','Gestión Cobranza','Aclaracion','Pago no reconocido',null,null,null,'Trade Marketing',user.Id,null);    
        ISSM_TypificationMatrix__c  TypificationMatrix2 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00003','Gestión Cobranza','Aclaracion','Cargo no reconocido',null,null,null,'Trade Marketing',user.Id,null);   
        ISSM_TypificationMatrix__c  TypificationMatrix3 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00004','Gestión Cobranza','Aclaracion','Plan de pago',null,null,null,'Trade Marketing',user.Id,null);  
        List<ISSM_AppSetting_cs__c> AppSettingProviderTypification =  ISSM_CreateDataTest_cls.fn_CreateAppSettingTypification(true,String.valueOf(TypificationMatrix1.Id),String.valueOf(TypificationMatrix2.Id),String.valueOf(TypificationMatrix3.Id),mapIdQueue.get('ISSM_WithoutOwner')); 
        
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;   

        //Creamos Cuenta
        Account  objAccount = new Account(); 
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
            insert objAccount;
        //Creamos el caso para que podamos asociar a la tarea
        Case objCase = new Case();
                objCase.Subject = 'Subject';
                objCase.Description = 'Description'; 
                objCase.Status = 'new';
                objCase.ISSM_TypificationLevel1__c='Refrigeración';
                objCase.ISSM_TypificationLevel2__c='Mantenimiento';
                objCase.ISSM_TypificationLevel3__c ='Barril';
                objCase.ISSM_TypificationLevel4__c ='Tanque CO2';
                objCase.ISSM_TypificationLevel5__c = null;
                objCase.ISSM_TypificationLevel6__c = null;
                objCase.AccountID = objAccount.Id;
                insert objCase;
       
       /* LLAMADAS CON TIPO DE REGISTRO RECALLED*/
        //Creamos tareas con tipo de regitro reellamada     
        Task objTask = new Task();
        objTask.RecordTypeId = RecordTypeTask;
        objTask.Status = 'Not Started';
        objTask.Priority = 'Normal';
        objTask.WhatId = objCase.Id;
        insert objTask;
        
        /* LLAMADAS CON TIPO DE REGISTRO DIFERENTE A RECALLED*/
        
        //Creamos la llamada
        ONCALL__Call__c ObjCall = new ONCALL__Call__c();
        ObjCall.RecordTypeId = RecordTypeTelecollectionCall;
        ObjCall.ISSM_Active__c = true;
        ObjCall.ISSM_NumberCallsMade__c=0;
        ObjCall.Name ='Call Telecollection';
        insert ObjCall;
        
        //Creamos la llamada con numero de order
        ONCALL__Call__c ObjCallOrder = new ONCALL__Call__c();
        ObjCallOrder.RecordTypeId = RecordTypeTelecollectionCall;
        ObjCallOrder.ISSM_Active__c = true;
        ObjCallOrder.ISSM_NumberCallsMade__c=0;
        ObjCallOrder.Name ='Call Telecollection 2';
        ObjCallOrder.ISSM_Order__c = 10;
        insert ObjCallOrder;
        
        
        
        Test.startTest(); 
            //Creamos tareas con tipo de rsgitro diferente a reellamada
           Task objTaskNoRecalled = new Task();
            objTaskNoRecalled.RecordTypeId = RecordTypeTelecollectionTask; 
            objTaskNoRecalled.WhatId = ObjCall.Id;
            objTaskNoRecalled.ISSM_CallEffectiveness__c='';
            objTaskNoRecalled.Status = 'Complete';
            objTaskNoRecalled.ISSM_PaymentPromise__c ='';
            objTaskNoRecalled.ISSM_PaymentPromiseDate__c= Date.today();
            objTaskNoRecalled.ISSM_PaymentPlanRequest__c='';
            objTaskNoRecalled.ISSM_PaymentPlanMaxDate__c= Date.today();
            objTaskNoRecalled.ISSM_ClarificationRequest__c='';
            objTaskNoRecalled.ISSM_ClarificationType__c='';
            objTaskNoRecalled.ISSM_CallEffectiveness__c = 'Effective';
            
            insert objTaskNoRecalled;   
          
            //Creamos tareas con tipo de rsgitro diferente a reellamada
           Task objTaskNoRecalled2 = new Task();
            objTaskNoRecalled2.RecordTypeId = RecordTypeTelecollectionTask; 
            objTaskNoRecalled2.WhatId = ObjCall.Id;
            objTaskNoRecalled2.Status = 'Complete';
            objTaskNoRecalled2.ISSM_CallEffectiveness__c = 'Not effective';
            insert objTaskNoRecalled2;
          
            //Creamos tareas con tipo de rsgitro diferente a reellamada
          Task objTaskNoRecalled3 = new Task();
            objTaskNoRecalled3.RecordTypeId = RecordTypeTelecollectionTask; 
            objTaskNoRecalled3.WhatId = ObjCallOrder.Id;
            objTaskNoRecalled3.Status = 'Incomplete';
            objTaskNoRecalled3.ISSM_CallEffectiveness__c = 'Not effective';
            insert objTaskNoRecalled3;
            
        Test.stopTest();
        
    }
}