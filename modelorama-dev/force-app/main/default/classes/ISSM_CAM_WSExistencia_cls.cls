/****************************************************************************************************
General Information
-------------------
author:     Daniel Rosales
email:      drosales@avanxo.com
company:    Avanxo México
Project:    CAM
Customer:   Grupo Modelo

Description: 
1. Calls out a WS (Salesforce - Mulesfot -SAP) to check for Stock of certain SKU in certain Warehouse,
used in CAM Project to verify if there are coolers of the requested SKU before sending an Order 

Information about changes (versions)
================================================================================================
Number    Dates                 Author                       Description          
------    -------------         --------------------------   -----------------------------------
1.0       16th-July-2018         Daniel Rosales               Class Creation
================================================================================================
****************************************************************************************************/

public class ISSM_CAM_WSExistencia_cls {
    public static Long wsExistencia(string pSalesOff, string strSKU){
        String Json_str = '';
        Json_str += '{';
        Json_str += '"' + 'materials' + '"' + ':[{';
        Json_str += '"' + 'unit' + '":' + '"' + 'EA' + '",';
        Json_str += '"' + 'salesOffice' + '":' + '"' + pSalesOff + '",';
        Json_str += '"' + 'productId' + '":' + '"' + strSKU + '"';
        Json_str += '}]';
        Json_str += '}';
        system.debug(json_str);
        Map<String, ISSM_PriceEngineConfigWS__c> configuracionWS_map = ISSM_PriceEngineConfigWS__c.getAll();
        String configName_str = 'ConfigCAMWSExistencia';
        if(Test.isRunningTest()){
            system.debug('TEST DENTRO Existencias - Setting up MOCK');
            Test.setMock(HttpCalloutMock.class, new ISSM_CAM_WSGenMockHttpResponse_cls());
        }
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(configuracionWS_map.get(configName_str).ISSM_EndPoint__c);
        request.setMethod(configuracionWS_map.get(configName_str).ISSM_Method__c);
        request.setTimeout(60000);
        request.setHeader('Content-Type', configuracionWS_map.get(configName_str).ISSM_HeaderContentType__c);
        request.setBody(Json_str);
        system.debug('Existencia WS request' + Json_str);
        try {
            HttpResponse response = http.send(request);
            // ======================== WS REQUEST END =========================================
            // If the request is successful, parse the JSON response.
            //system.debug('existencia getstatuscode: ' + response.getStatusCode());
            if (response.getStatusCode() == 200) {
                // Get ResponseMap
                String responseBody = response.getBody();
                system.debug('Existencia WS response: ' + responseBody);
                ISSM_CAM_WSExistencia_cls.existencia mapExistencia = (ISSM_CAM_WSExistencia_cls.existencia)JSON.deserialize(responseBody, ISSM_CAM_WSExistencia_cls.existencia.class);
                system.debug(responsebody);
                system.debug(mapExistencia.materials.size());
                if (mapExistencia.materials.size()>0 ) {
                    system.debug(mapExistencia.materials[0].availableQuantity);
                    return mapExistencia.materials[0].availableQuantity;                       } else return null;
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ISSM_EmptyOrderId));
            }
        } catch(System.CalloutException e) { system.debug('Error al llamar existencias ws: ' + e.getMessage()); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'TimeOut'));
        } return null;
        
        // Error al llamar existencias ws: You have uncommitted work pending. Please commit or rollback before calling out
        
    }
    
    public class existencia{
        public cls_materials[] materials;
    }
    public class cls_materials {
        public String unit;	//EA
        public String salesOffice;	//FG00
        public String productId;	//8000550
        public Long availableQuantity;	//0
    }
    
    
}