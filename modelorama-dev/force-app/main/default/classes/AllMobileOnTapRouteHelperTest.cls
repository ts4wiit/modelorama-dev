/**
 * Test class for AllMobileOnTapRouteHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileOnTapRouteHelperTest {

	/**
	 * Test method to test Presales routes.
	 */
	static testMethod void testRoutePresales() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplicationPreventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplicationPreventa;

		//Create Office 1.
		Account objSalesOffice1 = new Account(RecordTypeId = objRecordTypeAccountSalesOffice.Id, Name = 'Office1', ONTAP__SalesOffId__c = 'FG00');
		insert objSalesOffice1;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle1 = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice1, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle1;

		//Create Presales Route.
		ONTAP__Route__c objOnTapRoutePresales = new ONTAP__Route__c(RecordTypeId = objRecordTypeOnTapRouteSales.Id, ServiceModel__c = 'Presales', ONTAP__RouteId__c = 'FG0003', ONTAP__RouteName__c = 'Ruta norte', ONTAP__SalesOffice__c = objSalesOffice1.Id, Vehicle__c = objOnTapVehicle1.Id);
		insert objOnTapRoutePresales;

		//Update Presales route.
		objOnTapRoutePresales.ONTAP__RouteName__c = 'Ruta Norte Presales';
		update objOnTapRoutePresales;

		//Delete Presales route.
		delete objOnTapRoutePresales;
	}

	/**
	 * Test method to test Autosales routes.
	 */
	static testMethod void testRouteAutosales() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');

		//Creating an Application Autoventa.
		AllMobileApplication__c objAllMobileApplicationAutoventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('AUTOVENTA', '2');
		insert objAllMobileApplicationAutoventa;

		//Create Office 1.
		Account objSalesOffice1 = new Account(RecordTypeId = objRecordTypeAccountSalesOffice.Id, Name = 'Office1', ONTAP__SalesOffId__c = 'FG00');
		insert objSalesOffice1;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle1 = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice1, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle1;

		//Create Autosales Route.
		ONTAP__Route__c objOnTapRouteAutosales = new ONTAP__Route__c(RecordTypeId = objRecordTypeOnTapRouteSales.Id, ServiceModel__c = 'Autosales', ONTAP__RouteId__c = 'FG0003', ONTAP__RouteName__c = 'Ruta norte', ONTAP__SalesOffice__c = objSalesOffice1.Id, Vehicle__c = objOnTapVehicle1.Id);
		insert objOnTapRouteAutosales;
	}

	/**
	 * Test method to test Rep routes.
	 */
	static testMethod void testRouteRep() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');

		//Creating an Application Reparto.
		AllMobileApplication__c objAllMobileApplicationReparto = AllMobileUtilityHelperTest.createAllMobileApplicationObject('REPARTO', '3');
		insert objAllMobileApplicationReparto;

		//Create Office 1.
		Account objSalesOffice1 = new Account(RecordTypeId = objRecordTypeAccountSalesOffice.Id, Name = 'Office1', ONTAP__SalesOffId__c = 'FG00');
		insert objSalesOffice1;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle1 = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice1, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle1;

		//Create Rep Route.
		ONTAP__Route__c objOnTapRouteRep = new ONTAP__Route__c(ServiceModel__c = null, ONTAP__RouteId__c = 'FG0010', ONTAP__RouteName__c = 'Ruta norte reparto', ONTAP__SalesOffice__c = objSalesOffice1.Id, Vehicle__c = objOnTapVehicle1.Id);
		insert objOnTapRouteRep;
	}

	/**
	 * Test method to test BDR routes.
	 */
	static testMethod void testRouteBDR() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');

		//Get Profile: 'System Administrator'.
 		Profile objProfileSystemAdministrator = AllMobileUtilityHelperTest.getProfileObject('System Administrator');

		//Create a User.
		User objUserAgent = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'albertogomez@abinbev.org.dev', 'agome', 'alberto@gmail.com', 'ISO-8859-1', 'Gomez', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUserAgent;

		//Create Office 1.
		Account objSalesOffice1 = new Account(RecordTypeId = objRecordTypeAccountSalesOffice.Id, Name = 'Office1', ONTAP__SalesOffId__c = 'FG00');
		insert objSalesOffice1;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle1 = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice1, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle1;

		//Create BDR Route.
		ONTAP__Route__c objOnTapRouteBDR = new ONTAP__Route__c(RecordTypeId = objRecordTypeOnTapRouteSales.Id, ServiceModel__c = 'BDR', ONTAP__RouteId__c = 'FG0003', ONTAP__RouteName__c = 'Ruta norte', ONTAP__SalesOffice__c = objSalesOffice1.Id, Vehicle__c = objOnTapVehicle1.Id, RouteManager__c = objUserAgent.Id);
		insert objOnTapRouteBDR;

		//Create a User 2.
		User objUserAgent2 = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'kevinbadillo@abinbev.org.dev', 'kbad', 'kbadillo@gmail.com', 'ISO-8859-1', 'Badillo', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUserAgent2;

		//Update Agent in BDR Route.
		objOnTapRouteBDR.RouteManager__c = objUserAgent2.Id;
		update objOnTapRouteBDR;
	}

	/**
	 * Test method to test Autosales routes without record type.
	 */
	static testMethod void testRouteAutosalesWithoutRecordType() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');

		//Creating an Application Reparto.
		AllMobileApplication__c objAllMobileApplicationReparto = AllMobileUtilityHelperTest.createAllMobileApplicationObject('REPARTO', '3');
		insert objAllMobileApplicationReparto;

		//Create Office 1.
		Account objSalesOffice1 = new Account(RecordTypeId = objRecordTypeAccountSalesOffice.Id, Name = 'Office1', ONTAP__SalesOffId__c = 'FG00');
		insert objSalesOffice1;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle1 = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice1, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle1;

		//Create Autosales Route without record type.
		ONTAP__Route__c objOnTapRouteAutosalesHK = new ONTAP__Route__c(ServiceModel__c = 'Autosales', ONTAP__RouteId__c = 'FG0010', ONTAP__RouteName__c = 'Ruta norte reparto', ONTAP__SalesOffice__c = objSalesOffice1.Id, Vehicle__c = objOnTapVehicle1.Id);
		insert objOnTapRouteAutosalesHK;
	}
}