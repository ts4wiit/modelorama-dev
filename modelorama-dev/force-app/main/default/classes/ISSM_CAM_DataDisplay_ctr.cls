/******************************************************************************** 
    Company:            Avanxo México
    Author:             Oscar Alvarez Garcia
    Customer:           AbInbev - CAM
    Descripción:        Class that the COMODATO structure us according to the UEN, User and Coolers

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    20-Octubre-2018    Oscar Alvarez Garcia           Creation
    ******************************************************************************* */
public class ISSM_CAM_DataDisplay_ctr {
// START CABECERA
    public String typeDocument          {get; set;} 
    public String nameCompany           {get; set;}
    public String companyAddress        {get; set;}
    public String postalCode            {get; set;}
    public String rfc                   {get; set;}
    public String rfcCustomer           {get; set;}
    public String dateOfAssignment      {get; set;} 
    public Integer numberOfImpressions  {get; set;}
    public String labelNumOfImpressions {get; set;} 
// END CABECERA
// Seccion DECLATRACIONES   
    public String declaracionTitle    {get; set;}   
    public String declacacionI        {get; set;}
    public String declacacionIA       {get; set;}
    public String declacacionIB       {get; set;}
    public String declacacionIC       {get; set;}
    public String declacacionID       {get; set;}
    public String declacacionII       {get; set;}
    public String declacacionIIA1     {get; set;}
    public String declacacionIIA2     {get; set;}
    public String declacacionIIA21    {get; set;}
    public String declacacionIIBpart1 {get; set;}
    public String declacacionIIBpart2 {get; set;}
    public String declacacionIIC      {get; set;}
    public String declacacionIIDpart1 {get; set;}
    public String declacacionIIDpart2 {get; set;}
    public String declacacionIIEpart1 {get; set;}
    public String declacacionIIEpart2 {get; set;}
    public String declacacionIIend    {get; set;}
// Seccion DECLATRACIONES  
// datos corporativo
    public String avenueCorpo     {get; set;}
    public String coloniaCorpo    {get; set;}
    public String delegationCorpo {get; set;}
    public String cityCorpo       {get; set;}
    public String postalCodeCorpo {get; set;}
    public String mentionCorpo    {get; set;}
// datos corporativo
// header contract
    public String contractHeaderPart1 {get; set;}
    public String contractHeaderPart2 {get; set;}
    public String contractHeaderPart3 {get; set;}
    public String contractHeaderPart4 {get; set;}
    public String labelComodante      {get; set;}
    public String labelComodatario    {get; set;}
    public String comodante           {get; set;}
    public String comodante2          {get; set;}
    //public String comodatario         {get; set;}
// header contract
// name colums from coolers
    public String colum1 {get; set;}
    public String colum2 {get; set;}
    public String colum3 {get; set;}
    public String colum4 {get; set;}
    public String colum5 {get; set;}
// name colums from coolers
//clause contract
    public String clauseTitle               {get; set;}
    public String clause1Paragraph1         {get; set;}
    public String clause1Paragraph1_1       {get; set;}
    public String clause1Paragraph2         {get; set;}
    public String clause1Paragraph3         {get; set;}
    public String clause1Paragraph4         {get; set;}
    public String clause1Paragraph5         {get; set;}
    public String clause2                   {get; set;}
    public String clause3                   {get; set;}
    public String clause4                   {get; set;}
    public String clause5                   {get; set;}
    public String clause5Paragraph1         {get; set;}
    public String clause5Paragraph2         {get; set;}
    public String clause5Paragraph3         {get; set;}
    public String clause5Paragraph4         {get; set;}
    public String clause5Paragraph5         {get; set;}
    public String clause5Paragraph6         {get; set;}
    public String clause5Paragraph7         {get; set;}
    public String clause5Paragraph8         {get; set;}
    public String clause5Paragraph8i        {get; set;}
    public String clause5Paragraph9         {get; set;}
    public String clause5Paragraph9_1       {get; set;}
    public String clause6                   {get; set;}
    public String clause7                   {get; set;}
    public String clause8                   {get; set;}
    public String clause9                   {get; set;}
    public String clause10                  {get; set;}
    public String clause11                  {get; set;}
    public String clause11Paragraph1        {get; set;}
    public String clause12                  {get; set;}
    public String clause12Paragraph1        {get; set;}
//clause contract
// signature section
    public String labelNameCompany      {get; set;}
    public String legalRepresentative   {get; set;}
// signature section
// PAGARÉ
    public Integer numberOfCoolers              {get; set;}    
    public String promissoryNoteTitle           {get; set;} 
    public String promissoryNoteBueno           {get; set;} 
    public String promissoryNoteLabelBueno      {get; set;}  
    public String promissoryNoteBuenoletter     {get; set;}   
    public String promissoryNoteLabelBuenoletter{get; set;}  
    public String promissoryNotePart1           {get; set;}    
    public String promissoryNotePart2           {get; set;}  
    public String promissoryNotePart3           {get; set;}  
    public String promissoryNotePart4           {get; set;}
    public String promissoryNotePart51          {get; set;}
    public String promissoryNotePart6           {get; set;}
    public String promissoryLabelDebtor         {get; set;}  
    public String promissoryLabelName           {get; set;} 
    public String promissoryLabelDomicile       {get; set;}  
    public String promissoryLabelColony         {get; set;}  
    public String promissoryLabelPopulation     {get; set;}  
    public String promissoryLabelFirm           {get; set;} 
    public String promissoryNoteTitleNameUEN    {get; set;}
// PAGARÉ
// CONTROL DATA
    public String idCaseForce                       {get; set;} 
    public ONTAP__Case_Force__c objectCaseForce     {get; set;} 
    public List<ISSM_Case_Force_Asset__c> assetCAM  {get; set;} 
// CONTROL DATA

    public ISSM_CAM_DataDisplay_ctr(){
        idCaseForce = '';
    }

    public PageReference downloadPDF(){
        getData();
        System.PageReference pageRef = new System.PageReference('/apex/ISSM_CAM_PDFComodato_pag');    
         //ensure pdf downloads and is assigned with defined name
        String namePDF = objectCaseForce.ONTAP__Account__r.ONTAP__SAP_Number__c+'_'+dateOfAssignment+'_P'+objectCaseForce.ISSM_Printed_times__c+'.pdf';
        pageRef.getHeaders().put('content-disposition', 'attachment; filename='+namePDF);
        return pageRef;
    }
    public void getAssetCAM() {
        System.debug('####### getAssetCAM: '+objectCaseForce.Id);
        assetCAM =[SELECT ISSM_Asset__r.ISSM_Material_number__c,
                          ISSM_Asset__r.ISSM_Asset_Description__c,
                          ISSM_Asset__r.ISSM_Provider_Serial_Number__c,
                          ISSM_Asset__r.Asset_number__c
                    FROM ISSM_Case_Force_Asset__c //LIMIT 10]; 
                    WHERE ISSM_Case_Force__c =: objectCaseForce.Id];
                      
    }
    public  string getDate(Date d){
        System.debug('####### getDate ');// los meses no se trasladan a etiqueta por el echo de que solo es para mostrar en el contrato Fecha de entrega
        string mes = (d.month() == 1  ? 'Enero': (d.month() == 2  ? 'Febrero': (d.month() == 3  ? 'Marzo'     :(d.month() == 4  ? 'Abril'  : (d.month() == 5  ? 'Mayo'     : (d.month() == 6  ? 'Junio':
                     (d.month() == 7  ? 'Julio': (d.month() == 8  ? 'Agosto' : (d.month() == 9  ? 'Septiembre':(d.month() == 10 ? 'Octubre': (d.month() == 11 ? 'Noviembre': (d.month() == 12 ? 'Diciembre':''))))))))))));
      return d.day() + ' de ' + mes + ' de ' + d.year();
    }
    public ISSM_CAM_PDFContentComodato__mdt getPDFContentComodato(String developerName) {
         System.debug('####### getPDFContentComodato '+ developerName);
        ISSM_CAM_PDFContentComodato__mdt PDFContentComodato;
        String queryString = 'SELECT ';
            queryString += String.join(new List<String>(SObjectType.ISSM_CAM_PDFContentComodato__mdt.Fields.getMap().keySet()),', ');
            queryString += ' FROM ISSM_CAM_PDFContentComodato__mdt';
            queryString += ' WHERE DeveloperName = \'' + developerName + '\'';
        List<ISSM_CAM_PDFContentComodato__mdt> resultList = ISSM_CAM_GeneratePDFComodato_ctr.executeQuery(queryString);

        if(resultList.size() == 1) PDFContentComodato = resultList[0];

        return PDFContentComodato;
    }
    public void getData() {
        System.debug('####### getData ');
        Map<String, ISSM_CAM_UEN_Address__c> MapConfiguracionWs = ISSM_CAM_UEN_Address__c.getAll();
        objectCaseForce = [SELECT Id,
                                  Name,
                                  ONTAP__Account__r.ONTAP__SAP_Number__c,     // NUMERO SAP
                                  ONTAP__Account__r.ONTAP__LegalName__c,      // Razon social
                                  ONTAP__Account__r.ONTAP__Street__c,         // Calle
                                  ONTAP__Account__r.ONTAP__Street_Number__c,  // Número de la calle
                                  ONTAP__Account__r.ONCALL__Postal_Code__c,   // Código postal
                           		  ONTAP__Account__r.ONTAP__Province__c,
                                  ONTAP__Account__r.ONTAP__SalesOgId__c,
                                  ONTAP__Account__r.ONTAP__Colony__c ,        // Colonia
                                  ONTAP__Account__r.ONTAP__Municipality__c,   // Municipio
                                  ONTAP__Account__r.ISSM_RFC__c,
                                  ISSM_SAPOrderDate__c,
                                  ISSM_Printed_times__c 
                           FROM ONTAP__Case_Force__c
                           WHERE Id =:idCaseForce];
        Integer numPrinted = (objectCaseForce.ISSM_Printed_times__c != null || objectCaseForce.ISSM_Printed_times__c > 0) ? Integer.valueOf(objectCaseForce.ISSM_Printed_times__c) : 0;
        numPrinted++;
        objectCaseForce.ISSM_Printed_times__c = numPrinted;
        update objectCaseForce;       

        String strUEN = objectCaseForce.ONTAP__Account__r.ONTAP__SalesOgId__c;
        ISSM_CAM_PDFContentComodato__mdt mdtContrato = getPDFContentComodato('ISSM_CAM_Contrato');
        ISSM_CAM_PDFContentComodato__mdt mdtPagare   = getPDFContentComodato('ISSM_CAM_Pagare');

        dateOfAssignment            = objectCaseForce.ISSM_SAPOrderDate__c != null ? getDate(objectCaseForce.ISSM_SAPOrderDate__c) : 'NO DATE';
        numberOfImpressions         = Integer.valueOf(objectCaseForce.ISSM_Printed_times__c);
        labelNumOfImpressions       = mdtContrato.ISSM_CAM_labelNumOfImpressions__c;
        typeDocument                = mdtContrato.ISSM_CAM_typeDocument__c; 
        nameCompany                 = mdtContrato.ISSM_CAM_nameCompany__c;
        companyAddress              = MapConfiguracionWs.size() > 0 ? (MapConfiguracionWs.get(strUEN) != null ? MapConfiguracionWs.get(strUEN).UEN_Address__c: ''):'';  // CONFIGURACION   
        postalCode                  = MapConfiguracionWs.size() > 0 ? (MapConfiguracionWs.get(strUEN) != null ? MapConfiguracionWs.get(strUEN).ISSM_Short_address__c: ''):'';  // CONFIGURACION 
        rfc                         = mdtContrato.ISSM_CAM_rfc__c;
        rfcCustomer                 = objectCaseForce.ONTAP__Account__r.ISSM_RFC__c;
        labelComodante              = mdtContrato.ISSM_CAM_labelComodante__c;
        labelComodatario            = mdtContrato.ISSM_CAM_labelComodatario__c;
        comodante                   = MapConfiguracionWs.size() > 0 ? (MapConfiguracionWs.get(strUEN) != null ? MapConfiguracionWs.get(strUEN).ISSM_LegalRepresentative1__c: ''):'';     // CONFIGURACION 
        comodante2                  = MapConfiguracionWs.size() > 0 ? (MapConfiguracionWs.get(strUEN) != null ? MapConfiguracionWs.get(strUEN).ISSM_Legal_representative2__c: ''):'';       // CONFIGURACION 
        //DATOS COORPORATIVO
        avenueCorpo               = mdtContrato.ISSM_CAM_avenueCorpo__c; 
        coloniaCorpo              = mdtContrato.ISSM_CAM_coloniaCorpo__c;         
        delegationCorpo           = mdtContrato.ISSM_CAM_delegationCorpo__c; 
        cityCorpo                 = mdtContrato.ISSM_CAM_cityCorpo__c; 
        postalCodeCorpo           = mdtContrato.ISSM_CAM_postalCodeCorpo__c; 
        mentionCorpo              = mdtContrato.ISSM_CAM_mentionCorpo__c;
        // Seccion DECLATRACIONES
        declaracionTitle          = mdtContrato.ISSM_CAM_declaracionTitle__c;
        declacacionI              = mdtContrato.ISSM_CAM_declacacionI__c;
        declacacionIA             = mdtContrato.ISSM_CAM_declacacionIA__c;
        declacacionIB             = mdtContrato.ISSM_CAM_declacacionIB__c;
        declacacionIC             = mdtContrato.ISSM_CAM_declacacionIC__c;
        declacacionID             = mdtContrato.ISSM_CAM_declacacionID__c;
        declacacionII             = mdtContrato.ISSM_CAM_declacacionII__c;
        declacacionIIA1           = mdtContrato.ISSM_CAM_declacacionIIA1__c;
        declacacionIIA2           = mdtContrato.ISSM_CAM_declacacionIIA2__c;
        declacacionIIA21          = mdtContrato.ISSM_CAM_declacacionIIA21__c;
        declacacionIIBpart1       = mdtContrato.ISSM_CAM_declacacionIIBpart1__c;
        declacacionIIBpart2       = mdtContrato.ISSM_CAM_declacacionIIBpart2__c;
        declacacionIIC            = mdtContrato.ISSM_CAM_declacacionIIC__c;
        declacacionIIDpart1       = mdtContrato.ISSM_CAM_declacacionIIDpart1__c;
        declacacionIIDpart2       = mdtContrato.ISSM_CAM_declacacionIIDpart2__c;
        declacacionIIEpart1       = mdtContrato.ISSM_CAM_declacacionIIEpart1__c;
        declacacionIIEpart2       = mdtContrato.ISSM_CAM_declacacionIIEpart2__c;
        declacacionIIend          = mdtContrato.ISSM_CAM_declacacionIIend__c;
        //Header contract
        contractHeaderPart1       = mdtContrato.ISSM_CAM_contractHeaderPart1__c; 
        contractHeaderPart2       = mdtContrato.ISSM_CAM_contractHeaderPart2__c;
        contractHeaderPart3       = mdtContrato.ISSM_CAM_contractHeaderPart3__c;
        contractHeaderPart4       = mdtContrato.ISSM_CAM_contractHeaderPart4__c;
        //name colums
        colum1                    = mdtContrato.ISSM_CAM_colum1__c;
        colum2                    = mdtContrato.ISSM_CAM_colum2__c;
        colum3                    = mdtContrato.ISSM_CAM_colum3__c;
        colum4                    = mdtContrato.ISSM_CAM_colum4__c;
        colum5                    = mdtContrato.ISSM_CAM_colum5__c;
        //clause contract
        clauseTitle               = mdtContrato.ISSM_CAM_clauseTitle__c;
        clause1Paragraph1         = mdtContrato.ISSM_CAM_clause1Paragraph1__c  ;
        clause1Paragraph1_1       = mdtContrato.ISSM_CAM_clause1Paragraph1_1__c  ;
        clause1Paragraph2         = mdtContrato.ISSM_CAM_clause1Paragraph2__c;
        clause1Paragraph3         = mdtContrato.ISSM_CAM_clause1Paragraph3__c;
        clause1Paragraph4         = mdtContrato.ISSM_CAM_clause1Paragraph4__c;
        clause1Paragraph5         = mdtContrato.ISSM_CAM_clause1Paragraph5__c;
        clause2                   = mdtContrato.ISSM_CAM_clause2__c;
        clause3                   = mdtContrato.ISSM_CAM_clause3__c;
        clause4                   = mdtContrato.ISSM_CAM_clause4__c;
        clause5                   = mdtContrato.ISSM_CAM_clause5__c;
        clause5Paragraph1         = mdtContrato.ISSM_CAM_clause5Paragraph1__c;
        clause5Paragraph2         = mdtContrato.ISSM_CAM_clause5Paragraph2__c;
        clause5Paragraph3         = mdtContrato.ISSM_CAM_clause5Paragraph3__c;
        clause5Paragraph4         = mdtContrato.ISSM_CAM_clause5Paragraph4__c;
        clause5Paragraph5         = mdtContrato.ISSM_CAM_clause5Paragraph5__c;
        clause5Paragraph6         = mdtContrato.ISSM_CAM_clause5Paragraph6__c;
        clause5Paragraph7         = mdtContrato.ISSM_CAM_clause5Paragraph7__c;
        clause5Paragraph8         = mdtContrato.ISSM_CAM_clause5Paragraph8__c;
        clause5Paragraph8i        = mdtContrato.ISSM_CAM_Clause5Paragraph8i__c;
        clause5Paragraph9         = mdtContrato.ISSM_CAM_clause5Paragraph9__c;
        clause5Paragraph9_1       = mdtContrato.ISSM_CAM_clause5Paragraph9_1__c;
        clause6                   = mdtContrato.ISSM_CAM_clause6__c;
        clause7                   = mdtContrato.ISSM_CAM_clause7__c;
        clause8                   = mdtContrato.ISSM_CAM_clause8__c;
        clause9                   = mdtContrato.ISSM_CAM_clause9__c;
        clause10                  = mdtContrato.ISSM_CAM_clause10__c;
        clause11                  = mdtContrato.ISSM_CAM_clause11__c;
        clause11Paragraph1        = mdtContrato.ISSM_CAM_clause11Paragraph1__c;
        clause12                  = mdtContrato.ISSM_CAM_clause12__c;
        clause12Paragraph1        = mdtContrato.ISSM_CAM_clause12Paragraph1__c;
        labelNameCompany          = mdtContrato.ISSM_CAM_labelNameCompany__c;
        legalRepresentative       = mdtContrato.ISSM_CAM_legalRepresentative__c;

        getAssetCAM();
        //pagaré       
        numberOfCoolers                 = assetCAM.size();
        promissoryNoteTitle             = mdtPagare.ISSM_CAM_promissoryNoteTitle__c;
        promissoryNoteBueno             = mdtPagare.ISSM_CAM_promissoryNoteBueno__c;
        promissoryNoteLabelBueno        = mdtPagare.ISSM_CAM_promissoryNoteLabelBueno__c;
        promissoryNoteBuenoletter       = mdtPagare.ISSM_CAM_promissoryNoteBuenoletter__c;
        promissoryNoteLabelBuenoletter  = mdtPagare.ISSM_CAM_promissoryNoteLabelBuenoletter__c;
        promissoryNotePart1             = mdtPagare.ISSM_CAM_promissoryNotePart1__c;
        promissoryNotePart2             = mdtPagare.ISSM_CAM_promissoryNotePart2__c;
        promissoryNotePart3             = mdtPagare.ISSM_CAM_promissoryNotePart3__c;
        promissoryNotePart4             = mdtPagare.ISSM_CAM_promissoryNotePart4__c;
        promissoryNotePart51            = mdtPagare.ISSM_CAM_promissoryNotePart51__c;
        promissoryNotePart6             = mdtPagare.ISSM_CAM_promissoryNotePart6__c;
        promissoryLabelDebtor           = mdtPagare.ISSM_CAM_promissoryLabelDebtor__c;
        promissoryLabelName             = mdtPagare.ISSM_CAM_promissoryLabelName__c;
        promissoryLabelDomicile         = mdtPagare.ISSM_CAM_promissoryLabelDomicile__c;
        promissoryLabelColony           = mdtPagare.ISSM_CAM_promissoryLabelColony__c;
        promissoryLabelPopulation       = mdtPagare.ISSM_CAM_promissoryLabelPopulation__c;
        promissoryLabelFirm             = mdtPagare.ISSM_CAM_promissoryLabelFirm__c;
        promissoryNoteTitleNameUEN      = MapConfiguracionWs.size() > 0 ? (MapConfiguracionWs.get(strUEN) != null ? MapConfiguracionWs.get(strUEN).ISSM_Name_UEN__c: ''):'';       // CONFIGURACION               
    }
}