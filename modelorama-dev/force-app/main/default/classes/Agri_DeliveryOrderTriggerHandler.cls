public class Agri_DeliveryOrderTriggerHandler {    
    
    public static void asignaFolio(List<Delivery_order__c> lOrdenes) {
        System.debug('::: IN Agri_DeliveryOrderTriggerHandler.asignaFolio --> lOrdenes : ' + lOrdenes);
        
        Map<Id,RecordType> mapRT = new Map<Id,RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Delivery_order__c']);                
        
        AggregateResult[] agr = [SELECT max(Agri_nu_Conteo__c) FROM Delivery_order__c WHERE CreatedDate = TODAY AND RecordType.DeveloperName = 'Agri_tr_deliveryOrder'];
        Double conteo = double.valueOf(agr[0].get('expr0'));                
        System.debug('--> conteo HOY : ' + conteo); 
        if(conteo == null) { conteo = 0; }
        System.debug('--> conteo : ' + conteo);        
        
        Map<Id,User> mapUsers = new Map<Id,User>([SELECT id, Agri_Region__c FROM User WHERE Agri_Region__c != null]);
        List<Delivery_order__c> lDOupd = new List<Delivery_order__c>();
        
        for(Delivery_order__c dor : lOrdenes) {
            if(mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryOrder') {
                conteo++; 
                System.debug('--> dor.CreatedById : ' + dor.CreatedById);                
                String region = 'R';
                try {
                if(!Test.isRunningTest() && mapUsers.get(dor.CreatedById).Agri_Region__c != null)  { region = mapUsers.get(dor.CreatedById).Agri_Region__c.left(1); }
                } catch(Exception e) {
                    System.debug(e);
                }
                //region = mapUsers.get(dor.CreatedById).Agri_Region__c == null ? 'X' : mapUsers.get(dor.CreatedById).Agri_Region__c.left(1); //dor.Agri_Region__c == null ? 'X' : dor.Agri_Region__c.left(1);            
                String dia = String.valueOf(Date.today().day());   //dor.Agri_Delivery_date__c.day()
                String mes = String.valueOf(Date.today().month());
                String anio = String.valueOf(Date.today().year()).right(2);
                String conStr = String.valueOf(conteo.intValue());
                
                if(dia.length() == 1) { dia = '0' + dia; }
                if(mes.length() == 1) { mes = '0' + mes; }
                if(anio.length() == 1) { anio = '0' + anio; }               
                if(conStr.length() == 1) { conStr = '00' + conStr; }
                else if(conStr.length() == 2) { conStr = '0' + conStr; }
                
                Delivery_order__c d = new Delivery_order__c(); 
                d.Id = dor.Id;
                d.Agri_tx_registryNumber__c = region + dia + mes + anio + conStr; 
                d.Agri_nu_Conteo__c = conteo;   
                lDOupd.add(d);
            }
        }        
        System.debug('--> Conteo Final : ' + conteo);
        
        if(!lDOupd.isEmpty()){
            update lDOupd;
        }
        
        System.debug('::: OUT Agri_DeliveryOrderTriggerHandler.asignaFolio');
    }
    
    public static void gestionaUpdate(List<Delivery_order__c> lOrdenes) {
        System.debug('::: IN Agri_DeliveryOrderTriggerHandler.gestionaUpdate --> lOrdenes : ' + lOrdenes);
        
        Map<Id,RecordType> mapRT = new Map<Id,RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Delivery_order__c']);        
        Map<id,Delivery_order__c> mapOldOrdenes = (Map<id,Delivery_order__c>) Trigger.oldMap;
        
        //::::::::::::::::::::::::::::::::::::: ENTRADAS A ALMACEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
        Map<Id,Id> mapOrdenesWH = new Map<Id,Id>();
		Map<Id,Id> mapOrdenesCN = new Map<Id,Id>();   
        List<Id> lOrdenesLock = new List<Id>();
        Set<Id> setRechazadas = new set<Id>();
        for(Delivery_order__c dor : lOrdenes) {
            if(mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryOrder') {
                if(dor.Agri_ls_status__c == 'Entregada' && mapOldOrdenes.get(dor.Id).Agri_ls_status__c != 'Entregada') {
                    mapOrdenesWH.put(dor.Id, dor.Agri_rb_almacen__c);
                    mapOrdenesCN.put(dor.Id, dor.Agri_Contract__c);
                    lOrdenesLock.add(dor.Id);
                } else if(dor.Agri_ls_status__c == 'Rechazada' && mapOldOrdenes.get(dor.Id).Agri_ls_status__c != 'Rechazada') {
                    setRechazadas.add(dor.Id);
                }
            }           
        }                
        
        Map<Id,Agri_Warehouse__c> mapAlmacenes = new Map<Id,Agri_Warehouse__c>([SELECT Id, Agri_nu_totalDelivered__c FROM Agri_Warehouse__c WHERE Id IN : mapOrdenesWH.values()]); 
        Map<Id,Agri_Contract__c>  mapContracts = new Map<Id,Agri_Contract__c>([SELECT Id, Agri_nu_barleyTotalReceived__c FROM Agri_Contract__c WHERE Id IN : mapOrdenesCN.values()]);
        
        for(Agri_Quality_Control__c qa : [SELECT Id, Agri_nu_netWeight__c, Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c, Agri_pd_entregaPedido__r.Agri_rb_supplier__r.ONTAP__Email__c,
                                           Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c
                                          FROM Agri_Quality_Control__c 
                                          WHERE Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__c IN : mapOrdenesWH.keySet()
                                          AND Agri_ls_Result_of_quality_analysis__c = 'Aprobado']) 
        {
            if(mapAlmacenes.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c).Agri_nu_totalDelivered__c != null) {
                mapAlmacenes.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c).Agri_nu_totalDelivered__c += qa.Agri_nu_netWeight__c;
            } else {
                mapAlmacenes.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c).Agri_nu_totalDelivered__c = qa.Agri_nu_netWeight__c;
            }
            
            if(mapContracts.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c).Agri_nu_barleyTotalReceived__c != null) {
            	mapContracts.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c).Agri_nu_barleyTotalReceived__c += qa.Agri_nu_netWeight__c;     
            } else {
                mapContracts.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c).Agri_nu_barleyTotalReceived__c = qa.Agri_nu_netWeight__c; 
            }
            
            if(!Test.isRunningTest()) { Agri_PayOrderEmail_Ctrl.gestionaEnvioPDF(qa.Id); }            
        }
        
        if(!mapAlmacenes.values().isEmpty()) {
            update mapAlmacenes.values();
        }
        
        if(!mapContracts.values().isEmpty()) {
            update mapContracts.values();
        }     
        
        
        //::::::::::::::::::::::::: ENVIO PDF RECHAZADO ::::::::::::::::::::::::::::::::::
        for(Agri_Quality_Control__c qa : [SELECT Id, Agri_nu_netWeight__c, Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c, Agri_pd_entregaPedido__r.Agri_rb_supplier__r.ONTAP__Email__c,
                                           Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c
                                          FROM Agri_Quality_Control__c 
                                          WHERE Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__c IN : setRechazadas
                                          AND Agri_ls_Result_of_quality_analysis__c = 'Rechazado']) 
        {
            if(!Test.isRunningTest()) { Agri_PayOrderEmail_Ctrl.gestionaEnvioPDF(qa.Id); } 
        }
        
        
        //:::::::::::::::::::::::::::: SALIDAS DE ALMACEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
        Map<Id,Agri_Warehouse__c> mapAlmacenesAll = new Map<Id,Agri_Warehouse__c>([SELECT Id, Agri_nu_totalDelivered__c FROM Agri_Warehouse__c LIMIT 10000]); 
        
        for(Delivery_order__c dor : lOrdenes) {
            if(mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryMerchandise') {
                if(dor.Agri_nu_exitQuantityMerchandise__c != mapOldOrdenes.get(dor.Id).Agri_nu_exitQuantityMerchandise__c && dor.Agri_rb_almacen__c != mapOldOrdenes.get(dor.Id).Agri_rb_almacen__c) {
                    
                    //en almacen old, sumar OLD dor.Agri_nu_exitQuantityMerchandise__c
                    mapAlmacenesAll.get(mapOldOrdenes.get(dor.Id).Agri_rb_almacen__c).Agri_nu_totalDelivered__c += mapOldOrdenes.get(dor.Id).Agri_nu_exitQuantityMerchandise__c;
                    //en almacen new, restar y validad cantidad new
                    Decimal acumulado = mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c;
                    acumulado = acumulado - dor.Agri_nu_exitQuantityMerchandise__c;
                    if(acumulado < 0) {
                        dor.addError('No puede registrar una cantidad de salida mayor que la entrada');
                    } else {
                        mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c = acumulado;
                    }
                    
                    
                } else if(dor.Agri_nu_exitQuantityMerchandise__c != null && dor.Agri_nu_exitQuantityMerchandise__c != mapOldOrdenes.get(dor.Id).Agri_nu_exitQuantityMerchandise__c) {
                    
                    Decimal acumulado = mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c;
                    acumulado = (acumulado + mapOldOrdenes.get(dor.Id).Agri_nu_exitQuantityMerchandise__c) - dor.Agri_nu_exitQuantityMerchandise__c;
                    if(acumulado < 0) {
                        dor.addError('No puede registrar una cantidad de salida mayor que la entrada');
                    } else {
                        mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c = acumulado;
                    }
                    
                } else if(dor.Agri_rb_almacen__c != null && dor.Agri_rb_almacen__c != mapOldOrdenes.get(dor.Id).Agri_rb_almacen__c) {
                    
                    //Sumar cantidad a almacen old
                    mapAlmacenesAll.get(mapOldOrdenes.get(dor.Id).Agri_rb_almacen__c).Agri_nu_totalDelivered__c += dor.Agri_nu_exitQuantityMerchandise__c;
                    //restar cantidad en new
                    Decimal acumulado = mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c;
                    acumulado = acumulado - dor.Agri_nu_exitQuantityMerchandise__c;
                    if(acumulado < 0) {
                        dor.addError('No puede registrar una cantidad de salida mayor que la entrada');
                    } else {
                        mapAlmacenesAll.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c = acumulado;
                    }                    
                    
                }
            }            
        } 	
        
        if(!mapAlmacenesAll.values().isEmpty()) {
            update mapAlmacenesAll.values();
        }
        
        if(!lOrdenesLock.isEmpty()) {
            lockOrdenes(lOrdenesLock);
        }
        
        System.debug('::: OUT Agri_DeliveryOrderTriggerHandler.gestionaUpdate');
    }
        
    
    public static void gestionaInsert(List<Delivery_order__c> lOrdenes) {
        System.debug('::: IN Agri_DeliveryOrderTriggerHandler.gestionaInsert --> lOrdenes : ' + lOrdenes);
        
        Map<Id,RecordType> mapRT = new Map<Id,RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Delivery_order__c']);
        
        Map<Id,Delivery_order__c> mapSalidas = new Map<Id,Delivery_order__c>();
        for(Delivery_order__c dor : lOrdenes) {
            if(dor.Agri_nu_exitQuantityMerchandise__c != null && dor.Agri_rb_almacen__c != null && mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryMerchandise') {
                mapSalidas.put(dor.Agri_rb_almacen__c, dor);
            }
        }
        
        Map<Id,Agri_Warehouse__c> mapAlmacenes = new Map<Id,Agri_Warehouse__c>([SELECT Id, Agri_nu_totalDelivered__c FROM Agri_Warehouse__c WHERE Id IN : mapSalidas.keySet()]);
        
        for(Delivery_order__c dor : mapSalidas.values()) {
            Decimal acumulado = 0;
            acumulado = mapAlmacenes.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c;
            acumulado -= dor.Agri_nu_exitQuantityMerchandise__c;
            if(acumulado < 0) {
                dor.addError('No puede registrar una cantidad de salida mayor que la entrada a almacen');
            } else {
                mapAlmacenes.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c = acumulado;
            }
        } 
        
        if(!mapAlmacenes.values().isEmpty()) {
            update mapAlmacenes.values();
        }
        
        System.debug('::: OUT Agri_DeliveryOrderTriggerHandler.gestionaInsert');
    }
    
    
    public static void gestionaDelete(List<Delivery_order__c> lOrdenes) {
        System.debug('::: IN Agri_DeliveryOrderTriggerHandler.gestionaDelete --> lOrdenes : ' + lOrdenes);
        
        Map<Id,RecordType> mapRT = new Map<Id,RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Delivery_order__c']); 
        
        //::::::::::::::::::::::::::::::::::::: ENTRADAS A ALMACEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
        Map<Id,Id> mapOrdenesWH = new Map<Id,Id>();
		Map<Id,Id> mapOrdenesCN = new Map<Id,Id>();        
        for(Delivery_order__c dor : lOrdenes) {
            if(mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryOrder' && dor.Agri_ls_status__c == 'Entregada') {
                mapOrdenesWH.put(dor.Id, dor.Agri_rb_almacen__c);
                mapOrdenesCN.put(dor.Id, dor.Agri_Contract__c);   
            }           
        }
        
        Map<Id,Agri_Warehouse__c> mapAlmacenes = new Map<Id,Agri_Warehouse__c>([SELECT Id, Agri_nu_totalDelivered__c FROM Agri_Warehouse__c WHERE Id IN : mapOrdenesWH.values()]); 
        Map<Id,Agri_Contract__c>  mapContracts = new Map<Id,Agri_Contract__c>([SELECT Id, Agri_nu_barleyTotalReceived__c FROM Agri_Contract__c WHERE Id IN : mapOrdenesCN.values()]);
        
        for(Agri_Quality_Control__c qa : [SELECT Id, Agri_nu_netWeight__c, Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c, Agri_pd_entregaPedido__r.Agri_rb_supplier__r.ONTAP__Email__c,
                                           Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c
                                          FROM Agri_Quality_Control__c 
                                          WHERE Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__c IN : mapOrdenesWH.keySet()
                                          AND Agri_ls_Result_of_quality_analysis__c = 'Aprobado']) 
        {
            if(mapAlmacenes.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c).Agri_nu_totalDelivered__c != null) {
                mapAlmacenes.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_rb_almacen__c).Agri_nu_totalDelivered__c -= qa.Agri_nu_netWeight__c;
            }
            
            if(mapContracts.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c).Agri_nu_barleyTotalReceived__c != null) {
            	mapContracts.get(qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Agri_Contract__c).Agri_nu_barleyTotalReceived__c -= qa.Agri_nu_netWeight__c;     
            }
        }        
        
        if(!mapAlmacenes.values().isEmpty()) {
            update mapAlmacenes.values();
        }
        
        if(!mapContracts.values().isEmpty()) {
            update mapContracts.values();
        }

        
        //:::::::::::::::::::::::::::: SALIDAS DE ALMACEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
        Map<Id,Delivery_order__c> mapSalidas = new Map<Id,Delivery_order__c>();
        for(Delivery_order__c dor : lOrdenes) {
            if(dor.Agri_nu_exitQuantityMerchandise__c != null && dor.Agri_rb_almacen__c != null && mapRT.get(dor.RecordTypeId).DeveloperName == 'Agri_tr_deliveryMerchandise') {
                mapSalidas.put(dor.Agri_rb_almacen__c, dor);
            }
        }
        
        Map<Id,Agri_Warehouse__c> mapAlmacenesOUT = new Map<Id,Agri_Warehouse__c>([SELECT Id, Agri_nu_totalDelivered__c FROM Agri_Warehouse__c WHERE Id IN : mapSalidas.keySet()]);        
        for(Delivery_order__c dor : mapSalidas.values()) {
            if(mapAlmacenesOUT.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c != null) {
                mapAlmacenesOUT.get(dor.Agri_rb_almacen__c).Agri_nu_totalDelivered__c += dor.Agri_nu_exitQuantityMerchandise__c;
            }            
        } 
        
        if(!mapAlmacenesOUT.values().isEmpty()) {
            update mapAlmacenesOUT.values();
        }
        
        System.debug('::: OUT Agri_DeliveryOrderTriggerHandler.gestionaDelete');
    }
    
    public static void lockOrdenes(List<Id> lOrdenesLock) {
        List<Id> lEntregaLock = new List<Id>();
        for(Agri_receiving_orders__c ro : [SELECT Id FROM Agri_receiving_orders__c WHERE Agri_rb_deliveryOrder__c IN : lOrdenesLock AND Agri_ls_status__c = 'Recibido']) {
            lEntregaLock.add(ro.Id);
        }
        
        List<Id> lQALock = new List<Id>();
        for(Agri_Quality_Control__c qa : [SELECT Id FROM Agri_Quality_Control__c WHERE Agri_pd_entregaPedido__c IN : lEntregaLock AND Agri_ls_Result_of_quality_analysis__c = 'Aprobado']) {
            lQALock.add(qa.Id);
        }
        
        if(!lEntregaLock.isEmpty()) { lOrdenesLock.addAll(lEntregaLock); }
        if(!lQALock.isEmpty()) { lOrdenesLock.addAll(lQALock); }
        
        List<Approval.LockResult> lrOrdenes = Approval.lock(lOrdenesLock, false);        
        for(Approval.LockResult lr : lrOrdenes) {
            if (lr.isSuccess()) {
                System.debug('Bloqueado correctamente: ' + lr.getId());
            }
            /*else {              
                for(Database.Error err : lr.getErrors()) {                  
                    System.debug('ERROR : ' + err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Campos de error: ' + err.getFields());
                }
            }*/
        }
    }    

}