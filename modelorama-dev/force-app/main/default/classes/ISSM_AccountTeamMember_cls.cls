/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Carlos Duque  (CD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: Trigger que se ejecuta cuando se crea un comentario en el objeto Standard CaseComment
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Carlos Duque (CD)      Creador.
***********************************************************************************/
public with sharing class ISSM_AccountTeamMember_cls {

    //public final static String SUPERVISOR='Supervisor de Ventas';
    public final static String SUPERVISOR=System.label.ISSM_TeamMemberRoleSupervisor;
    
    public ISSM_AccountTeamMember_cls() {
        
    }

    public List<Id> getUserIdForAccountTeamMember(String teamMemberRole, Id accountId){
    	
		ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        List<Id> usersId = new List<Id>();
        List<AccountTeamMember> lstAccountTeamMember = new List<AccountTeamMember>();
        lstAccountTeamMember = objCSQuerys.QueryAccountTeamMemberObj(teamMemberRole,String.valueOf(accountId));
        for(AccountTeamMember a :  lstAccountTeamMember ){
            usersId.add(a.UserId);
        }
        return usersId;
    }   
}