/****************************************************************************************************
    Información general
    -------------------
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que deserealiza el response de creación de pedidos sugeridos

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       22-Octubre-2018       Hector Diaz                 Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
public class ISSM_DeserializeSuggestedOrder_tst {
    
	@isTest 
	static  void testDeserializeSuggestedOrder() {
		ISSM_DeserializeSuggestedOrder_cls objclass = new ISSM_DeserializeSuggestedOrder_cls();
		String json='{}';
		
		test.startTest();
			ISSM_DeserializeSuggestedOrder_cls.parse(json);	
		test.stopTest();	
	}
	
	@isTest 
	static  void testCrossSelling() {
		
		test.startTest();
			ISSM_DeserializeSuggestedOrder_cls.CrossSelling objclass = new ISSM_DeserializeSuggestedOrder_cls.CrossSelling('',0,0,'','',0,0);
		test.stopTest();	
	}
	
		
	@isTest 
	static  void testReplanishment() {
		
		test.startTest();
			ISSM_DeserializeSuggestedOrder_cls.Replanishment objclass = new ISSM_DeserializeSuggestedOrder_cls.Replanishment('',0,0,'','',0,0);
		test.stopTest();	
	}
			
	@isTest 
	static  void testSubstitute() {
		
		test.startTest();
			ISSM_DeserializeSuggestedOrder_cls.Substitute objclass = new ISSM_DeserializeSuggestedOrder_cls.Substitute('','',0,'','','',0,0);
		test.stopTest();	
	}
}