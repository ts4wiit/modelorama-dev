/**
 * Test class to generate Mock Http Responses class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
global class AllMobileMockHttpResponseGeneratorTest implements HttpCalloutMock {

	/**
	 * Respond method for the Mock Http Response.
	 *
	 * @param objHttpRequest	HttpRequest
	 * @return objHttpResponse	HttpResponse
	 */
	global HttpResponse respond(HttpRequest objHttpRequest) {

		//Variables.
		HttpResponse objHttpResponse = new HttpResponse();
		objHttpResponse.setHeader('Content-Type', 'application/json');
		String strBody = '';
		if(objHttpRequest.getMethod() == 'POST' && objHttpRequest.getHeader('Content-Type') == 'text/plain') {
			objHttpResponse.setStatusCode(201);
			objHttpResponse.setHeader('Content-Type', 'text/plain');
			strBody = '4573ea562c1e909376a37d06ea8285e3';
			objHttpResponse.setBody(strBody);
		} else if(objHttpRequest.getMethod() == 'POST' && objHttpRequest.getHeader('Content-Type') == 'application/json') {
			objHttpResponse.setStatusCode(201);
			strBody = 'Successfully Created';
			objHttpResponse.setBody(strBody);
		} else if(objHttpRequest.getMethod() == 'POST' && objHttpRequest.getHeader('Content-Type') == 'application/json; charset=UTF-8') {
			objHttpResponse.setStatusCode(201);
			strBody = 'Successfully Created';
			objHttpResponse.setBody(strBody);
		} else if(objHttpRequest.getMethod() == 'PUT') {
			objHttpResponse.setStatusCode(200);
			strBody = 'Successfully Updated';
		} else if(objHttpRequest.getMethod() == 'GET') {
			objHttpResponse.setStatusCode(200);
			if(objHttpRequest.getEndpoint().contains('routeAppVersion/all')) {
				strBody = '[{"routeappversionid":"1","route":"FG0000","version_id":"1.0.0","validstart":"2030-07-07T00:00:00.000Z","validend":"2098-06-28T00:00:00.000Z","softdeleteflag":false,"dtlastmodifieddate":"2025-06-08T00:21:10.000Z"}, {"routeappversionid":"2","route":"FG0001","version_id":"1.0.0","validstart":"2030-07-07T00:00:00.000Z","validend":"2098-06-14T00:00:00.000Z","softdeleteflag":false,"dtlastmodifieddate":"2025-06-07T23:20:40.000Z"}, {"routeappversionid":"10","route":"FG0010","version_id":"1.0.0","validstart":"2030-07-07T00:00:00.000Z","validend":"2098-06-14T00:00:00.000Z","softdeleteflag":false,"dtlastmodifieddate":"2025-06-07T23:20:40.000Z"}]';
				objHttpResponse.setBody(strBody);
			} else if(objHttpRequest.getEndpoint().contains('version/all')) {
				strBody = '[{"version_id":"1.0.0","application_id":1,"softdeleteflag":false,"dtlastmodifieddate":"2018-05-14T17:07:08.032Z"},{"version_id":"1.0.1","application_id":1,"softdeleteflag":false,"dtlastmodifieddate":"2018-05-29T17:20:24.000Z"},{"version_id":"VENTA+_(V1.0.0)_02_03_PRO","application_id":1,"softdeleteflag":true,"dtlastmodifieddate":"2018-05-16T00:09:00.000Z"},{"version_id":"VENTA+_(V2.0.0)_02_03_PRO","application_id":1,"softdeleteflag":false,"dtlastmodifieddate":"2018-05-16T00:11:00.000Z"},{"version_id":"2.0.0","application_id":1,"softdeleteflag":false,"dtlastmodifieddate":"2018-05-30T15:47:32.000Z"}]';
				objHttpResponse.setBody(strBody);
			} else if(objHttpRequest.getEndpoint().contains('application/all')) {
				strBody = '[{"application_id":1,"name":"PREVENTA","ship_type":"1","levelvalidation":1,"dtlastmodifieddate":"2018-06-08T01:10:50.000Z","softdeleteflag":false},{"application_id":2,"name":"AUTOVENTA","ship_type":"1","levelvalidation":1,"dtlastmodifieddate":"2018-06-08T01:10:50.000Z","softdeleteflag":false},{"application_id":3,"name":"REPARTO","ship_type":"1","levelvalidation":1,"dtlastmodifieddate":"2018-06-08T01:10:50.000Z","softdeleteflag":false},{"application_id":19,"name":"aplicacion19","ship_type":"1","levelvalidation":1,"dtlastmodifieddate":"2018-06-08T01:12:06.993Z","softdeleteflag":true},{"application_id":18,"name":"applicacion1000","ship_type":"1","levelvalidation":1,"dtlastmodifieddate":"2018-06-08T01:12:11.000Z","softdeleteflag":false},{"application_id":10,"name":"application1200","ship_type":null,"levelvalidation":1,"dtlastmodifieddate":"2018-06-15T23:17:31.176Z","softdeleteflag":true}]';
				objHttpResponse.setBody(strBody);
			} else if(objHttpRequest.getEndpoint().contains('deviceUser/all')) {
				strBody = '[{"id":"77788999","user_type":"ALL","vkbur":"FU07","user_name":"Griselda García","isactive":false,"route":"FG0002","email": "grissa@gmail.com","ldap_id": "459628275","password":"4573ea562c1e909376a37d06ea8285e3","softdeleteflag":false,"dtlastmodifieddate":"2018-07-09T15:13:41.000Z"}, {"id":"43252340","user_type":"ALL","vkbur":"FU06","user_name":"Roberto Hernández","isactive":true,"route":null,"email":"roberto@gmail.com","ldap_id":"4252","password":"996ddfc28ef353c605e364f2d6424bf","softdeleteflag":false,"dtlastmodifieddate":"2018-07-10T01:22:23.000Z"}, {"id":"9452344020","user_type":"ALL","vkbur":"FU10","user_name":"Antonio Leon","isactive":true,"route":"FG0005","email":"alregalado@gmail.com","ldap_id":"4255","password":"996ddfc28ef353c605e364f2d6424bf","softdeleteflag":false,"dtlastmodifieddate":"2018-07-10T01:22:23.000Z"}]';
				objHttpResponse.setBody(strBody);
			} else if(objHttpRequest.getEndpoint().contains('catUserType/all')) {
				strBody = '[{"user_type":"CAJ","user_type_desc":"Cajero","admon_externa":false,"permisos":"0010000000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"PRE","user_type_desc":"Prevendedor","admon_externa":false,"permisos":"0000000001","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"AUT","user_type_desc":"Autovendedor","admon_externa":false,"permisos":"0000000010","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"REP","user_type_desc":"Repartidor","admon_externa":false,"permisos":"0000000100","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"AYU","user_type_desc":"Ayudante","admon_externa":false,"permisos":"0000000000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"SADM","user_type_desc":"Super Administrador","admon_externa":false,"permisos":"1111111111","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"SN1","user_type_desc":"Soporte Nivel 1","admon_externa":false,"permisos":"1111000000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"TSAL","user_type_desc":"Tech Sales","admon_externa":false,"permisos":"1111100000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"ALM","user_type_desc":"Almacén Lleno y Vacío","admon_externa":false,"permisos":"1100000000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"ALV","user_type_desc":"Almacén Vacío","admon_externa":false,"permisos":"0100000000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:25:59.000Z"}, {"user_type":"ZYX","user_type_desc":"Almacén Eléctrico","admon_externa":true,"permisos":"0000101100","softdeleteflag":false,"dtlastmodifieddate":"2018-06-25T19:26:42.000Z"}, {"user_type":"OPN3","user_type_desc":"Operador Nivel 3","admon_externa":true,"permisos":"0011100000","softdeleteflag":false,"dtlastmodifieddate":"2018-06-28T19:14:12.000Z"}, {"user_type":"ALL","user_type_desc":"Almacén Lleno","admon_externa":false,"permisos":"1000000000","softdeleteflag":false,"dtlastmodifieddate":"2018-07-09T04:10:06.000Z"}]';
				objHttpResponse.setBody(strBody);
			}
		}
		return objHttpResponse;
	}
}