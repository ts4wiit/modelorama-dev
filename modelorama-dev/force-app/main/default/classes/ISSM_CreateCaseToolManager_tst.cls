/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
public class ISSM_CreateCaseToolManager_tst {
    public static Case DataCase;
    
    static testMethod void CaseToolManagerUpdateAccountTeam() {
        //String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
               
        //Account DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        
        //List<Case> lstCase = new List<Case>();
        //DataCase = ISSM_CreateDataTest_cls.fn_CreateCaseToolManager(true,'Descripcion','Asunto','New',null,DataAccount.Id);
        
        //lstCase.add(DataCase);
        //ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
    }
    
    static testMethod void testCaseCreate() {
        //INSERT CUSTOM SETTINGS 
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
        
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
	        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
	        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
	        matrix.ISSM_Entitlement__c = null;
	        matrix.ISSM_AssignedTo__c = 'Modelorama';
	        matrix.ISSM_OwnerQueue__c = null;
	        matrix.ISSM_StatusAssignQueue__c = null;
	        matrix.ISSM_OwnerUser__c = null;
	        matrix.ISSM_IdQueue__c = null;
	        matrix.ISSM_Email1CommunicationLevel3__c = null;
	        matrix.ISSM_Email1CommunicationLevel4__c = null;
	        matrix.ISSM_Email2CommunicationLevel3__c = null;
	        matrix.ISSM_Email2CommunicationLevel4__c = null;
	        matrix.ISSM_Email3CommunicationLevel3__c = null;
	        matrix.ISSM_Email3CommunicationLevel4__c = null;
	        matrix.ISSM_Email4CommunicationLevel4__c = null;
	        /*
	        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
	        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
	        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
			They were changed by labels
			*/
	        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
	        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
	        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        	insert matrix;
        
        //Create account for testing        
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        insert DataAccount;
        
        Test.startTest();
	        List<Case> lstCase = new List<Case>();
	        
	        Case caseNotEscalated = new Case();
	        caseNotEscalated.Subject = 'Subject';
	        caseNotEscalated.Description = 'Description';
	        caseNotEscalated.Status = 'New';
	        caseNotEscalated.AccountId = DataAccount.Id;
	        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
	        caseNotEscalated.IsEscalated = false; 
	        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id;
	        
	        lstCase.add(caseNotEscalated);
	        
	        Case caseEscalated = new Case();
	        caseEscalated.Subject = 'Subject';
	        caseEscalated.Description = 'Description';
	        caseEscalated.Status = 'New';
	        caseEscalated.AccountId = DataAccount.Id;
	        caseEscalated.OwnerId = usrWithoutMngr.Id;
	        caseEscalated.IsEscalated = true;
	        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
	        lstCase.add(caseEscalated);
	        
	        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
	        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
    
    static testMethod void testCaseCreate_Owner_User() {
        //INSERT CUSTOM SETTINGS
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
        
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        matrix.ISSM_Entitlement__c = null;
        matrix.ISSM_AssignedTo__c = 'User';
        matrix.ISSM_OwnerQueue__c = null;
        matrix.ISSM_StatusAssignQueue__c = null;
        matrix.ISSM_OwnerUser__c = usrWithoutMngr.Id;
        matrix.ISSM_IdQueue__c = null;
        matrix.ISSM_Email1CommunicationLevel3__c = null;
        matrix.ISSM_Email1CommunicationLevel4__c = null;
        matrix.ISSM_Email2CommunicationLevel3__c = null;
        matrix.ISSM_Email2CommunicationLevel4__c = null;
        matrix.ISSM_Email3CommunicationLevel3__c = null;
        matrix.ISSM_Email3CommunicationLevel4__c = null;
        matrix.ISSM_Email4CommunicationLevel4__c = null;
        /*
        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
		They were changed by labels
		*/
        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        insert matrix;
        
        //Create account for testing
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        insert DataAccount;
        Test.startTest();
        List<Case> lstCase = new List<Case>();
        
        Case caseNotEscalated = new Case();
        caseNotEscalated.Subject = 'Subject';
        caseNotEscalated.Description = 'Description';
        caseNotEscalated.Status = 'New';
        caseNotEscalated.AccountId = DataAccount.Id;
        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
        caseNotEscalated.IsEscalated = false;
        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseNotEscalated);
        
        Case caseEscalated = new Case();
        caseEscalated.Subject = 'Subject';
        caseEscalated.Description = 'Description';
        caseEscalated.Status = 'New';
        caseEscalated.AccountId = DataAccount.Id;
        caseEscalated.OwnerId = usrWithoutMngr.Id;
        caseEscalated.IsEscalated = true;
        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseEscalated);
        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
    
    static testMethod void testCaseCreate_Owner_Queue() {
        //INSERT CUSTOM SETTINGS
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
                
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        matrix.ISSM_Entitlement__c = null;
        matrix.ISSM_AssignedTo__c = 'Queue';
        matrix.ISSM_OwnerQueue__c = 'ISSm_WithoutOwner';
        matrix.ISSM_StatusAssignQueue__c = null;
        matrix.ISSM_OwnerUser__c = null;
        //matrix.ISSM_IdQueue__c = '00G63000001EY8F'; //q1.Id; //Without Owner Queue
        matrix.ISSM_Email1CommunicationLevel3__c = null;
        matrix.ISSM_Email1CommunicationLevel4__c = null;
        matrix.ISSM_Email2CommunicationLevel3__c = null;
        matrix.ISSM_Email2CommunicationLevel4__c = null;
        matrix.ISSM_Email3CommunicationLevel3__c = null;
        matrix.ISSM_Email3CommunicationLevel4__c = null;
        matrix.ISSM_Email4CommunicationLevel4__c = null;
        /*
        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
		They were changed by labels
		*/
        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        insert matrix;
        
        //Create account for testing
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        insert DataAccount;
        Test.startTest();
        List<Case> lstCase = new List<Case>();
        
        Case caseNotEscalated = new Case();
        caseNotEscalated.Subject = 'Subject';
        caseNotEscalated.Description = 'Description';
        caseNotEscalated.Status = 'New';
        caseNotEscalated.AccountId = DataAccount.Id;
        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
        caseNotEscalated.IsEscalated = false;
        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseNotEscalated);
        
        Case caseEscalated = new Case();
        caseEscalated.Subject = 'Subject';
        caseEscalated.Description = 'Description';
        caseEscalated.Status = 'New';
        caseEscalated.AccountId = DataAccount.Id;
        caseEscalated.OwnerId = usrWithoutMngr.Id;
        caseEscalated.IsEscalated = true;
        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseEscalated);
        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
    
    static testMethod void testCaseCreate_Owner_Supervisor() {
        //INSERT CUSTOM SETTINGS
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
        
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        matrix.ISSM_Entitlement__c = null;
        matrix.ISSM_AssignedTo__c = 'Supervisor';
        matrix.ISSM_OwnerQueue__c = null;
        matrix.ISSM_StatusAssignQueue__c = null;
        //matrix.ISSM_OwnerUser__c = usrWithoutMngr.Id;
        matrix.ISSM_IdQueue__c = null;
        matrix.ISSM_Email1CommunicationLevel3__c = null;
        matrix.ISSM_Email1CommunicationLevel4__c = null;
        matrix.ISSM_Email2CommunicationLevel3__c = null;
        matrix.ISSM_Email2CommunicationLevel4__c = null;
        matrix.ISSM_Email3CommunicationLevel3__c = null;
        matrix.ISSM_Email3CommunicationLevel4__c = null;
        matrix.ISSM_Email4CommunicationLevel4__c = null;
       /*
        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
		They were changed by labels
		*/
        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        
        insert matrix;
        
        //Create account for testing
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        insert DataAccount;
        Test.startTest();
        List<Case> lstCase = new List<Case>();
        
        Case caseNotEscalated = new Case();
        caseNotEscalated.Subject = 'Subject';
        caseNotEscalated.Description = 'Description';
        caseNotEscalated.Status = 'New';
        caseNotEscalated.AccountId = DataAccount.Id;
        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
        caseNotEscalated.IsEscalated = false;
        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseNotEscalated);
        
        Case caseEscalated = new Case();
        caseEscalated.Subject = 'Subject';
        caseEscalated.Description = 'Description';
        caseEscalated.Status = 'New';
        caseEscalated.AccountId = DataAccount.Id;
        caseEscalated.OwnerId = usrWithoutMngr.Id;
        caseEscalated.IsEscalated = true;
        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseEscalated);
        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
    
    static testMethod void testCaseCreate_Assigned_to_Billing_Manager() {
        //INSERT CUSTOM SETTINGS
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
        
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        matrix.ISSM_Entitlement__c = null;
        matrix.ISSM_AssignedTo__c = 'Billing Manager';
        matrix.ISSM_OwnerQueue__c = null;
        matrix.ISSM_StatusAssignQueue__c = null;
        //matrix.ISSM_OwnerUser__c = usrWithoutMngr.Id;
        matrix.ISSM_IdQueue__c = null;
        matrix.ISSM_Email1CommunicationLevel3__c = null;
        matrix.ISSM_Email1CommunicationLevel4__c = null;
        matrix.ISSM_Email2CommunicationLevel3__c = null;
        matrix.ISSM_Email2CommunicationLevel4__c = null;
        matrix.ISSM_Email3CommunicationLevel3__c = null;
        matrix.ISSM_Email3CommunicationLevel4__c = null;
        matrix.ISSM_Email4CommunicationLevel4__c = null;
        /*
        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
		They were changed by labels
		*/
        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        insert matrix;
        
        //Create account for testing
        String RecordTypeAccountId_SalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account salesOfficeAccount = new Account();
        salesOfficeAccount.RecordTypeId = RecordTypeAccountId_SalesOffice;
        salesOfficeAccount.Name = 'Sales Office';
        insert salesOfficeAccount;
        
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        DataAccount.ISSM_SalesOffice__c = salesOfficeAccount.Id;
        insert DataAccount;
        Test.startTest();
        List<Case> lstCase = new List<Case>();
        
        Case caseNotEscalated = new Case();
        caseNotEscalated.Subject = 'Subject';
        caseNotEscalated.Description = 'Description';
        caseNotEscalated.Status = 'New';
        caseNotEscalated.AccountId = DataAccount.Id;
        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
        caseNotEscalated.IsEscalated = false;
        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseNotEscalated);
        
        Case caseEscalated = new Case();
        caseEscalated.Subject = 'Subject';
        caseEscalated.Description = 'Description';
        caseEscalated.Status = 'New';
        caseEscalated.AccountId = DataAccount.Id;
        caseEscalated.OwnerId = usrWithoutMngr.Id;
        caseEscalated.IsEscalated = true;
        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseEscalated);
        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
    
    static testMethod void testCaseCreate_Assigned_to_Trade_Marketing() {
        //INSERT CUSTOM SETTINGS
        ISSM_AppSetting_cs__c custSett = new ISSM_AppSetting_cs__c();
        custSett.ISSM_IdQueueWithoutOwner__c = UserInfo.getUserId();
        insert custSett;
        
        //Create User without manager
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User usrWithoutMngr = new User(Alias = 'standt', 
                                       Email='userWithoutMng@example.com', 
                                       EmailEncodingKey='UTF-8', 
                                       LastName='Testing', 
                                       LanguageLocaleKey='en_US', 
                                       LocaleSidKey='en_US', 
                                       ProfileId = p.Id, 
                                       TimeZoneSidKey='America/Los_Angeles', 
                                       UserName='userWithoutMng@example.com');
        insert usrWithoutMngr;
        
        //INSERT TypificationMatrix
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c();
        matrix.ISSM_UniqueIdentifier__c = 'ABCDEF';
        matrix.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        matrix.ISSM_Entitlement__c = null;
        matrix.ISSM_AssignedTo__c = 'Trade Marketing';
        matrix.ISSM_OwnerQueue__c = null;
        matrix.ISSM_StatusAssignQueue__c = null;
        //matrix.ISSM_OwnerUser__c = usrWithoutMngr.Id;
        matrix.ISSM_IdQueue__c = null;
        matrix.ISSM_Email1CommunicationLevel3__c = null;
        matrix.ISSM_Email1CommunicationLevel4__c = null;
        matrix.ISSM_Email2CommunicationLevel3__c = null;
        matrix.ISSM_Email2CommunicationLevel4__c = null;
        matrix.ISSM_Email3CommunicationLevel3__c = null;
        matrix.ISSM_Email3CommunicationLevel4__c = null;
        matrix.ISSM_Email4CommunicationLevel4__c = null;
        /*
        matrix.ISSM_TypificationLevel1__c = 'Gestión herramienta';
        matrix.ISSM_TypificationLevel2__c = 'Gestión de usuarios';
        matrix.ISSM_TypificationLevel3__c = 'Actualizar Equipo de Cuenta';
		They were changed by labels
		*/
        matrix.ISSM_TypificationLevel1__c = System.label.ISSM_Level1_UpdateAccountTeam; // 'Gestión herramienta'
        matrix.ISSM_TypificationLevel2__c = System.label.ISSM_Level2_UpdateAccountTeam; // 'Gestión de usuarios'
        matrix.ISSM_TypificationLevel3__c = System.label.ISSM_Level3_UpdateAccountTeam; // 'Actualizar Equipo de Cuenta'
        insert matrix;
        
        //Create account for testing
        String RecordTypeAccountId_SalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account salesOfficeAccount = new Account();
        salesOfficeAccount.RecordTypeId = RecordTypeAccountId_SalesOffice;
        salesOfficeAccount.Name = 'Sales Office';
        insert salesOfficeAccount;
        
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account DataAccount = new Account();
        DataAccount.RecordTypeId = RecordTypeAccountId;
        DataAccount.Name = 'Customer Test';
        DataAccount.ISSM_SalesOffice__c = salesOfficeAccount.Id;
        insert DataAccount;
        Test.startTest();
        List<Case> lstCase = new List<Case>();
        
        Case caseNotEscalated = new Case();
        caseNotEscalated.Subject = 'Subject';
        caseNotEscalated.Description = 'Description';
        caseNotEscalated.Status = 'New';
        caseNotEscalated.AccountId = DataAccount.Id;
        caseNotEscalated.ISSM_UpdateAccountTeam__c = true;
        caseNotEscalated.IsEscalated = false;
        caseNotEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseNotEscalated);
        
        Case caseEscalated = new Case();
        caseEscalated.Subject = 'Subject';
        caseEscalated.Description = 'Description';
        caseEscalated.Status = 'New';
        caseEscalated.AccountId = DataAccount.Id;
        caseEscalated.OwnerId = usrWithoutMngr.Id;
        caseEscalated.IsEscalated = true;
        caseEscalated.ISSM_TypificationNumber__c = matrix.Id; 
        lstCase.add(caseEscalated);
        ISSM_CreateCaseToolManager_ctr toolManager = new ISSM_CreateCaseToolManager_ctr();
        ISSM_CreateCaseToolManager_ctr.CaseCreate(lstCase); 
        Test.stopTest();
    }
}