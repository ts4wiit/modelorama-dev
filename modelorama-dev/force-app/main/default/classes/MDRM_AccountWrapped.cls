public class MDRM_AccountWrapped {
    public Id id {get; set;}
    public String z019 {get; set;}
    public String Name {get; set;}
    public String MDRM_LastName {get; set;}
    public String ONTAP_PostalCode {get; set;}
    public String ONTAP_Colony {get; set;}
    public String ONTAP_Province {get; set;}
    public String ONTAP_Municipality {get; set;}
    public String ONTAP_Street {get; set;}
    public String ONTAP_Street_Number {get; set;}
    public String ONTAP_Email {get; set;}

    public Id ContactsId {get; set;}
    public String ContactsPhone {get; set;}
    public Date ContactsBirthdate {get; set;}
    public String ContactsGender {get; set;}

    public Id FormsId {get; set;}
    public string FormsFacebookAccount {get; set;}
    
    public MDRM_AccountWrapped() {
    }
}