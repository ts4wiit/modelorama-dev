/****************************************************************************************************
    General Information
    -------------------
    Author:     Hector Diaz
    email:      hdiaz@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the create PDF

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       15-Oct-2018    Hector Diaz                  Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_FinishOrderCreatePDF_cls {
    
    public string strIdOrder {get;set;} 
    public List<ONTAP__Order_Item__c> lstOrderItem {get;set;} 
    public List<ONTAP__Order_Item__c> lstOrderItemFinal {get;set;} 
    public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    
    public string strDistributionCenter {get;set;} 
    public string strRouteId {get;set;} 
    public string strServiceModel {get;set;} 
    public string strSapNumber {get;set;} 
    public string strBillingAddres {get;set;} 
    public string strTotaldisc {get;set;} 

    
    public string strNumOrder {get;set;} 
    public string strclientName {get;set;} 
    public string strTotalOrder {get;set;} 
    public string strBegindDate {get;set;} 
    //public string strDeleveryDate {get;set;} 
    //public string strUserCreateorder {get;set;} 
      
    public ISSM_FinishOrderCreatePDF_cls() {
        lstOrderItemFinal = new  List<ONTAP__Order_Item__c>();
        ONTAP__Order_Item__c objNewPdf = new ONTAP__Order_Item__c();
        strIdOrder = ApexPages.currentPage().getParameters().get('IdOrder');      
        //strIdOrder = 'a1Fg00000020yar';  

        if(String.isNotBlank(strIdOrder)){
            
            lstOrderItem = CTRSOQL.getOrderItemsPdf(strIdOrder);
            System.debug('lstOrderItem3 : '+ lstOrderItem);
        }
         
        if(lstOrderItem.size() > 0 ){
            for(ONTAP__Order_Item__c  objIterateOrderI : lstOrderItem  ){

                objNewPdf = new ONTAP__Order_Item__c();
                objNewPdf.ONCALL__OnCall_Product__r = objIterateOrderI.ONCALL__OnCall_Product__r;
                objNewPdf.ISSM_UnitPrice__c = objIterateOrderI.ISSM_UnitPrice__c.setscale(2);
                objNewPdf.ISSM_OrderItemSKU__c =   objIterateOrderI.ISSM_OrderItemSKU__c;
                objNewPdf.ISSM_MaterialProduct__c =  objIterateOrderI.ISSM_MaterialProduct__c;
                objNewPdf.ONCALL__OnCall_Quantity__c =  objIterateOrderI.ONCALL__OnCall_Quantity__c;
                objNewPdf.ISSM_TotalAmount__c =   objIterateOrderI.ISSM_TotalAmount__c.setscale(2);
                objNewPdf.ISSM_ItemDiscount__c = objIterateOrderI.ISSM_ItemDiscount__c;
                objNewPdf.ISSM_NoDiscountTotalAmount__c = objIterateOrderI.ISSM_NoDiscountTotalAmount__c.setscale(2);
                                 
                lstOrderItemFinal.add(objNewPdf);
            }
            System.debug('***lstOrderItemFinal : '+lstOrderItemFinal);
            strDistributionCenter = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__OnCall_Account__r.ISSM_DistributionCenter__c;
            strRouteId = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c;
            strServiceModel = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__Call__r.CallList__r.Route__r.ServiceModel__c;
            strSapNumber = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__OnCall_Account__r.ONTAP__SAP_Number__c;
            strclientName = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__OnCall_Account__r.Name;
            strBillingAddres = String.valueOf(lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__OnCall_Account__r.BillingStreet + ' - '+lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__OnCall_Account__r.BillingCity); 
            strTotalOrder = String.valueOf(lstOrderItem[0].ONTAP__CustomerOrder__r.ISSM_Subtotal__c.setscale(2));
            strTotaldisc = String.valueOf(lstOrderItem[0].ONTAP__CustomerOrder__r.ISSM_TotalDiscount__c.setscale(2));
            strNumOrder = lstOrderItem[0].ONTAP__CustomerOrder__r.ONCALL__SAP_Order_Number__c;
            strBegindDate = String.valueOf(lstOrderItem[0].ONTAP__CustomerOrder__r.ONTAP__BeginDate__c);    
            /*      
            strDeleveryDate = String.valueOf(lstOrderItem[0].ONTAP__CustomerOrder__r.ONTAP__DeliveryDate__c); 
                    
            */          
        }
    } 
}