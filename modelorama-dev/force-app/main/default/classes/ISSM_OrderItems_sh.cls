/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Scheduler to program the execution of order items sync

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       03-08-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_OrderItems_sh implements Schedulable {
	/**
	 * Execute method for the scheduler execution
	 * @param  Database.BatchableContext BC
	 */
	global void execute(SchedulableContext sc) {
        String stSalesOffice =Label.ISSM_SalesOfficeToProcess;
        List<String> arraySales = stSalesOffice.split(',');
        for(String salesOffice: arraySales){
		  Database.executeBatch(new ISSM_OrderItem_bch(true, salesOffice), 20);
        }
	}
}