/****************************************************************************************************
    General Information
    -------------------
    author:     Joseph Ceron
    email:      jceron@gmail.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Class for site control

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       15/08/2017      Joseph Ceron	JC 				Class created decorate class implemented
    2.0       15/09/2017      Hector Diaz	HD 				Class created decorate class implemented
    ================================================================================================
****************************************************************************************************/
global class ISSM_LaunchCampaingTelecolletion_ctr {
	public ISSM_LaunchCampaingTelecolletion_ctr() {
		
	}

	public ISSM_LaunchCampaingTelecolletion_ctr(ApexPages.StandardController controller){
		
	}

	@RemoteAction
	global static List<WrpOptions> LstOptionsCustomSet(){	
		List<WrpOptions> LstWrpOptions = new List<WrpOptions>();
		String IdUser =  UserInfo.getUserId();
		String CStrCountry = [Select Id,Name,Country from user Where id = : IdUser limit 1].Country;
		for(ISSM_OrderParameterTelecollections__c oCustomSet : [	Select 
																			Id, 
																			Name, 
																			ISSM_Active__c, 
																			ISSM_Country__c, 
																			ISSM_Object__c,
																			ISSM_OrderCriteria__c,
																			ISSM_Type__c
																	From 	ISSM_OrderParameterTelecollections__c
																	Where 	ISSM_Country__c = :CStrCountry
																	And 	ISSM_Object__c = 'Account'] )
		{ 
			LstWrpOptions.add(new WrpOptions(oCustomSet.Name,oCustomSet.ISSM_OrderCriteria__c+';'+oCustomSet.ISSM_Type__c));
		}

		return LstWrpOptions; 
	}
	/*
	private static String getCountAccountsError(){
		String retorno = null;
		Integer count = new ISSM_CustomerServiceQuerys_cls().getAccountNumberCampaing();
		if(count == 12000){
			retorno = Label.ISSM_MsgLimitError;
		}
		retorno = retorno == null ? '': retorno;
		return  retorno;
	}*/
	@RemoteAction
	global static Boolean GenerateCampaingFinal(String strCampaignName, String strStartDate, String strEndDate, Boolean active, String idCampaign, String filters, String drv, String salesOrg, String salesOff){
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '63','strCampaignName '+ strCampaignName);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '64','startDate '+ strStartDate);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '65','endDate '+ strEndDate);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '66','active '+ active);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '67','idCampaign '+ idCampaign);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '68','filters '+ filters);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '69','drv '+ drv);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '70','salesOrg '+ salesOrg);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '71','salesOff '+ salesOff);
		Date startDate;
		Date endDate;
		try{
		startDate = strEndDate != null ? Date.parse(strEndDate.trim()) : Date.today();
		} catch(Exception e){
			startDate = Date.today();
		}
		try{
		endDate = strEndDate != null ? Date.parse(strEndDate.trim()) : Date.today();
		} catch(Exception e){
			endDate = Date.today();
		}

		String soqlFilters = getFilters(filters, drv, salesOrg, salesOff);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '86','soqlFilters '+ soqlFilters);
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '87','startDate '+ startDate + ' Month ' + startDate.month());
		ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '88','endDate '+ endDate + ' Month ' + startDate.month());
        
        System.debug('strCampaignName: ' + strCampaignName + ' --- ' + 'startDate: ' + startDate + ' --- ' + 'endDate: ' + endDate + ' --- ' + 'active: ' + active + ' -- ' + 'idCampaign: ' + idCampaign + ' --- ' + 'soqlFilters: ' + soqlFilters);
        
		ISSM_LaunchCampaingTelecolletion_bch BtchProces = new ISSM_LaunchCampaingTelecolletion_bch(strCampaignName, startDate, endDate, active, idCampaign, soqlFilters); 
        database.executeBatch(BtchProces,150);
		return true;
	}

	public static String getFilters( String filters, String drv, String salesOrg, String salesOff ){
		String soqlFilters = 'and ';
		if(salesOff != null && salesOff != ''){
			soqlFilters += '  ISSM_SalesOffice__c =\''+ salesOff+'\' and ';
		} else if(salesOrg != null && salesOrg != ''){
			soqlFilters += ' ISSM_SalesOrg__c =\''+ salesOrg+'\' and ';
		} else if(drv != null && drv != ''){
			soqlFilters += ' ISSM_RegionalSalesDivision__c =\''+ drv+'\' and ';
		}
		String filtersFinal='';
		String[] filtersList;
		String[] specificFilterList;
		if(filters != null && filters != ''){
			filtersFinal = filters.replaceAll(';List', '');
			ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '96','filtersFinal '+ filtersFinal);
			filtersList = filtersFinal.split('¡!');
			ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '99','filtersList '+ filtersList);
			ISSM_Debug_cls.debug(ISSM_LaunchCampaingTelecolletion_ctr.class.getName(), 'GenerateCampaing', '100','filtersList '+ filtersList.size());
			for(String filter : filtersList){
				if(filter != null && filter != ''){
					specificFilterList = filter.split(',');
					soqlFilters += ' ' +specificFilterList[0] + ' =\''+ specificFilterList[1]+'\' and ';
				}
			}
		}
		if(soqlFilters.length() > 4){
			return soqlFilters.substring(0,soqlFilters.length()-4);	
		}
		return '';
	}

	@RemoteAction
	global static List<Account> getDRVs(){
		return new ISSM_Account_cls().getDRVs();
	}

	@RemoteAction
	global static List<Account> getAccountsByParentId(Id parentAccountId, Boolean salesorg){
		return new ISSM_Account_cls().getAccountsByParentId(parentAccountId, salesorg);
	}



	@RemoteAction
	global static list<WrpOptions> getPicklistValues( String fld){
			Set<String> SetFieldName = Schema.SObjectType.Account.fields.getMap().keySet();
			List<WrpOptions> LstOpcion = new List<WrpOptions>();
			if(SetFieldName.contains( fld.ToLowerCase())){
				Schema.sObjectType objType = Account.getSObjectType(); 
				Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
				map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
				list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
				for (Schema.PicklistEntry a : values)
				LstOpcion.add( new WrpOptions(a.getLabel() , a.getLabel() ) );
				return LstOpcion;
			}else{
				return null;
			}
		}
}