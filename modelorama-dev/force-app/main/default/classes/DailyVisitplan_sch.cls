/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Sheduler program batch and generate visit plans

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       05-07-2017        Nelson Sáenz Leal (NSL)  Creation Class
****************************************************************************************************/
global with sharing class DailyVisitplan_sch implements Schedulable {
    global void execute(SchedulableContext sc)
    {
        database.executebatch(new DailyVisitplan_bch(), 200);
    }
}