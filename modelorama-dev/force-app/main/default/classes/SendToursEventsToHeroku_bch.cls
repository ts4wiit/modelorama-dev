/****************************************************************************************************
   General Information
   -------------------
   author: Andrés Garrido
   email: agarrido@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Batch send tours and events to heroku

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       01-11-2017        Andres Garrido (AG)  		Creation Class
****************************************************************************************************/
global with sharing class SendToursEventsToHeroku_bch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		System.debug('\n*************** Start Send To Heroku ********************************');
		String strSoql 	= 	'Select Id, SynchronizedHeroku__c From ONTAP__Tour__c Where SynchronizedHeroku__c = false '; 
		strSoql			+= 	'And RecordType.DeveloperName=\'Presales\'';
		return Database.getQueryLocator(strSoql); 
	}
	
	global void execute(Database.BatchableContext BC, list<ONTAP__Tour__c> lstTours){
		System.debug('\n*************** Excecute Send To Heroku ********************************');
		set<Id> setIds = new set<Id>();
		set<Id> setIdsEvent = new set<Id>();
		for(ONTAP__Tour__c objTour : lstTours){
			setIds.add(objTour.Id);
			objTour.SynchronizedHeroku__c = true;
		}
			
		Boolean blnUpdateTours = false;
		if(!setIds.isEmpty())
			blnUpdateTours = TourOperations_cls.insertExternalTours(setIds, false);
			
		list<Event> lstEvents = [
			Select Id,SynchronizedHeroku__c From Event Where VisitList__c != null And VisitList__c = :setIds And SynchronizedHeroku__c=false
		];
		
		for(Event objEvent : lstEvents){
			setIdsEvent.add(objEvent.Id);
			objEvent.SynchronizedHeroku__c = true;
		}
		
		Boolean blnUpdateEvents = false;			
		if(!setIdsEvent.isEmpty())
			blnUpdateEvents = EventOperations_cls.insertEventToExternalObject(setIdsEvent, false);
			
		if(blnUpdateTours)
			update lstTours;
			
		if(blnUpdateEvents)
			update lstEvents;	
	}
	
	
    global void finish(Database.BatchableContext BC){
		System.debug('\n*************** Finish Send To Heroku ********************************');
        //programBatch();
    }
    
    /**
        * @description Method to automatically program the batch
        * @author Nelson Sáenz Leal
    **/
    /*public static void programBatch(){
        Datetime dtHoraActual       =   System.now();
        Datetime dtNextExecution    =   dtHoraActual.addDays(1);

        String strTime  =   dtNextExecution.second() +' ';
        strTime         +=  dtNextExecution.minute() +' ';
        strTime         +=  dtNextExecution.hour() +' ';
        strTime         +=  dtNextExecution.day() +' ';
        strTime         +=  dtNextExecution.month()+' ';
        strTime         +=  '? ';
        strTime         +=  dtNextExecution.year()+' ';

        String strDailyVisitplan    =   'DailyVisitPlanV2_sch-1';
        try
        {
            list<CronTrigger> lstCronTrigger    =   [SELECT Id
                                                        FROM  CronTrigger
                                                        WHERE CronJobDetail.Name =: strDailyVisitplan];

            if(lstCronTrigger.size() == 0)
            {
                String jobId = System.schedule(strDailyVisitplan, strTime, new DailyVisitPlanV2_sch(1) );
            }

        }catch(Exception e){ System.debug('\n ERROR ===========>>>>'+e.getMessage()); }
    }*/
}