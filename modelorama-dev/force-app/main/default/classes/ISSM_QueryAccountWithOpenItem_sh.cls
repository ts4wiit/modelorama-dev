/****************************************************************************************************
    General Information
    -------------------
    author:     Hecto Diaz
    email:      hdiaz@avaxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Class for site control   

    Information about changes (versions) 
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       20/Nov/2017      Hector Diaz  HD              Class created decorate class implemented
    ================================================================================================
****************************************************************************************************/
global with sharing class ISSM_QueryAccountWithOpenItem_sh  implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		ISSM_QueryAccountWithOpenItem_bch objQueryAccountWithOpenItem = new ISSM_QueryAccountWithOpenItem_bch();
		database.executebatch(objQueryAccountWithOpenItem,200);   
	}	
    
}