@isTest(seeAllData=false)
public class MDRM_ContractsTest {
    
    @testSetup static void setup() {
        Account org = new Account(
        	Name='Name'
        );
        insert org;
        
        Account acc = new Account(
        	Name = 'MDRM',
            ISSM_SalesOrg__c=org.Id
        );
        insert acc;
    }

    @isTest static void fectPickListTest() {
        MDRM_Contracts.fetchPicklistOptions(new MDRM_Document__c(), new List<String>{'MDRM_Status__c','MDRM_ClasificationPick__c','MDRM_PaymentPeriod__c','MDRM_Way_To_Pay__c','MDRM_Currency_Type__c'});
    }
    
    @isTest static void createContractProcess() {
        Account acc = [SELECT Name FROM Account LIMIT 1];
        
        Test.startTest();
        MDRM_Contracts.getAccount(acc.Id);
        
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Licencia',
            MDRM_Account__c = acc.Id
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contracts.getAccountContract(acc.Id);
        doc = MDRM_Contracts.getContract(mpResponse.get('id'));
        
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        entidad = MDRM_Contracts.getTitular(mpResponse.get('id'));
        MDRM_Contracts.deleteTitular(entidad);
        MDRM_Contracts.deleteDocument(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void runErrors() {
        MDRM_Contracts.saveDocument(null);
        MDRM_Contracts.saveTitular(null);
        MDRM_Contracts.deleteDocument(null);
        MDRM_Contracts.deleteTitular(null);
    }
    
    @isTest static void syncContractInfo() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(200, 'OK', '{"codigoError": "0","message": "SOLNPANCT0064074"}');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void syncContractInfo_codigo2() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(200, 'OK', '{"codigoError": "2","message": "Error"}');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void syncContractInfo_someError() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(200, 'OK', 'Error raro');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void syncContractInfo_timeout() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(504, 'Timeout', 'Timeout');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void syncContractInfo_badRequest() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(404, 'Bad request', '{"codigoError": "2","message": "Error de contracts"}');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }

    @isTest static void syncContractInfo_blankBody() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(520, '', '');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }
    
    @isTest static void syncContractInfo_internalError() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(500, '', '');
        Test.setMock(HttpCalloutMock.class, mock);

        Account acc = [SELECT Name FROM Account LIMIT 1];
        Id licenseRt = Schema.SObjectType.MDRM_Document__c.getRecordTypeInfosByDeveloperName().get('MDRM_License').getRecordTypeId();
        
        insert new MDRM_Document__c(
        	RecordTypeId = licenseRt,
            MDRM_Account__c = acc.Id,
            Name = 'Licencia',
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        MDRM_Document__c doc = new MDRM_Document__c(
        	Name = 'Contrato arrendamiento',
            MDRM_Account__c = acc.Id,
            MDRM_GuaranteeBond__c = 1,
            MDRM_ContractPeriod__c = 1,
            MDRM_Start_Date__c = Date.today(),
            MDRM_Total_contract_amount__c = 1,
            MDRM_RentAmount__c = 1,
            MDRM_Cost__c = 1,
            MDRM_Exp_Date__c = Date.today()+10
        );
        Map<String, String> mpResponse = MDRM_Contracts.saveDocument(doc);
        MDRM_Contract_Entity__c entidad = new MDRM_Contract_Entity__c(
            MDRM_Documento__c = mpResponse.get('id')
        );
        MDRM_Contracts.saveTitular(entidad);
        
        Test.startTest();
        MDRM_Contracts.sendToApproval(mpResponse.get('id'));
        Test.stopTest();
    }
}