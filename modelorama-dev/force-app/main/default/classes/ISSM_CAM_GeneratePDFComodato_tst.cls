/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description       :   Test class for class ISSM_CAM_GeneratePDFComodato_ctr
*
*  No.           Date              Author                      Description
* 1.0    05-Noviembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class ISSM_CAM_GeneratePDFComodato_tst {
	
	@testSetup static void loadData() {

		Account acc 					=  new Account();
            acc.Name 					= 'ACCTEST1';
            acc.ontap__sapcustomerid__c	= 'TEST';
            acc.ontap__salesoffid__c	= 'OFF';
            acc.ontap__salesogid__c		= 'ORG';
            acc.ONTAP__SAP_Number__c	= '0100ACTEST';
            acc.ISSM_Is_Blacklisted__C	= False;
            acc.ONTAP__Email__c			= 'test@est.com';
        insert acc; 

		ISSM_Asset__c asset 				= new ISSM_Asset__c();
			asset.name						= 'Unassignation';
			asset.Equipment_Number__c		= '0000UNASSIGNATION_';
			asset.ISSM_Serial_Number__c		= 'UNASSIGNATION_';
			asset.ISSM_Material_number__c	= '8000300';
			asset.ISSM_Status_SFDC__c		= 'Free Use';
			asset.ISSM_CutOverReason__c		= 'Lost Unit';
        insert asset;

		ONTAP__Case_Force__c caseForce = new  ONTAP__Case_Force__c();
			caseForce.ONTAP__Subject__c				='Retiro';
			caseForce.ISSM_Asset_CAM__c 			= asset.id;
			caseForce.ONTAP__Account__c 			= acc.id;
			caseForce.ISSM_TypificationLevel1__c 	= 'Refrigeración';
			caseForce.ISSM_ClassificationLevel2__c 	= 'Retiro de equipo';
			caseForce.ISSM_ApprovalStatus__c		= 'Approved';
			caseForce.ISSM_Delivery_Date_CAM__c 	= date.today().toStartOfWeek()+7;
        insert caseForce;
	}
	
	@isTest static void getCaseForceTEST() {
		Test.startTest();
			ISSM_CAM_GeneratePDFComodato_ctr generatePDFComodato= new ISSM_CAM_GeneratePDFComodato_ctr();
			ISSM_CAM_GeneratePDFComodato_ctr.getCaseForce([SELECT Id FROM ONTAP__Case_Force__c LIMIT 1].Id);
		Test.stopTest();
	}
	
}