public class ContentContractController {
    
    public String archivoSeleccionado{get;set;}
    public String 			Type 	{get; set;}
    public String 			fN 		{get; set;}
    public blob 			file 	{get; set;}
    public String 			title	{get; set;}
    public ContentVersion 	cv 		{get; set;}
    public Agri_Contract__c contract{get;set;}
    
    public ContentContractController (ApexPages.StandardController stdController){
        contract = (Agri_Contract__c) stdController.getRecord();
        contract = [SELECT Id, Name, Agri_Legal_Personality__c FROM Agri_Contract__c WHERE Id =: contract.Id];
    }
    
    public List<SelectOption> getArchivosOptionsContr(){
        List<selectOption> 	archivoListSO 	= new List<selectOption>();
        
        if(contract.Agri_Legal_Personality__c == 'F'){
            archivoListSO.add(new SelectOption('--Ninguno--','--Ninguno--')); 
            archivoListSO.add(new SelectOption('INE_1_INE','INE_1_INE')); 
            archivoListSO.add(new SelectOption('CURP_1_CURP','CURP_1_CURP')); 
            archivoListSO.add(new SelectOption('COMDOM_1_COMDOM','COMDOM_1_COMDOM'));
            archivoListSO.add(new SelectOption('ESCUBA_1_ESCUBA','ESCUBA_1_ESCUBA '));
            archivoListSO.add(new SelectOption('TITPRO_1_TITPRO','TITPRO_1_TITPRO'));
            archivoListSO.add(new SelectOption('RECOAG_1_RECOAG','RECOAG_1_RECOAG'));
            archivoListSO.add(new SelectOption('RFC_1_RFC','RFC_1_RFC'));
    	}
        
        if(contract.Agri_Legal_Personality__c == 'M') {
            archivoListSO.add(new SelectOption('--Ninguno--','--Ninguno--'));
            archivoListSO.add(new SelectOption('CURP_1_CURP','CURP_1_CURP')); 
            archivoListSO.add(new SelectOption('ACTCON_1_ACTCON','ACTCON_1_ACTCON'));
            archivoListSO.add(new SelectOption('COMDOM_1_COMDOM','COMDOM_1_COMDOM'));
            archivoListSO.add(new SelectOption('ESCUBA_1_ESCUBA','ESCUBA_1_ESCUBA '));
            archivoListSO.add(new SelectOption('RFC_1_RFC','RFC_1_RFC'));
            archivoListSO.add(new SelectOption('INE_2_INE','INE_2_INE')); 
            archivoListSO.add(new SelectOption('CURP_2_CURP','CURP_2_CURP')); 
            archivoListSO.add(new SelectOption('CODOFI_2_CODOFI','CODOFI_2_CODOFI'));
        }
        
        return archivoListSO;    
    }
    
    public PageReference upload() {
        ContentVersion cv 			= new ContentVersion();
        cv.versionData 				= file;
        cv.title 					= archivoSeleccionado;
        cv.pathOnClient 			= fN;
        cv.FirstPublishLocationId 	= ''+contract.Id; 
        try
        {
            insert cv;
            system.debug('*********************Result'+cv.pathonClient);
        }
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Document in Library'));
            return null;
        }
        finally
        {
            cv= new ContentVersion();
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'El archivo se adjuntó correctamente al contrato '+ contract.Name));
        return null;
    }

}