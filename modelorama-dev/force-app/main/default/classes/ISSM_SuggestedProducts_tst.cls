/****************************************************************************************************
    Información general
    -------------------
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       22-Octubre-2018       Hector Diaz                 Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_SuggestedProducts_tst {

	@isTest 
	static  void testDeserializeSuggestedOrder() {
		ISSM_SuggestedProducts_cls objclassSuggested = new ISSM_SuggestedProducts_cls();	
		
		insert new ISSM_PriceEngineConfigWS__c(
			Name = Label.ISSM_ConfigWSSuggestedOrder,
			ISSM_EndPoint__c = Label.ISSM_EPsuggestedProducts,
			ISSM_HeaderContentType__c = 'application/json',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = '',
			ISSM_Username__c = ''
		); 

		insert new ISSM_PriceEngineConfigWS__c(
			Name = Label.ISSM_ConfigWSAutorizationSuggested,
			ISSM_EndPoint__c = Label.ISSM_EPAuthSuggestedProds,
			ISSM_HeaderContentType__c = 'application/json',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = Label.ISSM_PasswordForSuggested,
			ISSM_Username__c = Label.ISSM_UserForSuggested
		);
			
		test.startTest();
			objclassSuggested.requestSuggestedOrder('0100169817',Date.today(),Date.today()+2);	
		test.stopTest();	
	}
	@isTest 
	static  void testDeserializeSuggestedOrderError() {
		ISSM_SuggestedProducts_cls objclassSuggested = new ISSM_SuggestedProducts_cls();	
		Map<String,ONTAP__Product__c> mapProduct = new Map<String,ONTAP__Product__c>();
		ONTAP__Product__c prod = new ONTAP__Product__c(
			ISSM_IsSuggested__c = 'S',
			ISSM_Suggested__c = '7',
			ISSM_Brand__c = 'Cucapa',
			ISSM_Size__c = 'Media',
			ISSM_ActualSale__c = '3',
			ONTAP__ProductShortName__c = '3',
			ISSM_Container_Size__c = '3',
			ONCALL__Material_Number__c = '3000005'
		);
		insert prod;

		ONTAP__Product__c prod2 = new ONTAP__Product__c(
			ISSM_IsSuggested__c = 'P',
			ISSM_Suggested__c = '17',
			ISSM_Brand__c = 'Corona Extra',
			ISSM_Size__c = 'Bote',
			ISSM_ActualSale__c = '3',
			ONTAP__ProductShortName__c = '3',
			ISSM_Container_Size__c = '3',
			ONCALL__Material_Number__c = '3000071'
		);
		insert prod2;

		insert new ISSM_PriceEngineConfigWS__c(
			Name = Label.ISSM_ConfigWSSuggestedOrder,
			ISSM_EndPoint__c = Label.ISSM_EPsuggestedProducts,
			ISSM_HeaderContentType__c = 'application/json',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = '',
			ISSM_Username__c = ''
		); 

		insert new ISSM_PriceEngineConfigWS__c(
			Name = Label.ISSM_ConfigWSAutorizationSuggested,
			ISSM_EndPoint__c = Label.ISSM_EPAuthSuggestedProds,
			ISSM_HeaderContentType__c = 'application/json',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = Label.ISSM_PasswordForSuggested,
			ISSM_Username__c = Label.ISSM_UserForSuggested
		);


		mapProduct.put(prod.ONCALL__Material_Number__c, prod);
		mapProduct.put(prod2.ONCALL__Material_Number__c, prod2);
		test.startTest();
			ISSM_DeserializeSuggestedOrder_cls objDes = objclassSuggested.requestSuggestedOrder('0100169817',Date.today(),Date.today()+2);	
			ISSM_SuggestedProducts_cls.requestAutentication();
			System.debug(loggingLevel.Error, '*** objDes: ' + objDes);
			objclassSuggested.getSuggestedProds(mapProduct, objDes, 'Push');
			objclassSuggested.getSuggestedProds(mapProduct, objDes, 'Suggested');
		test.stopTest();	
	}
	
	@isTest 
	static  void testWprRequestSuggested() {
		
		test.startTest();
			ISSM_SuggestedProducts_cls.WprRequestSuggested objclassSuggestedWpr = new ISSM_SuggestedProducts_cls.WprRequestSuggested('0000001234',Date.today(),Date.today()+2);	
		test.stopTest();	
	}
	
}