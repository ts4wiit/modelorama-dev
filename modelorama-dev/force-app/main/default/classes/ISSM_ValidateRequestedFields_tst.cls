/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_ValidateRequestedFields_tst {
	static Account ObjAcc = new Account();
	static List<Account> Ac_lstAcc = new List<Account>();
	static ONCALL__Call__c ObjCall = new ONCALL__Call__c();
    
    @isTest static void testValidateRequestedFields() {  
    	ISSM_ValidateRequestedFields_cls objcheckAccount = new ISSM_ValidateRequestedFields_cls();
		Id recTypeIdAcc = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
		Id RecTypeId =Schema.getGlobalDescribe().get('ONCALL__Call__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId();
		
		ObjAcc.Name                    = 'Account 1';
        ObjAcc.ISSM_CreditLimit__c     = 1000;
        ObjAcc.ONTAP__SAP_Number__c    = '0123456789';
        ObjAcc.RecordTypeId            = recTypeIdAcc;
        ObjAcc.ONTAP__Credit_Amount__c = 10;
        ObjAcc.ONCALL__OnCall_Route_Code__c = 'FG0050';
        Ac_lstAcc.add(ObjAcc);
        insert Ac_lstAcc;
		
		ObjCall = new ONCALL__Call__c(
       		Name            = 'TST',
         	ONCALL__POC__c  = Ac_lstAcc[0].Id,
       		RecordTypeId = RecTypeId,
       		ISSM_ValidateOrder__c=false
        );
        insert ObjCall;
        
		test.startTest();
        	objcheckAccount.checkAccountRequiredValues(Ac_lstAcc);
        	objcheckAccount.checkAccReqValuesUT(Ac_lstAcc);
        	objcheckAccount.checkOrderRouteWDSD(Ac_lstAcc[0].Id,'Telesales');
        	objcheckAccount.checkOrderRouteWODSD('FG0050');
        	objcheckAccount.checkCallList(ObjCall);
        	objcheckAccount.checkRoute(ObjCall);
        	objcheckAccount.checkCallsUniversalTelesales(Ac_lstAcc[0].Id,recTypeIdAcc);
        	objcheckAccount.CheckOutValidate(Ac_lstAcc);
		test.stopTest();
        
    }
}