/****************************************************************************************************
   General Information
   -------------------
   author: Andrés Garrido
   email: agarrido@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Batch to Synchronize the Event records between Heroku and Salesforce

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       31-07-2017        Andrés Garrido (AG)          Creation Class
****************************************************************************************************/
global class SyncToursAndEventsFromHerokuWS_bch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.StateFul{
    global String strSOQL;
    global DateTime dtLastRunProcess;
    global DateTime dtLastRun;
    global list<SyncObjects.ObjEventUpdate> lstNewObjExtEvent;
    global Integer intPos;
    
    public SyncToursAndEventsFromHerokuWS_bch(){
        strSOQL =   'Select Id, ONTAP__ActualStart__c, ONTAP__ActualEnd__c, Distance__c, EndMile__c, ONTAP__TourDate__c, ';
        strSOQL +=  'StartMile__c, ONTAP__TourStatus__c, TourSubStatus__c, ONTAP__TourId__c, ONTAP__IsActive__c ';
        strSOQL +=  'From ONTAP__Tour__c ';
        strSOQL +=  'Where (ONTAP__IsActive__c = true Or ONTAP__TourDate__c = TODAY) And RecordType.DeveloperName = \'Presales\'';  
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('\n******Start Sync Tours From Heroku *********');
        SyncHerokuParams__c sf = SyncHerokuParams__c.getAll().get('SyncToursEvents');
        //dtLastRun = sf.LastModifyDate__c;
        dtLastRun = DateTime.valueOfGMT(String.valueOf(sf.LastModifyDate__c));
        //dtLastRunProcess = DateTime.valueOfGMT(String.valueOf(DateTime.now()));
        dtLastRunProcess = DateTime.now();
        lstNewObjExtEvent = new list<SyncObjects.ObjEventUpdate>();
        intPos = 0;
        System.debug('\nQuery: '+strSOQL);
        return Database.getQueryLocator(strSOQL);
    }

    global void execute(Database.BatchableContext BC, list<ONTAP__Tour__c> lstScope){
        System.debug('\n******Execute Sync Tours From Heroku *********');
        set<String> setIdTours = new set<String>();
        map<String, ONTAP__Tour__c> mapTours = new map<String, ONTAP__Tour__c>();
        for(ONTAP__Tour__c objTour :lstScope){
            setIdTours.add(objTour.ONTAP__TourId__c);
            mapTours.put(objTour.Id, objTour);
        }
      
        //Fill the object with the parameters to get Event information in Heroku
        SyncObjects.GetEventObject objGet = new SyncObjects.GetEventObject();
        objGet.systemmodstamp = dtLastRun;
        objGet.tours = setIdTours;
        
        //Callout the heroku web service to obtain the events related with the tours
        SyncObjects.SyncEventObject objSyncEvent = SyncObjects.calloutGetHerokuEvents(objGet);
            
        //if there is a events to update, call the method
        if(objSyncEvent != null && objSyncEvent.events != null && !objSyncEvent.events.isEmpty()){
            updateSalesforceEvents(objSyncEvent);
        }
    }

    global void finish(Database.BatchableContext BC){
    	System.debug('\n******Finish Sync Tours From Heroku *********');
        if(!lstNewObjExtEvent.isEmpty()){       
            SyncObjects.UpdateEventObject objUpdate = new SyncObjects.UpdateEventObject();
            objUpdate.events = lstNewObjExtEvent;
            
            //Callout the heroku web service to update the events
            if(!Test.isRunningTest()) String res = SyncObjects.calloutUpdateHerokuEvents(objUpdate);
        }
        
        //Update the custom setting
        SyncHerokuParams__c sf = SyncHerokuParams__c.getAll().get('SyncToursEvents');
        sf.LastModifyDate__c = dtLastRunProcess;
        update sf;
        /*if(!Test.isRunningTest())*/ programBatch();
    }
    
    /**
    * Method to update the event info from heroku and create the off route events in salesforce
    * @params:
      1.-objSyncEvent: Object with the information that was changed in heroku
    * @return void
    **/
    public void updateSalesforceEvents(SyncObjects.SyncEventObject objSyncEvent){
        //set<String> setIdEvents = new set<String>();
        //map<String, SyncObjects.EventObject> mapExtEvent = new map<String, SyncObjects.EventObject>();
        
        set<String> setCustomerIds = new set<String>();
        set<String> setAgentNames = new set<String>();
        set<String> setTourNames = new set<String>();
        
        //list to fill the off route events
        list<SyncObjects.EventObject> lstAuxOffRoute = new list<SyncObjects.EventObject>();
        for(SyncObjects.EventObject objExtEvent :objSyncEvent.events){
            
            //fill the sets with the references ids
            if(objExtEvent.salesforceid == null && objExtEvent.isoffroute == true){
                setCustomerIds.add(objExtEvent.customerid);
                setAgentNames.add(objExtEvent.salesagent);
                setTourNames.add(objExtEvent.tourid);
                lstAuxOffRoute.add(objExtEvent);
                
                SyncObjects.ObjEventUpdate objUpdte = new SyncObjects.ObjEventUpdate();
                objUpdte.id = objExtEvent.id;
                objUpdte.sequence = Integer.valueOf(objExtEvent.sequence);
                lstNewObjExtEvent.add(objUpdte);
            }
        }
        
        System.debug('\nExternal New Events ==> '+lstAuxOffRoute);
        
        //Create the events with off route check
        if(!lstAuxOffRoute.isEmpty()){
            list<Event> lstNewEvents = getNewEventList(lstAuxOffRoute, setCustomerIds, setAgentNames, setTourNames);
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSync');
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSort');
            list<Database.SaveResult> lstSR = Database.insert(lstNewEvents, false);
            for(Database.SaveResult objSR : lstSR){
                if(objSR.isSuccess()){
                    lstNewObjExtEvent[intPos].salesforceid = objSR.getId();
                }
                else{ System.debug('\nERROOOOOR INSERT OFF ROUTE==> '+objSR.getErrors()[0].getMessage()); }
                intPos++;
            }
        }   
    }
    
    /**
    * Method to get the list with all new events 
    * @params:
      1.-lstObjExtEvent: list with the event information in heroku
      2.-setCustomerIds: set with customer ids
      2.-setAgentNames: set with sales agents
      2.-setTourNames: set with tour ids
    * @return list<Event> new event list
    **/
    public list<Event> getNewEventList(list<SyncObjects.EventObject> lstObjExtEvent, set<String> setCustomerIds, set<String> setAgentNames, set<String> setTourNames){
        map<String, String> mapAccCustomerId = new map<String, String>();
        list<Account> lstAcc = [
            Select  Id, ONTAP__SAP_Number__c
            From    Account
            Where   ONTAP__SAP_Number__c = :setCustomerIds
        ];
        
        for(Account objAcc :lstAcc)
            mapAccCustomerId.put(objAcc.ONTAP__SAP_Number__c, objAcc.Id);
            
        //Obtain the users from setAgentNames
        map<String, String> mapAgentNames = new map<String, String>();
        list<User> lstUser = [
            Select  Id, UserName
            From    User
            Where   UserName = :setAgentNames
        ];
        
        //Fill the map with users
        for(User objUser :lstUser)
            mapAgentNames.put(objUser.UserName, objUser.Id);
        
        //Obtains the tours from setTourNames
        map<String, String> mapTourNames = new map<String, String>();
        map<String, String> mapTourAgents = new map<String, String>();
        list<ONTAP__Tour__c> lstTours = [
            Select  Id, Name, Route__r.RouteManager__c
            From    ONTAP__Tour__c
            Where   Name = :setTourNames
        ];
        
        //Fill the map with tours
        for(ONTAP__Tour__c objTour :lstTours){
            mapTourNames.put(objTour.Name, objTour.Id);
            mapTourAgents.put(objTour.Name, objTour.Route__r.RouteManager__c);
        }
        
        //Create new events
        list<Event> lstNewEvent = new list<Event>();
        for(SyncObjects.EventObject objExt : lstObjExtEvent){   
            Event objNewEvent = new Event();
            objNewEvent.VisitList__c        =   mapTourNames.containsKey(objExt.tourid) ? mapTourNames.get(objExt.tourid) : null;
            objNewEvent.OwnerId             =   mapAgentNames.containsKey(objExt.salesagent) ? mapAgentNames.get(objExt.salesagent) : mapTourAgents.get(objExt.tourid);
            objNewEvent.WhatId              =   mapAccCustomerId.containsKey(objExt.customerid) ? mapAccCustomerId.get(objExt.customerid) : null;
            objNewEvent.Subject             =   Label.Subject;
            objNewEvent.CustomerId__c       =   objExt.customerid;
            objNewEvent.ONTAP__Estado_de_visita__c  =   objExt.visitstatus!=null ? objExt.visitstatus : null;
            objNewEvent.EventSubestatus__c          =   objExt.visitsubestatus!=null ? objExt.visitsubestatus : null;
            objNewEvent.Sequence__c                 =   objExt.sequence!=null ? objExt.sequence : null;
            objNewEvent.ONTAP__Control_inicio__c    =   objExt.controlinicio!=null ? objExt.controlinicio : null;
            objNewEvent.ONTAP__Control_fin__c       =   objExt.controlfin!=null ? objExt.controlfin : null;
            objNewEvent.RealSequence__c             =   objExt.realsequence!=null ? objExt.realsequence : null;
            objNewEvent.NumberCartons__c            =   objExt.numbercartons!=null ? objExt.numbercartons : null;
            objNewEvent.ReasonNotVisit__c           =   objExt.razonnovisita!=null ? objExt.razonnovisita : null;
            objNewEvent.ReasonNotSale__c            =   objExt.razonnoventa!=null ? objExt.razonnoventa : null;
            objNewEvent.EventSubestatus__c          =   objExt.visitsubestatus!=null ? objExt.visitsubestatus : null;
            objNewEvent.ONTAP__Latitud_inicio__c    =   objExt.latitude!=null ? objExt.latitude : null;
            objNewEvent.ONTAP__Longitud_inicio__c   =   objExt.longitude!=null ? objExt.longitude : null;
            objNewEvent.StartDateTime               =   objExt.starttime!=null ? objExt.starttime : System.now();
            objNewEvent.EndDateTime                 =   objExt.endtime!=null ? objExt.endtime : System.now().addMinutes(15);
            //objNewEvent.ActivityDateTime            =   objExt.starttime!=null ? objExt.starttime : System.now();
            objNewEvent.OrderDetailText__c          =   objExt.orderdetail!=null ? objExt.orderdetail.replace(',','-').replace(';','&') : null;
            objNewEvent.OffRoute__c                 =   true;
            lstNewEvent.add(objNewEvent);
        }
        
        return lstNewEvent;
    }
    
    
    public static void programBatch(){
        SyncHerokuParams__c sf = SyncHerokuParams__c.getAll().get('SyncToursEvents');
        Decimal runTime = sf.RunFrequency__c;
        //Datetime dtHoraActual       =   DateTime.valueOfGMT(String.valueOf(DateTime.now()));
        Datetime dtHoraActual       =   DateTime.now();
        Datetime dtNextExecution    =   dtHoraActual.addMinutes(Integer.valueOf(runTime));
        
        if(sf.IsActive__c && dtNextExecution.hour() >= sf.StartTime__c && dtNextExecution.hour() < sf.EndTime__c ){
            String strTime  =   dtNextExecution.second() + ' ';
            strTime         +=  dtNextExecution.minute() + ' ';
            strTime         +=  dtNextExecution.hour() + ' ';
            strTime         +=  dtNextExecution.day() + ' ';
            strTime         +=  dtNextExecution.month()+ ' ';
            strTime         +=  '? ';
            strTime         +=  dtNextExecution.year()+ ' ';
    
            String strSyncEvents = 'SyncToursEventsFromHeroku';
            System.debug('\nProgram Batch ===> '+strTime);
            try{
                
                for ( CronTrigger ct : [SELECT  Id
                    FROM    CronTrigger
                    WHERE   CronJobDetail.Name =: strSyncEvents] ) {
                    System.abortJob(ct.Id);
                }
                if(!Test.isRunningTest()) String jobId = System.schedule(strSyncEvents, strTime, new SyncToursAndEventsFromHerokuWS_sch());
    
            }catch(Exception e){
                System.debug('\n ERROR SYNC TOURS EVENTS ===========>>>> '+e.getMessage());
            }
        }
    }
    
}