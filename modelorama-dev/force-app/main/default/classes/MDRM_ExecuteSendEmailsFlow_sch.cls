/******************************************************************************* 
* Developed by      :   Everis México
* Author      :   Andrea Cedillo
* Project      :   AbInBev - Modeloramas
* Description    :   Schedule execution of flows for sending emails for Modeloramas. Run every day at 06:00:00 a.m.
*
* No.         Date               Author                      Description
* 1.0    	  13-June-2019       Andrea Cedillo              Create
*******************************************************************************/
global class MDRM_ExecuteSendEmailsFlow_sch implements Schedulable {
    @TestVisible
  	public static final String CRON_EXPR = Label.MDRM_CronExprExpansor;
    
    /*
      Called this from Anonymous Apex to schedule at hour stipulated.
    */
  	global static String scheduleIt() {
    	MDRM_ExecuteSendEmailsFlow_sch job = new MDRM_ExecuteSendEmailsFlow_sch();
    	return System.schedule((Test.isRunningTest() ? (Label.MDRM_NameSchSendEmails + System.now()) : Label.MDRM_NameSchSendEmails), CRON_EXPR, job);
  	}
   	/*
    	* Execution of the flows, for the send emails.
  	*/
  	global void execute(SchedulableContext sc) {
    	Map<String, Object> params1 = new Map<String, Object>();
        Flow.Interview.MDRM_AlertasPart1 flowAlertsPart1 = new Flow.Interview.MDRM_AlertasPart1(params1);
        
        Map<String, Object> params2 = new Map<String, Object>();
        Flow.Interview.MDRM_AlertasPart2 flowAlertsPart2 = new Flow.Interview.MDRM_AlertasPart2(params2);
        
        Map<String, Object> params3 = new Map<String, Object>();
        Flow.Interview.MDRM_AlertasPart3 flowAlertsPart3= new Flow.Interview.MDRM_AlertasPart3(params3);
        	
        if(!Test.isRunningTest()){ flowAlertsPart1.start(); flowAlertsPart2.start();flowAlertsPart3.start();}
  	}
}