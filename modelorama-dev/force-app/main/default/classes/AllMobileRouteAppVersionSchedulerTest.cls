/**
 * Test class for AllMobileRouteAppVersionSchedulerClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileRouteAppVersionSchedulerTest {

	/**
	 * Method to test execute method.
	 */
	static testMethod void testExecute() {

		//Set time.
		String strScheduleTimeRouteAppVersion = '0 57 * * * ?';

		//Start test.
		Test.startTest();

		//Schedule RouteAppVersion
		AllMobileRouteAppVersionSchedulerClass objAllMobileRouteAppVersionSchedulerClass = new AllMobileRouteAppVersionSchedulerClass();
		System.schedule('jobRouteAppVersion', strScheduleTimeRouteAppVersion, objAllMobileRouteAppVersionSchedulerClass);

		//Stop test.
		Test.stopTest();
	}
}