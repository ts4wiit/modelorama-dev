/******************************************************************************* 
* Developed by:   	ST4 Strategy
* Author:   		Daniel Hernández Díaz
* Project:   		AbInBev - Modeloramas
* Description:   	Handler for Quick Action and location component of Account object for Expander record type.
*
* No.         Date               Author                      Description
* 1.0    	  23-March-2020      Daniel Hernández            Created
*******************************************************************************/

public class MDRM_RecordLocation {
    @AuraEnabled
    public static Account updateGeolocalization(Id accId, Decimal lat, Decimal lon) {
        
        Account accToBeUpdated = [SELECT Id, ONTAP__Latitude__c, ONTAP__Longitude__c, MDRM_Store_Location__c, 
                                  ONTAP__Street__c, ONTAP__Street_Number__c, ONTAP__Municipality__c, ONTAP__Province__c, 
                                  ONTAP__Colony__c, ONTAP__PostalCode__c  FROM Account WHERE Id = :accId];
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        
        // Make a request to nominatim to obtain the address from the geolocation
        req.setEndpoint('https://nominatim.openstreetmap.org/reverse.php?format=html&lat='+lat+'&lon='+lon+'&zoom=18&format=json');
        HttpResponse res = h.send(req);
        
        Map<String,Object> gr = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        Map<String,Object> direccion = (Map<String,Object>)gr.get('address');
        
        
        String dir = '';
        System.debug(direccion);
        if(direccion.containsKey('road') && direccion.get('road')!=''){
            dir = dir + direccion.get('road') + ', ';
            if(accToBeUpdated.ONTAP__Street__c == null){
                accToBeUpdated.ONTAP__Street__c = String.valueOf(direccion.get('road'));
            }
        }if(direccion.containsKey('neighbourhood') && direccion.get('neighbourhood')!=''){
            dir = dir + direccion.get('neighbourhood') + ', ';
            if(accToBeUpdated.ONTAP__Colony__c == null){
                accToBeUpdated.ONTAP__Colony__c = String.valueOf(direccion.get('neighbourhood'));
            }
        }if(direccion.containsKey('suburb') && direccion.get('suburb')!=''){
            dir = dir + direccion.get('suburb') + ', ';
        }if(direccion.containsKey('memorial') && direccion.get('memorial')!=''){
            dir = dir + direccion.get('memorial') + ', ';
        }if(direccion.containsKey('postcode') && direccion.get('postcode')!=''){
            dir = dir + direccion.get('postcode') + ', ';
            if(accToBeUpdated.ONTAP__PostalCode__c == null){
                accToBeUpdated.ONTAP__PostalCode__c = String.valueOf(direccion.get('postcode'));
            }
        }if(direccion.containsKey('county') && direccion.get('county')!=''){
            dir = dir + direccion.get('county') + ', ';
            if(accToBeUpdated.ONTAP__Municipality__c == null){
                accToBeUpdated.ONTAP__Municipality__c = String.valueOf(direccion.get('county'));
            }
        }if(direccion.containsKey('town') && direccion.get('town')!=''){
            dir = dir + direccion.get('town') + ', ';
        }if(direccion.containsKey('state') && direccion.get('state')!=''){
            dir = dir + direccion.get('state') + ', ';
            if(accToBeUpdated.ONTAP__Province__c == null){
                accToBeUpdated.ONTAP__Province__c = String.valueOf(direccion.get('state'));
            }
        }if(direccion.containsKey('country') && direccion.get('country')!=''){
            dir = dir + direccion.get('country');
        }
        
        System.debug(direccion);
        
        accToBeUpdated.MDRM_Store_Location__c = dir;
        accToBeUpdated.ONTAP__Latitude__c = String.valueOf(lat);
        accToBeUpdated.ONTAP__Longitude__c = String.valueOf(lon);
        update accToBeUpdated;
        return accToBeUpdated;
    }
    
    @AuraEnabled
    public static Account getRecord(Id accId) {
        return [SELECT Id, ONTAP__Latitude__c, ONTAP__Longitude__c, MDRM_Store_Location__c FROM Account WHERE Id = :accId];
    }
}