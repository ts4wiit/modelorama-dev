/**
 * Test class for AllMobileCatUserTypeSchedulerClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileCatUserTypeSchedulerTest {

	/**
	 * Method to test execute method.
	 */
	static testMethod void testExecute() {

		//Set time.
		String strScheduleTimeCatUserType = '0 57 * * * ?';

		//Start test.
		Test.startTest();

		//Schedule CatUserType
		AllMobileCatUserTypeSchedulerClass objAllMobileCatUserTypeSchedulerClass = new AllMobileCatUserTypeSchedulerClass();
		System.schedule('jobCatUserType', strScheduleTimeCatUserType, objAllMobileCatUserTypeSchedulerClass);

		//Stop test.
		Test.stopTest();
	}
}