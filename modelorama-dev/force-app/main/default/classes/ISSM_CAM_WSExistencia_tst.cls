/**************************************************************************************
Nombre de la clase: ISSM_CAM_WSExistencias_tst
Versión : 1.0
Fecha de Creación : 20 Julio 2018
Funcionalidad : Apex test class to send orders to Mulesoft and SAP instead Case Force
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Daniel Rosales                                Class creation
*************************************************************************************/
@isTest
public class ISSM_CAM_WSExistencia_tst {
    @testSetup
    static void setup(){
        ISSM_PriceEngineConfigWS__c wsExistencia = new ISSM_PriceEngineConfigWS__c(
        Name='ConfigCAMWSExistencia', ISSM_EndPoint__c='https://cam-refrigeradores.us-w1.cloudhub.io/disponibilidad',
        ISSM_HeaderContentType__c='application/json', ISSM_Method__c='POST');
        insert wsExistencia;
    }
    static testMethod void runTestCAMWSExistencia(){
        Test.startTest();
        Long intStock = ISSM_CAM_WSExistencia_cls.wsExistencia('OFF','8000300');
        Long intError = ISSM_CAM_WSExistencia_cls.wsExistencia('FG00','8000300');
        Test.stopTest();
        system.debug('TEST Existencia: ' + intStock);
        system.debug('TEST Existencia error: ' + intError);
        system.assertEquals(100,intStock, 'Error al recuperar existencias: ' + intStock);
    }
}