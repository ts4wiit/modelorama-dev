/**
 * Test class for AllMobileDeviceUserHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserHelperTest {

	/**
	 * Test method to setup data.
	 */

	static testMethod void setup() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeAccountSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOrg');
		RecordType objRecordTypeVersionControlRoute = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileRoute');
		RecordType objRecordTypeVersionControlSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileSalesOffice');
		RecordType objRecordTypeVersionControlSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileSalesOrg');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');

		//Creating a Route... BEGINS.
		//Creating a User... BEGINS.

		//Get Profile: 'System Administrator'.
 		Profile objProfileSystemAdministrator = AllMobileUtilityHelperTest.getProfileObject('System Administrator');

		//Create a User.
		User objUserAgent = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'albertogomez@abinbev.org.dev', 'agome', 'alberto@gmail.com', 'ISO-8859-1', 'Gomez', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUserAgent;
		//Creating a User...ENDS

		//Create a Sales Org.
		Account objSalesOrg = AllMobileUtilityHelperTest.createSalesOrgObject(objRecordTypeAccountSalesOrg, 'Org1');
		insert objSalesOrg;

		//Create a Sales Office.
		Account objSalesOffice = AllMobileUtilityHelperTest.createSalesOfficeObject(objRecordTypeAccountSalesOffice, 'Office1Org1', 'FG00', objSalesOrg);
		insert objSalesOffice;
		Account objSalesOffice2 = AllMobileUtilityHelperTest.createSalesOfficeObject(objRecordTypeAccountSalesOffice, 'Office1Org1', 'FU07', objSalesOrg);
		insert objSalesOffice2;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle;

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;

		//Creating a Version Preventa.
		AllMobileVersion__c objAllMobileVersion = AllMobileUtilityHelperTest.createAllMobileVersionObject('1.0.0', objAllMobileApplication);
		insert objAllMobileVersion;

		//Create a Route.
		ONTAP__Route__c objOnTapRoute = AllMobileUtilityHelperTest.createOntapRouteObject(objRecordTypeOnTapRouteSales, objUserAgent, 'Presales', 'FG0001', 'Ruta centro', objSalesOffice, objOnTapVehicle);
		insert objOnTapRoute;
		//Creating a Route... ENDS

		//Create a Device User.
		AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1000000000', 'ALL', 'ALL', '1000000000');
		insert objAllMobilePermissionCategoryUserTypeAll;
		AllMobileCatUserType__c objAllMobileCatUserType = AllMobileUtilityHelperTest.createAllMobileCatUserType('ALL', false, false, 'Almacén lleno', 'ALL', objAllMobilePermissionCategoryUserTypeAll);
		insert objAllMobileCatUserType;
		AllMobileDeviceUser__c objAllMobileDeviceUser1 = AllMobileUtilityHelperTest.createAllMobileDeviceUser(false, 'Memento1', '', 'ricardoestrada@gmail.com', '7465345', '9452345345', objAllMobileCatUserType, 'Ricardo Estrada', objSalesOffice, objOnTapRoute, true);
		insert objAllMobileDeviceUser1;
		AllMobileDeviceUser__c objAllMobileDeviceUser2 = AllMobileUtilityHelperTest.createAllMobileDeviceUser(false, '', '', 'alejandrogomez@gmail.com', '2345234', '9452344939', objAllMobileCatUserType, 'Alejandro Gómez', objSalesOffice, objOnTapRoute, true);
		insert objAllMobileDeviceUser2;
		AllMobileDeviceUser__c objAllMobileDeviceUser3 = AllMobileUtilityHelperTest.createAllMobileDeviceUser(false, 'Memento2', '', 'alejandrogomez@gmail.com', '2345235', '9452344000', objAllMobileCatUserType, 'Roberto Hernández', objSalesOffice, objOnTapRoute, true);
		insert objAllMobileDeviceUser3;
		objAllMobileDeviceUser3.AllMobileDeviceUserSoftDeleteFlag__c = true;
		update objAllMobileDeviceUser3;
	}
}