/**
 * Developed by:	   Avanxo México
 * Author:			     Oscar Alvarez
 * Project:			     AbInbev - CAM
 * Description:		   Clase Controlador Apex del componente Lightning 'ISSM_CAM_Cutover_lcp'. 
 *
 *  No.        Fecha                  Autor               Descripción
 *  1.0    13-Noviembre-2018         Oscar Alvarez             CREATION
 *
 */
@isTest
private class ISSM_CAM_GenerateTemplateEmail_tst {
	@testSetup static void loadData() {}
	
	@isTest static void testGenerateTemplateEmailCenter() {
		ISSM_CAM_GenerateTemplateEmail_cls tst = new ISSM_CAM_GenerateTemplateEmail_cls();
		ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReason = new List<ISSM_CAM_ValidateCutover_ctr.WprCounterReason>();
		ISSM_CAM_ValidateCutover_ctr.WprCounterReason  wrp1 = new ISSM_CAM_ValidateCutover_ctr.WprCounterReason();
		ISSM_CAM_ValidateCutover_ctr.WprCounterReason  wrp2 = new ISSM_CAM_ValidateCutover_ctr.WprCounterReason();
		ISSM_CAM_ValidateCutover_ctr.WprCounterReason  wrp3 = new ISSM_CAM_ValidateCutover_ctr.WprCounterReason();
		wrp1.ReasonSpanish = 'Equipo Faltante';
		wrp1.ReasonEnglish= 'Missing Unit';
		wrp1.Counter= 2;
		lstWprCounterReason.add(wrp1);
		wrp2.ReasonSpanish = 'OK';
		wrp2.ReasonEnglish= 'OK';
		wrp2.Counter= 0;
		lstWprCounterReason.add(wrp2);
		wrp3.ReasonSpanish = 'Equipo sobrante';
		wrp3.ReasonEnglish= 'Surplus equipement';
		wrp3.Counter= 0;
		lstWprCounterReason.add(wrp3);

		ISSM_CAM_GenerateTemplateEmail_cls.generateTemplateEmailCenter(lstWprCounterReason
													,'center TEST'
													,'centerCode'
													,'totalErrors'
													,'initialBasis'
													,'totalCountedEquipment'
                                                    ,'level'
                                                    ,'URL');
	}
	
}