@isTest
public class ISSM_CoachingVisitsController_tst {
    private static final Map<String, RecordType> mapAccountRecordTypes = DevUtils_cls.getRecordTypes('Account', 'DeveloperName');
    private static final Map<String, RecordType> mapRT = DevUtils_cls.getRecordTypes('ONTAP__Route__c', 'DeveloperName');
    
    @testSetup
    private static void testData(){
        Profile p = [SELECT id FROM Profile WHERE Name='Hunter Supervisor' limit 1];
        User u = new User(Alias = 'TV', 
                          Email='tv@gmodelo.com',
                          EmailEncodingKey='UTF-8',
                          LastName='Testing', 
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='testusercoaching@gmodelo.com.mx');
        insert u;      
        u = new User(Alias = 'TV', 
                     Email='tv@gmodelo.com',
                     EmailEncodingKey='UTF-8',
                     LastName='Testing', 
                     LanguageLocaleKey='en_US',
                     LocaleSidKey='en_US', 
                     ProfileId = p.Id,
                     Manager = u,
                     TimeZoneSidKey='America/Los_Angeles', 
                     UserName='testusercoaching2@gmodelo.com.mx');
        insert u; 

        ISSM_Coaching__c coachSett = new ISSM_Coaching__c();
        coachSett.Name = 'ISSM_CoachingSettings';
        coachSett.ISSM_ApplyToAutosales__c = true;
        coachSett.ISSM_ApplyToBDR__c = true;
        coachSett.ISSM_ApplyToNationalAccount__c = true;
        coachSett.ISSM_ApplyToPresales__c = true;
        coachSett.ISSM_ApplyToTelesales__c = true;
        insert coachSett;
        
    }
    
    static void createData(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
        insertCustomSetting();
        User u = [SELECT Id, Username FROM User WHERE UserName='testusercoaching2@gmodelo.com.mx'];
        //dsd data
        Account objAccOrg              = new Account();
        objAccOrg.RecordTypeId         = mapAccountRecordTypes.get('SalesOrg').Id;
        objAccOrg.Name                 = 'CMM Test org';
        objAccOrg.StartTime__c         = '10:00';
        objAccOrg.EndTime__c           = '15:00';
        objAccOrg.ONTAP__SalesOgId__c  = '3116';
        insert objAccOrg;
        
        Account objAccount2              = new Account();
        objAccount2.RecordTypeId         = mapAccountRecordTypes.get('SalesOffice').Id;
        objAccount2.Name                 = 'CMM Test';
        objAccount2.StartTime__c         = '00:00';
        objAccount2.EndTime__c           = '14:00';
        objAccount2.ISSM_SalesOrg__c     = objAccOrg.Id;
        objAccount2.ONTAP__SalesOgId__c  = '3116';
        objAccount2.ONTAP__SalesOffId__c = 'FZ08';
        insert objAccount2;
        
        Account objAccount3                         = new Account();
        objAccount3.RecordTypeId            = mapAccountRecordTypes.get('SalesOffice').Id;
        objAccount3.Name                    = 'CMM Test3';
        objAccount3.StartTime__c            = '10:00';
        objAccount3.EndTime__c              = '14:00';
        objAccount3.ISSM_SalesOrg__c        = objAccOrg.Id;
        objAccount3.ONTAP__SalesOffId__c    = 'FZ09';
        objAccount3.ONTAP__SalesOgId__c  	= '3116';
        insert objAccount3;
        
        Account objAccount                                = new Account();
        objAccount.RecordTypeId                   = mapAccountRecordTypes.get('Account').Id;
        objAccount.Name                           = 'CMM Dolores FZ08';
        objAccount.StartTime__c                   = '12:00';
        objAccount.EndTime__c                     = '14:00';
        objAccount.ISSM_SalesOffice__c            = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c         = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c    = null;
        objAccount.ONTAP__SalesOgId__c            = '3116';
        objAccount.ONTAP__SalesOffId__c           = 'FZ08';
        objAccount.ONTAP__SAP_Number__c			  = '0888888888';
        insert objAccount;
        
        ONTAP__Route__c objONTAPRoute       = new ONTAP__Route__c();
        objONTAPRoute.RecordTypeId			= mapRT.get('Sales').Id;
        objONTAPRoute.ServiceModel__c       = 'Telesales';
        objONTAPRoute.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute.RouteManager__c       = u.Id;
        objONTAPRoute.Supervisor__c         = u.id;
        insert objONTAPRoute;
        
        AccountByRoute__c objAccxRoute                        = new AccountByRoute__c();
        objAccxRoute.Route__c               = objONTAPRoute.Id;
        objAccxRoute.Account__c             = objAccount.Id;
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   true;
        objAccxRoute.Thursday__c            =   true;
        objAccxRoute.Tuesday__c             =   true;
        objAccxRoute.Friday__c              =   true;
        objAccxRoute.Monday__c              =   true;
        objAccxRoute.Sunday__c              =   true;
        insert objAccxRoute;
        
        
        ONTAP__Route__c objONTAPRoute2                       = new ONTAP__Route__c();
        objONTAPRoute2.RecordTypeId			 = mapRT.get('Sales').Id;
        objONTAPRoute2.ServiceModel__c       = 'BDR';
        objONTAPRoute2.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute2.RouteManager__c       = u.Id;
        objONTAPRoute2.Supervisor__c         = u.id;
        insert objONTAPRoute2;
        
        VisitPlan__c objISSMVisitPlan                    =   new VisitPlan__c();
        objISSMVisitPlan.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan.Saturday__c        =   true;
        objISSMVisitPlan.Wednesday__c       =   true;
        objISSMVisitPlan.Thursday__c        =   true;
        objISSMVisitPlan.Tuesday__c         =   true;
        objISSMVisitPlan.Friday__c          =   true;
        objISSMVisitPlan.Monday__c          =   true;
        objISSMVisitPlan.Sunday__c          =   true;
        objISSMVisitPlan.VisitPlanId__c     =   '1';
        objISSMVisitPlan.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan;
        
        VisitPlan__c objISSMVisitPlan3                    =   new VisitPlan__c();
        objISSMVisitPlan3.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan3.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan3.Saturday__c        =   false;
        objISSMVisitPlan3.Wednesday__c       =   false;
        objISSMVisitPlan3.Thursday__c        =   false;
        objISSMVisitPlan3.Tuesday__c         =   false;
        objISSMVisitPlan3.Friday__c          =   false;
        objISSMVisitPlan3.Monday__c          =   false;
        objISSMVisitPlan3.Sunday__c          =   false;
        objISSMVisitPlan3.VisitPlanId__c     =   '3';
        objISSMVisitPlan3.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan3;
        
        VisitPlan__c objISSMVisitPlan2                    =   new VisitPlan__c();
        objISSMVisitPlan2.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan2.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan2.Saturday__c        =   true;
        objISSMVisitPlan2.Wednesday__c       =   true;
        objISSMVisitPlan2.Thursday__c        =   true;
        objISSMVisitPlan2.Tuesday__c         =   true;
        objISSMVisitPlan2.Friday__c          =   true;
        objISSMVisitPlan2.Monday__c          =   true;
        objISSMVisitPlan2.Sunday__c          =   true;
        objISSMVisitPlan2.VisitPlanId__c     =   '2';
        objISSMVisitPlan2.Route__c           =   objONTAPRoute2.Id;
        insert objISSMVisitPlan2;
        
        AccountByVisitPlan__c objISSMAccByVisitPlan =   new AccountByVisitPlan__c();
        objISSMAccByVisitPlan.Account__c            =   objAccount.Id;
        objISSMAccByVisitPlan.VisitPlan__c          =   objISSMVisitPlan.Id;
        objISSMAccByVisitPlan.Sequence__c           =   1;
        objISSMAccByVisitPlan.WeeklyPeriod__c       =   '1';
        objISSMAccByVisitPlan.LastVisitDate__c      =  System.today().addDays(-1);
        objISSMAccByVisitPlan.Saturday__c           =   true;
        objISSMAccByVisitPlan.Thursday__c           =   true;
        objISSMAccByVisitPlan.Tuesday__c            =   true;
        objISSMAccByVisitPlan.Friday__c             =   true;
        objISSMAccByVisitPlan.Monday__c             =   true;
        objISSMAccByVisitPlan.Wednesday__c          =   true;
        objISSMAccByVisitPlan.Sunday__c             =   true;
        insert objISSMAccByVisitPlan;
        
        AccountByVisitPlan__c objISSMAccByVisitPlan2 =   new AccountByVisitPlan__c();
        objISSMAccByVisitPlan2.Account__c            =   objAccount.Id;
        objISSMAccByVisitPlan2.VisitPlan__c          =   objISSMVisitPlan2.Id;
        objISSMAccByVisitPlan2.Sequence__c           =   5;
        objISSMAccByVisitPlan2.WeeklyPeriod__c       =   '1';
        objISSMAccByVisitPlan2.LastVisitDate__c      =   System.today().addDays(-1);
        objISSMAccByVisitPlan2.Saturday__c           =   true;
        objISSMAccByVisitPlan2.Thursday__c           =   true;
        objISSMAccByVisitPlan2.Tuesday__c            =   true;
        objISSMAccByVisitPlan2.Friday__c             =   true;
        objISSMAccByVisitPlan2.Monday__c             =   true;
        objISSMAccByVisitPlan2.Wednesday__c          =   true;
        objISSMAccByVisitPlan2.Sunday__c             =   true;
        insert objISSMAccByVisitPlan2;
        
        ONTAP__Tour__c tours = new ONTAP__Tour__c(Route__c=objONTAPRoute2.Id, ONTAP__TourId__c='T-000543723',
                                                  ONTAP__TourStatus__c = 'Assigned', ONTAP__IsActive__c=true,VisitPlan__c=objISSMVisitPlan2.id,
                                                  ONTAP__TourDate__c=System.today());
        insert tours;
        Event evt = new Event(visitlist__c=tours.Id,  ONTAP__Estado_de_visita__c='Abierto', WhatId=objAccount.Id,
                              StartDateTime=System.now(),EndDateTime=System.now().addHours(10));
        insert evt;
    }
    
    private static testmethod void test(){
        TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterInsertSync');
        TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSync');
        createData();
        User u = [SELECT Id, Username FROM User WHERE UserName='testusercoaching2@gmodelo.com.mx'];
        String userId = '';
        System.runAs(u){
            ISSM_CoachingVisitsController ctr = new ISSM_CoachingVisitsController(); 
            userId = ctr.getUserId();
        }
        ONTAP__Tour__c t =  [SELECT ONTAP__TourDate__c, Id, Route__r.RouteManager__r.UserName FROM ONTAP__Tour__c limit 1];
        System.debug(':::: toursss: '+t);
        System.debug(':::: touRouteManager__r: '+t.Route__r.RouteManager__r.UserName);
        System.debug(':::: userId: '+userId);
        List<Account> acctList = ISSM_CoachingVisitsController.getToursVisitsFromUser(
            System.today().year()+'-'+System.today().month()+'-'+System.today().day(), u.Id);
        
    }
    
    private static testmethod void test_saveCoaching(){
        createData();
        User u = [SELECT Id, Username FROM User WHERE UserName='testusercoaching2@gmodelo.com.mx'];
        
        String userId = '';
        System.runAs(u){
            ISSM_CoachingVisitsController ctr = new ISSM_CoachingVisitsController(); 
            userId = ctr.getUserId();
        }
        Account a = [SELECT Id from Account limit 1];
        test.startTest();
        List<Event> evtList =  ISSM_CoachingVisitsController.saveCoaching('[{"Id":"'+a.Id+'"}]', userId, System.today().year()+'-'+System.today().month()+'-'+System.today().day());
        ISSM_CoachingVisitsController.CoachingWrapper wrp = new ISSM_CoachingVisitsController.CoachingWrapper();
        ISSM_CoachingVisitsController.CoachingWrapper wrp2 = new ISSM_CoachingVisitsController.CoachingWrapper();
        wrp = new ISSM_CoachingVisitsController.CoachingWrapper(Date.today(), 'bdr', 'bdrId','route','routeId',10,'achingApplied');
        wrp2 = new ISSM_CoachingVisitsController.CoachingWrapper(Date.today().addDays(10), 'bdr', 'bdrId','route','routeId',10,'achingApplied');
        
        wrp = new ISSM_CoachingVisitsController.CoachingWrapper(Date.today(), 'bdr', 'bdrId','route','routeId',10,'achingApplied');
        wrp.compareTo(wrp2);
        
        List<ISSM_CoachingVisitsController.CoachingWrapper> coachWrp = ISSM_CoachingVisitsController.getCoaching('CUSTOM', System.today().year()+'-'+System.today().month()+'-'+System.today().day(), System.today().year()+'-'+System.today().month()+'-'+System.today().day(), userId);
        coachWrp = ISSM_CoachingVisitsController.getCoaching('THIS_WEEK', System.today().year()+'-'+System.today().month()+'-'+System.today().day(), System.today().year()+'-'+System.today().month()+'-'+System.today().day(), userId);
        test.stopTest();
        
    }
    
    public static void insertCustomSetting(){
        SyncHerokuParams__c objConf = new SyncHerokuParams__c();
        objConf.Name = 'SyncToursEvents';
        objConf.StartTime__c = 0;
        objConf.EndTime__c = 24;
        objConf.IsActive__c = true;
        objConf.LastModifyDate__c = DateTime.now().addDays(-30);
        objConf.RecordTypeIds__c = 'Presales';
        objConf.RunFrequency__c = 60;
        insert objConf;
        
        list<ISSM_PriceEngineConfigWS__c> lstConf = new list<ISSM_PriceEngineConfigWS__c>();
        ISSM_PriceEngineConfigWS__c objConf1 = new ISSM_PriceEngineConfigWS__c();
        objConf1.Name = 'DeleteHerokuTours';
        objConf1.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf1.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deletetours';
        objConf1.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf1.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf1);
        
        ISSM_PriceEngineConfigWS__c objConf2 = new ISSM_PriceEngineConfigWS__c();
        objConf2.Name = 'DeleteHerokuEvents';
        objConf2.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf2.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deleteevents';
        objConf2.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf2.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf2);
        
        ISSM_PriceEngineConfigWS__c objConf3 = new ISSM_PriceEngineConfigWS__c();
        objConf3.Name = 'GetHerokuTours';
        objConf3.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf3.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/gettours';
        objConf3.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf3.ISSM_Method__c = 'POST';
        lstConf.add(objConf3);
        
        ISSM_PriceEngineConfigWS__c objConf4 = new ISSM_PriceEngineConfigWS__c();
        objConf4.Name = 'GetHerokuEvents';
        objConf4.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf4.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/getevents';
        objConf4.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf4.ISSM_Method__c = 'POST';
        lstConf.add(objConf4);
        
        ISSM_PriceEngineConfigWS__c objConf5 = new ISSM_PriceEngineConfigWS__c();
        objConf5.Name = 'InsertHerokuTours';
        objConf5.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf5.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/inserttours';
        objConf5.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf5.ISSM_Method__c = 'POST';
        lstConf.add(objConf5);
        
        ISSM_PriceEngineConfigWS__c objConf6 = new ISSM_PriceEngineConfigWS__c();
        objConf6.Name = 'InsertHerokuEvents';
        objConf6.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf6.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/insertevents';
        objConf6.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf6.ISSM_Method__c = 'POST';
        lstConf.add(objConf6);
        
        ISSM_PriceEngineConfigWS__c objConf7 = new ISSM_PriceEngineConfigWS__c();
        objConf7.Name = 'UpdateHerokuTours';
        objConf7.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf7.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updatetours';
        objConf7.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf7.ISSM_Method__c = 'PUT';
        lstConf.add(objConf7);
        
        ISSM_PriceEngineConfigWS__c objConf8 = new ISSM_PriceEngineConfigWS__c();
        objConf8.Name = 'UpdateHerokuEvents';
        objConf8.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf8.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updateevents';
        objConf8.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8   ';
        objConf8.ISSM_Method__c = 'PUT';
        lstConf.add(objConf8);
        
        insert lstConf;
    }
    
    public static testMethod void testGetAccountCoachingEvents(){
        List<Account> accList = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'Account Test 1';
        accList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Account Test 2';
        accList.add(acc2);
        
        Insert accList;
        
        List<String> selectedAccIdList = new List<String>();
        for(Account a :accList){
            selectedAccIdList.add(String.valueOf(a.Id));
        }
        
        Date today = Date.today();
        String dDate = today.year() + '-' + today.month() + '-' + today.day();
        
        User manager1 = new User(
            ProfileId = UserInfo.getProfileId(),
            LastName = 'Test Manager',
            Email = 'testMng@test.com',
            Username = 'testMng@test.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'TestMng',
            Alias = 'TestMng',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert manager1;
        String RecordTypeCoachingEvt = Schema.getGlobalDescribe().get('Event').getDescribe().getRecordTypeInfosByDeveloperName().get('Coaching').getRecordTypeId();
        Event evt1 = new Event();
        evt1.OwnerId = manager1.Id;
        evt1.RecordTypeId = RecordTypeCoachingEvt;
        evt1.WhatId = acc1.Id;
        evt1.ActivityDate = Date.today();
        evt1.DurationInMinutes = 15;
        evt1.ActivityDateTime = DateTime.now();
        Insert evt1;
        
        Test.startTest();
        ISSM_CoachingVisitsController.getAccountCoachingEvents(dDate, manager1.Id, selectedAccIdList);
        Test.stopTest();
        
    }
    
}