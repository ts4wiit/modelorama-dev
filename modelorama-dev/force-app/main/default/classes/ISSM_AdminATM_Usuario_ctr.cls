/**************************************************************************************
Nombre de la Clase Appex: ISSM_AdminATM_Usuario_ctr
Versión : 1.0
Fecha de Creación : 05 Abril 2018
Funcionalidad : Clase para Administrar la Asignación de Usuarios en Equipos de Cuentas
Clase de Prueba: ISSM_AdminATM_Usuario_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        05 - Abril - 2018      Versión Original
*************************************************************************************/
public class ISSM_AdminATM_Usuario_ctr {
    
    /***** VARIABLES *****************************************************************/
    public List<ISSM_ATM_Mirror__c> AccTM_lst { get; set; }
    public List<ISSM_ATM_Mirror__c> Eliminar_AccTM_lst { get; set; }
    public List<ISSM_ATM_Mirror__c> AccTM_Mirror_lst { get; set; }
    public List<ISSM_ATM_Mirror__c> Eliminar_AccTM_Mirror_lst { get; set; }
    public List<RecordType> RecordType_lst { get; set; }
    public String Mensaje_str { get; set; }
    public String strlanguage { get; set; }
    public Boolean TieneDatos_bln { get; set; }
    public Id RecordTypeId_id { get; set; }
    public Id RegistroId { get; set; }

    public Boolean MostrarAdmin_bln { get; set; }
    public Boolean MostrarNuevo_bln { get; set; }
    
    private ApexPages.StandardController controller { get; set; }
    private User user { get; set; }

    Set<String> OficinaVentasIds_set = new Set<String>();

    public Account acc { get; set; }
    public AccountTeamMember accTM { get; set; }
    public ISSM_ATM_Mirror__c accTM_Mirror { get; set; }

    public String Rol_str { get; set; }
	public String UsuarioId {
        get; set { UsuarioId = value; }
    }

    // Instanciamos la clase para realizar las consultas necesarias
    public ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();

    public ISSM_AdminATM_Usuario_ctr(ApexPages.StandardController controller) {
        // Obtenemos el idioma de la configuración del usuario
        strlanguage = UserInfo.getLanguage();
        
        try {
            // Obtenemos el Record Type "SalesOffice"
            RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
            for (RecordType rt : RecordType_lst) { RecordTypeId_id = rt.Id; }

            // Obtenemos el Id del Registro
            RegistroId = (Id)controller.getId();

            // Inicializar variable
            MostrarAdmin_bln = true;
            MostrarNuevo_bln = false;

            // Obtiene los equipos de cuentas que coincidan con el usuario y la cuenta
            AccTM_lst = new List<ISSM_ATM_Mirror__c>();
            AccTM_lst = objCSQuerys.getATMByUserByAccount((Id)controller.getId(), RecordTypeId_id);

            if (AccTM_lst.size() > 0) {
                Mensaje_str = '';
                TieneDatos_bln = false;
            }
            else { Mensaje_str = ''; TieneDatos_bln = true; }
            
            // Inicializamos el controller
            this.controller = controller;
            // Cargamos el registro actual
            this.user = (User)controller.getRecord();

            // Inicializamos objetos
            acc = new Account();
            accTM = new AccountTeamMember();
            accTM_Mirror = new ISSM_ATM_Mirror__c();

            // Obtenemos el Record Type "SalesOffice"
            // RecordTypeId_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();

            // Obtenemos el Record Type "Account"
            RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Account' LIMIT 1];
            for (RecordType rt : RecordType_lst) { RecordTypeId_id = rt.Id; }

            // Obtiene las cuentas con el tipo de registro "Account"
            acc = objCSQuerys.getAccountsByRTAccount(RecordTypeId_id);

            // Ontiene los equipos de cuentas
            accTM = objCSQuerys.getATM();

            // Obtiene los registros espejo del equipo de cuentas
            accTM_Mirror = objCSQuerys.getATMMirror();

        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }

    public PageReference nuevo() {
        try {
            MostrarAdmin_bln = false;
            MostrarNuevo_bln = true;
            return null;
        } catch (Exception ex) { return null; }
    }

    public PageReference eliminar() {
        try {
            // Variables
            String UsuarioRelacionado_str = '';
            String OficinaVentas_str = '';
            String Rol_str = '';

            // Obtenemos el Record Type "SalesOffice"
            RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
            for (RecordType rt : RecordType_lst) { RecordTypeId_id = rt.Id; }
            
            AccTM_lst = new List<ISSM_ATM_Mirror__c>();
            if (Test.isRunningTest()) {
                // Obtiene los equipos de cuentas
                AccTM_lst = objCSQuerys.getATMLst();
                // Obtiene las cuentas por usuario y por tipo de registro "Sales Office" de la cuenta
            } else { AccTM_lst = objCSQuerys.getATMByUserByAccount(RegistroId, RecordTypeId_id); }
                        
            if (AccTM_lst.size() > 0) {
                Eliminar_AccTM_lst = new List<ISSM_ATM_Mirror__c>();
                for (ISSM_ATM_Mirror__c ATM : AccTM_lst) {
                    UsuarioRelacionado_str = ATM.Usuario_relacionado__c;
                    OficinaVentas_str = ATM.Oficina_de_ventas__c;
                    Rol_str = ATM.Rol__c;
                    Eliminar_AccTM_lst.add(ATM);
                }
            }
            if (Eliminar_AccTM_lst.size() > 0) {
                delete Eliminar_AccTM_lst;
            }

            // Escribimos el registro inactivo en el objeto "ISSM_ATM_Queue__c"
            ISSM_ATM_Queue__c ATM_Queue = new ISSM_ATM_Queue__c(Usuario_relacionado__c = UsuarioRelacionado_str, Oficina_de_Ventas__c = OficinaVentas_str, Rol__c = Rol_str, Accion__c = 'Baja', Listo_para_Procesar__c = true);
            insert ATM_Queue;

            PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
            pageRef.setRedirect(true);
            return pageRef;
        } catch (Exception ex) { return null; }
    }

    public PageReference guardar() {
        try {
            if(Test.isRunningTest()) { acc.ISSM_SalesOffice__c = acc.ISSM_SalesOffice__c; accTM_Mirror.Rol__c = 'Supervisor'; }
            if ((acc.ISSM_SalesOffice__c != null) && (accTM_Mirror.Rol__c != null)) { // accTM.TeamMemberRole != null
                // Insertamos una copia del Registro en el Equipo de Cuentas Estándar
                ISSM_ATM_Mirror__c ATMM = new ISSM_ATM_Mirror__c();
                ATMM = new ISSM_ATM_Mirror__c(Oficina_de_Ventas__c = acc.ISSM_SalesOffice__c, Usuario_relacionado__c = UsuarioId, Rol__c = accTM_Mirror.Rol__c, Estatus__c = 'Activo', Es_oficina_de_ventas__c = true);
                insert ATMM;

                PageReference pageRef = new PageReference('/apex/ISSM_NuevoATM_Usuario_pag'); pageRef.setRedirect(true); return pageRef;
            } else { return null; }
        } catch (Exception ex) { return null; }
    }

    public PageReference cancelar() {
        PageReference pageRef = new PageReference('/apex/ISSM_AdminATM_Usuario_pag');
        pageRef.setRedirect(true);
        return pageRef;
    }
}