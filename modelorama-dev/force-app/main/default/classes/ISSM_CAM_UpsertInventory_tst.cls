@isTest
private class ISSM_CAM_UpsertInventory_tst {
	
	@isTest static void test_method_one() {

        Account acc = new Account(
        	Name = 'test',
        	ontap__sapcustomerid__c = '3122'
        );
        insert acc;

        ISSM_Asset__c assets = new ISSM_Asset__c();
        	assets.ISSM_Sales_Organization__c = acc.id;
			assets.ISSM_Centre__c ='001';
			assets.ISSM_Model_Name__c = 'VN29R';
			assets.Type__c = 'VERTICAL BAJO CERO';
			assets.ISSM_Asset_Description__c = 'METALFRIO VN29R';
			assets.ISSM_Material_number__c = '8000551';
			assets.ISSM_Status_SFDC__c = 'Free Use';
        	assets.ISSM_CutOverReason__c = 'Incorrect Asset Number';
        insert assets;

        ISSM_Asset_Inventory__c assetInventory = new ISSM_Asset_Inventory__c(
            Name = 'Test',
            ISSM_Inventario_External_Id__c = '12345-12345-12345',
            ISSM_UEN__c = acc.id,
            ISSM_Centre__c = '001',
            ISSM_Material_Name__c = 'VERTICAL BAJO CERO',
            ISSM_Material_Description__c = 'METALFRIO VN29R',
            ISSM_Material_Number__c = '8000551',
            ISSM_Stock_SAP__c = 12345,
            ISSM_Stock_SFDC__c = 12345
        );
        insert assetInventory;


		Test.startTest();
			ISSM_CAM_UpsertInventory_sch scheduleInstance = new ISSM_CAM_UpsertInventory_sch();
			String strChronExpresion = '0 0 0 15 * ? ';
			System.schedule('AssignmentNewOwnerTest',
				strChronExpresion,
				scheduleInstance);
		Test.stopTest();
	}
	
}