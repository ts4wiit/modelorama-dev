public class ISSM_BDRTaskAssignment_cls {
    private final static String AccountQuery_str = 'SELECT Id, ONTAP__SAPCustomerId__c,Name,Owner.Name FROM Account WHERE OwnerId = \'';
    private final static String UserQuery_str = 'SELECT Id, Name, UserRole.Name, ONTAP__SAPUserId__c FROM User WHERE IsActive = true AND UserRole.Name like ';
    
    public String searchOption_str {get;set;}
    public String searchString_str {get; set;}
    public String searchAccString_str {get; set;}
    public String message_str{get;set;}
    public Boolean selectAll {get;set;}
    public Task taskToFill_obj {get;set;}
    public Integer step_int {get;set;}
    public Date activityDate_dat {get;set;}
    
    public List<AccountDecorator> acctDecorator_lst {get;set;}
    public List<UserDecorator> usrDecorator_lst {get;set;}
    
    public List<SelectOption> taskSubject {get;set;}
    public List<SelectOption> taskStatus {get;set;}
    public List<SelectOption> taskPriority {get;set;}
    
    /* Wrapper class with boolean for checks*/
    public class AccountDecorator{
        public Account account_obj {get;set;}
        public Boolean selected_bln {get;set;}
    }
    public class UserDecorator{
        public User user_obj {get;set;}
        public Boolean selected_bln {get;set;}
    }
    
    /** CONSTRUCTOR */
    public ISSM_BDRTaskAssignment_cls(){
        init();
        initLookups();
    }
    
    /** INITIALIZERS */
    //init all variables
    private void init(){
        message_str = '';
        taskToFill_obj = new Task();
        searchOption_str = 'DRV';
        searchString_str = '';
        step_int = 0;
        selectAll = false;
        usrDecorator_lst = new List<UserDecorator>();
        acctDecorator_lst = new List<AccountDecorator>();
    }
    //init lookups
    private void initLookups(){
        // Initialize select options
        taskSubject = new List<SelectOption>();
        List<Schema.PicklistEntry> pick_list_values = Schema.getGlobalDescribe().get('Task').newSObject().getSObjectType().getDescribe().fields.getMap().get('Subject').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
        	taskSubject.add(new SelectOPtion(a.getLabel(), a.getLabel()));
        }
        taskStatus = new List<SelectOption>();
        pick_list_values = Schema.getGlobalDescribe().get('Task').newSObject().getSObjectType().getDescribe().fields.getMap().get('Status').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
        	taskStatus.add(new SelectOPtion(a.getValue(), a.getLabel()));
        }
        taskPriority = new List<SelectOption>();
        pick_list_values = Schema.getGlobalDescribe().get('Task').newSObject().getSObjectType().getDescribe().fields.getMap().get('Priority').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
        	taskPriority.add(new SelectOPtion(a.getValue(), a.getLabel()));
        }
    }
    /** METHOD TO CHANGE BETWEEN SCREENS **/
    public void nextStep(){
        Boolean creationFlag = false;
        system.debug(':::::  '+step_int);
        if(step_int == 0){
            if(!getCheckIfAnySelected())
            lookupCriteria();
            if(getCheckIfAnySelected())
                step_int = 1;
        }else if(step_int == 1){
            creationFlag = taskToFill_obj.Description != '';
        }
        if(creationFlag){
            if(searchOption_str == 'DRV'){
                System.debug(':::'+getQuery()+' , '+ createTaskJSON());
                createTasks(getQuery(),createTaskJSON());
            }else if(searchOption_str == 'BDR'){
                createTasks(getQuery(),createTaskJSON());
            }else if(searchOption_str == 'PDV'){
                createTasks(getQuery(),createTaskJSON());
            }
            
            init();
            message_str = System.label.ISSM_MassiveAssignmentMessage;
        }else{
            taskToFill_obj.addError('Descripción requerida');
        }
    }
    /** METHOD TO LOOKUP  THE ACCOUNT USING THE CRITERIA */
    public void lookupCriteria(){
        String completeQuery;
        if(searchOption_str == 'DRV'){
            usrDecorator_lst = new List<UserDecorator>();
            completeQuery = UserQuery_str + '\'Fuerza de Ventas ' + searchString_str + '\'';
            System.debug(':::: '+completeQuery);
            for(User usr_obj : Database.query(completeQuery)){
                UserDecorator dec_obj = new UserDecorator();
                dec_obj.user_obj = usr_obj;
                dec_obj.selected_bln = false;
                usrDecorator_lst.add(dec_obj);
            }
        }else if(searchOption_str == 'BDR'){
            acctDecorator_lst = new List<AccountDecorator>();
            completeQuery = AccountQuery_str + searchString_str + '\' LIMIT 1000';
            System.debug(':::: '+completeQuery);
            for(Account acc_obj : Database.query(completeQuery)){
                AccountDecorator acct_obj = new AccountDecorator();
                acct_obj.account_obj = acc_obj;
                acct_obj.selected_bln = false;
                acctDecorator_lst.add(acct_obj);
            }
        }else if(searchOption_str == 'PDV'){
            acctDecorator_lst = new List<AccountDecorator>();
            if(searchAccString_str.length() >= 3){
                completeQuery = 'SELECT Id,Name,ONTAP__SAPCustomerId__c, Owner.Name FROM Account WHERE ((Name like \'%' + searchAccString_str + '%\' OR ONTAP__SAPCustomerId__c like \'%'+ searchAccString_str +'%\') AND RecordType.DeveloperName = \'Account\') LIMIT 1000';
                for(Account acc_obj : Database.query(completeQuery)){
                    AccountDecorator acct_obj = new AccountDecorator();
                    acct_obj.account_obj = acc_obj;
                    acct_obj.selected_bln = false;
                    acctDecorator_lst.add(acct_obj);
                }
            }
        } 
    }
    
    /** Reset variables to display on the page, returns to step 0*/
    public void reset(){
        usrDecorator_lst = new List<UserDecorator>();
        acctDecorator_lst = new List<AccountDecorator>();
        searchString_str = '';
        searchOption_str = 'DRV';
        selectAll = false;
        selectAll();
        step_int = 0;
    }
    
    /* Method that returns whether any check is on*/
    public Boolean getCheckIfAnySelected(){
        Boolean result_bln = false;
        if(!usrDecorator_lst.isEmpty()){
            for(UserDecorator usDc : usrDecorator_lst){
                result_bln |= usDc.selected_bln;
            }
        }else if(!acctDecorator_lst.isEmpty()){
            for(AccountDecorator usDc : acctDecorator_lst){
                result_bln |= usDc.selected_bln;
            }
        }
        return result_bln;
    }
    
    /* Selects all the elements from the list */
    public void selectAll(){
        for(UserDecorator usDc : usrDecorator_lst){
            usDc.selected_bln = selectAll;
        }
        for(AccountDecorator accDc : acctDecorator_lst){
            accDc.selected_bln = selectAll;
        }
    }
    /* CREATE TASK JSON **/
    private String createTaskJSON(){
        taskToFill_obj.ActivityDate = activityDate_dat;
        taskToFill_obj.RecordTypeId = [select Id from RecordType where developerName='Standard_Task' limit 1].Id;
        return JSON.serialize(taskToFill_obj, true);
    }
    
    /* build query string to lookup accounts */
    private String getQuery(){
        String usrIdsList_str = '';
        if(searchOption_str == 'DRV'){
            for(UserDecorator usrDc_obj : usrDecorator_lst){
                if(usrDc_obj.selected_bln){
                    usrIdsList_str += '\''+usrDc_obj.user_obj.Id+'\',';
                }
            }
            usrIdsList_str = ('SELECT Id, OwnerId FROM Account WHERE RecordType.DeveloperName = \'Account\' AND OwnerId IN ('+usrIdsList_str.substring(0,usrIdsList_str.length()-1)+')');
        }else if(searchOption_str == 'BDR' || searchOption_str == 'PDV'){
            for(AccountDecorator accDc_obj : acctDecorator_lst){
                if(accDc_obj.selected_bln){
                    usrIdsList_str += '\''+accDc_obj.account_obj.Id+'\',';
                }
            }
            usrIdsList_str = ('SELECT Id, OwnerId FROM Account WHERE Id IN ('+usrIdsList_str.substring(0,usrIdsList_str.length()-1)+')');
        }
        return usrIdsList_str;
        
    }
    
    /** CREATE TASKS TO USERS **/
    private static void createTasks(String q, String JSONTask){
        Id batchInstanceId = Database.executeBatch(new ISSM_TaskAssignBatch_bch(q, JSONTask), 25);
    }
    
}