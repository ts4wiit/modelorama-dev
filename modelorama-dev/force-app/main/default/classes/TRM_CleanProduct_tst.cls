/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for class TRM_CleanProduct_ctr
*
*  No.           Date              Author                      Description
* 1.0    	15-Octubre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_CleanProduct_tst {
	
	@testSetup static void loadData() {

	//Insert catalogos MDM_Parameter__c
		MDM_Parameter__c parameter1 = createMDMParameter('Cerveza',true,true,'SectorProd','01','Cerveza','SectorProd-01');
		insert parameter1;
		MDM_Parameter__c parameter2 = createMDMParameter('Media',true,true,'GrupoMateriales1','02','Media','GrupoMateriales1-02');
		insert parameter2;
		MDM_Parameter__c parameter3 = createMDMParameter('24/355 ml.',true,true,'GrupoMateriales2','42','24/355 ml.','GrupoMateriales2-42');
		insert parameter3;

	//Insert Product
		String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);

		ONTAP__Product__c product1 				= new ONTAP__Product__c();
			product1.ONTAP__ExternalKey__c 		= '000000000003000006';
			product1.ONTAP__MaterialProduct__c 	= 'CORONA EXTRA CLARA 24/355 ML CT R';
			product1.ISSM_SectorCode__c 		= parameter1.id;
			product1.ISSM_Quota__c 				= parameter2.id;
			product1.ISSM_MaterialGroup2__c 	= parameter3.id;
			product1.RecordTypeId 				= RecType;
			product1.ONTAP__ProductType__c 		= 'FERT';
		insert product1;
	}
	@isTest static void testSearchProducts() {
		Test.startTest();
			TRM_CleanProduct_ctr cleanProduct = new TRM_CleanProduct_ctr();
			String[] lstIds = new List<String>();
			Map<String,Decimal> mapProd = new Map<String,Decimal>();
			for(ONTAP__Product__c product : [SELECT Id FROM ONTAP__Product__c]){
				lstIds.add('\''+product.Id+'\'');
				mapProd.put(product.Id, 219);
			}
			TRM_CleanProduct_ctr.WrapperProduct[] LstProducts = TRM_CleanProduct_ctr.SearchProducts(lstIds,mapProd);

			System.assertEquals(true,LstProducts.size() > 0 ? true : false);
		Test.stopTest();
	}
	public static MDM_Parameter__c createMDMParameter(String name
													, boolean active
													, boolean activeRevenue
													, String catalog
													, String code
													, String description
													, String externalId){
		MDM_Parameter__c parameter 		= new MDM_Parameter__c();
			parameter.Name				= name;
			parameter.Active__c			= active;
			parameter.Active_Revenue__c = activeRevenue; 
			parameter.Catalog__c 		= catalog;
			parameter.Code__c 			= code;
			parameter.Description__c 	= description;
			parameter.ExternalId__c 	= externalId;
	return parameter;
	}
	
}