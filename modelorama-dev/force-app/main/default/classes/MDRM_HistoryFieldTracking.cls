public with sharing class MDRM_HistoryFieldTracking {
	private Map<String, MDRM_HistoryFieldTracking__mdt> lfieldmdt {get;set;}
	private Map<String, Map<String, String>> mpPickvalues {get;set;}
	private String FIELD_TYPE_LIST = 'List';
	private String OBJECT_ACCOUNT = 'Account';
	
	public MDRM_HistoryFieldTracking(String objType) {
		lfieldmdt = this.getFieldTrackingList(objType);
		mpPickvalues = this.getPicklistValues(objType);
	}

	public List<MDRM_Modelorama_Status_Change__c> generateFieldTracking(SObject newSobj, SObject oldSobj) {
		Set<String> lfieldgdsc;
		List<MDRM_Modelorama_Status_Change__c> lstsChange = new List<MDRM_Modelorama_Status_Change__c>();
		Schema.SObjectType sobjType = newSobj.getSObjectType();
		String objType = String.valueOf(sobjType);
		
		if(!lfieldmdt.isEmpty()) {
			System.debug('We are looking for fields to tracking');

			for(String fI : lfieldmdt.keySet()) {
				MDRM_Modelorama_Status_Change__c stsChange = null;
				try {
					if(oldSobj != null) { 
						if(newSobj.get(fI) != oldSobj.get(fI)) {
							stsChange = new MDRM_Modelorama_Status_Change__c(
								MDRM_PriorValue__c = String.valueOf(oldSobj.get(fI)),
								MDRM_PriorValueLabel__c = String.valueOf(oldSobj.get(fI)),
								MDRM_NewValue__c = String.valueOf(newSobj.get(fI)),
								MDRM_NewValueLabel__c = String.valueOf(newSobj.get(fI)),
								MDRM_FieldName__c = lfieldmdt.get(fI).MasterLabel,
								MDRM_ObjectAPI__c = objType,
								MDRM_ObjectName__c = sobjType.getDescribe().label,
								MDRM_RelatedTo__c = newSobj.Id
							);
							if(mpPickvalues.containsKey(fI)) {
								Map<String, String> mpLabels = mpPickvalues.get(fI);
								
								if(mpLabels.containsKey(stsChange.MDRM_PriorValue__c)) {
									stsChange.MDRM_PriorValueLabel__c = mpLabels.get(stsChange.MDRM_PriorValue__c);
								}
								if(mpLabels.containsKey(stsChange.MDRM_NewValue__c)) {
									stsChange.MDRM_NewValueLabel__c = mpLabels.get(stsChange.MDRM_NewValue__c);
								}
							} else {

							}
						}
					} else if(newSobj.get(fI) != null && newSobj.get(fI) != '') {
						stsChange = new MDRM_Modelorama_Status_Change__c(
							MDRM_NewValue__c = String.valueOf(newSobj.get(fI)),
							MDRM_NewValueLabel__c = String.valueOf(newSobj.get(fI)),
							MDRM_FieldName__c = lfieldmdt.get(fI).MasterLabel,
							MDRM_ObjectAPI__c = objType,
							MDRM_ObjectName__c = sobjType.getDescribe().label,
							MDRM_RelatedTo__c = newSobj.Id
						);
						if(mpPickvalues.containsKey(fI)) {
							Map<String, String> mpLabels = mpPickvalues.get(fI);
							
							if(mpLabels.containsKey(stsChange.MDRM_NewValue__c)) {
								stsChange.MDRM_NewValueLabel__c = mpLabels.get(stsChange.MDRM_NewValue__c);
							}
						}
					}

					if(objType == OBJECT_ACCOUNT) {
						stsChange.MDRM_Account_Status_Change__c = newSobj.Id;
					}
					
					lstsChange.add(stsChange);
				} catch(Exception e) {
					System.debug('Field name ' + fI + ' does not exist. ' + e);
				}
			}
		}

		return lstsChange;
	}

	/**
	 * Description. Get fields stored in Metadata for the tracking
	 */
	private Map<String, MDRM_HistoryFieldTracking__mdt> getFieldTrackingList(String objType) {
		Map<String, MDRM_HistoryFieldTracking__mdt> lfieldmdt = new Map<String, MDRM_HistoryFieldTracking__mdt>();

		for(MDRM_HistoryFieldTracking__mdt fieldI : [SELECT MDRM_FieldName__c, MDRM_ObjectName__c, MasterLabel, MDRM_FieldType__c
													 FROM MDRM_HistoryFieldTracking__mdt 
													 WHERE MDRM_ObjectName__c =: objType
													 AND MDRM_IsActive__c = true]) {
			lfieldmdt.put(fieldI.MDRM_FieldName__c, fieldI);
		}
		System.debug('Fields found to tracking: ' +  lfieldmdt.size());

		return lfieldmdt;
	}

	/**
	 * Description. Get labels and values for fields marked to store history
	 */
	private Map<String, Map<String, String>> getPicklistValues(String objType) {
		Map<String, Map<String, String>> mpPicklabels = new Map<String, Map<String, String>>();
		DescribeSObjectResult describeResult = Schema.getGlobalDescribe().get(objType).getDescribe();
		Map<String, Schema.SObjectField> desribedFields = describeResult.fields.getMap();
		List<Schema.PicklistEntry> picklistValues = null;
		Schema.SObjectField field = null;

		for(String fieldI : lfieldmdt.keySet()) {
			if(lfieldmdt.get(fieldI).MDRM_FieldType__c == FIELD_TYPE_LIST) {
				field = desribedFields.get(fieldI);
				picklistValues = field.getDescribe().getPickListValues();

				Map<String, String> mpLabels = new Map<String, String>();

				for(Schema.PicklistEntry pe : picklistValues) {
					System.debug('Value: ' + pe.getValue() + ' Label: ' + pe.getLabel());
					mpLabels.put(pe.getValue(), pe.getLabel());
				}
				mpPicklabels.put(fieldI, mpLabels);
			}
		}

		return mpPicklabels;
	}

}