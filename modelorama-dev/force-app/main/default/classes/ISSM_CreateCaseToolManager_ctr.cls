/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: clase que se ejecuta mediante un flow
---------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    10-Julio-2017    Hector Diaz (HD)     Creador.
1.1    26-Abril-2018    Leopoldo Ortega      Se ajustó la asignación del caso de los roles "Trade Marketing" y "Billing Manager".
***********************************************************************************/
public with sharing class  ISSM_CreateCaseToolManager_ctr {
    public ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    public ISSM_TypificationMatrix_cls typificationMatrix {get;set;}
        
    public ISSM_CreateCaseToolManager_ctr(){
        typificationMatrix = new ISSM_TypificationMatrix_cls();
    }  
    
    /**
    * Metodo que realiza una valdiacion para saber que registro se debe crear si "Actualizacion Equipo de cuenta" o "Actualizar Gestor" 
    * @param  :
        1.-Recibimos el objeto que nos manda un flow si es que cumple con ciertos criterios al ser creado el caso 
    * @return none  
    **/
    @InvocableMethod
    public static void CaseCreate(List<Case> lstobjCase) {
        try {
            System.debug('######ISSM_CreateCaseToolManager_ctr -- CaseCreate ');
            ISSM_CreateCaseToolManager_ctr objCreateCaseToolManager = new ISSM_CreateCaseToolManager_ctr();
            Boolean blnCreateUpdateAccoutTeam = false;
            Boolean blnCreateWithoutManager  = false;
            String strIdAccount  = '';
            ISSM_TypificationMatrix__c tpMatrix = null;
            for(Case ObjCase :lstobjCase ){
                strIdAccount = ObjCase.AccountId;
                if(ObjCase.ISSM_UpdateAccountTeam__c == true && ObjCase.IsEscalated == false){
                    blnCreateUpdateAccoutTeam = true;       
                    tpMatrix =  new ISSM_TypificationMatrix_cls().getTypificationMatrix(ObjCase.ISSM_TypificationNumber__c);
                }
                if(ObjCase.ISSM_UserWithoutManager__c == true && ObjCase.IsEscalated == true){
                    blnCreateWithoutManager = true;
                }            
            } 
            if(blnCreateUpdateAccoutTeam){//para crear caso con tipificacion "Actualizacion Equipo de cuenta" 
                if(tpMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.MODELORAMA){
                    objCreateCaseToolManager.CreateRowCaseRelated(strIdAccount,System.label.ISSM_SubjectUpdateAccountTeamModelorama,System.label.ISSM_DescriptionUpdateAccountTeam,System.label.ISSM_Level1_UpdateAccountTeam,System.label.ISSM_Level2_UpdateAccountTeam,System.label.ISSM_Level3_UpdateAccountTeam);
                } else if(tpMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.REPARTO){
                    objCreateCaseToolManager.CreateRowCaseRelated(strIdAccount,System.label.ISSM_SubjectUpdateAccountTeamDelivery,System.label.ISSM_DescriptionUpdateAccountTeam,System.label.ISSM_Level1_UpdateAccountTeam,System.label.ISSM_Level2_UpdateAccountTeam,System.label.ISSM_Level3_UpdateAccountTeam);
                } else {
                    objCreateCaseToolManager.CreateRowCaseRelated(strIdAccount,System.label.ISSM_SubjectUpdateAccountTeam,System.label.ISSM_DescriptionUpdateAccountTeam,System.label.ISSM_Level1_UpdateAccountTeam,System.label.ISSM_Level2_UpdateAccountTeam,System.label.ISSM_Level3_UpdateAccountTeam);
                }
            }else if(blnCreateWithoutManager){//para crear caso con tipificacion "Actualizacion Gestor" 
                objCreateCaseToolManager.CreateRowCaseRelated(strIdAccount,System.label.ISSM_SubjectUpdateManager,System.label.ISSM_DescriptionUpdateManager,System.label.ISSM_Level1_UpdateManager,System.label.ISSM_Level2_UpdateManager,System.label.ISSM_Level3_UpdateManager);
            }
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }      
      
     /** 
    * Metodo que crea un registro con una tipififcacion especifica
    * @param :
        1.-IdCuenta
        2.-asunto para el caso              (Etiqueta personalizada)
        3.-Descripcion para el caso         (Etiqueta personalizada)
        4.-Matriz de tipificacion level1    (Etiqueta personalizada)
        5.-Matriz de tipificacion level2    (Etiqueta personalizada)
        6.-Matriz de tipificacion level3    (Etiqueta personalizada)
    * @return none  
    **/
    public void CreateRowCaseRelated(String stAccountId,String strSubjectCase,String strDesciptionCase,String strOptionSelectedLevel1,String strOptionSelectedLevel2,String strOptionSelectedLevel3){
        System.debug('######ISSM_CreateCaseToolManager_ctr -- CreateRowCaseRelated '); 
        List<ISSM_TypificationMatrix__c> lstTypificationMatrix = typificationMatrix.getTypificationMatrix( strOptionSelectedLevel1,strOptionSelectedLevel2, strOptionSelectedLevel3, null, null, null);  
        ISSM_TypificationMatrix__c typificationMatrixResult = lstTypificationMatrix[0];
        String RecordTypeDevName = typificationMatrixResult.ISSM_CaseRecordType__c;
        id ownerId = this.getOwner(typificationMatrixResult, stAccountId);
        if(String.isEmpty(ownerId)){
            List<ISSM_AppSetting_cs__c> lstIdQueueWithoutOwner = objCSQuerys.QueryAppSettings();
            ownerId =  lstIdQueueWithoutOwner[0].ISSM_IdQueueWithoutOwner__c;
        }
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE DeveloperName  =: RecordTypeDevName LIMIT 1];  
        Case ObjCase  = new Case(     
            ISSM_TypificationLevel1__c  = strOptionSelectedLevel1,
            ISSM_TypificationLevel2__c  = strOptionSelectedLevel2,
            ISSM_TypificationLevel3__c  = strOptionSelectedLevel3,
            Subject                     = strSubjectCase,
            Description                 = strDesciptionCase,
            RecordTypeID                = rt.Id, 
            ISSM_TypificationNumber__c  = typificationMatrixResult.Id,
            EntitlementId               = typificationMatrixResult.ISSM_Entitlement__c,
            OwnerId                     = ownerId,
            AccountID = stAccountId          
        );      
        insert ObjCase; 
    }  
    /**
    * Metodo que trae el propietario para asignarle el caso 
    * @param :
        1.-objeto typicationMatrix
        2.-Id cuenta
    * @return id del propietario ya sea (USER,QUEUE,SUPERVISOR,BILLINGMANAGER,TRADEMARKETING) 
    **/ 
    public Id getOwner(ISSM_TypificationMatrix__c typicationMatrix, Id accountId){
        System.debug('######ISSM_CreateCaseToolManager_ctr -- getOwner ');
        List<Id> ids = null;
        Id idReturn = null;
        Account salesOffice = null;
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
        Account accountData = accountCls.getAccount(accountId); 
        if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.USER){
            idReturn = typicationMatrix.ISSM_OwnerUser__c;
        } else if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.QUEUE){
            idReturn = typicationMatrix.ISSM_IdQueue__c;
        } else if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.SUPERVISOR
            || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA
            || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO) {
            if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
                || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
                ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.ISSM_SalesOffice__r.Id);
            } else {
                ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id);
            }
            if(ids != null && ids.size() > 0) { idReturn = ids.get(0); }
        } else {
            salesOffice = accountCls.getAccount(accountData.ISSM_SalesOffice__c);
            if(salesOffice != null){
                if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BILLINGMANAGER) {
                    // idReturn = salesOffice.ISSM_BillingManager__c
                    ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id);
                        if(ids != null && ids.size() > 0) { idReturn = ids.get(0); }
                } else if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.TRADEMARKETING) {
                    // idReturn = salesOffice.ISSM_TradeMarketing__c;
                    ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id);
                        if(ids != null && ids.size() > 0) { idReturn = ids.get(0); }
                }
            } else { idReturn =  null; }   
        }
        return idReturn;
    } 
}