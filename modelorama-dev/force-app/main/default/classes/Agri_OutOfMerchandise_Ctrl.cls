public class Agri_OutOfMerchandise_Ctrl {
    
    public Delivery_order__c orden {get;set;}
    public String almacenId {get;set;}
    
    public Agri_OutOfMerchandise_Ctrl(ApexPages.StandardController controller) {
        orden = new Delivery_order__c();
        orden = (Delivery_order__c)controller.getRecord(); 
                
        almacenId = ApexPages.currentPage().getparameters().get('almacenId');
        if(almacenId != '' && almacenId != null) { orden.Agri_rb_almacen__c = almacenId; }
    }    
    
    public PageReference guardar() {                
        if(orden != null) {		
            try {
                ID rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Agri_tr_deliveryMerchandise' AND SObjectType = 'Delivery_order__c'].Id;
                orden.RecordTypeId = rtId;
                
                List<Agri_Warehouse__c> lwh = [SELECT Id, Agri_tx_centre__c FROM Agri_Warehouse__c WHERE ID =: almacenId];
                if(lwh[0].Agri_tx_centre__c != '') { orden.Agri_ls_centre__c = lwh[0].Agri_tx_centre__c; }
                
                insert orden;
            } catch(Exception e) {
            	ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Ha ocurrido un error: ' + e.getMessage())); 
                return null;
            }
        }
        
        //PageReference myVFPage = null;
        //myVFPage = new PageReference('/' + orden.Agri_rb_almacen__c);
        //return myVFPage;
        return new PageReference('/' + orden.Agri_rb_almacen__c);
    }
    
    public PageReference regresar() {
        PageReference myVFPage = null;
        
        if(orden != null) {
            myVFPage = new PageReference('/' + orden.Agri_rb_almacen__c);
        } else {
            myVFPage = new PageReference('/home/home.jsp');
        }        
        return myVFPage;
    }

}