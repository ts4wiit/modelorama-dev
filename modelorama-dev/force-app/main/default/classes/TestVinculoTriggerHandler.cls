@isTest
public class TestVinculoTriggerHandler 
{
 @isTest static void TestDeleteVinculos()
 {
  Account Businessman = VinculoDataFactory.createBusinessman();
  Account Modeloramas = VinculoDataFactory.createModelorama();
  MDRM_Vinculo__c vinculo = VinculoDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
  Test.startTest();
  Database.DeleteResult result = Database.delete(vinculo,false);
  Test.stopTest();
  System.assert(result.isSuccess());
 }
}