/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Batch use to realize orders clenasing. All accounts must have only 8 orders

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_OrderErase_bch implements Database.Batchable<sObject> {
	
	String query;
	global Set<String> accountsIds;

	/**
	 * Class constructor		                             
	 * @param  Set<String> accountsIdsParam
	 */
	global ISSM_OrderErase_bch(Set<String> accountsIdsParam) {
		accountsIds = accountsIdsParam;
		system.debug( '\n\n  ****** accountsIds = ' + accountsIds +'\n\n' );
	}

	/**
	 * Start method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
	global Iterable<sObject> start(Database.BatchableContext BC) {
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		system.debug( '\n\n  ****** accountsIds = ' + accountsIds+'\n\n' );
		system.debug( '\n\n  ****** devRecordTypeId = ' + devRecordTypeId+'\n\n' );
		return [SELECT Id, ONTAP__BeginDate__c, ONTAP__OrderAccount__c FROM ONTAP__Order__c WHERE ONTAP__OrderAccount__c IN : accountsIds and RecordTypeId =: devRecordTypeId];
	}

	/**
	 * Execute method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		system.debug( '\n\n  ****** scope.size() = ' + scope.size()+'\n\n' );
   		Map<Id,List<ISSM_OrderWrapper_cls>> accountOrder_map = new Map<Id,List<ISSM_OrderWrapper_cls>>();
   		for(sObject acctOrder:scope){
	   		if(!accountOrder_map.containsKey(((ONTAP__Order__c)acctOrder).ONTAP__OrderAccount__c)){
				accountOrder_map.put(((ONTAP__Order__c)acctOrder).ONTAP__OrderAccount__c, 
					new List<ISSM_OrderWrapper_cls>{
							new ISSM_OrderWrapper_cls((ONTAP__Order__c)acctOrder)
						});
			}else{
				accountOrder_map.get(((ONTAP__Order__c)acctOrder).ONTAP__OrderAccount__c).add(new ISSM_OrderWrapper_cls((ONTAP__Order__c)acctOrder));
			}
		}
		List<ONTAP__Order__c> ordersDelete = processAccountMap(accountOrder_map);
		if(ordersDelete != null){
			delete ordersDelete;
		}
	
	}

	/**
	 * Method to execute the order cleansing
	 * @param  Map<Id,List<ISSM_OrderWrapper_cls>> accountOrder_map
	 */
	private List<ONTAP__Order__c> processAccountMap(Map<Id,List<ISSM_OrderWrapper_cls>> accountOrder_map){
		List<ONTAP__Order__c> ordersDelete = new List<ONTAP__Order__c>();
		for(Id accId : accountOrder_map.keySet()){
			accountOrder_map.get(accId).sort();
			system.debug( '\n\n  ****** accountOrder_map.get(accId) = ' + accountOrder_map.get(accId)+'\n\n' );	
			system.debug( '\n\n  ****** accountOrder_map.get(accId) = ' + accountOrder_map.get(accId).size()+'\n\n' );	

			for(Integer i = 0; i < 8 ; i++){
				accountOrder_map.get(accId).remove(0);
			}
			system.debug( '\n\n  ****** accountOrder_map.get(accId) = ' + accountOrder_map.get(accId)+'\n\n' );	
			for(ISSM_OrderWrapper_cls orderWrapper: accountOrder_map.get(accId)){
				ordersDelete.add(orderWrapper.order);
			}
			system.debug( '\n\n  ****** ordersDelete = ' + ordersDelete+'\n\n' );

		}
		return ordersDelete;
	}

	/**
	 * Finish method for the batch execution
	 * @param  Database.BatchableContext BC
	 */	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}