global class ISSM_TaskAssignBatch_bch implements Database.Batchable<sObject>, Database.stateful{
    global final String Query;
    global final Task task_obj;

    global ISSM_TaskAssignBatch_bch(String q, String JSONTask){
        Query = q;
        task_obj = (Task) JSON.deserialize(JSONTask, Task.class);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('ISSM_TaskAssignBatch_bch START');
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        List<Task> taskToCreate = new List<Task>();
        System.debug(':::: ISSM_TaskAssignBatch_bch.execute [task_obj.RecordTypeId = '+task_obj.RecordTypeId+']');
        for(Account acc_obj : (List<Account>)scope){
            taskToCreate.add(new Task(
                OwnerId = acc_obj.OwnerId,
                Subject = task_obj.Subject,
                Description = task_obj.Description,
                Status = task_obj.Status,
                ActivityDate = task_obj.ActivityDate,
                Priority = task_obj.Priority,
                RecordTypeId = task_obj.RecordTypeId,
                WhatId = acc_obj.Id
            ));
        }
        System.debug(':::: ISSM_TaskAssignBatch_bch.execute [taskToCreate.size() = '+taskToCreate.size()+']');
        insert taskToCreate;
    }
    global void finish(Database.BatchableContext BC){
        System.debug('ISSM_TaskAssignBatch_bch FINISH');
    }
}