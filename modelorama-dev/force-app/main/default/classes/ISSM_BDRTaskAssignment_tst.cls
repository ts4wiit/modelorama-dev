@isTest
public class ISSM_BDRTaskAssignment_tst {
    @testSetup
    private static void mockData(){
        User ux = [SELECT Id,Name FROM User WHERE id= :System.UserInfo.getUserId()];
        System.runAs (ux) {
            UserRole ur_obj = [SELECT Id FROM UserRole WHERE name = 'Fuerza de Ventas FF10' ];
            List<User> users_lst = new List<User>();
            users_lst.add(new User(FirstName ='test',Username='test1@test.test.x', LastName='test', Email='test@test.test', Alias='tstx', CommunityNickname='tstxx', TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es_MX', EmailEncodingKey='ISO-8859-1', ProfileId=System.UserInfo.getProfileId(), LanguageLocaleKey='es',UserRoleId=ur_obj.id));
            insert users_lst;
        }
        ux = [SELECT Id,Name FROM User WHERE Username='test1@test.test.x'];
        List<Account> account_lst = new List<Account>();
        account_lst.add(new Account(Name = 'xxx-test', OwnerId = ux.Id));
        insert account_lst;
        ///
        ID regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='ISSM_RegionalSalesDivision'].Id;
        Account locationAcct_obj = new Account(Name='CMM Del Bajio',ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId);
        insert locationAcct_obj;
        regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='SalesOrg'].Id;
        locationAcct_obj = new Account(Name='Metropolitana',ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId, parentId=locationAcct_obj.Id);
        insert locationAcct_obj;
        regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='SalesOffice'].Id;
        locationAcct_obj = new Account(Name='Doctores',ONTAP__SalesOfficeDescription__c='FF10',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId, parentId=locationAcct_obj.id);
        
    }
    private static testMethod void whenDRVIsSelected(){
        ISSM_BDRTaskAssignment_cls cls = new ISSM_BDRTaskAssignment_cls();
        cls.searchOption_str = 'DRV';
        cls.searchString_str = 'FF10';
        cls.nextStep();
        cls.selectAll = true;
        cls.selectAll();
        cls.nextStep();
        cls.taskToFill_obj = new Task(Status='Not Started',Priority='Alta');
        cls.nextStep();
    }
    private static testMethod void whenBDRIsSelected(){
        ISSM_BDRTaskAssignment_cls cls = new ISSM_BDRTaskAssignment_cls();
        cls.reset();
        cls.searchOption_str = 'BDR';
        User ux = [SELECT Id,Name FROM User WHERE Username='test1@test.test.x'];
        cls.searchString_str = ux.Id;
        cls.nextStep();
        cls.selectAll();
        cls.selectAll = true;
        cls.selectAll();
        cls.nextStep();
        cls.taskToFill_obj = new Task(Status='Not Started',Priority='High');
        cls.nextStep();
    }
    private static testMethod void whenPDVIsSelected(){
        ISSM_BDRTaskAssignment_cls cls = new ISSM_BDRTaskAssignment_cls();
        cls.searchOption_str = 'PDV';
        cls.searchAccString_str = 'xxx-test';
        cls.nextStep();
        cls.selectAll = true;
        cls.selectAll();
        cls.nextStep();
        cls.taskToFill_obj = new Task(Status='Not Started',Priority='High');
        cls.nextStep();
    }
}