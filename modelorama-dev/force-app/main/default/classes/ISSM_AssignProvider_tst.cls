/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_AssignProvider_tst {

    /*
        Metodo que realiza la ejecicion cuando coincide el proccedor con una cuenta con tipo de registro provider
    */
    static testMethod void testBeforeSLAinProvider(){
        String RecordTypeAccountIdProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
         //Creamos proveedor sin correo 
        Account  objAccountProviderWithOutMail = new Account(); 
            objAccountProviderWithOutMail.Name = 'proveedor sin correo';
            objAccountProviderWithOutMail.ONTAP__Email__c = 'hdiaz@avanxo.com';
            objAccountProviderWithOutMail.RecordTypeId = RecordTypeAccountIdProvider;
            insert objAccountProviderWithOutMail;  
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;   
        //Creamos Cuenta
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
            insert objAccount;

        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 0098','Refrigeración','Mantenimiento','Barril','Tanque CO2',null,null,'Trade Marketing',user.id,null);
        List<Account> accProvider  = ISSM_CreateDataTest_cls.fn_CreateAccountProvider (true,RecordTypeAccountIdProvider,objAccountSalesOffice.Id);
  
        List<ISSM_AppSetting_cs__c> AppSettingProvider =  ISSM_CreateDataTest_cls.fn_CreateAppSettingProviderWit(true,objAccountProviderWithOutMail.Id);
        
        Test.startTest();  
       
           Case objCase = new Case();
                objCase.Subject = 'Subject';
                objCase.Description = 'Description'; 
                objCase.Status = 'new';
                objCase.ISSM_TypificationLevel1__c='Refrigeración';
                objCase.ISSM_TypificationLevel2__c='Mantenimiento';
                objCase.ISSM_TypificationLevel3__c ='Barril';
                objCase.ISSM_TypificationLevel4__c ='Tanque CO2';
                objCase.ISSM_TypificationLevel5__c = null;
                objCase.ISSM_TypificationLevel6__c = null;
                objCase.AccountID = objAccount.Id;
                objCase.ISSM_Provider__c =AppSettingProvider[0].ISSM_IdProviderWithOutMail__c;
                objCase.ISSM_TypificationNumber__c = TypificationMatrix.Id;
                insert objCase;
               // System.assertEquals(objCase.ISSM_Provider__c,accProvider[0].Id);
        Test.stopTest();
    }
    
    /*
        Metodo que realiza la ejecicion cuando no se encuentra proveedor y asigna el id que esta en el campo de una configuracion personalizada
    */
     static testMethod void testSLAinProviderNotAccountProvider(){
        String RecordTypeAccountIdProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();      
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;   
        //Creamos proveedor sin correo 
        Account  objAccountProviderWithOutMail = new Account(); 
            objAccountProviderWithOutMail.Name = 'proveedor sin correo';
            objAccountProviderWithOutMail.ONTAP__Email__c = 'hdiaz@avanxo.com';
            objAccountProviderWithOutMail.RecordTypeId = RecordTypeAccountIdProvider;
            insert objAccountProviderWithOutMail;  
        //Creamos Cuenta
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
            insert objAccount;
                
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        List<ISSM_AppSetting_cs__c> AppSettingProvider =  ISSM_CreateDataTest_cls.fn_CreateAppSettingProviderWit(true,objAccountProviderWithOutMail.Id);
        ISSM_TypificationMatrix__c  TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 0098','Refrigeración','Mantenimiento','Barril','Tanque CO2',null,null,'Trade Marketing',user.id,null);
        
        
        Test.startTest(); 
      
           Case objCase = new Case();
                objCase.Subject = 'Subject';
                objCase.Description = 'Description'; 
                objCase.Status = 'new';
                objCase.AccountID = objAccount.Id;
                objCase.ISSM_Provider__c = objAccountProviderWithOutMail.Id;
                objCase.ISSM_TypificationNumber__c = TypificationMatrix.Id;
                insert objCase;
               System.assertEquals(objCase.ISSM_Provider__c,AppSettingProvider[0].ISSM_IdProviderWithOutMail__c);
        Test.stopTest();
    }
}