/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Class to create or upsert a new Opening Goal. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Class to create or upsert a new Opening Goal.
==============================================================================================================================
*********************************************************************************************************************************/
public class MDRM_NewOpenStore {
    @INVOCABLEMETHOD(label ='Upsert the opening goals' description ='Opening Goals')
    public static void UpsertOpeningGoals (List<ID> lstID){
        account acc = [select id,  MDRM_Opening_Goal__c, ownerid, owner.EmployeeNumber, lastmodifieddate from account where id IN: lstID Limit 1 ];
        List<MDRM_Opening_Goal__c> lstGoals = new List<MDRM_Opening_Goal__c>();
    	list<account> lstAccount = new list<account>();
    	set<string> lstKeyacc = new set<string>();
    	map<string,account> mapAccount = new map<string,account>();

        
        Integer numberOfDays = Date.daysInMonth(acc.lastmodifieddate.year(), acc.lastmodifieddate.month());
		Date lastDayOfMonth = Date.newInstance(acc.lastmodifieddate.year(), acc.lastmodifieddate.month(), numberOfDays);
        Date accountdate = date.valueOf(acc.LastModifiedDate);
        Date firstDayofMonth = accountdate.toStartOfMonth();
        date thismonth = date.today();
        
        string keyacc = string.valueof(acc.lastmodifieddate.year())+string.valueof(acc.lastmodifieddate.month())+acc.owner.employeenumber;
        lstKeyacc.add(keyacc);
        
     
        mapAccount.put(keyacc, new account(id = acc.id));  
        lstAccount.add(acc);
        lstGoals.add(new MDRM_Opening_Goal__c(MDRM_user__c = acc.ownerid, 
                                                        MDRM_startDate__c =firstDayofMonth,
                                                        MDRMD_EndDate__c = lastDayOfMonth,
                                              			MDRM_External_Id__c = keyacc, 
                                             MDRM_Opening_Goal__c = 0
                                             ));
        
        
        Schema.SObjectField f = MDRM_Opening_Goal__c.Fields.MDRM_External_Id__c;
        Database.UpsertResult [] cr = Database.upsert(lstGoals, f, false);
       
        list<MDRM_Opening_Goal__c> lstthegoals = new list<MDRM_Opening_Goal__c>([select id, MDRM_External_Id__c from MDRM_Opening_Goal__c
                                                                                                   where MDRM_External_Id__c IN: lstKeyacc]);
        
       
        for(MDRM_Opening_Goal__c Goals: lstthegoals){
            mapAccount.get(Goals.MDRM_External_Id__c).MDRM_Opening_Goal__c = Goals.id;
            system.debug('the new goal' + goals);
        }
        list<account> lstUpdateAccount = mapAccount.values();
        update lstUpdateAccount;
    }
    }