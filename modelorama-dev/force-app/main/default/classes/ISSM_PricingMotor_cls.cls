/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Salesforce Implementatio 
    Customer:   Grupo Modelo

    Description:
    Class that makes the call of the service for price engine, 
    resulting in the list of prices and discounts of the products selected by the customer

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19/06/2017      Luis Licona                  Class creation
    1.1       04/07/2017      Luis Licona                  Add method for test class
    1.1       13/02/2018      Luis licona                  se agrego el parametro paymentMethod al request de motor de precios
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_PricingMotor_cls {
    public ISSM_DeserializePricingMotor_cls ObjDeserealized{get;set;}
    public Map<String,ONTAP__Order_Item__c> MapWS = new Map<String,ONTAP__Order_Item__c>();
    public WrpOrderItem[] LstWrpOI        = new List<WrpOrderItem>();
    public WrpPricingMotor[] LstWrpPM     = new List<WrpPricingMotor>(); 
    public ISSM_CallOutService_ctr CTR    = new ISSM_CallOutService_ctr();
    public ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    public Account ObjAcc = new Account();

    /**
    * It makes the call to the motor service of prices, 
    * resulting in the list of prices and discounts of the products
    * @param   StrIdAcc: Id Account  
    * @param   LstProd:  List of Products
    * @param   MapProd:  Map contain id and quantity of product
    * @return  ISSM_DeserializePricingMotor_cls: return list of Products convert in Order Item
    * with your pricing and total
    **/
    public ISSM_DeserializePricingMotor_cls CallOutPricingMotor(String StrIdAcc, 
                                                                ONTAP__Product__c[] LstProd,
                                                                Map<String, Decimal> MapProd,
                                                                Map<Id,String> MapCond,
                                                                Map<String,Map<String,Double>> allProds_map,
                                                                Map<String,Integer> comboQty_map){

        System.debug('MapProd = ' + MapProd);
        System.debug('LstProd = ' + LstProd);
        //get values of map
        Map<String, ISSM_PriceEngineConfigWS__c> MapConfiguracionWs = ISSM_PriceEngineConfigWS__c.getAll();   
        String StrToken = MapConfiguracionWs.get(Label.ISSM_ConfigurationWSTst).ISSM_AccessToken__c;  
        
        try {  
            String TPMFlag_Str;
            String dealFlag_Str;
            String comboID_Str;
            System.debug(loggingLevel.Error, '*** comboQty_map: ' + comboQty_map);
            System.debug(loggingLevel.Error, '*** LstProd: ' + LstProd);
            System.debug(loggingLevel.Error, '*** allProds_map: ' + allProds_map);
            if(comboQty_map.isEmpty() && !LstProd.isEmpty()){//Without combos
			
                System.debug(':::::::::alone Products processed 1::::::::::' + allProds_map);

                for(ONTAP__Product__c objProd : LstProd){
                
                	System.debug('********************TEST  ************* : '+objProd);
                	System.debug('********************TEST MapCond  ************* : '+MapCond);
                    
                    //TPMFlag_Str  = String.valueOf(MapCond.get(objProd.Id)).substring(0,1);
                    //dealFlag_Str = String.valueOf(MapCond.get(objProd.Id)).substring(1,2);
					if(MapCond !=null && !MapCond.isEmpty()){
						TPMFlag_Str = String.valueOf(MapCond.get(objProd.Id)).substring(0,1) == Label.ISSM_n ? Label.ISSM_Minus1 : String.valueOf(MapCond.get(objProd.Id)).substring(0,1);
                  		dealFlag_Str = String.valueOf(MapCond.get(objProd.Id)).substring(1,2) == Label.ISSM_n ? Label.ISSM_Minus1 : String.valueOf(MapCond.get(objProd.Id)).substring(1,2);
                  		LstWrpOI.add(new WrpOrderItem(objProd.ONCALL__Material_Number__c,Integer.valueOf(MapProd.get(objProd.Id)),TPMFlag_Str,DealFlag_Str));
					}else{
						LstWrpOI.add(new WrpOrderItem(objProd.ONCALL__Material_Number__c,1,'0','0','' ));
						LstProd =  new List<ONTAP__Product__c>();
					}
					
                    

                    
                }
            }else if(!allProds_map.isEmpty() && !LstProd.isEmpty()){
                System.debug(':::::::::combo Products processed 2::::::::::' + allProds_map);

                Map<String, ONTAP__Product__c> prodsInLst_map = new Map<String,ONTAP__Product__c>();
                Map<String, ONTAP__Product__c> prodsInLstBySKU_map = new Map<String,ONTAP__Product__c>();
                Map<String, ONTAP__Product__c> mapedProds_map = new Map<String,ONTAP__Product__c>();
                for(ONTAP__Product__c objProd : LstProd){
                    prodsInLst_map.put(objProd.Id,objProd);
                    prodsInLstBySKU_map.put(objProd.ONCALL__Material_Number__c,objProd);
                }
                System.debug(loggingLevel.Error, '*** prodsInLst_map: ' + prodsInLst_map);
                for(String idMap_Str : allProds_map.keySet()){
                    System.debug(loggingLevel.Error, '*** idMap_Str: ' + idMap_Str);
                    String key_Str = idMap_Str.length() > 18 ? idMap_Str.substring(0, 18) : idMap_Str;
                    System.debug(loggingLevel.Error, '*** key_Str: ' + key_Str);
                    mapedProds_map.put(idMap_Str, prodsInLst_map.get(key_Str));
                }

                System.debug('mapedProds_map = ' + mapedProds_map);
                System.debug('MapCond = ' + MapCond);

                for(String idMap_Str : mapedProds_map.keySet()){
                    String comboNumber_Str = idMap_Str.length() > 18 ? idMap_Str.substring(18,28) : '';
                    String idProduct_Str = idMap_Str.length() > 18 ? idMap_Str.substring(0,18) : idMap_Str;

                    TPMFlag_Str  = MapCond.containsKey(idProduct_Str) && String.valueOf(MapCond.get(idProduct_Str)) != '' ? String.valueOf(MapCond.get(idProduct_Str)).substring(0,1) : null;
                    dealFlag_Str = MapCond.containsKey(idProduct_Str) && String.valueOf(MapCond.get(idProduct_Str)) != '' ? String.valueOf(MapCond.get(idProduct_Str)).substring(1,2) : null;

                    WrpOrderItem woi_Obj = new WrpOrderItem();
                    woi_Obj.materialNumber  = mapedProds_map.get(idMap_Str).ONCALL__Material_Number__c;
                    woi_Obj.quantity        = Integer.valueOf(MapProd.get(idMap_Str));
                    woi_Obj.comboID         = comboNumber_Str;
                    woi_Obj.tpmFlagSelected = TPMFlag_Str;
                    woi_Obj.dealFlagSelected= dealFlag_Str;

                    System.debug('woi_Obj = ' + woi_Obj);

                    LstWrpOI.add(woi_Obj);
                }
            }

            if(!LstWrpOI.isEmpty()){
            	System.debug('**** LstWrpOI 1');
                ObjAcc = CTRSOQL.getAccountbyId(StrIdAcc);
                LstWrpPM.add(new WrpPricingMotor(StrToken,ObjAcc.ONTAP__SAP_Number__c,ObjAcc.ONTAP__SalesOgId__c,ObjAcc.ONTAP__ChannelId__c,ObjAcc.ISSM_CustomerType__c,LstWrpOI));
            	LstWrpOI = new List<WrpOrderItem>();
            }
            

            if(!LstWrpPM.isEmpty()){
            	System.debug('**** LstWrpPM 2');
                String StrSerializeJson = JSON.serializePretty(LstWrpPM[0]);
                System.debug('StrSerializeJsonPM----->'+StrSerializeJson);
                if(!Test.isRunningTest()) {
                    List<ISSM_CallOutService_ctr.WprResponseService> response = CTR.SendRequest(StrSerializeJson,Label.ISSM_ConfigurationWSTst);
                    System.debug('response----->'+response);
                    ObjDeserealized = ISSM_DeserializePricingMotor_cls.parse(response[0].strBodyService);
                    System.debug('ObjDeserealizedPM--->'+ObjDeserealized);
                    LstWrpPM = new List<WrpPricingMotor>(); 
                }
               
            }
        }catch(Exception exc){
            exc.setMessage(exc +' | '+ Label.ISSM_ConfigurationWSTst);
            throw exc;
            
        }
        
        return ObjDeserealized;
    }
 

    /**
    * Wrapper for Request
    * @param   none
    * @return  none
    **/
    public class WrpPricingMotor{
        public String Token;
        public String customerNumber;
        public String organizacionVentas;
        public String canalDistribucion;
        public String paymentMethod;
        public List<WrpOrderItem> orderItem;

        //constructor
        public WrpPricingMotor(String Token, String customerNumber,String organizacionVentas,String canalDistribucion,
                               String paymentMethod, List<WrpOrderItem> orderItem){
            this.Token=Token;
            this.customerNumber=customerNumber;
            this.organizacionVentas=organizacionVentas;
            this.canalDistribucion=canalDistribucion;
            this.paymentMethod=paymentMethod;
            this.orderItem=orderItem;
        }
    }

    /**
    * Wrapper for Request
    * @param   none
    * @return  none
    **/
    public class WrpOrderItem{
        public String materialNumber;
        public Integer quantity;
        public String tpmFlagSelected;
        public String dealFlagSelected;
        public String comboID;

        public WrpOrderItem(){}

        //constructor
        public WrpOrderItem(String materialNumber,Integer quantity,String tpmFlagSelected,String dealFlagSelected){
            this.materialNumber=materialNumber;
            this.quantity=quantity;
            this.tpmFlagSelected=tpmFlagSelected;
            this.dealFlagSelected=dealFlagSelected;
        }

        public WrpOrderItem(String materialNumber,Integer quantity,String tpmFlagSelected,String dealFlagSelected,String comboID){
            this.materialNumber=materialNumber;
            this.quantity=quantity;
            this.comboID=comboID;
            this.tpmFlagSelected=tpmFlagSelected;
            this.dealFlagSelected=dealFlagSelected;            
        }
    }
}