/******************************************************************************* 
* Developed by:   	Everis México
* Author:   		Alfonso de la Cuadra
* Project:   		AbInBev - Modeloramas
* Description:   	V360 controller
*
* No.         Date               Author                      Description
* 1.0    	  13-June-2019       Alfonso de la Cuadra        Created
*******************************************************************************/

public class MDRM_ModeloramaFullViewCtrl {
    /*@AuraEnabled 
    public static List<contentDocument> getImages(){
        
		List<contentDocument> img = [SELECT Id, Title, FileType, CreatedBy.Name, ContentSize 
                                     FROM contentDocument 
                                     WHERE FileType ='PNG' 
                                     OR FileType ='JPG'
                                     LIMIT 21];
        
        return img;
    }*/
    
    /**
     * @Description. 
     */
    @AuraEnabled
    public static List<MDRM_Sale_Summary__c> getSummary(String recordId, String soqlOrder) {
        /*return [SELECT Id, Name, MDRM_AveMonthlySaleHect__c, MDRM_MixHighEnd__c, MDRM_MixAboveCore__c, MDRM_SKUs__c, MDRM_Year__c, MDRM_Month__c,
                	MDRM_Account__c
                FROM MDRM_Sale_Summary__c
                WHERE MDRM_Account__c =: recordId
                ORDER BY MDRM_Year__c DESC, MDRM_Month__c DESC];*/
        String soql = 'SELECT Id, Name, MDRM_AveMonthlySaleHect__c, MDRM_MixHighEnd__c, MDRM_MixAboveCore__c, MDRM_SKUs__c, MDRM_Year__c, MDRM_Month__c,' +
            			' MDRM_Account__c' +
            		  ' FROM MDRM_Sale_Summary__c' +
                	  ' WHERE MDRM_Account__c =: recordId ' +
                	  ' ORDER BY MDRM_Year__c ' + soqlOrder + ', MDRM_Month__c ' + soqlOrder;
        return Database.query(soql);
    }
    
    @AuraEnabled
    public static Map<String, String> fetchPicklistLabels(String objName, String fieldName) {
        Map<String, String> mpValues = new Map<String, String>();
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(fieldName).getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry pv : picklistValues) {
            System.debug('Value: ' + pv.getValue());
            System.debug('Label: ' + pv.getLabel());
            
            mpValues.put(pv.getValue(), pv.getLabel());
        }
        
        return mpValues;
    }
    
    @AuraEnabled
    public static Account getAccount(String id) {
        return [SELECT Id, Name, OwnerId, MDRM_SupervisorSalesOfficeSAG__c, MDRM_Opening_Date__c, Z001__c, MDRM_WinningProfile__c, Phone, ONTAP__Email__c, MDRM_NumberEmployees__c, MDRM_SeniorityBusinessmanModelorama__c,
                	MDRM_BusinessTraining__c, MDRM_BusinessmanContract__c
                FROM Account 
                WHERE Id =: id];
    }
    
    @AuraEnabled
    public static User getUser(String id) {
        Account acc = getAccount(id);
        System.debug('Owner: ' + acc.OwnerId);
        return [SELECT Id, Name, UserRoleId, MDRM_LiderGerenteApprover__c, MDRM_SupervisorApprover__c FROM User WHERE Id =: acc.OwnerId];
    }
    
    @AuraEnabled 
    public static Boolean getEditPermission() {
        return FeatureManagement.checkPermission('MDRM_V360_Edit_Permission');
    }
    
    @AuraEnabled
    public static User getDRV(String id) {
        User user = null;
        UserRole role = [SELECT Id, Name, ParentRoleId FROM UserRole WHERE Id =: id];
        UserRole roleMngr = [SELECT Id, Name, ParentRoleId FROM UserRole WHERE Id =: role.ParentRoleId];
            
        System.debug('User role: ' + roleMngr.Name);
        
        if(roleMngr.ParentRoleId != null) {
            user = [SELECT Id, Name FROM User WHERE UserRoleId =: roleMngr.ParentRoleId AND IsActive = true LIMIT 1];
        }
        System.debug(user.Name);
        
        return user;
    }
    
    @AuraEnabled
    public static Account getBusinessman(String id) {
        List<MDRM_Businessman_Expansor__c> lbusinessRel = getBusinessmanRelated(id);
        Account acc = null;
        if(lbusinessRel != null && lbusinessRel.size() > 0) {
            acc = getAccount(lbusinessRel[0].MDRM_Businessman__c);
        }
        return acc;
    }
    
    private static List<MDRM_Businessman_Expansor__c> getBusinessmanRelated(String id) {
        return [SELECT Id, Name, MDRM_Businessman__c
                FROM MDRM_Businessman_Expansor__c 
                WHERE MDRM_Expansor__c =: id 
                AND MDRM_Status__c = 'Active'
                ORDER BY CreatedDate DESC];
    }
    
    @AuraEnabled
    public static List<WPDocumentClass> fetchDocuments(String id, List<String> lfileName) {
        Map<String, MDRM_Document__c> mpMdrmDocById = new Map<String, MDRM_Document__c>();
        List<WPDocumentClass> mpWpDoc = new List<WPDocumentClass>();
        Set<String> ldoc = new Set<String>();
       
        System.debug('Tienda: ' + id);
        System.debug('LDevName: ' + lfileName);
        
        for(MDRM_Document__c docI : [SELECT Id, Name, MDRM_License_Type__c, MDRM_Exp_Date__c, RecordType.DeveloperName
                					 FROM MDRM_Document__c
                					 WHERE MDRM_Account__c =: id 
                                     AND RecordType.DeveloperName IN: lfileName
                                     AND MDRM_Status__c = 'Aprobado'
                                     ORDER BY Name ASC, CreatedDate DESC]) {
            if(!ldoc.contains(docI.Name)) {
            	mpMdrmDocById.put(docI.Id, docI);
                ldoc.add(docI.Name);
            } 
        }
        
        if(mpMdrmDocById != null && mpMdrmDocById.size() > 0) {
            for(ContentDocumentLink contentLinkI : [SELECT ContentDocumentId,Id,LinkedEntityId 
                                                    FROM ContentDocumentLink 
                                                    WHERE LinkedEntityId IN: mpMdrmDocById.keySet()]) {
                MDRM_Document__c mdrmDoc = mpMdrmDocById.get(contentLinkI.LinkedEntityId);
                                                        
                WPDocumentClass wpDoc = new WPDocumentClass();
                wpDoc.mdrmDocument = mdrmDoc;
                wpDoc.content = contentLinkI;
                                                        
                mpWpDoc.add(wpDoc);
            }
        }
        /*List<ContentDocument> img = [SELECT Id, Title, FileType, CreatedBy.Name, ContentSize 
                                     FROM ContentDocument 
                                     WHERE Id IN: lcontentId];*/
        
        return mpWpDoc;
    }
    
    @AuraEnabled
    public static List<WPHeaderPictureClass> getBusinessmanSurveys(String businessman, String tienda) {
        List<WPHeaderPictureClass> lheaderPicture = new List<WPHeaderPictureClass>();
        Map<String, String> mpQuest = new Map<String, String>();
        
        if(String.isBlank(businessman)) {
            Account acc = getBusinessman(tienda);
            businessman = acc.Id;
        }
        
        ONTAP__SurveyTaker__c taker = getSurveyTaker(businessman, 'ASC');
        
        if(taker.ONTAP__Survey__c != null) {
            mpQuest = fetchQuestions(taker.ONTAP__Survey__c);
        }
        
        WPHeaderPictureClass headerPicture = new WPHeaderPictureClass();
        List<WPQuestionClass> lquest = new List<WPQuestionClass>();
        Map<String, ONTAP__SurveyQuestionResponse__c> mpResp = fetchResponses(taker.Id, mpQuest.keySet());
        Map<String, Set<String>> mpAttch = fetchImages(mpResp.keySet());
        
        for(String responseId : mpAttch.keySet()) {
            WPQuestionClass quest = new WPQuestionClass();
            
            if(mpAttch.containsKey(responseId)) {
                quest.lpicture = mpAttch.get(responseId);
            }
            
            if(mpResp.containsKey(responseId)) {
                quest.questionName = mpQuest.get(mpResp.get(responseId).ONTAP__Survey_Question__c);
            }
            
            lquest.add(quest);
        }
        
        headerPicture.surveyName = taker.ONTAP__Survey__r.Name;
        headerPicture.lquest = lquest;
        
        lheaderPicture.add(headerPicture);
        
        taker = getSurveyTaker(businessman, 'DESC');
        
        if(taker.ONTAP__Survey__c != null) {
            mpQuest = fetchQuestions(taker.ONTAP__Survey__c);
        }
        
        headerPicture = new WPHeaderPictureClass();
        lquest = new List<WPQuestionClass>();
        mpResp = fetchResponses(taker.Id, mpQuest.keySet());
        mpAttch = fetchImages(mpResp.keySet());
        
        for(String responseId : mpAttch.keySet()) {
            WPQuestionClass quest = new WPQuestionClass();
            
            if(mpAttch.containsKey(responseId)) {
                quest.lpicture = mpAttch.get(responseId);
            }
            if(mpResp.containsKey(responseId)) {
                quest.questionName = mpQuest.get(mpResp.get(responseId).ONTAP__Survey_Question__c);
            }
            lquest.add(quest);
        }
        
        headerPicture.surveyName = taker.ONTAP__Survey__r.Name;
        headerPicture.lquest = lquest;
        
        lheaderPicture.add(headerPicture);
        
        return lheaderPicture;
    }
    
    private static ONTAP__SurveyTaker__c getSurveyTaker(String businessman, String order) {
        String soql = 'SELECT Id, Name, ONTAP__Survey__c, ONTAP__Survey__r.Name ' +
            		  'FROM ONTAP__SurveyTaker__c ' +
            		  'WHERE ONTAP__Account__c =: businessman ';
        if(String.isNotBlank(order)) {
            soql = soql + 'ORDER BY CreatedDate ' + order + ' ';
        }
        soql = soql + 'LIMIT 1';
        
        return Database.query(soql);
    }
    
    private static Map<String, String> fetchQuestions(String survId) {
        Map<String, String> mpQuest = new Map<String, String>();
        for(ONTAP__Survey_Question__c questI : [SELECT Id, Name FROM ONTAP__Survey_Question__c WHERE ONTAP__Survey__c =: survId]) {
        	mpQuest.put(questI.Id, questI.Name);
        }
        return mpQuest;
    }
    
    private static Map<String, ONTAP__SurveyQuestionResponse__c> fetchResponses(String takerId, Set<String> lquest) {
        Map<String, ONTAP__SurveyQuestionResponse__c> mpResp = new Map<String, ONTAP__SurveyQuestionResponse__c>();
        for(ONTAP__SurveyQuestionResponse__c respI: [SELECT Id, Name, ONTAP__Survey_Question__c 
                                                     FROM ONTAP__SurveyQuestionResponse__c 
                                                     WHERE ONTAP__SurveyTaker__c =: takerId
                                                     AND ONTAP__Survey_Question__c IN: lquest]) {
            mpResp.put(respI.Id, respI);
        }
        return mpResp;
    }
    
    private static Map<String, Set<String>> fetchImages(Set<String> lparent) {
        Map<String, Set<String>> mpAttch = new Map<String, Set<String>>();
        for(Attachment attI : [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN: lparent 
                               ORDER BY ParentId ASC]) {
        	if(mpAttch.containsKey(attI.ParentId)) {
            	mpAttch.get(attI.ParentId).add(attI.Id);
            } else {
                mpAttch.put(attI.ParentId, new Set<String>{attI.Id});
            }
        }
        
        return mpAttch;
    }
    
    public class WPDocumentClass {
        @AuraEnabled public MDRM_Document__c mdrmDocument {get;set;}
        @AuraEnabled public ContentDocumentLink content {get;set;}
    }
    
    public class WPHeaderPictureClass {
        @AuraEnabled public String surveyName {get;set;}
        @AuraEnabled public List<WPQuestionClass> lquest {get;set;}
    }
    
    public class WPQuestionClass {
        @AuraEnabled public String questionName {get;set;}
        @AuraEnabled public Set<String> lpicture {get;set;}
    }
}