/**
 * This class is the schedule for the AllMobileDeviceUserBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileDeviceUserSchedulerClass implements Schedulable {

	/**
	 * Execute AllMobileDeviceUserBatchClass class.
	 */
	global void execute(SchedulableContext objSchedulableContext) {
		AllMobileDeviceUserBatchClass objAllMobileDeviceUserBatchClass = new AllMobileDeviceUserBatchClass();
		Database.executeBatch(objAllMobileDeviceUserBatchClass);
	}
}