@isTest
private class ISSM_TypificationMatrix_tst {
    public static ISSM_TypificationMatrix__c  typificationMatrix;
    
    @isTest static void testGetMatrixNotNull() {
        typificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia');
        Test.startTest();
        ISSM_TypificationMatrix_cls tipyficationMatrixCls = new ISSM_TypificationMatrix_cls();
        ISSM_TypificationMatrix__c typificationMatrixResult = tipyficationMatrixCls.getTypificationMatrix(typificationMatrix.Id);
        System.assert( typificationMatrixResult != null,'Typification matrix is not null');
        System.assertEquals(typificationMatrix.Id,typificationMatrixResult.Id);
        Test.stopTest();
    }

    @isTest static void testGetMatrixNull() {
        Test.startTest();
        ISSM_TypificationMatrix_cls tipyficationMatrixCls = new ISSM_TypificationMatrix_cls();
        System.assert(tipyficationMatrixCls.getTypificationMatrix(null) == null,'Typification matrix is null');
        Test.stopTest();
    }
    
    @isTest static void testGetMatrixOptions(){
        typificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia');
        Test.startTest();
        ISSM_TypificationMatrix_cls tipyficationMatrixCls = new ISSM_TypificationMatrix_cls();
        List<ISSM_TypificationMatrix__c> typificationMatrixResults = tipyficationMatrixCls.getTypificationMatrix('Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia');
        System.assert( typificationMatrixResults.size() > 0,'Typification matrix is not null');
        Test.stopTest();
    }
}