/**
 * Development by:      Avanxo Mexico
 * Author:              Carlos Pintor (cpintor@avanxo.com) / Luis Licona
 * Project:             AbInbev - Trade Revenue Management
 * Description:         Apex Controller class of the Lightning Component 'TRM_EditCondition_lcp'.
 *                      Methods included achives the processes: 
 *                      - get Custom Metadata Types for the Condition Class
 *                      - get a sObject Record from Salesforce
 *                      - get Catalogs of Condition Class
 *                      - get Sales Structures for Customers
 *                      - get an Id List of Condition Record Customers (Accounts)
 *
 * N.     Date             Author                     Description
 * 1.0    2018-09-18       Carlos Pintor              Created
 *
 */
public with sharing class TRM_EditCondition_ctr {
	
	/**
    * @description  Receives a string with the sObject type and the Id of the record to get from the server
    * 
    * @param    objectType     String of the sObject type to retrieve. E.g: 'TRM_ConditionClass__c'
    * @param    id             String of the id of the record to retrieve. E.g: '5tdS4l3sf0rc31d'
    * 
    * @return   Return a sObject record 
    */
	@AuraEnabled
	public static sObject getRecordById(String objectType, String id) {
		System.debug('#### start TRM_EditCondition_ctr.getRecordById()');
		sObject returnValue;
		if( id != null || id != '' ){
			returnValue = ISSM_UtilityFactory_cls.getRecordById(objectType, id);
		}
		System.debug('#### record: ' + returnValue);
		return returnValue;
	}

	/**
    * @description  Receives three parameters with the Object API name, externalKey and boolean to indicate if the requiered record is a parent or child. 
    * 				When the boolean atribute is true, the search is done in the TRM_ConditionsManagement__mdt metadata.
    * 				When the boolean atribute is false, the search is done in the TRM_ConditionRelationships__mdt metadata.
    *               If the externalKey is found, the record of Custom Metatada Type is returned. 
    * 
    * @param    	objectType      String of the Custom Metadata Type API name. E.g: 'TRM_ConditionsManagement__mdt'
    * @param    	externalKey     String of the externalKey to search in Custom Metadata Type. E.g: 'ZMIW_971'
    * @param    	isParent 	    Boolean to indicate if the required record is parent(true) or child(false). E.g: true
    * 
    * @return   Return a record of type 'sObject'
    */
	@AuraEnabled
	public static sObject getConditionMetadata(String objectType, String externalKey, Boolean isParent) {
		System.debug('#### start TRM_EditCondition_ctr.getConditionMetadata()');
		sObject conditionMetadata;
		String queryString = 'SELECT ';
				queryString += String.join(new List<String>(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().keySet()),', ');
				queryString += ' FROM ' + objectType;
				queryString += isParent ? ' WHERE TRM_ExternalKey__c = \'' + externalKey + '\'' : ' WHERE TRM_ConditionManagement__r.TRM_ExternalKey__c = \'' + externalKey + '\'' ;
		List<sObject> resultList = ISSM_UtilityFactory_cls.executeQuery(queryString);

		if(resultList.size() == 1){
			conditionMetadata = resultList[0];
		}
		
		System.debug('#### conditionMetadata: ' + conditionMetadata);
		return conditionMetadata;
	}

	/**
    * @description  Receives a parameter with the Salesforce Id of the Condition Class of the Condition Records to retrieve.
    * 				It queries on the TRM_ConditionRecord__c records to get the account Ids in the field 'TRM_Customer__c'.
    * 				The results are group by the field 'TRM_Customer__c' in order to get unique values.
    * 
    * @param     	conditionId			Salesforce Id of the parent Condition Class of the Condition Records
    * 
    * @return   Return a list of Salesforce Ids of Accounts
    */
	@AuraEnabled
	public static String[] getCustomerIdList(String conditionId){
		System.debug('#### START TRM_EditCondition_ctr.getCustomerIdList()');
		ID id = (ID) conditionId;
		Integer maxRows = Integer.valueOf(System.Label.TRM_MaxRowsReturned);
		String[] customerIdList = new List<String>();
		for(AggregateResult record : [SELECT TRM_Customer__c FROM TRM_ConditionRecord__c WHERE TRM_ConditionClass__c = :id GROUP BY TRM_Customer__c LIMIT :maxRows]){
			customerIdList.add(''+record.get('TRM_Customer__c'));
		}
		System.debug('#### customerIdList:' + customerIdList.size());
		return customerIdList;		
	}

	/**
    * @description  Get Catalogs for price zone and segment
    * @param    none 
    * @return   Return Map<String,String> for get estructures
    */
	@AuraEnabled
    public static Map<String,String> getCatalog() {
    	Map<String,String> MapCatalog = TRM_MainCreateCondition_ctr.getCatalog();
    	return MapCatalog;
    }
	
    /**
    * @description  Get sales offices and sales orgs 
    * @param    none 
    * @return   Return Map<String,String> for get estructures
    */
	@AuraEnabled
    public static Map<String,String> getStructures() {
    	Map<String,String> MapStructures = TRM_MainCreateCondition_ctr.getStructures();
    	return MapStructures;
    }
}