public class Agri_PayOrderEmail_Ctrl {
    
    public static void gestionaEnvioPDF(String recordId) {
        sendEmailPDF(recordId);
    }
    
    @future(callout=true)
    public static void sendEmailPDF(String recordId) {                
                
        PageReference PDF = Page.Agri_PayOrder_VF_PDF;
        PDF.getParameters().put('Id', recordId);
        PDF.setRedirect(true);
        
        Agri_Quality_Control__c qa = [SELECT Id, Agri_pd_entregaPedido__c, Agri_pd_entregaPedido__r.Agri_rb_supplier__r.Name, Agri_pd_entregaPedido__r.Agri_rb_supplier__r.ONTAP__Email__c, Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Name   
                                      FROM Agri_Quality_Control__c WHERE Id =: recordId];
        
        ContentVersion cv = new ContentVersion();
        Blob body;        
        
        if(!Test.isRunningTest()) { body = pdf.getContent(); }
        else { body = Blob.valueOf('TEST'); }
        
        cv.versionData = body;
        cv.title = 'Resultados QA - Orden de Pago - ' + qa.Agri_pd_entregaPedido__r.Agri_rb_supplier__r.Name;
        cv.pathOnClient = 'Resultados QA - Orden de Pago - ' + qa.Agri_pd_entregaPedido__r.Agri_rb_supplier__r.Name +'.pdf';
        cv.FirstPublishLocationId = qa.Agri_pd_entregaPedido__c; //recordId;
        insert cv;
        
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Resultados QA - Orden de Pago - ' + qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Name +'.pdf');
        efa.setBody(body);
        
        /*String email = qa.Agri_pd_entregaPedido__r.Agri_rb_supplier__r.ONTAP__Email__c;        
        List<String> lEmails = new List<String>();
        lEmails.add(email);        
        
        Messaging.SingleEmailMessage objectEmail = new Messaging.SingleEmailMessage();
        objectEmail.setToAddresses(lEmails);
        objectEmail.setSubject('Orden de Pago ' + qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Name);
        objectEmail.setHtmlBody('Se envia la orden de pago correspondiente a la orden de entrega con Folio: ' + qa.Agri_pd_entregaPedido__r.Agri_rb_deliveryOrder__r.Name);
        objectEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.SendEmailResult [] r1 = Messaging.sendEmail(new MEssaging.SingleEmailMessage[] {objectEmail});                  
		*/
    }

}