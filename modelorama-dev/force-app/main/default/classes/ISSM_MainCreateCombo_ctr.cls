/******************************************************************************** 
    Company:            Avanxo México
    Author(s):          Luis Licona / Carlos Pintor
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows to perform searches, save and send approval of a combo

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    17-April-2018    Luis Licona                 Creation
    1.1    17-April-2018    Carlos Pintor               saveComboByAccount...
    ******************************************************************************* */
public with sharing class ISSM_MainCreateCombo_ctr {
    
    @AuraEnabled
    public static void saveComboByAccount(ISSM_ComboByAccount__c[] comboByAccountList,String StrIdCombo){
        System.debug( '####comboByAccountList:' + comboByAccountList );
        System.debug( '####comboByAccountList.size:' + comboByAccountList.size());
        System.debug('########StrIdCombo: '+StrIdCombo);
        try{
            Database.insert(comboByAccountList);
            String comment      = Label.TRM_Comment; 
            String nameProcess  = 'ISSM_ApprovalCombo'; 
            Id userId           = UserInfo.getUserId();
            submitForApproval(StrIdCombo,nameProcess,userId,comment);
        } catch(Exception e){
            System.debug('##ERROR## comboByAccountList: '+ e.getStackTraceString());
        }
        
    }

    @AuraEnabled
    public static void saveComboByProductList(ISSM_ComboByProduct__c[] comboByProductList){
        try{
            Database.insert(comboByProductList);
        }catch(Exception Exc){
            System.debug('##ERRROR## comboByProductList: '+Exc.getStackTraceString());
        }
    }

    @AuraEnabled
    public static SalesStructure getParentSalesStructure(String Id){
        SalesStructure returnValue = new SalesStructure();
        String sQuery = 'SELECT Id, ISSM_ParentAccount__c, ISSM_ParentAccount__r.ISSM_ParentAccount__c';
                sQuery += ' FROM Account';
                sQuery += ' WHERE Id = \'' + id + '\'';
                sQuery += ' LIMIT 1';
        System.debug('####sQuery:' + sQuery);
        List<sObject> records = ISSM_UtilityFactory_cls.executeQuery(sQuery);
        Account acc = new Account();
        for(sObject o : records){
            acc = (Account) o;
        }
        
        returnValue.id = acc.Id;
        returnValue.parentId = acc.ISSM_ParentAccount__c;
        returnValue.grandParentId = acc.ISSM_ParentAccount__r.ISSM_ParentAccount__c;
        
        System.debug('####returnValue:' + returnValue);
        return returnValue;
    }

    //@AuraEnabled
    //public static RecordType getRecordTypeId(String strObject, String DeveloperName){
    //    RecordType objRecType = ISSM_UtilityFactory_cls.getRecordTypeId(strObject,DeveloperName);
    //    return objRecType;
    //}

    @AuraEnabled
    public static String getRecordTypeIdByDeveloperName(String objectType, String developerName){
        String recordTypeId = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName(objectType, developerName);
        return recordTypeId;
    }

    public class SalesStructure{
        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String parentId {get;set;}
        @AuraEnabled public String grandParentId {get;set;}
    }
    

    @AuraEnabled
    public static void submitForApproval(Id strId,String nameProcess,String userId,String comment){
        User[] Lst = [SELECT ManagerId FROM User WHERE Id =: userId];
        if(String.isNotBlank(Lst[0].ManagerId)){
            System.debug('EXECUTE APPROBAL');
            ISSM_ApprovalProcess_cls.submitForApproval(strId,comment,nameProcess,userId);
        }else{
            updateStatusCombo(strId);
        }
    }


    //@AuraEnabled
    //public static Boolean validateManager(){
    //    User[] Lst = [SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId()];
    //    return (String.isBlank(Lst[0].ManagerId)) ? true : false;
    //}


    @AuraEnabled
    public static void updateStatusCombo(Id ComboId){
        ISSM_Combos__c[] LstCombo = [SELECT ISSM_StatusCombo__c FROM ISSM_Combos__c WHERE Id =: ComboId];
        LstCombo[0].ISSM_StatusCombo__c = 'ISSM_Approved';
        upsert LstCombo;
    }
}