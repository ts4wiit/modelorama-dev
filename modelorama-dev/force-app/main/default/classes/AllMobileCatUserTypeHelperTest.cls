/**
 * Test class for AllMobileCatUserTypeHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileCatUserTypeHelperTest {

	/**
	 * Test method to setup data.
	 */
	static testMethod void setup() {

		//Create a CatUserType.
		AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1111111110', 'ZZZ', 'ZZZ', '1111111110');
        insert objAllMobilePermissionCategoryUserTypeAll;
        AllMobileCatUserType__c objAllMobileCatUserType = AllMobileUtilityHelperTest.createAllMobileCatUserType('ZZZ', false, false, 'Almacén ZZZ', 'ZZZ', objAllMobilePermissionCategoryUserTypeAll);
		insert objAllMobileCatUserType;
        objAllMobileCatUserType.Name = 'WWW';
        update objAllMobileCatUserType;
	}

    @isTest
    static void testValidateCatUserTypeNameDuplication() {
        try {
            AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1111111110', 'ZZZ', 'ZZZ', '1111111110');
        	insert objAllMobilePermissionCategoryUserTypeAll;
            AllMobileCatUserType__c objAllMobileCatUserType = AllMobileUtilityHelperTest.createAllMobileCatUserType('ZZZ', false, false, 'Almacén ZZZ', 'ZZZ', objAllMobilePermissionCategoryUserTypeAll);
			insert objAllMobileCatUserType;
            AllMobileCatUserType__c objAllMobileCatUserType2 = AllMobileUtilityHelperTest.createAllMobileCatUserType('ZZZ', false, false, 'Almacén YYY', 'YYY', objAllMobilePermissionCategoryUserTypeAll);
			insert objAllMobileCatUserType2;
        } catch(Exception objException) {

        }
    }
}