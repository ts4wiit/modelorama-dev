/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 20: Validation for KATR2 field, when this is equal to '30' then fields AUFSD , LIFSD and FAKSD must to be with the blocked code('01').
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       10-04-2018   	Iván Neria    			Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0020_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0020_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        Boolean ruleHasError = false;
        
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            
            for(MDM_FieldByRule__c field : uniqueFields(rule.MDM_Fields_by_Rule__r).Values()){
                if(field.MDM_IsActive__c && toEvaluate.get(field.MDM_APIFieldName__c)!=null) {
                        if(!MDM_Account_cls.getKALKSvalues().contains((String)toEvaluate.get(field.MDM_APIFieldName__c))){
                          	fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        	validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        	validationResult.put((fieldName + DESCRIPTION_SUFIX), String.format(rule.MDM_Error_Message__c, new List<String>{(String.valueOf(toEvaluate.get(field.MDM_APIFieldName__c)).length()+'')} ));
                        	validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        	validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        	ruleHasError = true;   
                        }                    
                }
                if(ruleHasError) validationResults.add((MDM_Validation_Result__c)validationResult);
                validationResult = new MDM_Validation_Result__c();
                validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
                ruleHasError = false;
            }
            
        }
        return validationResults;
    }
}