/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_RefreshCampaign_tst {
	public static List<ISSM_IntervalsByCountry__c> intervalsByCountry;
	public static List<ISS_DocumentTypeOpenItems__c> DocumentTypeOpenItems;
	public static  List<ISSM_TriggerFactory__c> AppTriggerFactory;
	public static  List<ISSM_AppSetting_cs__c> AppSetting;
    static testMethod void myUnitTest() {
    	intervalsByCountry = ISSM_CreateDataTest_cls.fn_CreateIntervalsByCountry(true,'Interval1','MX','8','90');
		User createUser =  ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
		DocumentTypeOpenItems = ISSM_CreateDataTest_cls.fn_CreateDocumentTypeOpenItems(true,'DocumentType');
    	
    	// Cretae configuracion personalizada 
		List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo','ISSM_Telecolletion') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
		AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSettingTelecollection(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'),mapIdQueue.get('ISSM_Telecolletion'));
		AppTriggerFactory = ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        
    	//Generamos campaña  Telecollection_Campaign__c
		Telecollection_Campaign__c objCreateCampaign = new Telecollection_Campaign__c();
		objCreateCampaign.Name = 'EjemploCampaignTest';
		objCreateCampaign.Start_Date__c = System.today();
		objCreateCampaign.End_Date__c =  System.today();
		objCreateCampaign.Active__c = true;
		objCreateCampaign.SoqlFilters__c='';
		objCreateCampaign.HONES_Country__c = 'México';
		insert objCreateCampaign;
		
		SchedulableContext dbBC;
		Test.startTest();
			 ISSM_RefreshCampaign_Sh objRefreshCampaign = new ISSM_RefreshCampaign_Sh();
     		objRefreshCampaign.execute(dbBC);
        Test.stopTest();
    
    }
}