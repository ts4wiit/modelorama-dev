@isTest
private class BusinessmanControllerTest {

    @isTest
    static void initTest(){
        BusinessmanController ctrl = new BusinessmanController();
        System.assert(!ctrl.genderlist.isEmpty());
    }
    
    @isTest
    static void buscarMiModeloramaFoundVinculos(){
        //to do
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessmanEmpty(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
        
        Test.startTest();
        PageReference pagina = Page.Enterprise_profile;
        Test.setCurrentPage(pagina);
        ApexPages.currentPage().getParameters().put('MDRM_Z019','55555');
        ApexPages.currentPage().getParameters().put('MDRM_Email','acc@acc.com');
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.buscarMiModelorama();
        
        List<MDRM_Vinculo__c> exist = ctrl.empresarioConModeloramas;
        System.assert(!exist.isEmpty());
        Test.stopTest();
    }
    
    @isTest
    static void buscarMiModeloramaFoundExpansor(){
        
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Businessman_Expansor__c expansor = BusinessmanDataFactory.createExpansorMdrmEnterprise(Businessman,Modeloramas);
        
        Test.startTest();
        PageReference pagina = Page.Enterprise_profile;
        Test.setCurrentPage(pagina);
        ApexPages.currentPage().getParameters().put('MDRM_Z019','55555');
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.buscarMiModelorama();
        
        List<MDRM_Vinculo__c> exist = ctrl.empresarioConModeloramas;
        System.assert(!exist.isEmpty());
        Test.stopTest();
    }
    
    @isTest
    static void buscarMiModeloramaNotFound(){
		BusinessmanController ctrl = new BusinessmanController();
        String nonExistingModelorama = 'xyz';
		ctrl.codigoModelorama = nonExistingModelorama;
        ctrl.buscarMiModelorama();
        String error = ctrl.inputTextErrorMessage;
        System.assert(error.contains(''));
    }
    
    @isTest
    static void buscarMiModeloramaErrorPage(){
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Businessman_Expansor__c expansor = BusinessmanDataFactory.createExpansorMdrmEnterprise(Businessman,Modeloramas);
        
		BusinessmanController ctrl = new BusinessmanController();
        PageReference pagina = Page.Enterprise_profile;
        Test.setCurrentPage(pagina);
        ApexPages.currentPage().getParameters().put('MDRM_Z019','0000');
        ctrl.buscarMiModelorama();
        List<MDRM_Businessman_Expansor__c> exist = ctrl.listaModeloramasExpansor;
        System.assert(exist != null);
    }
    
    @isTest
    static void changeEditableTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.changeEditable();
        Boolean isEditable = ctrl.isEditable;
        System.assertEquals(isEditable, false);
    }
    
    @isTest
    static void newMdrmTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.newMdrm();
        Boolean isNewMdrm = ctrl.isNewMdrm;
        System.assertEquals(isNewMdrm, false);
    }
    
    @isTest
    static void isMineMdrmTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.isMineMdrm();
        Boolean miModelorama = ctrl.miModelorama;
        System.assertEquals(miModelorama, false);
    }
    
    @isTest
    static void isMineEmployeeTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.isMineEmployee();
        Boolean miEmpleado = ctrl.miEmpleado;
        System.assertEquals(miEmpleado, false);
    }
    
    @isTest
    static void mostrarModeloramaTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.mostrarModelorama();
        Boolean mostrarMdrm = ctrl.mostrarMdrm;
        System.assertEquals(mostrarMdrm, false);
    }
    
    @isTest
    static void pagetomdrmTest(){
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.pagetomdrm();
        System.assert(true);
    }
    
    @isTest
    static void employNewTest(){
        //to do
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.parametroModelorama = paramModelorama;
        ctrl.employNew();
        List<Account> verMdrm = ctrl.verModelorama;
        System.assert(!verMdrm.isEmpty());
        
    }
    
    @isTest
    static void pagetoModeloramaTest(){
        
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
        MDRM_Employee__C Empleado = BusinessmanDataFactory.createEmployee(vinculo.Id);
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.parametroModelorama = paramModelorama;
        ctrl.pagetoModelorama();
        List<Account> verMdrm = ctrl.verModelorama;
        System.assert(!verMdrm.isEmpty());
        
            }
     @isTest
     static void deleteMdrmErrorTest()
     { 
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        Account ModeloramasDos = BusinessmanDataFactory.createModeloramaDos();
         
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
         
        List<MDRM_Vinculo__c> todosVinculos = new List<MDRM_Vinculo__c>();
        todosVinculos.add(vinculo);
         
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.parametroModelorama = paramModelorama;
        ctrl.listaModeloramas = todosVinculos;
        ctrl.deleteMdrm();
        String error = ctrl.inputTextErrorLastModelorama;
        System.assert(error.contains(' '));
     }
    
    @isTest
     static void deleteMdrmTest()
     { 
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        Account ModeloramasDos = BusinessmanDataFactory.createModeloramaDos();
         
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
        MDRM_Vinculo__c vinculo2 = BusinessmanDataFactory.createVinculoMdrmEnterpriseDos(Businessman,ModeloramasDos);
         
        List<MDRM_Vinculo__c> todosVinculos = new List<MDRM_Vinculo__c>();
        
        todosVinculos.add(vinculo);
        todosVinculos.add(vinculo2);
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
        ctrl.empresario = Businessman;
		ctrl.parametroModelorama = paramModelorama;
        ctrl.listaModeloramas = todosVinculos;
        ctrl.deleteMdrm();
         
        System.assert(todosVinculos != null);
     }
     
     @isTest
     static void deleteEmpleadoTest()
     {
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
         
        List<MDRM_Vinculo__c> todosVinculos = new List<MDRM_Vinculo__c>();
        todosVinculos.add(vinculo);
         
        MDRM_Employee__c Empleado = BusinessmanDataFactory.createEmployee(vinculo.Id);
        BusinessmanController ctrl = new BusinessmanController();
         
        String paramEmpleado = Empleado.Id;
		ctrl.parametroEmpleado = paramEmpleado;
        ctrl.listaModeloramas = todosVinculos;
        ctrl.deleteEmpleado();
        
        System.assert(ctrl.listaModeloramas != null);

     }
    
     @isTest
     static void savetoNewEmployeeErrorTest()
     { 
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
         
        List<MDRM_Vinculo__c> todosVinculos = new List<MDRM_Vinculo__c>();
        todosVinculos.add(vinculo);
         
        MDRM_Employee__c Empleado = BusinessmanDataFactory.createEmployeeEmptyError(vinculo.Id); 
        BusinessmanController ctrl = new BusinessmanController();
        String paramEmpleado = Empleado.Id;
		ctrl.parametroEmpleado = paramEmpleado;
        ctrl.savetoNewEmployee();
        System.assert(ctrl.validationFields == false);
        
     }
    
    @isTest
     static void savetoNewEmployeeTest()
     { 
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessman(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);  
        MDRM_Employee__c Empleado = BusinessmanDataFactory.createEmployee(vinculo.Id);
         
       List<MDRM_Employee__c> newEmployee = new List<MDRM_Employee__c>();
         
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.parametroModelorama = paramModelorama;
        ctrl.employee = Empleado;
        newEmployee.add(Empleado);
        ctrl.empleados = newEmployee;
        ctrl.savetoNewEmployee();
        System.assert(ctrl.validationFields == true);
		
     }
    
     @isTest
     static void pagetoEmployTest()
     {
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
         
        MDRM_Employee__c Empleado = BusinessmanDataFactory.createEmployee(vinculo.Id);
         
        BusinessmanController ctrl = new BusinessmanController();
        String paramEmpleado = Empleado.Id;
		ctrl.parametroEmpleado = paramEmpleado;
        ctrl.pagetoEmploy();
        List<MDRM_Employee__c> verEmpleado = ctrl.empleados;
        System.assert(!verEmpleado.isEmpty());
         
     }
    
    @isTest
    static void saveEmployeeErrorTest()
    {
        MDRM_Employee__c Empleado = BusinessmanDataFactory.employeeEmpty();
        
        List<MDRM_Employee__c> empleadosToSave = new List<MDRM_Employee__c>();
        empleadosToSave.add(Empleado);
        BusinessmanController ctrl = new BusinessmanController();
		ctrl.empleados = empleadosToSave;
        ctrl.saveEmployee();
        
        System.assert(ctrl.validationFields == false);
    }
    
     @isTest
    static void saveEmployeeTest()
    {
        
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        MDRM_Vinculo__c vinculo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
        
        MDRM_Employee__c Empleado = BusinessmanDataFactory.createEmployee(vinculo.Id);
        List<MDRM_Employee__c> empleadosToSave = new List<MDRM_Employee__c>();
        empleadosToSave.add(Empleado);
        empleadosToSave[0].Name='Updated Name';
        BusinessmanController ctrl = new BusinessmanController();
		ctrl.empleados = empleadosToSave;
        ctrl.saveEmployee();
        MDRM_Employee__c updateEmployee = [SELECT Name FROM MDRM_Employee__c WHERE Name = 'Updated Name'];
        System.assert(updateEmployee != null);
        
        
    }
    
    @isTest
    static void editProfileErrorEmptyValuesTest()
    {
        Account Businessman = BusinessmanDataFactory.createBusinessmanEmpty();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessmanEmpty(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        
        List<Contact> contactBusinessman = new List<Contact>(); 
        List<MDRM_Form__c> userBusinessman = new List<MDRM_Form__c>();
        MDRM_AccountWrapped accWrap = new MDRM_AccountWrapped();
        
        Account empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                                 ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                                 ONTAP__Email__c,
                      	  (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                          FROM Account 
                          WHERE Id =: Businessman.Id];
        
           contactBusinessman = empresario.Contacts;
           userBusinessman = empresario.Forms__r;
        
        accWrap = BusinessmanDataFactory.wrapBusinessman(empresario);
       
        BusinessmanController ctrl = new BusinessmanController();
        
        ctrl.accWrap = accWrap;
        contactBusinessman[0].Phone = accWrap.ContactsPhone;
        userBusinessman[0].MDRM_Facebook_Account__c = accWrap.FormsFacebookAccount;
        ctrl.editProfile();
        System.assert(ctrl.validationFields == false);
    }
    
    @isTest
    static void editProfileErrorIncorrectNumbersTest()
    {
        Account Businessman = BusinessmanDataFactory.createBusinessmanIncorrectNumbers();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessmanNumbers(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        
        List<Contact> contactBusinessman = new List<Contact>(); 
        List<MDRM_Form__c> userBusinessman = new List<MDRM_Form__c>();
        MDRM_AccountWrapped accWrap = new MDRM_AccountWrapped();
        
        Account empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                                 ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                                 ONTAP__Email__c,
                      	  (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                          FROM Account 
                          WHERE Id =: Businessman.Id];
        
           contactBusinessman = empresario.Contacts;
           userBusinessman = empresario.Forms__r;
        
        accWrap = BusinessmanDataFactory.WrapBusinessman(empresario);
       
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.accWrap = accWrap;
        ctrl.contactBusinessman = contactBusinessman;
        ctrl.userBusinessman = userBusinessman;
        ctrl.editProfile();
        
        System.assert(ctrl.validationFields == false);
    }
    
    @isTest
    static void editProfileTest()
    {
        
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessman(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        
        List<Contact> contactBusinessman = new List<Contact>();
        List<MDRM_Form__c> userBusinessman = new List<MDRM_Form__c>();
        MDRM_AccountWrapped accWrap = new MDRM_AccountWrapped();
        
        Account empresario = [SELECT id, Name, MDRM_LastName__c, ONTAP__PostalCode__c, ONTAP__Colony__c, 
                                 ONTAP__Province__c, ONTAP__Municipality__c, ONTAP__Street__c, ONTAP__Street_Number__c, 
                                 ONTAP__Email__c,
                      	  (SELECT id, Phone, Birthdate, MDRM_Gender__c FROM Contacts),(SELECT id, MDRM_Facebook_Account__c FROM Forms__r)
                          FROM Account 
                          WHERE Id =: Businessman.Id];
        	contactBusinessman = empresario.Contacts;
            userBusinessman = empresario.Forms__r;
        
              
       
        BusinessmanController ctrl = new BusinessmanController();
        empresario.ONTAP__PostalCode__c = '54321';
        accWrap = BusinessmanDataFactory.WrapBusinessman(empresario); 
        ctrl.accWrap = accWrap;
        //ctrl.empresario = empresario;
        ctrl.contactBusinessman = contactBusinessman;
        ctrl.userBusinessman = userBusinessman;
        ctrl.editProfile();
        Account updateEmpresario = [SELECT ONTAP__PostalCode__c FROM Account WHERE ONTAP__PostalCode__c='54321'];
        System.assert(updateEmpresario != null);
    }
    
    @isTest
    static void modeloramaSAPErrorTest()
    {
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55344';
		ctrl.sapid = paramModelorama;
        ctrl.modeloramaSAP();
        String error = ctrl.inputTextErrorMessage;
        System.assert(error.contains(' '));
    }
    
    @isTest
    static void modeloramaSAPTest()
    {
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.sapid = paramModelorama;
        ctrl.modeloramaSAP();
        List<Account> verMdrm = ctrl.model;
        System.assert(!verMdrm.isEmpty());
    }
    
    @isTest
    static void modeloramaSAPnullTest()
    {
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '';
		ctrl.sapid = paramModelorama;
        ctrl.modeloramaSAP();
        String error = ctrl.inputTextErrorMessage;
        System.assert(error.contains(' '));
    }
    
    @isTest
    static void savetoEnterpriseTest()
    {
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
        ctrl.empresario = Businessman;
		ctrl.sapid = paramModelorama;
        ctrl.savetoEnterprise();
        MDRM_Vinculo__c nuevoVinculo = [SELECT Name, MDRM_Expansor__r.z019__c FROM MDRM_Vinculo__c WHERE MDRM_Expansor__r.z019__c='55555'];
        
        System.assert(nuevoVinculo != null);
        
    }
    
    @isTest
    static void savetoEnterpriseBussyTest()
    {
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModeloramaBussy();
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
        ctrl.empresario = Businessman;
		ctrl.sapid = paramModelorama;
        ctrl.savetoEnterprise();
        String error = ctrl.inputTextErrorMessageBussy;
        System.assert(error.contains(' '));
    }
    
    @isTest
    static void backPageEnterpriseProfileTest()
    {	      
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        Account Modeloramas = BusinessmanDataFactory.createModelorama();
        Contact BusinessmanContact = BusinessmanDataFactory.contactBusinessmanEmpty(Businessman.Id); 
        MDRM_Form__c BusinessmanUser = BusinessmanDataFactory.businessmanFacebookUser(Businessman.Id);
        MDRM_Vinculo__c vincuo = BusinessmanDataFactory.createVinculoMdrmEnterprise(Businessman,Modeloramas);
        
        BusinessmanController ctrl = new BusinessmanController();
        String paramModelorama = '55555';
		ctrl.codigoModelorama = paramModelorama;
        ctrl.backPageEnterpriseProfile();
        List<MDRM_Vinculo__c> exist = ctrl.empresarioConModeloramas;
        System.assert(!exist.isEmpty());
    }
    
    @isTest
    static void EnterpriseProfileTest()
    {
        PageReference pagina = Page.Enterprise_profile;
        Test.setCurrentPage(pagina);
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.EnterpriseProfile();
        List<MDRM_Vinculo__c> exist = ctrl.empresarioConModeloramas;
        System.assert(exist == null);
    }
    
    @isTest
    static void pageSurveyTest()
    {
        Account Businessman = BusinessmanDataFactory.createBusinessman();
        List<MDRM_form__c> Survey = BusinessmanDataFactory.createSurvey(Businessman.Id);
        MDRM_form__c encuesta = [SELECT Id FROM MDRM_form__c WHERE MDRM_Marital_status__c = 'Married'];
        System.debug('Encuesta 1: ' + encuesta);
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.empresario = Businessman;
        ctrl.pageSurvey();
        System.assert(Survey != null);
    }
    
    @isTest
    static void updateSurveyErrorTest()
    {
		Account Businessman = BusinessmanDataFactory.createBusinessman();
        List<MDRM_form__c> Survey = BusinessmanDataFactory.createSurveyError();
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.empresario = Businessman;
        ctrl.survey = Survey;
        ctrl.updateSurvey();
        System.assert(Survey != null);        
    }
    
     @isTest
    static void updateSurveyTest()
    {
		Account Businessman = BusinessmanDataFactory.createBusinessman();
        List<MDRM_form__c> Survey = BusinessmanDataFactory.createSurvey(Businessman.Id);
        
        BusinessmanController ctrl = new BusinessmanController();
        ctrl.empresario = Businessman;
        ctrl.survey = Survey;
        ctrl.updateSurvey();
        System.assert(Survey != null);        
    }
    
    
}