/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
	Class that allows to deserialize the generated JSON of the service Material Availability

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       25/07/2017	  Luis Licona                  CLass created
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_DeserializeMaterialAvailability_tst {
	
	static testMethod void testParse() {
		String json = '{'+
		'  \"materials\" : [ {'+
		'    \"unit\" : \"CS\",'+
		'    \"salesOffice\" : \"FG00\",'+
		'    \"productId\" : \"3000543\"'+
		'  }, {'+
		'    \"unit\" : \"CS\",'+
		'    \"salesOffice\" : \"FG00\",'+
		'    \"productId\" : \"3000543\"'+
		'  } ]'+
		'}';
		ISSM_DeserializeMaterialAvailability_cls obj = ISSM_DeserializeMaterialAvailability_cls.parse(json);
		System.assert(obj != null);
	}
}