/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Andrea Cedillo
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Test class for schedule class ISSM_ChangeStatusComboActive_sch
*
* No.       Fecha              Autor                      Descripción
* 1.0    16-Noviembre-2018      Andrea Cedillo                   Creación
*******************************************************************************/
@isTest
private class ISSM_ChangeStatusComboActive_tst{
    
    static testmethod void testsScheduleIt() {
        Test.startTest();
        	ISSM_ChangeStatusComboActive_sch.scheduleIt(); 
        Test.stopTest();
    }
    
    static testmethod void testEjecuteFlow() {
        Test.startTest();
            String jobId = System.schedule(Label.TRM_NameSchActiveCmb + System.now(),ISSM_ChangeStatusComboActive_sch.CRON_EXPR,new ISSM_ChangeStatusComboActive_sch());
            System.debug('jobId--> '+jobId);
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(ISSM_ChangeStatusComboActive_sch.CRON_EXPR, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}