/****************************************************************************************************
    General Information
    -------------------
    Author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Class to calculate the new Credit Amount in the Account when the Open Items related
    						to the Client has changes.
		Events for the Trigger: after insert, after update, after undelete, before delete.

    Related classes: ISSM_OnCallQueries_cls, OpenItemCreditAmount_thr, ISSM_TriggerManager_cls
    Related triggers: OpenItem_tgr (currently inactive)

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       10-April-2018          Marco Zúñiga                Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
private class OpenItemCreditAmount_tst {
	static ISSM_OnCallQueries_cls CTRL = new ISSM_OnCallQueries_cls();
	static Id recTypeIdAcc = CTRL.getRecordTypeId('Account','Account');
	static Id recTypeIdOI  = CTRL.getRecordTypeId('ONTAP__OpenItem__c','ONTAP_Compact');
	static Account ObjAcc              = new Account();
	static ONTAP__OpenItem__c ObjOI    = new ONTAP__OpenItem__c();
	static ONTAP__OpenItem__c[] OI_lst = new ONTAP__OpenItem__c[]{};

	@testSetup static void createData1() {
		
		ObjAcc.Name                    = 'Account 1';
		ObjAcc.ISSM_CreditLimit__c     = 1000;
		ObjAcc.ONTAP__SAP_Number__c    = '0123456789';
		ObjAcc.RecordTypeId            = recTypeIdAcc;
		ObjAcc.ONTAP__Credit_Amount__c = 10;
		insert ObjAcc;

		ObjOI.ONTAP__Account__c        = ObjAcc.Id;
		ObjOI.ONTAP__SAPCustomerId__c  = ObjAcc.ONTAP__SAP_Number__c;
		ObjOI.ISSM_Debit_Credit__c     = Label.ISSM_S;
		ObjOI.RecordTypeId             = recTypeIdOI;
		ObjOI.ONTAP__DueDate__c        = Date.today() + 1;
		ObjOI.ONTAP__Amount__c         = 100;
		insert ObjOI;
	}

	@testSetup static void createData2() {

		ObjAcc.Name                    = 'Account 1';
		ObjAcc.ISSM_CreditLimit__c     = 1000;
		ObjAcc.ONTAP__SAP_Number__c    = '0123456789';
		ObjAcc.RecordTypeId            = recTypeIdAcc;
		ObjAcc.ONTAP__Credit_Amount__c = 10;
		insert ObjAcc;

		for(Integer i = 0; i < 10; i++){
			ONTAP__OpenItem__c ObjOI      = new ONTAP__OpenItem__c();
			ObjOI.ONTAP__Account__c       = ObjAcc.Id;
			ObjOI.ONTAP__SAPCustomerId__c = ObjAcc.ONTAP__SAP_Number__c;
			ObjOI.ISSM_Debit_Credit__c    = Label.ISSM_H;
			ObjOI.RecordTypeId            = recTypeIdOI;
			ObjOI.ONTAP__DueDate__c       = Date.today() + 1;
			ObjOI.ONTAP__Amount__c        = 100*Math.random();	
			OI_lst.add(ObjOI);
		}
	}

	@isTest static void updateOIAmount_tst(){
		Test.startTest();
			createData1();
			System.assertEquals(100,ObjOI.ONTAP__Amount__c,'Initial value for ONTAP__Amount__c field is changed.');
			ObjOI.ONTAP__Amount__c = 200;
			update ObjOI;

			System.assertEquals(200,ObjOI.ONTAP__Amount__c,'The value for ONTAP__Amount__c was not updated.');	
		Test.stopTest();
	}

	@isTest static void upsertDeleteOIAmount_tst(){
		Test.startTest();
			System.assert(OI_lst.size() == 0 ,'There are at least 1 Open Item(s) already inserted.');
			createData2();
			upsert OI_lst;
			System.assert(OI_lst.size() > 0 ,'There are not values of Open Items to modify.');

			for(ONTAP__OpenItem__c OI_obj : OI_lst){
				OI_obj.ONTAP__Amount__c = 200;
			}
			update OI_lst;
			System.assertEquals(200,OI_lst.get(3).ONTAP__Amount__c,'The value for ONTAP__Amount__c was not updated.');

			delete OI_lst;
			System.assertNotEquals(0,OI_lst.size(),'There are at least 1 Open Item(s) already inserted.');

		Test.stopTest();
	}

	@isTest static void queryTest_tst(){
		Test.startTest();
			System.assert(OI_lst.size() == 0 ,'There are at least 1 Open Item(s) already inserted.');
			createData2();
			upsert OI_lst;
			System.assert(OI_lst.size() > 0 ,'There are not values of Open Items to modify.');
			
			List<Account> Acc_lst = [SELECT Id, RecordTypeId FROM Account WHERE Name = 'Account 1' LIMIT 1];

			Set<String> AccId_Set = new Set<String>();
			AccId_Set.add(String.valueOf(Acc_lst[0].Id));		
			System.assert(AccId_Set.size() == 1,'There are not Accounts to be query');	

			List<ONTAP__OpenItem__c> OITest1_lst = CTRL.getOpenItemPerSetD(OI_lst,AccId_Set,recTypeIdOI);
			System.assertNotEquals(0,OI_lst.size(),'There are at least 1 Open Item(s) already inserted.');

			List<ONTAP__OpenItem__c> OITest2_lst= CTRL.getOpenItemPerSet(AccId_Set,recTypeIdOI);
			System.assertNotEquals(0,OI_lst.size(),'There are at least 1 Open Item(s) already inserted.');
		Test.stopTest();
	}


}