/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_FinishOrderCreatePDF_tst {
	static ONTAP__Order__c ObjOrder2{get;set;}
	static Account ObjAccount{get;set;}//Cuentas
    static ONCALL__Call__c ObjCall{get;set;}//Llamadas
    static ONTAP__Order_Item__c ObjOI{get;set;}
    static ONTAP__Product__c ObjProduct{get;set;}
    static ONTAP__Product__c ObjProduct2{get;set;}
    static ONTAP__Product__c ObjProduct3{get;set;}
    static ISSM_ProductByOrg__c ObjProductbyOrg{get;set;}
    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
    
	@isTest static void TestFinishOrderCreatePDF(){
		  insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = 'http://A',
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


		Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
		Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
		
		 ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;
         ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;
        
		ObjOrder2 = new ONTAP__Order__c(
     		ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today(),
            ONCALL__SAP_Order_Number__c = '123443',
            ISSM_Subtotal__c = 300.90,
            ISSM_TotalDiscount__c = 33.00
          
        );
        insert ObjOrder2;
         
        
        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;
        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ISSM_Empties_Material__c    = ObjProduct2.Id,
            ISSM_BoxRack__c             = ObjProduct3.Id

        );
        insert ObjProduct;
        
        ObjProductbyOrg  =  new  ISSM_ProductByOrg__c(
        	ISSM_AssociatedProduct__c  = ObjProduct.Id,
            ISSM_RelatedAccount__c  = ObjAccount.Id
            
        );
        insert ObjProductbyOrg;
        
     	ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_ComboNumber__c = '8768:8768',
            ISSM_UnitPrice__c = Decimal.valueOf('288.902'),
            ISSM_OrderItemSKU__c = '12323233',
            ISSM_MaterialProduct__c = 'EJEMPLO PDF',
            ISSM_TotalAmount__c  = 300.902,
            ISSM_NoDiscountTotalAmount__c = 30.902,
            ISSM_RelatedProduct__c = ObjProductbyOrg.Id
     
            
        );
        insert ObjOI;
        
		pageReference pdfPage = Page.ISSM_OncallCreateOrderPDF_pag;
		Test.setCurrentPage(pdfPage);
		pdfPage.getParameters().put('IdOrder',ObjOrder2.Id);

		ApexPages.StandardController sc = new ApexPages.StandardController(ObjOrder2);
		ISSM_FinishOrderCreatePDF_cls testAccPlan = new ISSM_FinishOrderCreatePDF_cls();	
        
    }
}