/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for schedule class TRM_ZMIWInSettlementPeriod_sch
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_ZMIWInSettlementPeriod_tst {
	
	@testSetup static void loadData() {		
		// insert TRM_SettlementPeriods__c
		TRM_SettlementPeriods__c settlementPeriods 		= new TRM_SettlementPeriods__c();
			settlementPeriods.TRM_ConditionClas__c		= 'ZMIW';
			settlementPeriods.TRM_Description__c		= 'Período de Liquidación TEST';
			settlementPeriods.TRM_EffectiveDate__c		= System.now().Date();
			settlementPeriods.TRM_EndTime__c 			= '23:45:00';
			settlementPeriods.TRM_RecordVolume__c 		= 10000;
			settlementPeriods.TRM_StartTime__c			= '23:30:00';
        Insert settlementPeriods;

        //insert TRM_ConditionClass__c
		TRM_ConditionClass__c  conditionClass1 			= new TRM_ConditionClass__c();
			conditionClass1.TRM_StartDate__c			= System.now().Date().addDays(2);
			conditionClass1.TRM_EndDate__c				= System.now().Date().addDays(5);
			conditionClass1.TRM_ConditionClass__c		= 'ZMIW';
			conditionClass1.TRM_AccessSequence__c		= '974';
			conditionClass1.TRM_UserRole__c				= 'DRVCentroSur';
			conditionClass1.TRM_StatePerZone__c			= 'PriceZone-13';
			conditionClass1.TRM_Status__c				= 'TRM_Approved';
			conditionClass1.TRM_ConditionUnit__c		= 'MXN';
			conditionClass1.TRM_Description__c			= 'Prueba 01 - ZMIW 974';
			conditionClass1.TRM_MarkedLiquidation__c	= true;
			conditionClass1.TRM_Segment__c				= 'Segmento-42';
			conditionClass1.TRM_DistributionChannel__c	= '01';
		insert conditionClass1;
		TRM_ConditionClass__c  conditionClass2 			= new TRM_ConditionClass__c();
			conditionClass2.TRM_StartDate__c			= System.now().Date().addDays(1);
			conditionClass2.TRM_EndDate__c				= System.now().Date().addDays(2);
			conditionClass2.TRM_ConditionClass__c		= 'ZMIW';
			conditionClass2.TRM_AccessSequence__c		= '974';
			conditionClass2.TRM_UserRole__c				= 'DRVCentroSur';
			conditionClass2.TRM_StatePerZone__c			= 'PriceZone-13';
			conditionClass2.TRM_Status__c				= 'TRM_Cancelled';
			conditionClass2.TRM_ConditionUnit__c		= 'MXN';
			conditionClass2.TRM_Description__c			= 'Prueba 02 - ZMIW 974';
			conditionClass2.TRM_MarkedLiquidation__c	= true;
			conditionClass2.TRM_Segment__c				= 'Segmento-40';
			conditionClass2.TRM_DistributionChannel__c	= '01';
		insert conditionClass2;
    }
	
	@isTest static void tstExecuteSchedule() {		
		 Test.startTest();
			List<TRM_SettlementPeriods__c> lstSettlementPeriod = [SELECT TRM_ConditionClas__c
																		,TRM_Description__c
																		,TRM_EffectiveDate__c
																		,TRM_EndTime__c
																		,TRM_RecordVolume__c
																		,TRM_StartTime__c
																  FROM TRM_SettlementPeriods__c];
			TRM_ZMIWInSettlementPeriod_sch.executeSchedule(lstSettlementPeriod);
			
        	// NO se incluyen 'assert' debido a que los metodos son 'void'

        Test.stopTest();
	}
	
}