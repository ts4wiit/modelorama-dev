/**********************************************************************************************************
General information
-------------------
author:     Marco Zúñiga
email:      mzuniga@avanxo.com
company:    Avanxo México
Project:    Salesforce Implementation
Customer:   Grupo Modelo

Description:
Test Class of ISSMOnCall_OrderList_ctr controller to get the last 8 Order of a POC

Classes to Test:
ISSM_OrderList_ctr

Information about changes (versions)
=======================================================================================================
Number    Dates           Author                       Description                      Code Coverage
------    --------        --------------------------   ------------------------------   ---------------
1.0       14-Junio-2017   Marco ZúñigasCretion of the  Apex class                       100 %
=======================================================================================================
**********************************************************************************************************/
@isTest
private class ISSM_OrderListAccount_tst {
    
    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Product__c ObjProduct{get;set;}
    static Account ObjAccount{get;set;}
    static ONCALL__Call__c ObjCall{get;set;}  
    static ApexPages.StandardController sc{get;set;}
    static ApexPages.StandardController sc2{get;set;}

    public static void StartValues(){

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true
            );
        insert ObjAccount;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            ISSM_ValidateOrder__c = true
            );
        insert ObjCall;
        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = 'Open',
            ONCALL__Origin__c= 'Telesales',
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;

        sc  = new ApexPages.standardController(ObjCall);
        sc2 = new ApexPages.standardController(ObjAccount);

    }  
    
    @isTest static void testISSM_OrderListAccount_ctr() {
        StartValues();
        Test.startTest();        
            PageReference pageRef = Page.ISSM_OrderListAccount_pag;            
            Test.setCurrentPage(pageRef);         
            ISSM_OrderListAccount_ctr OLObj = new ISSM_OrderListAccount_ctr(sc2);      
        
            System.assertNotEquals(null, sc2.getId());    
            System.assertNotEquals(null, OLObj.OrderList);
        Test.stopTest();
    }  
    
}