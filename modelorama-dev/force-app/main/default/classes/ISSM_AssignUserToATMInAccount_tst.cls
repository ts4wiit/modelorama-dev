/**************************************************************************************
Nombre de la clase: ISSM_AssignUserToATMInAccount_tst
Versión : 1.0
Fecha de Creación : 09 Mayo 2018
Funcionalidad : Clase de prueba del batch ISSM_AssignUserToATMInAccount_bch
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        09 - Mayo - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_AssignUserToATMInAccount_tst {
    static testmethod void principalMethod() {
        List<RecordType> RecordTypeSO_lst = new List<RecordType>();
        Id RecordTypeSOId_id;

        List<RecordType> RecordTypeAcc_lst = new List<RecordType>();
        Id RecordTypeAccId_id;

        Profile profile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'];
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
        insert user;

        // Obtenemos el Record Type "SalesOffice"
        RecordTypeSO_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'SalesOffice' LIMIT 1];
        for (RecordType rt : RecordTypeSO_lst) { RecordTypeSOId_id = rt.Id; }

        Account salesOffice1 = new Account();
        salesOffice1.Name = 'Sales Office 1';
        salesOffice1.RecordTypeId = RecordTypeSOId_id;
        insert salesOffice1;

        // Obtenemos el Record Type "Account"
        RecordTypeAcc_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Account' LIMIT 1];
        for (RecordType rt : RecordTypeAcc_lst) { RecordTypeAccId_id = rt.Id; }

        Account account1 = new Account();
        account1.Name = 'Account 1';
        account1.ISSM_SalesOffice__c = salesOffice1.Id;
        account1.RecordTypeId = RecordTypeAccId_id;
        insert account1;

        Test.startTest();

        ISSM_AssignUserToATMInAccount_bch objAlta = new ISSM_AssignUserToATMInAccount_bch(user.Id, salesOffice1.Id, 'Supervisor', 'Alta');
        DataBase.executeBatch(objAlta);

        ISSM_AssignUserToATMInAccount_bch objBaja = new ISSM_AssignUserToATMInAccount_bch(user.Id, salesOffice1.Id, 'Supervisor', 'Baja');
        DataBase.executeBatch(objBaja);

        ISSM_AssignUserToATMInAccount_bch objCambio = new ISSM_AssignUserToATMInAccount_bch(user.Id, salesOffice1.Id, 'Gerente', 'Cambio');
        DataBase.executeBatch(objCambio);

        Test.stopTest();
    }
}