/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  Realiza Update en el objeto CUSTOM de caso  ONTAP__Case_Force__c
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public class ISSM_UpdateCaseInCaseForceHandler_cls {
    ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();

/**
    * Descripcion :  Metodo que realiza el update al actualizar el status en el objeto standard del caso
    * @param  Trigger.oldMap, Trigger.newMap
    * @return none
**/
    
    public void AssignAddressCaseInCaseForce(List<Case> dataCase_lst) {
        // Variables
        String addressCAM_str = '';
        Set<String> caseNumber_set = new Set<String>();
        List<ONTAP__Case_Force__c> caseForce_lst = new List<ONTAP__Case_Force__c>();
        List<ONTAP__Case_Force__c> caseForceUpd_lst = new List<ONTAP__Case_Force__c>();
        List<Case> case_lst = new List<Case>();
        
        for (Case reg : dataCase_lst) {
            // Agregamos los números de casos en un set
            caseNumber_set.add(reg.CaseNumber);
        }
        
        if (caseNumber_set.size() > 0) {
            // Buscamos los casos force relacionados al número de caso del caso
            caseForce_lst = [SELECT Id, ISSM_CaseNumber__c, ISSM_CAM_Delivery_address__c FROM ONTAP__Case_Force__c WHERE ISSM_CaseNumber__c IN: caseNumber_set];
        }
        
        if (caseForce_lst.size() > 0) {
            // Buscamos los casos relacionados a los números de casos guardados previamente en un set
            case_lst = [SELECT Id, CaseNumber, ISSM_LocalStreet__c, ISSM_LocalNumber__c, ISSM_LocalColony__c, ISSM_LocalMunicpality__c, ISSM_LocalCity__c, ISSM_LocalState__c FROM Case WHERE CaseNumber IN: caseNumber_set];
            
            if (case_lst.size() > 0) {
                for (ONTAP__Case_Force__c caseForceLst : caseForce_lst) {
                    for (Case caseLst : case_lst) {
                        // Concatenamos la dirección del caso en un campo String
                        addressCAM_str = caseLst.ISSM_LocalStreet__c + ' '+ caseLst.ISSM_LocalNumber__c + ' '+ caseLst.ISSM_LocalColony__c + ' '+ caseLst.ISSM_LocalMunicpality__c + ' '+ caseLst.ISSM_LocalCity__c + ' '+ caseLst.ISSM_LocalState__c;
                    }
                    
                    // Asignamos la dirección concatenada en la dirección de los casos force
                    caseForceLst.ISSM_CAM_Delivery_address__c = addressCAM_str;
                    caseForceUpd_lst.add(caseForceLst);
                }
            }
        }
        
        if (caseForceUpd_lst.size() > 0) {
            // Actualizamos los casos force con su dirección obtenida del caso estándar
            update caseForceUpd_lst;
        }
    }
    
    public void UpdateCaseInCaseForce(Map<Id, Case> leadOldMap, Map<Id, Case> LeadNewMap ) {
        System.debug('#####ISSM_UpdateCaseInCaseForceHandler_cls -- UpdateCaseInCaseForce');
        String idSolucion = '';
        Boolean blnCreateCaseSolution= false;
        //Configuracion personalizada
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
        //List for make update outside principal for//
        List<ONTAP__Case_Force__c> LstCaseForce = new List<ONTAP__Case_Force__c>();
        //Case force query for make update on the truly force case//
        Set<Id> stid = new Set<Id>();
        Map<Id, ONTAP__Case_Force__c> MapCaseForce = new Map<Id, ONTAP__Case_Force__c>();
        for(Case ObjCaseData : LeadNewMap.values()){
            stid.adD(ObjCaseData.ISSM_CaseForceNumber__c);
        }
        List<ONTAP__Case_Force__c> LstOnTapCaseForce = new List<ONTAP__Case_Force__c>();
        LstOnTapCaseForce = objCSQuerys.QueryCaseForce(stid); 
        for(ONTAP__Case_Force__c oCaseForce : LstOnTapCaseForce)
            MapCaseForce.put(oCaseForce.Id,oCaseForce);

        List<ISSM_MappingFieldCase__c> lstMappingFields = objCSQuerys.QueryMappingFieldCase();  
        
        for(Case ObjCaseData : LeadNewMap.values()){
            //Valida que el campo status sea diferentes para poder realizar el update en el objeto ONTAP__Case_Force__c
             //if (ObjCaseData.Status != null && ObjCaseData.Status != leadOldMap.get(ObjCaseData.id).Status && ObjCaseData.ISSM_CaseForceNumber__c != null){
                for(ISSM_MappingFieldCase__c objMappingFields :lstMappingFields){
                    if(objMappingFields.ISSM_APICaseForce__c != System.label.ISSM_APICaseForceMappingField)//ISSM_APICaseForceMappingField :'ISSM_CaseNumber__c'
                    objCaseForce.put(objMappingFields.ISSM_APICaseForce__c,ObjCaseData.get(objMappingFields.Name));
                }
                if(MapCaseForce.containsKey(ObjCaseData.ISSM_CaseForceNumber__c)){
                    objCaseForce.Id = MapCaseForce.get(ObjCaseData.ISSM_CaseForceNumber__c).Id;
                    System.debug('####LstCaseForce objCaseForce'+objCaseForce);
                    LstCaseForce.add( objCaseForce );
                    objCaseForce = new ONTAP__Case_Force__c();
                    System.debug('####LstCaseForce HMDH'+LstCaseForce);
                }
            if(ObjCaseData.Status == System.label.ISSM_CaseStatusClosed && ObjCaseData.ISSM_ReOpenCase__c == true ){
                blnCreateCaseSolution = true;
            }

            //INSERTA EN Solution Y AGREGA EL VALOR DE LOS CAMPOS ESTANDARD ISSM_SolutionName__c Y  ISSM_SolutionNote__c

            if(ObjCaseData.Status ==  System.label.ISSM_CaseStatusClosed && ObjCaseData.ISSM_ReOpenCase__c == true && blnCreateCaseSolution){
                Solution objSolution = new Solution (
                    SolutionName = ObjCaseData.ISSM_SolutionName__c,
                    SolutionNote = ObjCaseData.ISSM_SolutionNote__c,
                    Status = System.label.ISSM_SolutionStatus
                );
                ObjCaseData.ISSM_CaseReason__c = ObjCaseData.ISSM_SolutionName__c;
                ObjCaseData.ISSM_InterncalComments__c = ObjCaseData.ISSM_SolutionNote__c;
                ObjCaseData.ISSM_SolutionName__c = '';
                ObjCaseData.ISSM_SolutionNote__c = '';
                if(objSolution.SolutionName!=null && objSolution.SolutionNote!=null){
                    insert objSolution;
                    idSolucion =  objSolution.Id;
                    blnCreateCaseSolution = true;
                }else{
                    blnCreateCaseSolution = false;
                    //ObjCaseData.addError('El Campo SolutionName y  SolutionNote son obligatorios');  Daniel Daza 15/07/2017
                }
            }
            //INSERTA EN CaseSolution Y RELACIONA LA SOLUCION objSolution.Id
            if(blnCreateCaseSolution ==  true){
                    CaseSolution objCaseSolution = new CaseSolution(
                        SolutionId = idSolucion,
                        CaseId = ObjCaseData.Id
                    );
                    insert objCaseSolution;
            }

         }
         update LstCaseForce;
    }

    public void InsertCaseInCaseForce(List<Case> LstCaseInsert ) {
        System.debug('ISSM_UpdateCaseInCaseForceHandler_cls---InsertCaseInCaseForce');
        Map<String, String> MapIdCasForce = new Map<String, String>();
        Map<String, String> MapIdCasForceNUmber = new Map<String, String>();

        List<Case> LstAfterUp = new List<Case>();
        List<ONTAP__Case_Force__c> LstForceCases = new List<ONTAP__Case_Force__c>();
        List<ISSM_MappingFieldCase__c> lstMappingFields = objCSQuerys.QueryMappingFieldCase();  
        for(Case ObjCaseData : LstCaseInsert){
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c();
            //Valida que el campo status sea diferentes para poder realizar el update en el objeto ONTAP__Case_Force__c
             //if (ObjCaseData.Status != null && ObjCaseData.Status != leadOldMap.get(ObjCaseData.id).Status && ObjCaseData.ISSM_CaseForceNumber__c != null){
                for(ISSM_MappingFieldCase__c objMappingFields :lstMappingFields){
                    objCaseForce.put(objMappingFields.ISSM_APICaseForce__c,ObjCaseData.get(objMappingFields.Name));
               }
               objCaseForce.ISSM_CaseNumber__c = ObjCaseData.CaseNumber;
               MapIdCasForceNUmber.put(objCaseForce.ISSM_CaseNumber__c,ObjCaseData.Id);
               LstForceCases.add(objCaseForce);
        }
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_ONTAPCaseForceTrigger); //Inactivamos el trigger
        insert LstForceCases;
        for(ONTAP__Case_Force__c oCaseForce : LstForceCases){
            MapIdCasForce.put(MapIdCasForceNUmber.get(oCaseForce.ISSM_CaseNumber__c),oCaseForce.Id);
        }
        InserCaseUpdateCaseNumber(MapIdCasForce);
    }

    @future
    Static void InserCaseUpdateCaseNumber(Map<String,String> MapStrIds  ) {
        System.debug('#####FUTURO - ISSM_UpdateCaseInCaseForceHandler_cls---InsertCaseInCaseForce');
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger);
        List<Case> LstCase = new List<Case>();
        for(String StrIdKEy: MapStrIds.keySet()){
            Case Ocase = new Case();
            Ocase.Id = StrIdKEy;
            Ocase.ISSM_CaseForceNumber__c = MapStrIds.get(StrIdKEy);
            LstCase.add(Ocase);
        }
        update LstCase;
    }
}