public with sharing class ISSM_CAM_Decommission_ctr 
{
    public Id currcaseId{get;set;}
    public string caseStatus{get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Set<string> filelines{get;set;}
    public Set<String>  filelinesSuccess{get;set;}
    public List<ISSM_Case_Force_Asset__c> lstDecommission{get;set;}
    List<ISSM_Case_Force_Asset__c> ocaseforceassets = new List<ISSM_Case_Force_Asset__c>();
    
    public ISSM_CAM_Decommission_ctr(ApexPages.StandardController sc){
        currcaseId = sc.getId();
        filelinesSuccess = new Set<string>();
        caseStatus = [SELECT ONTAP__Status__c FROM ONTAP__Case_Force__c WHERE id=:currcaseId LIMIT 1].ONTAP__Status__c;       
    }
    
    public Pagereference ReadFile(){
        /***This function reads the CSV file and display lines. ***/
        try{
            ISSM_Case_Force_Asset__c ocfa = new ISSM_Case_Force_Asset__c();
            List<ISSM_Asset__C> lstAssetxSerialNumber = new List<ISSM_Asset__C>();
            Set<String> filelines = new Set<String>();
            Set<String> filelinesError = new Set<String>();
        
           
            //Convert the uploaded file which is in BLOB format into a string
            nameFile = contentFile.toString();
            for(String objIterateSerialN :  nameFile.splitByCharacterType()){
                filelines.add(objIterateSerialN); 
            }
            
            //Now sepatate every row of the excel file
            if(filelines != null && !filelines.isEmpty()){
                lstAssetxSerialNumber = [Select Id,ISSM_Status_SFDC__c,ISSM_Serial_Number__c from ISSM_Asset__C Where ISSM_Serial_Number__c IN : filelines];    
            }
            
            if(lstAssetxSerialNumber != null && !lstAssetxSerialNumber.isEmpty()){
                for(ISSM_Asset__C objIteraAssets :  lstAssetxSerialNumber){
                     
                    //Valida los que son Maintenance in warehouse y diferentes del mismo  
                    if(objIteraAssets.ISSM_Status_SFDC__c != 'Maintenance in warehouse' && objIteraAssets.ISSM_Status_SFDC__c != 'Validation'){
                        filelinesError.add(objIteraAssets.ISSM_Serial_Number__c);

                    }else{
                        ocfa = new ISSM_Case_Force_Asset__c();
                        ocfa.ISSM_Case_Force__c = currcaseId;
                        ocfa.ISSM_Asset__c = objIteraAssets.Id;
                    
                        ocaseforceassets.add(ocfa);                       
                        filelinesSuccess.add(objIteraAssets.ISSM_Serial_Number__c);
                         
                    }  
                }
            }
            //Muestra en la VF los numeros de serie que son diferente de Maintenance in warehouse
            if(filelinesError !=null && !filelinesError.isEmpty()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Registros con diferente status: '+filelinesError));
            }
            
            //Muestra en la VF numero de registro que se insertaron
             if(filelinesSuccess !=null && !filelinesSuccess.isEmpty()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Insertados correctamente '+ filelinesSuccess.size()));
            }

            //Registros correctos a insertar 
            if(ocaseforceassets !=null && ! ocaseforceassets.isEmpty()){
                List<Database.SaveResult> srList = new List<Database.SaveResult>();
                Database.insert(ocaseforceassets, false);  
            }   
        }
        catch(Exception e){            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured reading the CSV file: '+e.getMessage()));
        }       
         
       // Refresh the content of assets loaded to the case*/
        lstDecommission = [SELECT ISSM_Case_Force__r.Name,  ISSM_Asset__r.Name,  ISSM_Asset__r.ISSM_Serial_number__c  
                           FROM ISSM_Case_Force_Asset__c 
                           WHERE ISSM_Case_Force__c = :currcaseId and ISSM_Asset__r.ISSM_Serial_number__c IN : filelinesSuccess];              
        return null; 
    }
}