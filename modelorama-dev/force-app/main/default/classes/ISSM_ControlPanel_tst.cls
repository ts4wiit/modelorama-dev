/****************************************************************************************************
    General Information
    -------------------------------
    author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    ISSM-OnCall
    Customer:   Grupo Modelo

    Description:
    Test class for apex controller ISSM_ControlPanel_ctr

    Information about changes (versions)
    ================================================================================================
    Number    Dates              Author                       Description               
    ------    --------           --------------------------   --------------------------------------
    1.0       09-November-2017   Marco Zúñiga                 Class Creation            
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_ControlPanel_tst {
    static ApexPages.StandardController sc{get;set;}
    static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    static ONTAP__Product__c ObjProduct{get;set;}
    static ONTAP__Product__c ObjProduct1{get;set;}
    static ONTAP__Product__c ObjProduct2{get;set;}
    static ONTAP__Product__c ObjProduct3{get;set;}
    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Order_Item__c ObjOI{get;set;}
    static ONTAP__Order_Item__c ObjOI1{get;set;}
    static ONTAP__Order_Item__c ObjOI2{get;set;}
    static ONTAP__Order_Item__c ObjOI3{get;set;}

    @isTest static void StandardCTR(){
        Account  ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true,
            ISSM_TradeIniciative__c = 'Off Trade',
            ISSM_TradeIniciativeValue__c = 'Aliados modelo'
        );
        insert ObjAccount;

        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecTypeProd = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000009',
            ONCALL__Material_Name__c    = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '01'
            /*ISSM_Sector__c              = Label.ISSM_Beer*/
        );
        insert ObjProduct;

        ObjProduct1 = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'AGUA 1.5 L',
            ONCALL__Material_Number__c  = '3000005',
            ONCALL__Material_Name__c    = 'AGUA 1.5 L',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = false,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '02'
            /*ISSM_Sector__c              = Label.ISSM_Water*/
        );
        insert ObjProduct1;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'REFRESCO HIDRATANTE /355 ML CT',
            ONCALL__Material_Number__c  = '3000010',
            ONCALL__Material_Name__c    = 'REFRESCO HIDRATANTE /355 ML CT',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '03'
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecTypeProd,
            ONTAP__MaterialProduct__c   = 'REDBULL 200 ML',
            ONCALL__Material_Number__c  = '3000011',
            ONCALL__Material_Name__c    = 'REDBULL 200 ML',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = false,
            ISSM_Discount__c            = 5.0,
            ONTAP__DivisionId__c        = '04'
        );
        insert ObjProduct3;

        ONTAP__Order__c ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c = ObjAccount.Id,
            ONTAP__OrderAccount__c    = ObjAccount.Id,     
            //ONCALL__Origin__c         = Label.ISSM_Televenta, //'B2B-0000',           
            ONTAP__DeliveryDate__c    = System.today()+1,
            ONTAP__BeginDate__c       = System.today(),
            CreatedDate               = System.today()//,
            //ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus
        );
        insert ObjOrder;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = ObjOrder.Id,
            ONCALL__OnCall_Product__c        = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = false,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000009',
            ISSM_EmptyMaterial__c            = '2000005',
            ISSM_EmptyRack__c                = '4000005',
            ISSM_OrderItemSKU__c             = '4000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10
        );
        insert ObjOI;

        ObjOI1 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = ObjOrder.Id,
            ONCALL__OnCall_Product__c        = ObjProduct1.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = false,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_OrderItemSKU__c             = '3000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10
        );
        insert ObjOI1;

        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = ObjOrder.Id,
            ONCALL__OnCall_Product__c        = ObjProduct2.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = false,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_OrderItemSKU__c             = '3000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10
        );
        insert ObjOI2;

        ObjOI3 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c          = ObjOrder.Id,
            ONCALL__OnCall_Product__c        = ObjProduct3.Id,
            ONCALL__OnCall_Quantity__c       = 10,
            ISSM_Is_returnable__c            = true,
            ISSM_IsBonus__c                  = false,
            ISSM_TotalAmount__c              = 100.00,
            ISSM_Uint_Measure_Code__c        = 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_OrderItemSKU__c             = '3000005',
            CreatedDate                      = System.today(),
            ONTAP__ActualQuantity__c         = 10
        );
        insert ObjOI3;

        ONTAP__KPI__c ObjKpi =  new ONTAP__KPI__c(
            ONTAP__Account_id__c = ObjAccount.iD,
            ONTAP__Actual__c = 1,
            ONTAP__Kpi_start_date__c = Date.valueOf(System.now()).toStartOfMonth(),
            ONTAP__Kpi_end_date__c = Date.newinstance(System.now().year(), System.now().month(), Date.daysInMonth(System.now().year(), System.now().month())),
            ONTAP__Kpi_id__c = 3000003,
            ONTAP__KPI_name__c = 'Corona Extra-Bote',
            ONTAP__Kpi_parent_id__c=1,
            ONTAP__Categorie__c = 'Volume',
            ONTAP__Percentage_increase_lastyear__c = 0.1
        );
        insert ObjKpi;

        ISSM_TradeIniciative__c objIT = new ISSM_TradeIniciative__c(
            ISSM_Description__c = 'Aliados modelo',
            ISSM_Valid__c=true,
            ISSM_BeerGap1__c=1,
            ISSM_BeerGap2__c=2,
            ISSM_BeerGap3__c=3
            );
        insert objIT;

        DateTime d = datetime.now();
        String StrMonthName= d.format('MMMMM');
        String StrYear = String.valueOf(d.year());

        String StrRecType = CTRSOQL.getRecordTypeId('ISSM_Goal__c','ISSM_OnCallAccount');
        ISSM_Goal__c objGoal = new ISSM_Goal__c(
            ISSM_EnergyDrinkMonthlyGoal__c=2.2,
            ISSM_SodaMonthlyGoal__c=3.3,
            ISSM_WaterMonthlyGoal__c=4.4,
            ISSM_AccountGoal__c = ObjAccount.Id,
            ISSM_ValidityMonth__c = StrMonthName,
            ISSM_ValidityYear__c = StrYear,
            RecordTypeId = StrRecType
            );
        insert objGoal;


        ONTAP__Account_Asset__c OBJASSET = new ONTAP__Account_Asset__c(
            ONTAP__Account__c = ObjAccount.Id,
            ISSM_AssetType__c = 'R',
            ONTAP__Quantity__c=3
            );
        insert OBJASSET;

        sc  = new ApexPages.standardController(ObjAccount);
        Test.startTest();
        ISSM_ControlPanel_ctr CTR = NEW ISSM_ControlPanel_ctr(sc); 
        CTR.getValidateThisMonth();
        List<ONTAP__Order_Item__c> oi_lst = [SELECT Id FROM ONTAP__Order_Item__c WHERE CreatedDate =: System.today()];
        delete oi_lst;
        CTR.getValidateLastMonth();
        Test.stopTest();
    }
}