public interface ISSM_ICaseSelector_cls {

	List<Case> selectById(Set<ID> idSet);
	Case selectById(ID caseID);
	List<Case> selectById(String caseID);
	List<Case> selectByCaseNumber(Set<String> caseNumber);
	List<Case> selectByCaseForceNumber(List<Id> caseForceNumber);
	Case selectByCaseNumber(String strCaseNumber);
	Map<Id,Case> selectByIdMap(Set<ID> idSet);
}