/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_UpdateRshpsController_ctr". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_UpdateRshpsController_tst {
    static testmethod void testController() {
        Test.startTest();
        System.assertEquals(new List<CDM_Temp_Taxes__c>(),CDM_UpdateRshpsController_ctr.getTaxes(new Set<String> {''}));
        System.assertEquals(new List<CDM_Temp_Interlocutor__c>(),CDM_UpdateRshpsController_ctr.getInterlocutors(new Set<String> {''}, new Set<String> {''}));
        System.assertEquals(new List<MDM_Account__c>(),CDM_UpdateRshpsController_ctr.getMDMAccounts(new List<String> {''}));
        Test.stopTest();
    }
}