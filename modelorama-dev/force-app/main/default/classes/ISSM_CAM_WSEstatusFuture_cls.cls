/**************************************************************************************
Nombre de la clase: ISSM_CAM_WSEstatusFuture_cls
Versión : 1.0
Fecha de Creación : 24 Julio 2018
Funcionalidad : Future apex class to call apex class ISSM_CAM_WSEstatus_cls that is who call the Web Service to send status to SAP
Clase de Prueba: ISSM_CAM_WSEstatus_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        24 - Julio - 2018      Versión Original
* Daniel Rosales         07 - Agosto -2018      Modifcado
**** Se modifica para recibir un mapa de Coolers y su estatus. Se envía el mapa con el estatus 
**** SAP correspondiente al WS que ahora acepta una lista de coolers y sus estatus en lugar de
**** un solo elemento
*************************************************************************************/
public class ISSM_CAM_WSEstatusFuture_cls {
    @future (callout = true)
    public static void callClassWSEstatus(map<String, String> mapEquipoEstatusSFDC) {
        if (mapEquipoEstatusSFDC.size() > 0) 
            ISSM_CAM_WSEstatus_cls.wsEstatus(mapEquipoEstatusSFDC);
    }
}