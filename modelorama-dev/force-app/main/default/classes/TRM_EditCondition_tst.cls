/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for class TRM_EditCondition_ctr
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_EditCondition_tst {
	
	@testSetup static void loadData() {
	//Insert Budget
		TRM_Budget__c budget 				= new TRM_Budget__c();
			budget.TRM_BudgetName__c		= 'budget test 01';
			budget.TRM_StartDate__c			= System.now().Date().addDays(2);
			budget.TRM_EndDate__c			= System.now().Date().addDays(6);
			budget.TRM_Status__c			= 'New';
			budget.TRM_BudgetType__c		= 'National';
			budget.TRM_RestrictedBudget__c	= true;
		insert budget;

	//Insert catalogos MDM_Parameter__c
		MDM_Parameter__c parameter1 = createMDMParameter('Cerveza',true,true,'SectorProd','01','Cerveza','SectorProd-01');
		insert parameter1;
		MDM_Parameter__c parameter2 = createMDMParameter('Media',true,true,'GrupoMateriales1','02','Media','GrupoMateriales1-02');
		insert parameter2;
		MDM_Parameter__c parameter3 = createMDMParameter('24/355 ml.',true,true,'GrupoMateriales2','42','24/355 ml.','GrupoMateriales2-42');
		insert parameter3;
		MDM_Parameter__c parameter4 = createMDMParameter('POR DEFINIR',true,true,'ConditionGroup3','99','POR DEFINIR','GpoConditions3-99');
		insert parameter4;
		MDM_Parameter__c parameter5 = createMDMParameter('Botella Abierta Imag',true,true,'Segmento','40','Botella Abierta Imag','Segmento-40');
		insert parameter5;
		MDM_Parameter__c parameter6 = createMDMParameter('ALTIPLANO',true,true,'PriceZone','01','ALTIPLANO','PriceZone-01');
		insert parameter6;

	//Insert Product
		String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);

		ONTAP__Product__c product1 				= new ONTAP__Product__c();
			product1.ONTAP__ExternalKey__c 		= '000000000003000006';
			product1.ONTAP__MaterialProduct__c 	= 'CORONA EXTRA CLARA 24/355 ML CT R';
			product1.ISSM_SectorCode__c 		= parameter1.id;
			product1.ISSM_Quota__c 				= parameter2.id;
			product1.ISSM_MaterialGroup2__c 	= parameter3.id;
			product1.RecordTypeId 				= RecType;
			product1.ONTAP__ProductType__c 		= 'FERT';
		insert product1;
	// insert customer
		Account customer 						= new Account();
			customer.ISSM_SegmentCode__c 		= parameter5.id;
			customer.ONTAP__SAPCustomerId__c 	= 'FK160100549705';
			customer.ONTAP__SAP_Number__c 		= '0100549705';
			customer.ONTAP__ExternalKey__c 		= '0100549705';
			customer.Name 						= 'TEST';
		insert customer;
	// insert conditionClass
		TRM_ConditionClass__c conditionClass 			= new TRM_ConditionClass__c();
			conditionClass.TRM_StartDate__c				= System.now().Date().addDays(2);				
			conditionClass.TRM_EndDate__c				= System.now().Date().addDays(4);
			conditionClass.TRM_Budget__c				= budget.Id;
			conditionClass.TRM_ConditionClass__c		= 'ZTPM';
			conditionClass.TRM_AccessSequence__c		= '986';
			conditionClass.TRM_StatePerZone__c			= 'PriceZone-01';
			conditionClass.TRM_Status__c				= 'TRM_Pending';
			conditionClass.TRM_ConditionUnit__c			= 'MXN';
			conditionClass.TRM_ConditionGroup3__c		= parameter4.Id;
			conditionClass.TRM_Sector__c				= '00';
			conditionClass.TRM_Description__c			= 'Condition Class ZTPM 986 - class tst | clone';
			conditionClass.TRM_Segment__c				= 'Segmento-40';
			conditionClass.TRM_DistributionChannel__c	= '01';
		insert conditionClass;
	// insert conditionRecord
		TRM_ConditionRecord__c conditionRecord		= new TRM_ConditionRecord__c();
			conditionRecord.TRM_Product__c			= product1.Id;
			conditionRecord.TRM_ConditionClass__c	= conditionClass.Id;
			conditionRecord.TRM_DummyKey__c			= 'CC-0000000443R0';
			conditionRecord.TRM_UnitMeasure__c		= 'MXN';
			conditionRecord.TRM_Segment__c			= parameter5.Id;
			conditionRecord.TRM_AmountPercentage__c	= 234;
			conditionRecord.TRM_StatePerZone__c		= parameter6.Id;
			conditionRecord.TRM_customer__c			= customer.Id;
		insert conditionRecord;
	}
	
	@isTest static void testGetRecordById() {
		Test.startTest();
			TRM_EditCondition_ctr.getRecordById('MDM_Parameter__c', [SELECT Id FROM MDM_Parameter__c WHERE Catalog__c = 'PriceZone' LIMIT 1].Id);
		Test.stopTest();
	}

	@isTest static void testGetConditionMetadata() {
		Test.startTest();
			TRM_EditCondition_ctr.getConditionMetadata('TRM_ConditionsManagement__mdt', 'ZMIW_971', true);
		Test.stopTest();
	}
	@isTest static void testGetCustomerIdList() {
		Test.startTest();
			TRM_EditCondition_ctr.getCustomerIdList([SELECT Id FROM TRM_ConditionClass__c LIMIT 1].Id);
		Test.stopTest();
	}

	@isTest static void testgetCatalogAndStructures() {
		Test.startTest();
			Map<String,String> mapStructures	= TRM_EditCondition_ctr.getStructures();
			Map<String,String> mapCatalogs 		= TRM_EditCondition_ctr.getCatalog();
		Test.stopTest();
	}

	public static MDM_Parameter__c createMDMParameter(String name
													, boolean active
													, boolean activeRevenue
													, String catalog
													, String code
													, String description
													, String externalId){
		MDM_Parameter__c parameter 		= new MDM_Parameter__c();
			parameter.Name				= name;
			parameter.Active__c			= active;
			parameter.Active_Revenue__c = activeRevenue; 
			parameter.Catalog__c 		= catalog;
			parameter.Code__c 			= code;
			parameter.Description__c 	= description;
			parameter.ExternalId__c 	= externalId;
	return parameter;
	}
	
}