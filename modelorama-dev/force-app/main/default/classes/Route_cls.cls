/****************************************************************************************************
    General Information
    -------------------
    author: Joseph Ceron
    email: jceron@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger Class for validate Sales agent and bring its boss

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       04-07-2017        Joseph Ceron (JC)            Creation Class
    1.1       21/08/2017        Daniel Peñaloza              Account record type settings
    1.2       19/04/2017        Rodrigo Resendiz             Change Account owner when the route is deleted/modified
    1.3       30/04/2018        Rodrigo Resendiz             When route is for Telesales add to account team
    1.4       28-10-2018        Leopoldo Ortega              Method AssignManagerToSalesAgent to solution throught Customer Service
****************************************************************************************************/
public class Route_cls {
    public set<Id> StIdAcc ;
    public Map<Id,Map<Id,AccountTeamMember>> MapMpAccMember;
    public Map<Id, AccountTeamMember> MapSupervisores;
    private AccountRouteSettings__mdt accountSettings;
    private static final AccountRouteSettings__mdt accountSettingsBDR = DevUtils_cls.getAccountRouteSettings('BDR');//RR

    public Route_cls() {
        MapMpAccMember = new Map<Id,Map<Id,AccountTeamMember>>();
        MapSupervisores = new Map<Id, AccountTeamMember>();
        accountSettings = DevUtils_cls.getAccountRouteSettings();
    }
    
    public void AssignManagerToSalesAgent(List<ONTAP__Route__c> route_lst, Map<Id, ONTAP__Route__c> mapRoutes) {
        User objUser = new User();
        List<User> usersUpdate_lst = new List<User>();
        if (!route_lst.isEmpty()) {
            for (ONTAP__Route__c route : route_lst) {
                objUser = new User();
                objUser.Id = route.RouteManager__c;
                objUser.ManagerId = mapRoutes.get(route.Id).Supervisor__c;
                usersUpdate_lst.add(objUser);
            }
        }
        
        if (!usersUpdate_lst.isEmpty()) {
            ISSM_TriggerManager_cls.inactivate('ISSM_AssignOwnerIdAccountTeam_tgr');
            try {
                Database.update(usersUpdate_lst);
            } catch(exception ex) {
                System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
            }
        }
    }

    public void ValidateSalesAgent(List<ONTAP__Route__c> LstRoutes)
    {
        Set<Id> setUsers = new Set<Id>();
        for(ONTAP__Route__c ORoute : LstRoutes) {
            setUsers.add(ORoute.RouteManager__c);
        }

        QryAccountTeamMember(setUsers);
        System.debug('MapAppAccMember: ' + MapMpAccMember);
        for(ONTAP__Route__c ORoute : LstRoutes)
        {
            System.debug('ORoute.RouteManager__c____>>>>>>> '+ORoute.RouteManager__c);
            if(ORoute.RouteManager__c == null) { ORoute.RouteManager__c.addError(label.SAEmpty);
            }
            else if(ORoute.ONTAP__SalesOffice__c == null) { ORoute.ONTAP__SalesOffice__c.addError(label.SOEmpty);
            }
            else if(MapMpAccMember.containsKey(ORoute.RouteManager__c))
            {
                if(MapMpAccMember.get(ORoute.RouteManager__c).containsKey(ORoute.ONTAP__SalesOffice__c)
                    && MapSupervisores.containsKey(MapMpAccMember.get(ORoute.RouteManager__c).get(ORoute.ONTAP__SalesOffice__c).AccountId)) { ORoute.Supervisor__c = MapSupervisores.get(MapMpAccMember.get(ORoute.RouteManager__c).get(ORoute.ONTAP__SalesOffice__c).AccountId).UserId;
                } else {
                    ORoute.addError(label.SANBelong);
                }
            }
            else
            ORoute.addError(label.SANBelong);
        }

    }

    public void QryAccountTeamMember(Set<Id> setUsers) {
        // Get Account Team Member Record Type to Validate
        Map<String, RecordType> mapRecordTypes = DevUtils_cls.getRecordTypes('Account', 'DeveloperName');
        Id accountRecordTypeId = mapRecordTypes.get(accountSettings.AccountRecordTypeToValidate__c).Id;
        Set<Id> setAccountId = new set<Id>();


        AccountTeamMember[] lstAccountTeamMembers = [
            SELECT ID,
                AccountId,
                Account.Name,
                Account.ISSM_SalesOffice__c,
                UserId,
                TeamMemberRole
            FROM AccountTeamMember
            WHERE UserId IN :setUsers
                AND Account.RecordTypeId = :accountRecordTypeId
        ];

        for(AccountTeamMember teamMember : lstAccountTeamMembers) {
            if (MapMpAccMember.containsKey(teamMember.UserId )) { MapMpAccMember.get(teamMember.UserId).put(teamMember.Account.ISSM_SalesOffice__c, teamMember);
            } else {
                MapMpAccMember.put(teamMember.UserId, new Map<Id,AccountTeamMember>{
                    teamMember.Account.ISSM_SalesOffice__c => teamMember
                });
            }

            setAccountId.add(teamMember.AccountId);
        }

        // Add Supervisor Users Team Members to Map
        AccountTeamMember[] lstSupervisors = [
            SELECT ID,
                AccountId,
                Account.Name,
                Account.ISSM_SalesOffice__c,
                UserId,
                TeamMemberRole
            FROM AccountTeamMember
            WHERE AccountId IN :setAccountId
                AND TeamMemberRole = :accountSettings.SuperVisorTeamMemberRole__c
        ];

        for (AccountTeamMember teamMember : lstSupervisors) { MapSupervisores.put(teamMember.AccountId, teamMember); }
    }

    /**
     * Filtar Rutas con Modelo de servicio para Presales y Autosales
     * @param  lstRoutes Lista de rutas a filtrar
     * @return           Lista de rutas filtradas
     */
    public ONTAP__Route__c[] filterRoutes(ONTAP__Route__c[] lstRoutes) {
        // Get service models to filter from visit plan settings
        Set<String> setModelsToFilter = new Set<String>(accountSettings.ServiceModelsToFilter__c.split(','));

        ONTAP__Route__c[] lstFilteredRoutes = (ONTAP__Route__c[]) DevUtils_cls.filterSObjectList(lstRoutes, 'ServiceModel__c', setModelsToFilter);

        return lstFilteredRoutes;
    }

    /**
       @method Filter Accounts with service model set as BDR
     * @param  lstAccounts Lista de cuentas a filtrar
     * @return             Lista de cuentas filtradas
     * Rodrigo Resendiz (1.2)
     */
    public static ONTAP__Route__c[] filterRoutesBDR(ONTAP__Route__c[] lstRoutes) { Set<String> setModelsToFilter = new Set<String>(accountSettingsBDR.ServiceModelsToFilter__c.split(',')); ONTAP__Route__c[] lstFilteredRoutes = (ONTAP__Route__c[]) DevUtils_cls.filterSObjectList(lstRoutes, 'ServiceModel__c', setModelsToFilter); return lstFilteredRoutes; }

    /**
     * Delete Accounts by Route on Route Deletion
     * @param setRouteIds Set of Route Ids
     */
    public void deleteRelatedAccountsByRoute(Set<Id> setRouteIds) {
        AccountByRoute__c[] lstAccountsByRoute = [
            SELECT Id
            FROM AccountByRoute__c
            WHERE Route__c IN :setRouteIds
        ];

        delete lstAccountsByRoute;
    }

    /**
     @method Set the BDR as the owner of the account in the route
     * @param  lstAccountsByRoute Account by route records that 
     * @return void
     * Rodrigo Resendiz (1.2)
     */
     public static void setBDRAsAccountOwner(ONTAP__Route__c[] lstRoutes, Map<Id, ONTAP__Route__c> mapRoutes){
        Set<Id> setRouteIds = new Set<Id>();
        for(ONTAP__Route__c route : lstRoutes){
            setRouteIds.add(route.Id);
        }
        AccountByRoute__c[] lstAccountsByRoute = [
            SELECT Id, Route__c, Account__c
            FROM AccountByRoute__c
            WHERE Route__c IN :setRouteIds
        ];
        AccountByRoute_cls.setBDRAsAccountOwner(lstAccountsByRoute, mapRoutes);
     }

      /**
     @method Set the BDR as the owner of the account in the route
     * @param  lstAccountsByRoute Account by route records that 
     * @return void
     * Rodrigo Resendiz (1.3)
     */
     public static void setAccountTeam(ONTAP__Route__c[] lstRoutes,Map<Id, ONTAP__Route__c> oldMapRoute){
        Set<Id> setRouteIds = new Set<Id>();
        for(ONTAP__Route__c route : lstRoutes){
            setRouteIds.add(route.Id);
        }
        AccountByRoute__c[] lstAccountsByRoute = [
            SELECT Id, Route__c, Account__c
            FROM AccountByRoute__c
            WHERE Route__c IN :setRouteIds
        ];
        AccountByRoute_cls.removeUsersFromAccountTeam(lstAccountsByRoute, oldMapRoute);
        AccountByRoute_cls.removeTelesalesFromAccountTeam(lstAccountsByRoute, oldMapRoute);
        AccountByRoute_cls.addUsersToAccountTeam(lstAccountsByRoute);
        AccountByRoute_cls.addTelesaleToAccountTeam(lstAccountsByRoute);
     }

     /**
    @method filterRoutesByChange: Spots differences between trigger.new and trigger.old lists regarding specific fields
    * @param newLstAccounts
    * @param oldLstAccounts
    * @param fieldsToCompare
    * @return ONTAP__Route__c[] lstFilteredRoutes values of trigger.Old that have diff
    * Rodrigo Resendiz (1.2)
    */
    public static ONTAP__Route__c[] filterRoutesByChange(ONTAP__Route__c[] newLstAccounts, Map<Id,ONTAP__Route__c> oldMapAccounts, String[] fieldsToCompare) { ONTAP__Route__c[] lstFilteredRoutes = (ONTAP__Route__c[]) DevUtils_cls.diffSObjectList(newLstAccounts, oldMapAccounts, fieldsToCompare); return lstFilteredRoutes; }
}