/**
 * This class serves as batch class for AllMobileDeviceUserOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileDeviceUserBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	/**
	 * This method returns a list with DeviceUser records to be deleted.
	 *
	 * @param objBatchableContext	Database.BatchableContext
	 */
	global Database.QueryLocator start(Database.BatchableContext objBatchableContext) {
		return Database.getQueryLocator(AllMobileStaticVariablesClass.STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_DEVICE_USERS);
	}

	/**
	 * This method execute a batch to delete the DeviceUser records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 * @param List<AllMobileDeviceUser__c>
	 */
	global void execute(Database.BatchableContext objBatchableContext, List<AllMobileDeviceUser__c> lstAllMobileDeviceUserSF) {
		AllMobileDeviceUserOperationClass.syncDeleteAllMobileDeviceUserInSF(lstAllMobileDeviceUserSF);
	}

	/**
	 * This method finish the execution of batch which deletes the DeviceUser records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 */
	global void finish(Database.BatchableContext objBatchableContext) {
	}
}