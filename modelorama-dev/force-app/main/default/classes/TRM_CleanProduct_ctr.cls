/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows Clean products of current list

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    20-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class TRM_CleanProduct_ctr {
	public TRM_CleanProduct_ctr() {	}

	//Wrapper de listado de productos
	public class WrapperProduct{
        @AuraEnabled public ONTAP__Product__c ObjProducts{get;set;}
        @AuraEnabled public boolean IsSelected{get;set;}
        @AuraEnabled public Decimal DecAmnt{get;set;}
        
        public WrapperProduct(ONTAP__Product__c prod,Decimal DecAmount) {
            this.ObjProducts = prod;
            this.IsSelected  = true;
            this.DecAmnt     = DecAmount;
        }
    }

    /**
    * @description  Method that performs the search of products according to the word entered and the products already selected
    * 
    * @param    LstIds              List Id´s of deselected products
    * @param    MapProd             Map with Id and Amount of products
    * 
    * @return   WrapperProduct[]    List of products
    */
    @AuraEnabled
    public static WrapperProduct[] SearchProducts(String[] LstIds,Map<String,Decimal> MapProd){
        WrapperProduct[] LstProducts = new  List<WrapperProduct>();
    	
        String  sQuery= 'SELECT Id, ONTAP__ProductId__c, ONTAP__MaterialProduct__c ';
                sQuery+='FROM ONTAP__Product__c ';
                sQuery+='WHERE ';
                sQuery+='Id IN (' + String.join( LstIds, ',' ) +')';

        System.debug('sQuery-> '+sQuery);
        ONTAP__Product__c[] LstProd = ISSM_UtilityFactory_cls.executeQuery(sQuery);    	
        
		if(!LstProd.isEmpty()){
	        for(ONTAP__Product__c prod : LstProd) {
	            LstProducts.add(new TRM_CleanProduct_ctr.WrapperProduct(prod,MapProd.get(prod.Id)));
	        }
		}
		
    	return LstProducts;
    }
}