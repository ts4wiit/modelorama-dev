/**
 * Developed by:	   Avanxo México
 * Author:			   Oscar Alvarez
 * Project:			   AbInbev - Trade Revenue Management
 * Description:		   Clase Controlador Apex del componente Lightning 'TRM_Approvals_lcp'. 
 *					   That allows to obtain the list of values ​​of a selection list
 *
 *  No.        Fecha            Autor                 Descripción
 * 1.0    24-Agosto-2018     Oscar Alvarez             CREATION
 *
 */
public class TRM_Approvals_ctr {
	/**
    * @description  Method that allows to obtain the list of values ​​of a selection list
    * 
    * @param    objObject    Object on which the search will be carried out
    * @param    strFld       Field that contains the list of values ​​to search
    * 
    * @return   Return a list of type 'String'
    */
    @AuraEnabled
	public static String[] getOptions(sObject objObject, String strFld){
		return ISSM_UtilityFactory_cls.getSelectOptions(objObject,strFld);
	}
}