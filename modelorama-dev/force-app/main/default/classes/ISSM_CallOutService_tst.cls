/****************************************************************************************************
    General Information
    -------------------
    author:     Ernesto Gutiérrez
    email:      egutierrez@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Test class to cover ISSM_CallOutService_ctr 
	
    Information about changes (versions)
    ================================================================================================
    Number    Dates                Author                       Description          
    ------    --------             --------------------------   ------------------------------------
    1.0       07-September-2018    Ernesto Gutiérrez            Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
public class ISSM_CallOutService_tst {

    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Order__c ObjOrder2{get;set;}
    static ONTAP__Product__c ObjProduct{get;set;}
    static ONTAP__Product__c ObjProduct2{get;set;}
    static ONTAP__Product__c ObjProduct3{get;set;}
    static Account ObjAccount{get;set;}//Cuentas
    static ONCALL__Call__c ObjCall{get;set;}//Llamadas
    static ONTAP__Order_Item__c ObjOI{get;set;}
    static ONTAP__Order_Item__c ObjOI2{get;set;}
    static ONTAP__Order_Item__c[] LstOI{get;set;}
    static ONTAP__Product__c[] LstProduc{get;set;}
    static ApexPages.StandardController sc{get;set;}
    static String IdOrderr{get;set;}
    static String IdAccnt{get;set;}
    static ONTAP__Order_Item__c[] lstIds{get;set;}
    static ISSM_Bonus__c ObjBns{get;set;}
    static ISSM_Bonus__c ObjBns2{get;set;}
    static ISSM_Bonus__c[] LstBns{get;set;}
    static EmptyBalanceB__c ObjEB{get;set;}
    static EmptyBalanceB__c[] LstObjEB2{get;set;}
    static EmptyBalanceB__c[] LstEB{get;set;}
    static ISSM_NewOrder_ctr.PriceCondition[] LstPC{get;set;}
    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
    static Map<Integer,String> ZTPMMap = new Map<Integer,String>();
    static ISSM_ProductByOrg__c ObjPBO{get;Set;}
    static Account ObjAccOrg{get;set;}//Cuentas
    public static ISSM_CreateOrder_ctr createOrder = new ISSM_CreateOrder_ctr();
    public static ISSM_PricingMotor_cls priceMotor = new ISSM_PricingMotor_cls();

/*
    public static void StartValues(){
        //LstAccount = Test.loadData(Account.sObjectType, 'ISSM_AccountDataTst');

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = Label.ISSM_EPPricingEngine,
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        LstObjEB2 = new List<EmptyBalanceB__c>();
        LstOI = new List<ONTAP__Order_Item__c>();
        LstBns = new List<ISSM_Bonus__c>();
        LstEB = new List<EmptyBalanceB__c>();
        LstPC = new List<ISSM_NewOrder_ctr.PriceCondition>();

        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;

        ObjAccOrg = new Account(Name = 'Cuenta Org',
            ONTAP__Main_Phone__c= '12345678',
            ONTAP__SalesOgId__c = '3116',
            RecordTypeId = CTRSOQL.getRecordTypeId('Account','SalesOrg'));
        insert ObjAccOrg;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;

        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ISSM_Empties_Material__c    = ObjProduct2.Id,
            ISSM_BoxRack__c             = ObjProduct3.Id

        );
        insert ObjProduct;


        ObjPBO = new ISSM_ProductByOrg__c(
            ISSM_RelatedAccount__c      =   ObjAccOrg.Id,
            ISSM_AssociatedProduct__c   =   ObjProduct.Id,
            ISSM_Empties_Material__c    =   ObjProduct2.Id,
            ISSM_EmptyRack__c           =   ObjProduct3.Id,
            RecordTypeId                =   CTRSOQL.getRecordTypeId('ISSM_ProductByOrg__c','ISSM_OnCallProduct'));

        insert ObjPBO;


        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;


        ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder2;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_ComboNumber__c = '8768:8768'
        );
        insert ObjOI;


        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c = true
        );

        insert ObjOI2;

        ObjBns = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct.Id,
                            ISSM_SAPOrderNumber__c  = '300',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;


        ObjBns2 = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct2.Id,
                            ISSM_SAPOrderNumber__c  = '200',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns2;


        ObjEB = new EmptyBalanceB__c(
                            Account__c         = ObjAccount.Id,
                            SAPOrderNumber__c  = '1',
                            DueDate__c         = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c  = 2,
                            MaterialProduct__c = 'ENVASE',
                            EmptyBalanceKey__c = '321',
                            Product__c         = ObjProduct.Id,
                            Process__c         = false
        );

        LstObjEB2.add(new EmptyBalanceB__c(
                            Account__c = ObjAccount.Id,
                            SAPOrderNumber__c = '2',
                            DueDate__c = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c = 2,
                            MaterialProduct__c = 'CAJA',
                            EmptyBalanceKey__c = '4321',
                            Product__c = ObjProduct.Id,
                            Process__c = false
        ));
        insert ObjEB;

        LstBns.add(ObjBns);
        LstEB.add(ObjEB);
        LstOI.add(ObjOI);
        LstPC.add(new ISSM_NewOrder_ctr.PriceCondition(Label.ISSM_ZPVM,5.0,10));
        Apexpages.currentPage().getParameters().put(Label.ISSM_IdOrder, ObjOrder.Id);
        lstIds  = new List<ONTAP__Order_Item__c>();
        lstIds.add(ObjOI);
        ZTPMMap.put(3000005, '');

    }

      @isTest  static  void StatusClosedOrderTest() {
     	
    
     	   insert new ISSM_PriceEngineConfigWS__c(
			Name = 'ConfigWSCancelOrders' ,
			ISSM_EndPoint__c = 'https://mx-issm-v360-order-api-service.cloudhub.io/api/client/order',
			ISSM_HeaderContentType__c = 'application/json; charset=UTF-8',
			ISSM_Method__c = 'POST',
			ISSM_Password__c = 'v360rderAcc2017',
			ISSM_Username__c = 'Mx0rderMgDev'
			); 
			
     	id RT_Emergency_id = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Emergency' LIMIT 1].Id;
     	  
        ONTAP__Order__c orderEmergencyStatusClosed = new ONTAP__Order__c();
        orderEmergencyStatusClosed.ONCALL__SAP_Order_Number__c = '12349190';
        orderEmergencyStatusClosed.ISSM_IdEmptyBalance__c = '67890';
        orderEmergencyStatusClosed.ONTAP__TotalAmount__c = 10;
        orderEmergencyStatusClosed.ONTAP__OrderStatus__c = 'Closed';
        orderEmergencyStatusClosed.RecordTypeId = RT_Emergency_id;
        orderEmergencyStatusClosed.ONCALL__Order_Created_Date_Time__c = System.today()+1;
        insert orderEmergencyStatusClosed;
        
     	
     	
     	 test.startTest();		
			
			ApexPages.StandardController orderStatusClose = new ApexPages.standardController(orderEmergencyStatusClosed);
      		ISSM_WSCancelaPedidos controllerstatus = new ISSM_WSCancelaPedidos(orderStatusClose);
      		
          	PageReference pageRef = Page.ISSM_WSCancelaPedidos_pag;
            Test.setCurrentPage(pageRef);
			PageReference pageRefInicializar = controllerstatus.inicializar();
			
			ISSM_CallOutService_ctr objCall = new ISSM_CallOutService_ctr();
			
			ISSM_CallOutService_ctr.CalloutClass objClass = new ISSM_CallOutService_ctr.CalloutClass();
			//objClass.getInfoFromExternalService('{}','ConfigWSCancelOrders');  

			
	
        test.stopTest();
     	
     	
     }*/
     
		@isTest  static  void TestSendRequest() {
			ISSM_CallOutService_ctr objCallOut = new ISSM_CallOutService_ctr();
			
        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = 'https://mx-issm-v360-order-api-service.cloudhub.io/api/client/order',
            ISSM_Method__c = 'PUT',
            ISSM_HeaderContentType__c = 'application/json; charset=UTF-8',          
            Name = 'ConfigWSCancelOrders');
			test.startTest();
				ISSM_CallOutService_ctr.WprResponseService  WprCallOut = new  ISSM_CallOutService_ctr.WprResponseService('ok','error');
				objCallOut.SendRequest('{"numClient": "0100169817"}','ConfigWSCancelOrders');
			
			test.stopTest();
      	
      }
     
     




}