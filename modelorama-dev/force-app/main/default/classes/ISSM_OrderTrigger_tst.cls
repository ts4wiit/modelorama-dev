@isTest
private class ISSM_OrderTrigger_tst {
    
    public static testMethod ONTAP__Order__c DeleteOrderItems() {
        // Variables
        //Id devRecordTypeId;
        Integer countOrderItemInitial = 0;
        Integer countOrderItemFinal = 0;
        
        Set<String> setOrderIds = new Set<String>();
        
        //devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
        
        // Insertar el Objeto del Pedido
        ISSM_TriggerFactory__c TriggerFactory = new ISSM_TriggerFactory__c();
        TriggerFactory.Name = 'ONTAP__Order__c';
        TriggerFactory.ISSM_TriggerHandler__c = 'ONTAP__Order__c ';
        insert TriggerFactory;
        
        // Insertar un Pedido
        ONTAP__Order__c Order = new ONTAP__Order__c();
        //Order.RecordTypeId = devRecordTypeId;
        Order.ONCALL__SAP_Order_Number__c = 'Test';
        insert Order;
		setOrderIds.add(Order.Id);
        
        // Insertar un Elemento del Pedido Previamente Creado
        ONTAP__Order_Item__c OrderItem = new ONTAP__Order_Item__c();
        OrderItem.ONTAP__CustomerOrder__c = Order.Id;
        OrderItem.ISSM_Uint_Measure_Code__c = 'Test';
        insert OrderItem;
        
        // Insertar Otro Elemento del Mismo Pedido Previamente Creado
        ONTAP__Order_Item__c OrderItem2 = new ONTAP__Order_Item__c();
        OrderItem2.ONTAP__CustomerOrder__c = Order.Id;
        OrderItem2.ISSM_Uint_Measure_Code__c = 'Test2';
        insert OrderItem2;
        
        // Query que Valida que se Hayan Insertado Correctamente los Elementos al Pedido
        countOrderItemInitial = database.countQuery('SELECT COUNT() FROM ONTAP__Order_Item__c WHERE ONTAP__CustomerOrder__c IN: setOrderIds');
        
        Test.startTest();
        
        delete Order;
        
        // Query que Valida que se Hayan Eliminado Correctamente los Elementos al Pedido
        countOrderItemFinal = database.countQuery('SELECT COUNT() FROM ONTAP__Order_Item__c WHERE ONTAP__CustomerOrder__c IN: setOrderIds');
        
        System.assertEquals(2, countOrderItemInitial);
        System.assertEquals(0, countOrderItemFinal);
        
        Test.stopTest();
        
        return null;
    }
}