/**
* This class contains the operations to synchronize the AllMobileRouteAppVersionHelperClass records with Heroku.
* <p /><p />
* @author Alberto Gómez
*/
global class AllMobileRouteAppVersionOperationClass implements Queueable {

	//Variables.
	String strEventTriggerFlag;
	List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion = new List<AllMobileRouteAppVersion__c>();

	//Constructor.
	public AllMobileRouteAppVersionOperationClass(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionTrigger, String strEventTriggerFlagTrigger) {
		lstAllMobileRouteAppVersion = lstAllMobileRouteAppVersionTrigger;
		strEventTriggerFlag = strEventTriggerFlagTrigger;
	}

	/**
	 *This method initiate a List of RouteAppVersion objects in Salesforce into Heroku routeappversion table using POST method (insert).
	 *
	 * @param lstAllMobileRouteAppVersion	List<AllMobileRouteAppVersion__c>
	 */
	public static void syncInsertAllMobileRouteAppVersionToExternalObject(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion) {
		Set<Id> setIdAllMobileRouteAppVersion = new Set<Id>();
		if(!lstAllMobileRouteAppVersion.isEmpty()) {
			setIdAllMobileRouteAppVersion = getSetIdAllMobileRouteAppVersionSFFromListAllMobileRouteAppVersionSF(lstAllMobileRouteAppVersion);
			AllMobileRouteAppVersionOperationClass.insertExternalAllMobileRouteAppVersionWS(setIdAllMobileRouteAppVersion);
		}
	}

	/**
	 * This method prepares a List of RouteAppVersion objects in Salesforce to communicate asynchronously and insert into Heroku routeappversion table.
	 *
	 * @param setIdAllMobileRouteAppVersion	Set<Id>
	 */
	@future(callout = true)
	public static void insertExternalAllMobileRouteAppVersionWS(Set<Id> setIdAllMobileRouteAppVersion) {
		AllMobileRouteAppVersionOperationClass.insertExternalAllMobileRouteAppVersion(setIdAllMobileRouteAppVersion);
	}

	/**
	 * This method sync a List of RouteAppVersion objects from  Salesforce into a Heroku routeappversion table using POST method (insert).
	 *
	 * @param setIdAllMobileRouteAppVersion	Set<Id>
	 */
	public static Boolean insertExternalAllMobileRouteAppVersion(Set<Id> setIdAllMobileRouteAppVersion) {

		//Variables.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToInsertInHeroku = new List<AllMobileRouteAppVersion__c>();
		AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass();

		//Get List of RouteAppVersion in SF.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionSF = [SELECT Id, Name, AllMobileRouteAppVersionExternalId__c, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileValidStartDate__c, AllMobileValidEndDate__c, LastModifiedDate, AllMobileSoftDeleteFlag__c FROM AllMobileRouteAppVersion__c WHERE Id =: setIdAllMobileRouteAppVersion];

		//Get List of All RouteAppVersion in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileAllRouteAppVersionHerokuObjectClass = getAllMobileAllRouteAppVersionHerokuDeserialize();

		//Compare both lists to avoid insert RouteAppVersion SF that already exist in HK. Generates new List to be inserted in Heroku.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersionSF: lstAllMobileRouteAppVersionSF) {
			Integer intContainedRouteAppVersionSFInRouteAppVersionHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClassIterate : lstAllMobileAllRouteAppVersionHerokuObjectClass) {
				if((objAllMobileRouteAppVersionSF.AllMobileRouteId__c == (objAllMobileRouteAppVersionHerokuObjectClassIterate.route).trim()) && (objAllMobileRouteAppVersionSF.AllMobileVersionId__c == (objAllMobileRouteAppVersionHerokuObjectClassIterate.version_id).trim())) {
					intContainedRouteAppVersionSFInRouteAppVersionHeroku++;
				}
			}
			if(intContainedRouteAppVersionSFInRouteAppVersionHeroku == 0) {
				lstAllMobileRouteAppVersionToInsertInHeroku.add(objAllMobileRouteAppVersionSF);
			}
		}

		//Insert the Final List of RouteAppVersion (SF) into Heroku.
		if(!lstAllMobileRouteAppVersionToInsertInHeroku.isEmpty()) {
			for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersion: lstAllMobileRouteAppVersionToInsertInHeroku) {
				objAllMobileRouteAppVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass();
				objAllMobileRouteAppVersionHerokuObjectClass.routeappversionid = Integer.valueOf(objAllMobileRouteAppVersion.AllMobileRouteAppVersionExternalId__c);
				objAllMobileRouteAppVersionHerokuObjectClass.route = objAllMobileRouteAppVersion.AllMobileRouteId__c;
				objAllMobileRouteAppVersionHerokuObjectClass.version_id = objAllMobileRouteAppVersion.AllMobileVersionId__c;
				objAllMobileRouteAppVersionHerokuObjectClass.validstart = objAllMobileRouteAppVersion.AllMobileValidStartDate__c;
				objAllMobileRouteAppVersionHerokuObjectClass.validend = objAllMobileRouteAppVersion.AllMobileValidEndDate__c;
				objAllMobileRouteAppVersionHerokuObjectClass.dtlastmodifieddate = objAllMobileRouteAppVersion.LastModifiedDate;
				objAllMobileRouteAppVersionHerokuObjectClass.softdeleteflag = objAllMobileRouteAppVersion.AllMobileSoftDeleteFlag__c;

				//Rest callout to insert RouteAppVersion in Heroku.
				String strStatusCodeInsert = AllMobileSyncObjectsClass.calloutInsertRouteAppVersionIntoHeroku(objAllMobileRouteAppVersionHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method initiate a List of RouteAppVersion objects in Salesforce into Heroku routeappversion table using PUT method (update).
	 *
	 * @param lstAllMobileRouteAppVersion	List<AllMobileRouteAppVersion__c>
	 */
	public static void syncUpdateAllMobileRouteAppVersionToExternalObject(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion) {
		Set<Id> setIdAllMobileRouteAppVersion = new Set<Id>();
		if(!lstAllMobileRouteAppVersion.isEmpty()) {
			setIdAllMobileRouteAppVersion = getSetIdAllMobileRouteAppVersionSFFromListAllMobileRouteAppVersionSF(lstAllMobileRouteAppVersion);
			AllMobileRouteAppVersionOperationClass.updateExternalAllMobileRouteAppVersionWS(setIdAllMobileRouteAppVersion);
		}
	}

	/**
	 * This method prepares a List of RouteAppVersion objects in Salesforce to communicate asynchronously and update into Heroku routeappversion table.
	 *
	 * @param setIdAllMobileRouteAppVersion Set<Id>
	 */
	@future(callout = true)
	public static void updateExternalAllMobileRouteAppVersionWS(Set<Id> setIdAllMobileRouteAppVersion) {
		AllMobileRouteAppVersionOperationClass.updateExternalAllMobileRouteAppVersion(setIdAllMobileRouteAppVersion);
	}

	/**
	 * This method sync a List of RouteAppVersion objects from  Salesforce into a Heroku routeappversion table using PUT method (update).
	 *
	 * @param setIdAllMobileRouteAppVersion	Set<Id>
	 */
	public static Boolean updateExternalAllMobileRouteAppVersion(Set<Id> setIdAllMobileRouteAppVersion) {

		//Variables.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToUpdateInHeroku = new List<AllMobileRouteAppVersion__c>();
		AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass();

		//Get List of RouteAppVersion in SF.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionSF = [SELECT Id, Name, AllMobileRouteAppVersionExternalIdHK__c, AllMobileRouteAppVersionExternalId__c, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileValidStartDate__c, AllMobileValidEndDate__c, LastModifiedDate, AllMobileSoftDeleteFlag__c  FROM AllMobileRouteAppVersion__c WHERE Id =: setIdAllMobileRouteAppVersion];

		//Get List of All RouteAppVersion in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileAllRouteAppVersionHerokuObjectClass = getAllMobileAllRouteAppVersionHerokuDeserialize();

		//Compare both lists to avoid update RouteAppVersion SF that do not exist in HK. Generates new List to be updated in Heroku.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersionSF : lstAllMobileRouteAppVersionSF) {
			Integer intContainedRouteAppVersionSFInRouteAppVersionHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClassIterate : lstAllMobileAllRouteAppVersionHerokuObjectClass) {
				if((objAllMobileRouteAppVersionSF.AllMobileRouteId__c == (objAllMobileRouteAppVersionHerokuObjectClassIterate.route).trim()) && (objAllMobileRouteAppVersionSF.AllMobileVersionId__c == (objAllMobileRouteAppVersionHerokuObjectClassIterate.version_id).trim())) {
					intContainedRouteAppVersionSFInRouteAppVersionHeroku++;
				}
			}
			if(intContainedRouteAppVersionSFInRouteAppVersionHeroku > 0) {
				lstAllMobileRouteAppVersionToUpdateInHeroku.add(objAllMobileRouteAppVersionSF);
			}
		}

		//Update the Final List of RouteAppVersion (SF) into Heroku.
		if(!lstAllMobileRouteAppVersionToUpdateInHeroku.isEmpty()) {
			for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersion: lstAllMobileRouteAppVersionToUpdateInHeroku) {
				objAllMobileRouteAppVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass();
				objAllMobileRouteAppVersionHerokuObjectClass.routeappversionid = Integer.valueOf(objAllMobileRouteAppVersion.AllMobileRouteAppVersionExternalId__c);
				objAllMobileRouteAppVersionHerokuObjectClass.route = objAllMobileRouteAppVersion.AllMobileRouteId__c;
				objAllMobileRouteAppVersionHerokuObjectClass.version_id = objAllMobileRouteAppVersion.AllMobileVersionId__c;
				objAllMobileRouteAppVersionHerokuObjectClass.validstart = objAllMobileRouteAppVersion.AllMobileValidStartDate__c;
				objAllMobileRouteAppVersionHerokuObjectClass.validend = objAllMobileRouteAppVersion.AllMobileValidEndDate__c;
				objAllMobileRouteAppVersionHerokuObjectClass.dtlastmodifieddate = objAllMobileRouteAppVersion.LastModifiedDate;
				objAllMobileRouteAppVersionHerokuObjectClass.softdeleteflag = objAllMobileRouteAppVersion.AllMobileSoftDeleteFlag__c;

				//Rest callout to update RouteAppVersion in Heroku.
				String strStatusCode = AllMobileSyncObjectsClass.calloutUpdateRouteAppVersionIntoHeroku(objAllMobileRouteAppVersionHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method delete a List of RouteAppVersion objects from  Salesforce according to a Heroku routeappversion table using DELETE method.
	 *
	 * @param lstAllMobileRouteAppVersionInSF	List<AllMobileRouteAppVersion__c>
	 */
	public static void syncDeleteAllMobileRouteAppVersionInSF(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionInSF) {

		//Inner variables.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToBeDeletedInSF = new List<AllMobileRouteAppVersion__c>();
		List<String> lstRouteAndVersionFromRouteAppVersionHeroku = new List<String>();

		//Get List of All RouteAppVersion in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileAllRouteAppVersionHerokuObjectClass = getAllMobileAllRouteAppVersionHerokuDeserialize();

		//Get List of Route & Version of RouteAppVersion Heroku to compare.
		for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClass : lstAllMobileAllRouteAppVersionHerokuObjectClass) {
			lstRouteAndVersionFromRouteAppVersionHeroku.add((objAllMobileRouteAppVersionHerokuObjectClass.route).trim() + (objAllMobileRouteAppVersionHerokuObjectClass.version_id).trim());
		}

		//Identify RouteAppVersion SF to be Deleted.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersionInSF : lstAllMobileRouteAppVersionInSF) {
			if(!lstRouteAndVersionFromRouteAppVersionHeroku.contains(objAllMobileRouteAppVersionInSF.AllMobileRouteId__c + objAllMobileRouteAppVersionInSF.AllMobileVersionId__c)) {
				lstAllMobileRouteAppVersionToBeDeletedInSF.add(objAllMobileRouteAppVersionInSF);
			}
		}
		if(!lstAllMobileRouteAppVersionToBeDeletedInSF.isEmpty() && lstAllMobileRouteAppVersionToBeDeletedInSF != NULL) {
			delete lstAllMobileRouteAppVersionToBeDeletedInSF;
		}
	}

	/**
	 * This method insert and update a List of RouteAppVersion objects from  Salesforce according to a Heroku routeappversion table using POST and PUT methods.
	 *
	 * @param lstAllMobileRouteAppVersionInSF	List<AllMobileRouteAppVersion__c>
	 */
	public static void syncInsertAndUpdateAllMobileRouteAppVersionInSFFromRouteAppVersionHeroku(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionInSF) {

		//Inner variables.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToBeInsertedInSF = new List<AllMobileRouteAppVersion__c>();
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileRouteAppVersionHerokuObjectClassToBeInsertedInSF = new List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize>();
		AllMobileRouteAppVersion__c objAllMobileRouteAppVersionToBeInsertedInSF = new AllMobileRouteAppVersion__c();
		List<String> lstRouteAppVersionRouteIdAndVersionIdFromAllMobileRouteAppVersionSF = new List<String>();
		List<Integer> lstApplicationIdFromHeroku = new List<Integer>();
		List<AllMobileApplication__c> lstAllMobileApplicationToBeDeletedInSF = new List<AllMobileApplication__c>();

		//Get List of All RouteAppVersion in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileAllRouteAppVersionHerokuObjectClass = getAllMobileAllRouteAppVersionHerokuDeserialize();

		//Get List of RouteAppVersion RouteId & Version Id from AllMobileRouteAppVersion SF.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersion : lstAllMobileRouteAppVersionInSF) {
			lstRouteAppVersionRouteIdAndVersionIdFromAllMobileRouteAppVersionSF.add(objAllMobileRouteAppVersion.AllMobileRouteId__c + objAllMobileRouteAppVersion.AllMobileVersionId__c);
		}

		//Identify RouteAppVersions From Heroku to be Inserted in SF.
		for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClass : lstAllMobileAllRouteAppVersionHerokuObjectClass) {
			if(!lstRouteAppVersionRouteIdAndVersionIdFromAllMobileRouteAppVersionSF.contains((objAllMobileRouteAppVersionHerokuObjectClass.route).trim() + (objAllMobileRouteAppVersionHerokuObjectClass.version_id).trim())) {
				lstAllMobileRouteAppVersionHerokuObjectClassToBeInsertedInSF.add(objAllMobileRouteAppVersionHerokuObjectClass);
			}
		}

		//Convert RouteAppVersion From Heroku to AllMobileApplication in SF.
		for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClass : lstAllMobileRouteAppVersionHerokuObjectClassToBeInsertedInSF) {
			objAllMobileRouteAppVersionToBeInsertedInSF = new AllMobileRouteAppVersion__c();
			objAllMobileRouteAppVersionToBeInsertedInSF.AllMobileRouteId__c = objAllMobileRouteAppVersionHerokuObjectClass.route;
			objAllMobileRouteAppVersionToBeInsertedInSF.AllMobileVersionId__c = objAllMobileRouteAppVersionHerokuObjectClass.version_id;
			objAllMobileRouteAppVersionToBeInsertedInSF.AllMobileValidStartDate__c = Date.newInstance((objAllMobileRouteAppVersionHerokuObjectClass.validstart).year(), (objAllMobileRouteAppVersionHerokuObjectClass.validstart).month(), (objAllMobileRouteAppVersionHerokuObjectClass.validstart).day());
			objAllMobileRouteAppVersionToBeInsertedInSF.AllMobileValidEndDate__c = Date.newInstance((objAllMobileRouteAppVersionHerokuObjectClass.validend).year(), (objAllMobileRouteAppVersionHerokuObjectClass.validend).month(), (objAllMobileRouteAppVersionHerokuObjectClass.validend).day());
			objAllMobileRouteAppVersionToBeInsertedInSF.AllMobileSoftDeleteFlag__c = objAllMobileRouteAppVersionHerokuObjectClass.softdeleteflag;
			lstAllMobileRouteAppVersionToBeInsertedInSF.add(objAllMobileRouteAppVersionToBeInsertedInSF);
		}
		if(!lstAllMobileRouteAppVersionToBeInsertedInSF.isEmpty() && lstAllMobileRouteAppVersionToBeInsertedInSF != NULL) {
			insert lstAllMobileRouteAppVersionToBeInsertedInSF;
		}

		//UPDATING
		List<AllmobileRouteAppVersion__c> lstAllMobileRouteAppVersionToBeUpdatedInSF = new List<AllmobileRouteAppVersion__c>();
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstAllMobileRouteAppVersionHerokuObjectClassExistingInSF = new List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize>();
		List<AllmobileRouteAppVersion__c> lstAllmobileRouteAppVersionPosibleBeUpdated = [SELECT Id, Name, AllMobileApplicationLK__c, AllMobileSoftDeleteFlag__c, AllMobileValidEndDate__c, AllMobileValidStartDate__c, AllMobileApplicationId__c, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileRouteAppVersionId__c, AllMobileRouteAppVersionExternalId__c, AllMobileRouteAppVersionExternalIdHK__c, AllMobileSalesOffice__c, AllMobileSalesOrg__c, AllMobileRouteName__c, AllMobileRouteLK__c, AllMobileVersionLK__c, LastModifiedDate FROM AllMobileRouteAppVersion__c WHERE AllMobileValidStartDate__c >= TODAY AND AllMobileValidEndDate__c >= TODAY];

		//Identify RouteAppVersion From Heroku to be Updated in SF.
		for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClass : lstAllMobileAllRouteAppVersionHerokuObjectClass) {
			if(lstRouteAppVersionRouteIdAndVersionIdFromAllMobileRouteAppVersionSF.contains((objAllMobileRouteAppVersionHerokuObjectClass.route).trim() + (objAllMobileRouteAppVersionHerokuObjectClass.version_id).trim())) {
				lstAllMobileRouteAppVersionHerokuObjectClassExistingInSF.add(objAllMobileRouteAppVersionHerokuObjectClass);
			}
		}

		//Generate List of RouteAppVersion ti be updated.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersionToCompareToUpdate : lstAllmobileRouteAppVersionPosibleBeUpdated) {
			for(AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate : lstAllMobileRouteAppVersionHerokuObjectClassExistingInSF) {
				if((objAllMobileRouteAppVersionToCompareToUpdate.AllMobileRouteId__c == (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.route).trim()) && (objAllMobileRouteAppVersionToCompareToUpdate.AllMobileVersionId__c == (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.version_id).trim())) {
					if(((objAllMobileRouteAppVersionToCompareToUpdate.AllMobileValidStartDate__c <= objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validstart) && (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validstart > System.today())) || ((objAllMobileRouteAppVersionToCompareToUpdate.AllMobileValidEndDate__c != objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validend) && (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validend > System.today())) || (objAllMobileRouteAppVersionToCompareToUpdate.AllMobileSoftDeleteFlag__c != objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.softdeleteflag)) {
						objAllMobileRouteAppVersionToCompareToUpdate.AllMobileValidEndDate__c = Date.newInstance((objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validend).year(), (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validend).month(), (objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.validend).day()) ;
						objAllMobileRouteAppVersionToCompareToUpdate.AllMobileSoftDeleteFlag__c = objAllMobileRouteAppVersionHerokuObjectClassToCompareToUpdateIterate.softdeleteflag;
						lstAllMobileRouteAppVersionToBeUpdatedInSF.add(objAllMobileRouteAppVersionToCompareToUpdate);
					}
				}
			}
		}
		if(!lstAllMobileRouteAppVersionToBeUpdatedInSF.isEmpty() && lstAllMobileRouteAppVersionToBeUpdatedInSF != NULL) {
			update lstAllMobileRouteAppVersionToBeUpdatedInSF;
		}
	}

	/**
	 * This method initiate a remote action to insert and update a List of RouteAppVersion objects of Salesforce according to a Heroku routeappversion table using POST AND PUT methods.
	 */
	@RemoteAction
	webservice static void syncInsertUpdateRouteAppVersionFromHeroku() {
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionInSF = [SELECT Id, Name, AllMobileApplicationLK__c, AllMobileSoftDeleteFlag__c, AllMobileValidEndDate__c, AllMobileValidStartDate__c, AllMobileApplicationId__c, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileRouteAppVersionId__c, AllMobileRouteAppVersionExternalId__c, AllMobileRouteAppVersionExternalIdHK__c, AllMobileSalesOffice__c, AllMobileSalesOrg__c, AllMobileRouteName__c, AllMobileRouteLK__c, AllMobileVersionLK__c, LastModifiedDate FROM AllMobileRouteAppVersion__c];
		syncInsertAndUpdateAllMobileRouteAppVersionInSFFromRouteAppVersionHeroku(lstAllMobileRouteAppVersionInSF);
	}

	/**
	 * This method get and set a List of Application Ids of Salesforce
	 *
	 * @param lstAllMobileRouteAppVersion	List<AllMobileRouteAppVersion__c>
	 * @return setIdAllMobileRouteAppVersion
	 */
	public static Set<Id> getSetIdAllMobileRouteAppVersionSFFromListAllMobileRouteAppVersionSF(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion) {
		Set<Id> setIdAllMobileRouteAppVersion = new Set<Id>();
		if(!lstAllMobileRouteAppVersion.isEmpty()) {
			for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersion : lstAllMobileRouteAppVersion) {
				setIdAllMobileRouteAppVersion.add(objAllMobileRouteAppVersion.Id);
			}
		}
		return setIdAllMobileRouteAppVersion;
	}

	/**
	 * This method get a List of all RouteAppVersion records of Heroku.
	 *
	 * @return List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize>
	 */
	public static List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> getAllMobileAllRouteAppVersionHerokuDeserialize() {
		String strJsonSerializeAllRouteAppVersionHeroku = AllMobileSyncObjectsClass.calloutGetAllRouteAppVersionFromHeroku();
		List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize> lstJsonDeserializeAllMobileAllRouteAppVersionHerokuObjectClass = (List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize>)JSON.deserialize(strJsonSerializeAllRouteAppVersionHeroku, List<AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClassDeserialize>.class);
		return lstJsonDeserializeAllMobileAllRouteAppVersionHerokuObjectClass;
	}

	/**
	 * This method execute a queueable trigger which insert and update a List of RouteAppVersion Ids of Salesforce.
	 *
	 * @param objQueueableContext	QueueableContext
	 */
	public void execute(QueueableContext objQueueableContext) {
		if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_INSERT) {
			syncInsertAllMobileRouteAppVersionToExternalObject(lstAllMobileRouteAppVersion);
		} else if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_UPDATE) {
			syncUpdateAllMobileRouteAppVersionToExternalObject(lstAllMobileRouteAppVersion);
		}
	}
}