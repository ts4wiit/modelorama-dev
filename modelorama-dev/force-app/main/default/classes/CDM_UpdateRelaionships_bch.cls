/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Batch to related all records to the object Model of CDM
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       23-April-2018   Iván Neria			   Initial version
***********************************************************************************/
global class CDM_UpdateRelaionships_bch implements Database.Batchable<sObject>
{

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT  Id, Name, Id_External__c,PARVW__c,KUNN2__c, VKORG__c,VTWEG__c, SPART__c,TAXKD01Id__c,'
        +' TAXKD02Id__c, TAXKD03Id__c, TAXKD04Id__c, TAXKD05Id__c, TAXKD06Id__c FROM MDM_Temp_Account__c WHERE TAXKD01Id__c=null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        Set<String> lstKUNNR = new Set<String>();
        Set<String> lstVKORG = new Set<String>();
        Set<String> lstVTWEG = new Set<String>();
        Set<String> lstSPART = new Set<String>();
        for ( MDM_Temp_Account__c tempAcc : (List<MDM_Temp_Account__c>)scope)
        {
          lstKUNNR.add(tempAcc.PARVW__c);
            lstVKORG.add(tempAcc.VKORG__c);
        }
        List<CDM_Temp_Taxes__c> lstTaxes= CDM_UpdateRshpsController_ctr.getTaxes(lstKUNNR);
        List<CDM_Temp_Interlocutor__c> lstInter= CDM_UpdateRshpsController_ctr.getInterlocutors(lstKUNNR,lstVKORG);
        List<CDM_Temp_Interlocutor__c> lstInterToUpdate= new List<CDM_Temp_Interlocutor__c>();
        //Set helper to avoid duplicity in list of Interlocutors.
        Set<String> setHelper = new Set<String>();
        
        for(MDM_Temp_Account__c tempAccToUpdate : (List<MDM_Temp_Account__c>)scope){
            for(CDM_Temp_Taxes__c Tax : lstTaxes){
                if(tempAccToUpdate.PARVW__c==Tax.KUNNR__c){
                    if(Tax.TATYP__c==Label.CDM_Apex_Tax1){
                        tempAccToUpdate.TAXKD01Id__c= Tax.Id;
                    }else if(Tax.TATYP__c==Label.CDM_Apex_Tax2){
                        tempAccToUpdate.TAXKD02Id__c= Tax.Id;
                    }else if (Tax.TATYP__c==Label.CDM_Apex_Tax3){
                        tempAccToUpdate.TAXKD03Id__c= Tax.Id;
                    }else if(Tax.TATYP__c==Label.CDM_Apex_Tax4){
                        tempAccToUpdate.TAXKD04Id__c= Tax.Id;
                    }else if (Tax.TATYP__c==Label.CDM_Apex_Tax5){
                        tempAccToUpdate.TAXKD05Id__c= Tax.Id;
                    }else if (Tax.TATYP__c==Label.CDM_Apex_Tax6){
                        tempAccToUpdate.TAXKD06Id__c= Tax.Id;
                    }
                }
            }
            
            for(CDM_Temp_Interlocutor__c tempIntToUpdate: lstInter){

                if(tempAccToUpdate.PARVW__c == tempIntToUpdate.KUNNR__c && tempAccToUpdate.VKORG__c == tempIntToUpdate.VKORG__c && tempAccToUpdate.SPART__c == tempIntToUpdate.SPART__c && tempAccToUpdate.VTWEG__c == tempIntToUpdate.VTWEG__c){
                    tempIntToUpdate.MDM_Temp_Account__c= tempAccToUpdate.Id;
                    
                    if(!setHelper.contains(tempIntToUpdate.Id)){
                        setHelper.add(tempIntToUpdate.Id);
                        lstInterToUpdate.add(tempIntToUpdate);
                    }
                }
            }
            
        }
        update scope;
        update lstInterToUpdate;
    }  
    global void finish(Database.BatchableContext BC)
    {
        EmailTemplate template =[SELECT Id FROM EmailTemplate WHERE Name=:Label.CDM_Apex_CDM_Relation_Records];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setTemplateId(template.Id);
        mail.setTargetObjectId(System.UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}