public class ISSM_Usuario_thr {
    @future (callout = true)
    public static void ManageTeamAccountByInactiveUser(String idUsr) {
        /*
        * VARIABLES
        */
        // Instanciamos la clase para realizar las consultas necesarias
        ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();

        List<AccountTeamMember> AccountTeamMember_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> DelAccountTeamMember_lst = new List<AccountTeamMember>();
        List<ISSM_ATM_Mirror__c> AccountTeamMember_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> DelAccountTeamMember_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        Set<String> UsuariosIds_set = new Set<String>();

        String OficinaVentas_str = '';
        String Rol_str = '';
        
        try {
            // Obtiene los registros de equipos de cuentas por usuario
            AccountTeamMember_lst = objCSQuerys.getATMByUsers(idUsr);
            /*AccountTeamMember_lst = [SELECT Id, UserId, AccountId, TeamMemberRole
                                     FROM AccountTeamMember
                                     WHERE UserId =: idUsr
                                     LIMIT 10000];*/

            // Verificamos que la lista tenga registros
            if (AccountTeamMember_lst.size() > 0) {
                for (AccountTeamMember atm : AccountTeamMember_lst) {
                    // Asignamos en sets, los Ids de los usuarios encontrados
                    UsuariosIds_set.add(atm.UserId);
                    // Asignamos en variables, la oficina de ventas y el rol encontrados
                    OficinaVentas_str = atm.AccountId;
                    Rol_str = atm.TeamMemberRole;
                }
                // Limpiamos la lista para que pueda ser utilizada nuevamente y no tener que crear otra
                AccountTeamMember_lst.clear();
            }

            // Buscamos el usuario que eliminaremos del equipo de cuentas estándar en base al set previamente llenado
            AccountTeamMember_lst = objCSQuerys.getATMBySetUsers(UsuariosIds_set);
            /*AccountTeamMember_lst = [SELECT Id, UserId, AccountId, TeamMemberRole
                                     FROM AccountTeamMember
                                     WHERE UserId IN: UsuariosIds_set
                                     LIMIT 10000];*/
            
            // Verificamos que la lista tenga registros
            if (AccountTeamMember_lst.size() > 0) {
                for (AccountTeamMember accTM : AccountTeamMember_lst) {
                    // Agregamos a la lista, los registros encontrados
                    DelAccountTeamMember_lst.add(accTM);
                }

                // Verificamos que la lista tenga registros
                if (DelAccountTeamMember_lst.size() > 0) {
                    // Eliminamos el usuario del equipo de cuentas estándar
                    delete DelAccountTeamMember_lst;
                }

                // Buscamos registros con usuarios que coincidan con los valores actuales
                AccountTeamMember_Mirror_lst = objCSQuerys.getATMMirrorBySetUsers(UsuariosIds_set);
                /*AccountTeamMember_Mirror_lst = [SELECT Id, Oficina_de_Ventas__c, Oficina_de_Ventas__r.Name, Usuario_relacionado__c
                                                FROM ISSM_ATM_Mirror__c
                                                WHERE Usuario_relacionado__c IN: UsuariosIds_set
                                                LIMIT 10000];*/

                // Verificamos que la lista tenga registros
                if (AccountTeamMember_Mirror_lst.size() > 0) {
                    for (ISSM_ATM_Mirror__c ATM : AccountTeamMember_Mirror_lst) {
                        // Agregamos a la lista, los registros encontrados
                        DelAccountTeamMember_Mirror_lst.add(ATM);
                    }
                    // Verificamos que la lista tenga registros
                    if (DelAccountTeamMember_Mirror_lst.size() > 0) {
                        // Eliminamos el usuario del equipo de cuentas personalizado
                        delete DelAccountTeamMember_Mirror_lst;
                    }
                }

                // Escribimos el registro en el objeto "ISSM_ATM_Queue__c"
                ISSM_ATM_Queue__c ATM_Queue = new ISSM_ATM_Queue__c(Usuario_relacionado__c = idUsr, Oficina_de_Ventas__c = OficinaVentas_str, Rol__c = Rol_str, Accion__c = 'Baja', Listo_para_Procesar__c = true);
                insert ATM_Queue;
            }
            
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
}