/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Test Class for 'ISSM_MultiSelectLookUp_cls'

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    31-Mayo-2018    	Luis Licona                 Creation
******************************************************************************* */
@isTest
private class ISSM_MultiSelectLookUp_tst {

	@testSetup
	static void setupData(){
		String recordTypeId = Schema.getGlobalDescribe().get('ONTAP__Product__c').getDescribe().getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId();
        MDM_Parameter__c MDMPARAM 		= new MDM_Parameter__c();
	        MDMPARAM.Name				= 'MDMPARAMETER';
	        MDMPARAM.Active__c    		= true;
	        MDMPARAM.Active_Revenue__c	= true;
	        MDMPARAM.Code__c 			= '40';
	        MDMPARAM.Catalog__c 		= 'Segmento';
	        MDMPARAM.Description__c		= 'MDMPARAMETER01';
        Insert MDMPARAM;

        MDM_Parameter__c MDMPARAM1 		= new MDM_Parameter__c();
	        MDMPARAM1.Name				= 'MDMPARAMETER02';
	        MDMPARAM1.Active__c    		= true;
	        MDMPARAM1.Active_Revenue__c	= true;
	        MDMPARAM1.Code__c 			= '40';
	        MDMPARAM1.Catalog__c 		= 'GrupoMateriales1';
	        MDMPARAM1.Description__c	= 'MDMPARAMETER02';
        Insert MDMPARAM1;

        ONTAP__Product__c PRODUCT = new ONTAP__Product__c();
	        PRODUCT.ONTAP__ProductId__c 		= '3000005';
	        PRODUCT.ONTAP__MaterialProduct__c 	= '3000005';
	        PRODUCT.ONTAP__ProductShortName__c 	= 'VICTORIA';
	        PRODUCT.ONTAP__ProductType__c 		= 'FERT';
	        PRODUCT.RecordTypeId 				= recordTypeId;
        Insert PRODUCT;
	}

	@isTest static void tstMethod_SearchPriceGrouping() {
		Test.startTest();
			sObject[] LstPriceGr  = ISSM_MultiSelectLookUp_cls.SearchPriceGrouping('Name,Code__c,Description__c',
																				  '40',
																				  'MDM_Parameter__c', 
																				  new List<sObject>(),
																				  5);
			System.assertEquals(!LstPriceGr.isEmpty(),true);
		Test.stopTest();
	}
	
	@isTest static void tstMethod_SearchProducts() {
		Test.startTest();
			ISSM_MultiSelectLookUp_cls.WrapperClass[] LstWrapper = new List<ISSM_MultiSelectLookUp_cls.WrapperClass>();
			LstWrapper  = ISSM_MultiSelectLookUp_cls.SearchProducts('Name, ONTAP__ProductId__c, ONTAP__MaterialProduct__c, ONTAP__ProductShortName__c',
																    '005',
																    'ONTAP__Product__c', 
																    new List<sObject>(),
																    5);
			System.assertEquals(!LstWrapper.isEmpty(),true);
		Test.stopTest();
	}

	@isTest static void tstMethod_SearchQuotas() {
		Test.startTest();
			ISSM_MultiSelectLookUp_cls.WrapperClass[] LstWrapper = new List<ISSM_MultiSelectLookUp_cls.WrapperClass>();
			LstWrapper  = ISSM_MultiSelectLookUp_cls.SearchQuotas('Name,Code__c,Description__c',
																  'MDM',
																  'MDM_Parameter__c', 
																  new List<sObject>(),
																  5);
			System.assertEquals(!LstWrapper.isEmpty(),true);
		Test.stopTest();
	}

	@isTest static void tstMethod_getOptions() {
		Test.startTest();
			String[] LstString  = ISSM_MultiSelectLookUp_cls.getOptions(new ISSM_Combos__c(),
																		'ISSM_TypeApplication__c');
			System.assertEquals(!LstString.isEmpty(),true);
		Test.stopTest();
	}
}