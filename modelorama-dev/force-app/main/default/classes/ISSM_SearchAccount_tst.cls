/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Clase de prueba para el controlador ISSM_SearchAccount_CTR

    Information about changes (versions)
    ================================================================================================
    Number    Dates           	Author                       Description				Cobertura
    ------    -----------       --------------------------   -----------------------------------------
    1.0       30-Nov-2017   	Luis Licona                  Creación de la Clase   	100%
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_SearchAccount_tst {
	public static ISSM_OnCallQueries_cls CTRSOQL 	= new ISSM_OnCallQueries_cls();
	public static Account ObjAccount = new Account();
	
	public static Account ObjAccount1 = new Account();
	public static Account ObjAccount2 = new Account();
	public static Account ObjAccount3 = new Account();
	public static ONTAP__Route__c objONTAPRoute2;
	public static AccountByRoute__c objAccxRoute2{get;set;}
	public static AccountByRoute__c ObjAccByRoute{get;set;}

	static Profile p{get;set;}
	static User user1{get;set;}
	static Id IdRec{get;set;}
	static User userTest{get;set;}

	static void createData()
    {
		userTest = new User(
		    ProfileId = [SELECT Id FROM Profile WHERE Name = :'ISSM-SalesTeam'].Id,
		    LastName = 'Test',
		    Email = 'asdasdasd@amamama.com',
		    Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
		    CompanyName = 'TEST2',
		    Title = 'title',
		    Alias = 'alias',
		    TimeZoneSidKey = 'America/Los_Angeles',
		    EmailEncodingKey = 'UTF-8',
		    LanguageLocaleKey = 'en_US',
		    LocaleSidKey = 'en_US'
    	);
        insert userTest;                       
            
        objAccount3                     = new Account();
        objAccount3.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        objAccount3.Name                = 'CMM Test3';
        objAccount3.ONTAP__SalesOgId__c = '3117';
        insert objAccount3;

        objAccount2                      = new Account();
        objAccount2.RecordTypeId         = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        objAccount2.Name                 = 'CMM Test';
        objAccount2.ONTAP__SalesOffId__c = 'FZ08';
        insert objAccount2;

        objAccount                             = new Account();
        objAccount.RecordTypeId                = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        objAccount.Name                        = 'CMM Dolores FZ08';
        objAccount.ISSM_SalesOffice__c         = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c      = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c = null;
        objAccount.ONTAP__SalesOgId__c         = '3117';
        objAccount.ONTAP__SalesOffId__c        = 'FZ08';
        objAccount.ONTAP__SAP_Number__c        = '0100169817';
        objAccount.ONTAP__Main_Phone__c        = '5510988765';
		objAccount.ISSM_MainContactE__c        = true;
		objAccount.ONTAP__ChannelId__c         = '01';
		objAccount.ONCALL__Ship_To_Name__c     = '0100169817';
		objAccount.ISSM_StartCall__c 		   = true;
        insert objAccount;

        objONTAPRoute2                       = new ONTAP__Route__c();
        objONTAPRoute2.RecordTypeId       	 = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
        objONTAPRoute2.ServiceModel__c       = Label.ISSM_OriginCall;
        objONTAPRoute2.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute2.RouteManager__c       = userTest.Id;
        objONTAPRoute2.Supervisor__c         = userTest.Id;
        objONTAPRoute2.ISSM_SMSubgroup__c   = Label.ISSM_NationalAccounts;
        insert objONTAPRoute2;        
  	}
	
	@isTest static void SearchAccountWithParameters() {
			createData();
			PageReference Pref;
			ISSM_SearchAccount_ctr CTR = new ISSM_SearchAccount_ctr();
			CTR.query = '';
			Pref = CTR.runQuery();
			CTR.query = 'XX';
			Pref = CTR.runQuery();
	}
}