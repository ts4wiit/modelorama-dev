/******************************************************************************* 
* Developed by      : 	Avanxo México
* Author				: 	Oscar Alvarez
* Project			: 	AbInbev - Trade Revenue Management
* Description		:   Class that generates the request and response for the integration of the creation of class condition in SAP
*
* No.       Date              Author                      Description
* 1.0    28-Agosto-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
public with sharing class TRM_SendClassConditionToSAP_cls {
	Public static Integer counterRetries = 0;
    Public static List<ISSM_WebServicesLog__c> lstWebServicesLog= new List<ISSM_WebServicesLog__c>();
    /**
    * @description  Method that invokes the Process builder 'TRM_SendClassConditionToSAP'. Process: 
    *                   1.- Validate that the filtered regirtro is not in a settlement period, if it is within a period of settlement only the flag 'TRM_MarkedLiquidation__c' is updated. 
    *                   2.- If it is not within a settlement period, the JSON is generated and subsequently sent to SAP 
    *   @param      lstConditionClass   List of objects of type 'TRM_ConditionClass__c'
    * 
    *   @return     No return
    */
    @InvocableMethod  
    public static void sendClassConditionToSAP(List<TRM_ConditionClass__c> lstConditionClass){

        System.debug('####START INTEGRATION (TRM_SendClassConditionToSAP_cls)');     
        List<boolean> lstBoolean = new List<boolean>();   
        String operation = lstConditionClass[0].TRM_Status__c == 'TRM_Approved' ? Label.TRM_CreateCC : Label.TRM_CancelCC;
        String typeCondition = lstConditionClass[0].TRM_ConditionClass__c;
        Set<String> lstNameCondWithPeriod = new Set<String>();
        boolean isSettlementPeriod = false;
        Integer numberLots;
        Map<String,TRM_SettlementPeriods__c> mapSettlementPeriod = new Map<String,TRM_SettlementPeriods__c>();

        List<TRM_SettlementPeriods__c> settlementPeriods = [SELECT TRM_ConditionClas__c
                                                                    ,TRM_Description__c
                                                                    ,TRM_RecordVolume__c
                                                                    ,TRM_ExternalKey__c
                                                                    ,TRM_EffectiveDate__c
                                                                    ,TRM_StartTime__c
                                                                    ,TRM_EndTime__c
                                                            FROM TRM_SettlementPeriods__c];        
        for (TRM_SettlementPeriods__c period: settlementPeriods) mapSettlementPeriod.put(period.TRM_ConditionClas__c,period);        

        lstNameCondWithPeriod = mapSettlementPeriod.keySet();
        if(lstNameCondWithPeriod.contains(typeCondition)){
            isSettlementPeriod = TRM_ApprovalConditions_ctr.isInTheDateTimePeriod(mapSettlementPeriod.get(typeCondition));  
            numberLots = (Integer) [SELECT TRM_RecordVolume__c 
                                    FROM TRM_SettlementPeriods__c 
                                    WHERE TRM_ConditionClas__c =:typeCondition][0].TRM_RecordVolume__c;
        }
        if(!isSettlementPeriod){
            String strJSON = TRM_ApprovalConditions_ctr.generateJson(lstConditionClass, operation, (numberLots != null ? numberLots : Integer.valueOf(Label.TRM_NumberLotsProcessed)));
            if(operation == Label.TRM_CreateCC && lstConditionClass[0].TRM_ReadyApproval__c) sendClassCondNotFuture(strJSON,operation);
            if(operation == Label.TRM_CreateCC && !lstConditionClass[0].TRM_ReadyApproval__c) sendClassCondFuture(strJSON,operation);
            if(operation == Label.TRM_CancelCC) sendClassCondFuture(strJSON,operation);
        }
        if(isSettlementPeriod || (Test.isRunningTest() && operation == Label.TRM_CancelCC)){
            lstConditionClass[0].TRM_MarkedLiquidation__c = true;
            if(operation == Label.TRM_CreateCC && lstConditionClass[0].TRM_ReadyApproval__c) update lstConditionClass;
            if(operation == Label.TRM_CreateCC && !lstConditionClass[0].TRM_ReadyApproval__c) updateCheckPeriodLiquidation(lstConditionClass[0].Id);
            if(operation == Label.TRM_CancelCC) updateCheckPeriodLiquidation(lstConditionClass[0].Id);
            
        }
    }
    /**
    * @description  Future method, puts in TRUE the flag of 'TRM_MarkedLiquidation__c' for the settlement period 
    *   @param      strId   Class Id condition to update
    * 
    *   @return     No return
    */
    @Future(callout=true)
     public static void updateCheckPeriodLiquidation(String strId){        
        List<TRM_ConditionClass__c> lstConditionClass = [SELECT TRM_MarkedLiquidation__c FROM TRM_ConditionClass__c where id =: strId];
        lstConditionClass[0].TRM_MarkedLiquidation__c = true;
        update lstConditionClass;
     }
    /**
    * @description  - Make the call to the mulesoft service and save the service response in 'ISSM_WebServicesLog__c' 
    *               - In case of connection service failure with Mulesoft, the flag of 'TRM_FailedSendtoSap__c' is left in TRUE for a later retry 
    *                 of the condition classes involved 
    *
    *   @param      strSerializeJson   Structure of JSON that will be sent to SAP
    *   @param      operation          C- create and B- Marked to delete
    * 
    *   @return     No return
    */
    @Future(callout=true)
    public static void sendClassCondFuture(String strSerializeJson,String operation){
        System.debug('####START (sendToMulesoft)'); 
        String nameConfigCustom = Label.TRM_CustomConfigCC;   
        ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
        try{
            List<ISSM_CallOutService_ctr.WprResponseService> response = new List<ISSM_CallOutService_ctr.WprResponseService>();
            response = CTR.SendRequest(strSerializeJson,nameConfigCustom); 
            System.debug('####response: '+ response);
            saveLog(response, nameConfigCustom, strSerializeJson, operation); 
            if(Test.isRunningTest()) throw new MyException('Exception test');
           }catch(Exception exc){System.debug('####ERROR: ' + exc + ' Message: ' + exc.getMessage());} 

        insert lstWebServicesLog;

        // agrego la marca de error encaso de que el servicio haya fallado
        if(counterRetries == Integer.valueOf(Label.TRM_NumberOfRetries)){
            WrapperConditions wprJSONError = (WrapperConditions) System.JSON.deserialize(strSerializeJson,WrapperConditions.class);
            List<TRM_ConditionClass__c> lstConditionUpdate = [SELECT Id,TRM_FailedSendtoSap__c,TRM_ExternalId__c 
                                                              FROM TRM_ConditionClass__c 
                                                              WHERE TRM_ExternalId__c IN :wprJSONError.ExternalIDs];
            for(TRM_ConditionClass__c cc :lstConditionUpdate) cc.TRM_FailedSendtoSap__c = true;
            update lstConditionUpdate;
        } 
    }
    /**
    * @description  - Make the call to the mulesoft service and save the service response in 'ISSM_WebServicesLog__c' 
    *               - In case of connection service NOT failure with Mulesoft, the flag of 'TRM_FailedSendtoSap__c' is left in TRUE for a later retry 
    *                 of the condition classes involved 
    *
    *   @param      strSerializeJson   Structure of JSON that will be sent to SAP
    *   @param      operation          C- create and B- Marked to delete
    * 
    *   @return     No return
    */
    public static void sendClassCondNotFuture(String strSerializeJson,String operation){
        System.debug('####START (sendToMulesoft)'); 
        String nameConfigCustom = Label.TRM_CustomConfigCC;   
        ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
        try{
            List<ISSM_CallOutService_ctr.WprResponseService> response = new List<ISSM_CallOutService_ctr.WprResponseService>();
            response = CTR.SendRequest(strSerializeJson,nameConfigCustom); 
            System.debug('####response: '+ response);
            saveLog(response, nameConfigCustom, strSerializeJson, operation); 
            if(Test.isRunningTest()) throw new MyException('Exception test');
           }catch(Exception exc){System.debug('####ERROR: ' + exc + ' Message: ' + exc.getMessage());} 

        insert lstWebServicesLog;
        // agrego la marca de error encaso de que el servicio haya fallado
        if(counterRetries == Integer.valueOf(Label.TRM_NumberOfRetries)){
            WrapperConditions wprJSONError = (WrapperConditions) System.JSON.deserialize(strSerializeJson,WrapperConditions.class);
            List<TRM_ConditionClass__c> lstConditionUpdate = [SELECT Id,TRM_FailedSendtoSap__c,TRM_ExternalId__c 
                                                              FROM TRM_ConditionClass__c 
                                                              WHERE TRM_ExternalId__c IN :wprJSONError.ExternalIDs];
            for(TRM_ConditionClass__c cc :lstConditionUpdate) cc.TRM_FailedSendtoSap__c = true;
            update lstConditionUpdate;
        }
    }
    /**
    * @description  - Method for the call of the mulesoft service
    *
    *   @param      strSerializeJson   Structure of JSON that will be sent to SAP
    *   @param      operation          C- create and B- Marked to delete
    * 
    *   @return     No return
    */
    public static void sendClassCondRetry(String strSerializeJson,String operation){
        String nameConfigCustom = Label.TRM_CustomConfigCC;   
        ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
        try{
            List<ISSM_CallOutService_ctr.WprResponseService> response = new List<ISSM_CallOutService_ctr.WprResponseService>();
            response = CTR.SendRequest(strSerializeJson,nameConfigCustom); 
            System.debug('####response: '+ response);
            saveLog(response, nameConfigCustom, strSerializeJson, operation); 
            if(Test.isRunningTest()) throw new MyException('Exception test');
           }catch(Exception exc){System.debug('####ERROR: ' + exc + ' Message: ' + exc.getMessage());}  
           
    }
    /**
    * @description      if it fell into error it retry (Retry is controlled with the label Label.TRM_NumberOfRetries)
    *   @Param        response              wrapper of response; 
    *   @Param        requestSerializeJson  request JSON;
    *   @Param        operation             C- create and B- Marked to delete      
    */
    private static void saveLog(List<ISSM_CallOutService_ctr.WprResponseService> response
                                   , String nameConfigCustom
                                   , String requestSerializeJson
                                   , String operation){
        System.debug('####counterRetries: '+ counterRetries);                            
        counterRetries ++;
        Map<String, ISSM_PriceEngineConfigWS__c> MapConfiguracionWs = ISSM_PriceEngineConfigWS__c.getAll();                                
        Integer maxLengthRequest = Integer.valueOf(Label.TRM_MaxLengthRequest);
        ISSM_WebServicesLog__c webServiceLog = new ISSM_WebServicesLog__c();                           
            webServiceLog.ISSM_EndPoint__c = MapConfiguracionWs.get(nameConfigCustom).ISSM_EndPoint__c;
            webServiceLog.ISSM_Message__c = response[0].strMessageError != null ? Label.TRM_Error :Label.TRM_Success;
            webServiceLog.ISSM_Request__c = requestSerializeJson.length() > maxLengthRequest ? requestSerializeJson.substring(0, maxLengthRequest) : requestSerializeJson;
            webServiceLog.ISSM_Response__c = response[0].strBodyService != null ? response[0].strBodyService : response[0].strMessageError;
            webServiceLog.ISSM_Source__c = operation == Label.TRM_CreateCC ? (Label.TRM_CreateCondition + ' - ' + Label.TRM_CreateCC) : (operation == Label.TRM_CancelCC ? (Label.TRM_CancelCondition + ' - ' + Label.TRM_CancelCC) : '');
        lstWebServicesLog.add(webServiceLog);
            if(Test.isRunningTest()) webServiceLog.ISSM_Message__c =  Label.TRM_Error;
        if(webServiceLog.ISSM_Message__c == Label.TRM_Error &&  counterRetries < Integer.valueOf(Label.TRM_NumberOfRetries)){
                sendClassCondRetry(requestSerializeJson,operation);
        }
    }
    //wrapper for the structure of the JSON that will be sent to SAP
    public class WrapperConditions{
        public String Operation;
        public Set<String> ExternalIDs;
        public Integer NumberLots;
        public Integer NumberLoops;

        public WrapperConditions(String operation,Set<String> externalIDs,Integer numberLots,Integer numberLoops){
            this.Operation      = operation;
            this.ExternalIDs    = externalIDs;
            this.NumberLots     = numberLots;
            this.NumberLoops    = numberLoops;
        } 
    }
    public class MyException extends Exception{}
}