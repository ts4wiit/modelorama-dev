/**************************************************************************************
Name apex class: ISSM_DataCleasingJob_sch
Version: 1.0
Createdate: 29 June 2018
Functionality: Class that schedules the batch class ISSM_DataCleasingJob_bch, Remove old data from the objects submited at the custom setting "ISSM_DataCleasingJob__c" and
               additionally, the option "Delete_to_Recycle_Bin__c" allows to remove deleted data from the recycling bin.
Test class: ISSM_DataCleasingJob_tst
Modifications history:
-----------------------------------------------------------------------------
* Developer            -       Date       -        Description
* ----------------------------------------------------------------------------
* Carlos Duque         26 - September - 2017      Original version
* Leopoldo Ortega        29 - June - 2018         Change way to execute batch with parameters from the new custom settings
*************************************************************************************/
global class ISSM_DataCleasingJob_sch implements Schedulable {
    global void execute(SchedulableContext sc) {
        // Variables
        List<ISSM_DataCleasingJob__c> config_lst = new List<ISSM_DataCleasingJob__c>();
        Integer batchSize_int = 0;
        // Constants
        Integer MAXBATCHSIZE = 10000; // Control's constant
        
        // Query of the custom settings
        config_lst = [SELECT Id, Name, Order__c, Batch_size__c, IsActive__c FROM ISSM_DataCleasingJob__c WHERE IsActive__c = true ORDER BY Order__c ASC LIMIT 1];
        if (config_lst.size() > 0) {
            // Parse the list of the custom settings
            for (ISSM_DataCleasingJob__c reg : config_lst) {
                // Validate that the batch have a valid value
                if ((reg.Batch_size__c == null) || (reg.Batch_size__c == 0) || (reg.Batch_size__c > MAXBATCHSIZE)) {
                    batchSize_int = MAXBATCHSIZE; } else { batchSize_int = Integer.ValueOf(reg.Batch_size__c); }
                
                // Execute batch with the name found of the custom settings and the order with the small value
                ISSM_DataCleasingJob_bch intObjectbch = new ISSM_DataCleasingJob_bch(reg.Name, Integer.ValueOf(reg.Order__c));
                database.executebatch(intObjectbch, batchSize_int);
            }
        }
    }
}