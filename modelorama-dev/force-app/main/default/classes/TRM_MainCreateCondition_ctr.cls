/**
 * Development by:      Avanxo Mexico
 * Author:              Carlos Pintor (cpintor@avanxo.com) / Luis Licona
 * Project:             AbInbev - Trade Revenue Management
 * Description:         Apex Controller class of the Lightning Component 'TRM_MainCreateCondition_lcp'.
 *                      Methods included achives the processes: 
 *                      - get Custom Metadata Types for the Condition Class
 *                      - get a sObject Record from Salesforce
 *                      - get Catalogs of Condition Class
 *                      - get Sales Structures for Customers
 *                      - delete a Condition Class record
 *
 * N.     Date             Author                     Description
 * 1.0    2018-08-23       Carlos Pintor              Created
 *
 */
public with sharing class TRM_MainCreateCondition_ctr {

	/*@AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        return theme;
    }*/

    /**
    * @description  Receives a string with the externalKey to search in the Custom Metadata Type 'TRM_ConditionsManagement__mdt' 
    *               if the externalKey is found, the record of Custom Metatada Type is returned. 
    * 
    * @param    externalKey     String of the externalKey to search in Custom Metadata Type. E.g: 'ZMIW_971'
    * 
    * @return   Return a record of type 'TRM_ConditionsManagement__mdt'
    */
	@AuraEnabled
	public static TRM_ConditionsManagement__mdt getConditionManagement(String externalKey) {
        System.debug('#### start TRM_MainCreateCondition_ctr.getConditionManagement()');
		TRM_ConditionsManagement__mdt conditionManagement;
		String queryString = 'SELECT ';
				queryString += String.join(new List<String>(SObjectType.TRM_ConditionsManagement__mdt.Fields.getMap().keySet()),', ');
				queryString += ' FROM TRM_ConditionsManagement__mdt';
				queryString += ' WHERE TRM_ExternalKey__c = \'' + externalKey + '\'';
		List<TRM_ConditionsManagement__mdt> resultList = ISSM_UtilityFactory_cls.executeQuery(queryString);

		if(resultList.size() == 1){
			conditionManagement = resultList[0];
		}
		System.debug('#### conditionManagement: ' + conditionManagement);
		return conditionManagement;
	}

    /**
    * @description  Receives a string with the externalKey to search in the Custom Metadata Type 'TRM_ConditionRelationships__mdt' 
    *               if the externalKey is found in the parent relationship 'TRM_ConditionManagement__r.TRM_ExternalKey__c', 
    *               the record of Custom Metatada Type is returned. 
    * 
    * @param    externalKey     String of the externalKey to search in Custom Metadata Type. E.g: 'ZMIW_971'
    * 
    * @return   Return a record of type 'TRM_ConditionRelationships__mdt'
    */
	@AuraEnabled
	public static TRM_ConditionRelationships__mdt getConditionRelationship(String externalKey) {
        System.debug('#### start TRM_MainCreateCondition_ctr.getConditionRelationship()');
		TRM_ConditionRelationships__mdt conditionRelationship;
		String queryString = 'SELECT ';
				queryString += String.join(new List<String>(SObjectType.TRM_ConditionRelationships__mdt.Fields.getMap().keySet()),', ');
				queryString += ' FROM TRM_ConditionRelationships__mdt';
				queryString += ' WHERE TRM_ConditionManagement__r.TRM_ExternalKey__c = \'' + externalKey + '\'';
		List<TRM_ConditionRelationships__mdt> resultList = ISSM_UtilityFactory_cls.executeQuery(queryString);

		if(resultList.size() == 1){
			conditionRelationship = resultList[0];
		}
        System.debug('#### conditionRelationship: ' + conditionRelationship);
		return conditionRelationship;
	}

    /**
    * @description  Receives a string with the sObject type and the Id of the record to get from the server
    * 
    * @param    objectType     String of the sObject type to retrieve. E.g: 'TRM_ConditionClass__c'
    * @param    id             String of the id of the record to retrieve. E.g: '5tdS4l3sf0rc31d'
    * 
    * @return   Return a sObject record 
    */
	@AuraEnabled
	public static sObject getRecordById(String objectType, String id) {
        System.debug('#### start TRM_MainCreateCondition_ctr.getRecordById()');
		sObject returnValue;
		if( id != null || id != '' ){
			returnValue = ISSM_UtilityFactory_cls.getRecordById(objectType, id);
		}
        System.debug('#### record: ' + returnValue);
		return returnValue;
	}

    /**
    * @description  Get Catalogs for price zone and segment
    * @param    none 
    * @return   Return Map<String,String> for get estructures
    */
	@AuraEnabled
    public static Map<String,String> getCatalog() {
    	String strCatalog1 = 'PriceZone';
    	String strCatalog2 = 'Segmento';
    	Map<String,String> MapCatalog 	  = new Map<String,String>();
    	MDM_Parameter__c[] lstCatalog = new List<MDM_Parameter__c>();

    	String  sQuery= 'SELECT Id, ExternalId__c';
                sQuery+=' FROM MDM_Parameter__c';
                sQuery+=' WHERE';
                sQuery+=' (Catalog__c = '+'\'' + strCatalog1 + '\'';
                sQuery+=' OR Catalog__c = '+'\'' + strCatalog2 + '\'';
                sQuery+=' ) AND Active_Revenue__c = true AND Active__c = true';
                sQuery+=' ORDER BY createdDate DESC';

        lstCatalog = ISSM_UtilityFactory_cls.executeQuery(sQuery);

        for(MDM_Parameter__c obj : lstCatalog){
            MapCatalog.put(obj.ExternalId__c, obj.Id);
        }

        return MapCatalog;
    }

    /**
    * @description  Get sales offices and sales orgs 
    * @param    none 
    * @return   Return Map<String,String> for get estructures
    */
    @AuraEnabled
    public static Map<String,String> getStructures() {
    	String recordTypeOff = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('Account','SalesOffice');
    	String recordTypeOrg = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('Account','SalesOrg');
    	Map<String,String> MapStructures = new Map<String,String>();
    	List<Account> lstAccount 	 = new List<Account>();

    	String  sQuery= 'SELECT Id, ONTAP__ExternalKey__c';
                sQuery+=' FROM Account';
                sQuery+=' WHERE';
                sQuery+=' (RecordTypeId = '+'\'' +recordTypeOff+ '\'';
                sQuery+=' OR RecordTypeId = '+'\'' +recordTypeOrg+ '\'';
                sQuery+=' )ORDER BY createdDate DESC';

        lstAccount = ISSM_UtilityFactory_cls.executeQuery(sQuery);

        for(Account obj : lstAccount){
            MapStructures.put(obj.ONTAP__ExternalKey__c, obj.Id);
        }

        return MapStructures;
    }

    /**
    * @description  Receives a object of type 'TRM_ConditionClass__c' to delete
    * 
    * @param    record     The record of type 'TRM_ConditionClass__c' to delete
    * 
    * @return   Return the record deleted TRM_ConditionClass__c
    */
    @AuraEnabled
    public static TRM_ConditionClass__c deleteRecord(TRM_ConditionClass__c record) {
        System.debug('#### start TRM_MainCreateCondition_ctr.deleteRecord()');
        delete record;
        return record;
    }

}