/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows to obtain the limits according to the level of the combo

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    01-Junio-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class ISSM_Popover_ctr {
	/**
    * @description  	   Receives the registration id as a parameter, to obtain the list of necessary fields 
    *                	   during the update of the combo IN ISSM_ComboLimit__c
    * 
    * @param    IdRec      Id of record to return
    * 
    * @return              Return a list of type 'ISSM_ComboLimit__c' for Combo Limit record
    */
    @AuraEnabled
	public static ISSM_ComboLimit__c[] getLimitRecords(String IdRec){

		ISSM_Combos__c ObjCombo = new ISSM_Combos__c();
		ISSM_ComboLimit__c[] LstLimitCombo = new List<ISSM_ComboLimit__c>();

		ObjCombo = [SELECT  ISSM_ComboLimit__r.ISSM_ComboLevel__c 
					FROM 	ISSM_Combos__c 
					WHERE 	Id=: IdRec];

		LstLimitCombo = [SELECT Id, ISSM_ComboLevel__c,
								ISSM_SalesStructure__r.Name 
						FROM 	ISSM_ComboLimit__c 
						WHERE 	ISSM_ComboLevel__c =: ObjCombo.ISSM_ComboLimit__r.ISSM_ComboLevel__c];

		return LstLimitCombo;
	}
}