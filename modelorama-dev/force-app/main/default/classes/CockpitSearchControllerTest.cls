/*-----------------------------------------------------------------------------------------------------------------------
 * Name: CockpitSearchControllerTest
 * Description : Test class for CockpitSearchController.
 * Created date : August 24 2018
 * @author name: Russell Perez
 * ----------------------------------------------------------------------------------------------------------------------
 * Developer				Date				Description<p />
 * ----------------------------------------------------------------------------------------------------------------------
 * Russell Perez			24/07/2018			Original version.<p />
 * Russell Perez			22/08/2018			Test methods to Collapsable Tables.<p />
 * Daniel Goncalves			03/09/2018			Test Method for generate CSV File.<p />
 * Alberto Gómez			12/09/2018			Eliminate testSetup and create data inside startTest()/stopTest().<p />
 * Alberto Gómez			24/09/2018			Edited testFetchClientVis method to get Visit data.
 * -----------------------------------------------------------------------------------------------------------------------
 */
@isTest
public class CockpitSearchControllerTest {

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsDRV() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase0 = CockpitSearchController.fetchResults('DRV', 'GM0006', 'option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsORG() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase1 = CockpitSearchController.fetchResults('Sales Org', '3104', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOffice() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase2 = CockpitSearchController.fetchResults('Sales Office', 'FU05', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsRoute() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase3 = CockpitSearchController.fetchResults('Route', 'FD0101', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsTour() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase4 = CockpitSearchController.fetchResults('Tour ID', 'S6000', 'option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsDRVTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase5 = CockpitSearchController.fetchResults('DRV', objOntapTour.DRVId__c, 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOrgTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase6 = CockpitSearchController.fetchResults('Sales Org', objOntapTour.OrgId__c, 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOfficeTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase7 = CockpitSearchController.fetchResults('Sales Office', objOntapTour.SalesOffice__c, 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsRouteTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase8 = CockpitSearchController.fetchResults('Route', objOntapTour.ONTAP__RouteId__c, 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsDRVNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase0Null = CockpitSearchController.fetchResults('DRV', 'GM0007', 'option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOrgNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase1Null = CockpitSearchController.fetchResults('Sales Org', '3105', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOfficeNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase2Null = CockpitSearchController.fetchResults('Sales Office', 'FU07', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsRouteNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase3Null = CockpitSearchController.fetchResults('Route', 'FD0103', '', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsTourNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase4Null = CockpitSearchController.fetchResults('Tour ID', 'T-111', 'option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsDRVTourStatusNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase5Null = CockpitSearchController.fetchResults('DRV', 'GM0007', 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOrgTourStatusNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase6Null = CockpitSearchController.fetchResults('Sales Org','3105', 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsOfficeTourStatusNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase7Null = CockpitSearchController.fetchResults('Sales Office','FU07', 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResults method.
	 */
	@isTest
	public static void testFetchResultsRouteTourStatusNull() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__tour__c> lstObjOntapTour = [SELECT Id, DRVId__c, ONTAP__TourDate__c, OrgId__c, SalesOffice__c, ONTAP__RouteId__c FROM ONTAP__tour__c LIMIT 1];
		ONTAP__tour__c objOntapTour = null;

		if(!lstObjOntapTour.isEmpty()) {
			objOntapTour = lstObjOntapTour.get(0);
			List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRCase8Null = CockpitSearchController.fetchResults('Route','FD0103', 'Finished', String.valueOf(objOntapTour.ONTAP__TourDate__c));
		}
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchResultsDrillDown method.
	 */
	@isTest
	public static void testFetchResultsDrillDown() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRDDCase1 = CockpitSearchController.fetchResultsDrillDown('0','GM0006','option', String.valueOf(Date.newInstance(2020, 12, 31)));
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRDDCase2 = CockpitSearchController.fetchResultsDrillDown('1','3104','option', String.valueOf(Date.newInstance(2020, 12, 31)));
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRDDCase3 = CockpitSearchController.fetchResultsDrillDown('2','FU05','option', String.valueOf(Date.newInstance(2020, 12, 31)));

		//Null case.
		List<CockpitSearchController.ResultsMatrix> CSCResultMatrixFRDDCaseNull = CockpitSearchController.fetchResultsDrillDown('0','GM0008','option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchFTRoute method.
	 */
	@isTest
	public static void testFetchFTRoute() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.routeTB> CSCRouteTBFFTR000 = CockpitSearchController.fetchFTRoute('S6000807039');
		List<CockpitSearchController.routeTB> CSCRouteTBFFTR001 = CockpitSearchController.fetchFTRoute('');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchSTChIChOHd method.
	 */
	@isTest
	public static void testFetchSTChIChOHd() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__COCIHeader__c> lstCSCFetchSTChIChOHd002 = CockpitSearchController.fetchSTChIChOHd('S6000807039');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchSTChIChOIt method.
	 */
	@isTest
	public static void testFetchSTChIChOIt() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__COCIItem__c> lstFetchSTChIChOIt = CockpitSearchController.fetchSTChIChOIt(99, 'S6000807039');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchSTChIChOPy method.
	 */
	@isTest
	public static void testFetchSTChIChOPy() {

		//Start test.
		Test.startTest();
		generateData();
		List<ONTAP__COCI_Payment__c> lstCociPaymFetchSTChIChOPy = CockpitSearchController.fetchSTChIChOPy(99, 'S6000807039');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchTourDoc method.
	 */
	@isTest
	public static void testFetchTourDoc() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitEventDocument__c> lstCEDocumentFetchTourDoc = CockpitSearchController.fetchTourDoc('Abarrotes La Estrella');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchVisitDoc method.
	 */
	@isTest
	public static void testFetchVisitDoc() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitEventDocument__c> lstCEDocumentFetchVisitDoc = CockpitSearchController.fetchVisitDoc('Abarrotes La Estrella', 'S6000807039');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchClient method.
	 */
	@isTest
	public static void testFetchClient() {

		//Start test.
		Test.startTest();
		generateData();
		List<Event> lstObjEvntFetchClient = [SELECT What.Name, VisitList__r.AlternateName__c FROM Event];
		Event objEventFetchClient = null;
		if(lstObjEvntFetchClient.size() > 0){
			objEventFetchClient = lstObjEvntFetchClient.get(0);
		}
		List<CockpitSearchController.clientVisits> lstCSCCVisitFetchClientTour =  CockpitSearchController.fetchClient(objEventFetchClient.VisitList__r.AlternateName__c);
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchClientVis method.
	 */
	@isTest
	public static void testFetchClientVis() {

		//Start test.
		Test.startTest();
		generateData();
		List<Event> lstEventQueried = [SELECT CustomerId__c, EndDateTime, Id, ISSM_RouteId__c, Sequence__c, StartDateTime, VisitList__c, VisitList__r.ONTAP__TourId__c, VisitList__r.RecordType.DeveloperName, VisitList__r.ONTAP__TourDate__c, VisitList__r.ONTAP__TourStatus__c, VisitList__r.TourSubStatus__c, VisitList__r.AlternateName__c, What.Name FROM Event LIMIT 1];
		System.debug('-> lstEventQueried: ' + lstEventQueried);
		Event objEvent0 = new Event();
		if(!lstEventQueried.isEmpty()) {
			objEvent0 = lstEventQueried.get(0);
		}
		List<CockpitSearchController.visitEvents> lstCSCVEventFetchClientVisTour =  CockpitSearchController.fetchClientVis(objEvent0.VisitList__r.AlternateName__c, objEvent0.What.Name);
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchClientSales method.
	 */
	@isTest
	public static void testFetchClientSales() {

		//Start test.
		Test.startTest();
		generateData();
		List<Event> lstEventFetchClientSalesTourClient = CockpitSearchController.fetchClientSales('0014C00000JYWWHQA5', 'a1N4C00000015DnUAI');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the fetchClientBalance method.
	 */
	@isTest
	public static void testFetchClientBalance() {

		//Start test.
		Test.startTest();
		generateData();
		List<Event> lstEventFetchClientBalanceTourClient =  CockpitSearchController.fetchClientBalance('0014C00000JYWWHQA5', 'a1N4C00000015DnUAI');
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the getEstatus method.
	 */
	@isTest
	public static void testGetEstatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<String> lstStrStatus =  CockpitSearchController.getEstatus();
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. DRV level.
	 */
	@isTest
	public static void testGenerateCSVDocDRV() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc0 = CockpitSearchController.generateCSVDoc('DRV', 'GM0006','option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Org level.
	 */
	@isTest
	public static void testGenerateCSVDocOrg() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc1 = CockpitSearchController.generateCSVDoc('Sales Org', '3104','', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Office Level.
	 */
	@isTest
	public static void testGenerateCSVDocOffice() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc2 = CockpitSearchController.generateCSVDoc('Sales Office', 'FU05','', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Route level.
	 */
	@isTest
	public static void testGenerateCSVDocRoute() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc3 = CockpitSearchController.generateCSVDoc('Route', 'FD0101','', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Tour Id.
	 */
	@isTest
	public static void testGenerateCSVDocTourId() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc4 = CockpitSearchController.generateCSVDoc('Tour ID', 'T-000','option', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. DRV level with Tour status.
	 */
	@isTest
	public static void testGenerateCSVDocDRVTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc5 = CockpitSearchController.generateCSVDoc('DRV', 'GM0006','Finished', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}


	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Org level with Tour status.
	 */
	@isTest
	public static void testGenerateCSVDocOrgTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc6 = CockpitSearchController.generateCSVDoc('Sales Org', '3104','Finished', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Office level with Tour status.
	 */
	@isTest
	public static void testGenerateCSVDocOfficeTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc7 = CockpitSearchController.generateCSVDoc('Sales Office', 'FU05','Finished', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method. Route level with Tour status.
	 */
	@isTest
	public static void testGenerateCSVDocRouteTourStatus() {

		//Start test.
		Test.startTest();
		generateData();
		List<CockpitSearchController.DocCSV> lstCSCDocCSVGenerateCSVDoc8 = CockpitSearchController.generateCSVDoc('Route', 'FD0101','Finished', String.valueOf(Date.newInstance(2020, 12, 31)));
		Test.stopTest();
	}

	/**
	 * This method is responsible for testing the GenerateCSVDoc method.
	 */
	@isTest
	public static void testDocCSVClass() {

		//Start test.
		Test.startTest();
		CockpitSearchController.DocCSV objCSearcCtrlDocCSV = new CockpitSearchController.DocCSV('GM0006', '3104', 'FU05', 'CMM Acaponeta', 'FD0101', 'T-000705193', 'YE0501', 'Bonificación', 'Pedido');
		Test.stopTest();
	}

	/**
	 * This method generates all the necessary data to be able to perform the tests.
	 */
	static void generateData() {

		//Mock response.
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//RecordTypes.
		RecordType objRecordTypeAccountSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOrg');
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeOnTapRegionalClient = AllMobileUtilityHelperTest.getRecordTypeObject('Account');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');
		RecordType objRecordTypeAccountDRV = AllMobileUtilityHelperTest.getRecordTypeObject('ISSM_RegionalSalesDivision');

		//Get perfil to user.
		Profile objProfileSystemAdministrator = AllMobileUtilityHelperTest.getProfileObject('System Administrator');

		//Create DRV.
		Account objAccountDRV = new Account();
		objAccountDRV.RecordTypeId = objRecordTypeAccountDRV.Id;
		objAccountDRV.Name = 'DRV Occidente Pacifico';
		objAccountDRV.ONTAP__SAPCustomerId__c = 'GM0006';
		insert objAccountDRV;

		//Create Org.
		Account objSalesOrg = new Account();
		objSalesOrg.RecordTypeId = objRecordTypeAccountSalesOrg.Id;
		objSalesOrg.Name = 'CMM Nayarit';
		objSalesOrg.StartTime__c = '02:00';
		objSalesOrg.EndTime__c = '01:00';
		objSalesOrg.ONTAP__SalesOgId__c = '3104';
		objSalesOrg.ISSM_RegionalSalesDivision__c = objAccountDRV.Id;
		insert objSalesOrg;

		//Create office.
		Account objOffice = new Account();
		objOffice.RecordTypeId = objRecordTypeAccountSalesOffice.Id;
		objOffice.Name = 'CMM Acaponeta';
		objOffice.StartTime__c = '02:00';
		objOffice.EndTime__c = '01:00';
		objOffice.ONTAP__SalesOffId__c = 'FU05';
		objOffice.ISSM_SalesOrg__c = objSalesOrg.Id;
		objOffice.ONTAP__SalesOgId__c = '3104';
		insert objOffice;

		//Create client.
		Account objClient = new Account();
		objClient.RecordTypeId = objRecordTypeOnTapRegionalClient.Id;
		objClient.Name = 'Abarrotes La Estrella';
		objClient.StartTime__c = '01:00';
		objClient.EndTime__c = '01:00';
		objClient.ISSM_SalesOffice__c = objOffice.Id;
		objClient.ONCALL__Order_Block__c = null;
		objClient.ONCALL__SAP_Deleted_Flag__c = null;
		objClient.ONTAP__SalesOgId__c = '3104';
		objClient.ONTAP__SalesOffId__c = 'FU05';
		objClient.ONTAP__SAP_Number__c = '0000000004';
		insert objClient;

		ONTAP__Vehicle__c objVehicle = new ONTAP__Vehicle__c();
		objVehicle.ONTAP__VehicleId__c = '112345-23';
		objVehicle.ONTAP__VehicleName__c = 'Zurdo Movil';
		objVehicle.ONTAP__SalesOffice__c = objOffice.id;
		insert objVehicle;

		User objUser = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'minion001@abinbev.org.dev', 'minion', 'minion@gmail.com', 'ISO-8859-1', 'minion', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUser;

		//Create route.
		ONTAP__Route__c objRoute = new ONTAP__Route__c();
		objRoute.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute.ServiceModel__c = 'Presales';
		objRoute.ONTAP__SalesOffice__c = objOffice.Id;
		objRoute.RouteManager__c = objUser.Id;
		objRoute.Supervisor__c = objUser.id;
		objRoute.Vehicle__c = objVehicle.id;
		objRoute.ONTAP__RouteId__c = 'FD0101';
		insert objRoute;

		//Create accountbyroute.
		AccountByRoute__c objAccByRoute = new AccountByRoute__c();
		objAccByRoute.Route__c = objRoute.Id;
		objAccByRoute.Account__c = objClient.Id;
		objAccByRoute.Saturday__c = true;
		objAccByRoute.Wednesday__c = true;
		objAccByRoute.Thursday__c = true;
		objAccByRoute.Tuesday__c = true;
		objAccByRoute.Friday__c = true;
		objAccByRoute.Monday__c = true;
		objAccByRoute.Sunday__c = false;
		insert objAccByRoute;

		//Create visit plan.
		VisitPlan__c objVisitPlan = new VisitPlan__c();
		objVisitPlan.EffectiveDate__c = System.today().addDays(25);
		objVisitPlan.ExecutionDate__c = System.today().addDays(-15);
		objVisitPlan.Saturday__c = true;
		objVisitPlan.Wednesday__c = true;
		objVisitPlan.Thursday__c = true;
		objVisitPlan.Tuesday__c = true;
		objVisitPlan.Friday__c = true;
		objVisitPlan.Monday__c = true;
		objVisitPlan.Route__c = objRoute.Id;
		insert objVisitPlan;

		//Create account by visit plan.
		AccountByVisitPlan__c objAccByVisitPlan = new AccountByVisitPlan__c();
		objAccByVisitPlan.Account__c = objClient.Id;
		objAccByVisitPlan.VisitPlan__c = objVisitPlan.Id;
		objAccByVisitPlan.Sequence__c = 1;
		objAccByVisitPlan.WeeklyPeriod__c = '1';
		objAccByVisitPlan.LastVisitDate__c = null;
		objAccByVisitPlan.Saturday__c = true;
		objAccByVisitPlan.Thursday__c = true;
		objAccByVisitPlan.Tuesday__c = true;
		objAccByVisitPlan.Friday__c = true;
		objAccByVisitPlan.Monday__c = true;
		objAccByVisitPlan.Wednesday__c = true;
		insert objAccByVisitPlan;

		//Create tour 1 Route 1.
		ONTAP__Tour__c objTour   = new ONTAP__Tour__c();
		objTour.ONTAP__TourId__c = 'T-000705193';
		objTour.AlternateName__c = 'S6000807039';
		objTour.ONTAP__TourStatus__c = 'Finished';
		objTour.VisitPlan__c = objVisitPlan.Id;
		objTour.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour.ONTAP__ActualStart__c = Date.newInstance(2020, 12, 31);
		objTour.ONTAP__ActualEnd__c = Date.newInstance(2021, 1, 5);
		objTour.Route__c = objRoute.Id;
		insert objTour;

		//Create COCIHeader.
		ONTAP__COCIHeader__c objOntapCociHeader = new ONTAP__COCIHeader__c();
		objOntapCociHeader.ONTAP__Tour__c = objTour.Id;
		objOntapCociHeader.ONTAP__TourId__c = 'S6000807039';
		objOntapCociHeader.CheckDirect__c = '1';
		objOntapCociHeader.ONTAP__COCIId__c = 99;
		insert objOntapCociHeader;

		//Create cociitem.
		ONTAP__COCIItem__c objCociItem = new ONTAP__COCIItem__c();
		objCociItem.ONTAP__COCI_Header__c = objOntapCociHeader.Id;
		objCociItem.ONTAP__COCIId__c = 99;
		insert objCociItem;

		//Create cocipayment.
		ONTAP__COCI_Payment__c objCociPaym = new ONTAP__COCI_Payment__c();
		objCociPaym.ONTAP__COCI_Header__c = objOntapCociHeader.Id;
		objCociPaym.ItemNo__c = 1;
		objCociPaym.ONTAP__COCIId__c = 99;
		insert objCociPaym;

		//Create event.
		Event objEvent = new Event();
		objEvent.WhatId = objAccByVisitPlan.Account__c;
		objEvent.Subject = 'Visit';
		objEvent.StartDateTime = DateTime.newInstance(Date.newInstance(2020, 8, 30), Time.newInstance(11, 45, 20, 0));
		objEvent.EndDateTime = DateTime.newInstance(Date.newInstance(2020, 9, 2), Time.newInstance(11, 45, 20, 0));
		objEvent.ONTAP__Estado_de_visita__c = 'Pending';
		objEvent.EventSubestatus__c = 'Visita no iniciada';
		objEvent.Sequence__c = objAccByVisitPlan.Sequence__c;
		objEvent.VisitList__c = objTour.Id;
		objEvent.CustomerId__c = '0000000004';
		insert objEvent;

		//Create event document Visit.
		CockpitEventDocument__c objCEDocument = new CockpitEventDocument__c();
		objCEDocument.CockpitTourId__c = 'T-000705193';
		objCEDocument.CockpitTourLK__c = objTour.Id;
		objCEDocument.CockpitVisitSequence__c = 1;
		objCEDocument.CockpitDocumentType__c = 'Bonificación';
		objCEDocument.CockpitDocumentSubtype__c = 'Pedido';
		objCEDocument.Name = 'YE0501';
		insert objCEDocument;

		//Create event document Tour.
		CockpitEventDocument__c objCEDocumentTour = new CockpitEventDocument__c();
		objCEDocumentTour.CockpitTourId__c = 'T-000705193';
		objCEDocumentTour.CockpitTourLK__c = objTour.Id;
		objCEDocumentTour.CockpitVisitSequence__c = 0;
		objCEDocumentTour.CockpitDocumentType__c = 'Bonificación';
		objCEDocumentTour.CockpitDocumentSubtype__c = 'Entrega';
		objCEDocumentTour.Name = 'YE0502';
		insert objCEDocumentTour;

		//Create Tour 2 Route 1.
		ONTAP__Tour__c objTour2   = new ONTAP__Tour__c();
		objTour2.ONTAP__TourId__c = 'T-000705194';
		objTour2.ONTAP__TourStatus__c = 'Finished';
		objTour2.VisitPlan__c = objVisitPlan.Id;
		objTour2.ONTAP__TourDate__c = Date.newInstance(2020, 12, 30);
		objTour2.Route__c = objRoute.Id;
		insert objTour2;

		//Create Tour 3 Route 1.
		ONTAP__Tour__c objTour3   = new ONTAP__Tour__c();
		objTour3.ONTAP__TourId__c = 'T-000705216';
		objTour3.ONTAP__TourStatus__c = 'Finished';
		objTour3.VisitPlan__c = objVisitPlan.Id;
		objTour3.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour3.Route__c = objRoute.Id;
		insert objTour3;
	}
}