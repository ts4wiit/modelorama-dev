/****************************************************************************************************
    General Information
    -------------------
    author:     Andrés Morales
    email:      lmorales@avanxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Generar información del Expansor en formato PDF

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                        Description          
    ------    --------        --------------------------    -----------------------------------------
    1.0       16/08/2017      Andrés Morales                Class created
    ================================================================================================
****************************************************************************************************/

public with sharing class MDRM_Expansor_Pdf_ctr {
    private List<MDRM_Form__c> lForms {get;set;}
    private List<User> lUser {get;set;}
    public List<MDRM_Market_Research__c> lEstudios_Precios {get;set;}
    public List<MDRM_Modelorama_Payback__c> lPayback {get;set;}

    public MDRM_Form__c FormAccount {get;set;}
    public User oUser {get;set;}
    public MDRM_Modelorama_Payback__c oPayback {get;set;}
    

    public list<Attachment> imgCroquis {get;set;}
    public list<Attachment> imgVolumen {get;set;}
    public list<Attachment> imgZPotenciales {get;set;}
    public list<Attachment> imgNivelSE {get;set;}
    public list<Attachment> imgReporte {get;set;}
    /*public list<Attachment> imgEstudio1 {get;set;}
    public list<Attachment> imgEstudio2 {get;set;}*/
    public list<Attachment> imgEntorno {get;set;}
    public list<Attachment> imgVistas {get;set;}
    public list<Attachment> imgLevantamiento {get;set;}

    public list<Attachment> imgLayout {get;set;}
    public list<Attachment> imgFachada {get;set;}
    public list<Attachment> imgRender {get;set;}
    public list<Attachment> imgPresupuesto {get;set;}
    public list<Attachment> imgOperando {get;set;}
    

    private final Account acct;

    public MDRM_Expansor_Pdf_ctr(ApexPages.StandardController stdController) {
        this.acct = (Account)stdController.getRecord();
        getData();
        getImages();
    }   

    public String getData()
    {       
        //String accountId = Apexpages.currentPage().getParameters().get('account.Id');
        //Obtener Id del Owner de la Cuenta
        List<Account> lAccountOwner = [SELECT OwnerId FROM Account WHERE Id=: acct.Id];
        String sOwnerId;
        //Validar que retorne registros
        if(lAccountOwner!=null && !lAccountOwner.isEmpty()) {
            sOwnerId = lAccountOwner[0].OwnerId;    
        }


        //  *****  Obtener datos del Formulario (Cuestionario)
        lForms = [SELECT MDRM_Location_Analysis__c, MDRM_Corner_Type__c, MDRM_Local_Area__c,
                        MDRM_Employment_Generator__c, MDRM_Population_Density__c, MDRM_Activity_Generator__c, 
                        MDRM_Visibility__c, MDRM_Street_Type__c, MDRM_Parking__c, 
                        MDRM_Speed_Bumps__c, MDRM_Street_Way__c, MDRM_Ridge__c, 
                        MDRM_Average_Foot_Traffic__c, MDRM_Average_Vehicular_Traffic__c,
                        MDRM_ABInBev_50__c, MDRM_ABInBev_50_100__c, MDRM_ABInBev_100__c,
                        MDRM_Heineken_50__c, MDRM_Heineken_50_100__c, MDRM_Heineken_100__c
                    FROM MDRM_Form__c
                    WHERE MDRM_Account_Form__c =: acct.Id
                    ORDER BY CreatedDate DESC NULLS LAST];

        //Validar que retorne registros
        if(lForms!=null && !lForms.isEmpty()) {
            FormAccount = lForms[0];
        }

        //  *****  Obtener datos de Owner
        lUser = [SELECT Name, Division, ONTAP__Business_Unit__c
                    FROM User
                    WHERE Id =: sOwnerId];

        //Validar que retorne registros
        if(lUser!=null && !lUser.isEmpty()) {
            oUser = lUser[0];
        }

        //  *****  Obtener datos de Estudios de precios (Este es un detalle de Cuenta)
        List<MDRM_Market_Research__c> lEstudios_Preciostmp = [SELECT Id, MDRM_Address__c, MDRM_Area__c, MDRM_Income__c,
                                                                MDRM_Contact__c, MDRM_Phone__c, MDRM_Photo__c, MDRM_Square_meter_mkt__c                                 
                                                            FROM MDRM_Market_Research__c
                                                            WHERE MDRM_Account_Market_Research__c =: acct.Id
                                                            ORDER BY CreatedDate DESC
                                                            LIMIT 2];      

        for (MDRM_Market_Research__c EP : lEstudios_Preciostmp){
            //Obtener imagen asignada en attachments
            List<Attachment> imgEstudios = [SELECT Id, Name, Description
                                            FROM Attachment 
                                            WHERE ParentId =: EP.Id
                                            AND ContentType LIKE 'image%'                                       
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1];

            if(imgEstudios!=null && !imgEstudios.isEmpty()) {                
                EP.MDRM_Photo__c = imgEstudios[0].Id;
            }
        }

        lEstudios_Precios = lEstudios_Preciostmp;

        //this.imgReporte = imgReporte;

        //  *****  Obtener datos del Formulario (Cuestionario)
        lPayback = [SELECT MDRM_Estimated_monthly_sales_cartons__c, MDRM_Estimated_Monthly_Sales_Hectolitres__c,
                            MDRM_Average_Price_Box__c, MDRM_Commission_Percentage__c,
                            MDRM_Electrical_Bill_Month__c, MDRM_Wages_Month__c,
                            MDRM_Maintenance_Month__c, MDRM_Others_Month__c,
                            MDRM_Building_Work__c, MDRM_License__c
                    FROM MDRM_Modelorama_Payback__c
                    WHERE MDRM_Account__c =: acct.Id
                    ORDER BY CreatedDate DESC
                    Limit 1];

        //Validar que retorne registros
        if(lPayback!=null && !lPayback.isEmpty()) {
            oPayback = lPayback[0];
        }

        return 'Ok';
    }

    public void getImages(){
        List<Attachment> imgCroquis = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Croquis%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgCroquis = imgCroquis;

        List<Attachment> imgVolumen = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Volumen%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgVolumen = imgVolumen;

        List<Attachment> imgZPotenciales = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'ZPotenciales%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgZPotenciales = imgZPotenciales;

        List<Attachment> imgNivelSE = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'NivelSE%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgNivelSE = imgNivelSE;

        List<Attachment> imgReporte = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Reporte%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgReporte = imgReporte;

        /*List<Attachment> imgEstudio1 = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Estudio1%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgEstudio1 = imgEstudio1;

        List<Attachment> imgEstudio2 = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Estudio2%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgEstudio2 = imgEstudio2;*/

        List<Attachment> imgEntorno = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Entorno%'
                                        ORDER BY CreatedDate ASC];
        this.imgEntorno = imgEntorno;

        List<Attachment> imgVistas = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Vistas%'
                                        ORDER BY CreatedDate ASC];
        this.imgVistas = imgVistas;

        List<Attachment> imgLevantamiento = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Levantamiento%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgLevantamiento = imgLevantamiento;

        List<Attachment> imgLayout = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Layout%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgLayout = imgLayout;

        List<Attachment> imgFachada = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Fachada%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgFachada = imgFachada;

        List<Attachment> imgRender = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Render%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgRender = imgRender;

        List<Attachment> imgPresupuesto = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Presupuesto%'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];
        this.imgPresupuesto = imgPresupuesto;

        List<Attachment> imgOperando = [SELECT Id, Name, Description 
                                        FROM Attachment 
                                        WHERE ParentId = :this.acct.Id 
                                        AND ContentType LIKE 'image%'
                                        AND Name LIKE 'Operando%'
                                        ORDER BY CreatedDate ASC];
        this.imgOperando = imgOperando;
    }
}