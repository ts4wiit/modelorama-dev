/**
 * This class contains the operations for synchronization of the AllMobileApplicationVersion__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileApplicationOperationClass implements Queueable {

	String strEventTriggerFlag;
	List<AllMobileApplication__c> lstAllMobileApplication = new List<AllMobileApplication__c>();

	//Constructor.
	public AllMobileApplicationOperationClass(List<AllMobileApplication__c> lstAllMobileApplicationTrigger, String strEventTriggerFlagTrigger) {
		strEventTriggerFlag = strEventTriggerFlagTrigger;
		lstAllMobileApplication = lstAllMobileApplicationTrigger;
	}

	/**
	 * This method initiaties the synchronization to insert AllMobileApplication to Heroku.
	 *
	 * @param lstAllMobileApplication	List<AllMobileApplication__c>
	 */
	public static void syncInsertAllMobileApplicationToExternalObject(List<AllMobileApplication__c> lstAllMobileApplication) {
		Set<Id> setIdAllMobileApplication = new Set<Id>();
		if(!lstAllMobileApplication.isEmpty()) {
			setIdAllMobileApplication = getSetIdAllMobileApplicationSFFromListAllMobileApplicationSF(lstAllMobileApplication);
			AllMobileApplicationOperationClass.insertExternalAllMobileApplicationWS(setIdAllMobileApplication);
		}
	}

	/**
	 * This method initiates the asynchronus communication to insert AllMobileApplication to Heroku.
	 *
	 * @param setIdAllMobileApplication	Set<Id>
	 */
	@future(callout = true)
	public static void insertExternalAllMobileApplicationWS(Set<Id> setIdAllMobileApplication) {
		AllMobileApplicationOperationClass.insertExternalAllMobileApplication(setIdAllMobileApplication);
	}

	/**
	 * This method sync a List of Application objects from Salesforce into a Heroku application table using POST method (insert).
	 *
	 * @param setIdAllMobileApplication Set<Id>
	 */
	public static Boolean insertExternalAllMobileApplication(Set<Id> setIdAllMobileApplication) {

		//Variables.
		List<AllMobileApplication__c> lstAllMobileApplicationToInsertHeroku = new List<AllMobileApplication__c>();
		AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass();

		//Get List of All Application in SF.
		List<AllMobileApplication__c> lstAllMobileApplicationSF = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileShipType__c, AllMobileLevelValidation__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c WHERE Id =: setIdAllMobileApplication];

		//Get List of All Application in HK.
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileApplicationHerokuObjectClass = getAllMobileAllApplicationHeroku();

		//Compare both lists to avoid insert Application SF records that already exists in HK. Generates a new list to be inserted in Heroku.
		for(AllMobileApplication__c objAllMobileApplicationSF :lstAllMobileApplicationSF) {
			Integer intContainedApplicationSFInApplicationHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClassIterate : lstAllMobileApplicationHerokuObjectClass) {
				if(objAllMobileApplicationSF.AllMobileApplicationId__c == String.valueOf(objAllMobileApplicationHerokuObjectClassIterate.application_id)) {
					intContainedApplicationSFInApplicationHeroku++;
				}
			}
			if(intContainedApplicationSFInApplicationHeroku == 0) {
				lstAllMobileApplicationToInsertHeroku.add(objAllMobileApplicationSF);
			}
		}

		//Insert the final list of Application (SF) into Heroku.
		if(!lstAllMobileApplicationToInsertHeroku.isEmpty()) {
			for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplicationToInsertHeroku) {
				objAllMobileApplicationHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass();
				objAllMobileApplicationHerokuObjectClass.application_id = Integer.valueOf(objAllMobileApplication.AllMobileApplicationId__c);
				objAllMobileApplicationHerokuObjectClass.name = objAllMobileApplication.Name;
				objAllMobileApplicationHerokuObjectClass.ship_type = objAllMobileApplication.AllMobileShipType__c;
				objAllMobileApplicationHerokuObjectClass.levelvalidation = Integer.valueOf(objAllMobileApplication.AllMobileLevelValidation__c);
				objAllMobileApplicationHerokuObjectClass.dtlastmodifieddate = objAllMobileApplication.LastModifiedDate;
				objAllMobileApplicationHerokuObjectClass.softdeleteflag = objAllMobileApplication.AllMobileSoftDeleteFlag__c;

				//Rest Callout to insert Application.
				String strStatusCode = AllMobileSyncObjectsClass.calloutInsertApplicationIntoHeroku(objAllMobileApplicationHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method initiates a List of Application objects in Salesforce into Heroku application table using PUT method (update).
	 *
	 * @param lstAllMobileApplication	List<AllMobileApplication__c>
	 */
	public static void syncUpdateAllMobileApplicationToExternalObject(List<AllMobileApplication__c> lstAllMobileApplication) {
		Set<Id> setIdAllMobileApplication = new Set<Id>();
		if(!lstAllMobileApplication.isEmpty()) {
			setIdAllMobileApplication = getSetIdAllMobileApplicationSFFromListAllMobileApplicationSF(lstAllMobileApplication);
			AllMobileApplicationOperationClass.updateExternalAllMobileApplicationWS(setIdAllMobileApplication);
		}
	}


	/**
	 * This method prepares a List of Application objects in Salesforce to communicate asynchronously and update into Heroku application table.
	 *
	 * @param setIdAllMobileApplication	Set<Id>
	 */
	@future(callout = true)
	public static void updateExternalAllMobileApplicationWS(Set<Id> setIdAllMobileApplication) {
		AllMobileApplicationOperationClass.updateExternalAllMobileApplication(setIdAllMobileApplication);
	}

	/**
	 * This method sync a List of Application objects from  Salesforce into a Heroku application table using PUT method (update)
	 *
	 * @param setIdAllMobileApplication Set<Id>
	 */
	public static Boolean updateExternalAllMobileApplication(Set<Id> setIdAllMobileApplication) {

		//Variables.
		Integer intContainedApplicationSFInApplicationHeroku = 0;
		List<AllMobileApplication__c> lstAllMobileApplicationToUpdateInHeroku = new List<AllMobileApplication__c>();
		AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass();

		//Get List of Application in SF.
		List<AllMobileApplication__c> lstAllMobileApplicationSF = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileShipType__c, AllMobileLevelValidation__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c WHERE Id =: setIdAllMobileApplication];

		//Get List of All Application in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileApplicationHerokuObjectClass = getAllMobileAllApplicationHeroku();

		//Compare both lists to avoid update Application SF that do not exist in HK. Generates new List to be updated in Heroku.
		for(AllMobileApplication__c objAllMobileApplicationSF : lstAllMobileApplicationSF) {
			intContainedApplicationSFInApplicationHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClassIterate : lstAllMobileApplicationHerokuObjectClass) {
				if(objAllMobileApplicationSF.AllMobileApplicationId__c == String.valueOf(objAllMobileApplicationHerokuObjectClassIterate.application_id)) {
					intContainedApplicationSFInApplicationHeroku++;
				}
			}
			if(intContainedApplicationSFInApplicationHeroku > 0) {
				lstAllMobileApplicationToUpdateInHeroku.add(objAllMobileApplicationSF);
			}
		}

		//Update the Final List of Application (SF) into Heroku.
		if(!lstAllMobileApplicationToUpdateInHeroku.isEmpty()) {
			for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplicationToUpdateInHeroku) {
				objAllMobileApplicationHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass();
				objAllMobileApplicationHerokuObjectClass.application_id = Integer.valueOf(objAllMobileApplication.AllMobileApplicationId__c);
				objAllMobileApplicationHerokuObjectClass.name = objAllMobileApplication.Name;
				objAllMobileApplicationHerokuObjectClass.ship_type = objAllMobileApplication.AllMobileShipType__c;
				objAllMobileApplicationHerokuObjectClass.levelvalidation = Integer.valueOf(objAllMobileApplication.AllMobileLevelValidation__c);
				objAllMobileApplicationHerokuObjectClass.dtlastmodifieddate = objAllMobileApplication.LastModifiedDate;
				objAllMobileApplicationHerokuObjectClass.softdeleteflag = objAllMobileApplication.AllMobileSoftDeleteFlag__c;

				//Rest callout to Update Application into Heroku.
				String strStatusCode = AllMobileSyncObjectsClass.calloutUpdateApplicationIntoHeroku(objAllMobileApplicationHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method delete a List of Application objects from  Salesforce according to a Heroku application table using DELETE method.
	 *
	 * @param lstAllMobileApplicationInSF List<AllMobileApplication__c>
	 */
	public static void syncDeleteAllMobileApplicationInSF(List<AllMobileApplication__c> lstAllMobileApplicationInSF) {

		//Inner variables.
		List<Integer> lstApplicationIdFromHeroku = new List<Integer>();
		List<AllMobileApplication__c> lstAllMobileApplicationToBeDeletedInSF = new List<AllMobileApplication__c>();

		//Get All Application From Heroku.
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileAllApplicationHerokuObjectClass = getAllMobileAllApplicationHeroku();

		//Get List of Integer Application Heroku to compare.
		for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass : lstAllMobileAllApplicationHerokuObjectClass) {
			lstApplicationIdFromHeroku.add(objAllMobileApplicationHerokuObjectClass.application_id);
		}

		//Identify Applications SF to be Deleted.
		for(AllMobileApplication__c objAllMobileApplicationSF : lstAllMobileApplicationInSF) {
			if(!lstApplicationIdFromHeroku.contains(Integer.valueOf(objAllMobileApplicationSF.AllMobileApplicationId__c))) {
				lstAllMobileApplicationToBeDeletedInSF.add(objAllMobileApplicationSF);
			}
		}
		if(!lstAllMobileApplicationToBeDeletedInSF.isEmpty() && lstAllMobileApplicationToBeDeletedInSF != NULL) {
			delete lstAllMobileApplicationToBeDeletedInSF;
		}
	}

	/**
	 * This method insert and update a List of Application objects from  Salesforce according to a Heroku application table using POST and PUT methods.
	 *
	 * @param lstAllMobileApplicationInSF	List<AllMobileApplication__c>
	 */
	public static void syncInsertAndUpdateAllMobileApplicationInSFFromApplicationHeroku(List<AllMobileApplication__c> lstAllMobileApplicationInSF) {

		//Inner variables.
		List<AllMobileApplication__c> lstAllMobileApplicationToBeInsertedInSF = new List<AllMobileApplication__c>();
		List<Integer> lstApplicationIdFromAllMobileApplicationSF = new List<Integer>();
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileApplicationHerokuObjectClassToBeInsertedInSF = new List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass>();
		AllMobileApplication__c objAllMobileApplicationToBeInsertedInSF = new AllMobileApplication__c();

		//Get All Application From Heroku.
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileAllApplicationHerokuObjectClass = getAllMobileAllApplicationHeroku();

		//Get List of Application Id from AllMobileAplication SF.
		for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplicationInSF) {
			lstApplicationIdFromAllMobileApplicationSF.add(Integer.valueOf(objAllMobileApplication.AllMobileApplicationId__c));
		}

		//Identify Applications From Heroku to be Inserted in SF.
		for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass : lstAllMobileAllApplicationHerokuObjectClass) {
			if(!lstApplicationIdFromAllMobileApplicationSF.contains(objAllMobileApplicationHerokuObjectClass.application_id)) {
				lstAllMobileApplicationHerokuObjectClassToBeInsertedInSF.add(objAllMobileApplicationHerokuObjectClass);
			}
		}

		//Convert Applications From Heroku to AllMobileApplication in SF.
		for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass : lstAllMobileApplicationHerokuObjectClassToBeInsertedInSF) {
			objAllMobileApplicationToBeInsertedInSF = new AllMobileApplication__c();
			objAllMobileApplicationToBeInsertedInSF.Name = objAllMobileApplicationHerokuObjectClass.name;
			objAllMobileApplicationToBeInsertedInSF.AllMobileApplicationId__c = String.valueOf(objAllMobileApplicationHerokuObjectClass.application_id);
			objAllMobileApplicationToBeInsertedInSF.AllMobileLevelValidation__c = objAllMobileApplicationHerokuObjectClass.levelvalidation;
			objAllMobileApplicationToBeInsertedInSF.AllMobileShipType__c = objAllMobileApplicationHerokuObjectClass.ship_type;
			objAllMobileApplicationToBeInsertedInSF.AllMobileSoftDeleteFlag__c = objAllMobileApplicationHerokuObjectClass.softdeleteflag;
			lstAllMobileApplicationToBeInsertedInSF.add(objAllMobileApplicationToBeInsertedInSF);
		}
		if(!lstAllMobileApplicationToBeInsertedInSF.isEmpty() && lstAllMobileApplicationToBeInsertedInSF != NULL) {
			insert lstAllMobileApplicationToBeInsertedInSF;
		}

		//UPDATING
		List<AllmobileApplication__c> lstAllMobileApplicationToBeUpdatedInSF = new List<AllmobileApplication__c>();
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstAllMobileApplicationHerokuObjectClassExistingInSF = new List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass>();


		//Identify Applications From Heroku to be Updated in SF.
		for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass : lstAllMobileAllApplicationHerokuObjectClass) {
			if(lstApplicationIdFromAllMobileApplicationSF.contains(objAllMobileApplicationHerokuObjectClass.application_id)) {
				lstAllMobileApplicationHerokuObjectClassExistingInSF.add(objAllMobileApplicationHerokuObjectClass);
			}
		}

		//Generate Application Heroku object.
		for(AllMobileApplication__c objAllMobileApplicationToCompareToUpdate : lstAllMobileApplicationInSF) {
			for(AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate : lstAllMobileApplicationHerokuObjectClassExistingInSF) {
				if(objAllMobileApplicationToCompareToUpdate.AllMobileApplicationId__c == String.valueOf(objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.application_id)) {
					if((objAllMobileApplicationToCompareToUpdate.Name != objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.name) || (objAllMobileApplicationToCompareToUpdate.AllMobileShipType__c != objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.ship_type) || (objAllMobileApplicationToCompareToUpdate.AllMobileLevelValidation__c != objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.levelvalidation) || (objAllMobileApplicationToCompareToUpdate.AllMobileSoftDeleteFlag__c != objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.softdeleteflag)) {
						objAllMobileApplicationToCompareToUpdate.Name = objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.name;
						objAllMobileApplicationToCompareToUpdate.AllMobileShipType__c = objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.ship_type;
						objAllMobileApplicationToCompareToUpdate.AllMobileLevelValidation__c = objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.levelvalidation;
						objAllMobileApplicationToCompareToUpdate.AllMobileSoftDeleteFlag__c = objAllMobileApplicationHerokuObjectClassToCompareToUpdateIterate.softdeleteflag;
						lstAllMobileApplicationToBeUpdatedInSF.add(objAllMobileApplicationToCompareToUpdate);
					}
				}
			}
		}
		if(!lstAllMobileApplicationToBeUpdatedInSF.isEmpty() && lstAllMobileApplicationToBeUpdatedInSF != NULL) {
			update lstAllMobileApplicationToBeUpdatedInSF;
		}
	}

	/**
	 * This method initiate a remote action to insert and update a List of Application objects of Salesforce according to a Heroku application table using POST AND PUT methods.
	 */
	@RemoteAction
	webservice static void syncInsertUpdateApplicationFromHeroku() {
		List<AllMobileApplication__c> lstAllMobileApplicationInSF = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileShipType__c, AllMobileLevelValidation__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c];
		syncInsertAndUpdateAllMobileApplicationInSFFromApplicationHeroku(lstAllMobileApplicationInSF);
	}

	/**
	 * This method get a List of all Application records of Heroku.
	 *
	 * @return List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass>
	 */
	public static List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> getAllMobileAllApplicationHeroku() {
		String strJsonSerializeAllApplicationHeroku = AllMobileSyncObjectsClass.calloutReadAllApplicationFromHeroku();
		List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass> lstJsonDeserializeAllMobileAllApplicationHerokuObjectClass = (List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass>)JSON.deserialize(strJsonSerializeAllApplicationHeroku, List<AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass>.class);
		return lstJsonDeserializeAllMobileAllApplicationHerokuObjectClass;
	}

	/**
	 * This method get and set a List of Application Ids of Salesforce.
	 *
	 * @param lstAllMobileApplication	List<AllMobileApplication__c>
	 * @return setIdAllMobileApplication	Set<Id>
	 */
	public static Set<Id> getSetIdAllMobileApplicationSFFromListAllMobileApplicationSF(List<AllMobileApplication__c> lstAllMobileApplication) {
		Set<Id> setIdAllMobileApplication = new Set<Id>();
		if(!lstAllMobileApplication.isEmpty()) {
			for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplication) {
				setIdAllMobileApplication.add(objAllMobileApplication.Id);
			}
		}
		return setIdAllMobileApplication;
	}

	/**
	 * This method execute a queueable trigger which insert and update a List of Application Ids of Salesforce.
	 *
	 * @param objQueueableContext	QueueableContext
	 */
	public void execute(QueueableContext objQueueableContext) {
		if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_APPLICATION) {
			syncInsertAllMobileApplicationToExternalObject(lstAllMobileApplication);
		} else if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_APPLICATION) {
			syncUpdateAllMobileApplicationToExternalObject(lstAllMobileApplication);
		}
	}
}