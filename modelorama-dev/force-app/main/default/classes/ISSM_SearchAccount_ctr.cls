/****************************************************************************************************
    General Information
    -------------------
    Authores:   Luis Licona
    email:      llicona@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the PopUp for search Account

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                  Description          
    ------    -----------------     ---------------------   ----------------------------------------
    1.0       13-Noviembre-2017     Luis Licona             Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_SearchAccount_ctr {

    public String query {get; set;}
    public Account[] LstAcc {get; set;}
    public String[] LstErr{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL {get;set;}
    public ONTAP__Route__c[] LstRoutes{get;set;}
    public Set<String> SetRoute{get;set;}
    public Set<Id> NationalAccsRoutesId_set{get;set;}
    public Id IdRec;

    public ISSM_SearchAccount_ctr(){
        LstErr    = new List<String>();
        LstRoutes = new List<ONTAP__Route__c>();
        CTRSOQL   = new ISSM_OnCallQueries_cls();        
        IdRec     = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        Id IdRecR = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
        LstRoutes = CTRSOQL.getRoutesUser(IdRecR);
        SetRoute  = new Set<String>();
        NationalAccsRoutesId_set = new Set<Id>();
        
        for(ONTAP__Route__c ObjRoute : LstRoutes){
            if(ObjRoute.ISSM_SMSubgroup__c != Label.ISSM_NationalAccounts){ SetRoute.add(ObjRoute.ONTAP__SalesOfficeId__c);}
            else{NationalAccsRoutesId_set.add(ObjRoute.Id);}            
        }

    	LstAcc = !NationalAccsRoutesId_set.isEmpty() ? CTRSOQL.getAccsByRoutes(IdRec,NationalAccsRoutesId_set) : CTRSOQL.getAccForRoute(IdRec,SetRoute);
    }

	//Ejecuta consulta para obtener las cuentas de acuerdo a la(s) oficina(s) que este asignado si es TV normal,
    // si es un Cuenta Nacional retorna las cuentas de Account By Route de sus rutas.
    public PageReference runQuery(){
        
        LstAcc = new List<Account>();
        
        if(String.isNotBlank(query)){
            query = '%'+query+'%';
            //LstAcc = CTRSOQL.getAccForSearch(query,SetRoute);
            LstAcc = !NationalAccsRoutesId_set.isEmpty() ? CTRSOQL.getNationalAccForSearch(IdRec,query,NationalAccsRoutesId_set) : CTRSOQL.getAccForSearch(query,SetRoute);
            if(LstAcc.isEmpty())
                LstErr.add(Label.ISSM_Mnsg7+' '+ query);

        }else{
            LstErr.add(Label.ISSM_Mnsg1);
        }        

        return null;
    }
}