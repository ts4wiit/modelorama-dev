global class WrpOptions{
	Public String StrLabel{get;Set;}
	Public String StrValue{get;Set;}
	public WrpOptions()
	{
		StrLabel = '';
		StrValue = '';
	}

	Public WrpOptions(String StrPLabel, String StrPValue)
	{
		StrLabel = StrPLabel;
		StrValue = StrPValue;
	}

}