global class ISSM_CAM_UpsertInventory_bch implements Database.Batchable<sObject> {
	String query;
	
	
	global ISSM_CAM_UpsertInventory_bch(String qParam) {
		query = qParam;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug(loggingLevel.Error, '*** query: ' + query);
		System.debug(loggingLevel.Error, '*** Database.getQueryLocator(query): ' + Database.getQueryLocator(query));
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		ISSM_CAM_UpsertInventory_cls.upsertInventory(scope);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}