/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_AdminCuentasATM_tst
Versión : 1.0
Fecha de Creación : 16 Abril 2018
Funcionalidad : Clase de prueba del batch ISSM_AdminCuentasATM_bch
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        16 - Abril - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_AdminCuentasATM_tst {
    static testmethod void staticResourceLoad() {
        //Load CSV file saved in static resource  
        // List<SObject> SalesOffice_lst = Test.loadData(Account.sObjectType,'ISSM_SalesOfficeLoad_Test');
        // List<SObject> Account_lst = Test.loadData(Account.sObjectType,'ISSM_AccountLoad_Test');
        // List<SObject> ATM_Queue_lst = Test.loadData(ISSM_ATM_Queue__c.sObjectType,'ISSM_ATM_QueueLoad_Test');
        List<ISSM_ATM_Queue__c> ATM_Queue_lst = new List<ISSM_ATM_Queue__c>();

        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
        insert user;

        Id RT_SalesOffice_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account salesOffice1 = new Account();
        salesOffice1.Name = 'Sales Office 1';
        salesOffice1.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice1;

        Account salesOffice2 = new Account();
        salesOffice2.Name = 'Sales Office 2';
        salesOffice2.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice2;

        Account salesOffice3 = new Account();
        salesOffice3.Name = 'Sales Office 3';
        salesOffice3.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice3;

        Account salesOffice4 = new Account();
        salesOffice4.Name = 'Sales Office 4';
        salesOffice4.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice4;

        Account salesOffice5 = new Account();
        salesOffice5.Name = 'Sales Office 5';
        salesOffice5.RecordTypeId = RT_SalesOffice_id;
        insert salesOffice5;

        Id RT_Account_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Account 1';
        account1.ISSM_SalesOffice__c = salesOffice1.Id;
        account1.RecordTypeId = RT_Account_id;
        insert account1;

        Account account2 = new Account();
        account2.Name = 'Account 2';
        account2.ISSM_SalesOffice__c = salesOffice2.Id;
        account2.RecordTypeId = RT_Account_id;
        insert account2;

        Account account3 = new Account();
        account3.Name = 'Account 3';
        account3.ISSM_SalesOffice__c = salesOffice3.Id;
        account3.RecordTypeId = RT_Account_id;
        insert account3;

        Account account4 = new Account();
        account4.Name = 'Account 4';
        account4.ISSM_SalesOffice__c = salesOffice4.Id;
        account4.RecordTypeId = RT_Account_id;
        insert account4;

        Account account5 = new Account();
        account5.Name = 'Account 5';
        account5.ISSM_SalesOffice__c = salesOffice5.Id;
        account5.RecordTypeId = RT_Account_id;
        insert account5;

        ISSM_ATM_Queue__c queue1 = new ISSM_ATM_Queue__c();
        queue1.Oficina_de_Ventas__c = salesOffice1.Id;
        queue1.Usuario_relacionado__c = user.Id;
        queue1.Rol__c = 'Supervisor';
        queue1.Accion__c = 'Alta';
        queue1.Listo_para_Procesar__c = true;
        insert queue1;

        ISSM_ATM_Queue__c queue2 = new ISSM_ATM_Queue__c();
        queue2.Oficina_de_Ventas__c = salesOffice2.Id;
        queue2.Usuario_relacionado__c = user.Id;
        queue2.Rol__c = 'Supervisor';
        queue2.Accion__c = 'Baja';
        queue2.Listo_para_Procesar__c = true;
        insert queue2;

        ISSM_ATM_Queue__c queue3 = new ISSM_ATM_Queue__c();
        queue3.Oficina_de_Ventas__c = salesOffice3.Id;
        queue3.Usuario_relacionado__c = user.Id;
        queue3.Rol__c = 'Supervisor';
        queue3.Accion__c = 'Cambio';
        queue3.Listo_para_Procesar__c = true;
        insert queue3;

        ISSM_ATM_Queue__c queue4 = new ISSM_ATM_Queue__c();
        queue4.Oficina_de_Ventas__c = salesOffice4.Id;
        queue4.Usuario_relacionado__c = user.Id;
        queue4.Rol__c = 'Supervisor';
        queue4.Accion__c = 'Alta';
        queue4.Listo_para_Procesar__c = true;
        insert queue4;

        ISSM_ATM_Queue__c queue5 = new ISSM_ATM_Queue__c();
        queue5.Oficina_de_Ventas__c = salesOffice5.Id;
        queue5.Usuario_relacionado__c = user.Id;
        queue5.Rol__c = 'Supervisor';
        queue5.Accion__c = 'Baja';
        queue5.Listo_para_Procesar__c = true;
        insert queue5;

        // System.assertEquals(SalesOffice_lst.size(), 5);
        // System.assertEquals(Account_lst.size(), 5);
        // System.assertEquals(ATM_Queue_lst.size(), 5);

        Test.startTest();

        ISSM_AdminCuentasATM_bch obj = new ISSM_AdminCuentasATM_bch();
        DataBase.executeBatch(obj);

        Test.stopTest();
    }
}