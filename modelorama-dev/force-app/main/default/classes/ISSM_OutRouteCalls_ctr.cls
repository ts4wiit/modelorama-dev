/****************************************************************************************************
    General Information
    -------------------
    Authores:   Luis Licona
    email:      llicona@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the Out Route Calls

    Information about changes (versions)
    ================================================================================================
    Number    Dates           		Author                  Description          
    ------    -----------------   	---------------------   ----------------------------------------
    1.0       November 13, 2017   	Luis Licona             Class Creation
    1.1       May 10, 2018          Marco Zúñiga	 	    Added functionality to asign a Call List 
    														to the Call Out of Route created and
    														a validation for the Inbound Telesales.
    														Even if the customer isn't in DSD Model, 
    														the Call can be created taking the route 
    														of the Account record of the Customer.
   	1.2       November 27, 2017     Marco Zúñiga            Add the functionality to create Calls Out
   															of Route for National Accounts from DSD 
   															model.
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_OutRouteCalls_ctr {
	
	public WrapperpCallList[] waCallList {get;set;}
	public Integer rowToRemove {get;set;}
	public String[] LstScc {get;set;}
	public String[] LstErr {get;set;}
	public Id IdRecType{get;set;}
	public ISSM_OnCallQueries_cls CTRSOQL {get;set;}
	public ONCALL__Call__c[] callRecordsToBeInserted {get;set;}
	public set<String> SetIdAcc{get;set;}
	public set<String> SetRoute{get;set;}
	public ONTAP__Route__c[] LstRoutes {get;set;}
	public Id RecType{get;set;}
	public Id AccRecType_Id{get;set;}

	//Metodo constructor	
	public ISSM_OutRouteCalls_ctr() {
		restartList();
		waCallList    = new List<WrapperpCallList>();
		CTRSOQL 	  = new ISSM_OnCallQueries_cls();
		SetIdAcc 	  = new set<String>();
		SetRoute 	  = new set<String>();
		LstRoutes 	  = new List<ONTAP__Route__c>();
		RecType       = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();
		AccRecType_Id = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
		LstRoutes     = CTRSOQL.getRoutesUser(RecType); 

		if(LstRoutes.isEmpty()){ LstErr.add(Label.ISSM_Errormnsg6);}
		else{ addNewRowToCallList(); }
	}

 	//Hace el insert de las llamadas que se seleccionaron
	public PageReference SaveMultipleCalls() {
		restartList();
		IdRecType = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId();
		callRecordsToBeInserted = new List<ONCALL__Call__c>();
		if(!waCallList.isEmpty()){
			
			for(ISSM_OutRouteCalls_ctr.WrapperpCallList eachRecord : waCallList ){
				ONCALL__Call__c calTemp 		= eachRecord.record;
				calTemp.ONCALL__POC__c 			= eachRecord.record.ONCALL__POC__c;
				if(String.isBlank(eachRecord.record.ONCALL__POC__c)){
					LstErr.add(Label.ISSM_Mnsg2);
					return null;
				}
				calTemp.ISSM_CallResult__c		= Label.ISSM_PendingCall;
				calTemp.Name 					= Label.ISSM_COR;
				calTemp.ISSM_TelesalesAgent__c 	= UserInfo.getUserId();
				calTemp.ONCALL__Date__c 		= Datetime.parse(System.now().format());
				calTemp.RecordTypeId 			= IdRecType;
				calTemp.ISSM_Call_ORC__c		= true;
				callRecordsToBeInserted.add(calTemp);
				SetIdAcc.add(eachRecord.record.ONCALL__POC__c);
			}
			Boolean insertedFlag_bln = false;
			try{
				ONCALL__Call__c[] acl_Lst = AssignCalllist(callRecordsToBeInserted,SetIdAcc);
				insert acl_Lst;
				insertedFlag_bln = acl_Lst.size() > 0 ? true : false;
				
            }catch(Exception exc){
            	if(String.valueOf(exc.getMessage()).contains(Label.ISSM_DUPLICATE_VALUE)){ LstErr.add(Label.ISSM_Mnsg3); }
            	else{ LstErr.add(String.valueOf(exc.getMessage()));}
            	insertedFlag_bln = false;
            	return null;
            }
            waCallList.clear(); //Limpia la lista despues de haber insertado para que no queden los valores visibles
        	if(LstScc.isEmpty() && insertedFlag_bln){ LstScc.add(Label.ISSM_Mnsg4); }
        }
		return null;
	}

	public ONCALL__Call__c[] AssignCalllist(ONCALL__Call__c[] LstCall, Set<String> SetAccIds) {
		Map<String,String> MapTour          = new Map<String,String>();
		Map<String,String> MapRoute         = new Map<String,String>();
		ONCALL__Call__c[] Calls_lst         = new List<ONCALL__Call__c>();
		Map<String,String> AccsNotFound_map = new Map<String,String>();
		Set<String> AccsNotFound_set        = new Set<String>();


		for(Account objAcc : CTRSOQL.GetAccbySet(SetAccIds)){ //Without DSD
			System.debug('Route from Account record = ' + objAcc);
			MapRoute.put(objAcc.Id, objAcc.ONCALL__OnCall_Route_Code__c);
			SetRoute.add(objAcc.ONCALL__OnCall_Route_Code__c);
		}

		for(AccountByRoute__c ObjAccByRoute : CTRSOQL.getSetAccByRoute(SetAccIds, Label.ISSM_OriginCall)){//With DSD
			System.debug('Route in Account By Route = ' + ObjAccByRoute);
			MapRoute.put(ObjAccByRoute.Account__c, ObjAccByRoute.Route__r.ONTAP__RouteId__c);
			SetRoute.add(ObjAccByRoute.Route__r.ONTAP__RouteId__c);
		}

		for(ONTAP__Tour__c ObjTour : CTRSOQL.GetTourBySet(SetRoute)){ MapTour.put(ObjTour.Route__r.ONTAP__RouteId__c, ObjTour.Id); }

		for(ONCALL__Call__c objCall : LstCall){
			objCall.CallList__c = MapTour.get(MapRoute.get(objCall.ONCALL__POC__c));
			if(MapTour.get(MapRoute.get(objCall.ONCALL__POC__c)) == null)
				AccsNotFound_set.add(objCall.ONCALL__POC__c);	
		}		

		if(AccsNotFound_set.size() > 0){
			Account[] Accs_lst = CTRSOQL.getAccountsBySet(AccsNotFound_set, AccRecType_Id);				
			for(Account Acc_obj : Accs_lst)
				AccsNotFound_map.put(Acc_obj.Id,Acc_obj.Name + '; ');
		}
		
		for(Integer i = 0; i < LstCall.size(); i++){
			if(!AccsNotFound_map.containsKey(LstCall.get(i).ONCALL__POC__c)){ Calls_lst.add(LstCall.get(i)); }
		}

		if(AccsNotFound_map.size() > 0){
			String NotCrtdCalls_str = LstScc.size() > 0 ? '; ' + Label.ISSM_CallsNotCreated + ' ' :Label.ISSM_CallsNotCreated + ' ';
			LstErr.add(NotCrtdCalls_str);
			LstErr.addAll(AccsNotFound_map.values());
		}
		return Calls_lst;
	}
	
	//quita del wraper las llamadas que se remuevan de la visual
	public void removeRowFromCallList() {
		waCallList.remove(rowToRemove);
	}

	//Agrega al wraper las llamadas que se den de alta en la visual
	public void addNewRowToCallList() {
		restartList();
		ONCALL__Call__c[] LstCall = CTRSOQL.GetCallByOwner(); //get the out of route call list for today

		Map<String, ISSM_Parameters__c> MapParameters = ISSM_Parameters__c.getAll();
		Integer IntMaxCalls = Integer.valueOf(MapParameters.get(Label.ISSM_OutRouteCalls).Value__c);

		if( (LstCall.size() + waCallList.size() ) < IntMaxCalls){
			ISSM_OutRouteCalls_ctr.WrapperpCallList newRecord = new ISSM_OutRouteCalls_ctr.WrapperpCallList();
			ONCALL__Call__c newCallRecord = new ONCALL__Call__c();        
			newRecord.record = newCallRecord;
			waCallList.add(newRecord);
		}else{ LstErr.add(Label.ISSM_Mnsg6); }
	}

	//Inicializa las listas de mensajes
	public void restartList() {
		LstScc = new List<String>();
		LstErr = new List<String>();
	}

	//Wrapper para gestionar las llamadas a crear
	public class WrapperpCallList {
		public String StrName{get;set;}
		public ONCALL__Call__c record {get;set;}
	}
}