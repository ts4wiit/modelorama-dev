/**
 * Created by:			Avanxo México
 * Author:				Carlos Pintor / cpintor@avanxo.com
 * Project:				AbInbev - Trade Revenue Management
 * Description:			Apex class to test the methods of the Apex controller 'ISSM_SearchWithResultsTable_ctr'
 * 						of the Lightning Component 'ISSM_SearchWithResultsTable_lcp'.
 *
 * N.     Date             Author                     Description
 * 1.0    25-05-2018       Carlos Pintor              Created
 *
 */
@isTest
public class ISSM_SearchWithResultsTable_tst {
    
    @testSetup 
    static void setupData() {
        //Create MDM_Parameter__c records (Segment)
        List<MDM_Parameter__c> parameters = new List<MDM_Parameter__c>();
        MDM_Parameter__c seg42 = new MDM_Parameter__c();
        seg42.Name = 'Foco (Tradicional)';
        seg42.Code__c = '42';
        seg42.Description__c = 'Foco (Tradicional)';
        seg42.Catalog__c = 'Segmento';
        seg42.Active__c = true;
        seg42.Active_Revenue__c = true;
        parameters.add(seg42);
        
        MDM_Parameter__c seg40 = new MDM_Parameter__c();
        seg40.Name = 'Botella Abierta Imag';
        seg40.Code__c = '40';
        seg40.Description__c = 'Botella Abierta Imag';
        seg40.Catalog__c = 'Segmento';
        seg40.Active__c = true;
        seg40.Active_Revenue__c = true;
        parameters.add(seg40);
        
        Database.insert(parameters);
        
        MDM_Parameter__c segFocoTrad = [SELECT Id FROM MDM_Parameter__c WHERE Code__c = '42' LIMIT 1];
        
        String RecordTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecordTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
            
        //Creamos Cuenta con SalesOrg
        Account  objAccountSalesOrg = new Account();
            objAccountSalesOrg.Name = 'Name Sales Org';
            objAccountSalesOrg.RecordTypeId = RecordTypeAccountSalesOrg;
            objAccountSalesOrg.ONTAP__SalesOgId__c = 'OR01';
            insert objAccountSalesOrg;
        
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
        	objAccount.ONTAP__SAP_Number__c = '0123456789';
        	objAccount.ISSM_SegmentCode__c = segFocoTrad.Id;
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ONTAP__SalesOffId__c = 'SO01';
            objAccount.ONTAP__SalesOgId__c ='OR01';
            insert objAccount;
        
    }
    
    static testMethod void testGetRecordWrapperList(){
        Account off01 = [SELECT Id FROM Account WHERE Name = 'Name SalesOffice' LIMIT 1];
        Account acc01 = [SELECT Id FROM Account WHERE Name = 'Name Account' LIMIT 1];
        
        MDM_Parameter__c segFocoTrad = [SELECT Id FROM MDM_Parameter__c WHERE Code__c = '42' LIMIT 1];
       
        List<String> deselectedRecordIds = new List<String>();
        deselectedRecordIds.add(acc01.Id);
        String fieldList = 'Name';
        String objectType = 'Account';
        String fieldOrderByList = 'Name';
        String numberOfRowsToReturn = '10';
        String recordType = 'Account';
        String parentRecordFieldName = 'ISSM_SalesOffice__c';
        String[] parentRecordIdList = new String[]{};
        String off01Id = '\'' + off01.Id + '\'';
        parentRecordIdList.add(off01Id);
        MDM_Parameter__c[] priceGroupList = new MDM_Parameter__c[]{};
        priceGroupList.add(segFocoTrad);
        
        Test.startTest();
        List<ISSM_SearchWithResultsTable_ctr.RecordWrapper> recordWprList = 
            ISSM_SearchWithResultsTable_ctr.getRecordWrapperList(deselectedRecordIds, fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, recordType, parentRecordFieldName, parentRecordIdList, priceGroupList);
        System.assertEquals(1, recordWprList.size());
        Test.stopTest();
    }
    
    static testMethod void testSearchByKeyWord(){
        Account off01 = [SELECT Id FROM Account WHERE Name = 'Name SalesOffice' LIMIT 1];
        Account acc01 = [SELECT Id FROM Account WHERE Name = 'Name Account' LIMIT 1];
       
        List<String> deselectedRecordIds = new List<String>();
        deselectedRecordIds.add(acc01.Id);
        String fieldList = 'Name';
        String objectType = 'Account';
        String fieldOrderByList = 'Name';
        String numberOfRowsToReturn = '10';
        String keyWord = 'Name Account';
        Boolean searchByList = false;
        String recordType = 'Account';
        String parentRecordFieldName = 'ISSM_SalesOffice__c';
        String[] parentRecordIdList = new String[]{};
        String off01Id = '\'' + off01.Id + '\'';
        parentRecordIdList.add(off01Id);
        MDM_Parameter__c[] priceGroupList = new MDM_Parameter__c[]{}; 
        
        Test.startTest();
        List<ISSM_SearchWithResultsTable_ctr.RecordWrapper> recordWprList = 
            ISSM_SearchWithResultsTable_ctr.searchByKeyWord(deselectedRecordIds, fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, keyWord, searchByList, recordType, parentRecordFieldName, parentRecordIdList, priceGroupList);
        System.assertEquals(1, recordWprList.size());
        
        //Test now when the search based on a list of SAP numbers
        searchByList = true;
        keyWord = '0123456789';
        List<ISSM_SearchWithResultsTable_ctr.RecordWrapper> recordWprList2 = 
            ISSM_SearchWithResultsTable_ctr.searchByKeyWord(deselectedRecordIds, fieldList, objectType, fieldOrderByList, numberOfRowsToReturn, keyWord, searchByList, recordType, parentRecordFieldName, parentRecordIdList, priceGroupList);
        System.assertEquals(1, recordWprList2.size());
        Test.stopTest();
    }
    
}