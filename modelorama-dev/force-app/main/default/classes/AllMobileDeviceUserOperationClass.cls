/**
 * This class contains the operations to synchronize the AllMobileDeviceUser records with Heroku.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileDeviceUserOperationClass implements Queueable {

    //Variables.
    String strEventTriggerFlag;
    List<AllMobileDeviceUser__c> lstAllMobileDeviceUser = new List<AllMobileDeviceUser__c>();

    //Constructor.
    public AllMobileDeviceUserOperationClass(List<AllMobileDeviceUser__c> lstAllMobileDeviceUserTrigger, String strEventTriggerFlagTrigger) {
        lstAllMobileDeviceUser = lstAllMobileDeviceUserTrigger;
        strEventTriggerFlag = strEventTriggerFlagTrigger;
    }

    /**
     *This method initiate a List of DeviceUser objects in Salesforce into Heroku device_user table using POST method (insert).
     *
     * @param lstAllMobileDeviceUser    List<AllMobileDeviceUser__c>
     */
    public static void syncInsertAllMobileDeviceUserToExternalObject(List<AllMobileDeviceUser__c> lstAllMobileDeviceUser) {
        Set<Id> setIdAllMobileDeviceUser = new Set<Id>();
        if(!lstAllMobileDeviceUser.isEmpty()) {
            setIdAllMobileDeviceUser = getSetIdAllMobileDeviceUserSFFromListAllMobileDeviceUserSF(lstAllMobileDeviceUser);
            AllMobileDeviceUserOperationClass.insertExternalAllMobileDeviceUserWS(setIdAllMobileDeviceUser);
        }
    }

    /**
     * This method prepares a List of DeviceUser objects in Salesforce to communicate asynchronously and insert into Heroku device_user table.
     *
     * @param setIdAllMobileDeviceUser  Set<Id>
     */
    @future(callout = true)
    public static void insertExternalAllMobileDeviceUserWS(Set<Id> setIdAllMobileDeviceUser) {
        AllMobileDeviceUserOperationClass.insertExternalAllMobileDeviceUser(setIdAllMobileDeviceUser);
    }

    /**
     * This method sync a List of DeviceUser objects from  Salesforce into a Heroku device_user table using POST method (insert).
     *
     * @param setIdAllMobileDeviceUser  Set<Id>
     */
    public static Boolean insertExternalAllMobileDeviceUser(Set<Id> setIdAllMobileDeviceUser) {

        //Variables.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserToInsertInHeroku = new List<AllMobileDeviceUser__c>();
        AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass();

        //Get List of DeviceUser in SF.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserSF = [SELECT Id, Name, AllMobileDeviceUserUserName__c, AllMobileDeviceUserId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserTypeLK__r.Name, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserIsActive__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserRouteLK__r.ONTAP__RouteId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileDeviceUser__c WHERE Id =: setIdAllMobileDeviceUser];

        //Get List of All DeviceUser in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileAllDeviceUserHerokuObjectClass = getAllMobileAllDeviceUserHeroku();
        System.debug('-> lstAllMobileAllDeviceUserHerokuObjectClass: ' + lstAllMobileAllDeviceUserHerokuObjectClass);

        //Compare both lists to avoid insert DeviceUser SF that already exist in HK. Generates new List to be inserted in Heroku.
        for(AllMobileDeviceUser__c objAllMobileDeviceUserSF: lstAllMobileDeviceUserSF) {
            Integer intContainedDeviceUserSFInDeviceUserHeroku = 0;
            for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClassIterate : lstAllMobileAllDeviceUserHerokuObjectClass) {
                if((objAllMobileDeviceUserSF.AllMobileDeviceUserUserName__c == (objAllMobileDeviceUserHerokuObjectClassIterate.user_name).trim()) || (objAllMobileDeviceUserSF.AllMobileDeviceUserId__c == (objAllMobileDeviceUserHerokuObjectClassIterate.id).trim())) {
                    intContainedDeviceUserSFInDeviceUserHeroku++;
                }
            }
            if(intContainedDeviceUserSFInDeviceUserHeroku == 0) {
                lstAllMobileDeviceUserToInsertInHeroku.add(objAllMobileDeviceUserSF);
            }
        }

        //Encrypt password.
        for(AllMobileDeviceUser__c objAllMobileDeviceUser: lstAllMobileDeviceUserToInsertInHeroku) {
            if(String.isNotBlank(objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c)) {
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = getEncryptedPassword(objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c);
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c);
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c);
            } else {
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c);
            }
        }

        //Insert the Final List of DeviceUser (SF) into Heroku.
        if(!lstAllMobileDeviceUserToInsertInHeroku.isEmpty()) {
            for(AllMobileDeviceUser__c objAllMobileDeviceUser: lstAllMobileDeviceUserToInsertInHeroku) {
                objAllMobileDeviceUserHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass();
                objAllMobileDeviceUserHerokuObjectClass.id = objAllMobileDeviceUser.AllMobileDeviceUserId__c;
                objAllMobileDeviceUserHerokuObjectClass.user_type = objAllMobileDeviceUser.AllMobileDeviceUserUserTypeLK__r.Name;
                objAllMobileDeviceUserHerokuObjectClass.vkbur = objAllMobileDeviceUser.AllMobileDeviceUserVkburId__c;
                objAllMobileDeviceUserHerokuObjectClass.user_name = objAllMobileDeviceUser.AllMobileDeviceUserUserName__c;
                objAllMobileDeviceUserHerokuObjectClass.isactive = objAllMobileDeviceUser.AllMobileDeviceUserIsActive__c;
                objAllMobileDeviceUserHerokuObjectClass.route = objAllMobileDeviceUser.AllMobileDeviceUserRouteId__c;
                objAllMobileDeviceUserHerokuObjectClass.email = objAllMobileDeviceUser.AllMobileDeviceUserEmail__c;
                objAllMobileDeviceUserHerokuObjectClass.ldap_id = objAllMobileDeviceUser.AllMobileDeviceUserLdapId__c;
                objAllMobileDeviceUserHerokuObjectClass.password = objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c;
                objAllMobileDeviceUserHerokuObjectClass.softdeleteflag = objAllMobileDeviceUser.AllMobileDeviceUserSoftDeleteFlag__c;
                objAllMobileDeviceUserHerokuObjectClass.dtlastmodifieddate = objAllMobileDeviceUser.LastModifiedDate;

                //Rest callout to insert DeviceUser in Heroku.
                String strStatusCodeInsert = AllMobileSyncObjectsClass.calloutInsertDeviceUserIntoHeroku(objAllMobileDeviceUserHerokuObjectClass);
            }
        }
        return false;
    }

    /**
     * This method initiate a List of DeviceUser objects in Salesforce into Heroku DeviceUser table using PUT method (update).
     *
     * @param lstAllMobileDeviceUser    List<AllMobileDeviceUser__c>
     */
    public static void syncUpdateAllMobileDeviceUserToExternalObject(List<AllMobileDeviceUser__c> lstAllMobileDeviceUser) {
        Set<Id> setIdAllMobileDeviceUser = new Set<Id>();
        if(!lstAllMobileDeviceUser.isEmpty()) {
            setIdAllMobileDeviceUser = getSetIdAllMobileDeviceUserSFFromListAllMobileDeviceUserSF(lstAllMobileDeviceUser);
            AllMobileDeviceUserOperationClass.updateExternalAllMobileDeviceUserWS(setIdAllMobileDeviceUser);
        }
    }

    /**
     * This method prepares a List of DeviceUser objects in Salesforce to communicate asynchronously and update into Heroku device_user table.
     *
     * @param setIdAllMobileDeviceUser Set<Id>
     */
    @future(callout = true)
    public static void updateExternalAllMobileDeviceUserWS(Set<Id> setIdAllMobileDeviceUser) {
        AllMobileDeviceUserOperationClass.updateExternalAllMobileDeviceUser(setIdAllMobileDeviceUser);
    }

    /**
     * This method sync a List of DeviceUser objects from  Salesforce into a Heroku device_user table using PUT method (update).
     *
     * @param setIdAllMobileDeviceUser  Set<Id>
     */
    public static Boolean updateExternalAllMobileDeviceUser(Set<Id> setIdAllMobileDeviceUser) {

        //Variables.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserToUpdateInHeroku = new List<AllMobileDeviceUser__c>();
        AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass();

        //Get List of DeviceUser in SF.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserSF = [SELECT Id, Name, AllMobileDeviceUserUserName__c, AllMobileDeviceUserId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserTypeLK__r.Name, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserIsActive__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserRouteLK__r.ONTAP__RouteId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileDeviceUser__c WHERE Id =: setIdAllMobileDeviceUser];

        //Get List of All DeviceUser in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileAllDeviceUserHerokuObjectClass = getAllMobileAllDeviceUserHeroku();

        //Compare both lists to avoid update DeviceUser SF that do not exist in HK. Generates new List to be updated in Heroku.
        for(AllMobileDeviceUser__c objAllMobileDeviceUserSF : lstAllMobileDeviceUserSF) {
            Integer intContainedDeviceUserSFInDeviceUserHeroku = 0;
            for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClassIterate : lstAllMobileAllDeviceUserHerokuObjectClass) {
                if((objAllMobileDeviceUserSF.AllMobileDeviceUserUserName__c == (objAllMobileDeviceUserHerokuObjectClassIterate.user_name).trim()) || (objAllMobileDeviceUserSF.AllMobileDeviceUserId__c == (objAllMobileDeviceUserHerokuObjectClassIterate.id).trim())) {
                    intContainedDeviceUserSFInDeviceUserHeroku++;
                }
            }
            if(intContainedDeviceUserSFInDeviceUserHeroku > 0) {
                lstAllMobileDeviceUserToUpdateInHeroku.add(objAllMobileDeviceUserSF);
            }
        }

        //Encrypt password.
        for(AllMobileDeviceUser__c objAllMobileDeviceUser: lstAllMobileDeviceUserToUpdateInHeroku) {
            if(String.isNotBlank(objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c)) {
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = getEncryptedPassword(objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c);
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c);
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c);
            } else {
                objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
                System.debug('-> objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c: ' + objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c);
            }
        }

        //Update the Final List of DeviceUser (SF) into Heroku.
        if(!lstAllMobileDeviceUserToUpdateInHeroku.isEmpty()) {
            for(AllMobileDeviceUser__c objAllMobileDeviceUser: lstAllMobileDeviceUserToUpdateInHeroku) {
                objAllMobileDeviceUserHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass();
                objAllMobileDeviceUserHerokuObjectClass.id = objAllMobileDeviceUser.AllMobileDeviceUserId__c;
                objAllMobileDeviceUserHerokuObjectClass.user_type = objAllMobileDeviceUser.AllMobileDeviceUserUserTypeLK__r.Name;
                objAllMobileDeviceUserHerokuObjectClass.vkbur = objAllMobileDeviceUser.AllMobileDeviceUserVkburId__c;
                objAllMobileDeviceUserHerokuObjectClass.user_name = objAllMobileDeviceUser.AllMobileDeviceUserUserName__c;
                objAllMobileDeviceUserHerokuObjectClass.isactive = objAllMobileDeviceUser.AllMobileDeviceUserIsActive__c;
                objAllMobileDeviceUserHerokuObjectClass.route = objAllMobileDeviceUser.AllMobileDeviceUserRouteId__c;
                objAllMobileDeviceUserHerokuObjectClass.email = objAllMobileDeviceUser.AllMobileDeviceUserEmail__c;
                objAllMobileDeviceUserHerokuObjectClass.ldap_id = objAllMobileDeviceUser.AllMobileDeviceUserLdapId__c;
                objAllMobileDeviceUserHerokuObjectClass.password = objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c;
                objAllMobileDeviceUserHerokuObjectClass.softdeleteflag = objAllMobileDeviceUser.AllMobileDeviceUserSoftDeleteFlag__c;
                objAllMobileDeviceUserHerokuObjectClass.dtlastmodifieddate = objAllMobileDeviceUser.LastModifiedDate;

                //Rest callout to update DeviceUser in Heroku.
                String strStatusCode = AllMobileSyncObjectsClass.calloutUpdateDeviceUserIntoHeroku(objAllMobileDeviceUserHerokuObjectClass);
            }
        }
        return false;
    }

    /**
     * This method delete a List of DeviceUser objects from  Salesforce according to a Heroku device_user table using DELETE method.
     *
     * @param lstAllMobileDeviceUserInSF    List<AllMobileDeviceUser__c>
     */
    public static void syncDeleteAllMobileDeviceUserInSF(List<AllMobileDeviceUser__c> lstAllMobileDeviceUserInSF) {

        //Inner variables.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserToBeDeletedInSF = new List<AllMobileDeviceUser__c>();
        List<String> lstIdAndUserNameFromDeviceUserHeroku = new List<String>();

        //Get List of All DeviceUser in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileAllDeviceUserHerokuObjectClass = getAllMobileAllDeviceUserHeroku();

        //Get List of Id & UserName of DeviceUser Heroku to compare.
        for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass : lstAllMobileAllDeviceUserHerokuObjectClass) {
            lstIdAndUserNameFromDeviceUserHeroku.add((objAllMobileDeviceUserHerokuObjectClass.id).trim() + (objAllMobileDeviceUserHerokuObjectClass.user_name).trim());
        }

        //Identify DeviceUser SF to be Deleted.
        for(AllMobileDeviceUser__c objAllMobileDeviceUserInSF : lstAllMobileDeviceUserInSF) {
            if(!lstIdAndUserNameFromDeviceUserHeroku.contains(objAllMobileDeviceUserInSF.AllMobileDeviceUserId__c + objAllMobileDeviceUserInSF.AllMobileDeviceUserUserName__c)) {
                lstAllMobileDeviceUserToBeDeletedInSF.add(objAllMobileDeviceUserInSF);
            }
        }
        if(!lstAllMobileDeviceUserToBeDeletedInSF.isEmpty() && lstAllMobileDeviceUserToBeDeletedInSF != NULL) {
            delete lstAllMobileDeviceUserToBeDeletedInSF;
        }
    }

    /**
     * This method insert and update a List of DeviceUser objects from Salesforce according to a Heroku device_user table using POST and PUT methods.
     *
     * @param lstAllMobileDeviceUserInSF    List<AllMobileDeviceUser__c>
     */

    public static void syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(List<AllMobileDeviceUser__c> lstAllMobileDeviceUserInSF) {

        //Inner variables.
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserToBeInsertedInSF = new List<AllMobileDeviceUser__c>();
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileDeviceUserHerokuObjectClassToBeInsertedInSF = new List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass>();
        AllMobileDeviceUser__c objAllMobileDeviceUserToBeInsertedInSF = new AllMobileDeviceUser__c();
        List<String> lstDeviceUserIdAndUserNameFromAllMobileDeviceUserSF = new List<String>();

        //Get List of All DeviceUser in Heroku.
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileAllDeviceUserHerokuObjectClass = getAllMobileAllDeviceUserHeroku();

        //Get List of all: RecordTypes: SalesOffice, SalesOffice, Routes, UserType.
        List<RecordType> lstAllRecordType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName =: AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_SALES_OFFICE LIMIT 1];
        List<Account> lstAllAccountSalesOffice = [SELECT Id, Name, ONTAP__SalesOffId__c FROM Account WHERE RecordTypeId =: lstAllRecordType[0].Id];
        List<ONTAP__Route__c> lstAllRoute = [SELECT Id, Name, ONTAP__RouteId__c FROM ONTAP__Route__c];
        List<AllMobilePermissionCategoryUserType__c> lstAllMobileAllPermissionCategoryUserType = [SELECT Id, Name, AllMobileDeviceUserPermissionName__c FROM AllMobilePermissionCategoryUserType__c];
        List<AllMobileCatUserType__c> lstAllMobileAllCatUserType = [SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserSoftDeleteFlag__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.Name FROM AllMobileCatUserType__c];

        //Get List of DeviceUser Id & UserName Id from AllMobileDeviceUser SF.
        for(AllMobileDeviceUser__c objAllMobileDeviceUserIterate : lstAllMobileDeviceUserInSF) {
            lstDeviceUserIdAndUserNameFromAllMobileDeviceUserSF.add(objAllMobileDeviceUserIterate.AllMobileDeviceUserId__c + objAllMobileDeviceUserIterate.AllMobileDeviceUserUserName__c);
        }

        //Identify DeviceUsers From Heroku to be Inserted in SF.
        for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass : lstAllMobileAllDeviceUserHerokuObjectClass) {
            if(!lstDeviceUserIdAndUserNameFromAllMobileDeviceUserSF.contains((objAllMobileDeviceUserHerokuObjectClass.id).trim() + (objAllMobileDeviceUserHerokuObjectClass.user_name).trim())) {
                lstAllMobileDeviceUserHerokuObjectClassToBeInsertedInSF.add(objAllMobileDeviceUserHerokuObjectClass);
            }
        }

        //Convert DeviceUser From Heroku to AllMobileDeviceUser in SF.
        for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass : lstAllMobileDeviceUserHerokuObjectClassToBeInsertedInSF) {
            objAllMobileDeviceUserToBeInsertedInSF = new AllMobileDeviceUser__c();
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserId__c = objAllMobileDeviceUserHerokuObjectClass.id;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserUserName__c = objAllMobileDeviceUserHerokuObjectClass.user_name;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserIsActive__c = objAllMobileDeviceUserHerokuObjectClass.isactive;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserEmail__c = objAllMobileDeviceUserHerokuObjectClass.email;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserLdapId__c = objAllMobileDeviceUserHerokuObjectClass.ldap_id;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserPasswordEncrypted__c = objAllMobileDeviceUserHerokuObjectClass.password;
            objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserSoftDeleteFlag__c = objAllMobileDeviceUserHerokuObjectClass.softdeleteflag;

            for(Account objAccountSalesOffice : lstAllAccountSalesOffice) {
                if((objAccountSalesOffice.ONTAP__SalesOffId__c == (objAllMobileDeviceUserHerokuObjectClass.vkbur).trim()) && (String.isNotBlank(objAccountSalesOffice.ONTAP__SalesOffId__c)) && (String.isNotBlank(objAllMobileDeviceUserHerokuObjectClass.vkbur))) {
                    objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserVkburLK__c = objAccountSalesOffice.Id;
                }
            }

            for(ONTAP__Route__c objONTAPRoute : lstAllRoute) {
                if(objAllMobileDeviceUserHerokuObjectClass.route != NULL) {
                    if(objONTAPRoute.ONTAP__RouteId__c == (objAllMobileDeviceUserHerokuObjectClass.route).trim()) {
                        objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserRouteLK__c = objONTAPRoute.Id;
                    }
                }
            }
            for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileAllCatUserType) {
                if(objAllMobileCatUserType.Name == (objAllMobileDeviceUserHerokuObjectClass.user_type).trim()) {
                    objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserUserTypeLK__c = objAllMobileCatUserType.Id;
                }
            }
            if((objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserVkburLK__c != NULL) && (objAllMobileDeviceUserToBeInsertedInSF.AllMobileDeviceUserUserTypeLK__c != NULL)) {
                lstAllMobileDeviceUserToBeInsertedInSF.add(objAllMobileDeviceUserToBeInsertedInSF);
            }
        }
        if(!lstAllMobileDeviceUserToBeInsertedInSF.isEmpty() && lstAllMobileDeviceUserToBeInsertedInSF != NULL) {
            insert lstAllMobileDeviceUserToBeInsertedInSF;
        }

        //UPDATING
        List<AllmobileDeviceUser__c> lstAllMobileDeviceUserToBeUpdatedInSF = new List<AllmobileDeviceUser__c>();
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstAllMobileDeviceUserHerokuObjectClassExistingInSF = new List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass>();

        //Identify DeviceUser From Heroku to be Updated in SF.
        for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass : lstAllMobileAllDeviceUserHerokuObjectClass) {
            if(lstDeviceUserIdAndUserNameFromAllMobileDeviceUserSF.contains((objAllMobileDeviceUserHerokuObjectClass.id).trim() + (objAllMobileDeviceUserHerokuObjectClass.user_name).trim())) {
                lstAllMobileDeviceUserHerokuObjectClassExistingInSF.add(objAllMobileDeviceUserHerokuObjectClass);
            }
        }

        //Generate List of DeviceUser to be updated.
        for(AllMobileDeviceUser__c objAllMobileDeviceUserToCompareToUpdate : lstAllMobileDeviceUserInSF) {
            for(AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate : lstAllMobileDeviceUserHerokuObjectClassExistingInSF) {
                if((objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserId__c == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.id).trim()) && (objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserUserName__c == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.user_name).trim())) {
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserEmail__c != NULL) {
                        if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserEmail__c != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.email).trim()) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserEmail__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.email).trim();
                        }
                    } else if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserEmail__c == NULL) {
                        if(objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.email != NULL) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserEmail__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.email).trim();
                        }
                    }
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserLdapId__c != NULL) {
                        if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserLdapId__c != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.ldap_id).trim()) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserLdapId__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.ldap_id).trim();
                        }
                    } else if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserLdapId__c == NULL) {
                        if(objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.ldap_id != NULL) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserLdapId__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.ldap_id).trim();
                        }
                    }
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserPasswordEncrypted__c != NULL) {
                        if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserPasswordEncrypted__c != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.password).trim()) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserPasswordEncrypted__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.password).trim();
                        }
                    } else if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserPasswordEncrypted__c == NULL) {
                        if(objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.password != NULL) {
                            objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserPasswordEncrypted__c = (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.password).trim();
                        }
                    }
                    if((objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserIsActive__c != objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.isactive) || (objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserSoftDeleteFlag__c != objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.softdeleteflag)) {
                        objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserIsActive__c = objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.isactive;
                        objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserSoftDeleteFlag__c = objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.softdeleteflag;
                    }
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserUserTypeLK__r.Name != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.user_type).trim()) {
                        for(AllMobileCatUserType__c objAllMobileCatUserType : lstAllMobileAllCatUserType) {
                            if(objAllMobileCatUserType.Name == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.user_type).trim()) { objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserUserTypeLK__c = objAllMobileCatUserType.Id;}
                        }
                    }
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserVkburId__c != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.vkbur).trim()) {
                        for(Account objAccountSalesOfficeIterate: lstAllAccountSalesOffice) {
                            if(objAccountSalesOfficeIterate.ONTAP__SalesOffId__c == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.vkbur).trim()) {objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserUserTypeLK__c = objAccountSalesOfficeIterate.Id;}
                        }
                    }
                    if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserRouteId__c != NULL) {
                        if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserRouteId__c != (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.route).trim()) {
                            for(ONTAP__Route__c objONTAPRoute : lstAllRoute) {
                                if(objONTAPRoute.ONTAP__RouteId__c == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.route).trim()) {objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserRouteLK__c = objONTAPRoute.Id;}
                            }
                        }
                    } else if(objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserRouteId__c == NULL) {
                        if(objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.route != NULL) {
                            for(ONTAP__Route__c objONTAPRoute : lstAllRoute) {
                                if(objONTAPRoute.ONTAP__RouteId__c == (objAllMobileDeviceUserHerokuObjectClassToCompareToUpdateIterate.route).trim()) {
                                    objAllMobileDeviceUserToCompareToUpdate.AllMobileDeviceUserRouteLK__c = objONTAPRoute.Id;
                                }
                            }
                        }
                    }
                    lstAllMobileDeviceUserToBeUpdatedInSF.add(objAllMobileDeviceUserToCompareToUpdate);
                }
            }
        }
        if(!lstAllMobileDeviceUserToBeUpdatedInSF.isEmpty() && lstAllMobileDeviceUserToBeUpdatedInSF != NULL) {
            update lstAllMobileDeviceUserToBeUpdatedInSF;
        }
    }

    /**
     * This method initiate a remote action to insert and update a List of DeviceUser objects of Salesforce according to a Heroku device_user table using POST AND PUT methods.
     */
    @RemoteAction
    webservice static void syncInsertUpdateDeviceUserFromHeroku() {
        List<AllMobileDeviceUser__c> lstAllMobileDeviceUserInSF = [SELECT Id, Name, AllMobileDeviceUserUserName__c, AllMobileDeviceUserId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserTypeLK__r.Name, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserIsActive__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserRouteLK__r.ONTAP__RouteId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileDeviceUser__c];
        syncInsertAndUpdateAllMobileDeviceUserInSFFromDeviceUserHeroku(lstAllMobileDeviceUserInSF);
    }

    /**
     * This method get and set a List of DeviceUser Ids of Salesforce
     *
     * @param lstAllMobileDeviceUser    List<AllMobileDeviceUser__c>
     * @return setIdAllMobileDeviceUser
     */
    public static Set<Id> getSetIdAllMobileDeviceUserSFFromListAllMobileDeviceUserSF(List<AllMobileDeviceUser__c> lstAllMobileDeviceUser) {
        Set<Id> setIdAllMobileDeviceUser = new Set<Id>();
        if(!lstAllMobileDeviceUser.isEmpty()) {
            for(AllMobileDeviceUser__c objAllMobileDeviceUser : lstAllMobileDeviceUser) {
                setIdAllMobileDeviceUser.add(objAllMobileDeviceUser.Id);
            }
        }
        return setIdAllMobileDeviceUser;
    }

    /**
     * This method get a List of all DeviceUser records of Heroku.
     *
     * @return List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass>
     */
    public static List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> getAllMobileAllDeviceUserHeroku() {
        String strJsonSerializeAllDeviceUserHeroku = AllMobileSyncObjectsClass.calloutGetAllDeviceUserFromHeroku();
        List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass> lstJsonDeserializeAllMobileAllDeviceUserHerokuObjectClass = (List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass>)JSON.deserialize(strJsonSerializeAllDeviceUserHeroku, List<AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass>.class);
        return lstJsonDeserializeAllMobileAllDeviceUserHerokuObjectClass;
    }

    /**
     * This method get an encrypted password .
     *
     * @return String
     */
    public static String getEncryptedPassword(String strPasswordClearText) {
        String strEncryptedPassword = AllMobileSyncObjectsClass.encryptPasswordDeviceUser(strPasswordClearText);
        return strEncryptedPassword;
    }

    /**
     * This method execute a queueable trigger which insert and update a List of DeviceUser Ids of Salesforce.
     *
     * @param objQueueableContext   QueueableContext
     */
    public void execute(QueueableContext objQueueableContext) {
        if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_INSERT) {
            syncInsertAllMobileDeviceUserToExternalObject(lstAllMobileDeviceUser);
        } else if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_UPDATE) {
            syncUpdateAllMobileDeviceUserToExternalObject(lstAllMobileDeviceUser);
        }
    }
}