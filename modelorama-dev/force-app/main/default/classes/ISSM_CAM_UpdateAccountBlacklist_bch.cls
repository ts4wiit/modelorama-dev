global class ISSM_CAM_UpdateAccountBlacklist_bch implements Database.Batchable<sObject> {
    
    private static final Id ACCOUNT_ACCOUNT_RECORDTYPEID;
    static{ACCOUNT_ACCOUNT_RECORDTYPEID = [SELECT id FROM Recordtype WHERE SobjectType ='Account' AND DeveloperName = 'Account' LIMIT 1].Id;}
    
    global Database.QueryLocator start(Database.BatchableContext bc ){
        system.debug('***ISSM CAM: START ISSM_CAM_UpdateAccountBlacklist');
        return Database.getQueryLocator([SELECT Id FROM Account WHERE RecordtypeId=:ACCOUNT_ACCOUNT_RECORDTYPEID]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope ){
        system.debug('***ISSM CAM: EXECUTE ISSM_CAM_UpdateAccountBlacklist');
    	ISSM_CAM_cls.isBlacklisted(scope);
    }
    
    global void finish(Database.BatchableContext bc ){
        system.debug('***ISSM CAM: FINISH ISSM_CAM_UpdateAccountBlacklist');
    }
}