/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que deserealiza el response de existencias

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       07-Julio-2017   Luis Licona                  Creación de la Clase
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_DeserializeMaterialAvailability_cls {
    public List<Materials> materials;

    public class Materials {
        public String productId;
        public String salesOffice;
        public String unit;
        public Long availableQuantity;
    }

    public static ISSM_DeserializeMaterialAvailability_cls parse(String json) {
        return (ISSM_DeserializeMaterialAvailability_cls) System.JSON.deserialize(json, ISSM_DeserializeMaterialAvailability_cls.class);
    }
}