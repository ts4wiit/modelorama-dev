/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Leopoldo Ortega (LO)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: Class to assign task to case force
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    07-Junio-2018    Leopoldo Ortega (LO) Creador - Process CAM & CS - Refrigeration Process
***********************************************************************************/
public class ISSM_AssignTaskToCaseForce {
    public static void assignTaskToCaseForce(List<ONTAP__Case_Force__c> DataCaseNew_lst) {
        List<Account> AccountsATM_lst = new List<Account>();
        List<ISSM_CS_CAM__c> CSCAM_lst = new List<ISSM_CS_CAM__c>();
        List<AccountTeamMember> ATMSalesOffice_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATMSalesOrg_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATMDRV_lst = new List<AccountTeamMember>();
        List<Task> CaseTask_lst = new List<Task>();
        List<Task> NewCaseTask_lst = new List<Task>();
        
        String strSalesOffice = '';
        String strSalesOrg = '';
        String strDRV = '';
        String strUserId = '';
        
        try {
            if (Test.isRunningTest()) { Account acct = new Account(Name='test acc'); insert acct; ONTAP__Case_Force__c ObjCaseForce = new ONTAP__Case_Force__c(); ObjCaseForce.ONTAP__Description__c = 'Descripción'; ObjCaseForce.ONTAP__Subject__c = 'Asunto'; ObjCaseForce.ONTAP__Status__c = 'Abierto'; ObjCaseForce.ONTAP__Account__c = acct.Id; ObjCaseForce.ISSM_StatusSerialNumber__c = 2; insert ObjCaseForce; DataCaseNew_lst.clear(); DataCaseNew_lst.add(ObjCaseForce); }
            for (ONTAP__Case_Force__c reg : DataCaseNew_lst) {
                // Verifica que el equipo del número de serie capturado exista pero no esté asignado al cliente en cuestión
                if (reg.ISSM_StatusSerialNumber__c == 2) {
                    AccountsATM_lst = [SELECT Id, Name, ISSM_SalesOffice__c, ISSM_SalesOrg__c, ISSM_RegionalSalesDivision__c
                                       FROM Account
                                       WHERE Id =: reg.ONTAP__Account__c
                                       LIMIT 1];
                    
                    CSCAM_lst = new List<ISSM_CS_CAM__c>();
                    CSCAM_lst = [SELECT Id, Name, SetupOwnerId FROM ISSM_CS_CAM__c];
                    
                    if (AccountsATM_lst.size() > 0) {
                        for (Account acc : AccountsATM_lst) {
                            // Save sales office from account
                            strSalesOffice = acc.ISSM_SalesOffice__c;
                            
                            // Save sales organization from account
                            strSalesOrg = acc.ISSM_SalesOrg__c;
                            
                            // Save regional sales division from account
                            strDRV = acc.ISSM_RegionalSalesDivision__c;
                        }
                    }
                    
                    ATMSalesOffice_lst = new List<AccountTeamMember>();
                    ATMSalesOrg_lst = new List<AccountTeamMember>();
                    ATMDRV_lst = new List<AccountTeamMember>();
                    
                    if ((strSalesOffice != null) && (strSalesOffice != '')) { ATMSalesOffice_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strSalesOffice AND TeamMemberRole = 'Trade Marketing'];
                    } else if ((strSalesOrg != null) && (strSalesOrg != '')) { ATMSalesOrg_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strSalesOrg AND TeamMemberRole = 'Trade Marketing'];
                    } else if ((strDRV != null) && (strDRV != '')) { ATMDRV_lst = [SELECT Id, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: strDRV AND TeamMemberRole = 'Trade Marketing']; }
                    
                    if (ATMSalesOffice_lst.size() > 0) { for (AccountTeamMember atm : ATMSalesOffice_lst) { strUserId = atm.UserId; }
                    } else if (ATMSalesOrg_lst.size() > 0) { for (AccountTeamMember atm : ATMSalesOrg_lst) { strUserId = atm.UserId; }
                    } else if (ATMDRV_lst.size() > 0) { for (AccountTeamMember atm : ATMDRV_lst) { strUserId = atm.UserId; }
                    } else if (CSCAM_lst.size() > 0) {
                        for (ISSM_CS_CAM__c atm : CSCAM_lst) {
                            strUserId = atm.SetupOwnerId;
                        }
                    }
                    
                    NewCaseTask_lst = new List<Task>();
                    
                    if ((strUserId != null) && (strUserId != '')) {
                        Task newTask = new Task();
                        newTask.OwnerId = strUserId;
                        newTask.WhatId = reg.Id;
                        newTask.Subject = 'Visita validación enfriador';
                        newTask.Status = 'Not Started';
                        newTask.Priority = 'Normal';
                        newTask.Description = System.Label.ISSM_equipment_not_correspond_client_P1 + ' ' + System.Label.ISSM_equipment_not_correspond_client_P2;
                        NewCaseTask_lst.add(newTask);
                        
                        if (NewCaseTask_lst.size() > 0) {
                            insert NewCaseTask_lst;
                        }
                    }
                }
            }
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
}