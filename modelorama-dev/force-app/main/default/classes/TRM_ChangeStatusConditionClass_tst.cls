/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description		:   Test class for schedule class TRM_ChangeStatusConditionClass_sch
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_ChangeStatusConditionClass_tst{
    
    static testmethod void testsScheduleIt() {
        Test.startTest();
        	TRM_ChangeStatusConditionClass_sch.scheduleIt(); 
        Test.stopTest();
    }
}