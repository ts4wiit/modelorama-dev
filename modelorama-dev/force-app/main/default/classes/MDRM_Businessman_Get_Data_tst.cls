@isTest
private class MDRM_Businessman_Get_Data_tst {
	
	@isTest static void test_questions() {
		Test.startTest();

        //String str = CheckExtension.getRecNumber('1');
        List<MDRM_Businessman_Get_Data_ctr.WrpQuestion> lstwq = MDRM_Businessman_Get_Data_ctr.lstQuestion();

        MDRM_Businessman_Get_Data_ctr.WrpQuestion wrpq = new MDRM_Businessman_Get_Data_ctr.WrpQuestion();

        
        Test.stopTest();
        //System.assertEquals('you receive one Record');
	}

	@isTest static void test_account_exist() {
		Test.startTest();

		///Crear Cuenta
		Account acci = createAccount();
		//Crear contacto
		Contact ctci = createContact(acci.Id);

        Boolean ctc = MDRM_Businessman_Get_Data_ctr.GetAccountExist('correop@correo.com');
        
        Test.stopTest();        
	}

	@isTest static void test_account_get() {
		Test.startTest();

		///Crear Cuenta
		Account acci = createAccount();
		//Crear contacto
		Contact ctci = createContact(acci.Id);

        Contact ctc = MDRM_Businessman_Get_Data_ctr.GetAccountInfo('correop@correo.com', '123456');
        
        Test.stopTest();        
	}

	@isTest static void test_postal_code() {
		Test.startTest();

		createPC();

        MDRM_Businessman_Get_Data_ctr.WrpPostalCode wpc = MDRM_Businessman_Get_Data_ctr.ValidatePostalCode('20000');

        MDRM_Businessman_Get_Data_ctr.WrpPostalCode wpc2 = MDRM_Businessman_Get_Data_ctr.ValidatePostalCode('20001');

        MDRM_Businessman_Get_Data_ctr.WrpPostalCode wrpPostal = new MDRM_Businessman_Get_Data_ctr.WrpPostalCode();

		Test.stopTest();        
	}

	@isTest static void test_save() {
		Test.startTest();

		string res = MDRM_Businessman_Get_Data_ctr.InsertForm('{"type":"Account","Name":"Nombre Prueba ApellidoP ApellidoM","ONTAP__Street__c":"Calle 123","ONTAP__Street_Number__c":"456","ONTAP__PostalCode__c":"20000","ONTAP__Province__c":"Aguascalientes","ONTAP__Municipality__c":"Aguascalientes","ONTAP__Colony__c":"Zona Centro","ONTAP__Email__c":"prueba@prueba.com"}',
			'{"type":"Contact","FirstName":"Nombre Prueba","LastName":"ApellidoP ApellidoM","MDRM_Gender__c":"M","Birthdate":"1967-05-23","MobilePhone":"30012345","Phone":"10012345","Email":"prueba@prueba.com"}',
			'{"type":"MDRM_Form__c","MDRM_How_did_you_hear_about__c":"Facebook","MDRM_How_did_you_hear_about_comment__c":"","MDRM_Grade_of_schooling__c":"High School (truncated or terminated)","MDRM_Marital_status__c":"Married","MDRM_People_financially_dependent__c":"1-2 dependent","MDRM_Currently_Employed__c":"No","MDRM_Current_employment_full_time__c":"No","MDRM_Current_Employment__c":"Driver  (Uber, taxi, truck, etc.)","MDRM_Activities_last_employment__c":"Tourism activities","MDRM_Activities_last_employment_comment__c":"","MDRM_Computer_domain_level__c":"Intermediate: Use Office packages, social networks, surf the internet","MDRM_Experience_operating_similar_store__c":"Yes, Oxxo","MDRM_Have_capital_necessary__c":"$ 50,000 to $ 59,999","MDRM_Former_or_retired_employee__c":"Yes","MDRM_Retired_military__c":"No","MDRM_Why_interested_entrepreneur__c":"Prueba Why"}','01/08/2017');

		Test.stopTest();        
	}

	@isTest static void test_update() {
		///Crear Cuenta
		Account acc = createAccount();
		//Crear contacto
		Contact ctc = createContact(acc.Id);
		//Crear Form
		MDRM_Form__c frm = createForm(acc.Id);

		Test.startTest();

		// Set mock callout class 
	    Test.setMock(HttpCalloutMock.class, new MDRM_Businessman_HttpCalloutMock_tst());
	    System.debug('setMock>> ');
		string res = MDRM_Businessman_Get_Data_ctr.UpdateForm('{"type":"Account","ONTAP__Street__c":"Calle 123","ONTAP__Street_Number__c":"456","ONTAP__PostalCode__c":"20000","ONTAP__POC_State__c":"Aguascalientes","ONTAP__Municipality__c":"Aguascalientes","ONTAP__Colony__c":"Zona Centro"}', '{"type":"Contact","Id":"'+ctc.Id+'","MobilePhone":"30012345","Phone":"10012345","Email":"prueba@prueba.com"}', '{"type":"MDRM_Form__c","MDRM_How_did_you_hear_about_comment__c":"","MDRM_Currently_Employed__c":"No","MDRM_Activities_last_employment__c":"Driver  (Uber, taxi, truck, etc.)","MDRM_Activities_last_employment_comment__c":"","MDRM_Have_capital_necessary__c":"$ 50,000 to $ 59,999","MDRM_Former_or_retired_employee__c":"Yes","MDRM_Why_interested_entrepreneur__c":""}');
		System.debug('endTest>>> ');
		Test.stopTest();        
	}
	
	@isTest static void test_mail() {
		///Crear Cuenta
		Account acc = createAccount();
		//Crear contacto
		Contact ctc = createContact(acc.Id);

		Test.startTest();
		
		boolean res = MDRM_Businessman_Get_Data_ctr.ToSendEmailPass('prueba@prueba.com');

		Test.stopTest();        
	}

	//*****     Datos     *****
	
	public static MDRM_Modelorama_Postal_Code__c createPC() {
	    MDRM_Modelorama_Postal_Code__c pc = new MDRM_Modelorama_Postal_Code__c();
	    pc.Name = 'Zona Centro';
	    pc.MDRM_Municipality__c = 'Aguascalientes';
	    pc.MDRM_Postal_Code__c = '20000';
	    pc.MDRM_State__c = 'Aguascalientes';
	    insert pc;
	    
	    return pc;
	}

	public static Account createAccount() {
	    Account account = new Account();
	    account.Name = 'Prueba Cuenta Expansor';
	    insert account;
	    
	    return account;
	}

	public static Contact createContact(Id idAccount) {
	    Contact contact = new Contact();
	    contact.AccountId  = idAccount;
	    contact.FirstName = 'Prueba Contacto';
	    contact.LastName = 'Expansor';
	    contact.Email = 'prueba@prueba.com';
	    insert contact;
	    
	    return contact;
	}

	public static MDRM_Form__c createForm(Id idAccount) {
	    MDRM_Form__c oForm = new MDRM_Form__c();
	    oForm.MDRM_Account_Form__c  = idAccount;	    
	    insert oForm;
	    
	    return oForm;
	}	
}