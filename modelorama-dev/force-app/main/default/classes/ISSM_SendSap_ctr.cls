/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Controller of ISSM_SendSapModal_lcp, for the extraction of combos records with ISSM_SentApproval status
*
* No.       Fecha              Autor                      Descripción
* 1.0    09-Mayo-2018      Oscar Alvarez                   Creación
*******************************************************************************/
public class ISSM_SendSap_ctr {    
    /*
    * Method Name	: sendSapCombos
    * Purpose		: To get the approval of combos
    * @Param        : strCombos = JSON string, which contains the combos to approve 
    */
    @AuraEnabled
    public static void sendSapCombos(String strCombos){
        list<ISSM_Combos__c> lstCombosForApproval = new List<ISSM_Combos__c>();
        list<String> lstCmbsForRetryModify = new List<String>();
        list<String> lstCmbsForRetryCreate = new List<String>();
        List<ISSM_Combos__c> lstCombos = new List<ISSM_Combos__c>();
        String strStatusCompareSA = 'ISSM_SentApproval';
        String strStatusCompareA = 'ISSM_Approved';
            
        lstCombos = (List<ISSM_Combos__c>)JSON.deserialize(strCombos, List<ISSM_Combos__c>.class);
        for(ISSM_Combos__c combo : lstCombos) {
            //Agregar combo para aprobación
            if(combo.ISSM_StatusCombo__c == strStatusCompareSA) lstCombosForApproval.add(combo);
            //Agregar combo(Modificado) para reintento de envio a mulesoft   
            if(combo.ISSM_ModifiedCombo__c && combo.ISSM_StatusCombo__c == strStatusCompareA) lstCmbsForRetryModify.add(combo.Id);
            //Agregar combo(Creado) para reintento de envio a mulesoft       
            if(!combo.ISSM_ModifiedCombo__c && combo.ISSM_StatusCombo__c == strStatusCompareA) lstCmbsForRetryCreate.add(combo.Id);
        }
        // Envio a proceso de aprobación | combos 
        system.debug('1- ### lstCombosForApproval: '+lstCombosForApproval.size());
        system.debug('2- ### lstCmbsForRetryModify: '+lstCmbsForRetryModify.size());
        system.debug('3- ### lstCmbsForRetryCreate: '+lstCmbsForRetryCreate.size());
        for(ISSM_Combos__c combo : lstCombosForApproval) ISSM_ApprovalProcess_cls.submitAndProcessApprovalRequest(combo.Id);
        //Ejecutar batch para el reintento de envio de combos para creación (Operacion A)
        if(lstCmbsForRetryCreate.size() > 0) Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstCmbsForRetryCreate,Label.TRM_OperationCreate), Integer.valueOf(Label.TRM_NumberLotsProcessed));
        //Ejecutar batch para el reintento de envio de combos para Modificacion (Operacion B)
        if(lstCmbsForRetryModify.size() > 0) Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstCmbsForRetryModify,Label.TRM_OperationModify), Integer.valueOf(Label.TRM_NumberLotsProcessed));
        
    }
    /*
    * Method Name	: getAccRecords
    * Purpose		: To get the wrapper of Columns and Headers
    * @Param         : strObjectName-> Name of the object to extract records
    */
    @AuraEnabled
    public static DataTableResponse getComboRecords(String strObjectName){                
        
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        List<String> lstFieldsToQuery = new List<String>();
        List<String> lstFieldDefault = new List<String>{'Name',
            'ISSM_StatusCombo__c',
            'ISSM_ComboType__c',
            'ISSM_SalesStructure__c',
            'ISSM_TypeApplication__c',
            'ISSM_StartDate__c',
            'ISSM_EndDate__c',
            'ISSM_NumberSalesOffice__c',
            'ISSM_NumberByCustomer__c'};
                DataTableResponse response = new DataTableResponse();
        
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(strObjectName).getDescribe().fields.getMap();
        
        for( Schema.SObjectField sfield : fieldMap.Values() ){
            schema.describefieldresult dfield = sfield.getDescribe();
            String dataType = String.valueOf(dfield.getType()).toLowerCase();
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            if (lstFieldDefault.contains(String.valueOf(dfield.getName()))){
                //Create a wrapper instance and store label, fieldname and type.
                DataTableColumns datacolumns = new DataTableColumns( String.valueOf(dfield.getLabel()) , 
                                                                    String.valueOf(dfield.getName()), 
                                                                    String.valueOf(dfield.getType()).toLowerCase() == 'datetime' ? 'date' : String.valueOf(dfield.getType()).toLowerCase());
                lstDataColumns.add(datacolumns);
                lstFieldsToQuery.add(String.valueOf(dfield.getName())); 
            }
        }
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(!lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String strIdUsuario = UserInfo.getUserId();
            String strStatusProcess = Label.TRM_StatusProcessApproval;
            Set<Id> setProcessInstanceId = new  Set<Id>();
            Set<Id> setProcessInstance = new  Set<Id>();
            // los isguientes loop's validan que para el usuario logueado tenga registros pendientes por aprobar
            for(ProcessInstanceWorkitem processInstItem : [SELECT ProcessInstanceId FROM ProcessInstanceWorkitem 
                                                           WHERE Actor.Id =: strIdUsuario  AND ProcessInstanceId IN (SELECT Id 
                                                           FROM ProcessInstance 
                                                           WHERE Status =: strStatusProcess)]){setProcessInstanceId.add(processInstItem.ProcessInstanceId);}
            for(ProcessInstance processInst:[SELECT TargetObjectId FROM ProcessInstance 
                                             WHERE Id IN :setProcessInstanceId]){ setProcessInstance.add(processInst.TargetObjectId);}
            
            String query = 'SELECT ISSM_ExternalKey__c,ISSM_ModifiedCombo__c,' + String.join(lstFieldsToQuery, ',');
            query 		+= ' FROM '+strObjectName;
            query 		+= ' WHERE (ISSM_StatusCombo__c = \'ISSM_SentApproval\' AND Id IN :setProcessInstance) OR (ISSM_StatusCombo__c = \'ISSM_Approved\' AND ISSM_SynchronizedWithSAP__c = false AND ISSM_SynchronizedCodeSAP__c <> \'01\')';	
            query 		+= ' ORDER BY ISSM_ExternalKey__c';
            System.debug(query);
            response.lstDataTableData = Database.query(query);
        }
        return response;
    }
        
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    } 
}