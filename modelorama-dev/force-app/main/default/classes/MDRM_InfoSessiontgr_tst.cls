/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Test Class for the Event Trigger.

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Test Class for the Event Trigger.
==============================================================================================================================
*********************************************************************************************************************************/

@istest
public class MDRM_InfoSessiontgr_tst {
    static testmethod void createsession(){
        date somedate = date.parse('05/11/2018');
        datetime startdate = DateTime.parse('05/11/2018 10:00 AM');
        datetime enddate = DateTime.parse('05/11/2018 11:00 AM');
        Id rtID = Schema.SObjectType.contact.getRecordTypeInfosByName().get('UEN Contact').getRecordTypeId();
        account acc = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        insert acc;
        account assistant = MDRM_TestUtils_tst.generateAccount(null, null, 123, null);
        insert assistant;
        
        contact uencontact = new contact(lastname = 'Testing', firstname = 'thistest', accountid = acc.id, MDRM_Active_UEN_Contact__c =true,
                                         email = 'testing9922@test.com', recordtypeid = rtID, phone = '8110260773',
                                        MDRM_UENContact_ID__c  = '66');
        insert uencontact;
         Id businessman = Schema.SObjectType.event.getRecordTypeInfosByName().get('Businessman').getRecordTypeId();
        event ev = new event(whatid = assistant.id, MDRM_UENContact__c = uencontact.id, Subject ='visit', StartDateTime = startdate,
                            EndDateTime =enddate, ActivityDate = somedate, recordtypeid = businessman);
      
        insert ev;
   
    }
}