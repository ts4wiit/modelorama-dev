public interface ISSM_IUserSelector_cls {
	
	List<User> selectById(Set<ID> idSet);
	User selectById(ID userID);
	List<User> selectById(String userID);
}