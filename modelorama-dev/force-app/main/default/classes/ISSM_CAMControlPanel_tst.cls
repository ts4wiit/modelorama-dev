@isTest //(SeeAllData=true)
public class ISSM_CAMControlPanel_tst {
    static testmethod void ISSM_CAMControlPanel_test(){
        ApexPages.StandardController sc;
        Account acc = new Account( Name = 'Test');
		ONTAP__KPI__c kpi01 = 
            new ONTAP__KPI__c(ONTAP__Kpi_id__c = 1,  ONTAP__Actual__c=500, ontap__kpi_name__c = 'Cerveza',
                              ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY(), ONTAP__Account_id__c = acc.id);
        ONTAP__KPI__c kpi02 = 
            new ONTAP__KPI__c(ONTAP__Kpi_id__c = 2,  ONTAP__Actual__c=100, ontap__kpi_name__c = 'Agua',
                              ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY(), ONTAP__Account_id__c = acc.id);
        ONTAP__KPI__c kpi25 = 
            new ONTAP__KPI__c(ONTAP__Kpi_id__c = 25, ONTAP__Actual__c=10,  ontap__kpi_name__c = 'Bebida Energizante',
                              ONTAP__Categorie__c='Volume', ONTAP__Kpi_start_date__c = date.TODAY(), ONTAP__Account_id__c = acc.id);
        ONTAP__Account_Asset__c AccAsset = 
            new ONTAP__Account_Asset__c(ONTAP__Account__c = acc.Id, ISSM_AssetType__c = 'R', ONTAP__Quantity__c=3);
        
        Test.startTest();
        insert AccAsset;
        insert kpi01;
        insert kpi02;
        insert kpi25;

        sc  = new ApexPages.standardController(Acc);
        ISSM_CAMControlPanel_ctr CTR = NEW ISSM_CAMControlPanel_ctr(sc); 
        
        Test.stopTest();
    }
}