/**
 * Test class for AllMobileApplicationSchedulerClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileApplicationSchedulerTest {

	/**
	 * Method to test execute method.
	 */
	static testMethod void testExecute() {

		//Set time.
		String strScheduleTimeApplication = '0 57 * * * ?';

		//Start test.
		Test.startTest();

		//Schedule Application
		AllMobileApplicationSchedulerClass objAllMobileApplicationSchedulerClass = new AllMobileApplicationSchedulerClass();
		System.schedule('jobApplication', strScheduleTimeApplication, objAllMobileApplicationSchedulerClass);

		//Stop test.
		Test.stopTest();
	}
}