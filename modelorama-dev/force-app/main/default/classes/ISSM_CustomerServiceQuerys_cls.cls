/****************************************************************************************************
    General Information
    -------------------   
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:Clase de querys para el consumo de los controladores
  
   
    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       05-09-2017      Hector Diaz                 Class create
    ================================================================================================
****************************************************************************************************/

public class ISSM_CustomerServiceQuerys_cls {
    /********************************************************
        Query al objeto CaseMilestone   Where = 
    ********************************************************/
    
    public List<CaseMilestone> QueryCaseMilestone(String strCaseID){
        List<CaseMilestone> lstCaseMilestone = new List<CaseMilestone>();
        lstCaseMilestone = [SELECT id,IsCompleted,IsViolated,TargetDate,CaseId,CompletionDate 
                            FROM CaseMilestone 
                            WHERE CaseId =: strCaseID
                            order by createddate asc limit 1];
        return lstCaseMilestone; 
    }
    
    /********************************************************
        Query al objeto CaseMilestone   Where IN
    ********************************************************/
    public List<CaseMilestone> QueryCaseMilestoneLst(List<String> lstIds){
        List<CaseMilestone> lstCaseMilestone = new List<CaseMilestone>();
        lstCaseMilestone = [SELECT id,IsCompleted,IsViolated,TargetDate,CaseId,CompletionDate  
                            FROM CaseMilestone  
                            WHERE CaseId IN :lstIds  
                            order by createddate asc limit 1 ];
        return lstCaseMilestone; 
    }
    
    /********************************************************
        Query al objeto ISSM_TypificationMatrix__c  
    ********************************************************/
    public List<ISSM_TypificationMatrix__c> QueryTypificationMatrix(String strTypificationNumber){
        List<ISSM_TypificationMatrix__c> lstTypificationMatrix = new List<ISSM_TypificationMatrix__c>();
        lstTypificationMatrix = [SELECT id,ISSM_SLAProvider__c,ISSM_CaseRecordType__c,ISSM_Entitlement__c,ISSM_AssignedTo__c, ISSM_OwnerQueue__c, 
                                        ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c,
                                        ISSM_TypificationLevel1__c,ISSM_TypificationLevel2__c,ISSM_TypificationLevel3__c,ISSM_TypificationLevel4__c,
                                        ISSM_TypificationLevel5__c,ISSM_TypificationLevel6__c,ISSM_Countries_ABInBev__c,
                                        ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, 
                                        ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c
                                 FROM ISSM_TypificationMatrix__c
                                 WHERE id =: strTypificationNumber]; 
        return lstTypificationMatrix;
    }
    
    public List<ISSM_TypificationMatrix__c> QueryTypificationMatrixBySet(Set<String> typNumber_set){
        List<ISSM_TypificationMatrix__c> lstTypificationMatrix = new List<ISSM_TypificationMatrix__c>();
        lstTypificationMatrix = [SELECT id,ISSM_SLAProvider__c,ISSM_CaseRecordType__c,ISSM_Entitlement__c,ISSM_AssignedTo__c, ISSM_OwnerQueue__c, 
                                        ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c,
                                        ISSM_TypificationLevel1__c,ISSM_TypificationLevel2__c,ISSM_TypificationLevel3__c,ISSM_TypificationLevel4__c,
                                        ISSM_TypificationLevel5__c,ISSM_TypificationLevel6__c,ISSM_Countries_ABInBev__c,
                                        ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, 
                                        ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c
                                 FROM ISSM_TypificationMatrix__c
                                 WHERE Id IN: typNumber_set];
        return lstTypificationMatrix;
    }
    
    /********************************************************
        Query al objeto ISSM_TypificationMatrix__c  getTypificationMatrix lstTypificationMatrixAllLevels
    ********************************************************/
    public List<ISSM_TypificationMatrix__c> QueryTypificationMatrixAllLevels(String strOptionSelectedLevel1,String strOptionSelectedLevel2,String strOptionSelectedLevel3,
                                                                            String strOptionSelectedLevel4,String strOptionSelectedLevel5,String strOptionSelectedLevel6){
        List<ISSM_TypificationMatrix__c> lstTypificationMatrixAllLevels = new List<ISSM_TypificationMatrix__c>();
        lstTypificationMatrixAllLevels = [Select id,ISSM_CaseRecordType__c,ISSM_Entitlement__c,ISSM_AssignedTo__c, ISSM_OwnerQueue__c, 
                                        ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c,
                                        ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, 
                                        ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, 
                                        ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c,
                                        ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_TypificationLevel5__c,
                                        ISSM_TypificationLevel6__c,ISSM_Countries_ABInBev__c
                                From ISSM_TypificationMatrix__c 
                                WHERE ISSM_TypificationLevel1__c =: strOptionSelectedLevel1 AND
                                        ISSM_TypificationLevel2__c =: strOptionSelectedLevel2 AND
                                        ISSM_TypificationLevel3__c =: strOptionSelectedLevel3 AND
                                        ISSM_TypificationLevel4__c =: strOptionSelectedLevel4 AND
                                        ISSM_TypificationLevel5__c =: strOptionSelectedLevel5 AND
                                        ISSM_TypificationLevel6__c =: strOptionSelectedLevel6 LIMIT 1]; 
        return lstTypificationMatrixAllLevels;
    }
    
    public List<ISSM_TypificationMatrix__c> QueryTypificationMatrixAllLevelsByChannel(String strOptionSelectedLevel1, String strOptionSelectedLevel2, String channel_str){
        List<ISSM_TypificationMatrix__c> lstTypificationMatrixAllLevelsByChannel = new List<ISSM_TypificationMatrix__c>();
        lstTypificationMatrixAllLevelsByChannel = [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, 
                                        ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c,
                                        ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, 
                                        ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, 
                                        ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_Countries_ABInBev__c, ISSM_Channel__c
                                FROM ISSM_TypificationMatrix__c 
                                WHERE ISSM_TypificationLevel1__c =: strOptionSelectedLevel1 AND
                                        ISSM_TypificationLevel2__c =: strOptionSelectedLevel2 AND
                                        ISSM_Channel__c =: channel_str LIMIT 1]; 
        return lstTypificationMatrixAllLevelsByChannel;
    }
    
    /********************************************************
        Query al objeto ISSM_TypificationMatrix__c  LIMIT 1
    ********************************************************/
    public ISSM_TypificationMatrix__c QueryTypificationMatrixLimit(String stridTypificationMatrix){
        ISSM_TypificationMatrix__c objTypificationMatrix = new ISSM_TypificationMatrix__c();
        try{
            objTypificationMatrix = [SELECT Id,ISSM_AssignedTo__c, 
            ISSM_OwnerQueue__c,ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, 
            ISSM_IdQueue__c, ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c,
            ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_TypificationLevel5__c,
            ISSM_TypificationLevel6__c,ISSM_Countries_ABInBev__c
                                FROM ISSM_TypificationMatrix__c 
                                WHERE Id =: stridTypificationMatrix Limit 1]; 
        } catch(QueryException qe){ objTypificationMatrix = null;  }
        return objTypificationMatrix;
    }
    /********************************************************
        Query al objeto AggregateResult  GROUP BY VISUALFORCE
    ********************************************************/
    public List<AggregateResult> QueryTypificationMatrixVF(){
        List<AggregateResult> lstTypificationMatrixAR = new List<AggregateResult>();
        lstTypificationMatrixAR = [Select ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_TypificationLevel5__c, ISSM_TypificationLevel6__c 
                                            From ISSM_TypificationMatrix__c 
                                            Where ISSM_Active__c = true  AND ISSM_Countries_ABInBev__c='México' AND ((ISSM_Channel__c != 'B2B') AND (ISSM_Channel__c != 'OnTap/OnCall'))
                                            GROUP BY ISSM_TypificationLevel1__c , ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_TypificationLevel5__c, ISSM_TypificationLevel6__c]; 
        return lstTypificationMatrixAR;
    }
    
    /********************************************************
        Query al objeto Case Id = 
    ********************************************************/
    public List<Case> QueryCase(String strParentId){
        //List<Case> lstCase = new List<Case>();
        //lstCase =  [SELECT id,ISSM_CaseForceNumber__c,DataTargetCaseMilestone__c,ISSM_Accountant__c
        //            FROM Case 
        //            WHERE Id =: strParentId  limit 1];
        Set<ID> idSet = new Set<ID>();
        idSet.add(strParentId);            
        //lstCase = new ISSM_CaseSelector_cls().selectById(idSet);
        return ISSM_CaseSelector_cls.newInstance().selectById(idSet);
    }  
    
    /********************************************************
        Query al objeto Case Id IN =:
    ********************************************************/
    public List<Case> QueryCaseLst(List<Id> lstDataCaseForceCommentNew){
        //List<Case> lstCase = new List<Case>();
        //lstCase =  [SELECT id,CaseNumber 
        //            FROM Case 
        //            WHERE ISSM_CaseForceNumber__c IN : lstDataCaseForceCommentNew];
        //return lstCase;
        return ISSM_CaseSelector_cls.newInstance().selectByCaseForceNumber(lstDataCaseForceCommentNew);
    }  
    /********************************************************
        Query al objeto Case Id IN =: SET Id
    ********************************************************/
    public List<Case> QueryCaseSet(Set<Id> setIdInsertCase){
        //List<Case> lstCase = new List<Case>();
        //lstCase =  [SELECT CaseNumber 
        //            FROM Case
        //            WHERE id IN : setIdInsertCase];
        //return lstCase;
        return ISSM_CaseSelector_cls.newInstance().selectById(setIdInsertCase);
        
    }  
    
    /********************************************************
        Query al objeto Case CaseNumber IN =: SET String
    ********************************************************/
    public List<Case> QueryCaseSetStr(Set<String> setStrtIdCase){
        //List<Case> lstCase = new List<Case>();
        //lstCase =  [SELECT id,CaseNumber
        //            FROM Case
        //            WHERE CaseNumber IN : setStrtIdCase];
        //return lstCase;
        return ISSM_CaseSelector_cls.newInstance().selectByCaseNumber(setStrtIdCase);
    }  
      
    /********************************************************
        Query al objeto Case CaseNumber LIKE =: SET String
    ********************************************************/
    public List<Case> QueryCaseSetStrIN(Set<String> setStrtIdCase){
        List<Case> lstCase = new List<Case>();
        lstCase =  [SELECT id,CaseNumber,status
                    FROM Case
                    WHERE CaseNumber IN: setStrtIdCase];
        return lstCase;
    }  
    
    /********************************************************
        Query al objeto Case QueryCaseCaseNumber  =:  String
    ********************************************************/
    public Case QueryCaseCaseNumber(String strCaseNumber){
        //Case objCase = new Case();
        //objCase =  [SELECT id,CaseNumber 
        //            FROM Case
        //            WHERE CaseNumber =: strCaseNumber limit 1];
        //return objCase;
        return ISSM_CaseSelector_cls.newInstance().selectByCaseNumber(strCaseNumber);
    }  
    
    /********************************************************
        Query al objeto ONTAP__Case_Force_Comment__c 
    ********************************************************/
    public List<ONTAP__Case_Force_Comment__c> QueryCaseForceComment(List<Id> lstIdsCaseComment){
        List<ONTAP__Case_Force_Comment__c> lstCaseForceComment = new List<ONTAP__Case_Force_Comment__c>();
        lstCaseForceComment =  [SELECT id,ONTAP__Comment__c
                                FROM ONTAP__Case_Force_Comment__c 
                                WHERE Id IN : LstIdsCaseComment limit 1 ];
        return lstCaseForceComment;
    }  
    
    /********************************************************
        Query al objeto ISSM_MappingFieldCase__c 
    ********************************************************/
    public List<ISSM_MappingFieldCase__c> QueryMappingFieldCase(){
        List<ISSM_MappingFieldCase__c> lstMappingFieldCase = new List<ISSM_MappingFieldCase__c>();
        lstMappingFieldCase =  [SELECT name,ISSM_APICaseForce__c
                                FROM ISSM_MappingFieldCase__c
                                WHERE Active__c = true];
        return lstMappingFieldCase;
    }   
    
    /********************************************************
        Query al objeto ISSM_AppSetting_cs__c 
    ********************************************************/
    public List<ISSM_AppSetting_cs__c> QueryAppSettings(){
        List<ISSM_AppSetting_cs__c> lstAppSettings = new List<ISSM_AppSetting_cs__c>();
        lstAppSettings  =  [SELECT ISSM_IdQueueWithoutOwner__c, ISSM_IdQueueFriendModel__c, ISSM_IdProviderWithOutMail__c, ISSM_IdQueueTelecolletion__c
                            FROM ISSM_AppSetting_cs__c 
                            limit 1];
        return lstAppSettings;
    }
    /********************************************************
        Query al objeto ISSM_AppSetting_cs__c ******
    ********************************************************/
    public List<ISSM_ValidateProspecSalesOffice__c> QueryValidateProspecSalesOffice(){
        List<ISSM_ValidateProspecSalesOffice__c   > lstAppSettings = new List<ISSM_ValidateProspecSalesOffice__c   >();
        lstAppSettings  =  [SELECT Name
                            FROM ISSM_ValidateProspecSalesOffice__c];
        return lstAppSettings; 
    }  
    /********************************************************
        Query al objeto ISSM_AppSetting_cs__c 
    ********************************************************/
    public ISSM_AppSetting_cs__c QueryAppSettingsObj(){ 
        ISSM_AppSetting_cs__c objAppSettings = new ISSM_AppSetting_cs__c();
        objAppSettings  =  [SELECT ISSM_IdPaymentNotRefelcted__c,ISSM_IdUnrecognizedCharge__c,ISSM_IdPaymentPlan__c,ISSM_IdQueueWithoutOwner__c,ISSM_IdQueueFriendModel__c,ISSM_IdProviderWithOutMail__c,ISSM_IdQueueTelecolletion__c
                            FROM ISSM_AppSetting_cs__c 
                            limit 1]; 
        return objAppSettings;
    }  
    
    /********************************************************
        Query al objeto ISSM_AppSetting_cs__c  LIST
    ********************************************************/
    public List<ISSM_AppSetting_cs__c> QueryAppSettingsList(){ 
        List<ISSM_AppSetting_cs__c> lstAppSettings = new List<ISSM_AppSetting_cs__c>();
        lstAppSettings  =  [SELECT ISSM_IdPaymentNotRefelcted__c,ISSM_IdUnrecognizedCharge__c,ISSM_IdPaymentPlan__c,ISSM_IdQueueWithoutOwner__c,ISSM_IdQueueFriendModel__c,ISSM_IdProviderWithOutMail__c,ISSM_IdQueueTelecolletion__c
                            FROM ISSM_AppSetting_cs__c 
                            limit 1]; 
        return lstAppSettings;
    }  
    
    /********************************************************
        Query al objeto Group 
    ********************************************************/
    public List<Group> QueryGroup(List <ISSM_TypificationMatrix__c>  lstDataTypificationMatix){
        List<Group> lstGroup = new List<Group>();
        lstGroup    =  [SELECT id 
                        FROM Group 
                        WHERE DeveloperName = : lstDataTypificationMatix[0].ISSM_OwnerQueue__c];
        return lstGroup;
    } 


    /********************************************************
        Query al objeto Account return number of rows for
        the campaing consult
    ********************************************************/
    public Integer getAccountNumberCampaing(){
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Account_RecordType);
        return database.countQuery('select count() from Account Where ISSM_FilterCondicion__c = true And ISSM_LastPaymentPlanDate__c = null and RecordTypeId=\''+ RecordTypeAccountId +'\' Limit 12000');
    }   
    
    /********************************************************
        Query al objeto Account 
    ********************************************************/
    public Account QueryAccount(String strAccountID){
        //Account objAccount = new Account();
        //objAccount  =  [SELECT Id,ONTAP__Email__c 
        //                FROM Account 
        //                WHERE id  =: strAccountID LIMIT 1];
        //return objAccount;
        Id idAccount = strAccountID;
        return ISSM_AccountSelector_cls.newInstance().selectById(idAccount, 'ISSM_SalesOffice__r');
    }  
    /********************************************************
        Query al objeto Account LIST
    ********************************************************/
    public List<Account> QueryAccountList(String strRecordTypeAccountId,String strTypificationLevel3,String strTypificationLevel4,List<Account> lstAccountsSalesOffice){
       // List<Account> lstAccountList = new List<Account>();
       // lstAccountList  =  [Select id,ONTAP__Email__c,ISSM_Additional_Email__c,ONCALL__Preferred_Contact_Email__c,ISSM_ProviderEntitlement__c,ISSM_BossRefrigeration__c
       //                     From Account 
       //                     Where RecordTypeId =: strRecordTypeAccountId AND
       //                         ISSM_ProviderClassification__c =: strTypificationLevel3 AND
       //                         ISSM_Brand__c =: strTypificationLevel4 AND
       //                        ISSM_SalesOffice__c =: lstAccountsSalesOffice[0].ISSM_SalesOffice__c limit 1 ];
       //System.debug('####lstAccountList : '+lstAccountList);
       
        //return lstAccountList;
        return ISSM_AccountSelector_cls.newInstance().selectByMultipleFields(strRecordTypeAccountId,strTypificationLevel3,strTypificationLevel4,lstAccountsSalesOffice[0].ISSM_SalesOffice__c +'');
    }  
    /********************************************************
        Query al objeto Account LIST SALESOFFICE
    ********************************************************/
    public List<Account> QueryAccountSalesOffice(List<Case> lstDataCaseNew){
        //List<Account> lstAccountList = new List<Account>();
        //lstAccountList  =  [SELECT ISSM_SalesOffice__c 
        //                    FROM Account
        //                    WHERE id =: lstDataCaseNew[0].AccountID limit 1 ];
        //return lstAccountList;
        if(lstDataCaseNew != null && lstDataCaseNew.size() > 0){
            Set<Id> idSet = new Set<Id>();
            idSet.add(lstDataCaseNew[0].AccountID);
            return ISSM_AccountSelector_cls.newInstance().selectById(idSet);
        }
        return null;
    }  
    /********************************************************
        Query al objeto Account  Id IN:
    ********************************************************/
    public Account QueryAccounIdInObj(List <Id> lstIdAccount){
        //Account objAccountList = new Account();
        //objAccountList  =  [SELECT Id,ONTAP__Email__c 
        //                    FROM Account 
        //                    WHERE id IN : lstIdAccount LIMIT 1];
        //return objAccountList;
        if(lstIdAccount != null && lstIdAccount.size() > 0){
            return ISSM_AccountSelector_cls.newInstance().selectById( lstIdAccount[0]);
        }
        return null; 
    }  
    /********************************************************
        Query al objeto Account  Id IN:
    ********************************************************/
    public List<Account> QueryAccounIdInLst(List <Id> lstIdsAccount){
        List<Account> lstAccountList = new List<Account>();
        lstAccountList  =  [SELECT id,name,ISSM_LastPromisePaymentDate__c,ISSM_LastContactDate__c,ISSM_LastPaymentPlanDate__c
                            FROM Account 
                            WHERE Id IN : lstIdsAccount];
        return lstAccountList; 
    }  
     /********************************************************
        Query al objeto Account  Id = SALES OFFICE 
    ********************************************************/ 
    public List<Account> QueryAccounSalesOffice( Set<String> lstIdsAccountSalesOffice , String RecordTypeSalesOffice){
        List<Account> lstAccountSalesOffice = new List<Account>();
        lstAccountSalesOffice = [SELECT id,Name,ParentId,ISSM_ParentAccount__c, ONTAP__SalesOffId__c, ONTAP__SalesOgId__c
                                FROM Account
                                WHERE ONTAP__SalesOffId__c  in: lstIdsAccountSalesOffice AND
                                RecordTypeId =: RecordTypeSalesOffice];  
                            
        return lstAccountSalesOffice; 
    }  
    /********************************************************
        Query al objeto Account  Id Organizacion  
    ********************************************************/
    public List<Account> QueryAccounSalesOrganization( Set<String> lstIdsAccountSalesOrg , String RecordTypeSalesOrg){
        List<Account> lstAccountSalesOrg = new List<Account>();
        lstAccountSalesOrg = [SELECT id,Name,ParentId,ISSM_ParentAccount__c, ONTAP__SalesOgId__c
                                FROM Account
                                WHERE ONTAP__SalesOgId__c  in: lstIdsAccountSalesOrg AND   
                                RecordTypeId =: RecordTypeSalesOrg];  
                            
        return lstAccountSalesOrg; 
    }  
    
    /********************************************************
        Query al objeto Account  ParentId  
    ********************************************************/
    public List<Account> QueryAccounSOParentId( Set<String> strIdsAccountParentId , String RecordTypeSalesOffice){
        List<Account> lstAccountParent = new List<Account>();
        lstAccountParent = [SELECT id,Name,ParentId,ISSM_ParentAccount__c
                                FROM Account
                                WHERE id  in: strIdsAccountParentId AND
                                RecordTypeId =: RecordTypeSalesOffice];  
                            
        return lstAccountParent;  
    }  
    /********************************************************
        Query al objeto ONTAP__Case_Force__c 
    ********************************************************/
    public List<ONTAP__Case_Force__c> QueryCaseForce(Set<Id> lstIdCaseForce){
        List<ONTAP__Case_Force__c> lstCaseForce = new List<ONTAP__Case_Force__c>();
        lstCaseForce =  [SELECT Id, Name,ISSM_CaseNumber__c 
                        FROM ONTAP__Case_Force__c 
                        WHERE Id IN : lstIdCaseForce];
        return lstCaseForce;
    }  
    
    /********************************************************
        Query al objeto ONTAP__Case_Force__c 
    ********************************************************/
    public List<ONCALL__Call__c> QueryOncallCall(String strIdCall){
        List<ONCALL__Call__c> lstOncallCall = new List<ONCALL__Call__c>();
        lstOncallCall =  [SELECT id,ONCALL__POC__c,RecordTypeId,ONCALL__Call_Status__c,
                                ISSM_CallEffectiveness__c,ISSM_NumberCallsMade__c,ISSM_PaymentPromise__c,ISSM_PaymentPromiseDate__c,
                                ISSM_PaymentPlanRequest__c,ISSM_PaymentPlanMaxDate__c,ISSM_ClarificationRequest__c,ISSM_ClarificationType__c
                        FROM ONCALL__Call__c 
                        WHERE id=: strIdCall];
        return lstOncallCall;
    }  
    /********************************************************
        Query al objeto ONCALL__Call__c order by ISSM_Order__c desc
    ********************************************************/
    public ONCALL__Call__c QueryOncallCallOrderBy(String IdTask){
        String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
        ONCALL__Call__c objOncallCallOrderBy = new ONCALL__Call__c();
        System.debug('IdTask : '+IdTask );
        System.debug('RecordTypeCallId : '+RecordTypeCallId );
        try{
         objOncallCallOrderBy =  [SELECT id, name, ISSM_Order__c
                                  FROM ONCALL__Call__c 
                                  WHERE Id = : IdTask
                                  AND Recordtypeid =: RecordTypeCallId           
                                  order by ISSM_Order__c desc LIMIT 1];
        } catch(QueryException qe){ objOncallCallOrderBy = null;  }
                              
           return  objOncallCallOrderBy;             
    }  
    /********************************************************
        Query al objeto User
    ********************************************************/
    public List<User> QueryUser(List<Account> lstAccounts){
        List<User> lstUser = new List<User>();
        lstUser =   [SELECT ManagerId 
                    FROM User
                    WHERE Id =: lstAccounts[0].ISSM_BossRefrigeration__c ];
        return lstUser;
    }
    /********************************************************
        Query al objeto ISSM_ColonyxAgency__c
    ********************************************************/
    public List<ISSM_ColonyxAgency__c> QueryColonyxAgency (Set<Id> setidsColonyAccount){
        List<ISSM_ColonyxAgency__c> lstColonyxAgency = new List<ISSM_ColonyxAgency__c>();
        lstColonyxAgency =  [SELECT id,ISSM_AgencyName__c
                            FROM ISSM_ColonyxAgency__c 
                            WHERE ISSM_ColonyNamePC__c IN : setidsColonyAccount ];
        return lstColonyxAgency;
    }
    /********************************************************
        Query al objeto AccountTeamMember
    ********************************************************/
    public List<AccountTeamMember> QueryAccountTeamMember (String strTeamMemberRole,set<Id> setidsColonyxAgencyName ){
        List<AccountTeamMember> lstAccountTeamMember = new List<AccountTeamMember>();
        lstAccountTeamMember =  [SELECT Id,UserId,AccountId 
                                 FROM AccountTeamMember 
                                 WHERE TeamMemberRole =: strTeamMemberRole  
                                        AND AccountId IN : setidsColonyxAgencyName  
                                        AND  User.IsActive = true ];
        return lstAccountTeamMember; 
    }
    /********************************************************
        Query al objeto AccountTeamMember AccountId :  SalesOffice
    ********************************************************/ 
    public List<AccountTeamMember> QueryAccountTeamMemberObj (String strTeamMemberRole,String strSalesOfficeId ){
        List<AccountTeamMember> lstAccountTeamMemberSO = new List<AccountTeamMember>();
        lstAccountTeamMemberSO =    [SELECT Id,UserId,AccountId  
                                FROM AccountTeamMember 
                                WHERE TeamMemberRole =: strTeamMemberRole
                                    AND AccountId  =: strSalesOfficeId
                                    AND  User.IsActive = true];
        return lstAccountTeamMemberSO; 
    }
     /********************************************************
        Query al objeto ISSM_AppSetting_cs__c  LIST
    ********************************************************/
    public Profile QueryProfile(){ 
        Profile objProfile = new Profile();
        objProfile  =  [SELECT Name FROM Profile WHERE Id =: userinfo.getProfileId() limit 1];  
        return objProfile;
    }  

     /********************************************************
        Query al objeto ISSM_AppSetting_cs__c  LIST
    ********************************************************/
    public List<ISSM_OpenItemB__c> getOpenItems(Set<Id> idsAccounts,List<String> DocumentType) {
        return [Select Id, Name,ISSM_Amounts__c, ISSM_Account__c,ISSM_ExpirationDays__c,ISSM_Debit_Credit__c from ISSM_OpenItemB__c where ISSM_Account__c in: idsAccounts
        /*and ONTAP__DocumentType__c IN:DocumentType*/]; 
    }
     
    public Map<ID, ISSM_OpenItemB__c> getOpenItemsById(Set<Id> idsOpenItems) {
        return new Map<ID, ISSM_OpenItemB__c>([Select Id, Name,ISSM_Amounts__c, ISSM_Account__c from ISSM_OpenItemB__c where id in: idsOpenItems]);   
    }    
    /********************************************************
        Query al objeto Attachment IN : set <String>
    ********************************************************/
    public List<Attachment> QueryAttachment(set<String> setAttcQuery) {
        List<Attachment> lstAttachment = new List<Attachment>();
        lstAttachment  =  [SELECT Id,ParentId, Name,body,ContentType FROM Attachment WHERE ParentId IN :setAttcQuery];  
        return lstAttachment;
    }
}