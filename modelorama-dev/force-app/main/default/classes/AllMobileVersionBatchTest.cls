/**
 * Test class for AllMobileVersionBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileVersionBatchTest {

	/**
	 * Method to setup data.
	 */
	@testSetup
	static void setup() {

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;

		//Creating a Version Preventa.
		AllMobileVersion__c objAllMobileVersion = AllMobileUtilityHelperTest.createAllMobileVersionObject('10.0.0', objAllMobileApplication);
		insert objAllMobileVersion;
	}

	/**
	 * Method to test AllMobileVersionBatchClass.
	 */
	static testMethod void testAllMobileVersionBatchClass() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		AllMobileVersionBatchClass objAllMobileVersionBatchClass = new AllMobileVersionBatchClass();
		Id idObjAllMobileVersionBatchClass = Database.executeBatch(objAllMobileVersionBatchClass);

		//Stop test.
		Test.stopTest();
	}
}