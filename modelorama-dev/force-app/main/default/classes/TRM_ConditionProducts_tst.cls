@isTest
private class TRM_ConditionProducts_tst {
	
	@isTest static void testSearchProducts() {

		List<MDM_Parameter__c> lstParam = new List<MDM_Parameter__c>();
		List<String> lststring = new List<String>();

		MDM_Parameter__c param1 = new MDM_Parameter__c(
			Active_Revenue__c = true, 
			Catalog__c = 'Segmento', 
			Code__c = '05',
			Description__c = 'MINI-MERCADO',
			ExternalId__c = 'Segmento-05'
		);
		lstParam.add(param1);
		MDM_Parameter__c param2 = new MDM_Parameter__c(
			Active_Revenue__c = true, 
			Catalog__c = 'GrupoMateriales2', 
			Code__c = '05',
			Description__c = '11.5 oz',
			ExternalId__c = 'GrupoMateriales2-05'
		);
		lstParam.add(param2);
		MDM_Parameter__c param3 = new MDM_Parameter__c(
			Active_Revenue__c = true, 
			Catalog__c = 'SectorProd', 
			Code__c = '00',
			Description__c = 'Camión',
			ExternalId__c = 'SectorProd-00'
		);
		lstParam.add(param3);
		insert lstParam;
		String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);
		ONTAP__Product__c product = new ONTAP__Product__c(
			ONTAP__ExternalKey__c = '000000000003000057',
			ONTAP__MaterialProduct__c = 'LEON 24/355 ML CT R',
			ISSM_Quota__c =  param1.id,
			ISSM_MaterialGroup2__c = param2.id,
			ISSM_SectorCode__c = param3.id,
			RecordTypeId = RecType,
			ONTAP__ProductType__c = 'FERT'
		);
		insert product;
		ONTAP__Product__c product2 = new ONTAP__Product__c(
			ONTAP__ExternalKey__c = '000000000003000058',
			ONTAP__MaterialProduct__c = 'LEON 24/355 ML CT R',
			ISSM_Quota__c =  param1.id,
			ISSM_MaterialGroup2__c = param2.id,
			ISSM_SectorCode__c = param3.id,
			RecordTypeId = RecType,
			ONTAP__ProductType__c = 'FERT'
		);
		insert product2;
		lststring.add('\''+product2.id+'\'');

		System.debug(loggingLevel.Error, '*** product: ' + product);
		System.debug(loggingLevel.Error, '*** product2: ' + product2);
		System.debug(loggingLevel.Error, '*** product.id: ' + product.id);
		System.debug(loggingLevel.Error, '*** product2.id: ' + product2.id);
		System.debug(loggingLevel.Error, '*** RecType: ' + RecType);

		TRM_ConditionProducts_ctr.SearchProducts(product.ISSM_Quota__c, '000000000003000057', lststring, '', true);
	}
	
	@isTest static void testGetProducts() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);
		ONTAP__Product__c product = new ONTAP__Product__c(
			ONTAP__ExternalKey__c = '000000000003000057',
			ONTAP__MaterialProduct__c = 'LEON 24/355 ML CT R',
			//ISSM_Quota__c =  param1.id,
			//ISSM_MaterialGroup2__c = param2.id,
			//ISSM_SectorCode__c = param3.id,
			RecordTypeId = RecType,
			ONTAP__ProductType__c = 'FERT'
		);
		insert product;

		TRM_ConditionRecord__c cR = new TRM_ConditionRecord__c(
			TRM_ConditionClass__c = trm.id,
			TRM_AmountPercentage__c = 10,
			TRM_Product__c	= product.id 
		);


		insert cR;
		TRM_ConditionProducts_ctr.getProducts(trm.id);
	}

	@isTest static void testGetScales() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;

		TRM_ConditionRecord__c cR = new TRM_ConditionRecord__c(
			TRM_ConditionClass__c = trm.id
		);
		insert cR;

		TRM_ProductScales__c pS = new TRM_ProductScales__c(
			TRM_ConditionRecord__c = cr.Id
		);
		insert pS;
		TRM_ConditionProducts_ctr.getScales(trm.id);
	}
    
    @isTest static void testExceptionRunQuery() {
        //Create a String with a SOQL requesting an invalid field to generate an exception
        String StrSoql = 'SELECT Id, NotAValidfield__c FROM ONTAP__Product__c';
        TRM_ConditionProducts_ctr.runQuery(StrSoql);
	}
}