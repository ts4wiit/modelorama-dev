/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Class with Invocable Method for process builder. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       06/07/2018      Cindy Fuentes		           Class with Invocable Method to create an attachment on the Informative Session.
==============================================================================================================================
*********************************************************************************************************************************/
public class MDRM_NewAttachmentProcessBuilder {
    @invocablemethod(label='Creates an attachment from a visualforce' description='Attachment')  
    public static void createattachment(List<ID> lstID){
        
        // Select the Informative Session that started your process builder. 
        mdrm_informative_session__c session =[select id, name, MDRM_SendEmail__c, MDRM_Attachment_Generated__c from mdrm_informative_session__C 
                                              where id in : lstid limit 1 ];
        id idsession = session.id;
        
        // Generate the visualforce with the informative Session from the process builder 
        PageReference ref = Page.mdrm_assistance_list_pdf;
        ref.getParameters().put('id', idsession);
        blob pageAsBlob;
        
        if(Test.isRunningTest()) { 
  		pageAsBlob = blob.valueOf('Blob Test');
        } else {
            pageAsBlob = ref.getContent();
        }
        list<attachment> oldatt = new list <attachment>([select id, parentid from attachment where parentid =: idsession]);
       integer attsize = oldatt.size();
        
        if(attsize != 0){
            delete oldatt;
            
        }
        // Create the attachment for the Informative Session using the Visualforce Page. 
        attachment att = new attachment(parentid = idsession, name = 'Assistance List', body = pageAsBlob,
                                        contenttype ='application/pdf');
        insert att;
        
        session.MDRM_SendEmail__c = false;
        update session;
    }
}