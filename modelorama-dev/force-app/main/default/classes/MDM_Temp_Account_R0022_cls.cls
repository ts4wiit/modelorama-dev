/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 22: Validation for KATR2 field, when this is equal to '30' then fields AUFSD , LIFSD and FAKSD must to be with the blocked code('01').
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       10-04-2018   	Iván Neria    			Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0022_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0022_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        Boolean ruleHasError = false;
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            
            
            for(MDM_FieldByRule__c field : uniqueFields(rule.MDM_Fields_by_Rule__r).Values()){
                //Validation for start proccess when the record is SH 
                    if(field.MDM_IsActive__c && toEvaluate.get(field.MDM_APIFieldName__c)!=null && toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z019) { 
                    Map<String,MDM_Temp_Account__c> mapSH = new Map<String,MDM_Temp_Account__c>();
                    Integer counterOfNulls=0;
                    //Iteration of Map that contains all the records with SH 
                    for(MDM_Temp_Account__c mp:MDM_Account_cls.getMapKUNNR(setsGranularity).values()){
                        if(mp.PARVW__c == toEvaluate.get(field.MDM_APIFieldName__c)){ 
                            mapSH.put(mp.VKORG__c, mp);
                        }
                    } 
                    if(mapSH.size()>1){
                        for(MDM_Temp_Account__c sh:mapSH.values()){
                            if(sh.AUFSD__c!=null && sh.FAKSD__c!=null && sh.LIFSD__c!=null){
                                if (sh.AUFSD__c!=Label.CDM_Apex_01 && sh.FAKSD__c!=Label.CDM_Apex_01 && sh.LIFSD__c!=Label.CDM_Apex_01){
                                  fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        		  validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c);
                        		  validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        		  ruleHasError = true;  
                                }
                                
                            } else {
                                 counterOfNulls++;
                            }
                            If(counterOfNulls>1){
                                  fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        		  validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c+Label.CDM_Apex_Error6+' '+sh.PARVW__c+Label.CDM_Apex_Error7+' ' +sh.VKORG__c );
                        		  validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        		  ruleHasError = true;
                                  counterOfNulls--;
                            }
                         }
                    }
                }
                if(ruleHasError) validationResults.add((MDM_Validation_Result__c)validationResult);
                validationResult = new MDM_Validation_Result__c();
                validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
                ruleHasError = false;
            }
        }
        return validationResults;
    }
}