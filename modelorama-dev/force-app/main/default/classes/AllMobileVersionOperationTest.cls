/**
 * Test class for AllMobileVersionOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileVersionOperationTest {

	/**
	 * Test method to setup data.
	 */
	@testSetup
	static void setup() {

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplicationPreventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplicationPreventa;

		//Creating an Application Autoventa.
		AllMobileApplication__c objAllMobileApplicationAutoventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('AUTOVENTA', '2');
		insert objAllMobileApplicationAutoventa;

		//Create a List of Versions.
		List<AllMobileVersion__c> lstAllMobileVersion = new List<AllMobileVersion__c>();
		for(Integer intI = 1; intI < 9; intI++) {
			lstAllMobileVersion.add(AllMobileUtilityHelperTest.createAllMobileVersionObject('VENTA+_(V' + String.valueOf(intI) + '.0.0)_02_03_PRO',  objAllMobileApplicationPreventa));
		}
		insert lstAllMobileVersion;
	}

	/**
	 * Test method to test Queueable.
	 */
	static testMethod void testQueueableInsertVersionInSFAndSendToHeroku() {

		//Variables to enqueue Job.
		String strEventTriggerFlagAfterInsertVersion = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_VERSION;
		List<AllMobileVersion__c> lstAllMobileVersionToInsertInHeroku = [SELECT Id, Name, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileVersion__c WHERE AllMobileVersionId__c LIKE 'VENTA%'];
		AllMobileVersionOperationClass objAllMobileVersionOperationClassInsert = new AllMobileVersionOperationClass(lstAllMobileVersionToInsertInHeroku, strEventTriggerFlagAfterInsertVersion);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileVersionOperationClassInsert);

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test Queueable updateVersionInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableUpdateVersionInSFAndSendToHeroku() {

		//Variables.
		String strEventTriggerFlagAfterUpdateVersion = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_VERSION;
		List<AllMobileVersion__c> lstAllMobileVersionToUpdate = [SELECT Id, Name, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileVersion__c WHERE AllMobileVersionId__c LIKE 'VENTA%'];

		//Updating Version.
		for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersionToUpdate) {
			objAllMobileVersion.AllMobileVersionId__c = '2';
		}
		AllMobileVersionOperationClass objAllMobileVersionOperationClassUpdate = new AllMobileVersionOperationClass(lstAllMobileVersionToUpdate, strEventTriggerFlagAfterUpdateVersion);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileVersionOperationClassUpdate);

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test Queueable syncInsertUpdateVersionFromHerokuRemoteAction method.
	 */
	static testMethod void testSyncInsertUpdateVersionFromHerokuRemoteAction() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileVersionOperationClass.syncInsertUpdateVersionFromHeroku();

		//Stop Test.
		Test.stopTest();
	}

	/**
	 * Test method to test Queueable syncDeleteAllMobileVersionInSF method.
	 */
	static testMethod void testSyncDeleteAllMobileVersionInSF() {

		//Variables.
		List<AllMobileVersion__c> lstAllMobileVersionInSF = [SELECT Id, Name, AllMobileVersionId__c, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileVersion__c WHERE AllMobileVersionId__c LIKE 'VENTA%'];

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileVersionOperationClass.syncDeleteAllMobileVersionInSF(lstAllMobileVersionInSF);

		//Stop Test.
		Test.stopTest();
	}
}