/**
 * Test class for AllMobileRouteAppVersionHelperClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileRouteAppVersionHelperTest {

	/**
	 * Test method to setup data.
	 */
	static testMethod void setup() {

		//Create a RouteAppVersion.
		ALlMobileRouteAppVersion__c objAllMobileRouteAppVersion = AllMobileUtilityHelperTest.createAllMobileRouteAppVersion('FG0001', '1.0.0', 1, Date.newInstance(2025, 07, 15), Date.newInstance(2025, 10, 20), false);
		insert objAllMobileRouteAppVersion;

		//Updating a RouteAppVersion.
		objALlMobileRouteAppVersion.AllMobileValidEndDate__c = Date.newInstance(2030, 12, 24);
		update objAllMobileRouteAppVersion;
	}
}