/**************************************************************************************
Name apex class: ISSM_DataCleasingJob_bch
Version: 1.0
Createdate: 29 June 2018
Functionality: Class to process objects of the custom settings to delete data and additionally, the option "Delete_to_Recycle_Bin__c" allows to remove deleted data from the recycling bin.
Test class: ISSM_DataCleasingJob_tst
Modifications history:
-----------------------------------------------------------------------------
* Developer            -       Date       -        Description
* ----------------------------------------------------------------------------
* Carlos Duque         26 - September - 2017      Original version
* Leopoldo Ortega        29 - June - 2018         Add logic in the method "QueryLocator" instead of "Iterator"
* Leopoldo Ortega       20 - Julio - 2018         Remove condition "ONTAP__BeginDate__c" in the object "ONTAP__Order__c"
*************************************************************************************/
global class ISSM_DataCleasingJob_bch implements Database.Batchable<sObject> {
    // Variables
	String query_str = ''; // Query to process to delete data
	String nameConfig_str = ''; // Name of the control's order
    Integer ordenConfig_int = 0; // Control's order of the custom settings
    
    String[] arrayRecordType; // Array to catch the recordtype
    String resultArray_str = ''; // Result after call the method "parseArrayRecordType" of the class "ISSM_DataCleasingJob_util"
    
    String typeConfig_str = ''; // Type of custom settings
    String emailConfig_str = ''; // Email to send notifications in case error
    String apiObject_str = ''; // Object to delete data
    Decimal nDays_dec = 0; // Quantity of days to query in decimal
    Integer nDays_int = 0; // Quantity of days to query in integer
    Boolean deleteToRecycleBin_bln = false; // Flag to know if delete data of recycle bin or not
    Boolean isActive_bln = false; // Flag to know if the object will be process
    
    // Constants
    String TYPEDAYS = System.Label.ISSM_Days;
    String TYPEDELETE = System.Label.ISSM_IsDeleted;
    Integer MINNDAYS = 60; // Control's constant
    
    // Constructor of the class for the assign of parameters and variables
    global ISSM_DataCleasingJob_bch(String nameConfigParam, Integer orden) {
        // Assign variables
        nameConfig_str = nameConfigParam;
        ordenConfig_int = orden;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
            // Get the custom settings
            ISSM_DataCleasingJob__c gv = ISSM_DataCleasingJob__c.getInstance(nameConfig_str);
            
            // Get the flag to know if the object will be process
            isActive_bln = (gv != null && gv.IsActive__c != null) ? gv.IsActive__c : false;
            
            // Get email to send notifications in case error
            emailConfig_str = (gv != null && gv.Email__c != null) ? gv.Email__c : '';
            
            // Get the object to delete data
            apiObject_str = (gv != null && gv.Objects__c != null) ? gv.Objects__c : '';
            
            // Get the type of custom settings
            typeConfig_str = (gv != null && gv.Type__c != null) ? gv.Type__c : '';
            
            // Get days to query
            nDays_dec = (gv != null && gv.NDays__c != null) ? gv.NDays__c : 0;
            nDays_int = Integer.ValueOf(nDays_dec);
            // Validate that the nDays have a valid value
            if ((nDays_dec == null) || (nDays_dec == 0) || (nDays_dec < MINNDAYS)) { nDays_int = MINNDAYS; }
            
            // Get the flag to know if delete data of recycle bin or not
            deleteToRecycleBin_bln = (gv != null && gv.Delete_to_Recycle_Bin__c != null) ? gv.Delete_to_Recycle_Bin__c : false;
            
            // Obtenemos el tipo de registro a considerar en la consulta
            if (gv != null && gv.RecordType__c != null) { arrayRecordType = gv.RecordType__c.split(';'); }
            
            // Process array for get the string and put in the query
            if (arrayRecordType != null) { resultArray_str = ISSM_DataCleasingJob_util.parseArrayRecordType(arrayRecordType); }
            
            // Validate that object have record type
            if (arrayRecordType == null) {
                // Validate the type of custom settings "IsDeleted"
                if (typeConfig_str == TYPEDELETE) {
                    query_str = 'SELECT Id FROM '+ apiObject_str +' WHERE ISSM_IsDeleted__c = true';
                }
                // Validate the type of custom settings "Days"
                if (typeConfig_str == TYPEDAYS) {
                    query_str = 'SELECT Id FROM '+ apiObject_str +' WHERE CreatedDate < LAST_N_DAYS:' + nDays_int;
                }
            } else {
                if (typeConfig_str == TYPEDELETE) { query_str = 'SELECT Id FROM '+ apiObject_str +' WHERE RecordTypeId IN('+ resultArray_str + ') AND ISSM_IsDeleted__c = true'; }
                if (typeConfig_str == TYPEDAYS) { query_str = 'SELECT Id FROM '+ apiObject_str +' WHERE RecordTypeId IN('+ resultArray_str + ') AND CreatedDate < LAST_N_DAYS:' + nDays_int; }
            }
            
            return Database.getQueryLocator(query_str);
        } catch (Exception ex) { ISSM_DataCleasingJob_util.sendEmail(emailConfig_str, nameConfig_str, ex.getMessage() + ' ## Line Number: ' + ex.getLineNumber()); return null; }
    }

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try {
            // Get the custom settings
            ISSM_DataCleasingJob__c gv = ISSM_DataCleasingJob__c.getInstance(nameConfig_str);
            
            // Get the flag to know if the object will be process
            isActive_bln = (gv != null && gv.IsActive__c != null) ? gv.IsActive__c : false;
            
            // Get email to send notifications in case error
            emailConfig_str = (gv != null && gv.Email__c != null) ? gv.Email__c : '';
            
            delete scope; if (deleteToRecycleBin_bln == true) { Database.EmptyRecycleBin(scope); }
        } catch (Exception ex) { ISSM_DataCleasingJob_util.sendEmail(emailConfig_str, nameConfig_str, ex.getMessage() + ' ## Line Number: ' + ex.getLineNumber()); }
    }
	global void finish(Database.BatchableContext BC) {
		// Variables
        List<ISSM_DataCleasingJob__c> config_lst = new List<ISSM_DataCleasingJob__c>();
        Integer batchSize_int = 0;
        // Constants
        Integer MAXBATCHSIZE = 10000; // Control's constant
        
        // Query to custom settings
        config_lst = [SELECT Id, Name, Order__c, Batch_size__c, IsActive__c FROM ISSM_DataCleasingJob__c WHERE IsActive__c = true AND Order__c >: ordenConfig_int ORDER BY Order__c ASC LIMIT 1];
        if (config_lst.size() > 0) {
            for (ISSM_DataCleasingJob__c reg : config_lst) {
                // Validate that batch have a validate value
                if ((reg.Batch_size__c == null) || (reg.Batch_size__c == 0) || (reg.Batch_size__c > MAXBATCHSIZE)) {
                    batchSize_int = MAXBATCHSIZE;
                } else {
                    batchSize_int = Integer.ValueOf(reg.Batch_size__c);
                }
                // Execute batch with the name and order found
                ISSM_DataCleasingJob_bch intObjectbch = new ISSM_DataCleasingJob_bch(reg.Name, Integer.ValueOf(reg.Order__c));
                database.executebatch(intObjectbch, batchSize_int);
            }
        } else { ordenConfig_int = 0; }
	}
}