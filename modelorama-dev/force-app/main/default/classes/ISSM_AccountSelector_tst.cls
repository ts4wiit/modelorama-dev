@isTest
private class ISSM_AccountSelector_tst {
	
	static testMethod void testselectById()
	{
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		Account dataAccount2 = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest2');
		List<Account> accountList = new List<Account> {dataAccount, dataAccount2};		
		Set<Id> idSet = new Set<Id>();
		for(Account item : accountList)
			idSet.add(item.Id);
			
		Test.startTest();		
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectById(idSet);		
		Test.stopTest();
		
		system.assertEquals(2,result.size());
		
	}

	static testMethod void testselectById2(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		Account result = new ISSM_Account_cls().getAccount(dataAccount.Id);
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(result.Id, dataAccount.Id);
	}

	static testMethod void testselectById3(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		Account dataAccount2 = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest2');
		List<Account> accountList = new List<Account> {dataAccount, dataAccount2};		
		Set<Id> idSet = new Set<Id>();
		for(Account item : accountList)
			idSet.add(item.Id);
			
		Test.startTest();		
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectById(idSet, 'ISSM_SalesOffice__r');		
		Test.stopTest();
		
		system.assertEquals(2,result.size());
	}


	static testMethod void testselectById4(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		Account result = ISSM_AccountSelector_cls.newInstance().selectById(dataAccount.Id);
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(result.Id, dataAccount.Id);
	}

	static testMethod void testselectById5(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectById(dataAccount.Id + '');
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(1, result.size());
	}

	static testMethod void testselectById6(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectById(dataAccount.Id + '', 'ISSM_SalesOffice__r');
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(1, result.size());
	}

	static testMethod void testselectByRecordType(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectByRecordType(recordTypeId);
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(1, result.size());
	}

	static testMethod void testselectByMultipleFields(){
		String RecordTypeAccountIdProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;   
        //Creamos Cuenta
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
            insert objAccount;

        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 0098','Refrigeración','Mantenimiento','Barril','Tanque CO2',null,null,'Trade Marketing',user.id,null);
        List<Account> accProvider  = ISSM_CreateDataTest_cls.fn_CreateAccountProvider (true,RecordTypeAccountIdProvider,objAccountSalesOffice.Id);

        Test.startTest();
        List<Account> result = ISSM_AccountSelector_cls.newInstance().selectByMultipleFields(RecordTypeAccountIdProvider,'Barril','Tanque CO2',objAccountSalesOffice.Id);
        Test.stopTest();

        system.assertNotEquals(result, null);
		system.assertEquals(1, result.size());
	}


	static testMethod void selectByRecordTypeByParentAccount(){
		Id recordTypeId = ISSM_CreateDataTest_cls.accountRecordTypeId();
		Account dataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,recordTypeId,'AccountTest');
		
		Test.startTest();
		List<Account> result = ISSM_AccountSelector_cls.newInstance().selectByRecordTypeByParentAccount(recordTypeId, null);
		Test.stopTest();
		system.assertNotEquals(result, null);
		system.assertEquals(1, result.size());
	}
	
}