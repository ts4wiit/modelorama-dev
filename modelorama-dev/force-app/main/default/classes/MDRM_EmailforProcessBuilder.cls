/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Class with Invocable Method for process builder. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       06/07/2018      Cindy Fuentes		           Class with Invocable Method to send the Informative Session's attachment 
													   by email to the UEN Contact.
==============================================================================================================================
*********************************************************************************************************************************/
public class MDRM_EmailforProcessBuilder {
    @invocablemethod(label='Send an email from event' description='sends an email') 
    
    public static void sendpdf(List<ID> lstID){

        try{
            
            // Select the Informative Session that started the process builder and it's attachment. 
            mdrm_informative_session__C session = [SELECT ID,MDRM_UEN_Contact__c, MDRM_UEN_Contact__r.id 
                                                   FROM MDRM_informative_session__c WHERE id  in :lstID ];
            
            string UENcontactEmail = session.MDRM_UEN_Contact__c;
            
            id idsession = session.id;
            
            attachment ref = [select id, body from attachment where parentid =:idsession order by createdDate DESC limit 1];
          
            // Create the email with defined sender information, recipient, and the attachment as a PDF file. 
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage semail= new Messaging.SingleEmailMessage();
            id owea = [select Id from OrgWideEmailAddress where Address = 'contactomodelorama@gmodelo.com.mx' limit 1].id;
            semail.setTargetObjectId(session.MDRM_UEN_Contact__r.id);
            EmailTemplate templateId = [Select id from EmailTemplate where name = 'MDRM_AssistanceList'];
            semail.setOrgWideEmailAddressId(owea);
            semail.setTemplateID(templateId.Id); 
            Messaging.EmailFileAttachment attach= new Messaging.EmailFileAttachment();
           attach.setBody(ref.body);
            attach.setContentType('application/pdf');
            attach.setFileName('assistancelist.pdf');
        	String[] emailIds= new String[]{UENcontactEmail};
               
            semail.setSaveAsActivity(false);
            semail.setToAddresses(emailIds);
            semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
            
            allmsg.add(semail);
            
            // Send the previously created email.
            Messaging.sendEmail(allmsg,false);
            
           
        }catch(Exception e){
            
            system.debug(e.getMessage());
        }
       
    }
}