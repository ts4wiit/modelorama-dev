/**
 * Test class CockpitStatusLogicClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class CockpitStatusLogicClassTest {

	@isTest
	static void testVisLevel0() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(0, Date.newInstance(2020, 12, 31), 'GM000', '');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel1() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(1, Date.newInstance(2020, 12, 31), '310', '');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel2() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(2, Date.newInstance(2020, 12, 31), 'FU0', '');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel3() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(3, Date.newInstance(2020, 12, 31), 'FD010', '');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel4() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(4, null, 'T-000705193', '');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel5() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(5, Date.newInstance(2020, 12, 31), 'GM0006', 'Finished');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel6() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(6, Date.newInstance(2020, 12, 31), '3104', 'Finished');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel7() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(7, Date.newInstance(2020, 12, 31), 'FU05', 'Finished');
		Test.stopTest();
	}

	@isTest
	static void testVisLevel8() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(8, Date.newInstance(2020, 12, 31), 'FD0101', 'Finished');
		Test.stopTest();
	}

	@isTest
	static void testLevel0Null() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(0, Date.newInstance(2020, 12, 31), 'FM', '');
		Test.stopTest();
	}

	@isTest
	static void testLevel1Null() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(1, Date.newInstance(2020, 12, 31), '35', '');
		Test.stopTest();
	}

	@isTest
	static void testLevel2Null() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(2, Date.newInstance(2020, 12, 31), 'FP', '');
		Test.stopTest();
	}

	@isTest
	static void testLevel3Null() {

		//Start test.
		Test.startTest();
		generateData();
		Map<Integer,Object> mapIntObjWC = CockpitStatusLogicClass.generateListObjsAccordVisualLevel(3, Date.newInstance(2020, 12, 31), 'FR', '');
		Test.stopTest();
	}

	@isTest
	static void testCreateTourWCListNull() {

		List<ONTAP__Tour__c> lstOntapTourEmtpy = new List<ONTAP__Tour__c>();
		List<CockpitStatusLogicClass.CockpitTourWrapperClass> lstCTourWC = new List<CockpitStatusLogicClass.CockpitTourWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCTourWC = CockpitStatusLogicClass.createLstCTourWCFLstOntapTour(lstOntapTourEmtpy);
		Test.stopTest();
	}

	@isTest
	static void testupdateStatusColorTextOfLstCTourWCNull() {

		List<CockpitStatusLogicClass.CockpitTourWrapperClass> lstCTourWC1 = new List<CockpitStatusLogicClass.CockpitTourWrapperClass>();
		List<CockpitStatusLogicClass.CockpitTourWrapperClass> lstCTourWC2 = new List<CockpitStatusLogicClass.CockpitTourWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCTourWC2 = CockpitStatusLogicClass.updateStatusColorTextOfLstCTourWC(lstCTourWC1);
		Test.stopTest();
	}

	@isTest
	static void testCreateOfficeWCListNull() {

		List<CockpitStatusLogicClass.CockpitTourWrapperClass> lstCTourWC = new List<CockpitStatusLogicClass.CockpitTourWrapperClass>();
		List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> lstCOfficeWC = new List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCOfficeWC = CockpitStatusLogicClass.createLstCOfficeWCFLstCTourWC(lstCTourWC);
		Test.stopTest();
	}

	@isTest
	static void testupdateStatusColorTextOfLstCOfficeWCNull() {

		List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> lstCOfficeWC1 = new List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>();
		List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> lstCOfficeWC2 = new List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCOfficeWC2 = CockpitStatusLogicClass.updateStatusColorTextOfLstCOfficeWC(lstCOfficeWC1);
		Test.stopTest();
	}

	@isTest
	static void testCreateOrgWCListNull() {

		List<CockpitStatusLogicClass.CockpitOfficeWrapperClass> lstCOfficeWC = new List<CockpitStatusLogicClass.CockpitOfficeWrapperClass>();
		List<CockpitStatusLogicClass.CockpitOrgWrapperClass> lstCOrgWC = new List<CockpitStatusLogicClass.CockpitOrgWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCOrgWC = CockpitStatusLogicClass.createLstCOrgWCFLstCOfficeWC(lstCOfficeWC);
		Test.stopTest();
	}

	@isTest
	static void testupdateStatusColorTextOfLstCOrgWCNull() {

		List<CockpitStatusLogicClass.CockpitOrgWrapperClass> lstCOrgWC1 = new List<CockpitStatusLogicClass.CockpitOrgWrapperClass>();
		List<CockpitStatusLogicClass.CockpitOrgWrapperClass> lstCOrgWC2 = new List<CockpitStatusLogicClass.CockpitOrgWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCOrgWC2 = CockpitStatusLogicClass.updateStatusColorTextOfLstCOrgWC(lstCOrgWC1);
		Test.stopTest();
	}

	@isTest
	static void testCreateDRVWCListNull() {

		List<CockpitStatusLogicClass.CockpitOrgWrapperClass> lstCOrgWC = new List<CockpitStatusLogicClass.CockpitOrgWrapperClass>();
		List<CockpitStatusLogicClass.CockpitDRVWrapperClass> lstCDRVWC = new List<CockpitStatusLogicClass.CockpitDRVWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCDRVWC = CockpitStatusLogicClass.createLstCDRVWCFLstCOrgWC(lstCOrgWC);
		Test.stopTest();
	}

	@isTest
	static void testupdateStatusColorTextOfLstCDRVWCNull() {

		List<CockpitStatusLogicClass.CockpitDRVWrapperClass> lstCDRVWC1 = new List<CockpitStatusLogicClass.CockpitDRVWrapperClass>();
		List<CockpitStatusLogicClass.CockpitDRVWrapperClass> lstCDRVWC2 = new List<CockpitStatusLogicClass.CockpitDRVWrapperClass>();

		//Start test.
		Test.startTest();
		generateData();
		lstCDRVWC2 = CockpitStatusLogicClass.updateStatusColorTextOfLstCDRVWC(lstCDRVWC1);
		Test.stopTest();
	}

	static void generateData() {

		//Insert Custom Settings (AllMobile Endpoints). (Tour).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//RecordTypes
		RecordType objRecordTypeAccountSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOrg');
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeOnTapRegionalClient = AllMobileUtilityHelperTest.getRecordTypeObject('Account');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');
		RecordType objRecordTypeAccountDRV = AllMobileUtilityHelperTest.getRecordTypeObject('ISSM_RegionalSalesDivision');

		//Get perfil to user
		Profile objProfileSystemAdministrator = AllMobileUtilityHelperTest.getProfileObject('System Administrator');

		//-------------------- R E G I O N 1 --------------------

		//Create DRV 1.
		Account objAccDRV1 = new Account();
		objAccDRV1.RecordTypeId = objRecordTypeAccountDRV.Id;
		objAccDRV1.Name = 'DRV Occidente Pacifico';
		objAccDRV1.ONTAP__SAPCustomerId__c = 'GM0006';
		insert objAccDRV1;

		//Create Org 1 DRV 1.
		Account objAccOrg1DRV1 = new Account();
		objAccOrg1DRV1.RecordTypeId = objRecordTypeAccountSalesOrg.Id;
		objAccOrg1DRV1.Name = 'CMM Nayarit';
		objAccOrg1DRV1.ONTAP__SalesOgId__c = '3104';
		objAccOrg1DRV1.ISSM_RegionalSalesDivision__c = objAccDRV1.Id;
		insert objAccOrg1DRV1;

		//Create Office 1, ORG 1, DRV 1.
		Account objAccOff1Org1DRV1 = new Account();
		objAccOff1Org1DRV1.RecordTypeId = objRecordTypeAccountSalesOffice.Id;
		objAccOff1Org1DRV1.Name = 'CMM Acaponeta';
		objAccOff1Org1DRV1.ONTAP__SalesOffId__c = 'FU05';
		objAccOff1Org1DRV1.ISSM_SalesOrg__c = objAccOrg1DRV1.Id;
		objAccOff1Org1DRV1.ONTAP__SalesOgId__c = '3104';
		insert objAccOff1Org1DRV1;

		//Create Office 2, Org 1, DRV 1.
		Account objAccOff2Org1DRV1 = new Account();
		objAccOff2Org1DRV1.RecordTypeId = objRecordTypeAccountSalesOffice.Id;
		objAccOff2Org1DRV1.Name = 'CMM San Nicolas';
		objAccOff2Org1DRV1.ONTAP__SalesOffId__c = 'FU06';
		objAccOff2Org1DRV1.ISSM_SalesOrg__c = objAccOrg1DRV1.Id;
		objAccOff2Org1DRV1.ONTAP__SalesOgId__c = '3104';
		insert objAccOff2Org1DRV1;

		//Create Vehicle 1, Office 1, Org 1, DRV 1.
		ONTAP__Vehicle__c objVehicle1Off1Org1DRV1 = new ONTAP__Vehicle__c();
		objVehicle1Off1Org1DRV1.ONTAP__VehicleId__c = '112345-23';
		objVehicle1Off1Org1DRV1.ONTAP__VehicleName__c = 'Zurdo Movil';
		objVehicle1Off1Org1DRV1.ONTAP__SalesOffice__c = objAccOff1Org1DRV1.id;
		insert objVehicle1Off1Org1DRV1;

		//Create Vehicle 1, Office 2, Org 1, DRV 1.
		ONTAP__Vehicle__c objVehicle1Off2Org1DRV1 = new ONTAP__Vehicle__c();
		objVehicle1Off2Org1DRV1.ONTAP__VehicleId__c = '112345-26';
		objVehicle1Off2Org1DRV1.ONTAP__VehicleName__c = 'Movil';
		objVehicle1Off2Org1DRV1.ONTAP__SalesOffice__c = objAccOff2Org1DRV1.id;
		insert objVehicle1Off2Org1DRV1;

		//Create Vehicle 2, Office 2, Org 1, DRV 1.
		ONTAP__Vehicle__c objVehicle2Off2Org1DRV1 = new ONTAP__Vehicle__c();
		objVehicle2Off2Org1DRV1.ONTAP__VehicleId__c = '112345-82';
		objVehicle2Off2Org1DRV1.ONTAP__VehicleName__c = 'Movil 2';
		objVehicle2Off2Org1DRV1.ONTAP__SalesOffice__c = objAccOff2Org1DRV1.id;
		insert objVehicle2Off2Org1DRV1;

		//User 1
		User objUser = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'minion001@abinbev.org.dev', 'minion', 'minion@gmail.com', 'ISO-8859-1', 'minion', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUser;

		//User 2
		User objUser2 = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'minion002@abinbev.org.dev', 'minion2', 'minion2@gmail.com', 'ISO-8859-1', 'minion2', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUser2;

		//Create Route 1, Office 1, Org 1, DRV 1.
		ONTAP__Route__c objRoute1Off1Org1DRV1 = new ONTAP__Route__c();
		objRoute1Off1Org1DRV1.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute1Off1Org1DRV1.ServiceModel__c = 'Presales';
		objRoute1Off1Org1DRV1.ONTAP__SalesOffice__c = objAccOff1Org1DRV1.Id;
		objRoute1Off1Org1DRV1.RouteManager__c = objUser.Id;
		objRoute1Off1Org1DRV1.Supervisor__c = objUser.id;
		objRoute1Off1Org1DRV1.Vehicle__c = objVehicle1Off1Org1DRV1.id;
		objRoute1Off1Org1DRV1.ONTAP__RouteId__c = 'FD0101';
		insert objRoute1Off1Org1DRV1;

		//Create Route 1, Office 2, Org 1, DRV 1.
		ONTAP__Route__c objRoute1Off2Org1DRV1 = new ONTAP__Route__c();
		objRoute1Off2Org1DRV1.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute1Off2Org1DRV1.ServiceModel__c = 'Presales';
		objRoute1Off2Org1DRV1.ONTAP__SalesOffice__c = objAccOff2Org1DRV1.Id;
		objRoute1Off2Org1DRV1.RouteManager__c = objUser2.Id;
		objRoute1Off2Org1DRV1.Supervisor__c = objUser2.id;
		objRoute1Off2Org1DRV1.Vehicle__c = objVehicle1Off2Org1DRV1.id;
		objRoute1Off2Org1DRV1.ONTAP__RouteId__c = 'FD0201';
		insert objRoute1Off2Org1DRV1;

		//Create Route 2, Office 2, Org 1, DRV 1.
		ONTAP__Route__c objRoute2Off2Org1DRV1 = new ONTAP__Route__c();
		objRoute2Off2Org1DRV1.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute2Off2Org1DRV1.ServiceModel__c = 'Presales';
		objRoute2Off2Org1DRV1.ONTAP__SalesOffice__c = objAccOff2Org1DRV1.Id;
		objRoute2Off2Org1DRV1.Vehicle__c = objVehicle2Off2Org1DRV1.id;
		objRoute2Off2Org1DRV1.ONTAP__RouteId__c = 'FD0203';
		insert objRoute2Off2Org1DRV1;

		//Create Tour 1, Route 1, Office 1, Org 1, DRV 1.
		ONTAP__Tour__c objTour1R1Off1Org1DRV1   = new ONTAP__Tour__c();
		objTour1R1Off1Org1DRV1.ONTAP__TourId__c = 'T-000705193';
		objTour1R1Off1Org1DRV1.ONTAP__TourStatus__c = 'Not Started';
		objTour1R1Off1Org1DRV1.TourSubStatus__c = 'Listo para descargar al dispositivo';
		//objTour1R1Off1Org1DRV1.VisitPlan__c = objVP1R1Off1Org1DRV1.Id;
		objTour1R1Off1Org1DRV1.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour1R1Off1Org1DRV1.Route__c = objRoute1Off1Org1DRV1.Id;
		insert objTour1R1Off1Org1DRV1;

		//Create Tour 1, Route 1, Office 2, Org 1, DRV 1.
		ONTAP__Tour__c objTour2R1Off2Org1DRV1 = new ONTAP__Tour__c();
		objTour2R1Off2Org1DRV1.ONTAP__TourId__c = 'T-000705194';
		objTour2R1Off2Org1DRV1.ONTAP__TourStatus__c = 'Finished';
		objTour2R1Off2Org1DRV1.TourSubStatus__c = 'Subida del dispositivo a la BD terminada OK.';
		objTour2R1Off2Org1DRV1.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour2R1Off2Org1DRV1.Route__c = objRoute1Off2Org1DRV1.Id;
		insert objTour2R1Off2Org1DRV1;

		//Create Tour 1, Route 2, Office 2, Org 1, DRV 1.
		ONTAP__Tour__c objTour1R2Off2Org1DRV1 = new ONTAP__Tour__c();
		objTour1R2Off2Org1DRV1.ONTAP__TourId__c = 'T-000705195';
		objTour1R2Off2Org1DRV1.ONTAP__TourStatus__c = 'Finished';
		objTour1R2Off2Org1DRV1.TourSubStatus__c = 'Subida del dispositivo a la BD terminada OK.';
		objTour1R2Off2Org1DRV1.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour1R2Off2Org1DRV1.Route__c = objRoute2Off2Org1DRV1.Id;
		insert objTour1R2Off2Org1DRV1;

		//-----------------Region 1 Ends-----------------

		//-----------------Region 2 Begins----------------------
		//Create DRV 2.
		Account objAccDRV2 = new Account();
		objAccDRV2.RecordTypeId = objRecordTypeAccountDRV.Id;
		objAccDRV2.Name = 'DRV Occidente 2';
		objAccDRV2.ONTAP__SAPCustomerId__c = 'GM0007';
		insert objAccDRV2;

		//create Org 1 DRV 2.
		Account objAccOrg1DRV2 = new Account();
		objAccOrg1DRV2.RecordTypeId = objRecordTypeAccountSalesOrg.Id;
		objAccOrg1DRV2.Name = 'CMM Nayarit 2';
		objAccOrg1DRV2.ONTAP__SalesOgId__c = '3105';
		objAccOrg1DRV2.ISSM_RegionalSalesDivision__c = objAccDRV2.Id;
		insert objAccOrg1DRV2;

		//create Org 2 DRV 2.
		Account objAccOrg2DRV2 = new Account();
		objAccOrg2DRV2.RecordTypeId = objRecordTypeAccountSalesOrg.Id;
		objAccOrg2DRV2.Name = 'CMM Nayarit 3';
		objAccOrg2DRV2.ONTAP__SalesOgId__c = '3106';
		objAccOrg2DRV2.ISSM_RegionalSalesDivision__c = objAccDRV2.Id;
		insert objAccOrg2DRV2;

		//Create Office 1, ORG 1, DRV 2.
		Account objAccOff1Org1DRV2 = new Account();
		objAccOff1Org1DRV2.RecordTypeId = objRecordTypeAccountSalesOffice.Id;
		objAccOff1Org1DRV2.Name = 'CMM Acaponeta 2';
		objAccOff1Org1DRV2.ONTAP__SalesOffId__c = 'FU06';
		objAccOff1Org1DRV2.ISSM_SalesOrg__c = objAccOrg1DRV2.Id;
		objAccOff1Org1DRV2.ONTAP__SalesOgId__c = '3105';
		insert objAccOff1Org1DRV2;

		//Create Office 1, ORG 2, DRV 2.
		Account objAccOff1Org2DRV2 = new Account();
		objAccOff1Org2DRV2.RecordTypeId = objRecordTypeAccountSalesOffice.Id;
		objAccOff1Org2DRV2.Name = 'CMM Acaponeta 3';
		objAccOff1Org2DRV2.ONTAP__SalesOffId__c = 'FU07';
		objAccOff1Org2DRV2.ISSM_SalesOrg__c = objAccOrg2DRV2.Id;
		objAccOff1Org2DRV2.ONTAP__SalesOgId__c = '3106';
		insert objAccOff1Org2DRV2;

		//Create Vehicle 1, Office 1, Org 1, DRV 2.
		ONTAP__Vehicle__c objVehicle1Off1Org1DRV2 = new ONTAP__Vehicle__c();
		objVehicle1Off1Org1DRV2.ONTAP__VehicleId__c = '112345-46';
		objVehicle1Off1Org1DRV2.ONTAP__VehicleName__c = 'Zurdo Movil 2';
		objVehicle1Off1Org1DRV2.ONTAP__SalesOffice__c = objAccOff1Org1DRV2.id;
		insert objVehicle1Off1Org1DRV2;

		//Create Vehicle 1, Office 1, Org 2, DRV 2.
		ONTAP__Vehicle__c objVehicle1Off1Org2DRV2 = new ONTAP__Vehicle__c();
		objVehicle1Off1Org2DRV2.ONTAP__VehicleId__c = '112338-59';
		objVehicle1Off1Org2DRV2.ONTAP__VehicleName__c = 'Zurdo Movil 3';
		objVehicle1Off1Org2DRV2.ONTAP__SalesOffice__c = objAccOff1Org2DRV2.id;
		insert objVehicle1Off1Org2DRV2;

		//Create Route 1, Office 1, Org 1, DRV 2.
		ONTAP__Route__c objRoute1Off1Org1DRV2 = new ONTAP__Route__c();
		objRoute1Off1Org1DRV2.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute1Off1Org1DRV2.ServiceModel__c = 'Presales';
		objRoute1Off1Org1DRV2.ONTAP__SalesOffice__c = objAccOff1Org1DRV2.Id;
		objRoute1Off1Org1DRV2.Vehicle__c = objVehicle1Off1Org1DRV2.id;
		objRoute1Off1Org1DRV2.ONTAP__RouteId__c = 'FD0207';
		insert objRoute1Off1Org1DRV2;

		//Create Route 1, Office 1, Org 2, DRV 2.
		ONTAP__Route__c objRoute1Off1Org2DRV2 = new ONTAP__Route__c();
		objRoute1Off1Org2DRV2.RecordTypeId = objRecordTypeOnTapRouteSales.Id;
		objRoute1Off1Org2DRV2.ServiceModel__c = 'Presales';
		objRoute1Off1Org2DRV2.ONTAP__SalesOffice__c = objAccOff1Org2DRV2.Id;
		objRoute1Off1Org2DRV2.Vehicle__c = objVehicle1Off1Org2DRV2.id;
		objRoute1Off1Org2DRV2.ONTAP__RouteId__c = 'FD0208';
		insert objRoute1Off1Org2DRV2;

		//Create Tour 1, Route 1, Office 1, Org 1, DRV 2.
		ONTAP__Tour__c objTour1R1Off1Org1DRV2 = new ONTAP__Tour__c();
		objTour1R1Off1Org1DRV2.ONTAP__TourId__c = 'T-000705205';
		objTour1R1Off1Org1DRV2.ONTAP__TourStatus__c = 'Assignment error';
		objTour1R1Off1Org1DRV2.TourSubStatus__c = 'Descarga de SAP finalizada con errores';
		objTour1R1Off1Org1DRV2.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour1R1Off1Org1DRV2.Route__c = objRoute1Off1Org1DRV2.Id;
		insert objTour1R1Off1Org1DRV2;

		//Create Tour 1, Route 1, Office 1, Org 2, DRV 2.
		ONTAP__Tour__c objTour1R1Off1Org2DRV2 = new ONTAP__Tour__c();
		objTour1R1Off1Org2DRV2.ONTAP__TourId__c = 'T-000705206';
		objTour1R1Off1Org2DRV2.ONTAP__TourStatus__c = 'Finished';
		objTour1R1Off1Org2DRV2.TourSubStatus__c = 'Subida del dispositivo a la BD terminada OK.';
		objTour1R1Off1Org2DRV2.ONTAP__TourDate__c = Date.newInstance(2020, 12, 31);
		objTour1R1Off1Org2DRV2.Route__c = objRoute1Off1Org2DRV2.Id;
		insert objTour1R1Off1Org2DRV2;

		//-----------------Region 2 Ends----------------------
	}
}