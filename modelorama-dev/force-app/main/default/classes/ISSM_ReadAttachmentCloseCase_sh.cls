/****************************************************************************************************
    General Information
    -------------------
    author:     Hector Diaz
    email:      hdiaz@gmail.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       29/Sep/2017       Hector Diaz	HD 				
    ================================================================================================
****************************************************************************************************/
global class ISSM_ReadAttachmentCloseCase_sh implements Schedulable {
     global void execute(SchedulableContext SC){
     	ISSM_ReadAttachmentCloseCase_cls  objBatch= new ISSM_ReadAttachmentCloseCase_cls();
		database.executeBatch(objBatch);
     }
}