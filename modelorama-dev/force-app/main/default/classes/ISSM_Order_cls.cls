/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice orders

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_Order_cls implements ISSM_ObjectInterface_cls{

	ONTAP__Order__c accountOrder_obj;
	public List<ONTAP__Order__c> accountsOrder_lst;

	/**
	 * Class constructor		                             
	 */
	public ISSM_Order_cls(){

	}



	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  Map<String, Account> mapAccountParam
	 */
	public ISSM_Order_cls(List<salesforce_ontap_order_c__x> accountOrderExt, Map<String, Account> mapAccount) {
		accountsOrder_lst = getList(accountOrderExt, mapAccount);
	}

	/**
	 * Create objects to delete		                             
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObjectDelete(Map<String, Account> mapAccount){
	}

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public List<sObject> getList(List<sObject> accountOrdersExt, Map<String, Account> mapAccount){
		List<ONTAP__Order__c> accountsOrdersReturn;
		Map<Id,List<ONTAP__Order__c>> accountOrder_map = new Map<Id,List<ONTAP__Order__c>>();
		ONTAP__Order__c acctOrder;
		Set<String> accountSap =mapAccount.keySet() ;
		accountsOrdersReturn = new List<ONTAP__Order__c>();
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		if(test.isRunningTest()){
			for(Integer i = 0;i<15;i++){
				salesforce_ontap_order_c__x orderext = new salesforce_ontap_order_c__x(
					ontap_sapcustomerid_c__c='000000000'+i,
					ontap_begindate_c__c=System.now().addDays(-i),
					oncall_total_order_item_quantity_c__c = 10,
					ontap_cashcreditcontrol_c__c='S',
					ontap_deliverydate_c__c=System.now().addDays(15),
					issm_hectolitersvolumen_c__c=10,
					issm_sap_number_c__c='000000000'+i,
					ontap_orderstatus_c__c='Draft',
					ontap_totalamount_c__c=200,
					systemmodstamp__c = System.today()
				);
				system.debug( '\n\n  ****** orderext = ' + orderext+'\n\n' );
				accountsOrdersReturn.add(getOrder(mapAccount, orderext, devRecordTypeId));
			}
		} else{
			for(SObject accountOrderExt: [SELECT account_ontap_sapcustomerid_c__c,systemmodstamp__c,oncall_origin_c__c,
			ontap_sapcustomerid_c__c, ontap_begindate_c__c,oncall_total_order_item_quantity_c__c,ontap_cashcreditcontrol_c__c, 
			ontap_deliverydate_c__c,issm_hectolitersvolumen_c__c, name__c, ontap_orderstatus_c__c, ontap_totalamount_c__c,issm_sap_number_c__c
				FROM salesforce_ontap_order_c__x
				 WHERE ontap_sapcustomerid_c__c in :accountSap and systemmodstamp__c >= LAST_N_MONTHS:2]){

				accountsOrdersReturn.add(getOrder(mapAccount, accountOrderExt, devRecordTypeId));
			}
		}

		return accountsOrdersReturn;

	}

	/**
	 * Get order record		                             
	 * @param  Map<String, Account> mapAccount
	 * @param  sObject accountAssetExt
	 * @param  Id devRecordTypeId
	 */
	private ONTAP__Order__c getOrder(Map<String, Account> mapAccount , sObject accountOrderExt, Id devRecordTypeId){
		ONTAP__Order__c acctOrder;
		acctOrder = new ONTAP__Order__c();
		try{
			acctOrder.ONTAP__OrderAccount__c = mapAccount.get(((salesforce_ontap_order_c__x)accountOrderExt).ontap_sapcustomerid_c__c).Id;
			acctOrder.ONCALL__OnCall_Account__c = mapAccount.get(((salesforce_ontap_order_c__x)accountOrderExt).ontap_sapcustomerid_c__c).Id;
			acctOrder.ONTAP__BeginDate__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_begindate_c__c;
			acctOrder.ONCALL__Total_Order_Item_Quantity__c = ((salesforce_ontap_order_c__x)accountOrderExt).oncall_total_order_item_quantity_c__c;
			acctOrder.ONTAP__CashCreditControl__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_cashcreditcontrol_c__c;
			acctOrder.ISSM_DeliveryDate__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_deliverydate_c__c;
			acctOrder.ONTAP__DeliveryDate__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_deliverydate_c__c;
			acctOrder.ISSM_HectolitersVolumen__c = ((salesforce_ontap_order_c__x)accountOrderExt).issm_hectolitersvolumen_c__c;
			acctOrder.ONCALL__SAP_Order_Number__c = ((salesforce_ontap_order_c__x)accountOrderExt).name__c;
			acctOrder.ONTAP__OrderStatus__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_orderstatus_c__c;
			acctOrder.ONTAP__TotalAmount__c = ((salesforce_ontap_order_c__x)accountOrderExt).ontap_totalamount_c__c;
			acctOrder.ONTAP__SAPCustomerId__c = ((salesforce_ontap_order_c__x)accountOrderExt).issm_sap_number_c__c;
			acctOrder.ONCALL__Origin__c = ((salesforce_ontap_order_c__x)accountOrderExt).oncall_origin_c__c;
			acctOrder.RecordTypeId = devRecordTypeId;
		} catch(NullPointerException ne){
			system.debug( '\n\n  ****** ne = ' + ne+'\n\n' );
		}
		return acctOrder;
	}

	/**
	 * Save the result of the sincronization process		                             
	 */
	public void save(){
		try{
			if(accountsOrder_lst != null)
			upsert accountsOrder_lst ONCALL__SAP_Order_Number__c;
		}catch(Exception e){
			System.debug(e.getMessage());
			System.debug('### ISSM_AccountOrders_bch: ISSM_Order_cls save (UPSERT FAILED)'); 
		}
	}

	/**
	 * Delete internal records that has the flag isdeleted on true		                             
	 */
	public Boolean deleteObjectsFinish(){return true;}

	/**
	 * Delete external records that has the flag isdeleted on true		                             
	 */
	public void deleteObjectsFinishExt(){

	}

	/**
	 * Create Objects to sincronice		                             
	 * Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObject(Map<String, Account> mapAccount){
		accountsOrder_lst = getList(null, mapAccount);

	}


	public Map<String, ONTAP__Order__c> getMapOrderOrderNumber(){
		Map<String, ONTAP__Order__c> orderMap = new Map<String, ONTAP__Order__c>(); 
		for(ONTAP__Order__c order : [Select Id,ONCALL__SAP_Order_Number__c  From ONTAP__Order__c Limit 10]){
		        orderMap.put(order.ONCALL__SAP_Order_Number__c, order);
		}
		return orderMap;
	}

	public Map<String, ONTAP__Order__c> getMapOrderOrderNumber(List<sObject> orders){
		Map<String, ONTAP__Order__c> orderMap = new Map<String, ONTAP__Order__c>();
		ONTAP__Order__c order; 
		for(Integer i = 0; i < orders.size(); i++){
				order = (ONTAP__Order__c)orders.get(i);
		        orderMap.put(order.ONCALL__SAP_Order_Number__c, order);
		}
		return orderMap;
	}
}