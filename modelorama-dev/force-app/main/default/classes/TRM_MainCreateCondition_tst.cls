@isTest
private class TRM_MainCreateCondition_tst {
	
	@isTest static void testGetConditionManagement() {
		Test.startTest();
			TRM_MainCreateCondition_ctr.getConditionManagement('ZTPM_986');
		Test.stopTest();
	}
	
	@isTest static void testGetConditionRelationship() {
		Test.startTest();
			TRM_MainCreateCondition_ctr.getConditionRelationship('ZTPM_986');
		Test.stopTest();
	}

	@isTest static void testGetRecordById() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		Test.startTest();
			TRM_MainCreateCondition_ctr.getRecordById('TRM_ConditionClass__c', trm.id);
		Test.stopTest();
	}

	@isTest static void testGetCatalog() {
        List<MDM_Parameter__c> catalogList = new List<MDM_Parameter__c>();
        MDM_Parameter__c pzone = new MDM_Parameter__c();
        pzone.Active__c = true;
        pzone.Active_Revenue__c = true;
        pzone.Catalog__c = 'PriceZone';
        pzone.ExternalId__c = 'PriceZone-01';
        catalogList.add(pzone);
        insert catalogList;
		Test.startTest();
			TRM_MainCreateCondition_ctr.getCatalog();
		Test.stopTest();
	}

	@isTest static void testGetStructures() {
        String RecordTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
		//Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
        Test.startTest();
			TRM_MainCreateCondition_ctr.getStructures();
		Test.stopTest();
	}

	@isTest static void testDeleteRecord() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		Test.startTest();
			TRM_MainCreateCondition_ctr.deleteRecord(trm);
		Test.stopTest();
	}
	
}