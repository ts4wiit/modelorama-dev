/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for AccountTeamMember
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public class ISSM_AccountTeamMemberSelector_cls extends fflib_SObjectSelector implements ISSM_IAccountTeamMemberSelector_cls
{

	public static ISSM_IAccountTeamMemberSelector_cls newInstance()
	{
		return (ISSM_IAccountTeamMemberSelector_cls) ISSM_Application_cls.Selector.newInstance(AccountTeamMember.SObjectType);
	}

	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				AccountTeamMember.Id,
				AccountTeamMember.UserId,
				AccountTeamMember.AccountId
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return AccountTeamMember.sObjectType;
	}

	public List<AccountTeamMember> selectById(Set<ID> idSet)
	{
		return (List<AccountTeamMember>) selectSObjectsById(idSet);
	}

	public AccountTeamMember selectById(ID accountTeamMemberID){
		return (AccountTeamMember) selectSObjectsById(accountTeamMemberID);
	}

	public List<AccountTeamMember> selectById(String accountTeamMemberID){
		return (List<AccountTeamMember>) selectSObjectsById(accountTeamMemberID);
	}	

	public List<AccountTeamMember> selectByMultipleFields(String strTeamMemberRole,set<Id> accountSetID)
	{
		assertIsAccessible();
		return (List<AccountTeamMember>) Database.query(
				String.format(
				'select {0}, ' +
				  'from {1} ' +
				  'WHERE TeamMemberRole =: strTeamMemberRole ' + 
				  'AND AccountId IN : accountSetID ' + 
				  'AND User.IsActive = true ' ,
				new List<String> {
					getFieldListString(),
					getSObjectName() } ) );
	}

}