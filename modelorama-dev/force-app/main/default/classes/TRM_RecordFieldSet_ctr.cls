/**
 * Development by:		Avanxo Mexico
 * Author:				Carlos Pintor / cpintor@avanxo.com
 * Project:				AbInbev - Trade Revenue Management
 * Description:			Apex Controller class of the Lightning Component 'TRM_RecordFieldSet_lcp'.
 * 						Methods included achives the processes: 
 * 						- get field definitions of a field set
 * 						- get the Record Type Id from the Developer Name
 * 						- get a sObject Record from the Salesforce Id
 * 						
 *
 * N.     Date             Author                     Description
 * 1.0    2018-08-21       Carlos Pintor              Created
 *
 */
public with sharing class TRM_RecordFieldSet_ctr {

	/**
    * @description  Receives two parameters with the object API name and the fieldset API name to search for. 
    *               The results are returned in a 'FieldSetMember' type list.
    * 
    * @param    objectApiName 		API name of the Object. E.g: 'TRM_ConditionClass__c'
    * @param    fieldSetApiName 	API name of the Field Set. E.g: 'TRM_Default'
    * 
    * @return   Return a list of type 'FieldSetMember' with definition of fields.
    * 			Each field definition contains: DBRequired, fieldPath, label, required, type, value, predefinedValue, hidden, readOnly
    */
	@AuraEnabled
    public static List<FieldSetMember> getFields(String objectApiName, String fieldSetApiName) {
    	System.debug('#### start TRM_RecordFieldSet_ctr.getFields()');

    	List<FieldSetMember> fset = new List<FieldSetMember>();
    	if( (objectApiName != null || objectApiName != '') && (fieldSetApiName != null || fieldSetApiName != '') ){
	    	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
		    Schema.DescribeSObjectResult describe = targetType.getDescribe();
		        
		    Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
		    Schema.FieldSet fs = fsMap.get(fieldSetApiName);
		    if(fs != null){
		    	List<Schema.FieldSetMember> fieldSet = fs.getFields();
		        
			    for (Schema.FieldSetMember f: fieldSet) {
			        fset.add(new FieldSetMember(f));
			    }
		    }
		    
    	}
        System.debug('#### fields size:' + fset.size());
        return fset;
    }

    /**
    * @description  Receives two parameters with the object API name and the record type developer name to get the Record Type Id.
    *               The found Record Type Id is returned in a String variable.
    * 
    * @param    objectType 			API name of the Object. E.g: 'TRM_ConditionClass__c'
    * @param    developerName 		API name of the Record Type. E.g: 'TRM_ConditionClassMX'
    * 
    * @return   Return a String with the Record Type Id
    */
    @AuraEnabled
    public static String getRecordTypeIdByDeveloperName(String objectType, String developerName){
    	System.debug('#### start TRM_RecordFieldSet_ctr.getRecordTypeIdByDeveloperName()');
        String recordTypeId = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName(objectType, developerName);
        System.debug('#### recordTypeId:' + recordTypeId);
        return recordTypeId;
    }

    /**
    * @description  Receives two parameters with the object API name and the object field API name where the return value must be retrieved from.
    * 
    * @param    objectType 			API name of the Object. E.g: 'TRM_ConditionClass__c'
    * @param    field 		 		API name of the object field. E.g: 'TRM_FieldSetApiName__c'
    * 
    * @return   Return a String with the value of the field indicated in the input parameter
    */
	@AuraEnabled
	public static String getFieldsetNameByField(String objectType, String id, String field) {
		System.debug('#### start TRM_RecordFieldSet_ctr.getFieldsetNameByField()');
		String returnValue;
		sObject record;
		if( id != null || id != '' ){
			record = ISSM_UtilityFactory_cls.getRecordById(objectType, id);
			returnValue = (String) record.get(field);
		}
		System.debug('#### FieldsetName:' + returnValue);
		return returnValue;
	}

	/**
    * @description  Receives two parameters with the object API name and the record Id to get from the server.
    * 
    * @param    objectType 			API name of the Object. E.g: 'MDM_Parameter__c'
    * @param    id 	 		 		Salesforce Standard Id of the Record. E.g: '5tdS4l3sf0rc31d'
    * 
    * @return   Return an instance of sObject with the record retrieved from the server
    */
	@AuraEnabled
	public static sObject getRecordById(String objectType, String id) {
		System.debug('#### start TRM_RecordFieldSet_ctr.getRecordById()');
		sObject returnValue;
		if( id != null || id != ''){
			returnValue = ISSM_UtilityFactory_cls.getRecordById(objectType, id);
		}
		System.debug('#### record:' + returnValue);
		return returnValue;
	}

	public class FieldSetMember {
    
	    public FieldSetMember(Schema.FieldSetMember f) {
	        this.DBRequired = f.DBRequired;
	        this.fieldPath = f.fieldPath;
	        this.label = f.label;
	        this.required = f.required;
	        this.type = '' + f.getType();
	        this.value = '';
	        this.predefinedValue = false;
	        this.hidden = false;
	        this.readOnly = false;
	    }
	    
	    @AuraEnabled
	    public Boolean DBRequired { get;set; }
	    
	    @AuraEnabled
	    public String fieldPath { get;set; }
	    
	    @AuraEnabled
	    public String label { get;set; }
	    
	    @AuraEnabled
	    public Boolean required { get;set; }
	    
	    @AuraEnabled
	    public String type { get; set; }

	    @AuraEnabled
	    public String value { get; set; }

	    @AuraEnabled
	    public Boolean predefinedValue { get;set; }

	    @AuraEnabled
	    public Boolean hidden { get;set; }

	    @AuraEnabled
	    public Boolean readOnly { get;set; }
	}

}