/****************************************************************************************************
    General Information
    -------------------
    author: Andrea Cedillo
    email: acedillo@avanxo.com
    company: Avanxo México
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Indicates when an account been assigned to a visit plan, or when it is not included in a plan.

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       05-07-2017        Andrea Cedillo           Creation Class
****************************************************************************************************/
public class AccountByVisitPlanOperations_cls {
	
	/**
    * Method to update in the account by route object if the account is assigned in any visit plan, count the number of visit plans
    * @params:
      1.-mapAccByVP: map with the account by visit plan that was updated or deleted
      2.-blnAggregate: flag to indicate if the account is added or deleted form a visit plan
    * @return void
    **/
    public static void manageAggregateAccount(map<Id, AccountByVisitPlan__c> mapAccByVP, Boolean blnAggregate){
    	set<Id> setAcct = new set<Id>();
    	set<Id> setRoute = new set<Id>();
        List<AccountByRoute__c> lstRouteUpd = new List<AccountByRoute__c>();
        
        mapAccByVP = new map<Id, AccountByVisitPlan__c>([
        	Select 	Id, Account__c, VisitPlan__c, VisitPlan__r.Route__c
        	From	AccountByVisitPlan__c
        	Where	Id = : mapAccByVP.keySet()
        ]);
        
        for(AccountByVisitPlan__c objAcct: mapAccByVP.values()){
            setAcct.add(objAcct.Account__c);
            setRoute.add(objAcct.VisitPlan__r.Route__c);
        }
        AccountByRoute__c[]  acctRouteQuery = [Select Id, Account__c, Route__c, CountVisitPlans__c FROM AccountByRoute__c Where Account__c = :setAcct and Route__c = :setRoute];
        
        for(AccountByRoute__c objAcctRoute: acctRouteQuery){
           objAcctRoute.CountVisitPlans__c = objAcctRoute.CountVisitPlans__c != null ?
               (blnAggregate ? (objAcctRoute.CountVisitPlans__c + 1) : (objAcctRoute.CountVisitPlans__c - 1) ) 
               : (blnAggregate ? 1 : 0);
           lstRouteUpd.add(objAcctRoute);
           System.debug('OBJ*'+objAcctRoute);
        }
        if(!lstRouteUpd.isEmpty())
        	update lstRouteUpd;
    }
    
    /**
    * Method to delete all visits related with a visit plan, ocurrs when a visit plan is deleted
    * @params:
      1.-lstOldAccByVP: map with the account by visit plan that was deleted
    * @return void
    **/
    public static void deleteVisits(list<AccountByVisitPlan__c> lstOldAccByVP){
    	set<String> setIdsAccounts = new set<String>();
    	set<String> setIdsVisitPlan = new set<String>();
    	
    	for(AccountByVisitPlan__c objAccByVP : lstOldAccByVP){
    		setIdsAccounts.add(objAccByVP.Account__c);
    		setIdsVisitPlan.add(objAccByVP.VisitPlan__c);
    	}
    	System.debug(':::: [AccountByVisitPlanOperations_cls.deleteVisits 1] System cpuTime start: '+Limits.getCpuTime());
    	AccountByVisitPlanOperations_cls.updateSortListBySequence(setIdsAccounts, setIdsVisitPlan);
        System.debug(':::: [AccountByVisitPlanOperations_cls.deleteVisits 2] System cpuTime start: '+Limits.getCpuTime());
    	AccountByRouteOperations_cls.removeVisitsOfVisitPlan(setIdsAccounts, setIdsVisitPlan);
        System.debug(':::: [AccountByVisitPlanOperations_cls.deleteVisits 3] System cpuTime start: '+Limits.getCpuTime());
    }
    
    
    /**
    * Method to validate if an account can coexist or can has the same frecuency or can has the same weekly period in differents routes and 
      service models depending of the config in the custom metadata type
    * @params:
      1.-lstNewAccByVP: list of the new account by visit plan records
    * @return void
    **/
    public static void validateServiceModelConfig(list<AccountByVisitPlan__c> lstNewAccByVP){
    	set<String> setAccountIds = new set<String>();
    	set<String> setVisitPlanIds = new set<String>();
    	
    	for(AccountByVisitPlan__c accByVP :lstNewAccByVP){
    		setAccountIds.add(accByVP.Account__c);
    		setVisitPlanIds.add(accByVP.VisitPlan__c);
    	}

    	map<String, VisitPlan__c> mapVisitPlan = new map<String, VisitPlan__c>([
    		Select	Id, Route__r.ServiceModel__c
    		From	VisitPlan__c
    		Where	Id = :setVisitPlanIds
    	]);
    	
    	//Obtain the metadata type config for all service models
        list<ParametersVisitControl__mdt> lstConf = [
            Select  Id, CanCoexist__c, ServiceModel__c, ServiceModel2__c, SameFrequency__c, SameWeeklyPeriod__c
            From    ParametersVisitControl__mdt                    
        ];
            
        System.debug('\n----lstConf: '+lstConf);
        if(!lstConf.isEmpty()){
        	//Obtain the list of accounnts that are in any other visit plan with a diferent service model 
	    	list<AccountByVisitPlan__c> lstTmpAccByVP = [
	    		Select	Id, Account__c, VisitPlan__c, VisitPlan__r.ExecutionDate__c, WeeklyPeriod__c,
	    				VisitPlan__r.EffectiveDate__c, VisitPlan__r.Route__r.ServiceModel__c, Monday__c, 
                        Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c
	    		From	AccountByVisitPlan__c
	    		Where	Account__c = :setAccountIds And (
	    				VisitPlan__r.ExecutionDate__c >= TODAY Or VisitPlan__r.EffectiveDate__c >= TODAY
	    		)
	    				
	    	];
	    	
	    	//Map to save all AccountByVisitPlan__c records by each account
            map<String, list<AccountByVisitPlan__c>> mapAccService = new map<String, list<AccountByVisitPlan__c>>();
            for(AccountByVisitPlan__c tmpAccByVP : lstTmpAccByVP){
                list<AccountByVisitPlan__c> lstTmp;
                if(mapAccService.containsKey(tmpAccByVP.Account__c))
                    lstTmp = mapAccService.get(tmpAccByVP.Account__c);
                else
                    lstTmp = new list<AccountByVisitPlan__c>();
                    
                lstTmp.add(tmpAccByVP);
                mapAccService.put(tmpAccByVP.Account__c, lstTmp);
            }

            //for each new AccountByVisitPlan__c record
            for(AccountByVisitPlan__c objAccByVP :lstNewAccByVP){
                //validate if the related account have an other visit plan with other service model
                if(mapAccService.containsKey(objAccByVP.Account__c)){
                    //Obtain the related records
                    list<AccountByVisitPlan__c> tmpLstObjAccByVP = mapAccService.get(objAccByVP.Account__c);
                    System.debug('\n----tmpLstObjAccByVP: '+tmpLstObjAccByVP);
                    //for each record, validate if exist a metadatatype with conditions that allow create a record 
                    //maximun four records are processed(one for each service model)
                    for(AccountByVisitPlan__c tmpObjAccByVP : tmpLstObjAccByVP){
                        String serviceModel = tmpObjAccByVP.VisitPlan__r.Route__r.ServiceModel__c;
                        String serviceModel2 = mapVisitPlan.get(objAccByVP.VisitPlan__c).Route__r.ServiceModel__c;
                        System.debug('\n----serviceModel: '+serviceModel);
                        System.debug('\n----serviceModel2: '+serviceModel2);
                        Boolean exito = true;
                        //for each metadatatype, validate that the service models can not coexist or not have the same frecuency
                        //maximun six records are processed
                        for(ParametersVisitControl__mdt conf :lstConf){
                        	System.debug('\n----conf: '+conf);
                        	//if the service models can not coexist or can not have the same frecuency or can not have the same weekly period, 
                        	//the process generate an error
                            if(
                                (
                                	//The service models should be in the metadata types config
                                    (conf.ServiceModel__c == serviceModel && conf.ServiceModel2__c == serviceModel2) ||
                                    (conf.ServiceModel__c == serviceModel2 && conf.ServiceModel2__c == serviceModel)
                                ) && 
                                (
                                	//Can not have coexist
                                    conf.CanCoexist__c == false ||
                                    (conf.SameFrequency__c == false && 
                                        ((objAccByVP.Monday__c==true && tmpObjAccByVP.Monday__c==true) ||   (objAccByVP.Wednesday__c==true && tmpObjAccByVP.Wednesday__c==true) ||
                                            (objAccByVP.Thursday__c==true && tmpObjAccByVP.Thursday__c==true) || (objAccByVP.Friday__c==true && tmpObjAccByVP.Friday__c==true) ||
                                            (objAccByVP.Saturday__c==true && tmpObjAccByVP.Saturday__c==true) ||(objAccByVP.Sunday__c==true && tmpObjAccByVP.Sunday__c==true)
                                        )
                                    ) ||
                                    (
                                    	conf.SameWeeklyPeriod__c == false && objAccByVP.WeeklyPeriod__c == tmpObjAccByVP.WeeklyPeriod__c &&
                                    	(
                                    		(objAccByVP.Monday__c==true && tmpObjAccByVP.Monday__c==true) ||(objAccByVP.Tuesday__c==true && tmpObjAccByVP.Tuesday__c==true) ||
                                            (objAccByVP.Wednesday__c==true && tmpObjAccByVP.Wednesday__c==true) || (objAccByVP.Thursday__c==true && tmpObjAccByVP.Thursday__c==true) ||
                                            (objAccByVP.Friday__c==true && tmpObjAccByVP.Friday__c==true) || (objAccByVP.Saturday__c==true && tmpObjAccByVP.Saturday__c==true) ||
                                            (objAccByVP.Sunday__c==true && tmpObjAccByVP.Sunday__c==true)
                                    	)
                                    )
                                                                   
                                )
                            ){
                                objAccByVP.addError(Label.VPCoexistFrecuencyError.replace('%s1', serviceModel).replace('%s2', serviceModel2));
                                exito = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
    * Method to sort the list according to its sequence
    * @params:
      1.-lstAccByVP: list with the account by visit plan to be sort
    * @return lstAux Auxiliar list sorted by Sequence
    **/
    public static list<AccountByVisitPlan__c> sortListBySequence(list<AccountByVisitPlan__c> lstAccByVP){
    	AccountByVisitPlan__c objAux = new AccountByVisitPlan__c();
    
    	Integer intMaxSecuence = 100000;        
        /*
		  The following lines will be commented to attend the CPU Limit exceeded exception
		*/
    	/*for(Integer i=0; i<lstAccByVP.size(); i++){
    		
    		for(Integer j=i; j<lstAccByVP.size(); j++){
    			
    			if(lstAccByVP[j].Sequence__c < intMaxSecuence){
    				objAux = lstAccByVP[j];
    				lstAccByVP[j] = lstAccByVP[i];
    				lstAccByVP[i] = objAux;
    				 
    				intMaxSecuence = Integer.valueOf(lstAccByVP[j].Sequence__c); 
    			}
    		}
    		
    		//System.debug('\nPos '+i+' ===> '+lstAccByVP[i]);
    		intMaxSecuence = 100000;
    	}*/
        System.debug(':::: [sortListBySequence] System cpuTime end: '+Limits.getCpuTime());
    	//Reassign sequence
    	for(Integer i=0; i<lstAccByVP.size(); i++)	lstAccByVP[i].Sequence__c = i+1;
    		
    	return lstAccByVP;
    }
    
    /**
    * Method to sort and update the list of visits palan according to its sequence
    * @params:
      1.-setIdsAccounts: list with the account Ids
      1.-setIdsVisitPlan: list with the visit plan Ids
    * @return lstAux Auxiliar list sorted by Sequence
    **/
    public static void updateSortListBySequence(set<String> setIdsAccounts, set<String> setIdsVisitPlan){
    		list<VisitPlan__c> lstVisitPlan = [
    		Select  Id, 
                    (
                        Select  Id, Sequence__c, Account__c
                        From    AccountsByVisitPlan__r 
                        Where 	Account__c NOT IN :setIdsAccounts
                        Order By Sequence__c asc
                    )
            From    VisitPlan__c
            Where   Id = :setIdsVisitPlan];
            
            list<AccountByVisitPlan__c> lstAllAccByVp = new list<AccountByVisitPlan__c>();
            for(VisitPlan__c objVP :lstVisitPlan){
            	if(objVP.AccountsByVisitPlan__r != null && !objVP.AccountsByVisitPlan__r.isEmpty())lstAllAccByVp.addAll(AccountByVisitPlanOperations_cls.sortListBySequence(objVP.AccountsByVisitPlan__r));
            }
            
            if(!lstAllAccByVp.isEmpty())
            	update lstAllAccByVp;
    }
    
}