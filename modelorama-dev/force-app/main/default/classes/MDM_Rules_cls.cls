/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ABInBev (MDM Rules)
Description: Strategy class that dynamically creates all rules from all objects
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-Enero-2018   Rodrigo RESENDIZ (RR)   Initial version
2.0		  06-06-2018	  Ivan Neria				Final Version
*/public with sharing class MDM_Rules_cls {
	public class NameException extends Exception{} 
	private MDM_RulesService_cls RuleStrategy;
	private final String ruleCode;
	private final String objName;

	public MDM_Rules_cls(String objectName, String ruleCode){
		objName = objectName;
		objectName = objectName.remove(Label.CDM_Apex_PREFIX2);
		String className = objectName + '_' + ruleCode + '_cls';
		try{
			RuleStrategy = (MDM_RulesService_cls) Type.forName(className).newInstance();
			this.ruleCode = ruleCode;
			}catch(Exception e){
				throw new NameException(className);
			}
	}

	public List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, Set<String> setsGranularity){
		return RuleStrategy.evaluateRule(toEvaluate, this.ruleCode, objName, setsGranularity);
	}

}