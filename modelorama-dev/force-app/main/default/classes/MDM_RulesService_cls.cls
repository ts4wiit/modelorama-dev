/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ABInBev (MDM Rules)
Description: Parent class to create the required validation rules for MDM
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-01-2018   Rodrigo RESENDIZ (RR)   Initial version
2.0		  06-06-2018   Ivan Neria				Final Version
*/

public abstract class MDM_RulesService_cls {
    protected final String[] RULE_CODES;
    public static final String VALIDATE_UPPER_ONLY = '[`~!@#$%^*()_°¬|+=?;:\"<>\\{\\}\\[\\]\\\\\\/]';
    public static final String VALIDATE_ONLY_NUMBERS = '([0-9]+)';
    public static final String VALIDATE_ZWELS = '(?:([BCEFJKT\\s])(?!.*\\1))*';
    public static final String VALIDATE_SALESORG_31 = '(31\\d*)';
    public static final String VALIDATE_RFC_FISICA = '([A-Z]{4})([0-9]{2})(0[1-9]{1}|1[1-2]{1})(((1|2|0)[0-9]{1})|(3[0-1]))([0-9]|[A-Z]{3})';
    public static final String VALIDATE_RFC_MORAL = '([A-Z]{3})([0-9]{2})(0[1-9]{1}|1[1-2]{1})(((1|2|0)[0-9]{1})|(3[0-1]))([0-9]|[A-Z]{3})';
    public static final String VALIDATE_CURP = '[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}';
    public static final Set<String> PERSONA_FISICA_SET = new Set<String>{'PERSONA FISICA.'};
    public static final String BURKS_VALUE = '1003';

    public static final String DESCRIPTION_SUFIX = Label.CDM_Apex_PREFIX3;
    public static final String HAS_ERROR_SUFIX = Label.CDM_Apex_PREFIX1;
    public static final String RULE_ID_FIELD_NAME =Label.CDM_Apex_MDM_RuleId_c;
    
    public final Map<String, MDM_Rule__c> rulesToApply;
    public final Map<String, Schema.SobjectField> accountFieldMap;
    public final List<MDM_Validation_Result__c> validationResults;
        
    public MDM_RulesService_cls(){
        Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe(); 
    	Schema.SObjectType MDMAccountType = sObjectTypeMap.get(Label.CDM_Apex_MDM_Account_c); 
    	accountFieldMap = MDMAccountType.getDescribe().fields.getMap();
    }
    
    public abstract List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity);
    protected Map<String,MDM_FieldByRule__c> uniqueFields (List<MDM_FieldByRule__c> fieldsFromRule){
        Map<String,MDM_FieldByRule__c> result = new Map<String,MDM_FieldByRule__c>();
        for(MDM_FieldByRule__c fRule : fieldsFromRule){
            result.put(fRule.MDM_APIFieldName__c, fRule);
        }
        return result;
    }
    // UTILITY METHODS
    public static Matcher stringMatcher(String toMatch, String regex){
        return getPattern(regex).matcher(toMatch);
    }
    public static Pattern getPattern(String regex){
        return Pattern.compile(regex);
    }
}