/**
 * This class serves to schedule the execution of batch for AllMobileVersionBatchClass.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileVersionSchedulerClass implements Schedulable {

	/**
	 * This method execute the AllMobileVersionBatchClass batch.
	 */
	global void execute(SchedulableContext objSchedulableContext) {
		AllMobileVersionBatchClass objAllMobileVersionBatchClass = new AllMobileVersionBatchClass();
		Database.executeBatch(objAllMobileVersionBatchClass);
	}
}