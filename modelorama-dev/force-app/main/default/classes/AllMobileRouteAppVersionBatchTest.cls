/**
 * Test class for AllMobileRouteAppVersionBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileRouteAppVersionBatchTest {

	/**
	 * Method to setup data.
	 */
	@testSetup
	static void setup() {

		//Create a List of RouteAppVersion.
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion = new List<AllMobileRouteAppVersion__c>();
		for(Integer intI = 0; intI < 9; intI++) {
			lstAllMobileRouteAppVersion.add(AllMobileUtilityHelperTest.createAllMobileRouteAppVersion('FG000' + String.valueOf(intI), '1.0.0', 1, Date.newInstance(2030, 07, 15), Date.newInstance(2098, 10, 20), false));
		}
		insert lstAllMobileRouteAppVersion;
	}

	/**
	 * Method to test AllMobileRouteAppVersionBatchClass.
	 */
	static testMethod void testAllMobileRouteAppVersionBatchClass() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		AllMobileRouteAppVersionBatchClass objAllMobileRouteAppVersionBatchClass = new AllMobileRouteAppVersionBatchClass();
		Id idObjAllMobileRouteAppVersionBatchClass = Database.executeBatch(objAllMobileRouteAppVersionBatchClass);

		//Stop test.
		Test.stopTest();
	}
}