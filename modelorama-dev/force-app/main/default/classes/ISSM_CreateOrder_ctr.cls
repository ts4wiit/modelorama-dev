/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que permite generar el request y response para el servicio de creación de pedidos

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       11-Julio-2017   Luis Licona                  Creación de la Clase
    ================================================================================================
****************************************************************************************************/
global with sharing class ISSM_CreateOrder_ctr {

	public ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls(); 
	public Account objAcc = new Account();
	public createOrderItems[] LstOI {get;set;}
	public orderConditions[] LstOC {get;set;}
	public orderPartners[] LstOP {get;set;}
	public MainOrder ObjMO {get;set;}
	public ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
	public ISSM_DeserializeCreateOrder_cls ObjDeserealized;
	public Integer IntCount = 10;
	public List<ONTAP__Order_Item__c> dummy_Lst{get;set;}

	/**
    * Method from create Request and Response for service CreateOrder
    * @param   StrIdAcc: Id AcSSM_creacount  
    * @return  ISSM_DeserializeCreateOrder_cls from obtain response from service
    **/
	public ISSM_DeserializeCreateOrder_cls CallOutCreateOrder(String IdOrder,String IdAcc,ONTAP__Order_Item__c[] LstOItem,
															  ISSM_NewOrder_ctr.PriceCondition[] LstAllPC,String StrPMethod,
															  String InvDoct,EmptyBalanceB__c[] LstEB,String StrPONum,
															  Map<Integer,String> MAPZTPM, Map<String,Integer> combo_map,
															  Integer counter_Int){

		dummy_Lst = new List<ONTAP__Order_Item__c>();

		System.debug('**********************EMTY BALANCE --> '+LstEB);
		ONTAP__Order__c CurrentOrd = new ONTAP__Order__c();
        CurrentOrd = CTRSOQL.getOrderbyId(IdOrder);
		//HACER CONSULTA AQUI SI ES POR TELEVENTAUNIVERSAL
		ONCALL__Call__c[] LstCall = CTRSOQL.getDateTime(IdAcc);
		System.debug('LstCall--> '+LstCall);

		System.debug('LstAllPC = = = = = ' + LstAllPC);
	
		//Datetime DtDate  = CurrentOrd.ONCALL__Call__r.ISSM_CallUT__c ? Datetime.parse(LstCall[0].ONCALL__Date__c.format())+1 : Datetime.parse(System.now().format())+1;
		Datetime DtDate  = Datetime.parse(System.now().format())+1;
		Datetime DtDateP = Datetime.parse(System.now().format());
		String StrReqDat = DtDate.format(Label.ISSM_FormatDate);
		String StrPurDat = DtDateP.format(Label.ISSM_FormatDate);
		String StrPurNam;
		LstOI = new List<createOrderItems>();
		LstOC = new List<orderConditions>();
		LstOP = new List<orderPartners>();
		ObjMO = new MainOrder();
		LstAllPC = LstAllPC != null ? LstAllPC : new List<ISSM_NewOrder_ctr.PriceCondition>();
		LstEB = LstEB != null ? LstEB : new List<EmptyBalanceB__c>();
		String StrSerializeJson='';
		System.debug('MAPZTPM---> '+MAPZTPM);
		System.debug('LstEBLstEB---> '+LstEB);
		try{
			objAcc 		= CTRSOQL.getAccountbyId(IdAcc);
			LstOP.add(new orderPartners(Label.ISSM_PartnerRole,objAcc.ONCALL__Ship_To_Name__c,'0000'));	

			if(InvDoct != Label.ISSM_DocTypeEB){//Y064

				String ZTPMCond = Label.ISSM_X;
				InvDoct = objAcc.ISSM_Invoicingdocument__c;
				if(combo_map.isEmpty()){
					//System.debug(loggingLevel.Error, '*** LstOItem: ' + LstOItem);
					for(ONTAP__Order_Item__c objOI : LstOItem){
						if(InvDoct == Label.ISSM_DocTypeOrder || InvDoct == Label.ISSM_Y029)
							ZTPMCond = MAPZTPM.containsKey(Integer.valueOf(objOI.ONCALL__SAP_Order_Item_Number__c)) ? '': Label.ISSM_X;
						
						LstOI.add(new createOrderItems(Integer.valueOf(objOI.ONCALL__OnCall_Quantity__c),
														ZTPMCond,
													   	objOI.ISSM_Uint_Measure_Code__c,
													   	objOI.ONCALL__SAP_Order_Item_Number__c,
													   	objOI.ISSM_ItemPosition__c,
													   	//String.valueOf(IntCount),
													   	objOI.ISSM_ComboNumber__c));	
					}
				//New Order WS
				}else{

					ZTPMCond = Label.ISSM_X;
					InvDoct = objAcc.ISSM_Invoicingdocument__c;

					Map<String, ISSM_DummyProduct__c> dummyProduct_map = ISSM_DummyProduct__c.getAll();
        			ISSM_DummyProduct__c dummyProduct_Obj = dummyProduct_map.get(Label.ISSM_WSConfDummy);

					Map<String,List<createOrderItems>> prodsByCombo_map = new Map<String,List<createOrderItems>>();

					Integer pos_Int = 0;

					Map<String, Integer> comboPos_map = new Map<String, Integer>();
					for(String comboId_Str : combo_map.keySet()){
						List<createOrderItems> prodQty_lst = new List<createOrderItems>();
						System.debug(loggingLevel.Error, '*** LstOItem: ' + LstOItem);
						System.debug(loggingLevel.Error, '*** comboId_Str: ' + comboId_Str);
						for(ONTAP__Order_Item__c objOI : LstOItem){
							if(objOI.ISSM_ComboNumber__c == comboId_Str){
								
								if(Integer.valueOf(objOI.ISSM_ItemPosition__c) >= pos_Int){
									pos_Int = Integer.valueOf(objOI.ISSM_ItemPosition__c);
								}
								comboPos_map.put(comboId_Str, pos_Int);

								prodQty_lst.add(new createOrderItems(Integer.valueOf(objOI.ONCALL__OnCall_Quantity__c),
																	ZTPMCond,
																   	objOI.ISSM_Uint_Measure_Code__c,
																   	objOI.ONCALL__SAP_Order_Item_Number__c,
																   	objOI.ISSM_ItemPosition__c,
																   	objOI.ISSM_ComboNumber__c));	
							}
						}	
						prodsByCombo_map.put(comboId_Str,prodQty_lst);
					}
					
					System.debug('prodsByCombo_map = -->' + prodsByCombo_map);
					

					List<createOrderItems> prodsCombo_lst = new List<createOrderItems>();

					for(String comboId_Str : prodsByCombo_map.keySet()){
						prodsCombo_lst.add(new createOrderItems(combo_map.get(comboId_Str),
														ZTPMCond,
													   	dummyProduct_Obj.ISSM_UnitMeasure__c,
													   	dummyProduct_Obj.ISSM_SKU__c,
													   	String.valueOf(IntCount),
													   	comboId_Str));

						ONTAP__Order_Item__c objOI = new ONTAP__Order_Item__c();
						objOI.ONCALL__OnCall_Quantity__c       = combo_map.get(comboId_Str);
						objOI.ISSM_Uint_Measure_Code__c        = dummyProduct_Obj.ISSM_UnitMeasure__c;
						objOI.ONCALL__SAP_Order_Item_Number__c = dummyProduct_Obj.ISSM_SKU__c;
						objOI.ISSM_ItemPosition__c             = String.valueOf(IntCount);
						objOI.ISSM_ComboNumber__c              = comboId_Str;
						objOI.ISSM_OrderItemSKU__c             = dummyProduct_Obj.ISSM_SKU__c;
						
						if(counter_Int == 1){
							dummy_Lst.add(objOI);	
						}						

						prodsCombo_lst.addAll(prodsByCombo_map.get(comboId_Str));
						System.debug(loggingLevel.Error, '*** comboPos_map: ' + comboPos_map);
						System.debug(loggingLevel.Error, '*** comboId_Str: ' + comboId_Str);
						IntCount = comboPos_map.get(comboId_Str) + 10;
					}

					System.debug('dummy_Lst = ' + dummy_Lst);

					LstOI.addAll(prodsCombo_lst);

					Id orderId;
					for(ONTAP__Order_Item__c objOI : LstOItem){
						if(objOI.ISSM_ComboNumber__c == ''){
							LstOI.add(new createOrderItems(Integer.valueOf(objOI.ONCALL__OnCall_Quantity__c),
														ZTPMCond,
													   	objOI.ISSM_Uint_Measure_Code__c,
													   	objOI.ONCALL__SAP_Order_Item_Number__c,
													   	objOI.ISSM_ItemPosition__c,
													   	objOI.ISSM_ComboNumber__c));
						}
						orderId = objOI.ONTAP__CustomerOrder__c;
					}

					for(ONTAP__Order_Item__c objOI : dummy_Lst){
						objOI.ONTAP__CustomerOrder__c = orderId;
					}

					//setdummy_Lst(dummy_Lst);
				}					
			}else{//YD53
				InvDoct = Label.ISSM_DocTypeEB;
				IntCount=10;
				for(EmptyBalanceB__c objEB : LstEB){
					LstOI.add(new createOrderItems(Integer.valueOf(objEB.BoxesNumReturn__c),
													Label.ISSM_X,'',
												   	objEB.Material_Number__c,
												   	String.valueOf(IntCount),
												   	''));
					IntCount+=10;
				}
			}
			
			if(!LstOI.isEmpty()){
				String StrOrg = InvDoct == Label.ISSM_Y013 || InvDoct == Label.ISSM_Y023 ? Label.ISSM_ModChaOrg : objAcc.ONTAP__SalesOgId__c;
				String Channel= InvDoct == Label.ISSM_Y013 || InvDoct == Label.ISSM_Y023 ? Label.ISSM_ModChanel : objAcc.ONTAP__ChannelId__c;
				for(ISSM_NewOrder_ctr.PriceCondition objPC : LstAllPC)
					LstOC.add(new orderConditions(objPC.conditionType,objPC.amount,objPC.itemNumber));
					
					String route_Str = CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c != null ? CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c : objAcc.ONCALL__OnCall_Route_Code__c; //Validates that the route is created and has a value, if not colocates the sales route of the account

					ObjMO = new MainOrder(new createOrder(StrOrg,
												          objAcc.ONTAP__SalesOffId__c,StrReqDat,
					  							          StrPONum, 
					  							          StrPurDat,Label.ISSM_Origin,LstOP,LstOC,
					  							          //objAcc.ONCALL__OnCall_Route_Code__c,
					  							          //CurrentOrd.ONCALL__Call__r.CallList__r.Route__r.ONTAP__RouteId__c,
					  							          route_Str,
					  							          InvDoct,StrPMethod,Label.ISSM_Division,
					  							          Channel,LstOI));
			}
				
			StrSerializeJson = JSON.serializePretty(ObjMO);
			System.debug('StrSerializeJsonCO----->'+StrSerializeJson);
			//List<ISSM_CallOutService_ctr.WprResponseService> response = CTR.SendRequest(StrSerializeJson,Label.ISSM_ConfigCreateOrder);
			if(!Test.isRunningTest()) {
				List<ISSM_CallOutService_ctr.WprResponseService> response = CTR.SendRequest(StrSerializeJson,Label.ISSM_WSConfDummyInfo);
				System.debug('##### responseCO : '+response);
				ObjDeserealized = ISSM_DeserializeCreateOrder_cls.parse(response[0].strBodyService);
				ObjDeserealized.StrRequest 	= StrSerializeJson;
				ObjDeserealized.ReqDate 	= DtDate;
			}
		}catch(Exception ex){
			ex.setMessage(Label.ISSM_Error01+' '+ex.getMessage()+' | '+StrSerializeJson);
			throw ex;
			//System.debug('EXCEPCIONCO--->'+ex.getMessage()+ex.getCause()+ex.getLineNumber());
		}
		
		return ObjDeserealized;
	}


	//WRAPPERS FROM SERIALIZE JSON TO REQUEST
	global class MainOrder {	 
		global createOrder createOrder;
		
		public MainOrder(createOrder createOrder){
			this.createOrder=createOrder;
		}
		public MainOrder(){
			createOrder=null;
		}
	}

	global class createOrder {
		global String salesOrganization;
		global String salesOff;
		global String requiredDate; 
		global String purchaseNumber; 
		global String purchaseDate;  
		global String orderReason;  
		global OrderPartners[] orderPartners;
		global OrderConditions[] orderConditions;
		global String name; 
		global String documentationType; 
		global String DLVSCHDUSE; 
		global String division; 
		global String distributionChannel;
		global CreateOrderItems[] createOrderItems;

		public createOrder(String salesOrganization, String salesOff, String requiredDate, String purchaseNumber,
						   String purchaseDate, String orderReason, OrderPartners[] orderPartners, 
						   OrderConditions[] orderConditions,String name, String documentationType, String DLVSCHDUSE,
						   String division, String distributionChannel, CreateOrderItems[] createOrderItems){

			this.salesOrganization  = salesOrganization;
			this.salesOff			= salesOff;
			this.requiredDate		= requiredDate;
			this.purchaseNumber		= purchaseNumber;
			this.purchaseDate		= purchaseDate;
			this.orderReason		= orderReason;
			this.orderPartners 		= orderPartners;
			this.orderConditions 	= orderConditions;
			this.name				= name;
			this.documentationType	= documentationType;
			this.DLVSCHDUSE			= DLVSCHDUSE;
			this.division			= division;
			this.distributionChannel= distributionChannel;
			this.createOrderItems 	= createOrderItems;

		}
	}

	global class orderPartners {
		global String partnerRole;
		global String partnerNumber;
		global String internalControl;

		public orderPartners(String partnerRole, String partnerNumber, String internalControl){
			this.partnerRole 	 = partnerRole;
			this.partnerNumber	 = partnerNumber;
			this.internalControl = internalControl;
		}
	}

	global class orderConditions {
		global String conditionType;
		global Decimal amount;
		global Integer itemNumber;

		public orderConditions(String conditionType,Decimal amount,Integer itemNumber){
			this.conditionType=conditionType;
			this.amount=amount;
			this.itemNumber=itemNumber;
		}
	}
	
	global class createOrderItems {
		global Integer targetQuantity;
		global String sinPromocion;
		global String salesUnit;
		global String productId;
		global String itemNumber;
		global String idCombo;

		global createOrderItems(Integer targetQuantity, 
								String sinPromocion, 
								String salesUnit, 
								String productId,
								String itemNumber,
								String idCombo){
			
			this.targetQuantity	= targetQuantity;
			this.sinPromocion	= sinPromocion;
			this.salesUnit		= salesUnit;
			this.productId		= productId;
			this.itemNumber		= itemNumber;
			this.idCombo        = idCombo;
		}
	}
}