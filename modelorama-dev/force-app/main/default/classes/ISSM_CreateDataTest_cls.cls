/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public with sharing class ISSM_CreateDataTest_cls { 
    //Creamos una cuenta para pasar el ID al contacto   
    public static Account fn_CreateAccount(Boolean blnCriar,String RecordTyp,String NameAcc  ){
        
        Account ObjAccount = new Account (
            RecordTypeId = RecordTyp, 
            Name = NameAcc);
        
        if(blnCriar){
            insert ObjAccount;
        }
        return ObjAccount;
    }
    
    public static Account fn_CreateAccount(Boolean blnCriar,String RecordTyp,String NameAcc, Account salesOffice  ){
        
        Account ObjAccount = new Account (
            RecordTypeId = RecordTyp, 
            Name = NameAcc,
            ISSM_SalesOffice__c = salesOffice.Id);      
        
        if(blnCriar){
            insert ObjAccount;
        }
        return ObjAccount;
    }
    
    public static Account fn_CreateAccountSalesOffice(Boolean blnCriar, String RecordTyp, String NameAcc, Id billingManager, Id tradeMarketing) {
        
        Account ObjAccount = new Account (
            RecordTypeId = RecordTyp,
            Name = NameAcc,
            ISSM_BillingManager__c = billingManager,
            ISSM_TradeMarketing__c = tradeMarketing);
        if (blnCriar) {
            insert ObjAccount;
        }
        return ObjAccount;
    }
    
    public static Account fn_CreateAccountSalesOrg(Boolean blnCriar, String RecordTyp, String NameAcc, Id billingManager, Id tradeMarketing) {
        
        Account ObjAccount = new Account (
            RecordTypeId = RecordTyp,
            Name = NameAcc,
            ISSM_BillingManager__c = billingManager,
            ISSM_TradeMarketing__c = tradeMarketing);
        if (blnCriar) {
            insert ObjAccount;
        }
        return ObjAccount;
    }
    
    public static Account fn_CreateAccountDRV(Boolean blnCriar, String RecordTyp, String NameAcc, Id billingManager, Id tradeMarketing) {
        
        Account ObjAccount = new Account (
            RecordTypeId = RecordTyp,
            Name = NameAcc,
            ISSM_BillingManager__c = billingManager,
            ISSM_TradeMarketing__c = tradeMarketing);
        if (blnCriar) {
            insert ObjAccount;
        }
        return ObjAccount;
    }
    
    //Creamos un caso para pasar el ID al contacto   
    public static Case fn_CreateCase(Boolean blnCriar, String strDescription, String strSubject, String strStatus, String strIdCaseFroce, Integer intStatusSerie) {
        Case ObjCase = new Case (
            Description = strDescription,
            Subject =  strSubject,
            Status = strStatus,
            ISSM_CaseForceNumber__c = strIdCaseFroce,
            ISSM_StatusSerialNumber__c = intStatusSerie);      
        
        if(blnCriar){
            insert ObjCase;
        }
        return ObjCase;
    }
    
    //Creamos un caso para pasar el ID al contacto   
    public static Case fn_CreateCase(Boolean blnCriar, String strDescription, String strSubject, String strStatus, String strIdCaseFroce) {
        Case ObjCase = new Case (
            Description = strDescription,
            Subject =  strSubject,
            Status = strStatus,
            ISSM_CaseForceNumber__c = strIdCaseFroce);      
        
        if(blnCriar){
            insert ObjCase;
        }
        return ObjCase;
    }
    
    //Actualizamod un caso para pasar el ID al contacto   
    public static Case fn_UpdateCase(Boolean blnCriar, String strDescription, String strSubject,String strStatus,String IdCaso ){
        ISSM_TriggerManager_cls.Activate();
        Case ObjCaseUpdate = new Case (
            id = IdCaso,
            Description = strDescription,
            Subject =  strSubject, 
            Status = strStatus
        );      
        if(blnCriar){
            update ObjCaseUpdate;
        }
        return ObjCaseUpdate;
    }
    //Creamos un caso para pasar el ID al contacto   
    public static ISSM_TypificationMatrix__c fn_CreateTypificationMatrix(Boolean blnCriar,String identificador,String level1,String level2,String level3,String level4,String level5,String level6  ){
        
        ISSM_TypificationMatrix__c ObjTypificationMatrix = new ISSM_TypificationMatrix__c (
            ISSM_UniqueIdentifier__c = identificador ,
            ISSM_Active__c = true,
            ISSM_CaseRecordType__c = 'ISSM_MaintenanceRefrigeration',
            ISSM_TypificationLevel1__c = level1,
            ISSM_TypificationLevel2__c = level2,
            ISSM_TypificationLevel3__c = level3,
            ISSM_TypificationLevel4__c = level4,
            ISSM_TypificationLevel5__c = level5,
            ISSM_TypificationLevel6__c = level6,
            ISSM_SLAProvider__c = true, 
            ISSM_Priority__c = 'Non priority' );      
        
        if(blnCriar){
            insert ObjTypificationMatrix;
        }
        return ObjTypificationMatrix;
    }
    
    public static ISSM_TypificationMatrix__c fn_CreateTypificationMatrix(Boolean blnCriar,String identificador,String level1,String level2,String level3,String level4,String level5,String level6, String assignedTo, String idUser, String idQueue  ){
        
        ISSM_TypificationMatrix__c ObjTypificationMatrix = new ISSM_TypificationMatrix__c (
            ISSM_UniqueIdentifier__c = identificador ,
            ISSM_Active__c = true,
            ISSM_CaseRecordType__c = 'ISSM_MaintenanceRefrigeration',
            ISSM_TypificationLevel1__c = level1,
            ISSM_TypificationLevel2__c = level2,
            ISSM_TypificationLevel3__c = level3,
            ISSM_TypificationLevel4__c = level4,
            ISSM_TypificationLevel5__c = level5,
            ISSM_TypificationLevel6__c = level6,
            ISSM_AssignedTo__c = assignedTo,
            ISSM_OwnerUser__c = idUser,
            ISSM_IdQueue__c = idQueue,
            ISSM_Countries_ABInBev__c = 'México',
            ISSM_SLAProvider__c = true, 
            ISSM_OwnerQueue__c='ISSM_WithoutOwner');      
        
        if(blnCriar){
            insert ObjTypificationMatrix;
        }
        return ObjTypificationMatrix;
    }
    
    public static ISSM_TypificationMatrix__c fn_CreateTypificationMatrix2(Boolean blnCriar,String identificador,String level1,String level2,String level3,String level4,String level5,String level6, String assignedTo, String idUser, String idQueue  ){
        
        ISSM_TypificationMatrix__c ObjTypificationMatrix = new ISSM_TypificationMatrix__c (
            ISSM_UniqueIdentifier__c = identificador ,
            ISSM_Active__c = true,
            ISSM_CaseRecordType__c = 'ISSM_MaintenanceRefrigeration',
            ISSM_TypificationLevel1__c = level1,
            ISSM_TypificationLevel2__c = level2,
            ISSM_TypificationLevel3__c = level3,
            ISSM_TypificationLevel4__c = level4,
            ISSM_TypificationLevel5__c = level5,
            ISSM_TypificationLevel6__c = level6,
            ISSM_AssignedTo__c = assignedTo,
            ISSM_OwnerUser__c = idUser,
            ISSM_IdQueue__c = idQueue,
            ISSM_SLAProvider__c = false, 
            ISSM_OwnerQueue__c='ISSM_WithoutOwner');      
        
        if(blnCriar){
            insert ObjTypificationMatrix;
        }
        return ObjTypificationMatrix;
    }
    public static User fn_CreateAdminUser(Boolean blnCriar){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User usr = new User(LastName = 'LIVESTON', FirstName='JASON', Alias = 'jliv', Email = 'abcd@sfmx.com', Username = 'abcd@sfmx.com', ProfileId = profileId.id, TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8', LocaleSidKey = 'en_US', Country = 'MX');
        if (blnCriar) { insert usr; } return usr;
    }
    
    public static User fn_CreateAdminUser(Boolean blnCriar, String userName){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'lortega@avanxo.com',
                            Username = userName,
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            //CommunityNickname = 'test',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        if(blnCriar){
            insert usr;
        }
        return usr;
    }
    
    public static AccountTeamMember fn_CreateAccountTeamMember(Boolean blnCriar, Id accountId, String teamRole, Id userId){
        AccountTeamMember accountTeamMember = new AccountTeamMember(
            AccountId = accountId,
            TeamMemberRole = teamRole,
            UserId = userId,
            AccountAccessLevel='Edit',
            CaseAccessLevel='None',
            ContactAccessLevel='Edit',
            OpportunityAccessLevel='None'
        );
        if(blnCriar){
            insert accountTeamMember;
        }
        return accountTeamMember;
    }
    
    public static QueueSObject fn_CreateQueue(Boolean blnCriar, String name){
        //Creating a Queue
        Group g = new Group(Type='Queue', Name=name);
        insert g;
        QueueSObject q;
        System.runAs(new User(Id=UserInfo.getUserId())){   
            q = new QueueSObject(SobjectType='Case', QueueId=g.Id);
            if(blnCriar){
                insert q;
            }
        }
        return q;
    }
    
    //Crea Caso en CASE FORCE 
    public static ONTAP__Case_Force__c fn_CreateCaseForce(Boolean blnCriar, String strDescription, String strSubject, String strStatus) {
        ONTAP__Case_Force__c objCaseForce = new ONTAP__Case_Force__c(
            ONTAP__Description__c = strDescription,
            ONTAP__Subject__c = strSubject,
            ONTAP__Status__c = strStatus
        );
        if(blnCriar){ 
            insert objCaseForce;
        }
        return objCaseForce;
    }
    
    //Crea Registro en la configuracion personalizada 
    public static ISSM_MappingFieldCase__c fn_CreateCaseForceMapping(Boolean blnCriar, String strName, String strAPICaseForce){
        ISSM_MappingFieldCase__c objMappingFieldCase = new ISSM_MappingFieldCase__c(
            Name = strName,
            ISSM_APICaseForce__c = strAPICaseForce
        ); 
        if(blnCriar){
            insert objMappingFieldCase;
        } 
        return objMappingFieldCase; 
    }
    
    
    //Crea CASECOMMENT 
    public static CaseComment fn_CreateCaseComment(Boolean blnCriar, String strCommentBody, String strParentId){
        CaseComment objCaseComment = new CaseComment(
            CommentBody = strCommentBody,
            ParentId = strParentId
        ); 
        if(blnCriar){
            insert objCaseComment;
        } 
        return objCaseComment; 
    }
    //Crea CASE_FORCE_COMMENT 
    public static ONTAP__Case_Force_Comment__c fn_CreateCaseForceComment(Boolean blnCriar, String strCaseForceCommentId, String strCommentCaseForce){
        ONTAP__Case_Force_Comment__c objCaseForceComment = new ONTAP__Case_Force_Comment__c(
            ONTAP__Case_Force__c = strCaseForceCommentId,
            ONTAP__Comment__c = strCommentCaseForce
        ); 
        if(blnCriar){
            insert objCaseForceComment;
        } 
        return objCaseForceComment; 
    }
    
    //Creamos un caso para pasar el ID al contacto   
    public static Case fn_CreateCaseToolManager(Boolean blnCriar, String strDescription, String strSubject,String strStatus,boolean blnUpdateAccountTeam,boolean blnIsEscalated,String strTypificationLevel1 ){
        
        Case ObjCase = new Case ( 
            Description = strDescription,
            ISSM_TypificationLevel1__c = strTypificationLevel1, 
            Subject =  strSubject,
            Status = strStatus,
            Origin = 'Email',
            ISSM_UpdateAccountTeam__c = blnUpdateAccountTeam,
            IsEscalated =  blnIsEscalated);      
        
        if(blnCriar){
            insert ObjCase;
        }
        return ObjCase;
    }
    //Creamos un caso ISSM_AppSetting_cs__c  
    public static List<ISSM_AppSetting_cs__c> fn_CreateAppSetting(Boolean blnCriar,String IdQueueWithoutOwner,String IdQueueFriendModel ){
        List<ISSM_AppSetting_cs__c> lstAppSetting = new List<ISSM_AppSetting_cs__c>();
        ISSM_AppSetting_cs__c ObjAppSetting= new ISSM_AppSetting_cs__c ( 
            ISSM_IdQueueWithoutOwner__c=IdQueueWithoutOwner,
            ISSM_IdQueueFriendModel__c=IdQueueFriendModel);      
        
        if(blnCriar){
            insert ObjAppSetting;
            lstAppSetting.add(ObjAppSetting);
        }
        return lstAppSetting;
    }  
    //Creamos un caso ISSM_AppSetting_cs__c  
    public static List<ISSM_AppSetting_cs__c> fn_CreateAppSettingTelecollection(Boolean blnCriar,String IdQueueWithoutOwner,String IdQueueFriendModel,String QueueTelecollection){
        List<ISSM_AppSetting_cs__c> lstAppSetting = new List<ISSM_AppSetting_cs__c>();
        ISSM_AppSetting_cs__c ObjAppSetting= new ISSM_AppSetting_cs__c(ISSM_IdQueueWithoutOwner__c=IdQueueWithoutOwner, ISSM_IdQueueTelecolletion__c=QueueTelecollection, ISSM_IdQueueFriendModel__c=IdQueueFriendModel);
        if (blnCriar) { insert ObjAppSetting; lstAppSetting.add(ObjAppSetting); } return lstAppSetting;
    }  
    
    //Creamos un caso fn_CreateAppSettingProviderWit  
    public static List<ISSM_AppSetting_cs__c> fn_CreateAppSettingProviderWit(Boolean blnCriar,String IdProviderWithOutMail){
        List<ISSM_AppSetting_cs__c> lstAppSetting = new List<ISSM_AppSetting_cs__c>();
        ISSM_AppSetting_cs__c ObjAppSetting= new ISSM_AppSetting_cs__c ( 
            ISSM_IdProviderWithOutMail__c=IdProviderWithOutMail);      
        
        if(blnCriar){
            insert ObjAppSetting;
            lstAppSetting.add(ObjAppSetting);
        }
        return lstAppSetting;
    }   
    //Creamos un caso fn_CreateAppSettingProviderWit  
    public static List<Account> fn_CreateAccountProvider(Boolean blnCriar,String RecordTypeId,String SalesOffice){
        List<Account> lstAccount = new List<Account>();
        Account objAccountProvider = new Account ( 
            Name = 'Name  Provider',
            ONTAP__Email__c = 'hdiaz@avanxo.com',
            ISSM_ProviderClassification__c = 'Barril',
            ISSM_Brand__c = 'Tanque CO2',
            RecordTypeId = RecordTypeId, 
            ISSM_SalesOffice__c  = SalesOffice );      
        
        if(blnCriar){
            insert objAccountProvider;
            lstAccount.add(objAccountProvider);
        }
        return lstAccount;
    }  
    
    
    //Creamos un caso fn_CreateAppSettingProviderWit  
    public static List<ISSM_ValidateProspecSalesOffice__c> fn_CreatetProspecSalesOffice(Boolean blnCriar,String strProfileName){
        List<ISSM_ValidateProspecSalesOffice__c> lstProspecSalesOffice = new List<ISSM_ValidateProspecSalesOffice__c>();
        ISSM_ValidateProspecSalesOffice__c ObjValidateProspec = new ISSM_ValidateProspecSalesOffice__c ( 
            
            Name = strProfileName);      
        
        if(blnCriar){
            insert ObjValidateProspec;
            lstProspecSalesOffice.add(ObjValidateProspec);
        }
        return lstProspecSalesOffice;
    }   
    
    
    //Creamos el llenado  fn_CreateAppSetting para los campos mencionados
    public static List<ISSM_AppSetting_cs__c> fn_CreateAppSettingTypification(Boolean blnCriar,String IdPaymentNotRefelcted,String IdUnrecognizedCharge,String IdPaymentPlan,String IdQueueWithoutOwner){
        List<ISSM_AppSetting_cs__c> lstAppSetting = new List<ISSM_AppSetting_cs__c>();
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_Telecolletion') order by developername];
        for(Group objgroupId : lstselectGroup) { mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id); }
        ISSM_AppSetting_cs__c ObjAppSetting= new ISSM_AppSetting_cs__c(ISSM_IdPaymentNotRefelcted__c = IdPaymentNotRefelcted, ISSM_IdUnrecognizedCharge__c = IdUnrecognizedCharge, ISSM_IdPaymentPlan__c =  IdPaymentPlan, ISSM_IdQueueWithoutOwner__c = IdQueueWithoutOwner, ISSM_IdQueueTelecolletion__c = mapIdQueue.get('ISSM_Telecolletion'));      
        if (blnCriar) { insert ObjAppSetting; lstAppSetting.add(ObjAppSetting); } return lstAppSetting;
    }   
    
    public static Id accountRecordTypeId() {
        try { return Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
            } catch(NullPointerException e) { return Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId(); }
    }
    //Creamos configuracion para Trigger Factory  
    public static List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory(Boolean blnCriar,String NameObject ,String NameClassHandler ){
        List<ISSM_TriggerFactory__c> lstTriggerFactory  = new List<ISSM_TriggerFactory__c>();
        ISSM_TriggerFactory__c ObjTriggerFactory = new ISSM_TriggerFactory__c ( 
            Name = NameObject,
            ISSM_TriggerHandler__c = NameClassHandler);      
        
        if(blnCriar){
            insert ObjTriggerFactory;
            lstTriggerFactory.add(ObjTriggerFactory);
        }
        return lstTriggerFactory;
    }  
    //Creamos configuracion para ISSM_IntervalsByCountry__c
    public static List<ISSM_IntervalsByCountry__c> fn_CreateIntervalsByCountry(Boolean blnCriar,String NameCong,String Country,String minInterval, String maxInterval){
        List<ISSM_IntervalsByCountry__c> lstIntervalsByCountry  = new List<ISSM_IntervalsByCountry__c>();
        ISSM_IntervalsByCountry__c ObjIntervalsByCountry = new ISSM_IntervalsByCountry__c ( 
            Name = NameCong,
            ISSM_Country__c = Country,
            ISSM_minInterval__c = minInterval,
            ISSM_maxInterval__c =maxInterval);      
        
        if(blnCriar){
            insert ObjIntervalsByCountry;
            lstIntervalsByCountry.add(ObjIntervalsByCountry);
        }
        return lstIntervalsByCountry;
    }  
    
    //Creamos configuracion ISS_DocumentTypeOpenItems__c
    public static List<ISS_DocumentTypeOpenItems__c> fn_CreateDocumentTypeOpenItems(Boolean blnCriar,String NameCong){
        List<ISS_DocumentTypeOpenItems__c> lsteDocumentTypeOpenItems  = new List<ISS_DocumentTypeOpenItems__c>();
        ISS_DocumentTypeOpenItems__c ObjeDocumentTypeOpenItems = new ISS_DocumentTypeOpenItems__c ( 
            Name = NameCong,
            ISSM_Active__c = true);      
        
        if(blnCriar){
            insert ObjeDocumentTypeOpenItems;
            lsteDocumentTypeOpenItems.add(ObjeDocumentTypeOpenItems);
        }
        return lsteDocumentTypeOpenItems;
    }  
    //Creamos ISSM_FieldsCloneCase__c  Custom Setting 
    public static List<ISSM_FieldsCloneCase__c> fn_CreateFieldsCloneCase(Boolean blnCriar,String strNameField){
        List<ISSM_FieldsCloneCase__c> lstCloneCaseFields = new List<ISSM_FieldsCloneCase__c>();
        ISSM_FieldsCloneCase__c ObjFieldsCloneCase = new ISSM_FieldsCloneCase__c ( 
            Name = strNameField,
            ISSM_Active__c=true);      
        
        if(blnCriar){ 
            insert ObjFieldsCloneCase;
            lstCloneCaseFields.add(ObjFieldsCloneCase);
        }
        return lstCloneCaseFields;
    }
}