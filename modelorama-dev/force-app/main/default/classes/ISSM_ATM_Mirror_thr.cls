/**************************************************************************************
Nombre del Trigger: ISSM_ATM_Mirror_thr
Versión : 1.0
Fecha de Creación : 06 Abril 2018
Funcionalidad : Clase helper del trigger ISSM_ATM_Mirror_tgr
Clase de Prueba: ISSM_ATM_Mirror_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        06 - Abril - 2018      Versión Original
* Leopoldo Ortega        27 - Agosto - 2018     Automatic update of managers
* Leopoldo Ortega        10 - Octubre - 2018    Wait to finish the apex batch to add user to account team member in sales office
*************************************************************************************/
public class ISSM_ATM_Mirror_thr {
    
    public static void altaUsuarioATM(List<ISSM_ATM_Mirror__c> reg_lst) {
        // Instanciamos la clase para realizar las consultas necesarias
        ISSM_CSAssignATMQuerys_cls objCSQuerys = 
            new ISSM_CSAssignATMQuerys_cls();

        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = 
            new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATM_MirrorRol_lst = 
            new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATM_Mirror_Upd_lst = 
            new List<ISSM_ATM_Mirror__c>();
        List<AccountTeamMember> ATM_lst = 
            new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_New_lst = 
            new List<AccountTeamMember>();
        List<ISSM_ATM_Queue__c> ATM_NewQueue_lst = 
            new List<ISSM_ATM_Queue__c>();
        
        Set<String> OficinaVentasIds_set = 
            new Set<String>();
        Set<String> UsuariosIds_set = 
            new Set<String>();
        
        Boolean executeBatch_bln = false;
        Boolean buscaUsuarioRepetido = false;
        
        String soAlta_str = '';
        String orgVAlta_str = '';
        String rolAlta_str = '';
        String managerAlta_str = '';
        
        try {
            for (ISSM_ATM_Mirror__c reg : reg_lst) {
                if (reg.Estatus__c == 'Activo') {
                    
                    // Valida que no se esté ejecutando 9 batch al mismo tiempo
                    List<AsyncApexJob> apexJob = new List<AsyncApexJob>();
                    apexJob = [SELECT Id, ApexClass.Name, Status
                               FROM AsyncApexJob WHERE JobType = 'BatchApex' AND ApexClass.Name = 'ISSM_AssignUserToATMInAccount_bch' AND Status = 'Processing'];
                    
                    if (apexJob.size() == 9) { reg.AddError(System.label.ISSM_WaitProcessBatch); } else {
                        
                        // Inicia lógica para asignación automática de gestores ************************************************************************************************
                        
                        // Buscamos la organización de ventas de la oficina de ventas
                        List<Account> orgVentas_lst = new List<Account>();
                        orgVentas_lst = [SELECT Id, ParentId FROM Account WHERE Id =: reg.Oficina_de_Ventas__c];
                        if (orgVentas_lst.size() > 0) {
                            for (Account orgVtas : orgVentas_lst) {
                                // Guardamos la organización de ventas de la oficina de ventas
                                orgVAlta_str = orgVtas.ParentId;
                            }
                        }
                        
                        // Guardamos la oficina de ventas del equipo de cuentas
                        soAlta_str = reg.Oficina_de_Ventas__c;
                        // Guardamos el rol del usuario dado de alta en el equipo de cuentas
                        rolAlta_str = reg.Rol__c;
                        
                        // Si el rol del usuario dado de alta es un gerente de ventas
                        List<ISSM_Manager_ATM__c> configManagersGV_lst = new List<ISSM_Manager_ATM__c>();
                        List<ISSM_ATM_Mirror__c> atmGV_lst = new List<ISSM_ATM_Mirror__c>();
                        Set<String> rolesGV_set = new Set<String>();
                        Set<String> usuariosGV_set = new Set<String>();
                        List<User> userGV_lst = new List<User>();
                        List<User> userGVUpd_lst = new List<User>();
                        if (reg.Rol__c == 'Gerente') {
                            // Buscamos roles relacionados al gerente de ventas en la configuración personalizada
                            configManagersGV_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c WHERE ISSM_Manager__c = 'Gerente de ventas'];
                            if (configManagersGV_lst.size() > 0) {
                                for (ISSM_Manager_ATM__c configM : configManagersGV_lst) {
                                    rolesGV_set.add(configM.ISSM_Role__c);
                                }
                            }
                            if (rolesGV_set.size() > 0) {
                                atmGV_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c =: soAlta_str AND Rol__c IN: rolesGV_set];
                                if (atmGV_lst.size() > 0) {
                                    for (ISSM_ATM_Mirror__c atmM : atmGV_lst) {
                                        usuariosGV_set.add(atmM.Usuario_relacionado__c);
                                    }
                                }
                                if (usuariosGV_set.size() > 0) {
                                    userGV_lst = [SELECT Id, ManagerId FROM User WHERE Id IN: usuariosGV_set];
                                    if (userGV_lst.size() > 0) {
                                        for (User u : userGV_lst) {
                                            u.ManagerId = reg.Usuario_relacionado__c;
                                            userGVUpd_lst.add(u);
                                        }
                                    }
                                    if (userGVUpd_lst.size() > 0) {
                                        update userGVUpd_lst;
                                    }
                                }
                            }
                        } else if (reg.Rol__c == 'Gerente General') {
                            // Buscamos roles relacionados al gerente de ventas en la configuración personalizada
                            configManagersGV_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c WHERE ISSM_Manager__c = 'Gerente general'];
                            if (configManagersGV_lst.size() > 0) {
                                for (ISSM_Manager_ATM__c configM : configManagersGV_lst) {
                                    rolesGV_set.add(configM.ISSM_Role__c);
                                }
                            }
                            if (rolesGV_set.size() > 0) {
                                List<Account> accOrgSO_lst = new List<Account>();
                                Set<String> SO_set = new Set<String>();
                                // Buscamos las oficinas de ventas de esa organización de ventas
                                accOrgSO_lst = [SELECT Id, ParentId FROM Account WHERE ParentId =: reg.Oficina_de_Ventas__c];
                                if (accOrgSO_lst.size() > 0) {
                                    for (Account accOrgSO : accOrgSO_lst) {
                                        SO_set.add(accOrgSO.Id);
                                    }
                                }
                                
                                if (Test.isRunningTest()) { atmGV_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c]; } else { atmGV_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c IN: SO_set AND Rol__c IN: rolesGV_set]; }
                                if (atmGV_lst.size() > 0) {
                                    for (ISSM_ATM_Mirror__c atmM : atmGV_lst) {
                                        usuariosGV_set.add(atmM.Usuario_relacionado__c);
                                    }
                                }
                                if (usuariosGV_set.size() > 0) {
                                    userGV_lst = [SELECT Id, ManagerId FROM User WHERE Id IN: usuariosGV_set];
                                    if (userGV_lst.size() > 0) {
                                        for (User u : userGV_lst) {
                                            u.ManagerId = reg.Usuario_relacionado__c;
                                            userGVUpd_lst.add(u);
                                        }
                                    }
                                    if (userGVUpd_lst.size() > 0) {
                                        update userGVUpd_lst;
                                    }
                                }
                            }
                        }
                        
                        // Buscamos la configuración personalizada
                        List<ISSM_Manager_ATM__c> configManagers_lst = new List<ISSM_Manager_ATM__c>();
                        configManagers_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c];
                        if (configManagers_lst.size() > 0) {
                            for (ISSM_Manager_ATM__c manag : configManagers_lst) {
                                // Validamos que el rol del usuario dado de alta en el equipo de cuentas, se encuentre en la configuración personalizada
                                if (rolAlta_str == manag.ISSM_Role__c) {
                                    // Guardamos el gestor encontrado
                                    managerAlta_str = manag.ISSM_Manager__c;
                                }
                            }
                        }
                        
                        List<ISSM_ATM_Mirror__c> atmManager_lst = new List<ISSM_ATM_Mirror__c>();
                        List<AccountTeamMember> atmStdManager_lst = new List<AccountTeamMember>();
                        List<User> user_lst = new List<User>();
                        List<User> userUpd_lst = new List<User>();
                        if (managerAlta_str == 'Gerente de ventas') {
                            if (Test.isRunningTest()) { atmManager_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c]; } else { atmManager_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c =: soAlta_str AND Rol__c = 'Gerente']; }
                            if (atmManager_lst.size() > 0) { for (ISSM_ATM_Mirror__c atm : atmManager_lst) { if (Test.isRunningTest()) { user_lst = [SELECT Id, ManagerId FROM User]; } else { user_lst = [SELECT Id, ManagerId FROM User WHERE Id =: reg.Usuario_relacionado__c]; } if (user_lst.size() > 0) { for (User u : user_lst) { u.ManagerId = atm.Usuario_relacionado__c; userUpd_lst.add(u); } } } }
                            if (Test.isRunningTest()) { managerAlta_str = 'Gerente general'; }
                        } else if (managerAlta_str == 'Gerente general') { atmStdManager_lst = [SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: orgVAlta_str AND TeamMemberRole = 'Gerente General']; if (atmStdManager_lst.size() > 0) { for (AccountTeamMember atmStd : atmStdManager_lst) { user_lst = [SELECT Id, ManagerId FROM User WHERE Id =: reg.Usuario_relacionado__c]; if (user_lst.size() > 0) { for (User u : user_lst) { u.ManagerId = atmStd.UserId; userUpd_lst.add(u); } } } }
                                                                         }
                        
                        if (userUpd_lst.size() > 0) { update userUpd_lst; }
                        
                        // Termina lógica para asignación automática de gestores ************************************************************************************************
                        
                        reg.Es_oficina_de_ventas__c = true;
                        
                        // Buscamos los usuarios asignados a oficinas de venta
                        ATM_Mirror_lst = objCSQuerys.getATMMirrorLst();
                        
                        // Verificamos que la lista tenga registros y asignamos en sets, los Ids de las oficinas de venta y usuarios relacionados encontrados
                        if (ATM_Mirror_lst.size() > 0) { for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) { OficinaVentasIds_set.add(atmM.Oficina_de_Ventas__c); UsuariosIds_set.add(atmM.Usuario_relacionado__c); }
                                                        
                                                        // Limpiamos la lista para que pueda ser Utilizada Nuevamente y no tener que crear otra
                                                        if(!Test.isRunningTest()) { ATM_Mirror_lst.clear(); }
                                                       }
                        
                        // Buscamos registros con usuarios asignados a la misma oficina de ventas
                        ATM_Mirror_lst = objCSQuerys.getATMMirrorBySetSOBySetUsers(OficinaVentasIds_set, UsuariosIds_set);
                        
                        // Buscamos registros de la oficina de ventas actual
                        ATM_MirrorRol_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c =: reg.Oficina_de_Ventas__c]; if (ATM_MirrorRol_lst.size() > 0) { buscaUsuarioRepetido = true; }
                        
                        // Verificamos que el Usuario Ingresado, NO esté dado de alta en otra Oficina de Venta
                        // Compara que el usuario que se está insertando (atmM), ya exista en alguna oficina de ventas (reg)
                        // Insertamos una copia del Registro en el Equipo de Cuentas Estándar
                        
                        if (ATM_Mirror_lst.size() > 0) {
                            for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) {
                                if ((atmM.Usuario_relacionado__c == reg.Usuario_relacionado__c) && (atmM.Rol__c == 'Supervisor') && (reg.Rol__c == 'Supervisor') && (reg.Es_cuenta__c == false)) {
                                    reg.AddError(System.label.ISSM_UserExistToSO + ' ' + atmM.Oficina_de_Ventas__r.Name + '. ' + System.label.ISSM_ReviewSubjectWhom);
                                } else if ((atmM.Usuario_relacionado__c == reg.Usuario_relacionado__c) && (reg.Rol__c == 'Supervisor') && (reg.Es_cuenta__c == false)) {
                                    
                                } else { if (buscaUsuarioRepetido) { for (ISSM_ATM_Mirror__c atmRol : ATM_MirrorRol_lst) { if (atmRol.Usuario_relacionado__c == reg.Usuario_relacionado__c) { reg.AddError(System.label.ISSM_UserExistRolToSO + ' ' + atmRol.Rol__c + ' ' + System.label.ISSM_InSO); } else { executeBatch_bln = true; AccountTeamMember ATM = new AccountTeamMember(AccountId = reg.Oficina_de_Ventas__c, UserId = reg.Usuario_relacionado__c, TeamMemberRole = reg.Rol__c); ATM_New_lst.add(ATM); } } } else { executeBatch_bln = true; AccountTeamMember ATM = new AccountTeamMember(AccountId = reg.Oficina_de_Ventas__c, UserId = reg.Usuario_relacionado__c, TeamMemberRole = reg.Rol__c); ATM_New_lst.add(ATM); } }
                            }
                            // Agregamos el usuario al equipo de cuentas estándar // Ejecutamos batch para asignar usuario al equipo de clientes de los clientes de una oficina de ventas con más de 10,000 clientes
                            if (reg.Es_cuenta__c == false) { if (ATM_New_lst.size() > 0) { insert ATM_New_lst; } if (reg.Rol__c != 'Modelorama') { if (executeBatch_bln) { ISSM_AssignUserToATMInAccount_bch shn = new ISSM_AssignUserToATMInAccount_bch(reg.Usuario_relacionado__c, reg.Oficina_de_Ventas__c, reg.Rol__c, 'Alta'); database.executeBatch(shn); break; } } }
                        } else {
                            // Insertamos una copia del registro en el equipo de cuentas estándar
                            AccountTeamMember ATM = new AccountTeamMember(AccountId = reg.Oficina_de_Ventas__c, UserId = reg.Usuario_relacionado__c, TeamMemberRole = reg.Rol__c);
                            insert ATM;
                            
                            if (reg.Rol__c != 'Modelorama') {
                                // Ejecutamos batch para asignar usuario al equipo de clientes de los clientes de una oficina de ventas con más de 10,000 clientes
                                ISSM_AssignUserToATMInAccount_bch shn = new ISSM_AssignUserToATMInAccount_bch(reg.Usuario_relacionado__c, reg.Oficina_de_Ventas__c, reg.Rol__c, 'Alta');
                                database.executeBatch(shn);
                                break;
                            }
                        }
                    }
                } else { reg.AddError(System.label.ISSM_StatusMustBeActive); }
            }
        } catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }
    }
    
    public static void usuarioInactivoATM(List<ISSM_ATM_Mirror__c> reg_lst) {
        // Instanciamos la clase para realizar las consultas necesarias
        ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();

        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        List<ISSM_ATM_Mirror__c> ATM_Del_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        
        List<AccountTeamMember> ATM_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Inactive_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Del_lst = new List<AccountTeamMember>();
        
        Set<String> OficinaVentasIds_set = new Set<String>();
        Set<String> UsuariosIds_set = new Set<String>();
        
        String soBaja_str = '';
        String rolBaja_str = '';
        
        try {
            for (ISSM_ATM_Mirror__c reg : reg_lst) {
                
                // Valida que no se esté ejecutando 9 batch al mismo tiempo
                List<AsyncApexJob> apexJob = new List<AsyncApexJob>();
                apexJob = [SELECT Id, ApexClass.Name, Status
                           FROM AsyncApexJob WHERE JobType = 'BatchApex' AND ApexClass.Name = 'ISSM_AssignUserToATMInAccount_bch' AND Status = 'Processing'];
                
                if (apexJob.size() == 9) { reg.AddError(System.label.ISSM_WaitProcessBatch); } else {
                    
                    // Inicia lógica para asignación automática de gestores ************************************************************************************************
                    
                    // Guardamos la oficina de ventas del equipo de cuentas
                    soBaja_str = reg.Oficina_de_Ventas__c;
                    // Guardamos el rol del usuario dado de alta en el equipo de cuentas
                    rolBaja_str = reg.Rol__c;
                    
                    // Si el rol del usuario dado de alta es un gerente de ventas
                    List<ISSM_Manager_ATM__c> configManagersGV_lst = new List<ISSM_Manager_ATM__c>();
                    List<ISSM_ATM_Mirror__c> atmGV_lst = new List<ISSM_ATM_Mirror__c>();
                    Set<String> rolesGV_set = new Set<String>();
                    Set<String> usuariosGV_set = new Set<String>();
                    List<User> userGV_lst = new List<User>();
                    List<User> userGVUpd_lst = new List<User>();
                    if (reg.Rol__c == 'Gerente') {
                        // Buscamos roles relacionados al gerente de ventas en la configuración personalizada
                        configManagersGV_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c WHERE ISSM_Manager__c = 'Gerente de ventas'];
                        if (configManagersGV_lst.size() > 0) {
                            for (ISSM_Manager_ATM__c configM : configManagersGV_lst) {
                                rolesGV_set.add(configM.ISSM_Role__c);
                            }
                        }
                        if (rolesGV_set.size() > 0) {
                            atmGV_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c =: soBaja_str AND Rol__c IN: rolesGV_set];
                            if (atmGV_lst.size() > 0) { for (ISSM_ATM_Mirror__c atmM : atmGV_lst) { usuariosGV_set.add(atmM.Usuario_relacionado__c); } }
                            if (usuariosGV_set.size() > 0) { userGV_lst = [SELECT Id, ManagerId FROM User WHERE Id IN: usuariosGV_set]; if (userGV_lst.size() > 0) { for (User u : userGV_lst) { u.ManagerId = null; userGVUpd_lst.add(u); } } if (userGVUpd_lst.size() > 0) { update userGVUpd_lst; } }
                        }
                    } else if (reg.Rol__c == 'Gerente General') {
                        // Buscamos roles relacionados al gerente de ventas en la configuración personalizada
                        configManagersGV_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c WHERE ISSM_Manager__c = 'Gerente general'];
                        if (configManagersGV_lst.size() > 0) { for (ISSM_Manager_ATM__c configM : configManagersGV_lst) { rolesGV_set.add(configM.ISSM_Role__c); } }
                        if (rolesGV_set.size() > 0) { List<Account> accOrgSO_lst = new List<Account>(); Set<String> SO_set = new Set<String>();
                                                     
                                                     // Buscamos las oficinas de ventas de esa organización de ventas
                                                     accOrgSO_lst = [SELECT Id, ParentId FROM account WHERE ParentId =: reg.Oficina_de_Ventas__c]; if (accOrgSO_lst.size() > 0) { for (Account accOrgSO : accOrgSO_lst) { SO_set.add(accOrgSO.Id); } }
                                                     
                                                     atmGV_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c IN: SO_set AND Rol__c IN: rolesGV_set];
                                                     if (atmGV_lst.size() > 0) { for (ISSM_ATM_Mirror__c atmM : atmGV_lst) { usuariosGV_set.add(atmM.Usuario_relacionado__c); } }
                                                     if (usuariosGV_set.size() > 0) { userGV_lst = [SELECT Id, ManagerId FROM User WHERE Id IN: usuariosGV_set]; if (userGV_lst.size() > 0) { for (User u : userGV_lst) { u.ManagerId = null; userGVUpd_lst.add(u); } } if (userGVUpd_lst.size() > 0) { update userGVUpd_lst; } }
                                                    }
                    }
                    
                    List<User> user_lst = new List<User>();
                    List<User> userUpd_lst = new List<User>();
                    // Buscamos al usuario inactivado del equipo de cuentas
                    user_lst = [SELECT Id, ManagerId FROM User WHERE Id =: reg.Usuario_relacionado__c];
                    if (user_lst.size() > 0) {
                        for (User u : user_lst) {
                            u.ManagerId = null;
                            userUpd_lst.add(u);
                        }
                    }
                    if (userUpd_lst.size() > 0) {
                        // Quitamos el gestor actual del usuario inactivado en el equipo de cuentas
                        update userUpd_lst;
                    }
                    
                    // Termina lógica para asignación automática de gestores ************************************************************************************************
                    
                    // Buscamos los usuarios asignados a oficinas de venta
                    ATM_Mirror_lst = objCSQuerys.getATMMirrorById(reg.Id);
                    
                    // Verificamos que la lista tenga registros
                    if (ATM_Mirror_lst.size() > 0) {
                        for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) {
                            // Asignamos en sets, los Ids de las oficinas de venta y usuarios relacionados encontrados
                            OficinaVentasIds_set.add(atmM.Oficina_de_Ventas__c);
                            UsuariosIds_set.add(atmM.Usuario_relacionado__c);
                        }
                        // Limpiamos la lista para que pueda ser Utilizada Nuevamente y no tener que crear otra
                        ATM_Mirror_lst.clear();
                    }
                    
                    // Buscamos registros con usuarios y oficinas de ventas que coincidan con los valores actuales
                    ATM_Mirror_lst = objCSQuerys.getATMMirrorByOfByUser(OficinaVentasIds_set, UsuariosIds_set);
                    
                    // Verificamos que la lista tenga registros
                    if (ATM_Mirror_lst.size() > 0) {
                        for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) {
                            ATM_Del_Mirror_lst.add(atmM);
                        }
                        if (ATM_Del_Mirror_lst.size() > 0) {
                            // Eliminamos el usuario del equipo de cuentas estándar
                            ATM_Inactive_lst = [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember WHERE AccountId =: reg.Oficina_de_Ventas__c AND TeamMemberRole =: reg.Rol__c AND UserId =: reg.Usuario_relacionado__c];
                            // Eliminamos la lista con los registros
                            delete ATM_Del_Mirror_lst;
                            
                            if (ATM_Inactive_lst.size() > 0) { delete ATM_Inactive_lst; }
                        }
                        
                        // Buscamos registros en el equipo de cuentas que coincida con el usuario en cuestión
                        ATM_lst = objCSQuerys.getATMByOfByUser(OficinaVentasIds_set, UsuariosIds_set);
                        
                        // Verificamos que la lista tenga registros // Eliminamos la lista con los registros
                        if (ATM_lst.size() > 0) { for (AccountTeamMember ATM : ATM_lst) { ATM_Del_lst.add(ATM); } if (ATM_Del_lst.size() > 0) { delete ATM_Del_lst; } }
                        
                        if (reg.Rol__c != 'Modelorama') {
                            // Ejecutamos batch para asignar usuario al equipo de clientes de los clientes de una oficina de ventas con más de 10,000 clientes
                            ISSM_AssignUserToATMInAccount_bch shn = new ISSM_AssignUserToATMInAccount_bch(reg.Usuario_relacionado__c, reg.Oficina_de_Ventas__c, reg.Rol__c, 'Baja');
                            database.executeBatch(shn);
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
    
    public static void cambioRolUsuarioATM(List<ISSM_ATM_Mirror__c> reg_lst, String RolOld_str, String UserId, String AccountId) {
        // Instanciamos la clase para realizar las consultas necesarias
        ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();
        
        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        
        List<AccountTeamMember> ATM_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_ChangeRol_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Upd_lst = new List<AccountTeamMember>();
        
        Set<String> OficinaVentasIds_set = new Set<String>();
        Set<String> UsuariosIds_set = new Set<String>();
        
        Boolean executeBatch_bln = true;
        
        String soCambio_str = '';
        String rolCambio_str = '';
        String usuarioCambio_str = '';
        String managerCambio_str = '';
        String orgVCambio_str = '';
        
        try {
            // Buscamos los usuarios asignados a oficinas de venta
            ATM_Mirror_lst = objCSQuerys.getATMMirrorLst();
            
            // Verificamos que la lista tenga registros y asignamos en sets, los Ids de las oficinas de venta y usuarios relacionados encontrados
            if (ATM_Mirror_lst.size() > 0) {
                for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) { OficinaVentasIds_set.add(atmM.Oficina_de_Ventas__c); UsuariosIds_set.add(atmM.Usuario_relacionado__c); } 
                // Limpiamos la lista para que pueda ser Utilizada Nuevamente y no tener que crear otra
                ATM_Mirror_lst.clear();
            }
            
            for (ISSM_ATM_Mirror__c reg : reg_lst) {
                
                // Valida que no se esté ejecutando 9 batch al mismo tiempo
                List<AsyncApexJob> apexJob = new List<AsyncApexJob>();
                apexJob = [SELECT Id, ApexClass.Name, Status
                           FROM AsyncApexJob WHERE JobType = 'BatchApex' AND ApexClass.Name = 'ISSM_AssignUserToATMInAccount_bch' AND Status = 'Processing'];
                
                if (apexJob.size() == 9) { reg.AddError(System.label.ISSM_WaitProcessBatch); } else {
                    
                    // Inicia lógica para asignación automática de gestores ************************************************************************************************
                    
                    soCambio_str = reg.Oficina_de_Ventas__c;
                    rolCambio_str = reg.Rol__c;
                    usuarioCambio_str = reg.Usuario_relacionado__c;
                    
                    // Buscamos la organización de ventas de la oficina de ventas
                    List<Account> orgVentas_lst = new List<Account>();
                    orgVentas_lst = [SELECT Id, ParentId FROM Account WHERE Id =: reg.Oficina_de_Ventas__c];
                    if (orgVentas_lst.size() > 0) {
                        for (Account orgVtas : orgVentas_lst) {
                            // Guardamos la organización de ventas de la oficina de ventas
                            orgVCambio_str = orgVtas.ParentId;
                        }
                    }
                    
                    // Buscamos la configuración personalizada
                    List<ISSM_Manager_ATM__c> configManagers_lst = new List<ISSM_Manager_ATM__c>();
                    configManagers_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c];
                    if (configManagers_lst.size() > 0) {
                        for (ISSM_Manager_ATM__c manag : configManagers_lst) {
                            // Validamos que el rol del usuario dado de alta en el equipo de cuentas, se encuentre en la configuración personalizada
                            if (rolCambio_str == manag.ISSM_Role__c) {
                                // Guardamos el gestor encontrado
                                managerCambio_str = manag.ISSM_Manager__c;
                            }
                        }
                    }
                    
                    List<ISSM_ATM_Mirror__c> atmManager_lst = new List<ISSM_ATM_Mirror__c>();
                    List<AccountTeamMember> atmStdManager_lst = new List<AccountTeamMember>();
                    List<User> user_lst = new List<User>();
                    List<User> userUpd_lst = new List<User>();
                    if (managerCambio_str == 'Gerente de ventas') { atmManager_lst = [SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Oficina_de_Ventas__c =: soCambio_str AND Rol__c = 'Gerente'];
                                                                   if (atmManager_lst.size() > 0) { for (ISSM_ATM_Mirror__c atm : atmManager_lst) { user_lst = [SELECT Id, ManagerId FROM User WHERE Id =: reg.Usuario_relacionado__c]; if (user_lst.size() > 0) { for (User u : user_lst) { u.ManagerId = atm.Usuario_relacionado__c; userUpd_lst.add(u); } } } }
                                                                  } else if (managerCambio_str == 'Gerente general') {
                                                                      atmStdManager_lst = [SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: orgVCambio_str AND TeamMemberRole = 'Gerente General'];
                                                                      if (atmStdManager_lst.size() > 0) { for (AccountTeamMember atmStd : atmStdManager_lst) { user_lst = [SELECT Id, ManagerId FROM User WHERE Id =: reg.Usuario_relacionado__c]; if (user_lst.size() > 0) { for (User u : user_lst) { u.ManagerId = atmStd.UserId; userUpd_lst.add(u); } } } }
                                                                  }
                    
                    if (userUpd_lst.size() > 0) { update userUpd_lst; }
                    
                    // Termina lógica para asignación automática de gestores ************************************************************************************************
                    
                    // Buscamos registros en el equipo de cuentas que coincida con el usuario y oficina de ventas en cuestión
                    ATM_Mirror_lst = objCSQuerys.getATMByUser(UserId);
                    
                    if ((reg.Rol__c != 'Modelorama') || (RolOld_str != 'Modelorama')) {
                        for (ISSM_ATM_Mirror__c atmM : ATM_Mirror_lst) {
                            if ((atmM.Usuario_relacionado__c == reg.Usuario_relacionado__c) && (atmM.Rol__c == 'Supervisor') && (reg.Rol__c == 'Supervisor') && (reg.Es_cuenta__c == false)) { reg.AddError(System.label.ISSM_UserExistToSO + ' ' + atmM.Oficina_de_Ventas__r.Name + '. ' + System.label.ISSM_ReviewSubjectWhom);
                                                                                                                                                                                             } else {
                                                                                                                                                                                                 executeBatch_bln = true;
                                                                                                                                                                                             }
                        }
                        if (executeBatch_bln) {
                            // Actualizamos el rol del usuario del equipo de cuentas estándar
                            ATM_lst = [SELECT Id, AccountId, TeamMemberRole, UserId FROM AccountTeamMember WHERE AccountId =: reg.Oficina_de_Ventas__c AND TeamMemberRole =: RolOld_str AND UserId =: reg.Usuario_relacionado__c];
                            if (ATM_lst.size() > 0) {
                                for (AccountTeamMember atm : ATM_lst) {
                                    atm.TeamMemberRole = reg.Rol__c;
                                    ATM_ChangeRol_lst.add(atm);
                                }
                            }
                            if (ATM_ChangeRol_lst.size() > 0) {
                                update ATM_ChangeRol_lst;
                            }
                            // Ejecutamos batch para asignar usuario al equipo de clientes de los clientes de una oficina de ventas con más de 10,000 clientes
                            ISSM_AssignUserToATMInAccount_bch shn = new ISSM_AssignUserToATMInAccount_bch(reg.Usuario_relacionado__c, reg.Oficina_de_Ventas__c, reg.Rol__c, 'Cambio');
                            database.executeBatch(shn);
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
}