/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows searches for filling relationships and filling information in 
                        wrapper classes

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    17-April-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class ISSM_MultiSelectLookUp_cls { 
    /**
    * @description  Receives a serie of parameters to create a SOQL query in order to retrieve records from 
    *               the server, the results are returned in Price Grouper
    * 
    * @param    StrColumns          Columns on which the SELECT is made
    * @param    searchKeyWord       Word where matches are required in the search
    * @param    ObjectName          Object on which the search will be carried out
    * @param    ExcludeitemsList    List of words on which you want to limit the search
    * @param    strLimit            Limit of records to show
    * 
    * @return   Return a list of type 'sObject' for Price Grouping
    */
	@AuraEnabled
    public static sObject[] SearchPriceGrouping(String StrColumns, String searchKeyWord, 
                                                String ObjectName, sObject[] ExcludeitemsList,
                                                Integer strLimit) {

        sObject[] returnList     = new List<sObject>();
        String[] lstExcludeitems = new List<String>();
        searchKeyWord   ='\'%' + String.escapeSingleQuotes(searchKeyWord.trim()) + '%\'';
        lstExcludeitems = ISSM_UtilityFactory_cls.createLstItems(ExcludeitemsList,'Id');     
        
        String  sQuery= 'SELECT '+StrColumns;
                sQuery+=' FROM '+ObjectName;
                sQuery+=' WHERE (Description__c LIKE '+searchKeyWord;
                sQuery+=' OR Code__c LIKE '+searchKeyWord;
                sQuery+=') AND Catalog__c = '+'\'' + Label.TRM_Segmento + '\'';
                sQuery+=' AND Active_Revenue__c = true AND Active__c = true';
                sQuery+= (!lstExcludeitems.isEmpty()) ? ' AND Id NOT IN (' + String.join( lstExcludeitems, ',' ) +')' : '';
                sQuery+=' ORDER BY createdDate DESC LIMIT ' +strLimit;
        
        System.debug(sQuery);
        // Create a Dynamic SOQL Query For Fetch Record List
        returnList = ISSM_UtilityFactory_cls.executeQuery(sQuery);
        return returnList;
    }

    /**
    * @description  Receives a serie of parameters to create a SOQL query in order to retrieve records from 
    *               the server, the results are returned in Products
    * 
    * @param    StrColumns          Columns on which the SELECT is made
    * @param    searchKeyWord       Word where matches are required in the search
    * @param    ObjectName          Object on which the search will be carried out
    * @param    ExcludeitemsList    List of words on which you want to limit the search
    * @param    strLimit            Limit of records to show
    * 
    * @return   Return a list of type 'WrapperClass' for Products
    */
    @AuraEnabled
    public static WrapperClass[] SearchProducts(String StrColumns, String searchKeyWord, 
                                                String ObjectName, sObject[] ExcludeitemsList,
                                                Integer strLimit) {

        String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName(ObjectName,Label.TRM_Products);
        ONTAP__Product__c[] returnList     = new List<ONTAP__Product__c>();
        WrapperClass[] wrpLst    = new List<WrapperClass>();
        String[] lstExcludeitems = new List<String>();
        searchKeyWord   ='\'%' + String.escapeSingleQuotes(searchKeyWord.trim()) + '%\'';
        lstExcludeitems = ISSM_UtilityFactory_cls.createLstItems(ExcludeitemsList,'Id');    
        String  sQuery= 'SELECT '+StrColumns;
                sQuery+=' FROM '+ObjectName;
                sQuery+=' WHERE (ONTAP__ProductId__c LIKE '+searchKeyWord;
                sQuery+=' OR ONTAP__MaterialProduct__c LIKE '+searchKeyWord+')';
                sQuery+=' AND RecordTypeId = '+'\'' +RecType+ '\'';
                sQuery+=' AND ONTAP__ProductType__c != '+'\'' + 'LEER' + '\'';
                sQuery+= (!lstExcludeitems.isEmpty()) ? ' AND Id NOT IN (' + String.join( lstExcludeitems, ',' ) +')' : '';
                sQuery+=' ORDER BY createdDate DESC LIMIT ' +strLimit;

        System.debug(sQuery);
        // Create a Dynamic SOQL Query For Fetch Record List
        returnList = ISSM_UtilityFactory_cls.executeQuery(sQuery);
        
        for(ONTAP__Product__c obj : returnList){
            wrpLst.add(new WrapperClass(obj,false,0,0.0,0.0,Label.TRM_Product,obj.ONTAP__MaterialProduct__c,obj.ONTAP__ProductId__c));
        }

        return wrpLst;
    } 

    /**
    * @description  Receives a serie of parameters to create a SOQL query in order to retrieve records from 
    *               the server, the results are returned in Quotas
    * 
    * @param    StrColumns          Columns on which the SELECT is made
    * @param    searchKeyWord       Word where matches are required in the search
    * @param    ObjectName          Object on which the search will be carried out
    * @param    ExcludeitemsList    List of words on which you want to limit the search
    * @param    strLimit            Limit of records to show
    * 
    * @return   Return a list of type 'WrapperClass' for Quotas
    */
    @AuraEnabled
    public static WrapperClass[] SearchQuotas(String StrColumns, String searchKeyWord, 
                                              String ObjectName, sObject[] ExcludeitemsList,
                                              Integer strLimit) {
        MDM_Parameter__c[] returnList     = new List<MDM_Parameter__c>();
        WrapperClass[] wrpLst    = new List<WrapperClass>();
        String[] lstExcludeitems = new List<String>();
        searchKeyWord   ='\'%' + String.escapeSingleQuotes(searchKeyWord.trim()) + '%\'';
        lstExcludeitems = ISSM_UtilityFactory_cls.createLstItems(ExcludeitemsList,'Id');     
        
        String  sQuery= 'SELECT '+StrColumns;
                sQuery+=' FROM '+ObjectName;
                sQuery+=' WHERE (Description__c LIKE '+searchKeyWord;
                sQuery+=' OR Code__c LIKE '+searchKeyWord;
                sQuery+=') AND Catalog__c = '+'\'' + Label.TRM_GrupoMateriales1 + '\'';
                sQuery+=' AND Active_Revenue__c = true AND Active__c = true';
                sQuery+= (!lstExcludeitems.isEmpty()) ? ' AND Id NOT IN (' + String.join( lstExcludeitems, ',' ) +')' : '';
                sQuery+=' ORDER BY createdDate DESC LIMIT ' +strLimit;
        
        System.debug(sQuery);
        // Create a Dynamic SOQL Query For Fetch Record List
        returnList = ISSM_UtilityFactory_cls.executeQuery(sQuery);
        
        for(MDM_Parameter__c obj : returnList){
            wrpLst.add(new WrapperClass(obj,false,0,0.0,0.0,Label.TRM_Quota,obj.Description__c,obj.Code__c));
        }

        return wrpLst;
    }

    /**
    * @description  Method that allows to obtain the list of values ​​of a selection list
    * 
    * @param    objObject    Object on which the search will be carried out
    * @param    strFld       Field that contains the list of values ​​to search
    * 
    * @return   Return a list of type 'String'
    */
    @AuraEnabled
    public static String[] getOptions(sObject objObject, String strFld){
        return ISSM_UtilityFactory_cls.getSelectOptions(objObject,strFld);
    } 

     /**
    * @description  Wrapper for list of products
    * @param    none
    * @return   none
    */
    //
    public class WrapperClass{
        @AuraEnabled public sObject objProducts{get;set;}
        @AuraEnabled public boolean isSelected{get;set;}
        @AuraEnabled public String StrType{get;set;}
        @AuraEnabled public Integer IntQuantity{get;set;}
        @AuraEnabled public Decimal DecUnitPriceTax{get;set;}
        @AuraEnabled public Decimal DecUnitPrice{get;set;}
        @AuraEnabled public String StrName{get;set;} 
        @AuraEnabled public String StrSKU{get;set;}      

        public WrapperClass(sObject objProducts, Boolean isSelected, Integer IntQuantity, 
                            Decimal DecUnitPriceTax, Decimal DecUnitPrice,String StrType, String StrName, String StrSKU) {
            
            this.objProducts        = objProducts;
            this.isSelected         = isSelected;
            this.IntQuantity        = IntQuantity;
            this.DecUnitPriceTax    = DecUnitPriceTax;
            this.DecUnitPrice       = DecUnitPrice;
            this.StrType            = StrType;
            this.StrName            = StrName;
            this.StrSKU             = StrSKU;
        }
    }   
}