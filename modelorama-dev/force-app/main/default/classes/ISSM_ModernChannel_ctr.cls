/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Controller that manage the Taking Order Process Modern Channel Y013 AND Y023

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       05-Junio-2017   Luis Licona                  Class Creation
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_ModernChannel_ctr {
    public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    public static ONTAP__Order_Item__c[] LstOItem{get;set;}
    public static ONTAP__Order_Item__c ObjOItem {get;set;}

    public static ISSM_NewOrder_ctr.WrpOrder getProductsModernChanel(String IdOrder,String StrIdAccnt,String StrOrg,
                                                                     Map<String, Double> MapProd,Map<Id,Id> MapIdOrd){
        System.debug('#####INGRESO A CANAL MODERNO');
        ISSM_MaterialAvailability_cls CLSWSMA = new ISSM_MaterialAvailability_cls();
        ISSM_DeserializeMaterialAvailability_cls ObjDesMA;
        ONTAP__Product__c[] LstProd = new List<ONTAP__Product__c>();
        Map<String,Long> MapOIMA    = new Map<String,Long>();
        ISSM_Bonus__c[] LstBns      = new List<ISSM_Bonus__c>();
        LstOItem = new List<ONTAP__Order_Item__c>();

        if(!MapProd.isEmpty()){
            //LstProd = CTRSOQL.getProductListbyMap(MapProd);
            List<String> idsProds_lst = new List<String>();
            idsProds_lst.addAll(MapProd.keySet());
            LstProd = CTRSOQL.getProductListbyMap2(idsProds_lst);
            //CALL SERVICE Material Available
            ObjDesMA = CLSWSMA.CallOutMaterialAvailability(StrIdAccnt,LstProd,false,LstBns);

            for(ISSM_DeserializeMaterialAvailability_cls.Materials objMA : ObjDesMA.Materials)
                MapOIMA.put(objMA.productId, objMA.availableQuantity);

            if(!MapOIMA.isEmpty()){
                for(ONTAP__Product__c OBJPRODUCT : LstProd){
                    ObjOItem = new ONTAP__Order_Item__c();
                    if(OBJPRODUCT != null){
                        ObjOItem.ONCALL__OnCall_Quantity__c = MapProd.get(OBJPRODUCT.Id);
                        ObjOItem.ONCALL__OnCall_Product__c  = OBJPRODUCT.Id;
                        ObjOItem.ONTAP__CustomerOrder__c    = IdOrder;
                        ObjOItem.ISSM_OrderItemSKU__c       = OBJPRODUCT.ISSM_ProductSKU__c;
                        ObjOItem.ISSM_ProductDesc__c        = OBJPRODUCT.ONTAP__ProductShortName__c;
                        ObjOItem.ISSM_MaterialProduct__c    = OBJPRODUCT.ONTAP__MaterialProduct__c;
                        ObjOItem.ISSM_UnitofMeasure__c      = OBJPRODUCT.ONTAP__UnitofMeasure__c;
                        ObjOItem.ONCALL__SAP_Order_Item_Number__c = OBJPRODUCT.ONCALL__Material_Number__c;
                        ObjOItem.ISSM_MaterialAvailable__c  = MapOIMA.get(OBJPRODUCT.ONCALL__Material_Number__c);
                        ObjOItem.Id                         = MapIdOrd.get(OBJPRODUCT.Id);
                        ObjOItem.ISSM_Uint_Measure_Code__c  = OBJPRODUCT.ISSM_Uint_Measure_Code__c;
                        ObjOItem.ISSM_BalanceNum__c         = OBJPRODUCT.ISSM_Empties_Material__r.ONCALL__Material_Number__c;
                        ObjOItem.ISSM_Is_returnable__c      = OBJPRODUCT.ISSM_Is_returnable__c;
                        ObjOItem.ISSM_EmptyMaterial__c      = OBJPRODUCT.ISSM_EmptyMaterial__c;
                        ObjOItem.ISSM_EmptyMaterialProduct__c= OBJPRODUCT.ISSM_Empties_Material__r.ONTAP__MaterialProduct__c;
                        ObjOItem.ISSM_EmptyRack__c          = OBJPRODUCT.ISSM_EmptyRack__c;
                        ObjOItem.ISSM_RackMaterialProduct__c= OBJPRODUCT.ISSM_BoxRack__r.ONTAP__MaterialProduct__c;  
                        LstOItem.add(ObjOItem);
                    }
                }
            }
            System.debug('LstOItem: '+LstOItem);
        }
        return new ISSM_NewOrder_ctr.WrpOrder(LstOItem,null,null,null,0.0,0.0,'',null,null,null,null,null,null,null,null);
        //return null;
    }
}