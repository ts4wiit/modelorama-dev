@isTest
private class TRM_RecordFieldSet_tst {

	@isTest static void testGetFields() {
		Test.startTest();
			TRM_RecordFieldSet_ctr.getFields('TRM_ConditionClass__c','TRM_Default');
		Test.stopTest();
	}

	@isTest static void testGetRecordTypeIdByDeveloperName() {
		Test.startTest();
			TRM_RecordFieldSet_ctr.getRecordTypeIdByDeveloperName('TRM_ConditionClass__c','TRM_ConditionClassMX');
		Test.stopTest();
	}

	@isTest static void testGetFieldsetNameByField() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		Test.startTest();
			TRM_RecordFieldSet_ctr.getFieldsetNameByField('TRM_ConditionClass__c', trm.id ,'TRM_Status__c');
		Test.stopTest();
	}

	@isTest static void testGetRecordById() {
		TRM_ConditionClass__c trm = new TRM_ConditionClass__c(
			TRM_Status__c = 'TRM_Pending'	
		);
		insert trm;
		Test.startTest();
			TRM_RecordFieldSet_ctr.getRecordById('TRM_ConditionClass__c', trm.id);
		Test.stopTest();
	}
}