/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class ISSM_CollectionManagementCase_tst {
    public static  List<ISSM_AppSetting_cs__c> AppSetting;
    
    
    /*-------------------------------------------Creacion de Casos --------------------------------------------------------------*/ 
    /*
Metodo que realiza las pruebas para la creacion de casos en sus 3 validaciones coorrespondientes
*/
    static testMethod void TestCreateCases() {
        String RecordTypeTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
        String RecordTypeAccountIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        
        //Creamos el usuario y la tipificacion para posterior insertarla en la configuracion personalizada
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix1 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00002','Gestión Cobranza','Aclaracion','Pago no reconocido',null,null,null,'Trade Marketing',user.Id,null);
        ISSM_TypificationMatrix__c  TypificationMatrix2 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00003','Gestión Cobranza','Aclaracion','Cargo no reconocido',null,null,null,'Trade Marketing',user.Id,null);
        ISSM_TypificationMatrix__c  TypificationMatrix3 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00004','Gestión Cobranza','Aclaracion','Plan de pago',null,null,null,'Trade Marketing',user.Id,null);
        List<ISSM_AppSetting_cs__c> AppSettingProviderTypification =  ISSM_CreateDataTest_cls.fn_CreateAppSettingTypification(true,String.valueOf(TypificationMatrix1.Id),String.valueOf(TypificationMatrix2.Id),String.valueOf(TypificationMatrix3.Id),mapIdQueue.get('ISSM_WithoutOwner')); 
        
        ISSM_TypificationMatrix__c tM1 = new ISSM_TypificationMatrix__c();
        tm1.ISSM_UniqueIdentifier__c = 'TM - 00005';
        tM1.ISSM_TypificationLevel1__c = 'Modelorama';
        tM1.ISSM_TypificationLevel2__c = 'Modelorama empresarios';
        tM1.ISSM_TypificationLevel3__c = 'Quejas y sugerencias';
        tM1.ISSM_TypificationLevel4__c = 'Producto dañado';
        tM1.ISSM_AssignedTo__c = 'Modelorama';
        insert tM1;
        
        ISSM_TypificationMatrix__c tM2 = new ISSM_TypificationMatrix__c();
        tm2.ISSM_UniqueIdentifier__c = 'TM - 00006';
        tM2.ISSM_TypificationLevel1__c = 'Clientes y clientes nuevos';
        tM2.ISSM_TypificationLevel2__c = 'Ventas';
        tM2.ISSM_TypificationLevel3__c = 'Administrativo';
        tM2.ISSM_TypificationLevel4__c = 'Facturación electrónica';
        tM2.ISSM_AssignedTo__c = 'Billing Manager';
        insert tM2;
        
        ISSM_TypificationMatrix__c tM3 = new ISSM_TypificationMatrix__c();
        tm3.ISSM_UniqueIdentifier__c = 'TM - 00007';
        tM3.ISSM_TypificationLevel1__c = 'Refrigeración';
        tM3.ISSM_TypificationLevel2__c = 'Mantenimiento';
        tM3.ISSM_TypificationLevel3__c = 'Cooler sin garantía (Flat fee)';
        tM3.ISSM_TypificationLevel4__c = 'Imbera';
        tM3.ISSM_AssignedTo__c = 'Boss Refrigeration';
        insert tM3;
        
        //Creamo el prospecto para asignar el valor de este al campo ONCALL__POC__c de  la llamada
        Account objAccount = new  Account();
        objAccount.Name = 'Account Prospect';
        objAccount.ONTAP__Street__c = 'SMateo';
        objAccount.ONTAP__Street_Number__c = '233';
        objAccount.ONTAP__Classification__c = 'Botella Abierta';
        objAccount.ONTAP__Segment__c = 'Buena Carta';
        objAccount.ONTAP__Main_Phone__c = '555555555';
        objAccount.ISSM_LastPaymentPlanDate__c = null;
        objAccount.ISSM_LastContactDate__c = null;
        objAccount.RecordTypeId = RecordTypeAccountIdProspect;
        insert objAccount;
        
        // Creamos las llamadas
        List<ONCALL__Call__c> lstDataNewCall = new List<ONCALL__Call__c>();
        ONCALL__Call__c ObjCall = new ONCALL__Call__c();
        ObjCall.RecordTypeId = RecordTypeTelecollection;
        ObjCall.ISSM_Active__c = true;
        ObjCall.Name = 'Call Telecollection';
        ObjCall.ONCALL__POC__c = objAccount.Id;
        insert ObjCall;
        lstDataNewCall.add(ObjCall);
        
        ONCALL__Call__c ObjCall2 = new ONCALL__Call__c();
        ObjCall2.RecordTypeId = RecordTypeTelecollection;
        ObjCall2.ISSM_Active__c = true;
        ObjCall2.Name = 'Call Telecollection';
        ObjCall2.ONCALL__POC__c = objAccount.Id;
        insert ObjCall2;
        
        ONCALL__Call__c ObjCall3 = new ONCALL__Call__c();
        ObjCall3.RecordTypeId = RecordTypeTelecollection;
        ObjCall3.ISSM_Active__c = true;
        ObjCall3.Name = 'Call Telecollection';
        ObjCall3.ONCALL__POC__c = objAccount.Id;
        insert ObjCall3;
        
        ONCALL__Call__c ObjCall4 = new ONCALL__Call__c();
        ObjCall4.RecordTypeId = RecordTypeTelecollection;
        ObjCall4.ISSM_Active__c = true;
        ObjCall4.Name = 'Call Telecollection';
        ObjCall4.ONCALL__POC__c = objAccount.Id;
        insert ObjCall4;
        
        ONCALL__Call__c ObjCall5 = new ONCALL__Call__c();
        ObjCall5.RecordTypeId = RecordTypeTelecollection;
        ObjCall5.ISSM_Active__c = true;
        ObjCall5.Name = 'Call Telecollection';
        ObjCall5.ONCALL__POC__c = objAccount.Id;
        insert ObjCall5;
        
        String strCustomSettingFieldId = '';
        List<ISSM_TypificationMatrix__c> typificationMatrix = new List<ISSM_TypificationMatrix__c>();
        strCustomSettingFieldId = [SELECT Id FROM ISSM_TypificationMatrix__c LIMIT 1].Id;
        
        Test.startTest();
        
        // Insertamos un caso
        List<Case> case_lst = new List<Case>();
        Case objCase = new Case();
        objCase.Subject = 'Subject';
        objCase.Description = 'Description';
        objCase.Status = 'Closed';
        objCase.ISSM_Channel__c = 'B2B';
        objCase.ISSM_TypificationLevel1__c = 'Refrigeración';
        objCase.ISSM_TypificationLevel2__c = 'Entrega de equipo';
        objCase.AccountId = objAccount.Id;
        insert objCase;
        case_lst.add(objCase);
        
        ISSM_CollectionManagementCaseHandler_cls.createCaseByChannel(case_lst);
        
        //ISSM_CollectionManagementCaseHandler_cls.CreateRecordCase(lstDataNewCall, strCustomSettingFieldId);
        
        /*Plan de pago
        	Actualizamos la llamada con los valores correspondientes para que se ejecute el trigger y posterior la clase
        */
        ONCALL__Call__c ObjCallUpdatePlanPayment = new ONCALL__Call__c();
        ObjCallUpdatePlanPayment.Id = ObjCall.Id;
        ObjCallUpdatePlanPayment.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdatePlanPayment.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdatePlanPayment.ISSM_PaymentPromise__c = 'No';
        ObjCallUpdatePlanPayment.ISSM_ClarificationRequest__c = 'No';
        ObjCallUpdatePlanPayment.ISSM_PaymentPlanRequest__c = 'Yes';
        ObjCallUpdatePlanPayment.ISSM_PaymentPlanMaxDate__c = date.today();
        ObjCallUpdatePlanPayment.ONCALL__POC__c = objAccount.Id;
        update ObjCallUpdatePlanPayment;
        
        /*Pago no reconocido
        	Actualizamos la llamada con los valores correspondientes para que se ejecute el trigger y posterior la clase
        */
        ONCALL__Call__c ObjCallUpdatePaymentNotR2 = new ONCALL__Call__c();
        ObjCallUpdatePaymentNotR2.Id = objCall2.Id;
        ObjCallUpdatePaymentNotR2.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdatePaymentNotR2.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdatePaymentNotR2.ISSM_PaymentPromise__c = 'No';
        ObjCallUpdatePaymentNotR2.ISSM_PaymentPlanRequest__c = 'No';
        ObjCallUpdatePaymentNotR2.ISSM_ClarificationRequest__c = 'Yes';
        ObjCallUpdatePaymentNotR2.ISSM_ClarificationType__c = 'Payment not reflected';
        ObjCallUpdatePaymentNotR2.ONCALL__POC__c = objAccount.Id;
        update ObjCallUpdatePaymentNotR2;
        
        /*Cargo no reconocido
        	Actualizamos la llamada con los valores correspondientes para que se ejecute el trigger y posterior la clase
        */
        ONCALL__Call__c ObjCallUpdateUnrecognized2 = new ONCALL__Call__c();
        ObjCallUpdateUnrecognized2.Id = objCall3.Id;
        ObjCallUpdateUnrecognized2.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdateUnrecognized2.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdateUnrecognized2.ISSM_ClarificationRequest__c = 'Yes';
        ObjCallUpdateUnrecognized2.ISSM_ClarificationType__c = 'Unrecognized charge';
        ObjCallUpdateUnrecognized2.ONCALL__POC__c = objAccount.Id;
        update ObjCallUpdateUnrecognized2;
        
        /*Create case when the customer request a payment plan
        	Actualizamos la llamada con los valores correspondientes para que se ejecute el trigger y posterior la clase
        */
        ONCALL__Call__c ObjCallUpdatePaymentPlanReq = new ONCALL__Call__c();
        ObjCallUpdatePaymentPlanReq.Id = ObjCall4.Id;
        ObjCallUpdatePaymentPlanReq.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdatePaymentPlanReq.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdatePaymentPlanReq.ISSM_PaymentPromise__c = 'No';
        ObjCallUpdatePaymentPlanReq.ISSM_ClarificationRequest__c = 'No';
        ObjCallUpdatePaymentPlanReq.ISSM_PaymentPlanRequest__c = 'Yes';
        ObjCallUpdatePaymentPlanReq.ISSM_PaymentPlanMaxDate__c = date.today();
        ObjCallUpdatePaymentPlanReq.ONCALL__POC__c = objAccount.Id;
        update ObjCallUpdatePaymentPlanReq;
        
        /*Update Call Back Queue
        	 Actualizamos la llamada con los valores correspondientes para que se ejecute el trigger y posterior la clase
        */
        ONCALL__Call__c ObjCallUpdateCallBackQueue = new ONCALL__Call__c();
        ObjCallUpdateCallBackQueue.Id = ObjCall5.Id;
        ObjCallUpdateCallBackQueue.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdateCallBackQueue.ONCALL__Call_Status__c = 'Incomplete';
        ObjCallUpdateCallBackQueue.ONCALL__POC__c = objAccount.Id;
        update ObjCallUpdateCallBackQueue;
        
        Test.stopTest();   
    }
    
    static testMethod void TestAssignCallBtn() {
        
        String RecordTypeTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
        
        //Creamos la llamada
        ONCALL__Call__c ObjCall = new ONCALL__Call__c();
        ObjCall.RecordTypeId = RecordTypeTelecollection;
        ObjCall.ISSM_Active__c = true;
        ObjCall.Name = 'Call Telecollection';
        insert ObjCall;
        
        Test.startTest();
        
        ISSM_ExecuteAssignCallBtn_ctr.validate(objCall.Id);
        
        Test.stopTest();
    }
    
    /*------------------------------------------- Actualizacion de Cuenta  --------------------------------------------------------------*/
    /*
    	Metodo que realiza las pruebas para la creacion de casos en sus 3 validaciones coorrespondientes
    */
    static testMethod void TestUpdateAccounts() {
        String RecordTypeTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId(); 
        String RecordTypeAccountIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        
        //Creamos el usuario y la tipificacion para posterior insertarla en la configuracion personalizada
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix1 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00002','Gestión Cobranza','Aclaracion','Pago no reconocido',null,null,null,'Trade Marketing',user.Id,null);    
        ISSM_TypificationMatrix__c  TypificationMatrix2 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00003','Gestión Cobranza','Aclaracion','Cargo no reconocido',null,null,null,'Trade Marketing',user.Id,null);   
        ISSM_TypificationMatrix__c  TypificationMatrix3 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00004','Gestión Cobranza','Aclaracion','Plan de pago',null,null,null,'Trade Marketing',user.Id,null);  
        List<ISSM_AppSetting_cs__c> AppSettingProviderTypification =  ISSM_CreateDataTest_cls.fn_CreateAppSettingTypification(true,String.valueOf(TypificationMatrix1.Id),String.valueOf(TypificationMatrix2.Id),String.valueOf(TypificationMatrix3.Id),mapIdQueue.get('ISSM_WithoutOwner')); 
        
        //Creamo el prospecto para asignar el valor de este al campo ONCALL__POC__c de  la llamada
        Account objAccountProspect = new  Account();
        objAccountProspect.Name = 'Account Prospect';
        objAccountProspect.ONTAP__Street__c = 'SMateo';
        objAccountProspect.ONTAP__Street_Number__c = '233';
        objAccountProspect.ONTAP__Classification__c = 'Botella Abierta';
        objAccountProspect.ONTAP__Segment__c = 'Buena Carta';
        objAccountProspect.ONTAP__Main_Phone__c = '555555555';
        objAccountProspect.RecordTypeId = RecordTypeAccountIdProspect;
        objAccountProspect.ISSM_LastPaymentPlanDate__c = null;
        objAccountProspect.ISSM_LastContactDate__c = null;
        insert objAccountProspect;
        
        //Creamos la llamada
        ONCALL__Call__c ObjCall = new ONCALL__Call__c();
        ObjCall.Name ='Call Telecollection';
        ObjCall.RecordTypeId = RecordTypeTelecollection;
        ObjCall.ISSM_Active__c = true;
        ObjCall.ONCALL__POC__c = objAccountProspect.Id;
        insert ObjCall;
        
        //Creamo el prospecto para asignar el valor de este al campo ONCALL__POC__c de  la llamada
        Account objAccountProspectCall2 = new  Account();
        objAccountProspectCall2.Name = 'Account Prospect';
        objAccountProspectCall2.ONTAP__Street__c = 'SMateo';
        objAccountProspectCall2.ONTAP__Street_Number__c = '233';
        objAccountProspectCall2.ONTAP__Classification__c = 'Botella Abierta';
        objAccountProspectCall2.ONTAP__Segment__c = 'Buena Carta';
        objAccountProspectCall2.ONTAP__Main_Phone__c = '555555555';
        objAccountProspectCall2.RecordTypeId = RecordTypeAccountIdProspect;
        objAccountProspectCall2.ISSM_LastPaymentPlanDate__c = null;
        objAccountProspectCall2.ISSM_LastContactDate__c = null;
        insert objAccountProspectCall2;
        
        //Creamos la llamada
        ONCALL__Call__c ObjCall2 = new ONCALL__Call__c();
        ObjCall2.Name ='Call Telecollection 2';
        ObjCall2.RecordTypeId = RecordTypeTelecollection;
        ObjCall2.ISSM_Active__c = true;
        ObjCall2.ONCALL__POC__c = objAccountProspectCall2.Id;
        insert ObjCall2;
        
        //Creamo el prospecto para asignar el valor de este al campo ONCALL__POC__c de  la llamada
        Account objAccountProspectCall3 = new  Account();
        objAccountProspectCall3.Name = 'Account Prospect';
        objAccountProspectCall3.ONTAP__Street__c = 'SMateo';
        objAccountProspectCall3.ONTAP__Street_Number__c = '233';
        objAccountProspectCall3.ONTAP__Classification__c = 'Botella Abierta';
        objAccountProspectCall3.ONTAP__Segment__c = 'Buena Carta';
        objAccountProspectCall3.ONTAP__Main_Phone__c = '555555555';
        objAccountProspectCall3.RecordTypeId = RecordTypeAccountIdProspect;
        objAccountProspectCall3.ISSM_LastContactDate__c = null;
        insert objAccountProspectCall3;
        
        //Creamos la llamada
        ONCALL__Call__c ObjCall3 = new ONCALL__Call__c();
        ObjCall3.Name ='Call Telecollection 2';
        ObjCall3.RecordTypeId = RecordTypeTelecollection;
        ObjCall3.ISSM_Active__c = true;
        ObjCall3.ONCALL__POC__c = objAccountProspectCall3.Id;
        insert ObjCall3;
        
        Test.startTest();
        
        ONCALL__Call__c ObjCallUpdate1 = new ONCALL__Call__c();
        ObjCallUpdate1.Id = ObjCall.Id;
        ObjCallUpdate1.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdate1.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdate1.ISSM_PaymentPromise__c = 'Yes';
        ObjCallUpdate1.ISSM_PaymentPlanRequest__c = 'No';
        ObjCallUpdate1.ISSM_ClarificationRequest__c ='No';
        ObjCallUpdate1.ISSM_PaymentPromiseDate__c = System.today();
        ObjCallUpdate1.ISSM_CompromisedAmount__c = 100;
        update ObjCallUpdate1;
        
        ONCALL__Call__c ObjCallUpdate2 = new ONCALL__Call__c();
        ObjCallUpdate2.Id = ObjCall2.Id;
        ObjCallUpdate2.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdate2.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdate2.ISSM_PaymentPromise__c = 'No';
        ObjCallUpdate2.ISSM_PaymentPlanRequest__c = 'Yes';
        ObjCallUpdate2.ISSM_PaymentPlanMaxDate__c = date.today();
        ObjCallUpdate2.ISSM_ClarificationRequest__c ='No';
        update ObjCallUpdate2;
        
        ONCALL__Call__c ObjCallUpdate3 = new ONCALL__Call__c();
        ObjCallUpdate3.Id = ObjCall3.Id;
        ObjCallUpdate3.ONCALL__Call_Status__c = 'Complete';
        ObjCallUpdate3.ISSM_CallEffectiveness__c = 'Effective';
        ObjCallUpdate3.ISSM_PaymentPromise__c = 'No';
        ObjCallUpdate3.ISSM_PaymentPlanRequest__c = 'No';
        ObjCallUpdate3.ISSM_ClarificationRequest__c ='No';
        update ObjCallUpdate3;
        
        Test.stopTest();   
    }
}