/**
 * Test class for AllMobileDeviceUserCATUserTypeController class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserCATUserTypeCtrlTest {

    //Test method.
    static testMethod void Test() {

        //Insert data.
        AllMobilePermissionCategoryUserType__c objAMPermissionCatUserType = new AllMobilePermissionCategoryUserType__c();
        objAMPermissionCatUserType.AllMobileDeviceUserBinaryPermission__c='0000000000';
        insert objAMPermissionCatUserType;
        inserts();
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAMPermissionCatUserType);
        AllMobileDeviceUserCATUserTypeController controller = new AllMobileDeviceUserCATUserTypeController(objStandardController);

        //Insert data.
        AllMobilePermissionCategoryUserType__c objAMPermissionCatUserType2 = new AllMobilePermissionCategoryUserType__c();
        objAMPermissionCatUserType2.AllMobileDeviceUserBinaryPermission__c='1111111111';
        insert objAMPermissionCatUserType2;
        ApexPages.StandardController objStandardController2 = new ApexPages.StandardController(objAMPermissionCatUserType2);
        AllMobileDeviceUserCATUserTypeController controller2 = new AllMobileDeviceUserCATUserTypeController(objStandardController2);
    }

    /**
     * This method inserts Permissions.
     */
    public static void inserts() {
        AllMobilePermission__c objAllMobilePermission = new AllMobilePermission__c();
        objAllMobilePermission.AllMobileDeviceUserBinaryPosition__c = 2.0;
        objAllMobilePermission.AllMobileDeviceUserPermissionKey__c = 'key';
        insert objAllMobilePermission;
    }
}