/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice Assets

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_Asset_cls implements ISSM_ObjectInterface_cls{

	ONTAP__Account_Asset__c accountAsset;
	public List<ONTAP__Account_Asset__c> accountsAssets;
	private List<salesforce_ontap_account_asset_c__x> accountAssetsDelete;
	
	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  Map<String, Account> mapAccountParam
	 */
	public ISSM_Asset_cls(List<salesforce_ontap_account_asset_c__x> accountAssetsExtParam, Map<String, Account> mapAccountParam) {
		accountsAssets = getList(accountAssetsExtParam, mapAccountParam);
	}

	/**
	 * Class constructor		                             
	 */
	public ISSM_Asset_cls() {
	}

	/**
	 * Create Objects to sincronice		                             
	 * Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObject(Map<String, Account> mapAccount){
		accountsAssets = getList(null, mapAccount);
	}

	/**
	 * Create objects to delete		                             
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public void createObjectDelete(Map<String, Account> mapAccount){
		accountAssetsDelete = ISSM_AssetsExtern_cls.getList(mapAccount.keySet(), true);
	}

	/**
	 * Get a list of the new records result of the sincronization		                             
	 * @param  List<sObject> accountAssetsExt list of external objects
	 * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
	 */
	public List<sObject> getList(List<sObject> accountAssetsExt, Map<String, Account> mapAccount){
		List<ONTAP__Account_Asset__c> accountsAssetsReturn;
		ONTAP__Account_Asset__c acctAsset;
		Set<String> accountSap =mapAccount.keySet() ;
		accountsAssetsReturn = new List<ONTAP__Account_Asset__c>();
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByName().get('Serialized Asset').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByName().get('Activo serializado').getRecordTypeId();
		}
		if(test.isRunningTest()){
			for(Integer i = 0;i<15;i++){
				salesforce_ontap_account_asset_c__x assetext = new salesforce_ontap_account_asset_c__x(
					ontap_asset_code_c__c = '0003003117',
					isdeleted__c=false, 
					issm_sap_number_c__c='1029120920192',
					ontap_asset_description_c__c='prueba',
					ontap_asset_status_c__c='Activo',
					ontap_brand_c__c='prueeba',
					issm_sapcustomerid_c__c='000000000'+i,
					systemmodstamp__c = System.today());
				system.debug( '\n\n  ****** assetext = ' + assetext+'\n\n' );
				accountsAssetsReturn.add(getAsset(mapAccount, assetext, devRecordTypeId));
			}
		} else{
			for(SObject accountAssetExt: [SELECT systemmodstamp__c,issm_sapcustomerid_c__c,ontap_asset_description_c__c,
			 ontap_asset_code_c__c,ontap_asset_ownership_c__c, ontap_asset_status_c__c, ontap_brand_c__c, 
			ontap_cam_vendor_c__c, ontap_capacity_c__c,ontap_componentid_c__c, ontap_ibase_c__c,ontap_quantity_c__c,
			ontap_serial_number_c__c, ontap_warranty_date_c__c,   ontap_inventory_date_c__c, ontap_last_modified_date_sap_c__c, 
			ontap_latitude_c__c, ontap_longitude_c__c, ontap_manufacturer_c__c,ontap_manufacturer_serial_number_c__c 
			FROM salesforce_ontap_account_asset_c__x 
			where issm_sapcustomerid_c__c in :accountSap and systemmodstamp__c >= YESTERDAY]){
				accountsAssetsReturn.add(getAsset(mapAccount, accountAssetExt, devRecordTypeId));
			}
		}
		return accountsAssetsReturn;
	}

	/**
	 * Get asset record		                             
	 * @param  Map<String, Account> mapAccount
	 * @param  sObject accountAssetExt
	 * @param  Id devRecordTypeId
	 */
	private ONTAP__Account_Asset__c getAsset(Map<String, Account> mapAccount , sObject accountAssetExt, Id devRecordTypeId){
		ONTAP__Account_Asset__c acctAsset;
		acctAsset = new ONTAP__Account_Asset__c();
		try{
			acctAsset.ONTAP__Account__c = mapAccount.get(((salesforce_ontap_account_asset_c__x)accountAssetExt).issm_sapcustomerid_c__c).Id;
			acctAsset.ONTAP__Asset_Description__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_asset_description_c__c;
			acctAsset.ONTAP__Asset_Code__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_asset_code_c__c;
			acctAsset.ONTAP__Asset_Ownership__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_asset_ownership_c__c;
			acctAsset.ONTAP__Asset_Status__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_asset_status_c__c;
			acctAsset.ONTAP__Brand__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_brand_c__c;
			acctAsset.ONTAP__Cam_Vendor__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_cam_vendor_c__c;
			acctAsset.ONTAP__Capacity__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_capacity_c__c;
			acctAsset.ONTAP__ComponentId__c =((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_componentid_c__c;
			acctAsset.ONTAP__IBASE__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_ibase_c__c;
			acctAsset.ONTAP__Inventory_Date__c =((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_inventory_date_c__c;
			acctAsset.ONTAP__Last_Modified_Date_SAP__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_last_modified_date_sap_c__c;
			acctAsset.ONTAP__Latitude__c =((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_latitude_c__c;
			acctAsset.ONTAP__Longitude__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_longitude_c__c;
			acctAsset.ONTAP__Manufacturer__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_manufacturer_c__c;
			acctAsset.ONTAP__Manufacturer_serial_number__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_manufacturer_serial_number_c__c;
			acctAsset.ONTAP__Quantity__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_quantity_c__c;
			acctAsset.ONTAP__Serial_Number__c =((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_serial_number_c__c;
			acctAsset.ONTAP__Warranty_Date__c = ((salesforce_ontap_account_asset_c__x)accountAssetExt).ontap_warranty_date_c__c;
			acctAsset.ISSM_IsMigrated__c = true; // WP 20171002: Flag to determine if the source of the record is this upload process
			acctAsset.RecordTypeId = devRecordTypeId;
		} catch(NullPointerException ne){
			system.debug( '\n\n  ****** ne = ' + ne+'\n\n' );
		}
		return acctAsset;
	}

	/**
	 * Save the result of the sincronization process		                             
	 */
	public void save(){
		if(accountsAssets != null){
			try {
	        	upsert accountsAssets ONTAP__Asset_Code__c;
	    	} catch (DmlException e) {
	        	System.debug(e.getMessage());
	    	}

		}
	}

	/**
	 * Delete internal records that has the flag isdeleted on true		                             
	 */
	public Boolean deleteObjectsFinish(){
		if(accountAssetsDelete != null){
			Set<String> assetsSet = ISSM_AssetsExtern_cls.getSetExternalId(accountAssetsDelete);
			List<ONTAP__Account_Asset__c> toDeleteAsset_lst = getAssets(assetsSet);
			Boolean success = true;
			try{
				System.debug('### ISSM_Asset_cls: toDeleteAsset_lst size ('+toDeleteAsset_lst.size()+')'); 
				Database.DeleteResult[] drList = Database.delete(toDeleteAsset_lst);
				for(Database.DeleteResult dr : drList) {
				    success &= dr.isSuccess();
				}
				return success;
			} catch(Exception e){
				System.debug('### ISSM_Asset_cls: removeExistingAssets (DELETE FAILED)'); 
				return false;
			}
		}
		return false;
	}

	/**
	 * Get list of assets based on asset code		                             
	 * @param  Set<String> assetsSet
	 */
	private List<ONTAP__Account_Asset__c> getAssets(Set<String> assetsSet){
		return [SELECT Id FROM ONTAP__Account_Asset__c WHERE ONTAP__Asset_Code__c IN : assetsSet ];
	}
	
	/**
	 * Delete external records that has the flag isdeleted on true		                             
	 */
	public void deleteObjectsFinishExt(){

		if(accountAssetsDelete != null){
			try{
				Database.DeleteResult[] drListExt = Database.deleteAsync(accountAssetsDelete);
				system.debug( '\n\n  ****** drListExt = ' + drListExt +'\n\n' );
			} catch(Exception e){
				System.debug('### ISSM_Asset_cls: removeExistingAssets (DELETE FAILED)'); 
			}
		}

	}
}