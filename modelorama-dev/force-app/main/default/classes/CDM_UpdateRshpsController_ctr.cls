/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria.
Project:  ABInBev (CDM Rules)
Description: Class helper for Batch 'CDM_UpdateRelaionships_bch' and for 'CDM_UpdateAccountRshp_bch'
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       23-April-2018    Iván Neria. 			   Initial version
***********************************************************************************/
public class CDM_UpdateRshpsController_ctr {
    public static List<CDM_Temp_Taxes__c> lstTaxes ;
    public static List<CDM_Temp_Interlocutor__c> lstInterlocutors ;
    public static List<MDM_Account__c> lstAccounts ;
    
    public static List<CDM_Temp_Taxes__c> getTaxes(Set<String> lstKUNNR){
        if(lstTaxes==null){
                lstTaxes = [SELECT Id, KUNNR__c,TATYP__c FROM CDM_Temp_Taxes__c WHERE KUNNR__c IN :lstKUNNR LIMIT 24000];
        }System.debug('Size Tax = '+lstTaxes.size());
        return lstTaxes;
    }
    
    public static List<CDM_Temp_Interlocutor__c> getInterlocutors(Set<String> lstKUNNR, Set<String> lstORG){
        if(lstInterlocutors==null){
            lstInterlocutors = [SELECT Id, KUNNR__c, VTWEG__c, VKORG__c, SPART__c FROM CDM_Temp_Interlocutor__c WHERE (KUNNR__c IN: lstKUNNR AND VKORG__c IN: lstORG) LIMIT 25000];
        }
        System.debug('Size Int = '+lstInterlocutors.size());
        return lstInterlocutors;
    }
    
    public static List<MDM_Account__c> getMDMAccounts(List<String> lstKUNNR){
        if(lstAccounts==null){
            lstAccounts = [SELECT Id, PARVW__c, VTWEG__c, VKORG__c, SPART__c FROM MDM_Account__c WHERE PARVW__c IN: lstKUNNR];
        }
        return lstAccounts;
    }

}