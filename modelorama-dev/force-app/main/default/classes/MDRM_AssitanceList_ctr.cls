/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Controller for the Visualforce MDRM_Assistance_List_PDF 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       06/07/2018      Cindy Fuentes		           Controller for the Visualforce MDRM_Assistance_List_PDF 
==============================================================================================================================
*********************************************************************************************************************************/
public with sharing class MDRM_AssitanceList_ctr {
    public list <event> lstEvent {get;set;}
    public MDRM_Informative_Session__c session {get;set;}
    public string eventdate  {get;set;}   
    public integer numAssistants {get; set;}
    public list<account> lstAcc {get; set;}
    public string UENname {get;set;}
    public MDRM_AssitanceList_ctr(ApexPages.StandardController stdController) {
        
        // Select the Informative Session and it's events. 
        this.session = (MDRM_Informative_Session__c)stdController.getRecord();
        session =[select MDRM_UEN_Contact__c, MDRM_UEN_Name__r.name, mdrm_date__c, mdrm_start__c, mdrm_end__c from mdrm_informative_session__c 
                  where id =: session.id LIMIT 1];
        UENname = session.MDRM_UEN_Name__r.Name;
        lstEvent = [select activitydate, whatid, startdatetime, enddatetime 
                    from event where mdrm_informative_session__c =: session.id];
        
        // Get the Account from the Event and the required fields to display on the Visualforce Page.
        Set<Id> accountsids = new Set<Id>();
        
        for(Event e : [Select whatid from Event where mdrm_informative_session__c =: session.id])
        {
            accountsids.Add(e.whatid);
        }
        
        lstAcc = [select Id, Name, phone from account where ID IN : accountsids];
        
        // Get required date with the correct format and number of assistants. 
        Date thedate = date.newinstance(session.MDRM_Date__c.year(),session.MDRM_Date__c.month(), session.MDRM_Date__c.day());
        eventdate = string.valueof(thedate);
        numAssistants = lstEvent.size();
        id idsession = session.id;
        
    }
}