@isTest
private class ISSM_CaseSelector_tst {
	
	@isTest static void selectById() {

		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');
		ONTAP__Case_Force__c objCaseForce2 = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        Case objCase2 = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce2.Id);
        
		Set<Id> idSet = new Set<Id>();
		idSet.add(objCase.Id);
		idSet.add(objCase2.Id);

		Test.startTest();		
		List<Case> casos = ISSM_CaseSelector_cls.newInstance().selectById( idSet);
		Test.stopTest();
		
		system.assertEquals(2,casos.size());
	}
	
	@isTest static void selectByIdCaseId() {
		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        
		Test.startTest();		
		Case caso = ISSM_CaseSelector_cls.newInstance().selectById( objCase.Id);
		Test.stopTest();
		
		system.assertNotEquals(caso, null);
		system.assertEquals(caso.Id,objCase.Id);
	}

	@isTest static void selectByIdCaseIdString() {
		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        
		Test.startTest();		
		List<Case> casos = ISSM_CaseSelector_cls.newInstance().selectById( objCase.Id +'');
		Test.stopTest();
		
		system.assertNotEquals(casos, null);
		system.assertEquals(1,casos.size());
	}
	

	@isTest static void selectByIdMap() {

		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');
		ONTAP__Case_Force__c objCaseForce2 = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        Case objCase2 = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce2.Id);
        
		Set<Id> idSet = new Set<Id>();
		idSet.add(objCase.Id);
		idSet.add(objCase2.Id);

		Test.startTest();		
		Map<Id,Case> casos = ISSM_CaseSelector_cls.newInstance().selectByIdMap( idSet);
		Test.stopTest();
		
		system.assertEquals(2,casos.size());
	}

	@isTest static void selectByCaseNumber() {

		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');
		ONTAP__Case_Force__c objCaseForce2 = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        Case objCase2 = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce2.Id);
        
        Set<Id> idSet = new Set<Id>();
		idSet.add(objCase.Id);
		idSet.add(objCase2.Id);
		List<Case> caseObj = [select id, casenumber from case where id = : idSet];

		Set<String> numberSet = new Set<String>();
		for(Case item : caseObj){
			numberSet.add(item.caseNumber + '');
		}

		Test.startTest();		
		List<Case> casos = ISSM_CaseSelector_cls.newInstance().selectByCaseNumber( numberSet);
		Test.stopTest();
		
		system.assertEquals(2,casos.size());
	}

	@isTest static void selectByCaseForceNumber() {

		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');
		ONTAP__Case_Force__c objCaseForce2 = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        Case objCase2 = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce2.Id);
        
        List<Id> idSet = new List<Id>();
		idSet.add(objCaseForce.Id);
		idSet.add(objCaseForce.Id);
		
		Test.startTest();		
		List<Case> casos = ISSM_CaseSelector_cls.newInstance().selectByCaseForceNumber( idSet);
		Test.stopTest();
		
		system.assertEquals(2,casos.size());
	}

	@isTest static void selectByCaseNumberString() {

		ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');
		ONTAP__Case_Force__c objCaseForce2 = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open');

        ISSM_TriggerManager_cls.inactivate('ISSM_CaseTrigger_tgr');
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce.Id);
        Case objCase2 = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','Open',objCaseForce2.Id);
        
        Set<Id> idSet = new Set<Id>();
		idSet.add(objCase.Id);
		idSet.add(objCase2.Id);
		List<Case> caseObj = [select id, casenumber from case where id = : idSet];
		Case caso1 = [select id, casenumber from case where id = : objCase.Id Limit 1];

		Set<String> numberSet = new Set<String>();
		for(Case item : caseObj){
			numberSet.add(item.caseNumber + '');
		}

		Test.startTest();		
		List<Case> casos = ISSM_CaseSelector_cls.newInstance().selectByCaseNumber( numberSet);
		Case caso1result =  ISSM_CaseSelector_cls.newInstance().selectByCaseNumber( caso1.caseNumber + '');
		Case caso1resultFin = ISSM_CaseSelector_cls.newInstance().selectByCaseNumber( '' );
		Test.stopTest();
		
		system.assertEquals(2,casos.size());
		system.assertNotEquals(caso1result, null);
		system.assertEquals(caso1.Id,caso1result.Id);
		system.assertEquals(caso1resultFin,null);
	}
}