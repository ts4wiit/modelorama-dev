/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ISSM  AB Int Bev (OnTap)
Description:  
---------------------------------------------------------------------------------
Version		Date      Author              	Description
------ ---------- ---------------------------------------------------------------
1.0 	17-Julio-2017 Rodrigo RESENDIZ (RR)      Creator.
1.1     04-Oct  -2017 Rodrigo RESENDIZ (RR)      Multi category POCE adjustment.
***********************************************************************************/
@isTest
public class ISSM_SurveyTakenTgrHnd_tst {
    private static final String COMPLETED_SURVEY_STATUS = 'Completada';
    private static final String OPEN_SURVEY_STATUS = 'Abierta';
    private static final String CASE_TO_CLASSIFY_STATUS = 'Pending to Classify';
    private static Decimal PASSING_SCORE;
    private static final Id POCE_SURVEY_RECORD_TYPE_ID;
    private static final Id CASE_FORCE_RECORD_TYPE_ID;
     static{
        CASE_FORCE_RECORD_TYPE_ID = [select Id from RecordType where developerName='ISSM_SalesForceCase' limit 1].Id;
         POCE_SURVEY_RECORD_TYPE_ID = [select Id from RecordType where developerName='POCE' limit 1].Id; 
     }
    @testSetup
    private static void mockDataCreation(){
        Account account_obj = new Account(Name = 'Test1');
        insert account_obj;
        List<User> usuarios = new List<User>{
            new User(Username='test1@test.test.x', LastName='test', Email='test@test.test', Alias='tstx', CommunityNickname='tstxx', TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es_MX', EmailEncodingKey='ISO-8859-1', ProfileId=System.UserInfo.getProfileId(), LanguageLocaleKey='es')
            ,new User(Username='test2@test.test.y', LastName='test', Email='test@test.test', Alias='tsty', CommunityNickname='tstyy', TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es_MX', EmailEncodingKey='ISO-8859-1', ProfileId=System.UserInfo.getProfileId(), LanguageLocaleKey='es')
            ,new User(Username='test3@test.test.z', LastName='test', Email='test@test.test', Alias='tstz', CommunityNickname='tstzz', TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es_MX', EmailEncodingKey='ISO-8859-1', ProfileId=System.UserInfo.getProfileId(), LanguageLocaleKey='es')
            ,new User(Username='test4@test.test.zz', LastName='test', Email='test@test.test', Alias='tstzz', CommunityNickname='tstzzz', TimeZoneSidKey='America/Mexico_City', LocaleSidKey='es_MX', EmailEncodingKey='ISO-8859-1', ProfileId=System.UserInfo.getProfileId(), LanguageLocaleKey='es')
                };
        insert usuarios;
        List<ISSM_SurveyAssignmentCase__c> surveyAssignment_lst = new List<ISSM_SurveyAssignmentCase__c>{
            new ISSM_SurveyAssignmentCase__c(Name='EF1',ISSM_AccountTeamRole__c='Customer service',ISSM_Classification__c='Refrigeración',ISSM_Grouping__c='Frio'),
                new ISSM_SurveyAssignmentCase__c(Name='AGN1',ISSM_AccountTeamRole__c='Televentas',ISSM_Classification__c='Ventas',ISSM_Grouping__c='Disponibilidad'),
                new ISSM_SurveyAssignmentCase__c(Name='TMKT',ISSM_AccountTeamRole__c='Trade Marketing',ISSM_Classification__c='Trade marketing',ISSM_Grouping__c='Visibilidad'),
                new ISSM_SurveyAssignmentCase__c(Name='Passing Score',ISSM_AccountTeamRole__c='x',ISSM_Classification__c='x',ISSM_Grouping__c='x', ISSM_Value__c = 80)
                
        };
            insert surveyAssignment_lst;
        List<AccountTeamMember> aTeamMember_lst = new List<AccountTeamMember>{
            new AccountTeamMember(UserId=usuarios[0].Id,TeamMemberRole='Invoice',AccountId=account_obj.Id),
                new AccountTeamMember(UserId=usuarios[1].Id,TeamMemberRole='Trade Marketing',AccountId=account_obj.Id),
                new AccountTeamMember(UserId=usuarios[2].Id,TeamMemberRole='Televentas',AccountId=account_obj.Id),
                new AccountTeamMember(UserId=usuarios[3].Id,TeamMemberRole='Customer service',AccountId=account_obj.Id)
        };
        insert aTeamMember_lst;
        PASSING_SCORE = ISSM_SurveyAssignmentCase__c.getValues('Passing Score').ISSM_Value__c;
        ONTAP__Survey__c survey = new ONTAP__Survey__c(Name = 'POCE Survey',
                                                       RecordTypeId = POCE_SURVEY_RECORD_TYPE_ID,
                                                       ONTAP__Status__c='En Desarrollo',
                                                       ONTAP__Active_Begin_Date__c=System.today(),
                                                       ONTAP__HTML5_Bundle__c  = '069630000003mJmAAI',
                                                       ONTAP__Profiles__c = 'BDR Urban Center,System Administrator'
                                                      );
        insert survey;
        
        
        List<ONTAP__Survey_Question__c> question_lst = new List<ONTAP__Survey_Question__c>{
            new ONTAP__Survey_Question__c(ONTAP__Choices__c='5: Si\n1: No',ONTAP__OrderNumber__c=1,ONTAP__POCE_Weight__c=20,ONTAP__Question__c='Need Frio?',ONTAP__Survey__c = survey.Id,ONTAP__Type__c='Única Selección -- Vertical',ONTAP__POCE_Question_Grouping__c='Frio'),
                new ONTAP__Survey_Question__c(ONTAP__Choices__c='5: Si\n1: No',ONTAP__OrderNumber__c=2,ONTAP__POCE_Weight__c=20,ONTAP__Question__c='Need Frio Maint?',ONTAP__Survey__c = survey.Id,ONTAP__Type__c='Única Selección -- Vertical',ONTAP__POCE_Question_Grouping__c='Frio'),
                new ONTAP__Survey_Question__c(ONTAP__Choices__c='5: Si\n1: No',ONTAP__OrderNumber__c=3,ONTAP__POCE_Weight__c=20,ONTAP__Question__c='Need Menu?',ONTAP__Survey__c = survey.Id,ONTAP__Type__c='Única Selección -- Vertical',ONTAP__POCE_Question_Grouping__c='Disponibilidad'),
                new ONTAP__Survey_Question__c(ONTAP__Choices__c='5: Si\n1: No',ONTAP__OrderNumber__c=4,ONTAP__POCE_Weight__c=20,ONTAP__Question__c='Need stopper?',ONTAP__Survey__c = survey.Id,ONTAP__Type__c='Única Selección -- Vertical',ONTAP__POCE_Question_Grouping__c='Visibilidad')
                };
        insert question_lst;
        ONTAP__SurveyTaker__c sTaker = new ONTAP__SurveyTaker__c(ONTAP__Account__c = account_obj.Id,
                                                                ONTAP__Status__c = 'Abierta',
                                                                ONTAP__Survey__c = survey.Id);
        insert sTaker;
    }
    //test that when passing a POCE survey, cases will not be generated
    private static testMethod void testSuccessfulSurvey(){
        List<ONTAP__SurveyQuestionResponse__c> response = new List<ONTAP__SurveyQuestionResponse__c>();
        Account account_obj = [SELECT Id FROM Account WHERE Name = 'Test1'];
        ONTAP__Survey__c survey = [SELECT Id FROM ONTAP__Survey__c WHERE Name = 'POCE Survey' LIMIT 1];
        ONTAP__SurveyTaker__c sTaker = [SELECT Id FROM ONTAP__SurveyTaker__c WHERE ONTAP__Account__c= :account_obj.Id LIMIT 1];
        //have all passed questions
        for(ONTAP__Survey_Question__c question_obj : [SELECT Id FROM ONTAP__Survey_Question__c WHERE ONTAP__Survey__c =: survey.Id]){
            response.add(new ONTAP__SurveyQuestionResponse__c(ONTAP__Response__c='5: Si', ONTAP__Survey_Question__c = question_obj.Id, ONTAP__SurveyTaker__c= sTaker.Id, ONTAP__POCE_Score__c=20));
        }
        insert response; // insert responses
        sTaker.ONTAP__Status__c = 'Completada';
        sTaker.ONTAP__Total_Score__c = 100;
        update sTaker; // update the survey taken with the specified data
        System.assertEquals(100, sTaker.ONTAP__Total_Score__c ); //assert a passed POCE
        System.assertEquals(0, [SELECT COUNT() FROM ONTAP__Case_Force__c]); //assert no cases were generated
    }
    //test that when failing all the questions, 4 cases (as maximum) will be generated
    private static testMethod void testAllSectionFail(){
        List<ONTAP__SurveyQuestionResponse__c> response = new List<ONTAP__SurveyQuestionResponse__c>();
        Account account_obj = [SELECT Id FROM Account WHERE Name = 'Test1'];
        ONTAP__Survey__c survey = [SELECT Id FROM ONTAP__Survey__c WHERE Name = 'POCE Survey' LIMIT 1];
        ONTAP__SurveyTaker__c sTaker = [SELECT Id FROM ONTAP__SurveyTaker__c WHERE ONTAP__Account__c= :account_obj.Id LIMIT 1];
        //have all failed questions
        for(ONTAP__Survey_Question__c question_obj : [SELECT Id FROM ONTAP__Survey_Question__c WHERE ONTAP__Survey__c =: survey.Id]){
        	response.add(new ONTAP__SurveyQuestionResponse__c(ONTAP__Response__c='1: No', ONTAP__Survey_Question__c = question_obj.Id, ONTAP__SurveyTaker__c= sTaker.Id,ONTAP__POCE_Score__c=0));
        }
        insert response; //insert failing responses
        sTaker.ONTAP__Status__c = 'Completada';
        sTaker.ONTAP__Total_Score__c = 0;
        update sTaker;//uupdate survey taken with the specified data
        System.assertEquals(0, sTaker.ONTAP__Total_Score__c );
        System.assertEquals(3, [SELECT COUNT() FROM ONTAP__Case_Force__c]);//4 cases were created
    }
    //test that if there is an open case with the same Subject/Description, no cases should be added even if survey failed
    private static testMethod void testNonDuplicatedCase(){
        List<ONTAP__SurveyQuestionResponse__c> response = new List<ONTAP__SurveyQuestionResponse__c>();
        Account account_obj = [SELECT Id FROM Account WHERE Name = 'Test1'];
        ONTAP__Survey__c survey = [SELECT Id FROM ONTAP__Survey__c WHERE Name = 'POCE Survey' LIMIT 1];
        ONTAP__SurveyTaker__c sTaker = [SELECT Id FROM ONTAP__SurveyTaker__c WHERE ONTAP__Account__c= :account_obj.Id LIMIT 1];
        
        for(ONTAP__Survey_Question__c question_obj : [SELECT Id FROM ONTAP__Survey_Question__c WHERE ONTAP__Survey__c =: survey.Id]){
        	response.add(new ONTAP__SurveyQuestionResponse__c(ONTAP__Response__c='1: No', ONTAP__Survey_Question__c = question_obj.Id, ONTAP__SurveyTaker__c= sTaker.Id,ONTAP__POCE_Score__c=0));
        }
        insert response;
        sTaker.ONTAP__Status__c = 'Completada';
        sTaker.ONTAP__Total_Score__c = 0;
        update sTaker;
        System.assertEquals(0, sTaker.ONTAP__Total_Score__c );
        System.assertEquals(3, [SELECT COUNT() FROM ONTAP__Case_Force__c]);
        
        //create a new Survey
        sTaker = new ONTAP__SurveyTaker__c(ONTAP__Account__c = account_obj.Id,
                                                                ONTAP__Status__c = 'Abierta',
                                                                ONTAP__Survey__c = survey.Id);
        insert sTaker;
        response = new List<ONTAP__SurveyQuestionResponse__c>();
        for(ONTAP__Survey_Question__c question_obj : [SELECT Id FROM ONTAP__Survey_Question__c WHERE ONTAP__Survey__c =: survey.Id]){
        	response.add(new ONTAP__SurveyQuestionResponse__c(ONTAP__Response__c='1: No', ONTAP__Survey_Question__c = question_obj.Id, ONTAP__SurveyTaker__c= sTaker.Id,ONTAP__POCE_Score__c=0));
        }
        insert response;
        sTaker.ONTAP__Status__c = 'Completada';
        sTaker.ONTAP__Total_Score__c = 0;
        update sTaker;//insert a failing survey
        System.assertEquals(0, sTaker.ONTAP__Total_Score__c );
        //System.assertEquals(0, [SELECT COUNT() FROM ONTAP__Case_Force__c]); // number of generated cases must not change
    }
    private static testmethod void testTypificatedCase(){
        ONTAP__Case_Force__c casoForce_tst = new ONTAP__Case_Force__c();
        Account account_obj = [SELECT Id FROM Account WHERE Name = 'Test1'];
        casoForce_tst.recordtypeId = CASE_FORCE_RECORD_TYPE_ID;
        casoForce_tst.ISSM_ClassificationLevel2__c = 'Refrigeración';
        casoForce_tst.ISSM_ClassificationLevel3__c = 'Mantenimiento';
        casoForce_tst.ONTAP__Description__c = 'Desc';
        casoForce_tst.ONTAP__Subject__c = 'Subjct';
        casoForce_tst.ONTAP__Account__c = account_obj.Id;
        insert casoForce_tst;
     }
}