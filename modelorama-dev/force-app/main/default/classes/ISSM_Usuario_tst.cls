/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_Usuario_tst
Versión : 1.0
Fecha de Creación : 20 Abril 2018
Funcionalidad : Clase de prueba para el helper ISSM_Usuario_tst del trigger ISSM_Usuario_tgr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Abril - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_Usuario_tst {
    static testmethod void ManageTeamAccountByInactiveUser() {

        List<User> UsuarioUpd_lst = new List<User>();

        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
        insert user;

        Id RecordTypeId_id = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        Account acc = new Account(Name = 'Account 1', RecordTypeId = RecordTypeId_id);
        insert acc;

        AccountTeamMember atm = new AccountTeamMember(UserId = user.Id, AccountId = acc.Id, TeamMemberRole = 'Supervisor');
        insert atm;

        ISSM_ATM_Mirror__c atmMirror = new ISSM_ATM_Mirror__c(Usuario_relacionado__c = user.Id, Oficina_de_Ventas__c = acc.Id, Estatus__c = 'Activo');
        insert atmMirror;

        test.startTest();

            user.Alias = 'csavx';
            UsuarioUpd_lst.add(user);

            if (UsuarioUpd_lst.size() > 0) {
                update UsuarioUpd_lst;
            }

        test.stopTest();
    }
}