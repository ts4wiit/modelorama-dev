/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Clase de prueba para el controlador ISSM_StartPageAccount_ctr

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    ------------------    --------------------------   -----------------------------------
    1.0       05-Septiembre-2017    Luis Licona                  Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_StartPageAccount_tst {
	
	public static ONTAP__Route__c objONTAPRoute;
	public static ONTAP__Route__c objONTAPRoute2;
	public static VisitPlan__c objISSMVisitPlan;
	public static AccountByVisitPlan__c objISSMAccByVisitPlan3;
	public static VisitPlan__c objISSMVisitPlan2;
	public static Account objAccount;
	public static Account objAccountOrder;
	public static Account objAccount2;
	public static Account objAccount3;
	public static   ONTAP__Vehicle__c objONTAPVehicle2;
	public static VisitPlan__c objISSMVisitPlan3;
	public static AccountByRoute__c objAccxRoute;
	public static AccountByRoute__c objAccxRoute2;
	static ApexPages.StandardController sc{get;set;}
	static ApexPages.StandardController sc2{get;set;}
	static ApexPages.StandardController sc3{get;set;}
	static User u{get;set;}
	static User userTest{get;set;}
	
	static ONCALL__Call__c ObjCall{get;set;}//Llamadas

  	static void createData()
    {
		userTest = new User(
		    ProfileId = [SELECT Id FROM Profile WHERE Name = :'ISSM-SalesTeam'].Id,
		    LastName = 'Test',
		    Email = 'asdasdasd@amamama.com',
		    Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
		    CompanyName = 'TEST2',
		    Title = 'title',
		    Alias = 'alias',
		    TimeZoneSidKey = 'America/Los_Angeles',
		    EmailEncodingKey = 'UTF-8',
		    LanguageLocaleKey = 'en_US',
		    LocaleSidKey = 'en_US'
    	);
        insert userTest;
        
        u = new User(
         	ProfileId = [SELECT Id FROM Profile WHERE Name = : 'ISSM-SalesTeam'].Id,
         	LastName = 'last',
         	Email = 'puser000@amamama.com',
         	Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         	CompanyName = 'TEST',
         	Title = 'title',
         	Alias = 'alias',
         	TimeZoneSidKey = 'America/Los_Angeles',
         	EmailEncodingKey = 'UTF-8',
         	LanguageLocaleKey = 'en_US',
         	LocaleSidKey = 'en_US',
         	managerId = userTest.Id
        );
        insert u;
        
        List<RecordType> lstRT = [SELECT Description,
        								DeveloperName,
        								Id,
        								Name,
        								SobjectType 
        								FROM RecordType 
        								WHERE SobjectType = 'Account' 
        								OR SobjectType = 'ONTAP__Route__c'];
        
        Map<String, RecordType> mapRT = new Map<String, RecordType>();
        for(RecordType rt : lstRT)
            mapRT.put(rt.DeveloperName, rt);
            
        objAccount3                         = new Account();
        objAccount3.RecordTypeId            = mapRT.get('SalesOrg').Id;
        objAccount3.Name                    = 'CMM Test3';
        objAccount3.ONTAP__SalesOgId__c     = '3117';
        objAccount3.ISSM_CheckOut__c        = Time.newInstance(1, 2, 3, 4);

        insert objAccount3;

        objAccount2                      = new Account();
        objAccount2.RecordTypeId         = mapRT.get('SalesOffice').Id;
        objAccount2.Name                 = 'CMM Test';
        objAccount2.ONTAP__SalesOffId__c = 'FZ08';
        objAccount2.ISSM_SalesOrg__c     = objAccount3.id;
        insert objAccount2;

        objAccount                             = new Account();
        objAccount.RecordTypeId                = mapRT.get('Account').Id;
        objAccount.Name                        = 'CMM Dolores FZ08';
        objAccount.ISSM_SalesOffice__c         = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c      = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c = null;
        objAccount.ONTAP__SalesOgId__c         = '3117';
        objAccount.ONTAP__SalesOffId__c        = 'FZ08';
        objAccount.ONTAP__SAP_Number__c        = '0100169817';
        objAccount.ONTAP__Main_Phone__c        = '5510988765';
		objAccount.ISSM_MainContactE__c        = true;
		objAccount.ONTAP__ChannelId__c         = '01';
		objAccount.ONCALL__Ship_To_Name__c     = '0100169817';
		objAccount.ISSM_StartCall__c 		   = true;
        objAccount.ONCALL__OnCall_Route_Code__c = 'FG0050';
        insert objAccount;
		ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
		Id RecTypeCall   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
		ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = objAccount.Id,
            RecordTypeId = RecTypeCall,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        objONTAPRoute2                       = new ONTAP__Route__c();
        objONTAPRoute2.RecordTypeId       	 = mapRT.get('Sales').Id;
        objONTAPRoute2.ServiceModel__c       = Label.ISSM_OriginCall;
        objONTAPRoute2.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute2.RouteManager__c       = userTest.Id;
        objONTAPRoute2.Supervisor__c         = userTest.id;
        insert objONTAPRoute2;

        sc  = new ApexPages.standardController(ObjAccount);
		sc2 = new ApexPages.standardController(ObjAccount2);
		sc3 = new ApexPages.standardController(ObjAccount3);	
	

	    objAccountOrder                            = new Account();
        objAccountOrder.RecordTypeId                = mapRT.get('Account').Id;
        objAccountOrder.Name                        = 'CMM Dolores FZ08';
        objAccountOrder.ISSM_SalesOffice__c         = objAccount2.Id;
        objAccountOrder.ONCALL__Order_Block__c      = null;
        objAccountOrder.ONCALL__SAP_Deleted_Flag__c = null;
        objAccountOrder.ONTAP__SalesOgId__c         = '3117';
        objAccountOrder.ONTAP__SalesOffId__c        = 'FZ08';
        objAccountOrder.ONTAP__SAP_Number__c        = '0100169817';
        objAccountOrder.ONTAP__Main_Phone__c        = '5510988765';
		objAccountOrder.ISSM_MainContactE__c        = true;
		objAccountOrder.ONTAP__ChannelId__c         = '01';
		objAccountOrder.ONCALL__Ship_To_Name__c     = '0100169817';
		objAccountOrder.ISSM_StartCall__c 		   = true;
        objAccountOrder.ONCALL__OnCall_Route_Code__c = 'FG0050';
        insert objAccountOrder;

List<ONTAP__Order__c> lstOrder = new List<ONTAP__Order__c>();
		ONTAP__Order__c ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = objAccount.Id, 
            ONCALL__Origin__c         = 'AUT', //Autoventa,        
            ONCALL__SAP_Order_Number__c   = '111111',
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus3,
            ONTAP__OrderAccount__c = ObjAccount2.Id
            
        );
        lstOrder.add(ObjOrder);
        ONTAP__Order__c ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = objAccount.Id, 
          	ONCALL__Origin__c         = 'PRE', //preventa
            ONCALL__SAP_Order_Number__c   = '1111112',         
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus3,
            ONTAP__OrderAccount__c = ObjAccount2.Id
        );
        lstOrder.add(ObjOrder2);
        
        ONTAP__Order__c ObjOrder3 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = objAccount.Id, 
            ONCALL__Origin__c         = 'B2B-0000', //mi modelo ,
            ONCALL__SAP_Order_Number__c   = '1111113'  ,         
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus3,
             ONTAP__OrderAccount__c = ObjAccount2.Id,
             CreatedDate = Date.today()
        );
        lstOrder.add(ObjOrder3);
        
        ONTAP__Order__c ObjOrder4 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = ObjAccount2.Id, 
           	ONCALL__Origin__c         = 'REP', //'B2B-0000',  //REPARTO  
            ONCALL__SAP_Order_Number__c   = '1111115',      
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus2,
             ONTAP__OrderAccount__c = ObjAccount2.Id
        );
        lstOrder.add(ObjOrder4); 
        //insert lstOrder;
        Datetime TodayNow = Datetime.now();
       insert lstOrder;

        System.debug('QUERYY : ' + [SELECT ONCALL__OnCall_Account__c,
                        ONCALL__Call__c,
                        ONTAP__OrderAccount__c,
                        ISSM_OrderOrigin__c,
                        ONCALL__SAP_Order_Number__c
                FROM   ONTAP__Order__c
                WHERE  (ONCALL__OnCall_Account__c =: objAccount.Id
                        OR ONTAP__OrderAccount__c =: objAccount.Id)
                AND ONTAP__OrderStatus__c !=: Label.ISSM_CanceledStatus
                AND CreatedDate = TODAY] ); 
        
		//Test.setCreatedDate(lstOrder[0].Id, TodayNow);
		//Test.setCreatedDate(lstOrder[1].Id, TodayNow);
		//Test.setCreatedDate(lstOrder[2].Id, TodayNow);
		//Test.setCreatedDate(lstOrder[3].Id, TodayNow);
		//System.debug('******* ObjOrder : '+[Select id,name,CreatedDate,ONCALL__OnCall_Account__r.Id,ONCALL__OnCall_Account__c FROM   ONTAP__Order__c
  //              WHERE  (ONCALL__OnCall_Account__c =: objAccount2.Id
  //                      OR ONTAP__OrderAccount__c =: objAccount2.Id)
  //                    	AND ONTAP__OrderStatus__c !=: Label.ISSM_CanceledStatus
  //                    	AND CreatedDate = TODAY
  //             ]);

  	}

	/*@isTest static void DataComplete() {
		createData();
		
		//System.runAs(u){	
		//	ISSM_StartPageAccount_ctr CTR = new ISSM_StartPageAccount_ctr(sc2);
		//	PageReference pr = CTR.startPage();

  //          //ISSM_ValidateRequestedFields_cls CTRVR = new ISSM_ValidateRequestedFields_cls();
  //          //List<Account> Acc_lst = [SELECT Id, RecordTypeId FROM Account WHERE Name = 'CMM Dolores FZ08' LIMIT 1];
  //          //CTRVR.checkCallsUniversalTelesales(Acc_lst[0].Id,Acc_lst[0].RecordTypeId);
		//	//	System.assertEquals(true,CTR.BlnHaveOrder);
		//}
		
		System.runAs(userTest){
			ISSM_StartPageAccount_ctr CTR = new ISSM_StartPageAccount_ctr(sc);
			CTR.LstMissedFields  = new List<String>();
			//PageReference pr = CTR.startPage();
			//	System.assertEquals(false,CTR.BlnHaveOrder);
		}
			
	}
*/
	@isTest static void OnlyStartPage() {
		createData();
		ISSM_StartPageAccount_ctr CTR = new ISSM_StartPageAccount_ctr(sc3);
		PageReference pr = CTR.startPage();
	}
}