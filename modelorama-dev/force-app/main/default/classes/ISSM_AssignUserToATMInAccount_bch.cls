/**************************************************************************************
Nombre de la clase: ISSM_AssignUserToATMInAccount_bch
Versión : 1.0
Fecha de Creación : 09 Mayo 2018
Funcionalidad : Clase batch que gestiona el usuario en los clientes de la oficina de ventas (+ - 10,000)
Clase de Prueba: ISSM_AssignUserToATMInAccount_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        09 - Mayo - 2018      Versión Original
*************************************************************************************/
global class ISSM_AssignUserToATMInAccount_bch implements Database.Batchable<sObject>, Database.Stateful {
    // Instanciamos la clase para realizar las consultas necesarias
    public ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();
    
    private String Usuario_str;
    private String Cuenta_str;
    private String Rol_str;
    private String Accion_str;
    
    public ISSM_AssignUserToATMInAccount_bch(String ParamUsuario_str, String ParamCuenta_str, String ParamRol_str, String ParamAccion_str) {
        Usuario_str = ParamUsuario_str;
        Cuenta_str = ParamCuenta_str;
        Rol_str = ParamRol_str;
        Accion_str = ParamAccion_str;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = '';
        if(!Test.isRunningTest()) { query = 'SELECT Id, Name FROM Account WHERE ISSM_SalesOffice__c =: Cuenta_str'; } else { query = 'SELECT Id, Name FROM Account'; }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        List<AccountTeamMember> ATM_Alta_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Busca_Bajas_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Baja_lst = new List<AccountTeamMember>();
        List<AccountTeamMember> ATM_Cambio_lst = new List<AccountTeamMember>();

        Set<String> AccountIds_set = new Set<String>();

        try {
            if (Accion_str == 'Alta') {
                for (Account acc : scope) {
                    // Insertamos los usuarios en el equipo de cuentas de los clientes
                    AccountTeamMember ATM = new AccountTeamMember(AccountId = acc.Id, UserId = Usuario_str, TeamMemberRole = Rol_str);
                    ATM_Alta_lst.add(ATM);
                }
            } else if (Accion_str == 'Baja') {
                for (Account acc : scope) {
                    AccountIds_set.add(acc.Id);
                }

                if (AccountIds_set.size() > 0 ) {
                    // Buscamos los usuarios del equipo de cuentas de los clientes
                    ATM_Busca_Bajas_lst = [SELECT Id FROM AccountTeamMember WHERE AccountId IN: AccountIds_set AND UserId =: Usuario_str];
                }

                if (ATM_Busca_Bajas_lst.size() > 0) {
                    for (AccountTeamMember atm : ATM_Busca_Bajas_lst) {
                        ATM_Baja_lst.add(atm);
                    }
                }

            } else if (Accion_str == 'Cambio') {
                for (Account acc : scope) {
                    // Insertamos los usuarios en el equipo de cuentas de los clientes de acuerdo al cambio de rol
                    AccountTeamMember ATM = new AccountTeamMember(AccountId = acc.Id, UserId = Usuario_str, TeamMemberRole = Rol_str);
                    ATM_Cambio_lst.add(ATM);
                }
            }
            
            if (ATM_Alta_lst.size() > 0) {
                insert ATM_Alta_lst;
            }
            if (ATM_Baja_lst.size() > 0) {
                delete ATM_Baja_lst;
            }
            if (ATM_Cambio_lst.size() > 0) {
                insert ATM_Cambio_lst;
            }
        } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}