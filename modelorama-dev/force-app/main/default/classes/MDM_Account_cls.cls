/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ABInBev (MDM Rules)
Description: Class to extract catalogs / picklist values
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-Enero-2018   Rodrigo RESENDIZ (RR)   Initial version
*/
public class MDM_Account_cls {
    @TestVisible private static Map<String, Map<String,MDM_Parameter__c>> catalogElements;
    @TestVisible private static Map<String, Map<String,MDM_Parameter__c>> catalogElementsByDescription;
    @TestVisible private static Map<String, DistributionChannel__c> distributionChannel;
    @TestVisible private static Map<String, SupplierCenter__c> supplierCenter;
    @TestVisible private static Map<String, Sector__c> sector;
    @TestVisible private static Map<String, GR_Sales_Team__c> salesTeam;
    @TestVisible private static Map<String, MDRM_Modelorama_Postal_Code__c> postalCodes;
    @TestVisible private static Map<String, Account> salesOffice;
    @TestVisible private static Map<String, Account> regionalsalesD;
    @TestVisible private static Map<String, Account> salesOrg;
    @TestVisible private static Map<String,Map<String,Set<String>>> structure;
    @TestVisible private static Map<String,Id> accountRecordTypes;
    @TestVisible private static Map<String,Map<String, MDM_Rule__c>> rulesToApply;
    @TestVisible private static Map<String,MDM_AccountFieldsMapping__mdt> accountMapping;
    @TestVisible private static Map<String,MDM_Temp_Account__c> mapKUNNR;
    @TestVisible private static Set<String> salesAreaGranSet;
    @TestVisible private static Set<String> AdmCenterGranSet;
    @TestVisible private static Set<String> SalesTeamGranSet;
    @TestVisible private static Set<String> GRSalesTeamGranSet;
    @TestVisible private static Set<String> supplierCenterSet;
    @TestVisible public static Set<String> accIds;
    @TestVisible public static Set<String> setZ019;
    @TestVisible public static Set<String> KUNN2r25;
    @TestVisible private static Set<String> setKUNN2;
    @TestVisible private static Set<String> setKALKSValues;
    @TestVisible private static Map<String, Map<String,Id>> sObjectCatalog;
    @TestVisible public static Map<String, List<SalesArea__c>> salesAreas;
    @TestVisible public static Map<String, CDM_Temp_Interlocutor__c> interlocutors;
    @TestVisible public static Map<String, MDM_Temp_Account__c> mapR0028;
    @TestVisible public static Map<String, CDM_Temp_Interlocutor__c> mapInterlocutorsR19;
	@TestVisible private static Set<String> setKUNNRR19;
    @TestVisible private static Set<String> setZ001;
    
    public static Map<String, List<SalesArea__c>> getSalesAreas (Set<String> salesOffice_lst){
        if(salesAreas == null){
            salesAreas = new Map<String, List<SalesArea__c>>();
            for(SalesArea__c sa : [SELECT Id, VTWEG__c, VKBUR__c, VKORG__c, SPART__c FROM SalesArea__c WHERE VKBUR__c IN:salesOffice_lst]){
                if(!salesAreas.containsKey(sa.VKBUR__c))
                	salesAreas.put(sa.VKBUR__c, new List<SalesArea__c>{sa});
                else
                    salesAreas.get(sa.VKBUR__c).add(sa);
            }
            return salesAreas;
        }else{
            Set<String> toSearch_lst = new Set<String>();
            for(String toSearch : salesOffice_lst){
                if(!salesAreas.containsKey(toSearch)){
                    toSearch_lst.add(toSearch);
                }
            }
            if(!toSearch_lst.isEmpty()){
                for(SalesArea__c sa : [SELECT Id, VTWEG__c, VKBUR__c, VKORG__c, SPART__c FROM SalesArea__c WHERE VKBUR__c IN:toSearch_lst]){
                    if(!salesAreas.containsKey(sa.VKBUR__c))
                        salesAreas.put(sa.VKBUR__c, new List<SalesArea__c>{sa});
                    else
                        salesAreas.get(sa.VKBUR__c).add(sa);
                }
                return salesAreas;
            }else{ 
                return salesAreas;
            }
        }
    }
    public static Set<String> getGRSalesTeam(){
        if(GRSalesTeamGranSet==null){
            GRSalesTeamGranSet = new Set<String>();
            for(GR_Sales_Team__c sa : [SELECT Id,Code__c FROM GR_Sales_Team__c ]){
                GRSalesTeamGranSet.add(sa.Code__c);
            }
            return GRSalesTeamGranSet;
        }else{ return GRSalesTeamGranSet;
        }
    }
    public static Set<String> getKALKSvalues(){
        if (setKALKSValues == null){
            setKALKSValues = new Set<String>{Label.CDM_Apex_Param1, Label.CDM_Apex_Param2, Label.CDM_Apex_Param3, Label.CDM_Apex_Param4};
        }
        return setKALKSValues;
    }
    public static Set<String> getKUNN2(Set<String> stK2){
        if(setKUNN2==null){
            setKUNN2 = new Set<String>();
            for(MDM_Temp_Account__c sa : [SELECT Id,KUNN2__c FROM MDM_Temp_Account__c WHERE KTOKD__c=:Label.CDM_Apex_Z001 AND KUNN2__c IN:stK2]){
                setKUNN2.add(sa.KUNN2__c);
            }
            return setKUNN2;
        }else{
            return setKUNN2;
        }
    }
    public static Map<String, CDM_Temp_Interlocutor__c> getKUNN2R19(Set<String> stK2){
       if(mapInterlocutorsR19==null){
           mapInterlocutorsR19 = new Map<String, CDM_Temp_Interlocutor__c>();
           for(CDM_Temp_Interlocutor__c sa : [SELECT Id,KUNN2__c,KUNNR__c FROM CDM_Temp_Interlocutor__c WHERE KUNN2__c IN:stK2 AND PARVW__c=:Label.CDM_Apex_SH]){
               if(sa.KUNN2__c != sa.KUNNR__c){  
                   mapInterlocutorsR19.put(sa.Id, sa);  
               } 
           }
           return mapInterlocutorsR19;
       }else{
           return mapInterlocutorsR19;
       }
   }
   public static Set<String> getKUNNRR19(Map<String, CDM_Temp_Interlocutor__c> mpKRR19){
     	if(setKUNNRR19==null){
           setKUNNRR19 = new Set<String>();
           set<String> setHelper = new set<String>();
           for(CDM_Temp_Interlocutor__c mpHelper:mpKRR19.values()){
               setHelper.add(mpHelper.KUNNR__c);
           }
           for(MDM_Temp_Account__c sa : [SELECT Id,PARVW__c FROM MDM_Temp_Account__c WHERE KTOKD__c=:Label.CDM_Apex_Z001 AND PARVW__c IN:setHelper ]){
               setKUNNRR19.add(sa.PARVW__c);
           }
           return setKUNNRR19;
       }else{
           return setKUNNRR19;
       }  
   }
    public static Set<String> getZ001(Set<String> setPARVW){
     	if(setZ001==null){
           setZ001 = new Set<String>();
           for(MDM_Temp_Account__c sa : [SELECT Id,PARVW__c,AUFSD__c,LIFSD__c,FAKSD__c FROM MDM_Temp_Account__c WHERE KTOKD__c=:Label.CDM_Apex_Z001 AND AUFSD__c=:Label.CDM_Apex_01 AND LIFSD__c=:Label.CDM_Apex_01 AND FAKSD__c=:Label.CDM_Apex_01 AND PARVW__c IN: setPARVW]){
               setZ001.add(sa.PARVW__c);
           }
           return setZ001;
       }else{
           return setZ001;
       }  
   }
    public static Map<String, MDM_Temp_Account__c> getMapKUNNR(Set<String> stKR){
        if(mapKUNNR==null){
         mapKUNNR = new Map<String,MDM_Temp_Account__c>();   
        for(MDM_Temp_Account__c ss :[SELECT Id, VKORG__c, AUFSD__c, LIFSD__c,FAKSD__c,PARVW__c FROM MDM_Temp_Account__c WHERE PARVW__c IN:stKR AND KTOKD__c=:Label.CDM_Apex_Z019 ]){
    		mapKUNNR.put(ss.PARVW__c+'-'+ss.VKORG__c, ss);
			}    
        }
		
        return mapKUNNR;
    }
    public static Set<String> getSalesAreaGranularity(Set<String> lstSAGranularity){
        if(salesAreaGranSet==null){
            salesAreaGranSet = new Set<String>();
            for(SalesArea__c sa : [SELECT Id,Id_External__c FROM SalesArea__c WHERE Id_External__c IN:lstSAGranularity]){
                salesAreaGranSet.add(sa.Id_External__c);
            }
            return salesAreaGranSet;
        }else{
            return salesAreaGranSet;
        }
    }
    public static Set<String> getAdmCenterGranularity(Set<String> setACenterGranularity){
        if(AdmCenterGranSet==null){
            AdmCenterGranSet = new Set<String>();
            for(AdmissibleCenter__c sa : [SELECT Id,Id_External__c FROM AdmissibleCenter__c WHERE Id_External__c IN:setACenterGranularity]){
                AdmCenterGranSet.add(sa.Id_External__c);
            }
            return AdmCenterGranSet;
        }else{
            return AdmCenterGranSet;
        }
    }
    public static Set<String> getSalesTeamGranularity(Set<String> setSTGranularity){
        if(SalesTeamGranSet==null){
            SalesTeamGranSet = new Set<String>();
            for(SalesTeambySalesOffice__c sa : [SELECT Id,Id_External__c FROM SalesTeambySalesOffice__c WHERE Id_External__c IN:setSTGranularity]){
                SalesTeamGranSet.add(sa.Id_External__c);
            }
            return SalesTeamGranSet;
        }else{
            return SalesTeamGranSet;
        }
    }
    public static Map<String, CDM_Temp_Interlocutor__c> getInterlocutors(Set<String> setBP, String intFunction){
        if(interlocutors==null){
            interlocutors = new Map<String, CDM_Temp_Interlocutor__c>();
            KUNN2r25 = new Set<String>();
            for(CDM_Temp_Interlocutor__c it : [SELECT Id,KUNNR__c,KUNN2__c,VKORG__c, SPART__c,VTWEG__c, MDM_Temp_Account__c FROM CDM_Temp_Interlocutor__c WHERE PARVW__c=:intFunction AND MDM_Temp_Account__c IN:setBP]){
                interlocutors.put(String.valueof(it.Id), it);
                KUNN2r25.add(it.KUNN2__c);
            }
            return interlocutors;
        }else{
            return interlocutors;
        }
    }
    public static Set<String> getKUNN2r25(){
        return KUNN2r25;
    }
    public static Set<String> isZ019(Set<String> KUNN2){
        if(setZ019==null){
         	setZ019 = new Set<String>();
        		for(MDM_Temp_Account__c result: [SELECT Id,PARVW__c FROM MDM_Temp_Account__c WHERE PARVW__c IN:KUNN2 AND KTOKD__c =:Label.CDM_Apex_Z019 ]){
            		setZ019.add(result.PARVW__c);
        		}  
            return setZ019;
        } else{
          return setZ019;  
        }
    }
    public static Map<String, MDM_Temp_Account__c> getR0028(Set<String> KUNN2){
        if(mapR0028==null){
         	mapR0028 = new Map<String, MDM_Temp_Account__c>();
        		for(MDM_Temp_Account__c result: [SELECT Id,PARVW__c,KONDA__c,KVGR5__c,KVGR3__c,KDGRP__c,PLTYP__c, Id_External__c FROM MDM_Temp_Account__c WHERE PARVW__c IN:KUNN2 AND (KTOKD__c =:Label.CDM_Apex_Z019 OR KTOKD__c=:Label.CDM_Apex_Z010) ]){
            		mapR0028.put(result.Id, result);
        		}  
            return mapR0028;
        } else{
          return mapR0028;  
        }
    }
    public static Set<String> validationReprocess (List<String> tmpAccId){
        if(accIds==null){
         	accIds = new Set<String>();
        		for(MDM_Validation_Result__c result: [SELECT Id, Code__c, MDM_Temp_AccountId__c FROM MDM_Validation_Result__c WHERE (Code__c=:Label.CDM_Apex_CodeRule8 OR Code__c=:Label.CDM_Apex_CodeRule14) AND MDM_Temp_AccountId__c IN :tmpAccId]){
            		accIds.add(result.MDM_Temp_AccountId__c);
        		}  
            return accIds;
        } else{
          return accIds;  
        }
    }
    public static Map<String,MDM_AccountFieldsMapping__mdt> getAccountMappingLst(){
        if(accountMapping==null){
            accountMapping = new Map<String,MDM_AccountFieldsMapping__mdt>();
            for(MDM_AccountFieldsMapping__mdt accMap : [SELECT Catalog__c, Field_Api_Name__c,MasterLabel, Related_Object_Field__c, Related_Object_Name__c FROM MDM_AccountFieldsMapping__mdt]){
                accountMapping.put(accMap.Field_Api_Name__c,accMap);
            }
            return accountMapping;
        }else{
            return accountMapping;
        }
    }

    /**** NEW ******/
    public static Map<String, MDM_Rule__c> getRulesToApply(String objectName){
        Map<String, MDM_Rule__c> tmpRules = new Map<String, MDM_Rule__c>();
        if(rulesToApply==null){
            rulesToApply = new Map<String,Map<String, MDM_Rule__c>>();
            for(MDM_Rule__c rule : [SELECT Id, MDM_Code__c, MDM_Rule_Description__c, MDM_IsActive__c, MDM_Error_Message__c, MDM_Object__c, (SELECT MDM_IsActive__c,MDM_APIFieldName__c FROM MDM_Fields_by_Rule__r) FROM MDM_Rule__c WHERE MDM_Object__c=:objectName AND MDM_IsActive__c=true]){
                tmpRules.put(rule.MDM_Code__c,rule);
            }
            rulesToApply.put(objectName, tmpRules);
            return rulesToApply.get(objectName);
        }else{
            if(rulesToApply.containsKey(objectName)){
                return rulesToApply.get(objectName);
            }else{
                for(MDM_Rule__c rule : [SELECT Id, MDM_Code__c, MDM_Rule_Description__c, MDM_IsActive__c, MDM_Error_Message__c, MDM_Object__c, (SELECT MDM_IsActive__c,MDM_APIFieldName__c FROM MDM_Fields_by_Rule__r) FROM MDM_Rule__c WHERE MDM_Object__c=:objectName AND MDM_IsActive__c=true]){
                    tmpRules.put(rule.MDM_Code__c,rule);
                }
                rulesToApply.put(objectName, tmpRules);
                return rulesToApply.get(objectName);
            }
        }
    }
    public static Map<String, Map<String,MDM_Parameter__c>> getInstance(){
        if(catalogElements == null){
            catalogElements = new Map<String, Map<String, MDM_Parameter__c>>();
            for(MDM_Parameter__c param_obj : [SELECT Id, Catalog__c, Code__c FROM MDM_Parameter__c WHERE Catalog__c <> null]){
                if(catalogElements.containsKey(param_obj.Catalog__c)){
                    catalogElements.get(param_obj.Catalog__c).put(param_obj.Code__c, param_obj);
                }else{
                    catalogElements.put(param_obj.Catalog__c, new Map<String, MDM_Parameter__c>{param_obj.Code__c => param_obj});
                }
            }
            return catalogElements;
        }else{
            return catalogElements;
        }
    }
    
    public static Map<String, Map<String,MDM_Parameter__c>> getInstanceByDescription(){
        if(catalogElementsByDescription == null){
            catalogElementsByDescription = new Map<String, Map<String, MDM_Parameter__c>>();
            for(MDM_Parameter__c param_obj : [SELECT Id, Catalog__c, Description__c, Code__c  FROM MDM_Parameter__c WHERE Catalog__c <> null]){
                if(catalogElementsByDescription.containsKey(param_obj.Catalog__c)){
                    catalogElementsByDescription.get(param_obj.Catalog__c).put(param_obj.Description__c, param_obj);
                }else{
                    catalogElementsByDescription.put(param_obj.Catalog__c, new Map<String, MDM_Parameter__c>{param_obj.Description__c => param_obj});
                }
            }
            return catalogElementsByDescription;
        }else{
            return catalogElementsByDescription;
        }
    }
    public static Map<String,Id> getAccountRT(){
        if(accountRecordTypes==null){
            accountRecordTypes = new Map<String,Id>();
            for(RecordType rt : [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE SobjectType=:Label.CDM_Apex_MDM_Account_c]){
                accountRecordTypes.put(rt.DeveloperName, rt.Id);
            }
            return accountRecordTypes;
        }else{
            return accountRecordTypes;
        }        
    }
    //no
    public static Map<String,Map<String,Set<String>>> getStructure(){
        if(structure == null){
            structure = new Map<String,Map<String,Set<String>>>();
            for(Account drv: getRegionalSalesDivision().values()){
                for(Account sOrg: getSalesOrg().values()){
                    if(sOrg.ISSM_ParentAccount__c == drv.ISSM_ParentAccount__c){
                        for(Account sOff: getSalesOffice().values()){
                            if(sOrg.ISSM_ParentAccount__c == sOff.ISSM_ParentAccount__c){
                                if(!structure.containsKey(drv.Id)){
                                    structure.put(drv.Id, new Map<String,Set<String>>{sOrg.Id => new Set<String>{sOff.Name}});
                                }else{
                                    if(structure.get(drv.Id)==null || !structure.get(drv.Id).containsKey(sOrg.Id)){
                                        if(structure.get(drv.Id).get(sOrg.Id)==null || !structure.get(drv.Id).get(sOrg.Id).contains(sOff.Name)){
                                            structure.get(drv.Id).put(sOrg.Id, new Set<String>{sOff.Name});
                                        }else{
                                            structure.get(drv.Id).get(sOrg.Id).add(sOff.Name);
                                        }
                                    }else{
                                        structure.get(drv.Id).put(sOrg.Id, new Set<String>{sOff.Name});
                                    }
                                }
                                
                            }
                        }
                    }
                }
            } 
            return structure; 
        }else{
        	return structure; 
        }
    }

    public static Map<String,Id> getCatalogObject(String sObjectName, String filterField, List<String> parameters){
        if(sObjectCatalog == null){
            Map<String, Id> tempMap = new Map<String, Id>();
            sObjectCatalog = new Map<String,Map<String, Id>>();
            String query = 'SELECT Id, '+filterField+' FROM '+sObjectName+' WHERE '+filterField+' IN (';
            for(String param : parameters){
                query += '\''+ param + '\',';
            }
            query = (query.substring(0,query.length()-1))+')';
            for(SObject tmpObj : Database.query(query)){ tempMap.put((String)tmpObj.get(filterField), (Id)tmpObj.get('Id')); }
            sObjectCatalog.put(sObjectName, tempMap);
            return sObjectCatalog.get(sObjectName);
        }else{
            if(!sObjectCatalog.containsKey(sObjectName)){
                Map<String, Id> tempMap = new Map<String, Id>();
                String query = 'SELECT Id, '+filterField+' FROM '+sObjectName+' WHERE '+filterField+' IN (';
                for(String param : parameters){
                    if(param != null)
                    query += '\''+param + '\',';
                }
                query = (query.substring(0,query.length()-1))+')';
                for(SObject tmpObj : Database.query(query)){ tempMap.put((String)tmpObj.get(filterField),(Id)tmpObj.get('Id'));
                }
                sObjectCatalog.put(sObjectName, tempMap);
                return sObjectCatalog.get(sObjectName);
            }else{
                return sObjectCatalog.get(sObjectName);
            }
        }
    }
    //
    public static Map<String,Id> getCatalogObjectCity2(String sObjectName, String filterField, List<String> parameters){
        if(sObjectCatalog == null){
            Map<String, Id> tempMap = new Map<String, Id>();
            sObjectCatalog = new Map<String,Map<String, Id>>();
            String query = 'SELECT Id, '+filterField+',MDRM_Postal_Code__c  FROM '+sObjectName+' WHERE '+filterField+' IN (';
            for(String param : parameters){
                query += '\''+ param + '\',';
            }
            query = (query.substring(0,query.length()-1))+')';
            for(SObject tmpObj : Database.query(query)){ tempMap.put(((String)tmpObj.get(filterField)).toUpperCase()+(String)tmpObj.get(Label.CDM_Apex_MDRM_Postal_Code_c), (Id)tmpObj.get(Label.CDM_Apex_Id));
            }
            sObjectCatalog.put(sObjectName, tempMap);
            return sObjectCatalog.get(sObjectName);
        }else{
            if(!sObjectCatalog.containsKey(sObjectName)){
                Map<String, Id> tempMap = new Map<String, Id>();
                String query = 'SELECT Id, '+filterField+',MDRM_Postal_Code__c  FROM '+sObjectName+' WHERE '+filterField+' IN (';
                for(String param : parameters){
                    if(param != null)
                    query += '\''+param + '\',';
                }
                query = (query.substring(0,query.length()-1))+')';
                System.debug(':::: query '+query);
                for(SObject tmpObj : Database.query(query)){ tempMap.put(((String)tmpObj.get(filterField)).toUpperCase()+(String)tmpObj.get(Label.CDM_Apex_MDRM_Postal_Code_c),(Id)tmpObj.get(Label.CDM_Apex_Id));
                }
                sObjectCatalog.put(sObjectName, tempMap);
                return sObjectCatalog.get(sObjectName);
            }else{ 
                return sObjectCatalog.get(sObjectName);
            }
        }
    }
	//
    public static Boolean isPicklistValue(Schema.DescribeFieldResult fieldDescribe, String value) {
        Set<String> picklistValue = new Set<String>();
        for(Schema.PicklistEntry f : fieldDescribe.getPicklistValues()){
            picklistValue.add(f.getValue());
        }
        System.debug(' picklistValue '+fieldDescribe);
        return picklistValue.contains(value);
    }
    
    //
    public static Map<String, Account> getRegionalSalesDivision(){
        if(regionalsalesD == null){
            regionalsalesD = new Map<String, Account>();
            for(Account sOff : [SELECT Id, Name, ISSM_ParentAccount__c FROM Account WHERE RecordType.DeveloperName =: Label.CDM_Apex_ISSM_RegionalSalesDivision ]){
                regionalsalesD.put(sOff.Name, sOff);
            }
            return regionalsalesD;
        }else{
            return regionalsalesD;
        }
    }
    public static Map<String, Account> getSalesOffice(){
        if(salesOffice == null){
            salesOffice = new Map<String, Account>();
            for(Account sOff : [SELECT Id, Name,ONTAP__SalesOffId__c, ONTAP__SAPCustomerId__c, ISSM_ParentAccount__c, ISSM_ParentAccount__r.Id, ISSM_ParentAccount__r.ISSM_ParentAccount__r.Id FROM Account WHERE RecordType.DeveloperName =: Label.CDM_Apex_SalesOffice ]){
                salesOffice.put(sOff.ONTAP__SAPCustomerId__c, sOff);
            }
            return salesOffice;
        }else{
            return salesOffice;
        }
    }

    public static Set<String> getSupplierCenter(){
        if(supplierCenterSet==null){
            supplierCenterSet = new Set<String>();
            for(SupplierCenter__c sa : [SELECT Id,Code__c FROM SupplierCenter__c ]){ supplierCenterSet.add(sa.Code__c); }
            return supplierCenterSet;
        }else{
            return supplierCenterSet;
        }
    }
    public static Map<String, Account> getSalesOrg(){
        if(salesOrg == null){
            salesOrg = new Map<String, Account>();
            for(Account sOff : [SELECT Id, Name,ONTAP__SalesOgId__c, ISSM_ParentAccount__c, ISSM_ParentAccount__r.Id FROM Account WHERE RecordType.DeveloperName =: Label.CDM_Apex_DevName1 ]){
                salesOrg.put(sOff.ONTAP__SalesOgId__c, sOff);
            }
            return salesOrg;
        }else{
            return salesOrg;
        }
    }
    public static Boolean getGranularity(String combinationSA, String sObj){
        String query = 'SELECT Id, Name FROM '+sObj+' WHERE Name='+ combinationSA;
        List<SalesArea__c> lst2 = Database.query(query);
        Boolean isCombination= false;
        List<SalesArea__c> lstSA = [SELECT Id, Name FROM SalesArea__c WHERE Name=: combinationSA];
        isCombination = lstSA.size()>0 ? true :false ;
        return isCombination;
    }
}