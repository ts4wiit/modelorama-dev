/****************************************************************************************************
    General Information
    -------------------
    author:     Hector Diaz
    email:      hdiaz@gmail.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       29/Sep/2017       Hector Diaz HD              
    ================================================================================================
****************************************************************************************************/
global with sharing class ISSM_ReadAttachmentCloseCase_cls implements Database.Batchable<sObject> , Database.Stateful{
    global ISSM_CustomerServiceQuerys_cls ObjQuery =  new ISSM_CustomerServiceQuerys_cls();
    global final String QueryMassClose = 'SELECT Id,ISSM_Status__c FROM ISSM_MassCloseCase__c WHERE ISSM_Processed__c = false';
         
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(QueryMassClose);
    }
    global void execute(Database.BatchableContext BC, List<ISSM_MassCloseCase__c> scope) {
        //OTRAS 
        Boolean blnHeadersOk = false;
        Boolean blnProcessedWithError = false;
        Boolean blnProcessedSuccess = false;
        String strNameFile='';
        Set<String> setAttcQuery = new Set<String>();
        Set<String> setCaseNumberFile = new Set<String>();
        
        
        //Objetos para reralizar update e insert
        Case objUpdateCase = new Case();
        CaseComment objUpdateCaseComment = new CaseComment();
        Attachment objInsertAttErro = new Attachment(); 
        Attachment objInsertAttOk = new Attachment(); 
        ISSM_MassCloseCase__c objUpdateStatus = new ISSM_MassCloseCase__c();
        
        //Listas para almacenar todo lo involucrado al attachment
        List <Attachment>lstAttachment = new List <Attachment>();
        List<Attachment>lstInsertReports = new List<Attachment>();
        List<CaseComment>lstInsertCaseComment = new List<CaseComment>();
        List<Case>      lstUpdateCases = new    List<Case>();
        List<Case>      lstQueryCases = new List<Case>();       
        List<String>    lstinputvalues = new List<String>(); 
        List<String>    lstHeadersFile = new List<String>(); 
        List<String>    lstfilelines = new List<String>();  
        List<String>    lstFinalRecordsOk = new List<String>();  
        List<String>    lstFinalRecordsError = new List<String>(); 
        List<String>    lstvalorMap = new List<String> ();
        
        //Mapas para almacenar los registros del attachment             
        Map<String,List<String>>  mapCorrectRecords = new Map<String,List<String>>();
        Map<String,List<String>>  mapCorrectRecordsMirror = new Map<String,List<String>>();
        Map<String,List<String>>  mapRecordsWithError = new Map<String,List<String>>();
         
        //VARIABLE PARA REPORTE DE REGISTROS CORRECTOS
        String strheaderOK ='';
        String strRecordOk  =''; 
        String strRecordCleanOk ='';
        String strRecordFinalOk = '';
        blob  blbCSVBlobRecordOk = null;
        String strCSVNameRecordOk ='';
        String strFinalReportInsertOk=strheaderOK;
        String StatusRecordOk = '';
        
        //VARIABLES PARA REPORTE CON ERORES
        String strheaderError = '';
        String strRecordFail  =''; 
        String strRecordClean ='';
        String strRecordFinalWithError = '';
        blob  blbCSVBlob = null;
        String strCSVNameRecordError ='';
        String strFinalReportInsertError=strheaderError;
        String StatusRecordError = '';
        
        //Iteramos los registros que se encuentren en la consulta ISSM_MassCloseCase__c
        for(ISSM_MassCloseCase__c objIteraMassClose : scope){  
            setAttcQuery.add(objIteraMassClose.Id); 
        }
        //Validamos y consultamos que setAttcQuery tenga resultado
        if(setAttcQuery != null && !setAttcQuery.isEmpty()){
            lstAttachment = ObjQuery.QueryAttachment(setAttcQuery);
        }        
         
        //Valida que la lista de attachment tenga un resultado
        if(lstAttachment != null && !lstAttachment.isEmpty()){
            for(Attachment objIteraAtt : lstAttachment){
                
                //Inicializamos las variables para actualizar del objeto ISSM_MassCloseCase__c con los valores por default, estas pueden cambiar deacuerdo al flujo
                objUpdateStatus.Id = objIteraAtt.ParentId;
                objUpdateStatus.ISSM_Processed__c=true;   
                objUpdateStatus.ISSM_TotalCases__c=0;
                objUpdateStatus.ISSM_UpdateWithErrors__c=0;
                objUpdateStatus.ISSM_UpdateSuccess__c=0;
                objUpdateStatus.ISSM_ErrorMessage__c = '';
                
                //Reiniciamos las variables por cada registro encontrado en Attachment
                objInsertAttErro = new Attachment(); 
                objInsertAttOk = new Attachment();
                blnProcessedSuccess = false;
                blnProcessedWithError = false;
                strFinalReportInsertOk =  System.label.ISSM_FileSuccessHeader+'\n';
                strFinalReportInsertError = System.label.ISSM_FileErrorHeader+'\n';
                mapCorrectRecords =  new Map<String,List<String>>();
                mapRecordsWithError = new Map<String,List<String>>();   
                lstInsertReports =  new List<Attachment>(); 
                lstFinalRecordsOk = new List<String>(); 
                lstFinalRecordsError =  new List<String>();
                
                try{
                    strNameFile = objIteraAtt.body.toString();
                    System.debug('strNameFile : '+strNameFile); 
                    lstfilelines = strNameFile.split('\n',20000); //Quitamos los saltos de linea de cada registro
                    lstHeadersFile = lstfilelines[0].split(','); //Solo toma la primer linea que es el encabezado               
                
                    //Validacion del CaseNumber 
                    /*if(lstHeadersFile[0] == System.label.ISSM_ColumnNumCase && lstHeadersFile[1] == System.label.ISSM_ColumnDateTime 
                    &&  lstHeadersFile[2] == System.label.ISSM_ColumnDescription && lstHeadersFile[3].replaceAll('\r','') ==  System.label.ISSM_CloseEffective){*/
                    if(lstHeadersFile[0] == '﻿Numero de caso' && lstHeadersFile[1] == 'Fecha y hora de cierre' &&  lstHeadersFile[2] == 'Descripcion' && lstHeadersFile[3].replaceAll('\r','') == 'Cierre Efectivo'){
                        blnHeadersOk = true; 
                        System.debug('#####PASO VALIDACION DE COLUMNAS :'+blnHeadersOk);
                    }else{//Si falla una columa del archivo actualizamos el status del registro
                        blnHeadersOk = false;
                        objUpdateStatus.ISSM_Status__c = System.label.ISSM_StatusFileErrorColumn;
                        objUpdateStatus.ISSM_ErrorMessage__c= System.label.ISSM_ErrorMessageColumn; 
                    }    
                    lstfilelines.remove(0);//Removemos el encabezado del archivo
                    
                    if(blnHeadersOk){//Si el archivo cumple con las columnas
                        for(String objFilelines : lstfilelines){
                            lstinputvalues = objFilelines.split(',');
                            if(/*lstinputvalues[0].length() == 8 && */lstinputvalues[0]!= null && lstinputvalues[0]!=''  && lstinputvalues[0].isNumeric() && Datetime.valueOf(lstinputvalues[1])!=null){ 
                                mapCorrectRecords.put(lstinputvalues[0],lstinputvalues);
                                mapCorrectRecordsMirror.put(lstinputvalues[0],lstinputvalues);//Espejo se ocupa para la seccion de update en el caso 
                                //setCaseNumberFile.add('%'+lstinputvalues[0]+'%');
                                setCaseNumberFile.add(lstinputvalues[0]);
                                
                            }else{
                                //Contiene solo los registros que no coincidan con la validacion
                                mapRecordsWithError.put(lstinputvalues[0],lstinputvalues); 
                            }                         
                        }      
                    }System.debug('***setCaseNumberFile:'+ setCaseNumberFile);
                    /***********************************REGISTRO SIN ERROR**************************************************/
                    //Itera el map de los registros Sin errores
                    if(mapCorrectRecords.size() > 0){
                        blnProcessedSuccess =true;  
                        StatusRecordOk = System.label.ISSM_StatusRecordSuccessFile;
                        for(String objIteraMapRecordsOk :  mapCorrectRecords.keySet()){
                            strRecordOk = mapCorrectRecords.get(objIteraMapRecordsOk)+',';  
                            strRecordCleanOk = strRecordOk.replaceAll('[( - )\n\r]', '');
                            strRecordFinalOk = strRecordCleanOk + StatusRecordOk +'\n';
                            strFinalReportInsertOk = strFinalReportInsertOk + strRecordFinalOk;
                        }
                        //Agregamos a la lista todos los registros correctos para que sea generado el reporte
                        lstFinalRecordsOk.add(strFinalReportInsertOk);
                        
                    }               
                    
                    if(lstFinalRecordsOk != null && !lstFinalRecordsOk.isEmpty()){
                        blbCSVBlobRecordOk = Blob.valueOf(String.valueOf(lstFinalRecordsOk)); //Generamos la lista en formato BLOB para generar el attachment       
                        strCSVNameRecordOk = System.label.ISSM_FileNameSucces+'.csv';
                         
                        //Preparamos datos para insertar el CSV
                        objInsertAttOk.Name =  strCSVNameRecordOk;
                        objInsertAttOk.ParentId = objIteraAtt.ParentId;
                        objInsertAttOk.body = blbCSVBlobRecordOk;
                        lstInsertReports.add(objInsertAttOk);                   
                    }
                    /***********************************REGISTRO CON ERROR**************************************************/
                    //Itera el map de los registros Con errores
                    if(mapRecordsWithError.size() > 0){ 
                        blnProcessedWithError = true;           
                        StatusRecordError = System.label.ISSM_StatusRecordErrorFile;
                        for(String objIteraMapRecordsError :  mapRecordsWithError.keySet()){                    
                            strRecordFail = mapRecordsWithError.get(objIteraMapRecordsError)+',';   
                            strRecordClean = strRecordFail.replaceAll('[( - )\n\r]', '');
                            strRecordFinalWithError = strRecordClean + StatusRecordError+'\n';
                            strFinalReportInsertError = strFinalReportInsertError + strRecordFinalWithError;            
                        }   
                        //Agregamos a la lista todos los registros incorrectos para que sea generado el reporte
                        lstFinalRecordsError.add(strFinalReportInsertError);
                    }
                    
                    
                    if(lstFinalRecordsError != null && !lstFinalRecordsError.isEmpty()){
                        //Generar reporte CSV de Registros CON error
                        blbCSVBlob = Blob.valueOf(String.valueOf(lstFinalRecordsError)); //Generamos la lista en formato BLOB para generar el attachment        
                        strCSVNameRecordError = System.label.ISSM_FileNameError+'.csv';
                        
                        //Preparamos datos para insertar el CSV                     
                        objInsertAttErro.Name =  strCSVNameRecordError;
                        objInsertAttErro.ParentId = objIteraAtt.ParentId;
                        objInsertAttErro.body = blbCSVBlob;
                        lstInsertReports.add(objInsertAttErro);                     
                    }
                    
                    //Validamos para insertar el o los attachment al registros
                    if(lstInsertReports != null && !lstInsertReports.isEmpty()){
                        insert lstInsertReports;
                    }
                    //Si la variables estan en true ejecuta este flujo que son los registros con errores
                    if(blnProcessedSuccess && blnProcessedWithError){
                        objUpdateStatus.ISSM_Status__c = System.label.ISSM_StatusFileProcessedWithErrors;
                        objUpdateStatus.ISSM_TotalCases__c= Decimal.valueOf(lstfilelines.size());
                        objUpdateStatus.ISSM_UpdateWithErrors__c=Decimal.valueOf(mapRecordsWithError.size());
                        objUpdateStatus.ISSM_UpdateSuccess__c=Decimal.valueOf(mapCorrectRecords.size());
                    }
                    //Si la variables blnProcessedWithError es diferente de true ejecuta este flujo que son los registros sin errores
                    if(blnProcessedSuccess && blnProcessedWithError == false){
                        objUpdateStatus.ISSM_Status__c = System.label.ISSM_StatusFileProcessedSuccess;
                        objUpdateStatus.ISSM_TotalCases__c= Decimal.valueOf(lstfilelines.size());
                        objUpdateStatus.ISSM_UpdateSuccess__c=Decimal.valueOf(mapCorrectRecords.size());
                        objUpdateStatus.ISSM_UpdateWithErrors__c=0;
                    }
                }
                //Se obtienen las Exception que puedan ocurrir durante el flujo y actualiza el registo con la info correspondiente 
                catch(TypeException TE){ 
                    objUpdateStatus.ISSM_Status__c = System.label.ISSM_StatusFileErrorColumn;   
                    objUpdateStatus.ISSM_ErrorMessage__c = String.valueOf(TE);
                    
                }catch(StringException SE){
                    objUpdateStatus.ISSM_Status__c = System.label.ISSM_StatusFileFormatIncorrect;
                    objUpdateStatus.ISSM_ErrorMessage__c = String.valueOf(SE);                  
                }catch(QueryException QE){
                    System.debug('QueryException : '+ QE); 
                } 
                catch(Exception ex){
                    System.debug('Exception: '+ ex); 
                }
                //ACtualizamso los registros de ISSM_MassCloseCase__c
                update objUpdateStatus;             
            }
            
            //Update Cases FUERA DEL FOR SOLO DENTRO DEL IF lstAttachment
            try{
                //Consultamos los casos deacuerdo al setCaseNumberFile que contiene el casenumber del attachment
                //lstQueryCases = ObjQuery.QueryCaseSetStr(setCaseNumberFile);
                lstQueryCases = ObjQuery.QueryCaseSetStrIN(setCaseNumberFile);
                
                System.debug('####lstQueryCases : '+lstQueryCases);
                
                if(lstQueryCases != null && !lstQueryCases.isEmpty()){
                    String casenumber = ''; 
                    for(Case objCase : lstQueryCases){
                        
                        objUpdateCase = new Case();
                        objUpdateCaseComment = new CaseComment();   
                        lstvalorMap = mapCorrectRecordsMirror.get(objCase.CaseNumber);   
                        System.debug('*******lstvalorMap : '+lstvalorMap);
                        System.debug('*******lstvalorMap SIZE: '+lstvalorMap.size());
                        System.debug('MAP : '+ mapCorrectRecordsMirror);
                        
                        /* if(mapCorrectRecordsMirror.containsKey(objCase.CaseNumber)){
                            lstvalorMap = mapCorrectRecordsMirror.get(objCase.CaseNumber);  
                            System.debug('Si lo contiene');
                        }else{
                            casenumber = objCase.CaseNumber.substring(2);
                            if(mapCorrectRecordsMirror.containsKey(casenumber)){
                                lstvalorMap = mapCorrectRecordsMirror.get(casenumber);
                                System.debug('le quite los 00 ');
                            }else{
                                casenumber = objCase.CaseNumber.substring(1);
                                lstvalorMap = mapCorrectRecordsMirror.get(casenumber);
                                System.debug('le quite los 0 ');
                            }
                                
                            
                        }*/
                        
                        //Lenamos para actualizar el caso
                        objUpdateCase.Id = objCase.Id;    
                        objUpdateCase.ISSM_ProviderCloseCase__c = Datetime.valueOf(lstvalorMap[1]);             
                        objUpdateCase.Description = lstvalorMap[3];
                        objUpdateCase.Status = System.label.ISSM_UpdateStatusCloseCaseAtt;
                        lstUpdateCases.add(objUpdateCase);
                        System.debug('*****UPDATE : '+lstUpdateCases);
                        
                        //Llenamos para insertar el  CaseCommnet
                        objUpdateCaseComment.ParentId = objCase.Id;
                        objUpdateCaseComment.CommentBody = lstvalorMap[2];
                        lstInsertCaseComment.add(objUpdateCaseComment);
                    }
                }
                
                //Actualizamos el Caso si la lista contiene informacion 
                if(lstUpdateCases!= null && !lstUpdateCases.isEmpty()){
                    System.debug('####lstUpdateCases ** : '+lstUpdateCases);
                    ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger);
                    update lstUpdateCases;  
                }
                //Insertamos el caseComment si la lista contiene informacion 
                if(lstInsertCaseComment!= null && !lstInsertCaseComment.isEmpty()){
                    System.debug('####lstInsertCaseComment : '+lstInsertCaseComment);
                    ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseCommentTrigger);
                    insert lstInsertCaseComment;    
                }
            }catch(ListException LE){
                System.debug(LE.getLineNumber());
                System.debug(LE.getCause()); 
                System.debug(LE.getStackTraceString());             
            }
            catch(Exception e){
                System.debug('Exception: '+ e);
            }
        }            
    }  
    
    global void finish(Database.BatchableContext BC) {
        System.debug('######Finish######');
        
    }
}