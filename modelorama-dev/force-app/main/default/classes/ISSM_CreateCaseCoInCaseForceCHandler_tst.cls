@isTest
public class ISSM_CreateCaseCoInCaseForceCHandler_tst {
	static testmethod void principalMethod() {
        
        List<CaseComment> lstDataCaseCommentNew = new List<CaseComment>();
        
        Case c = new Case();
        c.Description = 'Case';
        insert c;
        
        CaseComment cCom = new CaseComment();
        cCom.CommentBody = 'Case Comment';
        cCom.ParentId = c.Id;
        insert cCom;
        lstDataCaseCommentNew.add(cCom);
        
        Test.startTest();
        
        ISSM_CreateCaseCoInCaseForceCHandler_cls clase = new ISSM_CreateCaseCoInCaseForceCHandler_cls();
        clase.CreateCaseCommentInCaseForceComment(lstDataCaseCommentNew);
        
        Test.stopTest();
    }
}