global class ISSM_IntObject_bch implements Database.Batchable<sObject> {
	
	String query;
	String nameConfig;
	String field;
	String valueField;
	
	global ISSM_IntObject_bch(String nameConfigParam) {
		nameConfig = nameConfigParam;
	}

	global ISSM_IntObject_bch(String nameConfigParam, String fieldParam, String valueFieldParam) {
		nameConfig = nameConfigParam;
		field = fieldParam;
		valueField = valueFieldParam;
	}
	
	global Iterable<SObject> start(Database.BatchableContext BC) {
		return new ISSM_IntObject_cls(nameConfig, field, valueField);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		ISSM_Debug_cls.debugList(ISSM_IntObject_bch.class.getName(), 'execute', '14', 'delete', scope);
		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}