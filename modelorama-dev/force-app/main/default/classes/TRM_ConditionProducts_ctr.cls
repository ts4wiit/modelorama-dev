/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows searching through words entered by the user, 
                        as well as being able to obtain products that have been selected in a save and exit

    Information about changes (versions)
    ===============================================================================
    No.    Date                 Author                      Description
    1.0    21-August-2018       Luis Licona                 Creation
    2.0    November 05, 2018    Carlos Pintor               Modified
    ******************************************************************************* */
public with sharing virtual class TRM_ConditionProducts_ctr {

    //Product Listing Wrapper
    public class WrapperClass{
        @AuraEnabled public ONTAP__Product__c ObjProducts{get;set;}
        @AuraEnabled public boolean IsSelected{get;set;}
        @AuraEnabled public Decimal DecAmount{get;set;}
        @AuraEnabled public Decimal DecAmntPrev{get;set;}
        
        public WrapperClass(ONTAP__Product__c prod,Boolean isSelect,Decimal decAmount,Decimal decAmntPrevious ) {
            this.ObjProducts = prod;
            this.IsSelected  = isSelect;
            this.DecAmount   = decAmount;
            this.DecAmntPrev = decAmntPrevious;
        }
    }
    
    /**
    * @description  Method that performs the search of products according to the word entered and the products already selected
    * 
    * @param    StrQuota        Id of Quote selected
    * @param    StrKey          Word entered in the search engine
    * @param    LstIds          List of Ids of products selected
    * @param    StrCondition    additional condition determined by metadata
    * 
    * @return   WrapperClass[]  Wrapper class with structure of products
    */
    @AuraEnabled
    public static WrapperClass[] SearchProducts(String StrQuota, String StrKey,String[] LstIds,String StrCondition,Boolean blnSbyTab){
        String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);
        ONTAP__Product__c[] LstProd = new List<ONTAP__Product__c>();
        WrapperClass[] returnwrapperClass = new  List<WrapperClass> ();
        String[] keyWordList = new List<String>();

        if(blnSbyTab){
            for(String str : String.escapeSingleQuotes(StrKey).split(' ')){
                keyWordList.add('\'' + str + '\'');
            }
        }
        // On November 5th 2018, the field 'ONCALL__Material_Number__c' was added to the SOQL query
        // to replace the field 'ONTAP__ExternalKey__c' in the queries of the Object 'ONTAP__Product__c'
        String soql = 'SELECT ONTAP__ExternalKey__c, ONCALL__Material_Number__c, ONTAP__MaterialProduct__c, ';
        soql += 'ISSM_Quota__r.Name, ISSM_MaterialGroup2__r.Name, ISSM_SectorCode__r.Name ';
        soql += 'FROM ONTAP__Product__c WHERE ';
        soql += 'ISSM_Flag_deleted__c = false AND ';
        soql += (String.isNotBlank(StrQuota)) ? 'ISSM_Quota__c = '+'\'' +StrQuota+ '\' AND ' : '';
        soql += (!LstIds.isEmpty()) ? 'Id NOT IN (' + String.join( LstIds, ',' ) +') AND ' : '';
        soql += ' RecordTypeId = '+'\'' +RecType+ '\'';
        //soql += ' AND ISSM_SectorCode__c != null AND ONTAP__ExternalKey__c != null AND ONTAP__ProductType__c != null'; 
        soql += ' AND ISSM_SectorCode__c != null AND ONTAP__ExternalKey__c != null AND ONCALL__Material_Number__c != null AND ONTAP__ProductType__c != null'; 
        //soql += (blnSbyTab) ? ' AND ONTAP__ExternalKey__c IN (' + String.join(keyWordList, ',') + ') ' : '';
        soql += (blnSbyTab) ? ' AND ONCALL__Material_Number__c IN (' + String.join(keyWordList, ',') + ') ' : '';
        soql += (blnSbyTab) ? '' : ' AND (ONTAP__MaterialProduct__c LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\'';
        //soql += (blnSbyTab) ? '' : ' OR ONTAP__ExternalKey__c LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\'';
        soql += (blnSbyTab) ? '' : ' OR ONCALL__Material_Number__c LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\'';
        soql += (blnSbyTab) ? '' : ' OR ISSM_Quota__r.Name LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\'';
        soql += (blnSbyTab) ? '' : ' OR ISSM_MaterialGroup2__r.Name LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\'';
        soql += (blnSbyTab) ? '' : ' OR ISSM_SectorCode__r.Name LIKE \''+'%'+String.escapeSingleQuotes(StrKey)+'%\')';
        soql += (String.isNotBlank(StrCondition)) ? ' AND '+StrCondition : '';
        //soql += ' ORDER BY ONTAP__ExternalKey__c';
        soql += ' ORDER BY ONCALL__Material_Number__c';

        System.debug('###soql SearchProducts#####: '+soql);
        LstProd = runQuery(soql);
        if(!LstProd.isEmpty()){
            for(ONTAP__Product__c prod : LstProd) {
                returnwrapperClass.add(new TRM_ConditionProducts_ctr.WrapperClass(prod,false,null,null));
            }
        }
        
        System.debug('#####returnwrapperClass '+returnwrapperClass);
        return returnwrapperClass;
    }

    /**
    * @description  Method that performs the search of products according to the word entered and the products already selected
    * @param    IdCondClass     Id of condition class
    * @return   WrapperClass[]  Wrapper class with structure of products
    */
    @AuraEnabled
    public static WrapperClass[] getProducts(String IdCondClass){
        WrapperClass[] returnwrapperClass = new  List<WrapperClass> ();
		
        Map<String,Decimal> MapProd = new Map<String,Decimal>();
        AggregateResult [] LSTCR = [SELECT   TRM_Product__c, 
                                             MIN(TRM_AmountPercentage__c)
                                    FROM     TRM_ConditionRecord__c
                                    WHERE    TRM_ConditionClass__c =: IdCondClass
                                    GROUP BY TRM_Product__c];

        for(AggregateResult objAR : LSTCR){
            MapProd.put((String)objAR.get('TRM_Product__c'), (Decimal)objAR.get(Label.TRM_expr0));
        }

        // On November 5th 2018, the field 'ONCALL__Material_Number__c' was added to the SOQL query
        // to replace the field 'ONTAP__ExternalKey__c' in the queries of the Object 'ONTAP__Product__c'
        for(ONTAP__Product__c objP : [SELECT ONTAP__ExternalKey__c, ONCALL__Material_Number__c, ONTAP__MaterialProduct__c,
        									 ISSM_Quota__r.Name, ISSM_MaterialGroup2__r.Name, 
                                             ISSM_SectorCode__r.Name
        							  FROM   ONTAP__Product__c 
                                      WHERE  Id IN: MapProd.keySet()]){
            returnwrapperClass.add(new TRM_ConditionProducts_ctr.WrapperClass(objP,true,MapProd.get(objP.Id),MapProd.get(objP.Id)));
        }
  
        System.debug('returnwrapperClass----->'+returnwrapperClass);
        return returnwrapperClass;
    }
    
    /**
    * @description  Method that allows you to consult the scalable products of a condition that you want to clone
    * @param    IdCondClass             Id of condition class
    * @return   TRM_ProductScales__c[]  List of product scales 
    */
    @AuraEnabled
    public static TRM_ProductScales__c[] getScales(String IdCondClass){
        TRM_ProductScales__c[] lstScales = new List<TRM_ProductScales__c>();
        
        for(TRM_ProductScales__c obj : [SELECT 	TRM_AmountPercentage__c,TRM_Quantity__c,TRM_ConditionRecord__c,
                                        		TRM_ConditionRecord__r.TRM_Product__c
                                        FROM 	TRM_ProductScales__c 
                                        WHERE 	TRM_ConditionRecord__r.TRM_ConditionClass__c =:IdCondClass ]){
			TRM_ProductScales__c objNew = new TRM_ProductScales__c();
            objNew.TRM_Quantity__c 			= obj.TRM_Quantity__c;
			objNew.TRM_AmountPercentage__c 	= obj.TRM_AmountPercentage__c;
            objNew.TRM_ConditionRecord__c 	= obj.TRM_ConditionRecord__r.TRM_Product__c;            
         	lstScales.add(objNew);
        }

        System.debug('####lstScales####: '+lstScales);
        return lstScales;
    }

    /**
    * @description  Method that allows Execution of the query for Search Products
    * @param    StrSoql             Query
    * @return   ONTAP__Product__c[]  List of products
    */
    public static ONTAP__Product__c[] runQuery(String StrSoql) {
        ONTAP__Product__c[] LstProd = new List<ONTAP__Product__c>();
        try {
            LstProd = Database.query(StrSoql);
        } catch (Exception e) {
            System.debug('ERROR EN runQuery: '+e.getStackTraceString());
        }
        return LstProd;
    }
}