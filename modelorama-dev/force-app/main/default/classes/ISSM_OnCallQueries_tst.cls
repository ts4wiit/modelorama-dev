/**
********************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @proyect          ISSM OnCall
* @name             ISSM_OnCallQueries_tst
* @description      Test class that covers the following class ISSM OnCallQueries_cls
* @dependencies
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------
* @version   1.0   2018-09-05   Antonio Torres                Initial version.
********************************************************************************************************
**/

@isTest
public class ISSM_OnCallQueries_tst {
    /**
    * @method      generateTestData
    * @description Method that generates the test data used in test methods.
    * @author      Antonio Torres - 2018-09-05
    */
    @testSetup public static void generateTestData() {
        Account objAccount = new Account(
            Name                    = 'TST',
            ONTAP__Main_Phone__c    = '5510988765',
            ISSM_MainContactE__c    = true,
            ONTAP__SAPCustomerId__c = '100153987',
            ONTAP__SalesOgId__c     = '3116',
            ONTAP__ChannelId__c     = '1'
        );
        insert objAccount;

        Account ObjAccountOrg = new Account(
            Name                    = 'Cuenta Org',
            ONTAP__Main_Phone__c    = '12345678',
            ONTAP__SalesOgId__c     = '3116',
            RecordTypeId            = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId()
        );
        insert ObjAccountOrg;

        List<String> idsQtyProds_Lst = new List<String>();
        ONTAP__Product__c product = new ONTAP__Product__c();
        product.ISSM_ComboNumber__c = 'Combo 1';
        idsQtyProds_Lst.add(product.ISSM_ComboNumber__c);
        insert product;

        ISSM_ComboLimit__c comboLimit = new ISSM_ComboLimit__c();
        insert comboLimit;

        ISSM_PriceEngineConfigWS__c objPriceEngineConfigWS = new ISSM_PriceEngineConfigWS__c();
        objPriceEngineConfigWS.ISSM_AccessToken__c          = '';
        objPriceEngineConfigWS.ISSM_EndPoint__c             = Label.ISSM_EPMaterialAvailable;
        objPriceEngineConfigWS.ISSM_Method__c               = Label.ISSM_MethodTst;
        objPriceEngineConfigWS.ISSM_HeaderContentType__c    = Label.ISSM_HeaderContentTypeTst;
        objPriceEngineConfigWS.Name                         = Label.ISSM_ConfigurationMatAvailable;
        insert objPriceEngineConfigWS;

        ISSM_Combos__c combos = new ISSM_Combos__c();
        combos.ISSM_ComboLimit__c           = comboLimit.Id;
        combos.ISSM_ModifiedCombo__c        = false;
        combos.ISSM_StartDate__c            = Date.today() - 1;
        combos.ISSM_EndDate__c              = Date.today() + 1;
        combos.ISSM_LongDescription__c      = 'Long description for the new combo';
        combos.ISSM_ShortDescription__c     = 'ShortDescr';
        combos.ISSM_NumberSalesOffice__c    = 10;
        combos.ISSM_NumberByCustomer__c     = 1;
        combos.ISSM_ComboType__c            = 'Expecific';
        combos.ISSM_StatusCombo__c          = 'ISSM_Actived';
        combos.ISSM_SynchronizedWithSAP__c  = true;
        combos.ISSM_Currency__c             = 'MXN';
        combos.ISSM_TypeApplication__c      = 'TELESALES';
        combos.RecordTypeId                 = Schema.SObjectType.ISSM_Combos__c.getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        insert combos;

        ISSM_Combos__c combos3 = new ISSM_Combos__c();
        combos3.ISSM_ComboLimit__c          = comboLimit.Id;
        combos3.ISSM_StartDate__c           = Date.today() + 2;
        combos3.ISSM_EndDate__c             = Date.today() + 30;
        combos3.ISSM_LongDescription__c     = 'Long description for the new combo';
        combos3.ISSM_ShortDescription__c    = 'ShortDescr';
        combos3.ISSM_NumberSalesOffice__c   = 10;
        combos3.ISSM_NumberByCustomer__c    = 0;
        combos3.ISSM_ComboType__c           = 'Expecific';
        combos3.ISSM_StatusCombo__c         = 'ISSM_Actived';
        combos3.ISSM_SynchronizedWithSAP__c = true;
        combos3.ISSM_Currency__c            = 'MXN';
        combos3.ISSM_TypeApplication__c     = 'ALLMOBILE;TELESALES;B2B;BDR';
        combos3.RecordTypeId                = Schema.SObjectType.ISSM_Combos__c.getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        insert combos3;

        ISSM_Combos__c combos2 = new ISSM_Combos__c();
        combos2.ISSM_ComboLimit__c          = comboLimit.Id;
        combos2.ISSM_StartDate__c           = Date.today() + 2;
        combos2.ISSM_EndDate__c             = Date.today() + 30;
        combos2.ISSM_LongDescription__c     = 'Long description for the new combo';
        combos2.ISSM_ShortDescription__c    = 'ShortDescr';
        combos2.ISSM_NumberSalesOffice__c   = 10;
        combos2.ISSM_ComboType__c           = 'Expecific';
        combos2.ISSM_StatusCombo__c         = 'ISSM_Actived';
        combos2.ISSM_SynchronizedWithSAP__c = true;
        combos2.ISSM_Currency__c            = 'MXN';
        combos2.ISSM_TypeApplication__c     = 'ALLMOBILE;TELESALES;B2B;BDR';
        combos2.RecordTypeId                = Schema.SObjectType.ISSM_Combos__c.getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        insert combos2;

        ISSM_AccountantByCustomer__c objAcountantByCus = new ISSM_AccountantByCustomer__c();
        objAcountantByCus.ISSM_ComboNumber__c   = combos2.Id;
        objAcountantByCus.ISSM_Customer__c      = objAccount.id;
        insert objAcountantByCus;

        ISSM_ComboByProduct__c comboByProduct = new ISSM_ComboByProduct__c();
        comboByProduct.ISSM_Product__c      = product.Id;
        comboByProduct.ISSM_ComboNumber__c  = combos.Id;
        insert comboByProduct;

        ONTAP__Product__c prod = new ONTAP__Product__c();
        prod.ISSM_ComboNumber__c = 'test';
        insert prod;

        ISSM_ComboByAccount__c cba_obj = new ISSM_ComboByAccount__c();
        cba_obj.ISSM_ComboNumber__c         = combos.Id;
        cba_obj.ISSM_Customer__c            = objAccount.Id;
        cba_obj.ISSM_ExclusionAccount__c    = TRUE;
        insert cba_obj;

        ONCALL__Call__c objCall = new ONCALL__Call__c(
            Name                    = 'TST',
            ONCALL__POC__c          = objAccount.Id,
            RecordTypeId            = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId(),
            ISSM_ValidateOrder__c   = false
        );
        insert objCall;

        ONTAP__Product__c objProduct3 = new ONTAP__Product__c(
            RecordTypeId                = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
        );
        insert objProduct3;

        ONTAP__Product__c objProduct2 = new ONTAP__Product__c(
            RecordTypeId                = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
        );
        insert objProduct2;

        ONTAP__Product__c objProduct = new ONTAP__Product__c(
            RecordTypeId                = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ISSM_Empties_Material__c    = objProduct2.Id,
            ISSM_BoxRack__c             = objProduct3.Id
        );
        insert objProduct;

        ISSM_ProductByOrg__c objProductByOrg = new ISSM_ProductByOrg__c(
            RecordTypeId                = Schema.SObjectType.ISSM_ProductByOrg__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallProduct').getRecordTypeId(),
            ISSM_RelatedAccount__c      = objAccountOrg.Id,
            ISSM_AssociatedProduct__c   = objProduct.Id,
            ISSM_Empties_Material__c    = objProduct2.Id,
            ISSM_EmptyRack__c           = objProduct3.Id
        );
        insert objProductByOrg;

        ONTAP__Order__c objOrder = new ONTAP__Order__c(
            RecordTypeId                = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId(),
            ONCALL__OnCall_Account__c   = objAccount.Id,
            ONCALL__Call__c             = objCall.Id,
            ONTAP__OrderStatus__c       = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c           = Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c      = System.today() + 1,
            ONTAP__BeginDate__c         = System.today()
        );
        insert objOrder;

        ONTAP__Order_Item__c objOrderItem = new ONTAP__Order_Item__c(
            ONCALL__OnCall_Product__c = objProduct.Id,
            ONCALL__OnCall_Order__c   = objOrder.Id
        );
        insert objOrderItem;

        ONTAP__Route__c objRoute = new ONTAP__Route__c(
            ServiceModel__c                 = 'Telesales'
        );
        insert objRoute;

        ISSM_OpenItemB__c ObjOpenItem = new ISSM_OpenItemB__c(
            ISSM_Account__c        = objAccount.Id,
            ISSM_SAPCustomerId__c  = objAccount.ONTAP__SAPCustomerId__c,
            ISSM_Debit_Credit__c   = Label.ISSM_S,
            RecordTypeId           = Schema.SObjectType.ISSM_OpenItemB__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_ONTAPCompact).getRecordTypeId(),
            ISSM_DueDate__c        = Date.today() + 1,
            ISSM_Amounts__c        = 100
        );
        insert ObjOpenItem;        
    }

    /**
    * @method      testMethod1
    * @description Method that tests XXXXX.
    * @author      Antonio Torres - 2018-09-05
    */
    @isTest public static void testMethod1() {
        Account objAccount = [SELECT Id,ISSM_EmptyCreditQty__c,ONTAP__EmptyLoanDays__c,ONTAP__SalesOgId__c,ISSM_StartCall__c,
                                       ISSM_SalesOffice__r.ONTAP__SalesOffId__c,ONTAP__ChannelId__c,ONCALL__OnCall_Route_Code__c,
                                       ONCALL__Ship_To_Name__c,ISSM_PaymentMethod__c,ISSM_Invoicingdocument__c,ONTAP__SAP_Number__c,
                                       Name,ISSM_CustomerComments__c,ONTAP__SalesOffId__c,ISSM_TradeIniciative__c,
                                       ISSM_TradeIniciativeValue__c,ISSM_MainContactA__c,ISSM_MainContactB__c,
                                       ISSM_MainContactC__c,ISSM_MainContactD__c,ISSM_MainContactE__c,
                                       ONTAP__Main_Phone__c,ONCALL__Phone__c,ONTAP__Secondary_Phone__c,ONTAP__Mobile_Phone__c,
                                       ONTAP__Email__c,ONTAP__Preferred_Service_Hours__c, ISSM_CustomerType__c,
                                       ONTAP__SAPCustomerId__c,ISSM_DistributionCenterLU__c,
                                       ISSM_SalesOffice__c, ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c,
                                       ISSM_SalesOrg__c, ISSM_SalesOrg__r.ONTAP__SAPCustomerId__c,
                                       ISSM_RegionalSalesDivision__c, ISSM_RegionalSalesDivision__r.ONTAP__SAPCustomerId__c,
                                       ISSM_SegmentCode__c,ISSM_SegmentCode__r.Code__c,ISSM_SegmentCode__r.Name
                                FROM   Account
                               LIMIT   1];

        Account objAccountOrg = [SELECT Id
                                   FROM Account
                                  WHERE Name = 'Cuenta Org'
                                  LIMIT 1];

        ISSM_Combos__c objCombos = [SELECT Id
                                      FROM ISSM_Combos__c
                                     LIMIT 1];

        ONTAP__Order__c objOrder = [SELECT Id
                                      FROM ONTAP__Order__c
                                     LIMIT 1];

        ONCALL__Call__c objCall = [SELECT Id,
                                    RecordTypeId
                                    FROM ONCALL__Call__c
                                    LIMIT 1];

        ONTAP__Route__c objRoute = [SELECT Id
                                     FROM ONTAP__Route__c
                                    LIMIT 1];

        ONTAP__Order_Item__c objOrderItem = [SELECT Id,
                                                    ONCALL__OnCall_Product__c
                                            FROM ONTAP__Order_Item__c
                                            LIMIT 1];

        ISSM_OpenItemB__c openItemB = [SELECT Id
                                       FROM ISSM_OpenItemB__c
                                       LIMIT 1];    

        ISSM_ProductByOrg__c objProductByOrg = [SELECT Id,
                                                        ISSM_AssociatedProduct__c,
                                                        ISSM_Empties_Material__c,
                                                        ISSM_EmptyRack__c
                                                FROM ISSM_ProductByOrg__c
                                                LIMIT 1];                                

        Set<String> setSkuProduct = new Set<String>();
        setSkuProduct.add(objOrderItem.ONCALL__OnCall_Product__c);

        Set<String> AccId_set = new Set<String>();
        AccId_set.add(String.valueOf(objAccount.Id));

        Set<Id> emptiesId_set = new Set<Id>();
        emptiesId_set.add(objProductByOrg.ISSM_Empties_Material__c);

        ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();

        Test.startTest();
            CTRSOQL.getExcludedCombos(objAccount);

            CTRSOQL.getCombosByLevel(new Set<Id>(),
                                     Label.ISSM_SalesOffice,
                                     objAccount.ISSM_SalesOffice__c);


            CTRSOQL.getNationalCombos(new Set<Id>(),
                                      Label.ISSM_National);

            CTRSOQL.getProductByCombo(new List<ISSM_Combos__c>(),
                                      Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId(),
                                      new List<ISSM_ComboByProduct__c>(),
                                      Schema.SObjectType.ISSM_ProductByOrg__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallProduct').getRecordTypeId(),
                                      objAccount);

            CTRSOQL.getProductByCombo2('strIdCombo',
                                       Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId(),
                                       true,
                                       objAccount,
                                       Schema.SObjectType.ISSM_ProductByOrg__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallProduct').getRecordTypeId());

            CTRSOQL.getProductByCombo3('strIdCombo',
                                       true,
                                       Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId(),
                                       new List<Id>());

            CTRSOQL.getProductByCombo4('strIdCombo',
                                       Schema.SObjectType.ISSM_ComboByProduct__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProdByComboMx').getRecordTypeId(),
                                       true);

            CTRSOQL.getProdsCombo(new Map<Id,List<ISSM_ComboByProduct__c>>(),
                                  'searchKey',
                                  objAccount);

            CTRSOQL.getProdsCombo2(new Map<String,Integer>());

            CTRSOQL.getCombo(objCombos.Id);

            CTRSOQL.getCBA(new Set<String>());

            CTRSOQL.getComboByAccount(Schema.SObjectType.ISSM_Combos__c.getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId(),
                                      new Set<Id>(),
                                      new List<String>());

            CTRSOQL.getCounterByComboByCustomer(new Set<Id>(),
                                                objAccount);

            CTRSOQL.getOrderbyId(objOrder.Id);

            CTRSOQL.getOrderListbyId('IdCall');

            CTRSOQL.getOrderbyRecType(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId(),
                                      objOrder.Id);

            CTRSOQL.getOrderListAcc(objAccount.Id);

            CTRSOQL.getAccountbyId(objAccount.Id);
            CTRSOQL.getAccountbyIdForUpdate(objAccount.Id);
            CTRSOQL.getAccountListbyId(objAccount.Id);

            CTRSOQL.getEmptyBalanceBByIdAcc(objAccount.Id,
                                            'trLike',
                                            Schema.SObjectType.EmptyBalanceB__c.getRecordTypeInfosByDeveloperName().get('Compact').getRecordTypeId());

            CTRSOQL.getRoute(objRoute.Id);

            CTRSOQL.getMaxOrderToday('strDtConcat');

            CTRSOQL.getInvoicingMaxOrderToday('strInvoice');

            CTRSOQL.getSalesOfficeAccnt(objAccount.Id);

            CTRSOQL.getSalesOffices('strRecType');

            CTRSOQL.getLastOrders(objAccount.Id, System.now());

            CTRSOQL.getCallbyId(objCall.Id);

            CTRSOQL.getCalls(objAccount.Id,
                             Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId());

            CTRSOQL.getLstCalls(objAccount.Id,
                                Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId());
            
            CTRSOQL.getCallByOrder(objCall.RecordTypeId,
                                    String.valueOf(objAccount.Id),
                                    String.valueOf(objOrder.Id));

            CTRSOQL.getDateTime(objAccount.Id);

            CTRSOQL.getMaximumByCustomer(Schema.SObjectType.ISSM_CounterByCustomerBySKU__c.getRecordTypeInfosByDeveloperName().get('ISSM_CounterByCustomer').getRecordTypeId(),
                                         objAccount.Id,
                                         new List<ONTAP__Product__c>());

            CTRSOQL.getProductsListbyRectype(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                             objAccount.Id,
                                             objAccountOrg.Id);

            CTRSOQL.getEmptiesListbyRectype(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                            Schema.SObjectType.ISSM_ProductByPlant__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_ProductByPlant).getRecordTypeId(),
                                            objAccount);

            List<ONTAP__Product__c> prods_lst = CTRSOQL.getEmptiesList(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId(),
                                                                        Schema.SObjectType.ISSM_ProductByPlant__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProductByPlant').getRecordTypeId(),
                                                                        objAccount,
                                                                        setSkuProduct,
                                                                        'ENVASE%');


            CTRSOQL.getInvTypeProductsListByRectype(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                                    objAccountOrg.Id,
                                                    'strInvTypeAcc',
                                                    'strInvTypeProd',
                                                    objAccount.Id);

            CTRSOQL.getProductsListbyRectypeONTAP(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                                  objAccountOrg.Id);

            CTRSOQL.getProdLstPBO('strOrg',
                                  new Set<String>(),
                                  objAccount.Id);

            CTRSOQL.getOrderItemById(objOrder.Id);

            CTRSOQL.getOrderItemAreNotIn(new Set<Id>(),
                                         objOrder.Id);

            CTRSOQL.getProductListbyMap(new Map<Id, Double>());

            CTRSOQL.getProductListbyMap2(new List<String>());

            CTRSOQL.getDealListbyOrg(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                     'strOrg',
                                     new Set<String>(),
                                     objAccount.Id);

            CTRSOQL.getBonusById(objAccount.Id,
                                 new Set<String>());

            CTRSOQL.getBonusByIdLike(objAccount.Id);

            CTRSOQL.getMessageError('strErrorCode');

            CTRSOQL.getRecordTypeId('ONTAP__Product__c',
                                    Label.ISSM_Products);

            CTRSOQL.getOrderTomorrow(objAccount.Id,
                                     Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId(),
                                     Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('OnCall_Order').getRecordTypeId(),
                                     'strOrderStatus');

            CTRSOQL.getOrdersItems(objAccount.Id,
                                   Date.today(),
                                   Date.today(),
                                   'strSector');

            CTRSOQL.getOrdersQty(objAccount.Id,
                                 Date.today(),
                                 Date.today());

            CTRSOQL.getKPIs(objAccount.Id,
                            1,
                            Date.today(),
                            Date.today());

            CTRSOQL.getTradeInitiative('strDescription');

            CTRSOQL.getNumOIRecordsPerAccount(objAccount.Id);

            CTRSOQL.getGoalsPerCustomer(objAccount.Id,
                                        'strMonth',
                                        'strYear',
                                        Schema.SObjectType.ISSM_Goal__c.getRecordTypeInfosByDeveloperName().get('ISSM_OnCallAccount').getRecordTypeId());

            CTRSOQL.getAccAssPerCustomer(objAccount.Id);

            CTRSOQL.getPermissionSets(UserInfo.getUserId());

            CTRSOQL.getAccbySet(new Set<String>{objAccount.Id});

            CTRSOQL.getTourBySet(new Set<String>{objOrder.Id});

            CTRSOQL.getCallByOwner();

            CTRSOQL.getRoutesUser(Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId());

            CTRSOQL.getAccForRoute(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(), 
                                    new Set<String>());

            CTRSOQL.getAccsByRoutes(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(), 
                                    new Set<Id>());

            CTRSOQL.getAccForSearch('strQuery', new Set<String>());

            CTRSOQL.getNationalAccForSearch(Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId(), 
                                            'strQuery', 
                                            new Set<Id>());

            CTRSOQL.getAccByRoute(objAccount.Id, 'strServModel');

            CTRSOQL.getSetAccByRoute(new Set<String>(), 'strServModel');

            CTRSOQL.getOpenItemPerSet(new Set<String>{objAccount.Id},
                                      Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get('ONTAP_Compact').getRecordTypeId());

            CTRSOQL.getOpenItemPerSetD(new List<ONTAP__OpenItem__c>(),
                                       new Set<String>{objAccount.Id},
                                       Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get('ONTAP_Compact').getRecordTypeId());

            CTRSOQL.getAccountsBySet(new Set<String>{objAccount.Id},
                                     Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId());

            CTRSOQL.getSumOpenItemB(AccId_set,
                                    Schema.SObjectType.ISSM_OpenItemB__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_ONTAPCompact).getRecordTypeId());

            CTRSOQL.getEmptyBalancexOrg(Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_Products).getRecordTypeId(),
                                        emptiesId_set);

            CTRSOQL.getOrdersByAccount(String.valueOf(objAccount.Id));

            CTRSOQL.isSuggestedProdsActive(Label.ISSM_SuggestedProducts);

            CTRSOQL.getOnCallFunctionality(Label.ISSM_SuggestedProducts);

            CTRSOQL.getProductByPlantEmp(prods_lst,setSkuProduct,'ENVASE%',String.valueOf(objAccount.ISSM_DistributionCenterLU__c));

            CTRSOQL.getOrderItemsPdf(String.valueOf(objOrder.Id));

        Test.stopTest();
    }
}