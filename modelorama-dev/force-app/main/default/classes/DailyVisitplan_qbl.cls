/****************************************************************************************************
    General Information
    -------------------
    author: Nelson Sáenz Leal
    email: nsaenz@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class DailyVisitplan_qbl to batch DailyVisitPlan

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       08-08-2017        Nelson Sáenz(NSL)            Created Class
****************************************************************************************************/
public with sharing class DailyVisitplan_qbl implements Queueable
{
  public map<string, list<VisitPlan__c>> mapStrSalesOrgAccVp = new map<string, list<VisitPlan__c>>();
  public map<Id, set<string>> mapIdAccServModels             = new map<Id, set<string>>();
  public map<id, list<sObject>> mapIdVisitListSobjects       = new map<id, list<sObject>>();
  public list<VisitPlanWrapper>  lstVsPlan                   = new list<VisitPlanWrapper>();
  public list<ONTAP__Tour__c> lstTour                        = new list<ONTAP__Tour__c>();
  public list<sObject> lstEventOnCall                        = new list<sObject>();
  public list<VisitPlan__c> lstVisitPlan                     = new list<VisitPlan__c>();
  public list<AccountByVisitPlan__c> lstAccountByVisitPlan   = new list<AccountByVisitPlan__c>();
  public static map<Id, Date> mapIdPlVsDate = new map<Id, Date>();
  public set<Id> idSetAccPlVs = new set<id>();
  public map<Id ,set<Id>> mapIdSetAccPlVs = new map<Id, set<id>>();
  public map<Id, ONTAP__Route__c>  mapIdRoute = new map<Id, ONTAP__Route__c>();
  public integer intQuantity;

  // Visit Plan Settings
  private static VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
  private static Integer VISIT_PERIOD 	  = Integer.valueOf(SyncHerokuParams__c.getAll().get('SyncToursEvents').VisitPeriodConfig__c);
  private static Integer VISIT_PERIOD_CONF  = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;
  private static Integer INTQUANTITYLOTE = Integer.valueOf(visitPlanSettings.BatchSize__c);
  public Date dtNextVisit = Date.Today().addDays(VISIT_PERIOD_CONF);

  // Record types for ONTAP__Tour__c, Event and ONCALL__Call__c
  private Map<String, RecordType> mapTourRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Tour__c', 'DeveloperName');
  private Map<String, RecordType> mapEventRecordTypes = DevUtils_cls.getRecordTypes('Event', 'DeveloperName');
  private Map<String, RecordType> mapCallRecordTypes = DevUtils_cls.getRecordTypes('ONCALL__Call__c', 'DeveloperName');

  /**
   * Method Queueable each thousand records
   * @param  lstVsPlan          list visit plans to generate
   * @param  mapIdAccServModels Id account and model service
   * @param  mapIdRoute         Map to validate assing or created Tours
   * @param  intQuantity        Number of records to be processed per batch
   */
  public DailyVisitplan_qbl(list<VisitPlanWrapper> lstVsPlan, map<Id, set<string>> mapIdAccServModels, map<Id, ONTAP__Route__c>  mapIdRoute, integer intQuantity)
  {
    this.lstVsPlan          = lstVsPlan;
    this.mapIdAccServModels = mapIdAccServModels;
    this.intQuantity        = intQuantity;
    this.mapIdRoute         = mapIdRoute;
    System.debug('**_** ====>>> this.lstVsPlan '+this.lstVsPlan);
    System.debug('**_** ====>>> this.mapIdAccServModels '+this.mapIdAccServModels);
    System.debug('**_** ====>>> this.intQuantity '+this.intQuantity);
    System.debug('**_** ====>>> this.mapIdRoute '+this.mapIdRoute);
  }
  /**
    * Main interface method Queueable, has the main logic of the process to generate the visiting plans
    * @params:
    * @return void
  **/
  public void execute(QueueableContext context)
  {
    String ownerField = visitPlanSettings.OwnerField__r.DeveloperName;
    ownerField += (ownerField == 'Owner') ? 'Id' : '__c';

    Integer intQuantityAux = 0;
    for(Integer i = (intQuantity -1) * INTQUANTITYLOTE; i < intQuantity * INTQUANTITYLOTE && i < lstVsPlan.size(); i ++)
    {
      VisitPlanWrapper planWrapper = lstVsPlan[i];
      if((planWrapper.objVisitPlan.Tours__r == null || planWrapper.objVisitPlan.Tours__r.isEmpty())
      && planWrapper.lstAccounts.size() > 0 )
      {
          lstEventOnCall       = new list<sObject>();
          idSetAccPlVs         = new set<Id>();
          lstVisitPlan         = mapStrSalesOrgAccVp.get(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOgId__c);
          if(lstVisitPlan == null)
          {
              lstVisitPlan = new list<VisitPlan__c>();
          }

          Time dTimeOrgSales = Time.newInstance(Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringAfter(':')), 0, 0);
          for(AccountByVisitPlan__c objAccByVisit: planWrapper.lstAccounts)
          {
              if(Integer.valueOf(objAccByVisit.WeeklyPeriod__c) != null
              && (objAccByVisit.LastVisitDate__c  == null || getVisitPeriod(objAccByVisit.LastVisitDate__c) == Integer.valueOf(objAccByVisit.WeeklyPeriod__c) || getVisitPeriod(objAccByVisit.LastVisitDate__c) == 0))
              {
                  if(mapIdAccServModels != null && !mapIdAccServModels.isEmpty())
                  {
                      if(mapIdAccServModels.get(objAccByVisit.Account__c).contains(planWrapper.objVisitPlan.VisitPlanType__c))
                      {
                          if(planWrapper.objVisitPlan.VisitPlanType__c  == visitPlanSettings.RTTelesales__c)
                          {
                              lstEventOnCall.add(new ONCALL__Call__c(
                                  ONCALL__POC__c         = objAccByVisit.Account__c,
                                  ONCALL__Date__c        = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringAfter(':')), 0),
                                  ONCALL__Call_Status__c = visitPlanSettings.InitialCallStatus__c,
                                  OwnerId                = (Id) planWrapper.objVisitPlan.Route__r.get('RouteManager__c'),
                                  Name                   = visitPlanSettings.NameOncall__c + ': ' + objAccByVisit.Account__r.ONTAP__SAP_Number__c,
                                  ONCALL__Call_Time__c   = string.valueOf(dTimeOrgSales).substringBeforeLast(':'),
                                  RecordTypeId           = mapCallRecordTypes.get('ISSM_TelesalesCall').Id
                                ));
                            dTimeOrgSales = dTimeOrgSales.addMinutes(15);
                            System.debug('dTimeOrgSales===>>'+dTimeOrgSales);
                            System.debug('*__* =====> '+string.valueOf(dTimeOrgSales).substringBeforeLast(':'));
                          }
                          else
                          {
                              lstEventOnCall.add(new Event(
                                  WhatId                     = objAccByVisit.Account__c,
                                  Subject                    = visitPlanSettings.VisitSubject__c,
                                  StartDateTime              = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringAfter(':')), 0),
                                  EndDateTime                = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringBefore(':')), Integer.valueOf(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringAfter(':')), 0),
                                  ONTAP__Estado_de_visita__c = visitPlanSettings.InitialVisitStatus__c,
                                  EventSubestatus__c         = visitPlanSettings.InitialVisitSubStatus__c,
                                  Sequence__c                = objAccByVisit.Sequence__c,
                                  CustomerId__c              = objAccByVisit.Account__r.ONTAP__SAP_Number__c,
                                  OwnerId                    = (Id) planWrapper.objVisitPlan.Route__r.get('RouteManager__c'),
                                  RecordTypeId               = mapEventRecordTypes.get('Sales').Id
                                ));
                          }
                          idSetAccPlVs.add(objAccByVisit.Id);
                      }
                  }
              }

              if(idSetAccPlVs != null && !idSetAccPlVs.isEmpty()){
                mapIdSetAccPlVs.put(planWrapper.objVisitPlan.Id, idSetAccPlVs);
              }

              if(lstEventOnCall != null && !lstEventOnCall.isEmpty())
                  mapIdVisitListSobjects.put(planWrapper.objVisitPlan.Id, lstEventOnCall);
              }
              lstVisitPlan.add(planWrapper.objVisitPlan);
              if(!lstVisitPlan.isEmpty() && !lstEventOnCall.isEmpty()) {
                  mapStrSalesOrgAccVp.put(planWrapper.objVisitPlan.Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOgId__c, lstVisitPlan);
          }
      }
      intQuantityAux ++;
    }

    System.debug('intQuantityAux =======>> '+intQuantityAux);
    System.debug('lstVsPlan.size() =======>> '+lstVsPlan.size());
    if(intQuantityAux != lstVsPlan.size())
    {
      ID jobID = System.enqueueJob(new DailyVisitplan_qbl(lstVsPlan, mapIdAccServModels,mapIdRoute, ++this.intQuantity));
    }

    System.debug('mapStrSalesOrgAccVp =======>> '+mapStrSalesOrgAccVp);
    System.debug('lstEventOnCall===>>>'+lstEventOnCall);
    //Validate Business Hours
    set<id> setIdAccValids        = validateBusinessHours(mapStrSalesOrgAccVp);

    System.debug('setIdAccValids===>>'+setIdAccValids);
    System.debug('mapIdVisitListSobjects===>>>'+mapIdVisitListSobjects);
    System.debug('mapIdPlVsDate===>>>'+mapIdPlVsDate);

    // Generate Tours with accounts that meet the business hours criteria
    for(VisitPlanWrapper planWrapper : lstVsPlan)
    {
        if(mapIdVisitListSobjects.containsKey(planWrapper.objVisitPlan.Id)
            && setIdAccValids.contains(planWrapper.objVisitPlan.Id))
        {
            Id idRecordType = mapTourRecordTypes.get(planWrapper.objVisitPlan.VisitPlanType__c).Id;
            lstTour.add(new ONTAP__Tour__c(
                ONTAP__TourId__c         = visitPlanSettings.NameTourId__c,
                RecordTypeId             = idRecordType,
                ONTAP__TourDate__c       = System.today().addDays(VISIT_PERIOD_CONF),
                AlternateName__c         = ' ',
                VisitPlan__c             = planWrapper.objVisitPlan.Id,
                ONTAP__TourStatus__c     = mapIdRoute.get(planWrapper.objVisitPlan.Route__r.Id).Tours__r.isEmpty() ? visitPlanSettings.AssignedTourStatus__c : visitPlanSettings.CreatedTourStatus__c,
                TourSubStatus__c         = mapIdRoute.get(planWrapper.objVisitPlan.Route__r.Id).Tours__r.isEmpty() ? visitPlanSettings.AssignedTourSubStatus__c : visitPlanSettings.CreatedTourSubStatus__c,
                ONTAP__IsActive__c       = mapIdRoute.get(planWrapper.objVisitPlan.Route__r.Id).Tours__r.isEmpty() ? true : false,
                OwnerId                  = (Id) planWrapper.objVisitPlan.Route__r.get(ownerField),
                Route__c                 = planWrapper.objVisitPlan.Route__r.Id,
                EstimatedDeliveryDate__c = mapIdPlVsDate.get(planWrapper.objVisitPlan.Id)
            ));
        }
        else
        {
          mapIdSetAccPlVs.remove(planWrapper.objVisitPlan.Id);
        }
    }
    System.debug('mapIdSetAccPlVs===>>>'+mapIdSetAccPlVs);
    System.debug('lstTour====>'+lstTour);
    // insert list Tours
    if(lstTour != null && !lstTour.isEmpty())
        insert lstTour;

    for(Id idVisitPlan : mapIdSetAccPlVs.keyset())
    {
      for(id idAccByVisit: mapIdSetAccPlVs.get(idVisitPlan))
      {
        lstAccountByVisitPlan.add(new AccountByVisitPlan__c(Id = idAccByVisit, LastVisitDate__c = System.today().addDays(VISIT_PERIOD_CONF)));
      }
    }

    List<sobject> lstSobjEventOncall        =   new List<sobject>();
    for(ONTAP__Tour__c objTour : lstTour)
    {
        for(sObject  objSobject : mapIdVisitListSobjects.get(objTour.VisitPlan__c))
        {
            if(objSobject instanceof Event)
            {
                objSobject.put(Schema.Event.VisitList__c, objTour.Id);
            }
            if(objSobject instanceof ONCALL__Call__c)
            {
                objSobject.put(Schema.ONCALL__Call__c.CallList__c, objTour.Id);
                objSobject.put(Schema.ONCALL__Call__c.ONCALL__Delivery_Date__c, mapIdPlVsDate.get(objTour.VisitPlan__c));
            }
            lstSobjEventOncall.add(objSobject);
        }
    }
    System.debug('lstSobjEventOncall === >'+lstSobjEventOncall);
    // Insert Events or On calls
    if(lstSobjEventOncall != null &&  !lstSobjEventOncall.isEmpty())
        insert lstSobjEventOncall;
    // Update lastVisitDate to accounts valids
    if(lstAccountByVisitPlan != null &&  !lstAccountByVisitPlan.isEmpty())
        update lstAccountByVisitPlan;
  }
  /**
    * Method with formula to validate if the accounts have the period to generate the plans of visit
    * @params: - intVisitPeriod Period
    * @return void
  **/
  public static Decimal getVisitPeriod(Date lastVisitDate)
  {
    Date dtStartWeek        = lastVisitDate.toStartOfWeek();
    Decimal intVisitPeriod  = Math.floor(Decimal.valueOf(dtStartWeek.daysBetween(System.today().addDays(VISIT_PERIOD_CONF)) / 7));
    System.debug('dtStartWeek=====>>'+lastVisitDate.addDays(VISIT_PERIOD_CONF*-1).toStartOfWeek());
    System.debug('intVisitPeriod==>>>'+intVisitPeriod);
    return intVisitPeriod;
  }
  /**
    * Method in charge of validating if there is a valid business hours for the date of the visit
    * @params: - setIdAccValids Accounts Valids
    * @return void
  **/
  public static set<id> validateBusinessHours(map<string, list<VisitPlan__c>> mapStrSalesVplan)
  {
      System.debug('mapStrSalesVplan=====>'+mapStrSalesVplan);
      set<Id> setIdAccValids    = new set<id>();
      list<BusinessHours> lstBuHours = [SELECT Id, Name FROM BusinessHours
                                        WHERE Name =: mapStrSalesVplan.keyset()
                                        AND isActive = true];
      System.debug('lstBuHours====>>>>>'+lstBuHours);

      Date dtVisit = System.today().addDays(VISIT_PERIOD_CONF);
      Time tmHours = Time.newInstance(17, 0, 0, 0);
      DateTime dtmVisit = DateTime.valueOfGMT(String.valueOf(DateTime.newInstance(dtVisit, tmHours)));
      DateTime dtmEstimatedDate = DateTime.newInstance(System.today().addDays(VISIT_PERIOD_CONF + 1), tmHours);

      for(BusinessHours objBh : lstBuHours)
      {
        if(mapStrSalesVplan.containsKey(objBh.Name))
        {
          for(VisitPlan__c objVsplan : mapStrSalesVplan.get(objBh.Name))
          {
            System.debug('objBh====>>>>'+objBh);
            System.debug('BusinessHours.isWithin(objBh.id, dtmVisit)===>>'+BusinessHours.isWithin(objBh.id, dtmVisit));
            System.debug('dtmVisit===>>'+dtmVisit);
            System.debug('dtEstimatedDate===>>'+dtmEstimatedDate);
            if(BusinessHours.isWithin(objBh.id, dtmVisit))
            {
                setIdAccValids.add(objVsplan.Id);
                mapIdPlVsDate.put(objVsplan.Id, Date.valueOf(BusinessHours.nextStartDate(objBh.Id, dtmEstimatedDate)));
            }
          }
        }
      }
      return setIdAccValids;
  }
}