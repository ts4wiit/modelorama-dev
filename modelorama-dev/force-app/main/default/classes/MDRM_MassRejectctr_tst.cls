@istest
public class MDRM_MassRejectctr_tst {    
    //This test class tests the Controller MDRM_MassReject_ctr and Batch MDRM_MassReject_btch
    static testmethod void MassReject_Correct(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user with correct information to execute Mass Reject.
        date somedate = date.parse('05/11/2012');
        
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;
        */
        Test.startTest();
        //Run Batch and Controller with the correct User.
        //System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 0;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();
            Controlador.step2();
            Controlador.Confirmar();
            Controlador.Regresar();
            
            Test.stopTest();

       // }
    }
    static testmethod void MassReject_InvalidLimits(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        //Create user with correct information to execute Mass Reject.
        date somedate = date.parse('05/11/2012');
        
       /* String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 1345;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();

            Test.stopTest();

       // }
}
     static testmethod void MassReject_UserwillExceedLimit(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who was already reached the daily limit.
        date somedate = date.today();
        
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =900, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 15;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();
			Controlador.step2();
            Test.stopTest();

       // }
}
     static testmethod void MassReject_UserwillExceedLimit2(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who was already reached the limit.
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =900, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = null);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 15;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();
			Controlador.step2();	
            Test.stopTest();

      //  }
}
     static testmethod void MassReject_CorrectUserUpdate(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user with correct info to update the field MDRM_emails_sent__c
        date somedate = date.today();
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 15;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();
            Controlador.step2();
            Controlador.Confirmar();

            Test.stopTest();

       // }
}
    static testmethod void MassReject_NoAccounts(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 15;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 

        insert Businessman;
        
        /*Create user with correct information to execute Mass Reject.
        date somedate = date.parse('05/11/2012');
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Colima';
            Controlador.availableaccs = 0;
            Controlador.search();

            Test.stopTest();

       // }
}
     static testmethod void MassReject_UserRunningBatch(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 5;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user with correct information to execute Mass Reject.
        date somedate = date.parse('05/11/2012');
        
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =0, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;
       list <AsyncApexJob>batchinprogress = new list<AsyncApexJob>([select createdbyid, id, status, JobType from AsyncApexJob where CreatedById =: usuario.id 
                                                 AND JobType = 'BatchApex' and Status = 'Processing']);
         integer batchsize =batchinprogress.size();
         if (batchsize==0){*/
        Test.startTest();
         //Run Batch and Controller with the correct User.
       //System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 10;
            controlador.batchs =1;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            //Controlador.search();

            Test.stopTest();
       /* }
        }*/
}
    static testmethod void MassReject_UserNullUpdates(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 1;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who has not executed any mass reject.
        date somedate = date.today();
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =null, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();

            Test.stopTest();

       // }
}
    static testmethod void MassReject_UserSurpassesLimit(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 1;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who was executed more than 900 accounts today. 
        date somedate = date.parse('05/11/2012');
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =901, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
      // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();
            Controlador.step2();

            Test.stopTest();

       // }
}
 static testmethod void MassReject_Useriscorrect(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 1;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who was executed less than 900 accounts today. 
        date somedate = date.today();
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =800, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 1;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();

            Test.stopTest();

       // }
}   
    static testmethod void MassReject_Invertedlimits(){ 
        //Create account with correct information for Mass Reject.
        Account Businessman = MDRM_TestUtils_tst.generateAccount(null, null, 123, null); 
        Businessman.MDRM_of_Attachment__c = 1;
        Businessman.ONTAP__Province__c ='Tamaulipas';
        Businessman.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Businessman').getRecordTypeId(); 
        Businessman.MDRM_Stage__c = 'Prospect';
        insert Businessman;
        
        /*Create user who was executed less than 900 accounts today. 
        date somedate = date.today();
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        profile p = [select name, id from profile where name = 'Coordinador Empresario'];
        user usuario = new user(profileid = p.id, MDRM_emails_sent__c =800, UserName=uniqueUserName, lastname ='PruebillasMassReject',
                                Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', TimeZoneSidKey='America/Los_Angeles', MDRM_UpdateDate__c = somedate);
        
        insert usuario;*/
        
        Test.startTest();
         //Run Batch and Controller with the correct User.
       // System.runAs(usuario){        
            MDRM_MassReject_ctr Controlador = new MDRM_MassReject_ctr(new ApexPages.StandardController(Businessman));
            Controlador.lowlimit = 13;
            Controlador.highlimit = 10;
            Controlador.acc.ONTAP__Province__c ='Tamaulipas';
            Controlador.search();

            Test.stopTest();

      // }
}   
}