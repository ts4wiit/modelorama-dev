/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_CloneCase_tst { 

    static testMethod void TestControllerClass() {
		//Objeto de Case
        Case objCase = new Case();
		ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        
        List<ISSM_FieldsCloneCase__c> lstCloneCaseCustomSetting = ISSM_CreateDataTest_cls.fn_CreateFieldsCloneCase(true,'Description');
		Test.startTest();  
        	ISSM_CloneCase_ctr objISSMCloneCase = new ISSM_CloneCase_ctr (sc);
        	objISSMCloneCase.cloneCaseInCaseForce();
        Test.stopTest();
        
    }
}