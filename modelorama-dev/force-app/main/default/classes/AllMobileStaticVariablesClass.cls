/**
 * Static variables class for AllMobile app.
 * <p /><p />
 * @author Alberto Gómez
 */
public virtual class AllMobileStaticVariablesClass {

	//Symbols.
	@TestVisible public static String STRING_SYMBOL_HYPHEN {get {return '-';}}
	@TestVisible public static String STRING_SYMBOL_SEMI_COLON {get {return ';';}}
	@TestVisible public static String STRING_SYMBOL_COMMA {get {return ',';}}
	@TestVisible public static String STRING_SYMBOL_QUOTATION_MARK {get {return '"';}}

	//Character
	@TestVisible public static String STRING_CHARACTER_BLANK {get {return '';}}
	@TestVisible public static String STRING_WHITE_SPACE {get {return ' ';}}
	@TestVisible public static String STRING_PERCENT {get {return '%';}}
	@TestVisible public static String STRING_SIMPLE_QUOTE {get {return '\'';}}

	//Record type variables.
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_ROUTE {get{return'AllMobileRoute';}}
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_OFFICE {get{return'AllMobileSalesOffice';}}
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_ORG {get{return'AllMobileSalesOrg';}}
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_ONTAP_ROUTE_SALES {get{return'Sales';}}
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_ONTAP_ROUTE_IREP {get{return'IREP';}}
	@TestVisible public static String STRING_RECORD_TYPE_DEVELOPER_NAME_SALES_OFFICE {get{return'SalesOffice';}}

	//Endpoint Configuration.
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_READ_ALL {get{return 'AllMobileRouteAppVersionReadAll';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_INSERT {get{return 'AllMobileRouteAppVersionInsert';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_ROUTE_APP_VERSION_UPDATE {get{return 'AllMobileRouteAppVersionUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_READ_ALL {get{return 'AllMobileVersionReadAll';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_INSERT {get{return 'AllMobileVersionInsert';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_VERSION_UPDATE {get{return 'AllMobileVersionUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_READ_ALL {get{return 'AllMobileApplicationReadAll';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_INSERT {get{return 'AllMobileApplicationInsert';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_APPLICATION_UPDATE {get{return 'AllMobileApplicationUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_READ_ALL {get{return 'AllMobileDeviceUserReadAll';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_INSERT {get{return 'AllMobileDeviceUserInsert';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_ENCRYPT {get{return 'AllMobileDeviceUserEncrypt';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_DEVICE_USER_UPDATE {get{return 'AllMobileDeviceUserUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_READ_ALL {get{return 'AllMobileCatUserTypeReadAll';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_INSERT {get{return 'AllMobileCatUserTypeInsert';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_CAT_USER_TYPE_UPDATE {get{return 'AllMobileCatUserTypeUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_EVENT_UPDATE {get{return 'AllMobileEventUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_TOUR_UPDATE {get{return 'AllMobileTourUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_EVENT_DOCUMENT_UPDATE {get{return 'AllMobileEventDocumentUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_COCI_HEADER_UPDATE {get{return 'AllMobileCociHeaderUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_COCI_ITEM_UPDATE {get{return 'AllMobileCociItemUpdate';}}
	@TestVisible public static String STRING_ENDPOINT_CONFIGURATION_NAME_ALLMOBILE_COCI_PAYMENT_UPDATE {get{return 'AllMobileCociPaymentUpdate';}}

	//Trigger events.
	@TestVisible public static String STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_INSERT {get{return 'TriggerEventRouteAppVersionAfterInsert';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_UPDATE {get{return 'TriggerEventRouteAppVersionAfterUpdate';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_AFTER_INSERT_VERSION {get{return 'TriggerEventAfterInsertVersion';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_AFTER_UPDATE_VERSION {get{return 'TriggerEventAfterUpdateVersion';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_AFTER_INSERT_APPLICATION {get{return 'TriggerEventAfterInsertApplication';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_AFTER_UPDATE_APPLICATION {get{return 'TriggerEventAfterUpdateApplication';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_INSERT {get{return 'TriggerEventDeviceUserAfterInsert';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_UPDATE {get{return 'TriggerEventDeviceUserAfterUpdate';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_INSERT_AND_UPDATE {get{return 'TriggerEventDeviceUserAfterInsertAndUpdate';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_INSERT {get{return 'TriggerEventCatUserTypeAfterInsert';}}
	@TestVisible public static String STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_UPDATE {get{return 'TriggerEventCatUserTypeAfterUpdate';}}

	//Ontap Route Application Name.
	@TestVisible public static String STRING_ONTAP_ROUTE_APPLICATION_NAME_PREVENTA {get{return 'PREVENTA';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_APPLICATION_NAME_AUTOVENTA {get{return 'AUTOVENTA';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_APPLICATION_NAME_REPARTO {get{return 'REPARTO';}}

	//Ontap Route Sales mode.
	@TestVisible public static String STRING_ONTAP_ROUTE_SALES_MODE_PRESALES {get{return 'Presales';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_SALES_MODE_AUTOSALES {get{return 'Autosales';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_SALES_MODE_TELESALES {get{return 'Telesales';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_SALES_MODE_DISTRIBUTION {get{return 'Distribution';}}
	@TestVisible public static String STRING_ONTAP_ROUTE_SALES_MODE_BDR {get{return 'BDR';}}

	//Batchable query locator.
	@TestVisible public static String STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_APPLICATIONS {get{return 'SELECT Id, Name, AllMobileApplicationId__c, AllMobileShipType__c, AllMobileLevelValidation__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c';}}
	@TestVisible public static String STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_ROUTE_APP_VERSIONS {get{return 'SELECT Id, Name, AllMobileApplicationLK__c, AllMobileSoftDeleteFlag__c, AllMobileValidEndDate__c, AllMobileValidStartDate__c, AllMobileApplicationId__c, AllMobileRouteId__c, AllMobileVersionId__c, AllMobileRouteAppVersionId__c, AllMobileRouteAppVersionExternalId__c, AllMobileRouteAppVersionExternalIdHK__c, AllMobileSalesOffice__c, AllMobileSalesOrg__c, AllMobileRouteName__c, AllMobileRouteLK__c, AllMobileVersionLK__c, LastModifiedDate FROM AllMobileRouteAppVersion__c';}}
	@TestVisible public static String STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_VERSIONS {get{return 'SELECT Id, Name, AllMobileApplicationMD__c, AllMobileSoftDeleteFlag__c, AllMobileApplicationId__c, AllMobileVersionId__c, LastModifiedDate FROM AllMobileVersion__c';}}
	@TestVisible public static String STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_DEVICE_USERS {get{return 'SELECT Id, Name, AllMobileDeviceUserUserName__c, AllMobileDeviceUserId__c, AllMobileDeviceUserUserTypeLK__c, AllMobileDeviceUserUserTypeLK__r.Name, AllMobileDeviceUserVkburLK__c, AllMobileDeviceUserVkburId__c, AllMobileDeviceUserIsActive__c, AllMobileDeviceUserRouteLK__c, AllMobileDeviceUserRouteLK__r.ONTAP__RouteId__c, AllMobileDeviceUserRouteId__c, AllMobileDeviceUserEmail__c, AllMobileDeviceUserLdapId__c, AllMobileDeviceUserPasswordText__c, AllMobileDeviceUserPasswordEncrypted__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileDeviceUser__c';}}
	@TestVisible public static String STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_CAT_USER_TYPES {get{return 'SELECT Id, Name, AllMobileDeviceUserExternalManagement__c, AllMobileDeviceUserUserTypeDescription__c, AllMobileDeviceUserPermissionsLK__c, AllMobileDeviceUserPermissionsLK__r.AllMobileDeviceUserBinaryPermission__c, AllMobileDeviceUserSoftDeleteFlag__c, LastModifiedDate FROM AllMobileCatUserType__c';}}

	//Device User.
	@TestVisible public static String STRING_DEVICE_USER_CATEGORY_USER_TYPE_BINARY_PERMISSION_ALL_ZERO {get{return '0000000000';}}
	@TestVisible public static String STRING_DEVICE_USER_CATEGORY_USER_TYPE_BINARY_VALUE_ONE {get{return '1';}}
	@TestVisible public static String STRING_DEVICE_USER_CATEGORY_USER_TYPE_BINARY_VALUE_ZERO {get{return '0';}}

	//Tour Status group.
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_GROUP_GREEN {get {return 'Finished';}}
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_GROUP_YELLOW {get {return 'Not Started;Checking Out;In Progress,Checking In;Complete;Downloading;Uploading;Sync Download;Sync Upload;Created;Assigned;Not Completed;Recovered;Settlement';}}
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_GROUP_RED {get {return 'Error;Error Download;Error Upload;Assignment error;Incorrect Data';}}

	//Tour Subestatus group.
	@TestVisible public static String STRING_ONTAP_TOUR_SUBSTATUS_GROUP_GREEN {get {return 'Liquidación terminada sin error;Subida del dispositivo a la BD terminada OK.';}}
	@TestVisible public static String STRING_ONTAP_TOUR_SUBSTATUS_GROUP_YELLOW {get {return 'Tour Creado y asignado en Salesforce;Tour listo para generar archivo download;Listo para descargar al dispositivo;Descarga al dispositivo finalizada correctamente;Usuario declara datos correctos;Cierre de ruta;Liquidación dispositivo;Tour recuperado del dispositivo;Subida del dispositivo a la BD terminada OK;Liquidación no iniciada;Usuario declara datos incorrectos;Descarga iniciada de BD al dispositivo;Descarga finalizada con errores, BD al dispositivo;Subida iniciado del dispositivo a la BD.;Subida a BD terminada con errores;Reapertura de ruta;Error al recuperar tour desde el WS;Salida a ruta iniciada;Salida a ruta iniciada ' + '(' + 'CO' + ')';}}
	@TestVisible public static String STRING_ONTAP_TOUR_SUBSTATUS_GROUP_RED {get {return 'Descarga de SAP finalizada con errores;Subida del dispositivo a BD finalizada con error;Error al subir de Jbridge a SAP;Liquidación terminada con error;Descarga de Salesforce finalizada con errores;ErrFile;ErrUP_RecoveryData_PG;errPG_LIBERA_TRANSP_SAP;Error al generar archivo';}}

	//Tour text color status.
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_VERDE {get {return 'Verde';}}
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_AMARILLO {get {return 'Amarillo';}}
	@TestVisible public static String STRING_ONTAP_TOUR_STATUS_ROJO {get {return 'Rojo';}}

	//Office text color status.
	@TestVisible public static String STRING_ACCOUNT_OFFICE_STATUS_VERDE {get {return 'Verde';}}
	@TestVisible public static String STRING_ACCOUNT_OFFICE_STATUS_AMARILLO {get {return 'Amarillo';}}
	@TestVisible public static String STRING_ACCOUNT_OFFICE_STATUS_ROJO {get {return 'Rojo';}}

	//Org text color status.
	@TestVisible public static String STRING_ACCOUNT_ORG_STATUS_VERDE {get {return 'Verde';}}
	@TestVisible public static String STRING_ACCOUNT_ORG_STATUS_AMARILLO {get {return 'Amarillo';}}
	@TestVisible public static String STRING_ACCOUNT_ORG_STATUS_ROJO {get {return 'Rojo';}}

	//DRV text color status.
	@TestVisible public static String STRING_ACCOUNT_DRV_STATUS_VERDE {get {return 'Verde';}}
	@TestVisible public static String STRING_ACCOUNT_DRV_STATUS_AMARILLO {get {return 'Amarillo';}}
	@TestVisible public static String STRING_ACCOUNT_DRV_STATUS_ROJO {get {return 'Rojo';}}

	//Cockpit Database query.
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT {get {return 'SELECT ONTAP__ActualEnd__c, ONTAP__ActualStart__c, AlternateName__c, CockpitCheckOut__c, Distance__c, ONTAP__DriverId__c, EndMile__c, EstimatedDeliveryDate__c, ONTAP__ExecutionTime__c, DRVId__c, DRV__c, SalesOffice__c, SalesOfficeName__c, OrgId__c, SalesOrg__c, ONTAP__IsActive__c, CockpitPayment__c, CockpitMarket__c, Route__c, RouteDescription__c, ONTAP__RouteId__c, SalesAgent__c, ONTAP__SFUser__c, StartMile__c, SynchronizedHeroku__c, ONTAP__TourDate__c, ONTAP__TourId__c, Name, ONTAP__TourStatus__c, TourSubStatus__c, ONTAP__UserId__c, Vehicle__c, ONTAP__VehicleId__c, VisitPlan__c ';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM {get {return 'FROM ONTAP__Tour__c ';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE {get {return '%\' AND ONTAP__TourDate__c = ';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_TOUR_STATUS_FILTER_WHERE {get {return ' AND ONTAP__TourStatus__c = \'';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_DRVID_WHERE_LIKE {get {return 'WHERE DRVId__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_ORG_WHERE_LIKE {get {return 'WHERE OrgId__c LIKE\'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_OFFICE_WHERE_LIKE {get {return 'WHERE SalesOffice__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_STATUS_BY_ROUTE_WHERE_LIKE {get {return 'WHERE ONTAP__RouteId__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_ALTERNATE_NAME_WHERE_LIKE {get {return 'WHERE AlternateName__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_DRV_WHERE_LIKE {get {return 'WHERE DRVId__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_ORG_WHERE_LIKE {get {return 'WHERE OrgId__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_OFFICE_WHERE_LIKE {get {return 'WHERE SalesOffice__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_ROUTE_WHERE_LIKE {get {return 'WHERE ONTAP__RouteId__c LIKE \'%';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_ROUTE_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT ONTAP__TourId__c, Name, ONTAP__TourDate__c, ONTAP__TourStatus__c, ONTAP__VehicleId__c, RecordTypeId, ONTAP__DriverId__c, AlternateName__c, ONTAP__ActualStart__c, ONTAP__ActualEnd__c, TourSubStatus__c, RecordType.DeveloperName ';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_ALTERNATE_NAME_WHERE_EQUALS {get {return 'WHERE AlternateName__c = \'';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_CLOSE_QUOTE {get {return '\'';}}
	@TestVisible public static String STRING_ONTAP_TOUR_QUERY_LIMIT_1 {get {return ' LIMIT 1';}}
	@TestVisible public static String STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT ONTAP__COCIId__c, ONTAP__COCIDate__c, ONTAP__Tour__r.AlternateName__c ';}}
	@TestVisible public static String STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM ONTAP__COCIHeader__c ';}}
	@TestVisible public static String STRING_CHECKIN_CHECKOUT_HD_QUERY_COCKPITSEARCHCONTROLLER_WHERE {get {return 'WHERE ONTAP__TourId__c  = \'';}}
	@TestVisible public static String STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_WHERE {get {return 'WHERE ONTAP__COCIId__c = ';}}
	@TestVisible public static String STRING_CHECKIN_CHECKOUT_IT_QUERY_COCKPITSEARCHCONTROLLER_AND {get {return ' AND ONTAP__Tour__r.AlternateName__c = \'';}}
	@TestVisible public static String STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT Name, CockpitDocumentType__c, CockpitErrorDescription__c, CockpitTourId__c ';}}
	@TestVisible public static String STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM CockpitEventDocument__c ';}}
	@TestVisible public static String STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR_ID {get {return 'WHERE ((CockpitTourId__c  = \'';}}
	@TestVisible public static String STRING_TOUR_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE_SEQUENCE {get {return '\') AND (CockpitVisitSequence__c = 0)) ';}}
	@TestVisible public static String STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT What.Name, VisitList__r.AlternateName__c, ONTAP__Estado_de_visita__c, Sequence__c ';}}
	@TestVisible public static String STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM Event ';}}
	@TestVisible public static String STRING_CLIENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE {get {return 'WHERE VisitList__r.AlternateName__c  = \'';}}
	@TestVisible public static String STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT CustomerId__c, EndDateTime, Id, ISSM_RouteId__c, Sequence__c, StartDateTime, VisitList__c, VisitList__r.ONTAP__TourId__c, VisitList__r.RecordType.DeveloperName, VisitList__r.ONTAP__TourDate__c, VisitList__r.ONTAP__TourStatus__c, VisitList__r.TourSubStatus__c, ONTAP__Estado_de_visita__c, EventSubestatus__c ';}}
	@TestVisible public static String STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM Event ';}}
	@TestVisible public static String STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE {get {return 'WHERE VisitList__r.ONTAP__TourId__c = \'';}}
	@TestVisible public static String STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_ALTERNATE_NAME {get {return 'WHERE VisitList__r.AlternateName__c = \'';}}
	@TestVisible public static String STRING_VISIT_EVENT_QUERY_COCKPITSEARCHCONTROLLER_WHERE_AND_VISIT {get {return '\' AND What.Name = \'';}}
	@TestVisible public static String STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT CockpitHectoliters__c, NumberCartons__c, HectolitersPlanned__c, NumberCartonsPlanned__c, CockpitEmptyPlanned__c, CockpitEmptyReal__c, VisitList__r.AlternateName__c, What.Name ';}}
	@TestVisible public static String STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM Event ';}}
	@TestVisible public static String STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR {get {return 'WHERE VisitList__r.AlternateName__c = \'';}}
	@TestVisible public static String STRING_CLIENT_SALES_QUERY_COCKPITSEARCHCONTROLLER_WHERE_CLIENT {get {return '\' AND What.Name = \'';}}
	@TestVisible public static String STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT CockpitChargedAmount__c, CockpitPastDueAmount__c, CockpitTotalAmount__c, VisitList__c, What.Name ';}}
	@TestVisible public static String STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM Event ';}}
	@TestVisible public static String STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_WHERE_TOUR {get {return 'WHERE VisitList__r.AlternateName__c = \'';}}
	@TestVisible public static String STRING_CLIENT_BALANCE_QUERY_COCKPITSEARCHCONTROLLER_WHERE_CLIENT {get {return '\' AND What.Name = \'';}}
	@TestVisible public static String STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_SELECT {get {return 'SELECT CockpitTourLK__r.OrgId__c, CockpitTourLK__r.SalesOrg__c, CockpitTourLK__r.SalesOffice__c, CockpitTourLK__r.SalesOfficeName__c, CockpitTourLK__r.ONTAP__RouteId__c, CockpitTourId__c, Name, CockpitDocumentType__c, CockpitErrorDescription__c ';}}
	@TestVisible public static String STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_FROM {get {return 'FROM CockpitEventDocument__c ';}}
	@TestVisible public static String STRING_EVENT_DOC_QUERY_COCKPITSEARCHCONTROLLER_WHERE {get {return 'WHERE CockpitTourId__c =: setStrTourID';}}

	//Visual Level.
	@TestVisible public static String STRING_VISUAL_LEVEL_TOUR_ID {get {return 'Tour ID';}}
	@TestVisible public static String STRING_OPTION {get {return 'option';}}

	//Format date.
	@TestVisible public static String STRING_DATE_FORMAT_DD_MM_H_MM_A {get {return 'dd/MM h:mm a';}}
}