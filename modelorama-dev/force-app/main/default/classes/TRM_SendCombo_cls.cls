/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		:   The class contains 3 methods, 2 are for modification and creation queries and the last one spawns the JSON generation
*
* No.       Fecha              Autor                      Descripción
* 1.0    20-Julio-2018      Oscar Alvarez                   Creación
*******************************************************************************/
public class TRM_SendCombo_cls {
    /*
    * Method Name	: generateJSONForSend
    * Purpose		: Generates the JSON request for the integration
    * @Param        : combo = Combos for integration
    * @Param        : operation = Operation to perform (A = create and B = Modify)
    */
    public static String generateJSONForSend(ISSM_Combos__c combo, List<ISSM_ComboByAccount__c> lstCmbByAccount, List<ISSM_ComboByProduct__c> lstCmbByProduct,String operation){
        
        String strSerializeJson;
        List<Clientes> lstClientes= new List<Clientes>();
        
        for(ISSM_ComboByAccount__c ComboByAccount : lstCmbByAccount){
            lstClientes.add(new Clientes(ComboByAccount.ISSM_ComboNumber__r.ISSM_ExternalKey__c
                                         ,ComboByAccount.ISSM_RegionalSalesDirection__r.ONTAP__SAPCustomerId__c
                                         ,ComboByAccount.ISSM_SalesOrganization__r.ONTAP__SAPCustomerId__c
                                         ,ComboByAccount.ISSM_SalesOffice__r.ONTAP__SAPCustomerId__c
                                         ,ComboByAccount.ISSM_SegmentCode__c
                                         ,ComboByAccount.ISSM_SAPNumber__c
                                         ,String.valueOf(ComboByAccount.ISSM_ExclusionAccount__c)));
        }
        List<Productos> lstProductos= new List<Productos>();
        for(ISSM_ComboByProduct__c ComboByProduct : lstCmbByProduct){
            lstProductos.add(new Productos(ComboByProduct.ISSM_ComboNumber__r.ISSM_ExternalKey__c
                                           ,ComboByProduct.ISSM_MaterialDescription__c
                                           ,ComboByProduct.ISSM_Product__r.ONTAP__ProductId__c
                                           ,ComboByProduct.ISSM_ItemCode__c
                                           ,ComboByProduct.ISSM_ItemQuota__c
                                           ,String.valueOf(ComboByProduct.ISSM_QuantityProduct__c)
                                           ,ComboByProduct.ISSM_UnitMeasure__c
                                           ,ComboByProduct.ISSM_MaterialGroup2__c
                                           ,String.valueOf(ComboByProduct.ISSM_UnitPriceTax__c)
                                           ,String.valueOf(ComboByProduct.ISSM_UnitPrice__c)
                                           ,ComboByProduct.ISSM_CurrencyProduct__c
                                           ,String.valueOf(ComboByProduct.ISSM_TotalPrice__c)
                                           ,ComboByProduct.ISSM_Position__c));
        } 
        System.debug('*** lstProductos size: '+lstProductos.size());
        System.debug('*** lstClientes size: '+lstClientes.size());
        Combo wrapperCombo= new Combo(operation
                                      ,combo.Id
                                      ,combo.CreatedBy.Name
                                      ,combo.LastModifiedBy.Name
                                      ,String.valueOf(combo.ISSM_ApprovalDate__c)
                                      ,String.valueOf(combo.ISSM_StartDate__c)
                                      ,String.valueOf(combo.ISSM_EndDate__c)
                                      ,combo.ISSM_StatusCombo__c
                                      ,combo.ISSM_LongDescription__c
                                      ,combo.ISSM_ShortDescription__c
                                      ,String.valueOf(combo.ISSM_NumberSalesOffice__c)
                                      ,String.valueOf(combo.ISSM_NumberByCustomer__c)
                                      ,combo.ISSM_TypeApplication__c
                                      ,String.valueOf(combo.ISSM_TotalAmount__c)
                                      ,combo.ISSM_Currency__c
                                      ,combo.ISSM_ExternalKey__c
                                      ,lstClientes
                                      ,lstProductos);
        strSerializeJson = Json.serialize(wrapperCombo);
        return  strSerializeJson;
    }
    // wrapper father | COMBOS
    public class Combo{
        public String Operation;
        public String Id;
        public String CreatedBy;
        public String LastModifiedBy;
        public String ISSM_ApprovalDate_c; 		
        public String ISSM_StartDate_c; 			
        public String ISSM_EndDate_c; 			
        public String ISSM_StatusCombo_c; 		
        public String ISSM_LongDescription_c; 	
        public String ISSM_ShortDescription_c; 	
        public String ISSM_NumberSalesOffice_c; 	
        public String ISSM_NumberByCustomer_c; 	
        public String ISSM_TypeApplication_c; 	
        public String ISSM_TotalAmount_c; 		
        public String ISSM_Currency_c; 			
        public String ISSM_ExternalKey_c;			
        public List<Clientes> Clientes; 			
        public List<Productos> Productos;  	
        
        public Combo(String Operation,String Id,String CreatedBy,String LastModifiedBy,String ISSM_ApprovalDate_c,String ISSM_StartDate_c,String ISSM_EndDate_c,String ISSM_StatusCombo_c,String ISSM_LongDescription_c,String ISSM_ShortDescription_c,String ISSM_NumberSalesOffice_c
                     ,String ISSM_NumberByCustomer_c,String ISSM_TypeApplication_c,String ISSM_TotalAmount_c,String ISSM_Currency_c,String ISSM_ExternalKey_c,List<Clientes> Clientes,List<Productos> Productos ){
                         this.Operation 				= Operation;
                         this.Id 						= Id;
                         this.CreatedBy 				= CreatedBy;
                         this.LastModifiedBy 			= LastModifiedBy;
                         this.ISSM_ApprovalDate_c 		= ISSM_ApprovalDate_c;   
                         this.ISSM_StartDate_c 			= ISSM_StartDate_c;
                         this.ISSM_EndDate_c 			= ISSM_EndDate_c;
                         this.ISSM_StatusCombo_c 		= ISSM_StatusCombo_c;
                         this.ISSM_LongDescription_c 	= ISSM_LongDescription_c;
                         this.ISSM_ShortDescription_c 	= ISSM_ShortDescription_c;
                         this.ISSM_NumberSalesOffice_c	= ISSM_NumberSalesOffice_c;
                         this.ISSM_NumberByCustomer_c 	= ISSM_NumberByCustomer_c;
                         this.ISSM_TypeApplication_c 	= ISSM_TypeApplication_c;
                         this.ISSM_TotalAmount_c 		= ISSM_TotalAmount_c;
                         this.ISSM_Currency_c 			= ISSM_Currency_c;
                         this.ISSM_ExternalKey_c		= ISSM_ExternalKey_c;
                         this.Clientes 					= Clientes;
                         this.Productos       			= Productos;
                     }
    }
   // Wrapper child | PRODUCTS 
   public class Productos {
        public String ISSM_ComboNumber_r;
        public String ISSM_MaterialDescription_c; 
        public String ONTAP_ProductId_c; 
        public String ISSM_ItemCode_c; 
        public String ISSM_ItemQuota_c; 
        public String ISSM_QuantityProduct_c; 
        public String ISSM_UnitMeasure_c; 
        public String ISSM_MaterialGroup2_c; 
        public String ISSM_UnitPriceTax_c; 
        public String ISSM_UnitPrice_c; 
        public String ISSM_CurrencyProduct_c; 
        public String ISSM_TotalPrice_c; 
        public String ISSM_Position_c; 
        public Productos(String ISSM_ComboNumber_r,String ISSM_MaterialDescription_c,String ONTAP_ProductId_c,String ISSM_ItemCode_c,String ISSM_ItemQuota_c,String ISSM_QuantityProduct_c 		
                        ,String ISSM_UnitMeasure_c,String ISSM_MaterialGroup2_c,String ISSM_UnitPriceTax_c,String ISSM_UnitPrice_c,String ISSM_CurrencyProduct_c,String ISSM_TotalPrice_c,String ISSM_Position_c ){
            this.ISSM_ComboNumber_r				= ISSM_ComboNumber_r;
            this.ISSM_MaterialDescription_c		= ISSM_MaterialDescription_c;	
            this.ONTAP_ProductId_c				= ONTAP_ProductId_c;
            this.ISSM_ItemCode_c				= ISSM_ItemCode_c;
            this.ISSM_ItemQuota_c				= ISSM_ItemQuota_c;
            this.ISSM_QuantityProduct_c			= ISSM_QuantityProduct_c;
            this.ISSM_UnitMeasure_c				= ISSM_UnitMeasure_c;
            this.ISSM_MaterialGroup2_c			= ISSM_MaterialGroup2_c;
            this.ISSM_UnitPriceTax_c			= ISSM_UnitPriceTax_c;
            this.ISSM_UnitPrice_c				= ISSM_UnitPrice_c;
            this.ISSM_CurrencyProduct_c			= ISSM_CurrencyProduct_c;
            this.ISSM_TotalPrice_c				= ISSM_TotalPrice_c;
            this.ISSM_Position_c 				= ISSM_Position_c;
        }
    }
    // Wrapper child | CUSTOMER
    public class Clientes {
        public String ISSM_ComboNumber_r; 
       	public String ISSM_RegionalSalesDirection_r;
        public String ISSM_SalesOrganization_r;
        public String ISSM_SalesOffice_r;
        public String ISSM_SegmentCode_c;
        public String ISSM_SAPNumber_c;
        public String ISSM_ExclusionAccount_c;
        public Clientes(String ISSM_ComboNumber_r,String ISSM_RegionalSalesDirection_r,String ISSM_SalesOrganization_r,String ISSM_SalesOffice_r,String ISSM_SegmentCode_c,String ISSM_SAPNumber_c,String ISSM_ExclusionAccount_c){
        	this.ISSM_ComboNumber_r 			= ISSM_ComboNumber_r;
            this.ISSM_RegionalSalesDirection_r	=ISSM_RegionalSalesDirection_r;
            this.ISSM_SalesOrganization_r		=ISSM_SalesOrganization_r;
            this.ISSM_SalesOffice_r				=ISSM_SalesOffice_r;
            this.ISSM_SegmentCode_c				=ISSM_SegmentCode_c;
            this.ISSM_SAPNumber_c				=ISSM_SAPNumber_c;
            this.ISSM_ExclusionAccount_c		=ISSM_ExclusionAccount_c;	
        }
    }
}