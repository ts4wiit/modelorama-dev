/**
 * Test class for AllMobileDeviceUserSchedulerClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserSchedulerTest {

	/**
	 * Method to test execute method.
	 */
	static testMethod void testExecute() {

		//Set time.
		String strScheduleTimeDeviceUser = '0 57 * * * ?';

		//Start test.
		Test.startTest();

		//Schedule CatUserType
		AllMobileDeviceUserSchedulerClass objAllMobileDeviceUserSchedulerClass = new AllMobileDeviceUserSchedulerClass();
		System.schedule('jobDeviceUser', strScheduleTimeDeviceUser, objAllMobileDeviceUserSchedulerClass);

		//Stop test.
		Test.stopTest();
	}
}