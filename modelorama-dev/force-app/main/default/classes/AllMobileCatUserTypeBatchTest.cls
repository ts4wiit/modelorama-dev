/**
 * Test class for AllMobileCatUserTypeBatchClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileCatUserTypeBatchTest {

	/**
	 * Method to setup data.
	 */
	@testSetup
	static void setup() {

		//Create a List of CatUserType.
		AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeAll = AllMobileUtilityHelperTest.createAllMobilePermissionCategoryUserType('1111111110', 'ZZZ', 'ZZZ', '1111111110');
        insert objAllMobilePermissionCategoryUserTypeAll;
		List<AllMobileCatUserType__c> lstAllMobileCatUserType = new List<AllMobileCatUserType__c>();
		for(Integer intI = 0; intI < 9; intI++) {
			lstAllMobileCatUserType.add(AllMobileUtilityHelperTest.createAllMobileCatUserType('ZZ' + intI, false, false, 'Almacén ZZZ' + intI, 'ZZ' + intI, objAllMobilePermissionCategoryUserTypeAll));
		}
		insert lstAllMobileCatUserType;
	}

	/**
	 * Method to test AllMobileCatUserTypeBatchClass.
	 */
	static testMethod void testAllMobileCatUserTypeBatchClass() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		AllMobileCatUserTypeBatchClass objAllMobileCatUserTypeBatchClass = new AllMobileCatUserTypeBatchClass();
		Id idObjAllMobileCatUserTypeBatchClass = Database.executeBatch(objAllMobileCatUserTypeBatchClass);

		//Stop test.
		Test.stopTest();
	}
}