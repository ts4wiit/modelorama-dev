/**
 * Developed by:	   Avanxo México
 * Author:			   Oscar Alvarez
 * Project:			   AbInbev - CAM
 * Description:		   Clase apex for generate template email. For Salesoffice, UEN, DRV and corporative. 
 *
 *  No.        Fecha                  Autor               Descripción
 *  1.0    13-Noviembre-2018         Oscar Alvarez             CREATION
 *
 */
public with sharing class ISSM_CAM_GenerateTemplateEmail_cls {
	public ISSM_CAM_GenerateTemplateEmail_cls() {}

	public static String generateTemplateEmailCenter(ISSM_CAM_ValidateCutover_ctr.WprCounterReason[] lstWprCounterReason
													,String center
													,String centerCode
													,String totalErrors
													,String initialBasis
													,String totalCountedEquipment
                                                    ,String level
                                                    ,String URL){
	
	String  strTemplateEmail  = '<p>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail += 'Estimado usuario,'; 
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';
		strTemplateEmail += '<p>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">Se le informa que el periodo de cutover de ';
		strTemplateEmail += '</span>';
		strTemplateEmail += '<b style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail +=  center;
		strTemplateEmail += '</b>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail += ' se ha ejecutado sin excepción en la fecha designada. Terminó con los siguientes resultados:';
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';
		strTemplateEmail += '<ul>';
		strTemplateEmail += '<li>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
		strTemplateEmail += 'Base inicial (periodo anterior): '; 
		strTemplateEmail += '</span>';
		strTemplateEmail += '<b style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
		strTemplateEmail +=  initialBasis;
		strTemplateEmail += '</b>';
		strTemplateEmail += '</li>';
		strTemplateEmail += '<li>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
		strTemplateEmail += 'Total de equipos contados: '; 
		strTemplateEmail += '</span>';
		strTemplateEmail += '<b style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
		strTemplateEmail += totalCountedEquipment;
		strTemplateEmail += '</b>';
		strTemplateEmail += '</li>';
		strTemplateEmail += '<li>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">Total de equipos con diferencia: ';
		strTemplateEmail += '</span>';
		strTemplateEmail += '<b style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
		strTemplateEmail +=  totalErrors;
		strTemplateEmail += '</b>';
		strTemplateEmail += '</li>';

		Boolean isOKCenter = true;
		for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReason : lstWprCounterReason){
			if(wprCounterReason.ReasonEnglish.equals('Surplus equipement')){
				strTemplateEmail += '<li>';
				strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">Total de equipos sobrantes en almacén: ';
				strTemplateEmail += '</span>';
				strTemplateEmail += '<b style="font-size: 12pt; font-family: Arial, sans-serif; color: black; background-color: rgb(255, 255, 255);">';
				strTemplateEmail +=  wprCounterReason.Counter;
				strTemplateEmail += '</b>';
				strTemplateEmail += '</li>';
			}
			if(wprCounterReason.ReasonEnglish.equals('OK')) isOKCenter = false;
		}
		strTemplateEmail += '</ul>';

		if (isOKCenter || level != 'CENTRO'){
			strTemplateEmail += '<table class="ql-table-blob" border="1" style="margin-left: 35.4pt;">';
			strTemplateEmail += '<tbody>';
			strTemplateEmail += '<tr>';
			strTemplateEmail += '<td colspan="2" rowspan="1" valign="top" width="538" style="width: 403.8pt;">';
			strTemplateEmail += '<p align="center" class="MsoNormal" style="text-align: center;">';
			strTemplateEmail += '<b>';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail +=  center;
			strTemplateEmail += '</span>';
			strTemplateEmail += '</b>';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail += '</span>';
			strTemplateEmail += '</p>';
			strTemplateEmail += '</td>';
			strTemplateEmail += '</tr>';
			strTemplateEmail += '<tr>';
			strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="349" style="width: 262.05pt;">';
			strTemplateEmail += '<p align="center" class="MsoNormal" style="text-align: center;">';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail += 'MOTIVO DE DIFERENCIA';
			strTemplateEmail += '</span>';
			strTemplateEmail += '</p>';
			strTemplateEmail += '</td>';
			strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="189" style="width: 5.0cm;">';
			strTemplateEmail += '<p align="center" class="MsoNormal" style="text-align: center;">';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail += 'TOTAL DE EQUIPOS';
			strTemplateEmail += '</span>';
			strTemplateEmail += '</p>';
			strTemplateEmail += '</td>';
			strTemplateEmail += '</tr>';
			// seccion de motivos - loop

			for(ISSM_CAM_ValidateCutover_ctr.WprCounterReason wprCounterReason : lstWprCounterReason){
				if(!wprCounterReason.ReasonEnglish.equals('Surplus equipement') && !wprCounterReason.ReasonEnglish.equals('OK')){
					strTemplateEmail += '<tr>';
					strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="349" style="width: 262.05pt;">';
					strTemplateEmail += '<p class="MsoNormal" style="">';
					strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
					strTemplateEmail +=  wprCounterReason.ReasonSpanish;
					strTemplateEmail += '</span>';
					strTemplateEmail += '</p>';
					strTemplateEmail += '</td>';
					strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="189" style="width: 5.0cm;">';
					strTemplateEmail += '<p align="center" class="MsoNormal" style="text-align: center;">';
					strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
					strTemplateEmail +=  wprCounterReason.Counter;
					strTemplateEmail += '</span>';
					strTemplateEmail += '</p>';
					strTemplateEmail += '</td>';
					strTemplateEmail += '</tr>';
				}
			}

			strTemplateEmail += '<tr>';
			strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="349" style="width: 262.05pt;">';
			strTemplateEmail += '<p align="right" class="MsoNormal" style="text-align: right;">';
			strTemplateEmail += '<b>';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail += 'TOTAL';
			strTemplateEmail += '</span>';
			strTemplateEmail += '</b>';
			strTemplateEmail += '</p>';
			strTemplateEmail += '</td>';
			strTemplateEmail += '<td colspan="1" rowspan="1" valign="top" width="189" style="width: 5.0cm;">';
			strTemplateEmail += '<p align="center" class="MsoNormal" style="text-align: center;">';
			strTemplateEmail += '<b>';
			strTemplateEmail += '<span style="font-size: 12.0pt; color: black;">';
			strTemplateEmail +=  totalErrors;
			strTemplateEmail += '</span>';
			strTemplateEmail += '</b>';
			strTemplateEmail += '</p>';
			strTemplateEmail += '</td>';
			strTemplateEmail += '</tr>';
			strTemplateEmail += '</tbody>';
			strTemplateEmail += '</table>';
		}

		strTemplateEmail += '<p>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail += 'Para visualizar reporte de clic en el siguiente link: ';
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';
		strTemplateEmail += '<p>';
		strTemplateEmail += '<span style="font-size: 7pt; font-family: Arial, sans-serif; color: black;">';
        strTemplateEmail +=  URL;
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';

		strTemplateEmail += '<p>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail += 'Muchas gracias.';
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';
		strTemplateEmail += '<p>';
		strTemplateEmail += '<span style="font-size: 12pt; font-family: Arial, sans-serif; color: black;">';
		strTemplateEmail += 'Quedamos atentos a sus comentarios.';
		strTemplateEmail += '</span>';
		strTemplateEmail += '</p>';	
		system.debug('strTemplateEmail'+strTemplateEmail);  
		return strTemplateEmail;       
	}
}