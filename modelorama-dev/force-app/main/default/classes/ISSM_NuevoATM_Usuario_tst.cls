/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_NuevoATM_Usuario_tst
Versión : 1.0
Fecha de Creación : 20 Abril 2018
Funcionalidad : Clase de prueba para el controlador ISSM_NuevoATM_Usuario_ctr
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        20 - Abril - 2018      Versión Original
*************************************************************************************/
@isTest
public class ISSM_NuevoATM_Usuario_tst {
    static testmethod void staticResourceLoad() {

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'admin', Email='systemadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');

        test.startTest();

            ApexPages.StandardController sc = new ApexPages.standardController(u);
            ISSM_NuevoATM_Usuario_ctr controller = new ISSM_NuevoATM_Usuario_ctr(sc);

        test.stopTest();
    }
}