/********************************************************************************** 
    Company:            Avanxo México
    Author:             Oscar Alvarez Garcia
    Customer:           AbInbev - CAM
    Descripción:        Class test for class controller - ISSM_CAM_DataDisplay_ctr

    Information about changes (versions)
===================================================================================
    No.    Date                 Author                      Description
    1.0    06-Nobiembre-2018    Oscar Alvarez Garcia           Creation
**********************************************************************************/
@isTest
private class ISSM_CAM_ValidateCutover_tst {
	public static String IdRecordTypeHistory  = [SELECT Id FROM RecordType WHERE DeveloperName = 'History' AND SobjectType ='ISSM_CutOver_History__c'].Id;
	public static String recTypeAccountSalesOrg                    = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOrg'].Id;
    public static String recTypeAccountSalesOffice                 = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOffice'].Id;
    public static String recTypeAccountRegionalSalesDivision       = [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].Id;
    public static String IdRecordTypeUEN                           = [SELECT Id FROM RecordType WHERE DeveloperName = 'UEN' AND SobjectType ='ISSM_CutOver_History__c'].Id;

	@testSetup static void loadData() {

		User user3122 						= new User();
			user3122.Alias 					= 'user3122';
			user3122.Email					= 'user3116@testorg.com';
			user3122.LastName				= 'Testing';
			user3122.UserName				= 'user3116@testorg.com';
			user3122.ONTAP__Business_Unit__c= '3122-CMM Morelos';
			user3122.TimeZoneSidKey			= 'America/Mexico_City';
			user3122.LocaleSidKey			= 'es';
			user3122.EmailEncodingKey		= 'ISO-8859-1';
			user3122.ProfileId				= [SELECT Id FROM Profile WHERE Name = 'Asset Manager'].Id;
			user3122.LanguageLocaleKey		= 'es';	

		insert new ISSM_EstatusSFDC_CutOverReason__c(ISSM_CAM_StatusSFDC__c = 'Loss',
                                               Name = 'Lost Unit');
		insert user3122;

		Account  accDRV 	= new Account();
	        accDRV.Name 		 = 'Name DRV';
	        accDRV.RecordTypeId = recTypeAccountRegionalSalesDivision;
        Insert accDRV;

        Account  accOrg = new Account();
	        accOrg.Name 					= 'Name SalesOrg';
	        accOrg.RecordTypeId 			= recTypeAccountSalesOrg;
	        accOrg.ONTAP__SalesOgId__c	= '3108';
	        accOrg.ISSM_ParentAccount__c = accDRV.Id;
        Insert accOrg;		

		Account salesOffice1  								= new Account();
			salesOffice1.Name								= 'CMM Tetecala';
			salesOffice1.ONTAP__SAPCustomerId__c			= 'FT05';
			salesOffice1.ONTAP__SalesOgId__c				= '3108';
			salesOffice1.RecordTypeId						= recTypeAccountSalesOffice;
			salesOffice1.ONTAP__SalesOffId__c				= 'FT05';
		insert salesOffice1;

		Account acc 					=  new Account();
            acc.Name 					= 'ACCTEST1';
            acc.ontap__sapcustomerid__c	= 'TEST';
            acc.ontap__salesoffid__c	= 'OFF';
            acc.ontap__salesogid__c		= 'ORG';
            acc.ONTAP__SAP_Number__c	= '0100ACTEST';
            acc.ISSM_Is_Blacklisted__C	= False;
            acc.ONTAP__Email__c			= 'test@est.com';
            acc.ONTAP__SAP_Number__c    = '1234567890';
	        acc.ONTAP__LegalName__c     = 'Razon social del cliente';
	        acc.ONTAP__Street__c		= 'Didirección del cliente';
	        acc.ONTAP__Street_Number__c	= '234';
	        acc.ONCALL__Postal_Code__c	= '01250';
		    acc.ONTAP__Province__c		= 'Crovince test';
	        acc.ONTAP__SalesOgId__c		= '3116';
	        acc.ONTAP__Colony__c		= 'Colony test';
	        acc.ONTAP__Municipality__c	= 'Municipality test';
	        acc.ISSM_RFC__c				= 'XAXX010101000';
        insert acc; 

		ISSM_Asset__c asset 				= new ISSM_Asset__c();
			asset.name						= 'Unassignation';
			asset.Equipment_Number__c		= '0000UNASSIGNATION_';
			asset.ISSM_Serial_Number__c		= 'UNASSIGNATION_';
			asset.ISSM_Material_number__c	= '8000300';
			asset.ISSM_Status_SFDC__c		= 'Free Use';
			asset.ISSM_CutOverReason__c		= 'Lost Unit';
        insert asset;

        ISSM_Asset__c asset1 				=  new ISSM_Asset__c();
			asset1.name						= 'Unassignation2';
			asset1.Equipment_Number__c		= '0000UNASSIGNATION2';
			asset1.ISSM_Serial_Number__c	= 'UNASSIGNATION2';
			asset1.ISSM_Material_number__c	= '8000302';
			asset1.ISSM_Status_SFDC__c		= 'Free Use';
			asset1.ISSM_CutOverReason__c	= 'Lost Unit';
        insert asset1;

		ONTAP__Case_Force__c caseForce = new  ONTAP__Case_Force__c();
			caseForce.ONTAP__Subject__c				='Retiro';
			caseForce.ISSM_Asset_CAM__c 			= asset.id;
			caseForce.ONTAP__Account__c 			= acc.id;
			caseForce.ISSM_TypificationLevel1__c 	= 'Refrigeración';
			caseForce.ISSM_ClassificationLevel2__c 	= 'Retiro de equipo';
			caseForce.ISSM_ApprovalStatus__c		= 'Approved';
        	caseForce.ISSM_SAPOrderDate__c			= date.today().toStartOfWeek()+7;
			caseForce.ISSM_Printed_times__c			= 1;
        insert caseForce;

        ISSM_CutOver_History__c cutOverHistory1 	= new ISSM_CutOver_History__c();
			cutOverHistory1.ISSM_Asset_Description__c	= 'Equipo sobrante 1';
			cutOverHistory1.ISSM_Asset_number__c		= '206030003504';
			cutOverHistory1.ISSM_CAM_Leftover__c		= true;
			cutOverHistory1.ISSM_CAM_Temporal__c		= false;
			cutOverHistory1.ISSM_Centre__c				= 'GC08';
			cutOverHistory1.ISSM_Equipment_number__c	= '300452834';
			cutOverHistory1.ISSM_Material_number__c		= '8000930';
			cutOverHistory1.ISSM_Serial_Number__c		= '3010272';
			cutOverHistory1.RecordTypeId              	= IdRecordTypeHistory;
		insert cutOverHistory1;

		ISSM_CutOver_History__c cutOverHistory2 	= new ISSM_CutOver_History__c();
			cutOverHistory2.ISSM_Asset_Description__c	= 'Equipo sobrante 2';
			cutOverHistory2.ISSM_Asset_number__c		= '206030000055';
			cutOverHistory2.ISSM_CAM_Leftover__c		= true;
			cutOverHistory2.ISSM_CAM_Temporal__c		= false;
			cutOverHistory2.ISSM_Centre__c				= 'GC08';
			cutOverHistory2.ISSM_Equipment_number__c	= '300058337';
			cutOverHistory2.ISSM_Material_number__c		= '8000426';
			cutOverHistory2.ISSM_Serial_Number__c		= '2994468';
			cutOverHistory2.RecordTypeId              	= IdRecordTypeHistory;
		insert cutOverHistory2;

		ISSM_CutOver_History__c cutOverHistory3 	= new ISSM_CutOver_History__c();
			cutOverHistory3.ISSM_Asset_Description__c	= 'Equipo sobrante 3';
			cutOverHistory3.ISSM_Asset_number__c		= '206030000080';
			cutOverHistory3.ISSM_CAM_Leftover__c		= false;
			cutOverHistory3.ISSM_CAM_Temporal__c		= true;
			cutOverHistory3.ISSM_Centre__c				= 'GC05';
			cutOverHistory3.ISSM_Equipment_number__c	= '300064917';
			cutOverHistory3.ISSM_Material_number__c		= '8000703';
			cutOverHistory3.ISSM_Serial_Number__c		= '2999819';
			cutOverHistory3.RecordTypeId              	= IdRecordTypeHistory;
		insert cutOverHistory3;
		ISSM_CutOver_History__c cutOverHistoryUEN2 		= new ISSM_CutOver_History__c();
			cutOverHistoryUEN2.Name								= 'CMM Hidalgo';
			cutOverHistoryUEN2.RecordTypeId              		= IdRecordTypeUEN;			
			cutOverHistoryUEN2.ISSM_Code_Name__c				= '3108';			
			cutOverHistoryUEN2.ISSM_Total_errors__c				= 11;
			cutOverHistoryUEN2.ISSM_Summary_Results__c			= 'Estimado';
			cutOverHistoryUEN2.ISSM_CAM_TotalCountedEquipment__c= 1070;
			cutOverHistoryUEN2.ISSM_CAM_InitialBaseEquipment__c	= 0;
			cutOverHistoryUEN2.ISSM_CAM_TotalSurplusEquipment__c= 0;
			cutOverHistoryUEN2.ISSM_CAM_SummaryReasonCounter__c	= '[{"ReasonSpanish":"OK","ReasonEnglish":"OK","Counter":0},{"ReasonSpanish":"Número de activo incorrecto","ReasonEnglish":"Incorrect Asset Number","Counter":1}]';			
		insert cutOverHistoryUEN2;
		
	}
	
	@isTest static void testGetPeriodoCutOver() {
		Test.startTest();
			ISSM_CAM_ValidateCutover_ctr.getPeriodoCutOver();
		Test.stopTest();
	}
	@isTest static void testGetRecords() {
		Test.startTest();
			String[] lstSalesofficeCutOver= new List<String>(); 
			lstSalesofficeCutOver.add('FT06');
			String[] lstSalesofficeNotCutOver= new List<String>(); 
			ISSM_CAM_ValidateCutover_ctr.getRecords('ISSM_Asset__c',
													'Name',
													'50000',
													'ISSM_CAM_NameColCutoverTable'
													,true
													,lstSalesofficeCutOver
													,lstSalesofficeNotCutOver);
		Test.stopTest();
	}
	@isTest static void testSaveRecords() {
		Test.startTest();
			ISSM_Asset__c[] lstAsset =[SELECT Id, ISSM_CutOverReason__c,ISSM_CutOver__c FROM ISSM_Asset__c];
			String[] lstIdAsset = new List<String>();
			String[] lstAssetWithCutOver = new List<String>();
			for(ISSM_Asset__c asset : lstAsset){
				lstIdAsset.add(asset.Id );
				lstAssetWithCutOver.add(asset.Id + ',' + asset.ISSM_CutOverReason__c + ',"false"');
			}
            Map<String,String> mapSalesofficeForReason = new Map<String,String>();

            ISSM_CutOver_History__c[] lstCutOverHistoryBackUp= [SELECT ISSM_Asset_Description__c	
																       ,ISSM_Asset_number__c		
																       ,ISSM_CAM_Leftover__c		
																       ,ISSM_CAM_Temporal__c		
																       ,ISSM_Centre__c				
																       ,ISSM_Equipment_number__c	
																       ,ISSM_Material_number__c		
																       ,ISSM_Serial_Number__c		
																FROM ISSM_CutOver_History__c 
																WHERE RecordTypeId =: IdRecordTypeHistory];
		    
            
			ISSM_CutOver_History__c[] lstCutOverHistoryBackUp2 = new List<ISSM_CutOver_History__c>();													
            ISSM_CutOver_History__c[] lstCutOverHistoryList = lstCutOverHistoryBackUp.clone();
            ISSM_CutOver_History__c[] lstCutOverHistoryList2= new List<ISSM_CutOver_History__c>();

            ISSM_CutOver_History__c cutOverHistory4 		= new ISSM_CutOver_History__c();
				cutOverHistory4.ISSM_Asset_Description__c	= 'Equipo sobrante 4';
				cutOverHistory4.ISSM_Asset_number__c		= '206030000288';
				cutOverHistory4.ISSM_CAM_Leftover__c		= false;
				cutOverHistory4.ISSM_CAM_Temporal__c		= true;
				cutOverHistory4.ISSM_Centre__c				= 'GC05';
				cutOverHistory4.ISSM_Equipment_number__c	= '300062565';
				cutOverHistory4.ISSM_Material_number__c		= '8000551';
				cutOverHistory4.ISSM_Serial_Number__c		= '2974310';
			lstCutOverHistoryList.add(cutOverHistory4);
			lstCutOverHistoryList2.add(cutOverHistory4);
			ISSM_CutOver_History__c cutOverHistory5 		= new ISSM_CutOver_History__c();
				cutOverHistory5.ISSM_Asset_Description__c	= 'Equipo sobrante 5';
				cutOverHistory5.ISSM_Asset_number__c		= '206030000360';
				cutOverHistory5.ISSM_CAM_Leftover__c		= true;
				cutOverHistory5.ISSM_CAM_Temporal__c		= false;
				cutOverHistory5.ISSM_Centre__c				= 'GC05';
				cutOverHistory5.ISSM_Equipment_number__c	= '300057807';
				cutOverHistory5.ISSM_Material_number__c		= '8000551';
				cutOverHistory5.ISSM_Serial_Number__c		= '2985089';			
			lstCutOverHistoryList.add(cutOverHistory5);
			lstCutOverHistoryList2.add(cutOverHistory5);

			String cutOverHistoryList;
			String cutOverHistoryBackUp = JSON.serialize(lstCutOverHistoryBackUp);			
            String operation = 'A';

            cutOverHistoryList 		= JSON.serialize(lstCutOverHistoryList2);
            cutOverHistoryBackUp 	= JSON.serialize(lstCutOverHistoryBackUp2);
            ISSM_CAM_ValidateCutover_ctr.saveRecords(mapSalesofficeForReason,lstIdAsset,lstAssetWithCutOver,cutOverHistoryList,cutOverHistoryBackUp,operation);

            cutOverHistoryBackUp 	= JSON.serialize(lstCutOverHistoryBackUp);

		    cutOverHistoryList 		= JSON.serialize(lstCutOverHistoryList);
            ISSM_CAM_ValidateCutover_ctr.saveRecords(mapSalesofficeForReason,lstIdAsset,lstAssetWithCutOver,cutOverHistoryList,cutOverHistoryBackUp,operation);

            lstCutOverHistoryBackUp= [SELECT ISSM_Asset_Description__c	
																       ,ISSM_Asset_number__c		
																       ,ISSM_CAM_Leftover__c		
																       ,ISSM_CAM_Temporal__c		
																       ,ISSM_Centre__c				
																       ,ISSM_Equipment_number__c	
																       ,ISSM_Material_number__c		
																       ,ISSM_Serial_Number__c		
																FROM ISSM_CutOver_History__c 
																WHERE RecordTypeId =: IdRecordTypeHistory];
			cutOverHistoryBackUp = JSON.serialize(lstCutOverHistoryBackUp);
            cutOverHistoryList 		= JSON.serialize(lstCutOverHistoryList2);
            ISSM_CAM_ValidateCutover_ctr.saveRecords(mapSalesofficeForReason,lstIdAsset,lstAssetWithCutOver,cutOverHistoryList,cutOverHistoryBackUp,operation);           

		Test.stopTest();
	}
	@isTest static void testSaveHistoricalSummary() {
		Test.startTest();
			Map<String,String> mapSalesofficeForReason = new Map<String,String>();
			String[] lstGC00 = new List<String>();
			String[] lstGC08 = new List<String>();
            String operation = 'B';

            ISSM_Asset__c[] lstAsset =[SELECT Id, ISSM_CutOverReason__c,ISSM_CutOver__c FROM ISSM_Asset__c];
			String[] lstIdAsset = new List<String>();
			String[] lstAssetWithCutOver = new List<String>();
			for(ISSM_Asset__c asset : lstAsset){
				lstIdAsset.add(asset.Id );
				lstAssetWithCutOver.add(asset.Id + ',' + asset.ISSM_CutOverReason__c + ',"false"');
			}

			lstGC00.add('GC00,Incorrect Asset Number,1,GC00-CMM M Pachuca,Número de activo incorrecto,551');
			lstGC00.add('GC00,Incorrect Serial Number,2,GC00-CMM M Pachuca,Número de serie incorrecto,551');
			lstGC00.add('GC00,Incorrect Equipet Number,1,GC00-CMM M Pachuca,Número de equipo incorrecto,551');
			lstGC00.add('GC00,Incorrect Material Number,1,GC00-CMM M Pachuca,Número de material incorrecto,551');
			lstGC00.add('GC00,Missing Unit,1,GC00-CMM M Pachuca,Equipo Faltante,551');
			lstGC00.add('GC00,Equipment with legal,1,GC00-CMM M Pachuca,Equipo en Jurídico,551');
			lstGC00.add('GC00,Lost Unit,1,GC00-CMM M Pachuca,Equipo perdida en el mercado,551');
			lstGC08.add('GC08,OK,0,GC08-CMM Tula,OK,257');
			mapSalesofficeForReason.put('GC00',JSON.serialize(lstGC00));
			mapSalesofficeForReason.put('GC08',JSON.serialize(lstGC08));



			ISSM_CutOver_History__c[] lstCutOverHistoryBackUp = new List<ISSM_CutOver_History__c>();	
            ISSM_CutOver_History__c[] lstCutOverHistoryList= new List<ISSM_CutOver_History__c>();

            ISSM_CutOver_History__c cutOverHistory4 		= new ISSM_CutOver_History__c();
				cutOverHistory4.ISSM_Asset_Description__c	= 'Equipo sobrante 4';
				cutOverHistory4.ISSM_Asset_number__c		= '206030000288';
				cutOverHistory4.ISSM_CAM_Leftover__c		= false;
				cutOverHistory4.ISSM_CAM_Temporal__c		= true;
				cutOverHistory4.ISSM_Centre__c				= 'GC05';
				cutOverHistory4.ISSM_Equipment_number__c	= '300062565';
				cutOverHistory4.ISSM_Material_number__c		= '8000551';
				cutOverHistory4.ISSM_Serial_Number__c		= '2974310';
			lstCutOverHistoryList.add(cutOverHistory4);
			ISSM_CutOver_History__c cutOverHistory5 		= new ISSM_CutOver_History__c();
				cutOverHistory5.ISSM_Asset_Description__c	= 'Equipo sobrante 5';
				cutOverHistory5.ISSM_Asset_number__c		= '206030000360';
				cutOverHistory5.ISSM_CAM_Leftover__c		= true;
				cutOverHistory5.ISSM_CAM_Temporal__c		= false;
				cutOverHistory5.ISSM_Centre__c				= 'GC05';
				cutOverHistory5.ISSM_Equipment_number__c	= '300057807';
				cutOverHistory5.ISSM_Material_number__c		= '8000551';
				cutOverHistory5.ISSM_Serial_Number__c		= '2985089';
			lstCutOverHistoryList.add(cutOverHistory5);
			
            String cutOverHistoryList 		= JSON.serialize(lstCutOverHistoryList);
            String cutOverHistoryBackUp 	= JSON.serialize(lstCutOverHistoryBackUp);
            ISSM_CAM_ValidateCutover_ctr.saveRecords(mapSalesofficeForReason,lstIdAsset,lstAssetWithCutOver,cutOverHistoryList,cutOverHistoryBackUp,operation);


		Test.stopTest();
	}
	@isTest static void testSaveHistoricalSummaryUEN() {
		Test.startTest();
			Map<String,ISSM_CutOver_History__c> mapSalesofficeForRecordHistory = new Map<String,ISSM_CutOver_History__c>();
			ISSM_CutOver_History__c cutOverHistorySO4 = new ISSM_CutOver_History__c();
				cutOverHistorySO4.ISSM_CAM_TotalSurplusEquipment__c = 10;
	            cutOverHistorySO4.ISSM_Total_errors__c				= 10;
	            cutOverHistorySO4.ISSM_CAM_TotalCountedEquipment__c	= 10;
				cutOverHistorySO4.ISSM_CAM_SummaryReasonCounter__c	= '[{"ReasonSpanish":"Número de activo incorrecto","ReasonEnglish":"Incorrect Asset Number","Counter":1},{"ReasonSpanish":"Número de serie incorrecto","ReasonEnglish":"Incorrect Serial Number","Counter":2},{"ReasonSpanish":"Número de equipo incorrecto","ReasonEnglish":"Incorrect Equipet Number","Counter":1},{"ReasonSpanish":"Número de material incorrecto","ReasonEnglish":"Incorrect Material Number","Counter":1},{"ReasonSpanish":"Equipo Faltante","ReasonEnglish":"Missing Unit","Counter":1},{"ReasonSpanish":"Equipo en Jurídico","ReasonEnglish":"Equipment with legal","Counter":1},{"ReasonSpanish":"Equipo perdida en el mercado","ReasonEnglish":"Lost Unit","Counter":1}]';
				cutOverHistorySO4.ISSM_CAM_InitialBaseEquipment__c	= 10;

			
			mapSalesofficeForRecordHistory.put('FT05',cutOverHistorySO4);
			ISSM_CAM_ValidateCutover_ctr.saveHistoricalSummaryUEN(mapSalesofficeForRecordHistory);
		
		Test.stopTest();
				}
}