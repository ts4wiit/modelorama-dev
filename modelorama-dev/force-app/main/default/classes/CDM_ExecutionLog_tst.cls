/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_ExecutionLog_ctr". This class must to implements "SeeAllData" because in Salesforce isn't possible create records 
			 for the object "AsyncApexJob". Error: Fields are not writable.
NOTE: The due validations of "System.AssertEquals()" cannot add to this class, because all data depends of the data of Environment.
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest(SeeAllData=true)// Se notifico de esto al Arquitecto
public class CDM_ExecutionLog_tst {
	//Method where already exist current records of AsyncApexJob, so only updates them values
	@isTest static void methodUpdateOldBatches(){
        test.startTest();
        CDM_ExecutionLog_ctr.getJobs();
        CDM_ExecutionLog_ctr.getLogs();
        CDM_ExecutionLog_ctr.oldLogs();
        CDM_ExecutionLog_ctr.oldLoglsts();
        CDM_ExecutionLog_ctr.lstJobs= CDM_ExecutionLog_ctr.getJobs();
        CDM_ExecutionLog_ctr.save();
        test.stopTest();
    }
    //Method where no Old batch records were found, so the class create them
    @isTest static void methodNoOldBatches(){
        test.startTest();
        CDM_ExecutionLog_ctr.lstJobs = [SELECT Id, CreatedBy.Name, Status,CreatedDate, CompletedDate, NumberOfErrors, ExtendedStatus, JobItemsProcessed,TotalJobItems,ApexClass.Name FROM AsyncApexJob ORDER BY CreatedDate DESC LIMIT 10];
        CDM_ExecutionLog_ctr.lstLogs = new List<CDM_Logs__c>();
        CDM_ExecutionLog_ctr.save();
        test.stopTest();
    }
    //Method where already exist records of AsyncApexJob, but none exists on Object CDM_Logs
    @isTest static void methodCreateBatches(){
        test.startTest();
        CDM_ExecutionLog_ctr.lstJobs = [SELECT Id, CreatedBy.Name, Status,CreatedDate, CompletedDate, NumberOfErrors, ExtendedStatus, JobItemsProcessed,TotalJobItems,ApexClass.Name FROM AsyncApexJob ORDER BY CreatedDate DESC LIMIT 10];
        CDM_ExecutionLog_ctr.lstLogs = [SELECT Id,BatchId__c, CDM_State__c, Credopor__c, CDM_StartDate__c,CDM_EndDate__c,CDM_Failure__c,CDM_Total__c,CDM_BatchProc__c,CDM_Details__c FROM CDM_Logs__c LIMIT 10];
        CDM_ExecutionLog_ctr.save();
        test.stopTest();
    }
}