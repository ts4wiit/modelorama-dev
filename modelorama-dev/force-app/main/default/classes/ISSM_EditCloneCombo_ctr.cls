/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona, Carlos Pintor
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows to recover the information of the combo to be preloaded in 
    					the edition or cloning of combo

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    17-April-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class ISSM_EditCloneCombo_ctr {
	
	/**
    * @description  	Receives the registration id as a parameter, to obtain the list of necessary fields 
    *                	during the update of the combo IN ISSM_ComboLimit__c
    * 
    * @param    IdRec     Id of record to return
    * 
    * @return   Return a list of type 'ISSM_ComboLimit__c' for Combo Limit record
    */
	@AuraEnabled
	public static ISSM_ComboLimit__c[] getLimitRecord(String IdRec){

		return [SELECT 	Id, 
						Name, 
						ISSM_ComboLevel__c, 
						ISSM_SalesStructure__c, 
						ISSM_SalesStructure__r.Name,
						ISSM_SalesStructure__r.RecordType.DeveloperName
				FROM 	ISSM_ComboLimit__c 
				WHERE 	Id =: IdRec
				AND 	ISSM_IsActive__c = true
				AND 	ISSM_AvailableCombos__c > 0];
	}

	/**
    * @description  	Receives the registration id as a parameter, to obtain the list of necessary fields 
    *                	during the update of the combo IN Products and Quotas
    * 
    * @param    IdRec     Id of record to return
    * 
    * @return   Return a list of type 'ISSM_MultiSelectLookUp_cls.WrapperClass' for Products and Quotas records
    */
	@AuraEnabled
	public static ISSM_MultiSelectLookUp_cls.WrapperClass[] getComboByProduct(String IdRec){
		ISSM_ComboByProduct__c[] lstPBC  = new List<ISSM_ComboByProduct__c>();
		ISSM_MultiSelectLookUp_cls.WrapperClass[] wrpLst = new List<ISSM_MultiSelectLookUp_cls.WrapperClass>();

		lstPBC =[SELECT Id,
						Name,
						ISSM_ActiveValue__c,
						ISSM_QuantityProduct__c,
						ISSM_UnitPriceTax__c,
                        ISSM_UnitPrice__c,
						ISSM_Product__c,
						ISSM_Quota__c,
						ISSM_Type__c,
						ISSM_ComboNumber__c,
						ISSM_Quota__r.Description__c,
						ISSM_Quota__r.Code__c,
						ISSM_Product__r.ONTAP__MaterialProduct__c,
						ISSM_Product__r.ONTAP__ProductId__c
				FROM 	ISSM_ComboByProduct__c 
				WHERE 	ISSM_ComboNumber__c =: IdRec
				AND 	ISSM_ActiveValue__c = true];


		for(ISSM_ComboByProduct__c obj : lstPBC){
			String strName = (obj.ISSM_Type__c == Label.TRM_Product) ? obj.ISSM_Product__r.ONTAP__MaterialProduct__c : obj.ISSM_Quota__r.Description__c;
			String strSKU  = (obj.ISSM_Type__c == Label.TRM_Product) ? obj.ISSM_Product__r.ONTAP__ProductId__c : obj.ISSM_Quota__r.Code__c;
            wrpLst.add(new ISSM_MultiSelectLookUp_cls.WrapperClass(obj,true,Integer.valueOf(obj.ISSM_QuantityProduct__c),obj.ISSM_UnitPriceTax__c,obj.ISSM_UnitPrice__c,obj.ISSM_Type__c,strName,strSKU));
        }
        System.debug('wrpLst--> '+wrpLst);
		return wrpLst;
	}

	/**
    * @description  	Receives the registration id as a parameter, to obtain the list of necessary fields 
    *                	during the update of the combo IN ISSM_ComboByAccount__c
    * 
    * @param    IdRec     Id of record to Combo
    * 
    * @return   Return a list of type 'ISSM_ComboByAccount__c' for ISSM_ComboByAccount__c records
    */
	@AuraEnabled
	public static ISSM_ComboByAccount__c[] getComboByAccount(String IdRec){
		ISSM_ComboByAccount__c[] lstCBA  = new List<ISSM_ComboByAccount__c>();
		lstCBA =[SELECT Id,
						Name,
						ISSM_ActiveValue__c,
						ISSM_ComboNumber__c,
						ISSM_Segment__c,
						ISSM_SynchronizedWithSAP__c,
						ISSM_SynchronizedCodeSAP__c,
						ISSM_RegionalSalesDirection__c,
						ISSM_SalesOrganization__c,
						ISSM_SalesOffice__c,
						ISSM_Customer__c,
						ISSM_ExclusionAccount__c
				FROM 	ISSM_ComboByAccount__c 
				WHERE 	ISSM_ComboNumber__c =: IdRec];

        System.debug('lstCBA--> '+lstCBA);
		return lstCBA;
	}

	/**
    * @description  Receives the registration id as a parameter, to obtain the list of necessary fields 
    *               during the update of the combo IN MDM_Parameter__c
    * 
    * @param    PriceGroupCode     List of Codes for search Price Grouping
    * 
    * @return   Return a list of type 'MDM_Parameter__c' for Price Grouping records
    */
	@AuraEnabled
	public static MDM_Parameter__c[] getPriceGroup(String[] PriceGroupCode){

		return [SELECT 	Name, 
						Active__c, 
						Active_Revenue__c, 
						Catalog__c, 
						Code__c, 
						Description__c 
				FROM 	MDM_Parameter__c 
				WHERE 	Active__c = true 
				AND		Active_Revenue__c = true
				AND 	Catalog__c =: Label.TRM_Segmento
				AND 	Code__c IN: PriceGroupCode];
	}

    /**
    * @description  Allows carrying out an Upsert operation on the object ISSM_ComboByProduct__c
    *               
    * 
    * @param    comboByProductList     List of ISSM_ComboByProduct__c for upsert
    * 
    * @return   none
    */
	@AuraEnabled
    public static void upsertComboByProductList(ISSM_ComboByProduct__c[] comboByProductList,String StrComboId){
    	List<SObject> castRecords = (List<SObject>)Type.forName('List<ISSM_ComboByProduct__c>').newInstance();
    	castRecords.addAll(comboByProductList);
        try{
            upsert(castRecords);
            System.debug('#### DML operation done');
            submitForApproval(StrComboId, Label.TRM_Comment, Label.TRM_ApprovalProcessEditCombo, UserInfo.getUserId());
            if(Test.isRunningTest()) throw new MyException('Exception test');
        }catch(Exception Exc){System.debug('##ERRROR## upsertComboByProductList: '+Exc.getStackTraceString());}
    }


    /**
    * @description  Allows carrying out an Insert operation on the ISSM_ComboByAccount__c object
    *               
    * 
    * @param    comboByAccountList     List of ISSM_ComboByAccount__c for insert
    * 
    * @return   none
    */
    @AuraEnabled
    public static void saveComboByAccountList(ISSM_ComboByAccount__c[] comboByAccountList,String StrCombo){
        try{
            Database.insert(comboByAccountList);
            submitForApproval(StrCombo, Label.TRM_Comment, 'ISSM_ApprovalCombo', UserInfo.getUserId());
        }catch(Exception Exc){System.debug('##ERRROR## saveComboByAccountList: '+Exc.getStackTraceString());}
    }


    /**
    * @description  Allows carrying out an Insert operation on the ISSM_Combos__c object by returning the ID of the inserted record
    *               
    * 
    * @param    objCombo     Object of type ISSM_Combos__c
    * 
    * @return   Id           Returns the Id of the Inserted record
    */
    @AuraEnabled
    public static Id saveCombo(ISSM_Combos__c objCombo){
        String StrId = '';
        User[] Lst = [SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId()];
        try{
        	Database.SaveResult srList = Database.insert(objCombo);
        	StrId = srList.getId();
            if(Test.isRunningTest()) throw new MyException('Exception test');
        }catch(Exception Exc){System.debug('##ERRROR## saveCombo: '+Exc.getStackTraceString());}
        
        return StrId;
    }  

    /**
    * @description  It allows to generate the structure of the object ISSM_ComboByAccount__c to 
    *               relate the accounts according to the level of the combo
    *               
    * 
    * @param    strLimitId              Limit Id of the combo
    * @param    LstPriceGroup           List of Price Grouper
    * 
    * @return   ISSM_ComboByAccount__c  List of ISSM_ComboByAccount__c that was generated in the method.
    */
    @AuraEnabled
    public static ISSM_ComboByAccount__c[] getStructureforComboByAccount(String strLimitId,MDM_Parameter__c[] LstPriceGroup){
    	ISSM_ComboByAccount__c[] LstComboByAccount = new List<ISSM_ComboByAccount__c>();
    	ISSM_ComboLimit__c[] LstLimit  = getLimitRecord(strLimitId);
    	String StrLevel 			   = LstLimit[0].ISSM_ComboLevel__c;

 		String StrId 	  = (StrLevel == 'ISSM_National') ? null : LstLimit[0].ISSM_SalesStructure__c;
 		String StrRecType = (StrLevel == 'ISSM_National') ? 'ISSM_RegionalSalesDivision' : LstLimit[0].ISSM_SalesStructure__r.RecordType.DeveloperName; 
		StrRecType 		  = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('Account',StrRecType);
		LstComboByAccount = getAccounts(StrId,StrRecType,StrLevel);
		LstComboByAccount = (LstPriceGroup.isEmpty()) ? LstComboByAccount : setPriceGroup(LstComboByAccount,LstPriceGroup);

    	return LstComboByAccount;
    }

    /**
    * @description                      It allows to recover records of type Account and assigns the 
    *                                   Id of the account according to the Level.
    *               
    * @param    StrId                   Id del Combo
    * @param    StrRecordType           Id Tipo de Registro
    * @param    StrLevel                Nivel de combo
    * 
    * @return   ISSM_ComboByAccount__c  List of ISSM_ComboByAccount _c generated in the method.
    */
    public static ISSM_ComboByAccount__c[] getAccounts(String StrId, String StrRecordType,String StrLevel){
  		ISSM_ComboByAccount__c[] LstComboByAccount = new List<ISSM_ComboByAccount__c>();
  		Account[] LstAccounts 				  = new List<Account>();
  		ISSM_ComboByAccount__c ComboByAccount = new ISSM_ComboByAccount__c();
  						
  		String  sQuery =  'SELECT Id, ISSM_ParentAccount__c, ISSM_ParentAccount__r.ISSM_ParentAccount__c ';
                sQuery += 'FROM Account WHERE ';
                sQuery += (String.isNotBlank(StrId)) ? 'Id = \'' + StrId + '\' AND ' : ' ';
                sQuery += 'RecordTypeId = \'' + StrRecordType + '\'';

        sObject[] records = ISSM_UtilityFactory_cls.executeQuery(sQuery);
        
        for(sObject objAc : records){
            LstAccounts.add((Account) objAc);
        }
  		
        for(Account objAcc : LstAccounts){
        	System.debug('StrLevel: '+StrLevel);
  			ComboByAccount = new ISSM_ComboByAccount__c();
  			ComboByAccount.ISSM_RegionalSalesDirection__c = (StrLevel == 'ISSM_National' || StrLevel == 'ISSM_RegionalSalesOffice') ? objAcc.Id : 
  								   						    (StrLevel == 'ISSM_SalesOrganization') ? objAcc.ISSM_ParentAccount__c : 
  								    						objAcc.ISSM_ParentAccount__r.ISSM_ParentAccount__c;
  			ComboByAccount.ISSM_SalesOrganization__c 	  = (StrLevel == 'ISSM_National' || StrLevel == 'ISSM_RegionalSalesOffice') ? null : 
  								   							(StrLevel == 'ISSM_SalesOrganization') ? objAcc.Id : objAcc.ISSM_ParentAccount__c;
  			ComboByAccount.ISSM_SalesOffice__c 			  = (StrLevel == 'ISSM_National' || StrLevel == 'ISSM_RegionalSalesOffice' || StrLevel == 'ISSM_SalesOrganization') ? null : objAcc.Id;
  			LstComboByAccount.add(ComboByAccount);
  		}
  		System.debug('LstComboByAccount--> '+LstComboByAccount);
  		
  		return LstComboByAccount;
    }

    /**
    * @description                      Allows you to assign the Price Grouper to the list of ISSM_ComboByAccount__c
    *               
    * 
    * @param    LstComboByAccount       List of ISSM_ComboByAccount__c
    * @param    LstPriceGroup           List of price groupers (MDM Parameter _ c)
    * 
    * @return   ISSM_ComboByAccount__c  List of ISSM_ComboByAccount__c with the allocation of segments.
    */
    public static ISSM_ComboByAccount__c[] setPriceGroup(ISSM_ComboByAccount__c[] LstComboByAccount, MDM_Parameter__c[] LstPriceGroup){
    	
    	for (Integer i=0; i < LstPriceGroup.size(); i++) {
    		for(ISSM_ComboByAccount__c objCbA : LstComboByAccount){
    			objCbA.ISSM_Segment__c = LstPriceGroup[i].Id;
    		}
    	}

    	return LstComboByAccount;
    }

    /**
    * @description              Allows carrying out the shipment to approval of the newly created combo
    *               
    * @param    strId           Combo Id
    * @param    comment         Comments
    * @param    nameProcess     Approval process name
    * @param    userId          ID of the User that sends to Approval.
    * 
    * @return   none
    */
    //@AuraEnabled
    public static void submitForApproval(Id strId, String comment, String nameProcess, Id userId){
        User[] Lst = [SELECT ManagerId FROM User WHERE Id =: userId];        

        if(String.isNotBlank(Lst[0].ManagerId)){
            System.debug('EXECUTE APPROBAL');
            ISSM_ApprovalProcess_cls.submitForApproval(strId,comment,nameProcess,userId);
        }else{
            ISSM_Combos__c[] LstCombo = [SELECT ISSM_StatusCombo__c FROM ISSM_Combos__c WHERE Id =: strId];
            LstCombo[0].ISSM_StatusCombo__c = 'ISSM_Approved';
            upsert LstCombo;
        }
    }

    public class MyException extends Exception{}
}