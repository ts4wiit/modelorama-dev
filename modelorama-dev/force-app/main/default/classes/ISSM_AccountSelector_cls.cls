/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for Account
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public class ISSM_AccountSelector_cls extends fflib_SObjectSelector implements ISSM_IAccountSelector_cls
{

	public static ISSM_IAccountSelector_cls newInstance()
	{
		return (ISSM_IAccountSelector_cls) ISSM_Application_cls.Selector.newInstance(Account.SObjectType);
	}
	
	public ISSM_AccountSelector_cls()
	{
		super(true);
	}
	
	
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Account.Id,
				Account.ISSM_BillingManager__c,
				Account.ISSM_TradeMarketing__c, 
				Account.ISSM_SalesOffice__c,
				Account.OwnerId,
				Account.ISSM_RegionalSalesDivision__c,
				Account.Name, 
				Account.ParentId,
				Account.ONTAP__Email__c,
				Account.ISSM_LastPromisePaymentDate__c,
				Account.ISSM_LastContactDate__c,
				Account.ISSM_LastPaymentPlanDate__c,
				Account.ISSM_Additional_Email__c,
				Account.ONCALL__Preferred_Contact_Email__c,
				Account.ISSM_ProviderEntitlement__c,
				Account.ISSM_ParentAccount__c,
				Account.ISSM_BossRefrigeration__c
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Account.sObjectType;
	}

	public List<Account> selectById(Set<ID> idSet)
	{
		return (List<Account>) selectSObjectsById(idSet);
	}

	public List<Account> selectById(Set<ID> idSet, String relationshipFieldPath)
	{
		return (List<Account>) selectSObjectsById(idSet, relationshipFieldPath);
	}

	public Account selectById(ID accountID){
		return (Account) selectSObjectsById(accountID);
	}

	public Account selectById(ID accountID, String relationshipFieldPath){
		return (Account) selectSObjectsById(accountID, relationshipFieldPath);
	}

	public List<Account> selectById(String accountID){
		return (List<Account>) selectSObjectsById(accountID);
	}

	public List<Account> selectById(String accountID, String relationshipFieldPath){
		return (List<Account>) selectSObjectsById(accountID, relationshipFieldPath);
	}			

	public List<Account> selectByMultipleFields(String strRecordTypeAccountId,String strTypificationLevel3,String strTypificationLevel4,String accountsSalesOffice)
	{
		assertIsAccessible();
		return (List<Account>) Database.query(
			String.format(
			'select {0},{1} ' +
			  'from {2} ' +
			  'Where RecordTypeId =: strRecordTypeAccountId AND '+ 
              'ISSM_ProviderClassification__c =: strTypificationLevel3 AND '+
              'ISSM_Brand__c =: strTypificationLevel4 AND ' +
              'ISSM_SalesOffice__c =:accountsSalesOffice limit 1 ' ,
			new List<String> {
				getFieldListString(),
				getRelatedFieldListString('ISSM_SalesOffice__r'),
				getSObjectName() } ) );
		
	}

	public List<Account> selectByRecordType(Id recordTypeId)
	{
		assertIsAccessible();
		return (List<Account>) Database.query( String.format(
			'select {0},{1} ' +
			  'from {2} ' +
			  'Where RecordTypeId =: recordTypeId '+
			  'order by {3}',
			new List<String> {
				getFieldListString(),
				getRelatedFieldListString('ISSM_SalesOffice__r'),
				getSObjectName(),
				getOrderBy() } )
			 );

		
	}

	public List<Account> selectByRecordTypeByParentAccount(Id recordTypeId, Id parentAccountId)
	{
		assertIsAccessible();
		return (List<Account>) Database.query(
			String.format(
			'select {0},{1} ' +
			  'from {2} ' +
			  'Where RecordTypeId =: recordTypeId AND '+ 
              'ISSM_ParentAccount__c=:parentAccountId '+
			  'order by {3}',
			new List<String> {
				getFieldListString(),
				getRelatedFieldListString('ISSM_SalesOffice__r'),
				getSObjectName(),
				getOrderBy() } ) );
		
	}
	
}