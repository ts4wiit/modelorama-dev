/****************************************************************************************************
    General Information
    -------------------
    Author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Class to calculate the new Credit Amount in the Account when the Open Items related
    						to the Client has changes.
		Events for the Trigger: after insert, after update, after undelete, before delete.

    Related classes: ISSM_OnCallQueries_cls
    Related triggers: xOpenItem_tgr 

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       27-March-2018          Marco Zúñiga                Class Creation
    ================================================================================================
****************************************************************************************************/

public with sharing class xOpenItemCreditAmount_thr {
	public static ISSM_OnCallQueries_cls CTRL = new ISSM_OnCallQueries_cls();

	/**
	* calculateOI: It calculates the new Credit Amount per Account for a list of ISSM_OpenItemB__c records
	*								and do an update for those Accounts
	* @param ISSM_OpenItemB__c[] OI_lst: List of Open Items to process.
	* @param Boolean isDelete: Indicates if the List of Open Items was deleted from the trigger.
	* @return void: not return a value.
	**/
	public static void calculateOI(ISSM_OpenItemB__c[] OI_lst){
		Id recTypeOpenItemB_Id = Schema.SObjectType.ISSM_OpenItemB__c.getRecordTypeInfosByDeveloperName().get(Label.ISSM_ONTAPCompact).getRecordTypeId();
		Set<String> AccId_set = new Set<String>();

		for(ISSM_OpenItemB__c ObjOI : OI_lst){
			if(ObjOI.ISSM_Account__c != null && ObjOI.RecordTypeId == recTypeOpenItemB_Id){
				AccId_set.add(ObjOI.ISSM_Account__c);
			}
		}

		List<AggregateResult> results_lst = !AccId_set.isEmpty() ? CTRL.getSumOpenItemB(AccId_set,recTypeOpenItemB_Id) : new List<AggregateResult>();

		List<Account> accountsToUpdate_lst = new List<Account>();
		for(Integer i = 0; i < results_lst.size(); i++){
			Double debtAmount_Dbl = Double.valueOf(results_lst[i].get('payment')) - Double.valueOf(results_lst[i].get('debit'));
			Account Acc_obj = new Account();
			Acc_obj.Id = (Id) results_lst[i].get('ISSM_Account__c');
			Acc_obj.ONTAP__Credit_Amount__c = debtAmount_Dbl * (-1.00);
			accountsToUpdate_lst.add(Acc_obj);
		}

		if(accountsToUpdate_lst.size() > 0){
            ISSM_TriggerManager_cls.inactivate('ISSM_TriggerManager_cls');
            ISSM_TriggerManager_cls.inactivate('ISSM_AssignOwnerIdAccountTeam_tgr');
            ISSM_TriggerManager_cls.inactivate('Account_tgr');
			update accountsToUpdate_lst;
		} 
	}
}