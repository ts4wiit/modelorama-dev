/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description       :   Schedule responsible for scheduling next day's jobs. For settlement periods.
*                       And It runs every day at 23:47 hrs (parameterized by label)
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
* 2.0    16-Noviembre-2018       Andrea Cedillo                  MODIFIED
*******************************************************************************/
global class TRM_StartSchedulers_sch implements Schedulable {
    @TestVisible
    public static final String CRON_EXPR = Label.TRM_CronExprSettlementPeriods;
    /*
    * Method Name   : executeSchedule
    * creation      : Program the scheduler according to the "cron expression" and the assigned "name"
    */
     global static String executeSchedule() {

        TRM_ApprovalConditions_ctr.unSchedule(Label.TRM_NameSchSettlementPeriods);
        TRM_StartSchedulers_sch job = new TRM_StartSchedulers_sch();
        return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchSettlementPeriods + System.now()) : Label.TRM_NameSchSettlementPeriods), CRON_EXPR, job);
    }
    /**
    * @description:  Execute the runSchedulerProgramming () method to reprogram the schedulers
    *
    *   @return     No return
    */
    global void execute(SchedulableContext sc) {
        runSchedulerProgramming();
    }
    /**
    * @description  Send to schedule all the existing settlement periods that will have to be programmed from the current day '+1'
    *
    *   @return     No return
    */
    public static void runSchedulerProgramming() {
        String ZMIW = Label.TRM_ZMIW;
        String ZPV3 = Label.TRM_ZPV3;
        String ZPV2 = Label.TRM_ZPV2;
        String ZTPM = Label.TRM_ZTPM;
        String ZSP1 = Label.TRM_ZSP1;
        String ZPR1 = Label.TRM_ZPR1;
        String ZV00 = Label.TRM_ZV00;
        Date strToday = System.now().Date().addDays(1);
        
        List<TRM_SettlementPeriods__c> lstSettlementPeriods =[SELECT TRM_ConditionClas__c
                                                                    ,TRM_EffectiveDate__c
                                                                    ,TRM_EndTime__c
                                                                    ,TRM_RecordVolume__c
                                                                    ,TRM_StartTime__c 
                                                              FROM TRM_SettlementPeriods__c 
                                                              WHERE TRM_EffectiveDate__c =: strToday];          

        for(TRM_SettlementPeriods__c settlementPeriod :lstSettlementPeriods){
            if(ZMIW.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZMIWInSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZPV3.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZPV3InSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZPV2.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZPV2InSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZTPM.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZTPMInSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZSP1.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZSP1InSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZPR1.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZPR1InSettlementPeriod_sch.programSchedule(settlementPeriod,true);
            if(ZV00.equals(settlementPeriod.TRM_ConditionClas__c)) TRM_ZV00InSettlementPeriod_sch.programSchedule(settlementPeriod,true);
        }
    }
}