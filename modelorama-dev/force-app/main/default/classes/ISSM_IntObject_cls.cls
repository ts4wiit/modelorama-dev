public with sharing class ISSM_IntObject_cls implements Iterable<SObject> {
	String nameConfig;
	String field;
	String valueField;

	public ISSM_IntObject_cls(String nameConfigParam, String fieldParam, String valueFieldParam){
		nameConfig = nameConfigParam;
		field = fieldParam;
		valueField = valueFieldParam;
	}

	public Iterator<SObject> Iterator(){
		return new ISSM_IntObjectCustomIterable_cls(nameConfig, field, valueField);
  	}
  
}