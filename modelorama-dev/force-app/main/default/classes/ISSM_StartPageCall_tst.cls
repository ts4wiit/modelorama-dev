/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que gestiona la pagina a mostrar al ingresar a la llamada

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description              Cobertura
    ------    --------        --------------------------   -----------------------------------------
    1.0       05-Junio-2017   Luis Licona                  Creación de la Clase     94%
	2.0		  05-Septiemre-2018 Ernesto Gutiérrez          Alcance de cobertura     
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_StartPageCall_tst {

    static Account ObjAccount{get;set;}//Cuentas
     static Account ObjAccountHD{get;set;}//Cuentas
    static Account ObjAccount1{get;set;}//Cuentas
    static Account ObjAccount2{get;set;}//Cuentas
    static ONCALL__Call__c ObjCall{get;set;}//Llamadas
     static ONCALL__Call__c ObjCallHD{get;set;}//Llamadas
    static ONCALL__Call__c ObjCall2{get;set;}//Llamadas
    static ApexPages.StandardController sc{get;set;}
    static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Order__c ObjOrder1{get;set;}

    public static void StartValues1(){  
        Id RecTypeCal = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        
        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true
        );
        insert ObjAccount;


        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            ISSM_ValidateOrder__c=false,
            RecordTypeId = RecTypeCal,
            ONCALL__Date__c = System.now(),
            ISSM_CallResult__c = 'Effective call'

        );
        insert ObjCall;
        
        List<ONTAP__Order__c> lstOrder = new List<ONTAP__Order__c>();
		ONTAP__Order__c ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = ObjAccount.Id, 
            ONCALL__Origin__c         = 'AUT', //Autoventa,        
            ONCALL__SAP_Order_Number__c   = '111111',
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus
        );
        lstOrder.add(ObjOrder);
        ONTAP__Order__c ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = ObjAccount.Id, 
          	ONCALL__Origin__c         = 'PRE', //preventa
            ONCALL__SAP_Order_Number__c   = '1111112',         
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus
        );
        lstOrder.add(ObjOrder2);
        
        ONTAP__Order__c ObjOrder3 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = ObjAccount.Id, 
            ONCALL__Origin__c         = 'B2B-0000', //mi modelo ,
            ONCALL__SAP_Order_Number__c   = '1111113'  ,         
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus
        );
        lstOrder.add(ObjOrder3);
        
        ONTAP__Order__c ObjOrder4 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c  = ObjAccount.Id, 
           	ONCALL__Origin__c         = 'REP', //'B2B-0000',  //REPARTO  
            ONCALL__SAP_Order_Number__c   = '1111115',      
            ONTAP__BeginDate__c       =  Datetime.now(),
            ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus
        );
        lstOrder.add(ObjOrder4); 
        insert lstOrder;

        sc = new ApexPages.standardController(ObjCall);
    }


     public static void StartValues2(){
        Id RecTypeCal = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.Account.RecordTypeInfosByName;
        String StrRecType;
        try{
            StrRecType = MapSchema.get('Oficina de ventas').getRecordTypeId();
        }catch(exception exc){
            StrRecType = MapSchema.get('Sales Office').getRecordTypeId();
        }

        ObjAccount1 = new Account(
            Name = 'TST1',
            ONTAP__Main_Phone__c      = '5510988764',
            ISSM_MainContactE__c      = true,
            ONTAP__SalesOffId__c      = 'FG00',
            RecordTypeId              = StrRecType
        );
        insert ObjAccount1;

        ObjAccount2 = new Account(
            Name = 'TST2',
            ONTAP__Main_Phone__c         = '5510988765',
            ISSM_MainContactE__c         = true,
            ONTAP__SAP_Number__c         = '0100169817',
            ONTAP__SalesOgId__c          = '3116',
            ONTAP__ChannelId__c          = '01',   
            ONCALL__Ship_To_Name__c      = '0100169817',
            ONTAP__SalesOffId__c         = 'FG00',
            ISSM_SalesOffice__c          = ObjAccount1.Id,
            ONCALL__OnCall_Route_Code__c = 'FG0050',
            ONTAP__CustomerGroup__c      = '',
            ISSM_KVGR5__c                = ''
        );
        insert ObjAccount2;

        ObjCall2= new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount2.Id,
            ISSM_ValidateOrder__c=true,
            RecordTypeId = RecTypeCal,
            ONCALL__Date__c = System.now()+1,
            ISSM_CallResult__c = 'Effective call'
        );
        insert ObjCall2;

        sc = new ApexPages.standardController(ObjCall2);

    }

    public static void StartValues3(){  
        Id RecTypeCal   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        Id RecTypeOrder = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
        
        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true
        );
        insert ObjAccount;


        ObjCall = new ONCALL__Call__c(
            Name                  = 'TST',
            ONCALL__POC__c        = ObjAccount.Id,
            ISSM_ValidateOrder__c = false,
            RecordTypeId          = RecTypeCal,
            ONCALL__Date__c       = System.now(),
            ISSM_CallUT__c        = true,
            ISSM_CallResult__c = 'Effective call'

        );
        insert ObjCall;

        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+2,
            ONTAP__BeginDate__c= System.today(),
            RecordTypeId = RecTypeOrder
        );
        insert ObjOrder;

        sc = new ApexPages.standardController(ObjCall);
    }

    public static void StartValues4(){  
        Id RecTypeCal   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        Id RecTypeOrder = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
        
        ObjAccount = new Account(
            Name = 'TSTacc1',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true
        );
        insert ObjAccount;


        ObjCall = new ONCALL__Call__c(
            Name                  = 'TST2',
            ONCALL__POC__c        = ObjAccount.Id,
            ISSM_ValidateOrder__c = true,
            RecordTypeId          = RecTypeCal,
            ONCALL__Date__c       = System.now() + 1,
            ISSM_CallResult__c = 'Effective call'
        );
        insert ObjCall;

        ObjOrder = new ONTAP__Order__c(
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+2,
            ONTAP__BeginDate__c= System.today(),
            RecordTypeId = RecTypeOrder
        );
        insert ObjOrder;

        sc = new ApexPages.standardController(ObjCall);
    }

    @isTest static void UserDateIncomplete() {

        Profile p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

        User u = new User(Alias = 'TV', 
                        Email='tv@gmodelo.com',
                        EmailEncodingKey='UTF-8', 
                        LastName='Testing', 
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', 
                        ProfileId = p.Id,
                        TimeZoneSidKey='America/Los_Angeles', 
                        UserName='tv@gmodelo.com.mx');

         System.runAs(u){
            StartValues1();
                Test.startTest();
                ISSM_StartPageCall_ctr CTR = new ISSM_StartPageCall_ctr(sc);

                PageReference pr = CTR.startPage();
                //System.assertEquals(true,CTR.BlnHaveOrder);
            Test.stopTest();
         }

    }

    @isTest static void UserDatecomplete() {

        Profile p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

        User u = new User(Alias = 'TV', 
                        Email='tv@gmodelo.com',
                        EmailEncodingKey='UTF-8', 
                        LastName='Testing', 
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', 
                        ProfileId = p.Id,
                        TimeZoneSidKey='America/Los_Angeles', 
                        UserName='tv@gmodelo.com.mx',
                        ISSM_RouteID__c = 'FG00502',
                        ONTAP__SAPUserId__c = '5060764');

         System.runAs(u){
            StartValues2();
                Test.startTest();
                ISSM_StartPageCall_ctr CTR = new ISSM_StartPageCall_ctr(sc);

                PageReference pr = CTR.startPage();
            //    System.assertEquals(true,CTR.BlnHaveOrder);
            Test.stopTest();
         }
    }   

    @isTest static void AccountWithOrder() {

        Profile p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

        User u = new User(Alias = 'TV', 
                        Email='tv@gmodelo.com',
                        EmailEncodingKey='UTF-8', 
                        LastName='Testing', 
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', 
                        ProfileId = p.Id,
                        TimeZoneSidKey='America/Los_Angeles', 
                        UserName='tv@gmodelo.com.mx',
                        ISSM_RouteID__c = 'FG00502',
                        ONTAP__SAPUserId__c = '5060764');
        insert u;

        List<PermissionSet> ps_Lst = [SELECT Id FROM PermissionSet WHERE Name =: Label.ISSM_PermisionSett];

        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=u.Id,PermissionSetId=ps_Lst[0].id);
        upsert psa;

        Test.startTest();
            System.runAs(u){
                StartValues3();                
                ISSM_StartPageCall_ctr CTR = new ISSM_StartPageCall_ctr(sc);
                PageReference pr = CTR.startPage();
            Test.stopTest();
         }
    }

    @isTest static void CallWithOrder() {
		 Id RecTypeCal   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
		 Id RecTypeOrder = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
		   
        ObjAccount = new Account(
            Name = 'TST1',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true
        );
        insert ObjAccount;


        ObjCall = new ONCALL__Call__c(
            Name            = 'TST2',
            ONCALL__POC__c  = ObjAccount.Id,
            ISSM_ValidateOrder__c=false,
            RecordTypeId = RecTypeCal,
            ONCALL__Date__c = System.now(),
            ISSM_CallResult__c = 'Effective call'

        );
        insert ObjCall;
		 ObjOrder = new ONTAP__Order__c(
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+2,
            ONTAP__BeginDate__c= System.today(),
            RecordTypeId = RecTypeOrder
        );
        insert ObjOrder;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

        User u = new User(Alias = 'TV', 
                        Email='tv@gmodelo.com',
                        EmailEncodingKey='UTF-8', 
                        LastName='Testing', 
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US', 
                        ProfileId = p.Id,
                        TimeZoneSidKey='America/Los_Angeles', 
                        UserName='tv@gmodelo.com.mx',
                        ISSM_RouteID__c = 'FG00502',
                        ONTAP__SAPUserId__c = '5060764');
        insert u;

        Test.startTest();
            System.runAs(u){
                StartValues4();     
                ISSM_StartPageCall_ctr CTR = new ISSM_StartPageCall_ctr(sc);
                PageReference pr = CTR.startPage();
            Test.stopTest();
         }
    }
    
    public static void StartValues5(){  
        Id RecTypeCal   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        Id RecTypeOrder = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
        
        ObjAccountHD = new Account(
            Name = 'TST3',
            ONTAP__Main_Phone__c = '5510988765',
            ISSM_MainContactE__c = true,
            ONTAP__SalesOffId__c = 'FG00',
            ONTAP__SAPCustomerId__c = '123456789'
           
        );
        insert ObjAccountHD;


        ObjCallHD = new ONCALL__Call__c(
            Name                  = 'TST4',
            ONCALL__POC__c        = ObjAccountHD.Id,
            ISSM_ValidateOrder__c = false,
            RecordTypeId          = RecTypeCal,
            ONCALL__Date__c       = System.now(),
            ISSM_CallResult__c = 'Effective call'
        );
        insert ObjCallHD;

        ObjOrder = new ONTAP__Order__c(
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+2,
            ONTAP__BeginDate__c= System.today(),
            RecordTypeId = RecTypeOrder
        );
        insert ObjOrder;

        sc = new ApexPages.standardController(ObjCall);
    }

   // @isTest static void CallWithExistOrder() {
		 //Id RecTypeCal   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
		 //Id RecTypeOrder = CTRSOQL.getRecordTypeId('ONTAP__Order__c','OnCall_Order');
		   
   //     ObjAccount = new Account(
   //         Name = 'TST5',
   //         ONTAP__Main_Phone__c = '5510988765',
   //         ISSM_MainContactE__c = true
   //     );
   //     insert ObjAccount;


   //     ObjCall = new ONCALL__Call__c(
   //         Name            = 'TST6',
   //         ONCALL__POC__c  = ObjAccount.Id,
   //         ISSM_ValidateOrder__c=false,
   //         RecordTypeId = RecTypeCal,
   //         ONCALL__Date__c = System.now(),
   //         ISSM_CallResult__c = 'Effective call'

   //     );
   //     insert ObjCall;
		 //ObjOrder = new ONTAP__Order__c(
   //         ONCALL__Call__c = ObjCall.Id,
   //         ONTAP__OrderStatus__c = Label.ISSM_OrderStatus,
   //         ONCALL__Origin__c= Label.ISSM_OriginCall,
   //         ONTAP__DeliveryDate__c= System.today()+2,
   //         ONTAP__BeginDate__c= System.today(),
   //         RecordTypeId = RecTypeOrder
   //     );
   //     insert ObjOrder;
        
   //     Profile p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

   //     User u = new User(Alias = 'TV', 
   //                     Email='tv@gmodelo.com',
   //                     EmailEncodingKey='UTF-8', 
   //                     LastName='Testing', 
   //                     LanguageLocaleKey='en_US',
   //                     LocaleSidKey='en_US', 
   //                     ProfileId = p.Id,
   //                     TimeZoneSidKey='America/Los_Angeles', 
   //                     UserName='tv@gmodelo.com.mx',
   //                     ISSM_RouteID__c = 'FG00502',
   //                     ONTAP__SAPUserId__c = '5060764');
   //     insert u;

   //     Test.startTest();
   //         System.runAs(u){
   //             StartValues5();
   //             ISSM_StartPageCall_ctr CTR = new ISSM_StartPageCall_ctr(sc);
   //             PageReference pr = CTR.startPage();
   //         Test.stopTest();
   //      }
   // }
}