/******************************************++++**********************************************************
    General Information
    -------------------
    author:     Hector Diaz, Marco Zúñiga
    email:      hdiaz@avanxo.com, mzuniga@avanzco.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:

    Information about changes (versions)
    =====================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   ----------------------------------------------
    1.0       10-Agosto-2017   Hector Diaz                  Class Creation
    1.1       16-Agosto-2018   Marco Zúñiga                 Redirect the deployment to EmptyBalanceB__c
    =====================================================================================================
*********************************************************************************************************/
 
global with sharing class ISSM_BoxRackBalance_cls{

    public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls(); 
    
    //Obtiene el listado de envases que llegan por bonificación y los corrientes
    public static WrpBoxRackBalance getBoxRackBalance(ONTAP__Order_Item__c[] LstEOI,ISSM_Bonus__c[] LstBns,String IdAcc, String IdOrder,ONTAP__Product__c[] LstProductEmB){
       
        EmptyBalanceB__c ObjBRB              = new EmptyBalanceB__c();
        //Se crean listas distintas para mostrar detalle y agrupacion de rejillas
        EmptyBalanceB__c[] LstBRB            = new List<EmptyBalanceB__c>(); //Lst Current order
        EmptyBalanceB__c[] LstBRBonuses      = new List<EmptyBalanceB__c>(); //Lst BonusEB Account
        EmptyBalanceB__c[] LstBRBPending     = new List<EmptyBalanceB__c>(); //Lst Historic order
        EmptyBalanceB__c[] LstAllBRB         = new List<EmptyBalanceB__c>(); // Lst Current+Historic+BousEB
        EmptyBalanceB__c[] LstEBProductEmpty = new List<EmptyBalanceB__c>(); //Lst Current order
        WrapperBRBG ObjWrpBRBG;
        Datetime StrDate = System.now();
        
        List<ONTAP__Product__c> lstProducts = new List<ONTAP__Product__c>(); 
        Set<String> setSkuProduct = new Set<String>();
        Set<String> setSkuProduct2 = new Set<String>();
        List<ONTAP__Order_Item__c> lstOrderItem = new List<ONTAP__Order_Item__c>();
     	Map<String,List<ONTAP__Order_Item__c>> mapOrderItemXSku = new Map<String,List<ONTAP__Order_Item__c>>();
     	List<ISSM_ProductByPlant__c> lstProductByplant = new List<ISSM_ProductByPlant__c>(); 
  		ONTAP__Order_Item__c objEOI2 =  new ONTAP__Order_Item__c();

        Account ObjAcc = CTRSOQL.getAccountbyId(IdAcc);//getDaysCredit(IdAcc);
        Integer IntCD  = ObjAcc.ONTAP__EmptyLoanDays__c != null ? Integer.valueOf(ObjAcc.ONTAP__EmptyLoanDays__c) : 0;
        //se suman los dias de credito a la fecha de vencimiento        
        DateTime DtDD = ObjAcc.ISSM_EmptyCreditQty__c ? Datetime.parse(StrDate.format())+IntCD+1 : Datetime.parse(StrDate.format())+1;
        DateTime StrDateBon = Datetime.parse(StrDate.format())+1;
		Boolean isActiveIntegrationProxPlant = CTRSOQL.isSuggestedProdsActive(Label.ISSM_InteProductByPlant); 
		
		if(isActiveIntegrationProxPlant){
			System.debug('********** ENTRO LA INTEGRACION PRODUCT BY PLANT ***********');
			for(ONTAP__Order_Item__c objEOI : LstEOI){            
				setSkuProduct.add(objEOI.ONCALL__OnCall_Product__c);
				setSkuProduct2.add(objEOI.ISSM_OrderItemSKU__c);
				System.debug('************** objEOI BOXRACK :  '+objEOI);
				System.debug('************** setSkuProduct2 BOXRACK :  '+setSkuProduct2);
        	}
       	 	List<ONTAP__Order_Item__c> lstOrderI = new List<ONTAP__Order_Item__c>();
        
        	if(setSkuProduct.size() > 0 ){   	
	        	id RecordTypeIdProductByPlan_id =  Schema.SObjectType.ISSM_ProductByPlant__c.getRecordTypeInfosByDeveloperName().get('ISSM_ProductByPlant').getRecordTypeId();
	       		id RecordTypeId_id =  Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId();
				lstProducts =  CTRSOQL.getEmptiesList(RecordTypeId_id,RecordTypeIdProductByPlan_id,ObjAcc,setSkuProduct,'CAJA%');
				lstProductByplant = CTRSOQL.getProductByPlantEmp(lstProducts,setSkuProduct,'CAJA%',ObjAcc.ISSM_DistributionCenterLU__c);
				 
				System.debug('********** lstProducts : '+lstProducts); //producstos base de envace que estan en productbypant
				System.debug('********** lstProductByplant : '+lstProductByplant); //producstos base de envace que estan en productbypant
        	}
 
	        for(ONTAP__Order_Item__c objEOI : LstEOI  ){
	        	System.debug('********* objEOI boxrck :  '+objEOI);		
				for( ISSM_ProductByPlant__c objProductsEmptys : lstProductByplant ){   
	        		objEOI2 =  new ONTAP__Order_Item__c();
	        		
	        		objEOI2.ISSM_EmptyMaterialProduct__c	= objProductsEmptys.ISSM_AssociatedEmpty__r.ONTAP__MaterialProduct__c;
					objEOI2.ISSM_MaterialAvailable__c 		= objEOI.ISSM_MaterialAvailable__c;	
					objEOI2.ISSM_ProductDesc__c 			= objEOI.ISSM_ProductDesc__c;	
					objEOI2.ISSM_BalanceNum__c				= objProductsEmptys.ISSM_AssociatedEmpty__r.ISSM_ProductSKU__c;	
					objEOI2.ISSM_EmptyMaterial__c			= objProductsEmptys.ISSM_AssociatedEmpty__r.ISSM_ProductSKU__c;
					objEOI2.ISSM_IsSuggested__c				= objEOI.ISSM_IsSuggested__c;	
					objEOI2.ONCALL__OnCall_Quantity__c		= objEOI.ONCALL__OnCall_Quantity__c;
					objEOI2.ISSM_ComboNumber__c				= objEOI.ISSM_ComboNumber__c;
					objEOI2.ISSM_TaxAmount__c				= objEOI.ISSM_TaxAmount__c;
					objEOI2.ISSM_ItemDiscount__c			= objEOI.ISSM_ItemDiscount__c;
					objEOI2.ISSM_ItemPosition__c			= objEOI.ISSM_ItemPosition__c;
					objEOI2.ONTAP__CustomerOrder__c			= objEOI.ONTAP__CustomerOrder__c;
					objEOI2.ONCALL__SAP_Order_Item_Number__c = objEOI.ONCALL__SAP_Order_Item_Number__c;
					objEOI2.ISSM_NoDiscountTotalAmount__c	= objEOI.ISSM_NoDiscountTotalAmount__c;
					objEOI2.ONCALL__OnCall_Product__c		= objEOI.ONCALL__OnCall_Product__c;
					objEOI2.ISSM_UnitPrice__c				= objEOI.ISSM_UnitPrice__c;
					objEOI2.ISSM_Condition__c				= objEOI.ISSM_Condition__c;
					objEOI2.ISSM_MaterialProduct__c			= objProductsEmptys.ISSM_AssociatedEmpty__r.ONTAP__MaterialProduct__c;
					objEOI2.ONCALL__SFDC_Suggested_Order_Item_Number__c	= objEOI.ONCALL__SFDC_Suggested_Order_Item_Number__c;
					objEOI2.ISSM_Is_returnable__c			= objEOI.ISSM_Is_returnable__c;
					objEOI2.ISSM_OrderItemSKU__c			= objEOI.ISSM_OrderItemSKU__c;
					objEOI2.ISSM_UnitofMeasure__c			= objEOI.ISSM_UnitofMeasure__c;
					objEOI2.ISSM_TotalAmount__c				= objEOI.ISSM_TotalAmount__c;
					objEOI2.ISSM_Uint_Measure_Code__c 		= objEOI.ISSM_Uint_Measure_Code__c;
	        		
	        		System.debug('**bjProductsEmptys.ISSM_ProductSKU__c  : '+objProductsEmptys.ISSM_ProductSKU__c  +'  ========  objEOI : '+ objEOI.ISSM_OrderItemSKU__c); 
	        		if(String.valueOf(objProductsEmptys.ISSM_ProductSKU__c) == String.valueOf(objEOI.ISSM_OrderItemSKU__c )){
	        			System.debug('SI ENTRO  : ');
		        		lstOrderItem.add(objEOI2);  
	        		}
	        	}
	        }
       		System.debug('lstOrderItem BOXRACK***** : '+lstOrderItem);
			for(ONTAP__Order_Item__c objEOI : lstOrderItem){
				System.debug('******* objEOI2s BOXRACK : '+objEOI);
				if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyMaterial__c != null) || objEOI.ISSM_EmptyMaterial__c != null ){
	               ObjBRB = new EmptyBalanceB__c();
	                ObjBRB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
	                ObjBRB.RackMaterial__c      = objEOI.ISSM_EmptyMaterialProduct__c;  //objEOI.ISSM_EmptyMaterialProduct__c;
	                ObjBRB.Account__c           = IdAcc;
	                ObjBRB.OrderID__c           = IdOrder;
	                ObjBRB.MaterialProduct__c   = objEOI.ISSM_EmptyMaterialProduct__c;
	                ObjBRB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
	                ObjBRB.EmptyType__c         = objEOI.ISSM_EmptyType__c;
	                ObjBRB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
	                ObjBRB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
	                ObjBRB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
	                ObjBRB.Process__c           = false; 
	                ObjBRB.EmptyMaterial__c     = objEOI.ISSM_EmptyMaterial__c;
	                ObjBRB.EmptyRack__c         = objEOI.ISSM_EmptyMaterial__c;
	               LstBRB.add(ObjBRB);  
	            }  
			}
		}else{
			System.debug('********** NO SE CUENTA CON LA INTEGRACION PRODUCT BY PLANT ***********');
			for(ONTAP__Order_Item__c objEOI : LstEOI){       
	            if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyRack__c != null) || objEOI.ISSM_EmptyRack__c != null ){
	                ObjBRB = new EmptyBalanceB__c();
	                ObjBRB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
	                ObjBRB.Account__c           = IdAcc;
	                ObjBRB.OrderID__c           = IdOrder;
	                ObjBRB.RackMaterial__c      = objEOI.ISSM_RackMaterialProduct__c;
	                ObjBRB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
	                ObjBRB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
	                ObjBRB.EmptyType__c         = objEOI.ISSM_EmptyType__c; 
	                ObjBRB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
	                ObjBRB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
	                ObjBRB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
	                ObjBRB.Process__c           = false;
	                ObjBRB.EmptyRack__c         = objEOI.ISSM_EmptyRack__c;
	                LstBRB.add(ObjBRB); 
	            }
        	}	
		}
   
      	System.debug('***************LstBRB BOXRACK : '+LstBRB);

        for(ISSM_Bonus__c objEBonus : LstBns){
            if(objEBonus.ISSM_Product__r.ISSM_Is_returnable__c && objEBonus.ISSM_Product__r.ISSM_EmptyRack__c != null){
                ObjBRB = new EmptyBalanceB__c();
                ObjBRB.Product__c           = objEBonus.ISSM_Product__c;
                ObjBRB.Account__c           = IdAcc; 
                ObjBRB.OrderID__c           = IdOrder;
                ObjBRB.RackMaterial__c      = objEBonus.ISSM_RackMaterialProduct__c;
                ObjBRB.BoxesNumReturn__c    = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.BoxesNumRequest__c   = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.Balance__c           = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.EmptyBalanceKey__c   = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c; 
                ObjBRB.DueDate__c           = StrDateBon.date();
                ObjBRB.Material_Number__c   = objEBonus.ISSM_Material_Number__c;
                ObjBRB.Uint_Measure_Code__c = objEBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c;
                ObjBRB.Process__c           = false;
                ObjBRB.EmptyRack__c         = objEBonus.ISSM_Product__r.ISSM_EmptyRack__c;
                LstBRBonuses.add(ObjBRB);
               
            }
        }
        
         for(ONTAP__Product__c objProdEmB : LstProductEmB){
            System.debug('*********** OBJETO RACK : ');
            System.debug(objProdEmB.ISSM_QuantityInput__c);
            if(objProdEmB.ONCALL__Material_Number__c !=null){
                decimal valorBox = Decimal.valueOf(String.valueOf(objProdEmB.ISSM_QuantityInput__c));
        
                ObjBRB = new EmptyBalanceB__c();
                
                ObjBRB.Product__c           = objProdEmB.Id;
                ObjBRB.Account__c           = IdAcc;
                ObjBRB.OrderID__c           = IdOrder;
                ObjBRB.RackMaterial__c      = objProdEmB.ONTAP__MaterialProduct__c;
                ObjBRB.MaterialProduct__c   = objProdEmB.ONCALL__OnCall_Product__c;
                ObjBRB.BoxesNumReturn__c    = valorBox;
                ObjBRB.BoxesNumRequest__c   = valorBox;
                ObjBRB.Balance__c           = valorBox;
                ObjBRB.EmptyBalanceKey__c   = objProdEmB.ISSM_EmptyMaterial__c;
                ObjBRB.DueDate__c           = StrDateBon.date();
                ObjBRB.Material_Number__c   = objProdEmB.ONCALL__Material_Number__c;
                ObjBRB.Uint_Measure_Code__c = objProdEmB.ISSM_Uint_Measure_Code__c;
                ObjBRB.Process__c           = false; 
                ObjBRB.EmptyMaterial__c     = objProdEmB.ISSM_EmptyMaterial__c;      
            }
            LstEBProductEmpty.add(ObjBRB);
        }
         
        //Se agregan todos los envases para consolidarse en una sola lista
        LstAllBRB.addAll(LstEBProductEmpty);
        LstAllBRB.addAll(LstBRB);
        LstAllBRB.addAll(LstBRBonuses);
        LstBRBPending = getBoxRackPending(IdAcc); 
        LstAllBRB.addAll(LstBRBPending);//Add List Empty Balance Historic
        
        System.debug('LstAllBRB BOXRACK : '+LstAllBRB);
        ObjWrpBRBG = groupBoxRackBalance(LstAllBRB,DtDD,IntCD,IdOrder,IdAcc,ObjAcc.ISSM_EmptyCreditQty__c);//return Empty Balance Group
        
        return new WrpBoxRackBalance(LstBRB,LstBRBonuses,LstBRBPending,ObjWrpBRBG,LstEBProductEmpty);
    }
    
    /*public static WrpBoxRackBalance getBoxRackBalance(ONTAP__Order_Item__c[] LstEOI,ISSM_Bonus__c[] LstBns,String IdAcc, String IdOrder,ONTAP__Product__c[] LstProductEmB){
        EmptyBalanceB__c ObjBRB              = new EmptyBalanceB__c();
        //Se crean listas distintas para mostrar detalle y agrupacion de rejillas
        EmptyBalanceB__c[] LstBRB            = new List<EmptyBalanceB__c>(); //Lst Current order
        EmptyBalanceB__c[] LstBRBonuses      = new List<EmptyBalanceB__c>(); //Lst BonusEB Account
        EmptyBalanceB__c[] LstBRBPending     = new List<EmptyBalanceB__c>(); //Lst Historic order
        EmptyBalanceB__c[] LstAllBRB         = new List<EmptyBalanceB__c>(); // Lst Current+Historic+BousEB
        EmptyBalanceB__c[] LstEBProductEmpty = new List<EmptyBalanceB__c>(); //Lst Current order
        WrapperBRBG ObjWrpBRBG;
        Datetime StrDate = System.now();

        Account ObjAcc = CTRSOQL.getAccountbyId(IdAcc);//getDaysCredit(IdAcc);
        Integer IntCD  = ObjAcc.ONTAP__EmptyLoanDays__c != null ? Integer.valueOf(ObjAcc.ONTAP__EmptyLoanDays__c) : 0;
        //se suman los dias de credito a la fecha de vencimiento        
        DateTime DtDD = ObjAcc.ISSM_EmptyCreditQty__c ? Datetime.parse(StrDate.format())+IntCD+1 : Datetime.parse(StrDate.format())+1;
        DateTime StrDateBon = Datetime.parse(StrDate.format())+1;

        for(ONTAP__Order_Item__c objEOI : LstEOI){       
            if((objEOI.ISSM_Is_returnable__c && objEOI.ISSM_EmptyRack__c != null) || objEOI.ISSM_EmptyRack__c != null ){
                ObjBRB = new EmptyBalanceB__c();
                ObjBRB.Product__c           = objEOI.ONCALL__OnCall_Product__c;
                ObjBRB.Account__c           = IdAcc;
                ObjBRB.OrderID__c           = IdOrder;
                ObjBRB.RackMaterial__c      = objEOI.ISSM_RackMaterialProduct__c;
                ObjBRB.BoxesNumReturn__c    = objEOI.ONCALL__OnCall_Quantity__c;
                ObjBRB.BoxesNumRequest__c   = objEOI.ONCALL__OnCall_Quantity__c;
                ObjBRB.Balance__c           = objEOI.ONCALL__OnCall_Quantity__c;
                ObjBRB.EmptyBalanceKey__c   = objEOI.ISSM_BalanceNum__c;
                ObjBRB.EmptyType__c         = objEOI.ISSM_EmptyType__c; 
                ObjBRB.DueDate__c           = DateTime.newInstance(DtDD.getTime()).Date();
                ObjBRB.Material_Number__c   = objEOI.ONCALL__SAP_Order_Item_Number__c;
                ObjBRB.Uint_Measure_Code__c = objEOI.ISSM_Uint_Measure_Code__c;
                ObjBRB.Process__c           = false;
                ObjBRB.EmptyRack__c         = objEOI.ISSM_EmptyRack__c;
                LstBRB.add(ObjBRB); 
            }
        }

        for(ISSM_Bonus__c objEBonus : LstBns){
            if(objEBonus.ISSM_Product__r.ISSM_Is_returnable__c && objEBonus.ISSM_Product__r.ISSM_EmptyRack__c != null){
                ObjBRB = new EmptyBalanceB__c();
                ObjBRB.Product__c           = objEBonus.ISSM_Product__c;
                ObjBRB.Account__c           = IdAcc; 
                ObjBRB.OrderID__c           = IdOrder;
                ObjBRB.RackMaterial__c      = objEBonus.ISSM_RackMaterialProduct__c;
                ObjBRB.BoxesNumReturn__c    = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.BoxesNumRequest__c   = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.Balance__c           = objEBonus.ISSM_BonusQuantity__c;
                ObjBRB.EmptyBalanceKey__c   = objEBonus.ISSM_Product__r.ISSM_EmptyMaterial__c; 
                ObjBRB.DueDate__c           = StrDateBon.date();
                ObjBRB.Material_Number__c   = objEBonus.ISSM_Material_Number__c;
                ObjBRB.Uint_Measure_Code__c = objEBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c;
                ObjBRB.Process__c           = false;
                ObjBRB.EmptyRack__c         = objEBonus.ISSM_Product__r.ISSM_EmptyRack__c;
                LstBRBonuses.add(ObjBRB);
               
            }
        }
        
         for(ONTAP__Product__c objProdEmB : LstProductEmB){
            System.debug('*********** OBJETO RACK : ');
            System.debug(objProdEmB.ISSM_QuantityInput__c);
            if(objProdEmB.ONCALL__Material_Number__c !=null){
                decimal valorBox = Decimal.valueOf(String.valueOf(objProdEmB.ISSM_QuantityInput__c));
        
                ObjBRB = new EmptyBalanceB__c();
                
                ObjBRB.Product__c           = objProdEmB.Id;
                ObjBRB.Account__c           = IdAcc;
                ObjBRB.OrderID__c           = IdOrder;
                ObjBRB.RackMaterial__c      = objProdEmB.ONTAP__MaterialProduct__c;
                ObjBRB.MaterialProduct__c   = objProdEmB.ONCALL__OnCall_Product__c;
                ObjBRB.BoxesNumReturn__c    = valorBox;
                ObjBRB.BoxesNumRequest__c   = valorBox;
                ObjBRB.Balance__c           = valorBox;
                ObjBRB.EmptyBalanceKey__c   = objProdEmB.ISSM_EmptyMaterial__c;
                ObjBRB.DueDate__c           = StrDateBon.date();
                ObjBRB.Material_Number__c   = objProdEmB.ONCALL__Material_Number__c;
                ObjBRB.Uint_Measure_Code__c = objProdEmB.ISSM_Uint_Measure_Code__c;
                ObjBRB.Process__c           = false; 
                ObjBRB.EmptyMaterial__c     = objProdEmB.ISSM_EmptyMaterial__c;      
            }
            LstEBProductEmpty.add(ObjBRB);
        }
         
        //Se agregan todos los envases para consolidarse en una sola lista
        LstAllBRB.addAll(LstEBProductEmpty);
        LstAllBRB.addAll(LstBRB);
        LstAllBRB.addAll(LstBRBonuses);
        LstBRBPending = getBoxRackPending(IdAcc); 
        LstAllBRB.addAll(LstBRBPending);//Add List Empty Balance Historic
        ObjWrpBRBG = groupBoxRackBalance(LstAllBRB,DtDD,IntCD,IdOrder,IdAcc,ObjAcc.ISSM_EmptyCreditQty__c);//return Empty Balance Group
        
        return new WrpBoxRackBalance(LstBRB,LstBRBonuses,LstBRBPending,ObjWrpBRBG,LstEBProductEmpty);
    }*/
    
    //Obtiene el listado de envases por historico
    public static EmptyBalanceB__c[] getBoxRackPending(String IdAcc){
        Id RecType = CTRSOQL.getRecordTypeId('EmptyBalanceB__c','Compact');
        // "CAJA%"" DEIFINIDO POR EL AREA FUNCIONAL
        EmptyBalanceB__c[] LstEBPending = CTRSOQL.getEmptyBalanceBByIdAcc(IdAcc,'CAJA%',RecType);

        for(EmptyBalanceB__c ObjEB : LstEBPending){           
            ObjEB.EmptyRack__c       = ObjEB.Product__r.ONCALL__Material_Number__c;
            ObjEB.RackMaterial__c    = ObjEB.Product__r.ONTAP__MaterialProduct__c;
            ObjEB.BoxesNumRequest__c = ObjEB.Balance__c;
            ObjEB.BoxesNumReturn__c  = ObjEB.Balance__c;
        }

        if(!LstEBPending.isEmpty()){
            update LstEBPending;
        }

        return LstEBPending;
    }


    //Agrupa los envases que lleguen por historico, corrientes y por bonificación
    public static WrapperBRBG groupBoxRackBalance(EmptyBalanceB__c[] LstEBInput,DateTime DtDueDate,Integer DaysOfCredit,
                                                  String IdOrder,String IdAcc,Boolean BlnSlct){
                                                  	
        Map<String, EmptyBalanceB__c[]> MapEBGroup = new Map<String, EmptyBalanceB__c[]>();
        BoxRackBalanceGroup[] LstWrpObjBRBGroup          = new List<BoxRackBalanceGroup>();
        EmptyBalanceB__c[] LstEB = new List<EmptyBalanceB__c>(); 
        EmptyBalanceB__c OEB     = new EmptyBalanceB__c();
        Map<String,Decimal> MapEB      = new Map<String,Decimal>();        
        Integer boxesRet  = 0;
        Integer boxesReq  = 0;
        Integer MinReturn = 0;
        String StrMat;
        String IdProd;
        String MatNumber;

        LstEBInput.sort();//Ordena la lista
        //create Map for GROUP OF EMPTIES
        for(EmptyBalanceB__c recEB : LstEBInput){
        	System.debug('***recEB'+recEB);
            if(MapEBGroup.containsKey(recEB.EmptyRack__c))
                MapEBGroup.get(recEB.EmptyRack__c).add(recEB);
            else 
                MapEBGroup.put(recEB.EmptyRack__c, new List<EmptyBalanceB__c> { recEB });  
        }
		System.debug(' ** MapEBGroup rack : '+MapEBGroup);
        for(String recordId : MapEBGroup.keySet()){
            boxesRet    = 0;boxesReq = 0;
            MinReturn   = 0;StrMat   = null;
            Datetime StrDate = System.now()+1;

            for(EmptyBalanceB__c EBRecord : MapEBGroup.get(recordId)){
                //if(EBRecord.ONTAP__DueDate__c.format() == StrDate.format(Label.ISSM_FormatDate2))
                //    MinReturn += Integer.valueOf(EBRecord.ISSM_BoxesNumRequest__c) != null ? Integer.valueOf(EBRecord.ISSM_BoxesNumRequest__c) : 0;
                
                if(EBRecord.DueDate__c > StrDate && BlnSlct)
                    MinReturn-=Integer.valueOf(EBRecord.BoxesNumRequest__c);
                
                if(EBRecord.DueDate__c <= StrDate)
                    MinReturn+=Integer.valueOf(EBRecord.BoxesNumRequest__c);
                

                MatNumber =  EBRecord.EmptyRack__c; 
                boxesRet  += Integer.valueOf(EBRecord.BoxesNumReturn__c) != null ? Integer.valueOf(EBRecord.BoxesNumReturn__c) : 0;
                StrMat    =  EBRecord.RackMaterial__c;
                boxesReq  += Integer.valueOf(EBRecord.BoxesNumRequest__c) != null ? Integer.valueOf(EBRecord.BoxesNumRequest__c) : 0;
                IdProd    =  EBRecord.Product__c;
            }
            LstWrpObjBRBGroup.add(new BoxRackBalanceGroup(MatNumber,boxesReq,boxesRet,StrMat,DtDueDate,MinReturn,false,IdProd,IdOrder,IdAcc));
        }

        return new WrapperBRBG(LstWrpObjBRBGroup);
    }
     
    
    //****************************************  WRAPPERS *********************************************
    global class WrpBoxRackBalance{
        global EmptyBalanceB__c[] LstBRBOrder;
        global EmptyBalanceB__c[] LstBRBonuses;
        global EmptyBalanceB__c[] LstBRBPending;
        global WrapperBRBG LstBRBGroup;
        global EmptyBalanceB__c[] LstEBProductEmpty;
        
        
        //METODO WRAPPER
        public WrpBoxRackBalance(EmptyBalanceB__c[] LstBRBOrder, EmptyBalanceB__c[] LstBRBonuses, 
                                 EmptyBalanceB__c[] LstBRBPending,WrapperBRBG LstBRBGroup,EmptyBalanceB__c[] LstEBProductEmpty){
            this.LstBRBOrder=LstBRBOrder;
            this.LstBRBonuses = LstBRBonuses;
            this.LstBRBPending=LstBRBPending;
            this.LstBRBGroup=LstBRBGroup;
            this.LstEBProductEmpty =  LstEBProductEmpty;
        }
    }
    
    global class BoxRackBalanceGroup{
        global String MatNumber;
        global Integer NumReq;
        global Integer NumRet;
        global String Name;
        global DateTime DtDueDate;
        global Integer IntMin;
        global Boolean BlnSlct;
        global String IdProd;
        global String IdOrder;
        global String IdAcc;
        
        public BoxRackBalanceGroup(String MatNumber,Integer NumReq,Integer NumRet,String Name,DateTime DtDueDate,
                                   Integer IntMin,Boolean BlnSlct,String IdProd,String IdOrder,String IdAcc){
            this.MatNumber=MatNumber;
            this.NumReq=NumReq;
            this.NumRet=NumRet;
            this.Name=Name;
            this.DtDueDate=DtDueDate;
            this.IntMin=IntMin;
            this.BlnSlct=BlnSlct;
            this.IdProd=IdProd;
            this.IdOrder=IdOrder;
            this.IdAcc=IdAcc;
        }
    }
    
    global class WrapperBRBG{
        global BoxRackBalanceGroup[] WrpLstBRBGroup;

        public WrapperBRBG(BoxRackBalanceGroup[] WrpLstBRBGroup){
            this.WrpLstBRBGroup=WrpLstBRBGroup;
        }
    }    
}