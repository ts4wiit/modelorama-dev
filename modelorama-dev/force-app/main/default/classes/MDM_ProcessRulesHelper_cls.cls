/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ABInBev (MDM Rules)
Description: Class to process the validation rules for the fields of the object
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       09-01-2018   Rodrigo RESENDIZ (RR)   Initial version
2.0		  06-06-2018	Ivan Neria				Final version
*/
public class MDM_ProcessRulesHelper_cls {
    //Decorator class for MDM Accounts and Validation Result
    public class MDM_ValidationResult{
        private final List<MDM_Validation_Result__c> vResult;
        private final MDM_Account__c account;
        private final MDM_Temp_Account__c tmpAccount;
        MDM_ValidationResult(MDM_Account__c account,MDM_Temp_Account__c tmpAccount,List<MDM_Validation_Result__c> vResult){
            this.account = account;
            this.vResult = vResult;
            this.tmpAccount = tmpAccount;
        }
        public List<MDM_Validation_Result__c> getVResult(){
            return this.vResult;
        }
        public MDM_Account__c getAccount(){
            return this.account;
        }
        public MDM_Temp_Account__c getTmpAccount(){
            return this.tmpAccount;
        }
    }
    public static MDM_ValidationResult processRules(MDM_Temp_Account__c accScope, Map<String, List<String>> parametersList, Set<String> setSAGranularity, Set<String> setACenterGranularity, Set<String> setSTGranularity, Set<String> setKUNN2, Set<String> setKUNNR, Set<String> setPARVW_BP, Set<String>setIDs, Set<String> setClientType){
        List<MDM_Validation_Result__c> validationResults = new List<MDM_Validation_Result__c>();
        Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe(); 
        Map<String, Schema.SobjectField> MDMAccountFields = sObjectTypeMap.get(Label.CDM_Apex_MDM_Account_c).getDescribe().fields.getMap(); 
        Map<String, Schema.SobjectField> MDMTempAccountFields = sObjectTypeMap.get(Label.CDM_Apex_MDM_Temp_Account_c).getDescribe().fields.getMap(); 
        Set<String> invalidFields = new Set<String>();
        MDM_Rules_cls ruleApplied;
        SObject tmpAcc, account = new MDM_Account__c();
        String codeFieldName = '', objectName;
		
        objectName = accScope.getSObjectType().getDescribe().getName();
        Map<String, MDM_Rule__c> rulesThatApply = MDM_Account_cls.getRulesToApply(objectName);
    
        //Dynamically call and apply all rules
        for(MDM_Rule__c rule_obj : rulesThatApply.values()){
            ruleApplied = new MDM_Rules_cls(objectName, rule_obj.MDM_Code__c);
            if(rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule5){
                Set<String> queryAC = MDM_Account_cls.getAdmCenterGranularity(setACenterGranularity);
            	validationResults.addAll(ruleApplied.evaluateRule(accScope, queryAC));
            } else if(rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule6){
				Set<String> querySA = MDM_Account_cls.getSalesAreaGranularity(setSAGranularity);
                validationResults.addAll(ruleApplied.evaluateRule(accScope, querySA));
            } else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule7){
				Set<String> queryST = MDM_Account_cls.getSalesTeamGranularity(setSTGranularity);
                validationResults.addAll(ruleApplied.evaluateRule(accScope, queryST));
            } else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule22){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setKUNNR));
            }else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule27){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setClientType));
            }	else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule25){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setIDs));
            }	else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule28){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setIDs));
            }else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule24){
				setKUNNR.addAll(setKUNN2);
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setKUNNR));
            }else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule19){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setPARVW_BP));
            }else if (rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule18){
                validationResults.addAll(ruleApplied.evaluateRule(accScope, setKUNN2));
            }else {

                validationResults.addAll(ruleApplied.evaluateRule(accScope, null));
            }
            if(rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule14 || rule_obj.MDM_Code__c==Label.CDM_Apex_CodeRule8){
                accScope.CDM_Flag_Control__c=true;
            }
            
        }
        
        //initialize account values
        tmpAcc = accScope;
        account.put(Label.CDM_Apex_RecordTypeId, MDM_Account_cls.getAccountRT().get(accScope.KTOKD__c));
        account.put(Label.CDM_Apex_Unique_Id_c, accScope.Id_External__c);

        //add to set fields that have errors
        for(String field : MDMAccountFields.keySet()){
            for(SObject result : validationResults){
               // invalidFields.add(field);
                if(MDMAccountFields.get(field).getDescribe().getType().name() != 'REFERENCE' && field.contains(Label.CDM_Apex_PREFIX2) && !field.contains('unique') && !field.contains('mdm_')){
                    codeFieldName = field.remove('__c')+Label.CDM_Apex_PREFIX1;
                    try{
                        if(result.get(codeFieldName)!=null)
                            if(Boolean.valueOf(result.get(codeFieldName)))
                                invalidFields.add(field);
                    } catch(System.SObjectException e){
                        continue;
                    }
                }
            }
        }
        //START passing values from tmpAccount to Account
        for(String field : MDMAccountFields.keySet()){
            if(MDMAccountFields.get(field).getDescribe().getType().name() != 'Reference' && field.contains(Label.CDM_Apex_PREFIX2) && !field.contains('unique') && !field.contains('mdm_')){
                //remove after validation for picklists is ready .... && MDMAccountFields.get(field).getDescribe().getType().name() != 'Picklist' 
                //validate that field exists in both objects
                if(MDMTempAccountFields.containsKey(field) &&  !invalidFields.contains(field) && MDMAccountFields.get(field).getDescribe().isUpdateable()){
                    if(MDMAccountFields.get(field).getDescribe().getName()!=Label.CDM_Apex_ANRED_c){
                     if(MDMAccountFields.get(field).getDescribe().getType().name() == 'Boolean'){
                        if((String)tmpAcc.get(field)==Label.CDM_Apex_X||(String)tmpAcc.get(field)=='true' ){
                             account.put(field, true);
                        } else{
                            account.put(field, false);
                        }
                        
                    }else{
                        account.put(field, tmpAcc.get(field));
                    }   
                    }
                    
                }
            }
            
        }
        
        
        for(String field : MDMAccountFields.keySet()){
            if (MDMAccountFields.get(field).getDescribe().getType().name() == 'REFERENCE' && (MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD01Id_c || MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD02Id_c ||MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD03Id_c || MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD04Id_c || MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD05Id_c || MDMAccountFields.get(field).getDescribe().getName()==Label.CDM_Apex_TAXKD06Id_c )){
             if(field.contains(Label.CDM_Apex_PREFIX2) && !field.contains('unique') && !field.contains('mdm_')){
                //remove after validation for picklists is ready .... && MDMAccountFields.get(field).getDescribe().getType().name() != 'Picklist' 
                //validate that field exists in both objects
                if(MDMTempAccountFields.containsKey(field) && tmpAcc.get(field) != null && !invalidFields.contains(field) && MDMAccountFields.get(field).getDescribe().isUpdateable()){
                    if(MDMAccountFields.get(field).getDescribe().getName()!=Label.CDM_Apex_ANRED_c){
                     	if(MDMAccountFields.get(field).getDescribe().getType().name() == 'Boolean'){
                        	if((String)tmpAcc.get(field)==Label.CDM_Apex_X||(String)tmpAcc.get(field)=='true' ){
                             	account.put(field, true);
                        	} else{
                            	account.put(field, false);
                        	}
                        
                    	} else{
                        	account.put(field, tmpAcc.get(field));
                    	}   
                    }
                }
            }   
         }
            
            
        }
		//passing values from text to the corresponding relationship
        for(MDM_AccountFieldsMapping__mdt mappedFields : MDM_Account_cls.getAccountMappingLst().Values() ){
            codeFieldName = (String)mappedFields.MasterLabel+Label.CDM_Apex_PREFIX2;
            if(!invalidFields.contains(codeFieldName)  && tmpAcc.get(codeFieldName) != null) {
                if(mappedFields.Related_Object_Name__c == Label.CDM_Apex_MDM_Parameter_c){ System.debug('P1 = ' + (String)tmpAcc.get(codeFieldName));System.debug('P2 = ' + MDM_Account_cls.getInstance().get(mappedFields.Catalog__c));
                    if(codeFieldName==Label.CDM_Apex_ANRED_c && MDM_Account_cls.getInstanceByDescription().get(mappedFields.Catalog__c).containsKey((String)tmpAcc.get(codeFieldName))){
                        account.put(mappedFields.Field_Api_Name__c, MDM_Account_cls.getInstanceByDescription().get(mappedFields.Catalog__c).get((String)tmpAcc.get(codeFieldName)).Id);
                        account.put(Label.CDM_Apex_ANRED_c, MDM_Account_cls.getInstanceByDescription().get(mappedFields.Catalog__c).get((String)tmpAcc.get(codeFieldName)).Code__c);
                    }else if(MDM_Account_cls.getInstance().get(mappedFields.Catalog__c).containsKey((String)tmpAcc.get(codeFieldName))){
                      account.put(mappedFields.Field_Api_Name__c, MDM_Account_cls.getInstance().get(mappedFields.Catalog__c).get((String)tmpAcc.get(codeFieldName)).Id);
                    }
                        
                }else if(mappedFields.Related_Object_Name__c == Label.CDM_Apex_Account){
                    if(mappedFields.Related_Object_Field__c == Label.CDM_Apex_ONTAP_SalesOgId_c){
                        if(MDM_Account_cls.getSalesOrg().containsKey((String)tmpAcc.get(codeFieldName)))
                            account.put(mappedFields.Field_Api_Name__c, MDM_Account_cls.getSalesOrg().get((String)tmpAcc.get(codeFieldName)).Id);
                    }else if(mappedFields.Related_Object_Field__c == Label.CDM_Apex_ONTAP_SalesOfficeId_c){
                        if(MDM_Account_cls.getSalesOffice().containsKey((String)tmpAcc.get(codeFieldName)))
                            account.put(mappedFields.Field_Api_Name__c, MDM_Account_cls.getSalesOffice().get((String)tmpAcc.get(codeFieldName)).Id);
                    }
                }else{
                    if(MDM_Account_cls.getCatalogObject(mappedFields.Related_Object_Name__c, mappedFields.Related_Object_Field__c, parametersList.get(codeFieldName)).containsKey((String)tmpAcc.get(codeFieldName))){
                        account.put(mappedFields.Field_Api_Name__c, 
                                   MDM_Account_cls.getCatalogObject(mappedFields.Related_Object_Name__c, mappedFields.Related_Object_Field__c, parametersList.get(codeFieldName)).get((String)tmpAcc.get(codeFieldName)));
                    }
                } 
            }
        }
        //FINISH passing values from tmpAccount to Account
        return new MDM_ValidationResult((MDM_Account__c)account,accScope,validationResults);
    }
}