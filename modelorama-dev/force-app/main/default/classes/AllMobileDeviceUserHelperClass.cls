/**
 * This class serves as helper class for AllMobileDeviceUserTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileDeviceUserHelperClass {

	/**
	 * This method enqueue a Job to initiate the synchronization of Device User object to Heroku when insert or update.
	 *
	 * @param lstAllMobileDeviceUser	List<AllMobileDeviceUser__c>
	 * @param strEventTriggerFlagApplication	String
	 */
	public static void syncDeviceUserWithHeroku(List<AllMobileDeviceUser__c> lstAllMobileDeviceUser, String strEventTriggerFlag) {
		if(lstAllMobileDeviceUser != NULL && !lstAllMobileDeviceUser.isEmpty()) {
			AllMobileDeviceUserOperationClass objAllMobileDeviceUserOperationClass = new AllMobileDeviceUserOperationClass(lstAllMobileDeviceUser, strEventTriggerFlag);
			ID IdEnqueueJob = System.enqueueJob(objAllMobileDeviceUserOperationClass);
			//for(AllMobileDeviceUser__c objAllMobileDeviceUser : lstAllMobileDeviceUser) {
				//objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
				//objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
			//}
		}
	}

	/*
	public static void updatePasswordFields(List<AllMobileDeviceUser__c> lstAllMobileDeviceUser) {
		if(lstAllMobileDeviceUser != NULL && !lstAllMobileDeviceUser.isEmpty()) {
			for(AllMobileDeviceUser__c objAllMobileDeviceUser : lstAllMobileDeviceUser) {
				objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
				objAllMobileDeviceUser.AllMobileDeviceUserPasswordEncrypted__c = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
			}
		}
	}
	*/
}