/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_Account_tst {
    static Account dataAccount;
    static String RecordTypeAccountId;


    @isTest static void testGetAccountNotNull() {
        try{
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        } catch(NullPointerException e){
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
        }
        dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        Test.startTest();
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        Account account = accountCls.getAccount(dataAccount.Id);
        System.assert( account != null,'Account is not null');
        System.assertEquals(dataAccount.Id,account.Id);
        Test.stopTest();
    }

    @isTest static void testGetAccountNull() {
        Test.startTest();
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        System.assert(accountCls.getAccount(null) == null,'Account is null');
        Test.stopTest();
    }

    @isTest static void testGetMapAccountSapCustomerId(){
        try{
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        } catch(NullPointerException e){
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
        }
        dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        List<Account> accounts = new List<Account>{dataAccount};
        Test.startTest();
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        accountCls.getMapAccountSapCustomerId(accounts);
        Test.stopTest();
    }

    @isTest static void testGetMapAccountId(){
        try{
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        } catch(NullPointerException e){
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
        }
        dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        List<Account> accounts = new List<Account>{dataAccount};
        Test.startTest();
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        accountCls.getMapAccountId(accounts);
        Test.stopTest();
    }


    @isTest static void testGetParnertAccount(){
        List<Account> accounts = new List<Account>();
        Set<Id> SETaccounts = new Set<Id>();
       //Generamos RegionalSalesdivision
         
        String RecordTypeRegional = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();        
        Account objCreateRegionalDiv  = new Account();
        objCreateRegionalDiv.Name ='RegionaSales';
        objCreateRegionalDiv.RecordTypeId = RecordTypeRegional;
        insert  objCreateRegionalDiv;
        //Generamos SalesOrg
        String RecordTypeSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();        
        Account objTypeSalesOrg  = new Account();
        objTypeSalesOrg.Name ='SalesOrg';
        objTypeSalesOrg.ISSM_ParentAccount__c = objCreateRegionalDiv.Id;
        objTypeSalesOrg.RecordTypeId =RecordTypeSalesOrg;
         insert objTypeSalesOrg; 
        //Generamos SalesOffice
        String RecordTypeSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        Account ObjTypeSalesOffice  = new Account();
        ObjTypeSalesOffice.Name ='SalesOffcie';
        ObjTypeSalesOffice.ISSM_ParentAccount__c = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.ParentId = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.RecordTypeId = RecordTypeSalesOffice;
        insert ObjTypeSalesOffice;
        try{
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        } catch(NullPointerException e){
            RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
        }
        dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        accounts.add(dataAccount);
        SETaccounts.add(dataAccount.Id);
        Test.startTest();
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        accountCls.getParnertAccount(dataAccount.Id);
        accountCls.getDRVs();
        accountCls.getAccountsByParentId(ObjTypeSalesOffice.ISSM_ParentAccount__c,true);
        accountCls.getSetSpecificField(accounts,'ISSM_RegionalSalesDivision__c');
        accountCls.getMapAccountId(SETaccounts);
        //accountCls.getSetSpecificField(accounts,'');
        Test.stopTest();
    }   
}