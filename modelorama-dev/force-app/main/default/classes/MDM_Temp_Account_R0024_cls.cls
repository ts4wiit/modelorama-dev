/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria
Project:  ABInBev (MDM Rules)
Description: MDM_Rules child class to implement Rule 24
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       16-05-2018   	Iván Neria    			Initial version 
2.0		  06-06-2018	Ivan Neria				Final Version
*/
public class MDM_Temp_Account_R0024_cls extends MDM_RulesService_cls {
    public MDM_Temp_Account_R0024_cls(){
        super();
        validationResults = new List<MDM_Validation_Result__c>();
    }
    
    public override List<MDM_Validation_Result__c> evaluateRule(SObject toEvaluate, String ruleCode, String objectName, Set<String> setsGranularity){
        MDM_Rule__c rule = MDM_Account_cls.getRulesToApply(objectName).get(ruleCode);
        SObject validationResult = new MDM_Validation_Result__c();
        String fieldName = '';
        Boolean ruleHasError = false;
        if(rule.MDM_IsActive__c){
            validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
            
            
            for(MDM_FieldByRule__c field : uniqueFields(rule.MDM_Fields_by_Rule__r).Values()){
                //Validation for start proccess when the record is SH 
                    if(field.MDM_IsActive__c && (toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z019 || toEvaluate.get(Label.CDM_Apex_KTOKD_c)==Label.CDM_Apex_Z010)) {
                    	if(MDM_Account_cls.getZ001(setsGranularity).contains((String)toEvaluate.get(Label.CDM_Apex_PARVW_c) )){
                			if ( toEvaluate.get(Label.CDM_Apex_AUFSD_c)==null  || toEvaluate.get(Label.CDM_Apex_LIFSD_c)==null || toEvaluate.get(Label.CDM_Apex_FAKSD_c)==null){
                    			  fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        		  validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c);
                        		  validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        		  ruleHasError = true; 
                            } else if (toEvaluate.get(Label.CDM_Apex_AUFSD_c)!=Label.CDM_Apex_01 || toEvaluate.get(Label.CDM_Apex_LIFSD_c)!=Label.CDM_Apex_01 || toEvaluate.get(Label.CDM_Apex_FAKSD_c)!=Label.CDM_Apex_01) {
                                  fieldName = field.MDM_APIFieldName__c.substring(0,(field.MDM_APIFieldName__c.length()-3));
                        		  validationResult.put(Label.CDM_Apex_Unique_Id_c,toEvaluate.get(Label.CDM_Apex_Id_External_c)+field.MDM_APIFieldName__c);
                        		  validationResult.put((fieldName + DESCRIPTION_SUFIX), rule.MDM_Error_Message__c);
                        		  validationResult.put((fieldName + HAS_ERROR_SUFIX), true);
                        		  validationResult.put(RULE_ID_FIELD_NAME, rule.Id);
                        		  ruleHasError = true; 
                            }
            			}		   
                }
                if(ruleHasError) validationResults.add((MDM_Validation_Result__c)validationResult);
                validationResult = new MDM_Validation_Result__c();
                validationResult.put((objectName.substring(0,(objectName.length()-3)))+Label.CDM_Apex_PREFIX4, toEvaluate.get(Label.CDM_Apex_Id));
                ruleHasError = false;
            }
        }
        return validationResults;
    }
}