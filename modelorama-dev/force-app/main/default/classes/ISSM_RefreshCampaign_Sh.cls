global class ISSM_RefreshCampaign_Sh implements Schedulable {
    global void execute(SchedulableContext sc) {
        System.debug('***************************************************  ISSM_RefreshCampaign_Sh');
        //MyBatchClass b = new MyBatchClass();
        //database.executebatch(b);
        List <Telecollection_Campaign__c> campaignToInactivate = new List<Telecollection_Campaign__c>();
        Boolean refreshCampaing = true;
        String soqlFilters = '';
        Integer counter = 0;
        List<Telecollection_Campaign__c> telecollectionCampaignList = [SELECT id,Active__c,CreatedDate,End_Date__c,OwnerId,SoqlFilters__c,Start_Date__c,Name FROM Telecollection_Campaign__c WHERE Active__c = true AND HONES_Country__c = 'México'];
        for(Telecollection_Campaign__c telecollectionCampaign : telecollectionCampaignList){
            
            counter++;
            refreshCampaing = telecollectionCampaign.End_Date__c < System.today() ? false: true;
            soqlFilters = telecollectionCampaign.SoqlFilters__c == null ? '' : telecollectionCampaign.SoqlFilters__c;
            System.debug('PASA1 --- '+counter);
            ISSM_InactivateCallTelecollection_bch BtchProces = new ISSM_InactivateCallTelecollection_bch(telecollectionCampaign.Name, telecollectionCampaign.Start_Date__c, telecollectionCampaign.End_Date__c, telecollectionCampaign.Active__c, telecollectionCampaign.Id, soqlFilters, refreshCampaing); 
            database.executeBatch(BtchProces,150);
            System.debug('FIN PASO1');
            
            if(!refreshCampaing){ 
                telecollectionCampaign.Active__c = false;
                campaignToInactivate.add(telecollectionCampaign);
            }
           

        }
        update campaignToInactivate;
        System.debug('*************************************************** FIN  ISSM_RefreshCampaign_Sh'+campaignToInactivate);
    }
}