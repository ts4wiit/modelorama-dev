/**
 * This class is the controller extension for the AllMobileDeviceUserCategoryTypePage visualforce page.
 * <p /><p />
 * @author Alberto Gómez
 */
public  class AllMobileDeviceUserCATUserTypeController {

	//Variables get/set.
	public List<SelectedPermissions> lstSelectedPermissions {get; set;}

	//Object variables.
	public AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserType;
	public AllMobilePermissionCategoryUserType__c objAllMobilePermissionCategoryUserTypeEmpty = new AllMobilePermissionCategoryUserType__c();


	/**
	 * Constructor.
	 *
	 * @param objStandardController	ApexPages.StandardController.
	 */
	public AllMobileDeviceUserCATUserTypeController(ApexPages.StandardController objStandardController) {
		objAllMobilePermissionCategoryUserType = (AllMobilePermissionCategoryUserType__c)objStandardController.getRecord();
		if(objAllMobilePermissionCategoryUserType != objAllMobilePermissionCategoryUserTypeEmpty && objAllMobilePermissionCategoryUserType != NULL) {
			showSelectedPermissions();
		}
	}

	/**
	 * Wrapper class for AllMobilePermission__c object.
	 */
	public class SelectedPermissions {
		public AllMobilePermission__c objAllMobilePermission {get; set;}
		public Boolean boolSelectedPermission {get; set;}
		/**
		 * Constructor of the wrapper class.
		 */
		public SelectedPermissions(AllMobilePermission__c objAllMobilePermissionConstructor) {
			objAllMobilePermission = objAllMobilePermissionConstructor;
			boolSelectedPermission = false;
		}
	}

	/**
	 * This method return a list of the Selected Permissions wrapper class.
	 *
	 * @return lstSelectedPermissions	List<SelectedPermissions>.
	 */
	public List<SelectedPermissions> getPermissions() {
		if(lstSelectedPermissions == null) {
			lstSelectedPermissions = new List<SelectedPermissions>();
			for(AllMobilePermission__c objAllMobilePermissionQueried : [SELECT Id, Name, AllMobileDeviceUserPermissionKey__c, AllMobileDeviceUserBinaryPosition__c FROM AllMobilePermission__c ORDER BY AllMobileDeviceUserBinaryPosition__c ASC]) {
				lstSelectedPermissions.add(new SelectedPermissions(objAllMobilePermissionQueried));
			}
		}
		return lstSelectedPermissions;
	}

	/**
	 * This method show the selected permissions in the wrapper class when update a AllMobilePermissionCategoryUserType__c record.
	 */
	public void showSelectedPermissions() {
		lstSelectedPermissions = getPermissions();
		List<String> lstStringBinaryPermission = (objAllMobilePermissionCategoryUserType.AllMobileDeviceUserBinaryPermission__c).split(AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);
		Integer intI = 0;
		for(SelectedPermissions objSelectedPermissions : lstSelectedPermissions) {
			for(intI = 0; intI < lstStringBinaryPermission.size(); intI++) {
				if(objSelectedPermissions.objAllMobilePermission.AllMobileDeviceUserBinaryPosition__c == intI) {
					if(lstStringBinaryPermission[intI] == AllMobileStaticVariablesClass.STRING_DEVICE_USER_CATEGORY_USER_TYPE_BINARY_VALUE_ONE) {
						objSelectedPermissions.boolSelectedPermission = true;
					} else if(lstStringBinaryPermission[intI] == AllMobileStaticVariablesClass.STRING_DEVICE_USER_CATEGORY_USER_TYPE_BINARY_VALUE_ZERO) {
						objSelectedPermissions.boolSelectedPermission = false;
					}
				}
			}
		}
	}
}