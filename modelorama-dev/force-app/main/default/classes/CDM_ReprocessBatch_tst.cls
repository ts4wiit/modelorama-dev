/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_ReprocessBatch_bch". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_ReprocessBatch_tst {
    @testSetup static void createData(){
       //Load test data
       List<sObject> ls = Test.loadData(MDM_Temp_Account__c.sObjectType, 'MDMTempAccount'); 
        
    }
    static testmethod void testBatch() {
        MDM_Temp_Account__c temp = [SELECT Id, MDM_Control_Check__c FROM MDM_Temp_Account__c LIMIT 1];
        temp.MDM_Control_Check__c = false;
        update temp;
        
        Test.startTest();
        CDM_ReprocessBatch_bch b = new CDM_ReprocessBatch_bch();
        Database.executeBatch(b);
        Test.stopTest();
        
        //Assert validation: Validate that all records in MDM_Temp_Account__c be updated to false in the field MDM_Control_Check__c
        List<MDM_Temp_Account__c> accs = [SELECT Id, MDM_Control_Check__c FROM MDM_Temp_Account__c WHERE MDM_Control_Check__c=false ];
        System.assertEquals(0, accs.size());
    }
    

}