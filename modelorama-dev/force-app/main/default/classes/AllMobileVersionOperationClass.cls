/**
 * This class contains the operations for the synchronization of AllMobileVersionHelperClass records.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileVersionOperationClass implements Queueable{

	//Variables.
	String strEventTriggerFlag;
	List<AllMobileVersion__c> lstAllMobileVersion = new List<AllMobileVersion__c>();

	//Constructor.
	public AllMobileVersionOperationClass(List<AllMobileVersion__c> lstAllMobileVersionTrigger, String strEventTriggerFlagTrigger) {
		strEventTriggerFlag = strEventTriggerFlagTrigger;
		lstAllMobileVersion = lstAllMobileVersionTrigger;
	}

	/**
	 * This method initiate a List of Version objects in Salesforce into Heroku version table using POST method (insert).
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 */
	public static void syncInsertAllMobileVersionToExternalObject(List<AllMobileVersion__c> lstAllMobileVersion) {
		Set<Id> setIdAllMobileVersion = new Set<Id>();
		if(!lstAllMobileVersion.isEmpty()) {
			setIdAllMobileVersion = getSetIdAllMobileVersionSFFromListAllMobileVersionSF(lstAllMobileVersion);
			AllMobileVersionOperationClass.insertExternalAllMobileVersionWS(setIdAllMobileVersion);
		}
	}

	/**
	 * This method prepares a List of Version objects in Salesforce to communicate asynchronously and insert into Heroku version table.
	 *
	 * @param setIdAllMobileVersion	Set<Id>
	 */
	@future(callout = true)
	public static void insertExternalAllMobileVersionWS(Set<Id> setIdAllMobileVersion) {
		AllMobileVersionOperationClass.insertExternalAllMobileVersion(setIdAllMobileVersion);
	}

	/**
	 * This method sync a List of Version objects from  Salesforce into a Heroku version table using POST method (insert).
	 *
	 * @param setIdAllMobileVersion	Set<Id>
	 * @return Boolean.
	 */
	public static Boolean insertExternalAllMobileVersion(Set<Id> setIdAllMobileVersion) {

		//Variables.
		List<AllMobileVersion__c> lstAllMobileVersionToInsertHeroku = new List<AllMobileVersion__c>();
		AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass();

		//Get List of All Version in SF.
		List<AllMobileVersion__c> lstAllMobileVersionSF = [SELECT Id, Name, AllMobileVersionId__c, AllMobileSoftDeleteFlag__c, AllMobileApplicationId__c, AllMobileApplicationMD__c, LastModifiedDate FROM AllMobileVersion__c WHERE Id =: setIdAllMobileVersion];

		//Get List of All Version in HK.
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileVersionHerokuObjectClass = getAllMobileAllVersionHeroku();

		//Compare both lists to avoid insert Version SF records that already exists in HK. Generates a new list to be inserted in Heroku.
		for(AllMobileVersion__c objAllMobileVersionSF :lstAllMobileVersionSF) {
			Integer intContainedVersionSFInVersionHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClassIterate : lstAllMobileVersionHerokuObjectClass) {
				if(objAllMobileVersionSF.AllMobileVersionId__c == objAllMobileVersionHerokuObjectClassIterate.version_id) {
					intContainedVersionSFInVersionHeroku++;
				}
			}
			if(intContainedVersionSFInVersionHeroku == 0) {
				lstAllMobileVersionToInsertHeroku.add(objAllMobileVersionSF);
			}
		}

		//Insert the final list of Version (SF) into Heroku.
		if(!lstAllMobileVersionToInsertHeroku.isEmpty()){
			for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersionToInsertHeroku) {
				objAllMobileVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass();
				objAllMobileVersionHerokuObjectClass.version_id = objAllMobileVersion.AllMobileVersionId__c;
				objAllMobileVersionHerokuObjectClass.application_id = Integer.valueOf(objAllMobileVersion.AllMobileApplicationId__c);
				objAllMobileVersionHerokuObjectClass.softdeleteflag = objAllMobileVersion.AllMobileSoftDeleteFlag__c;
				objAllMobileVersionHerokuObjectClass.dtlastmodifieddate = objAllMobileVersion.LastModifiedDate;

				//Rest callout to insert Version in Heroku.
				String strStatusCode = AllMobileSyncObjectsClass.calloutInsertVersionIntoHeroku(objAllMobileVersionHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method initiate a List of Version objects in Salesforce into Heroku version table using PUT method (update).
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 */
	public static void syncUpdateAllMobileVersionToExternalObject(List<AllMobileVersion__c> lstAllMobileVersion) {
		Set<Id> setIdAllMobileVersion = new Set<Id>();
		if(!lstAllMobileVersion.isEmpty()) {
			setIdAllMobileVersion = getSetIdAllMobileVersionSFFromListAllMobileVersionSF(lstAllMobileVersion);
			AllMobileVersionOperationClass.updateExternalAllMobileVersionWS(setIdAllMobileVersion);
		}
	}

	/**
	 * This method prepares a List of Version objects in Salesforce to communicate asynchronously and update into Heroku version table.
	 *
	 * @param setIdAllMobileVersion	Set<Id>
	 */
	@future(callout = true)
	public static void updateExternalAllMobileVersionWS(Set<Id> setIdAllMobileVersion) {
		AllMobileVersionOperationClass.updateExternalAllMobileVersion(setIdAllMobileVersion);
	}

	/**
	 * This method sync a List of Version objects from  Salesforce into a Heroku version table using PUT method (update).
	 *
	 * @param setIdAllMobileVersion Set<Id>
	 * @return Boolean.
	 */
	public static Boolean updateExternalAllMobileVersion(Set<Id> setIdAllMobileVersion) {

		//Variables.
		List<AllMobileVersion__c> lstAllMobileVersionToUpdateInHeroku = new List<AllMobileVersion__c>();
		AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass();

		//Get List of Version in SF.
		List<AllMobileVersion__c> lstAllMobileVersionSF = [SELECT Id, Name, AllMobileVersionId__c, AllMobileSoftDeleteFlag__c, AllMobileApplicationId__c, AllMobileApplicationMD__c, LastModifiedDate FROM AllMobileVersion__c WHERE Id =: setIdAllMobileVersion];

		//Get List of All Version in Heroku.
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileVersionHerokuObjectClass = getAllMobileAllVersionHeroku();

		//Compare both lists to avoid update Version SF that do not exist in HK. Generates new List to be updated in Heroku.
		for(AllMobileVersion__c objAllMobileVersionSF : lstAllMobileVersionSF) {
			Integer intContainedVersionSFInVersionHeroku = 0;
			for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClassIterate : lstAllMobileVersionHerokuObjectClass) {
				if(objAllMobileVersionSF.AllMobileVersionId__c == objAllMobileVersionHerokuObjectClassIterate.version_id) {
					intContainedVersionSFInVersionHeroku++;
				}
			}
			if(intContainedVersionSFInVersionHeroku > 0) {
				lstAllMobileVersionToUpdateInHeroku.add(objAllMobileVersionSF);
			}
		}

		//Update the Final List of Version (SF) into Heroku.
		if(!lstAllMobileVersionToUpdateInHeroku.isEmpty()) {
			for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersionToUpdateInHeroku) {
				objAllMobileVersionHerokuObjectClass = new AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass();
				objAllMobileVersionHerokuObjectClass.version_id = objAllMobileVersion.AllMobileVersionId__c;
				objAllMobileVersionHerokuObjectClass.application_id = Integer.valueOf(objAllMobileVersion.AllMobileApplicationId__c);
				objAllMobileVersionHerokuObjectClass.softdeleteflag = objAllMobileVersion.AllMobileSoftDeleteFlag__c;
				objAllMobileVersionHerokuObjectClass.dtlastmodifieddate = objAllMobileVersion.LastModifiedDate;

				//Rest callout to update Version in Heroku.
				String strStatusCode = AllMobileSyncObjectsClass.calloutUpdateVersionIntoHeroku(objAllMobileVersionHerokuObjectClass);
			}
		}
		return false;
	}

	/**
	 * This method delete a List of Version objects from  Salesforce according to a Heroku version table using DELETE method.
	 *
	 * @param lstAllMobileVersionInSF	List<AllMobileVersion__c>
	 */
	public static void syncDeleteAllMobileVersionInSF(List<AllMobileVersion__c> lstAllMobileVersionInSF) {

		//Inner variables.
		List<AllMobileVersion__c> lstAllMobileVersionToBeDeletedInSF = new List<AllMobileVersion__c>();
		List<String> lstVersionFromVersionHeroku = new List<String>();

		//Get All Version From Heroku.
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileAllVersionHerokuObjectClass = getAllMobileAllVersionHeroku();

		//Get List of Version of Version Heroku to compare.
		for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass : lstAllMobileAllVersionHerokuObjectClass) {
			lstVersionFromVersionHeroku.add(objAllMobileVersionHerokuObjectClass.version_id);
		}

		//Identify Version SF to be Deleted.
		for(AllMobileVersion__c objAllMobileVersionInSF : lstAllMobileVersionInSF) {
			if(!lstVersionFromVersionHeroku.contains(objAllMobileVersionInSF.AllMobileVersionId__c)) {
				lstAllMobileVersionToBeDeletedInSF.add(objAllMobileVersionInSF);
			}
		}
		if(!lstAllMobileVersionToBeDeletedInSF.isEmpty() && lstAllMobileVersionToBeDeletedInSF != NULL) {
			delete lstAllMobileVersionToBeDeletedInSF;
		}
	}

	/**
	 * This method insert and update a List of Version objects from  Salesforce according to a Heroku version table using POST and PUT methods.
	 *
	 * @param lstAllMobileVersionInSF List<AllMobileVersion__c>
	 */
	public static void syncInsertAndUpdateAllMobileVersionInSFFromVersionHeroku(List<AllMobileVersion__c> lstAllMobileVersionInSF) {

		//Inner variables.
		List<AllMobileVersion__c> lstAllMobileVersionToBeInsertedInSF = new List<AllMobileVersion__c>();
		List<String> lstVersionIdFromAllMobileVersionSF = new List<String>();
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileVersionHerokuObjectClassToBeInsertedInSF = new List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass>();
		AllMobileVersion__c objAllMobileVersionToBeInsertedInSF = new AllMobileVersion__c();
		List<String> lstApplicationIdFromAllVersionHerokuObject = new List<String>();

		//Get All Version From Heroku.
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileAllVersionHerokuObjectClass = getAllMobileAllVersionHeroku();

		//Get AppliccationId List From All Version Heroku.
		for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass : lstAllMobileAllVersionHerokuObjectClass) {
			lstApplicationIdFromAllVersionHerokuObject.add(String.valueOf(objAllMobileVersionHerokuObjectClass.application_id));
		}

		//Get AllMobileApplication From AllVersion Heroku.
		List<AllMobileApplication__c> lstAllMobileApplicationFromAllVersionHerokuObject = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileShipType__c, AllMobileLevelValidation__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c WHERE AllMobileApplicationId__c =: lstApplicationIdFromAllVersionHerokuObject];

		//Get List of Version Id from AllMobileVersion SF.
		for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersionInSF) {
			lstVersionIdFromAllMobileVersionSF.add(objAllMobileVersion.AllMobileVersionId__c);
		}

		//Identify Version From Heroku to be Inserted in SF.
		for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass : lstAllMobileAllVersionHerokuObjectClass) {
			if(!lstVersionIdFromAllMobileVersionSF.contains(objAllMobileVersionHerokuObjectClass.version_id)) {
				lstAllMobileVersionHerokuObjectClassToBeInsertedInSF.add(objAllMobileVersionHerokuObjectClass);
			}
		}

		//Convert Version From Heroku to AllMobileVersion in SF.
		for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass : lstAllMobileVersionHerokuObjectClassToBeInsertedInSF) {
			for(AllMobileApplication__c objAllMobileApplicationToCompare : lstAllMobileApplicationFromAllVersionHerokuObject) {
				if(String.valueOf(objAllMobileVersionHerokuObjectClass.application_id) == objAllMobileApplicationToCompare.AllMobileApplicationId__c) {
					objAllMobileVersionToBeInsertedInSF = new AllMobileVersion__c();
					objAllMobileVersionToBeInsertedInSF.AllMobileApplicationMD__c = objAllMobileApplicationToCompare.Id;
					objAllMobileVersionToBeInsertedInSF.AllMobileVersionId__c = objAllMobileVersionHerokuObjectClass.version_id;
					objAllMobileVersionToBeInsertedInSF.AllMobileApplicationId__c = String.valueOf(objAllMobileVersionHerokuObjectClass.application_id);
					objAllMobileVersionToBeInsertedInSF.AllMobileSoftDeleteFlag__c = objAllMobileVersionHerokuObjectClass.softdeleteflag;
					lstAllMobileVersionToBeInsertedInSF.add(objAllMobileVersionToBeInsertedInSF);
				}
			}
		}
		if(!lstAllMobileVersionToBeInsertedInSF.isEmpty() && lstAllMobileVersionToBeInsertedInSF != NULL) {
			insert lstAllMobileVersionToBeInsertedInSF;
		}

		//UPDATING.
		List<AllmobileVersion__c> lstAllMobileVersionToBeUpdatedInSF = new List<AllmobileVersion__c>();
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstAllMobileVersionHerokuObjectClassExistingInSF = new List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass>();

		//Identify Version From Heroku to be Updated in SF.
		for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass : lstAllMobileAllVersionHerokuObjectClass) {
			if(lstVersionIdFromAllMobileVersionSF.contains(objAllMobileVersionHerokuObjectClass.version_id)) {
				lstAllMobileVersionHerokuObjectClassExistingInSF.add(objAllMobileVersionHerokuObjectClass);
			}
		}

		//Generate list of Version to update.
		for(AllMobileVersion__c objAllMobileVersionToCompareToUpdate : lstAllMobileVersionInSF) {
			for(AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate : lstAllMobileVersionHerokuObjectClassExistingInSF) {
				if(objAllMobileVersionToCompareToUpdate.AllMobileVersionId__c == objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate.version_id) {
					if((objAllMobileVersionToCompareToUpdate.AllMobileApplicationId__c != String.valueOf(objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate.application_id)) || (objAllMobileVersionToCompareToUpdate.AllMobileSoftDeleteFlag__c != objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate.softdeleteflag)) {
						for(AllMobileApplication__c objAllMobileApplicationToCompare : lstAllMobileApplicationFromAllVersionHerokuObject) {
							if(String.valueOf(objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate.application_id) == objAllMobileApplicationToCompare.AllMobileApplicationId__c) {
								objAllMobileVersionToCompareToUpdate.AllMobileApplicationMD__c = objAllMobileApplicationToCompare.Id;
								objAllMobileVersionToCompareToUpdate.AllMobileApplicationId__c = objAllMobileApplicationToCompare.AllMobileApplicationId__c;
								objAllMobileVersionToCompareToUpdate.AllMobileSoftDeleteFlag__c = objAllMobileVersionHerokuObjectClassToCompareToUpdateIterate.softdeleteflag;
								lstAllMobileVersionToBeUpdatedInSF.add(objAllMobileVersionToCompareToUpdate);
							}
						}
					}
				}
			}
		}
		if(!lstAllMobileVersionToBeUpdatedInSF.isEmpty() && lstAllMobileVersionToBeUpdatedInSF != NULL) {
			update lstAllMobileVersionToBeUpdatedInSF;
		}
	}

	/**
	 * This method initiate a remote action to insert and update a List of Version objects of Salesforce according to a Heroku version table using POST AND PUT methods
	 */
	@RemoteAction
	webservice static void syncInsertUpdateVersionFromHeroku() {
		List<AllMobileVersion__c> lstAllMobileVersionInSF = [SELECT Id, Name, AllMobileVersionId__c, AllMobileApplicationMD__c, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileVersion__c];
		syncInsertAndUpdateAllMobileVersionInSFFromVersionHeroku(lstAllMobileVersionInSF);
	}

	/**
	 * This method get a List of all Version records of Heroku.
	 *
	 * @return List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass>
	 */
	public static List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> getAllMobileAllVersionHeroku() {
		String strJsonSerializeAllVersionHeroku = AllMobileSyncObjectsClass.calloutReadAllVersionFromHeroku();
		List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass> lstJsonDeserializeAllAllMobileVersionHerokuObjectClass = (List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass>)JSON.deserialize(strJsonSerializeAllVersionHeroku, List<AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass>.class);
		return lstJsonDeserializeAllAllMobileVersionHerokuObjectClass;
	}

	/**
	 * This method get and set a List of Version Ids of Salesforce.
	 *
	 * @param lstAllMobileVersion	List<AllMobileVersion__c>
	 * @return setIdAllMobileVersion	Set<Id>
	 */
	public static Set<Id> getSetIdAllMobileVersionSFFromListAllMobileVersionSF(List<AllMobileVersion__c> lstAllMobileVersion) {
		Set<Id> setIdAllMobileVersion = new Set<Id>();
		if(!lstAllMobileVersion.isEmpty()) {
			for(AllMobileVersion__c objAllMobileVersion : lstAllMobileVersion) {
				setIdAllMobileVersion.add(objAllMobileVersion.Id);
			}
		}
		return setIdAllMobileVersion;
	}

	/**
	 * This method execute a queueable trigger which insert and update a List of Version Ids of Salesforce.
	 *
	 * @param objQueueableContext QueueableContext
	 */
	public void execute(QueueableContext objQueueableContext) {
		if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_VERSION) {
			syncInsertAllMobileVersionToExternalObject(lstAllMobileVersion);
		} else if(strEventTriggerFlag == AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_VERSION) {
			syncUpdateAllMobileVersionToExternalObject(lstAllMobileVersion);
		}
	}
}