public class Agri_DeliveryOrders_Ctrl {
    
    public Delivery_order__c orden {get;set;}
    public Agri_Contract__c contrato {get;set;}
    public String contractId {get;set;}
    public String  enviarMail {get;set;}
    public Integer numOrdenes {get;set;}    
    public String aplicaOtroWH {get;set;}
    public String validationWarning {get;set;}
    public Decimal totalToneladas {get;set;}
    public Boolean continuar {get;set;}  
    public Decimal toneladasRegistradasEnContrato {get;set;}
    public Decimal toneladasEstimadasEntregadasContrato {get;set;}
    public Decimal totalRealFaltante {get;set;}
    public Boolean muestraDisponibilidad {get;set;}
    
    public Agri_DeliveryOrders_Ctrl(ApexPages.StandardController controller) {
        orden = new Delivery_order__c();
        orden = (Delivery_order__c)controller.getRecord();  
        muestraDisponibilidad = false;
        contractId = ApexPages.currentPage().getparameters().get('contractId');
        getContract();
        numOrdenes = 1;        
        totalToneladas = 0;        
        continuar = false;
        aplicaOtroWH = ApexPages.currentPage().getparameters().get('regeneraOrden');
    }
    
    public void getContract() {        
        System.debug('--> Contrato ID : ' + contractId); 
        
        if(contractId != '' && contractId != null) {
            contrato = new Agri_Contract__c();
            contrato = [SELECT Id, Agri_Farmer__c, Agri_Tons_to_Produce__c, Agri_Farmer__r.ONTAP__Email__c, Agri_re_barleyAddendum__c, Agri_nu_barleyTotalReceived__c, Agri_Centre_Barley__c FROM Agri_Contract__c WHERE Id = : contractId];
            System.debug('--> Contract : ' + contrato);
            if(contrato != null) {
                orden.Agri_ls_centre__c = contrato.Agri_Centre_Barley__c;
                orden.Agri_Agriculture__c = contrato.Agri_Farmer__c;
                orden.Agri_Contract__c = contrato.Id;
            }
            System.debug('--> Orden : ' + orden);
            
            toneladasRegistradasEnContrato = 0;
            for(Delivery_order__c dor : [SELECT Id, Agri_Estimated_weight__c FROM Delivery_order__c WHERE Agri_Contract__c = : contrato.Id AND Agri_rb_almacen__r.agri_ls_warehouseCode__c = 'IASA' AND Agri_ls_status__c != 'Cancelada' AND Agri_ls_status__c != 'Rechazada' AND Agri_ls_status__c != 'Orden cancelada en SAP' AND Agri_ls_status__c != 'Enviar a SAP' AND Agri_ls_status__c != 'Orden creada en SAP' AND Agri_ls_status__c != 'Error en creación SAP' AND Agri_ls_status__c != 'Error en cancelación SAP']) {
                if(dor.Agri_Estimated_weight__c != null) {
                    toneladasRegistradasEnContrato += dor.Agri_Estimated_weight__c; 
                }                
            }
            System.debug('--> toneladasRegistradasEnContrato : ' + toneladasRegistradasEnContrato);
            
            toneladasEstimadasEntregadasContrato = 0;
            for(Delivery_order__c dor : [SELECT Id, Agri_Estimated_weight__c FROM Delivery_order__c WHERE Agri_Contract__c = : contrato.Id AND Agri_ls_status__c = 'Entregada']) {
                if(dor.Agri_Estimated_weight__c != null) {
                    toneladasEstimadasEntregadasContrato += dor.Agri_Estimated_weight__c; 
                }                
            }   
			System.debug('--> toneladasEstimadasEntregadasContrato : ' + toneladasEstimadasEntregadasContrato);            
            
            totalRealFaltante = 0;
            if(toneladasEstimadasEntregadasContrato != null && contrato.Agri_nu_barleyTotalReceived__c != null) {
                totalRealFaltante = toneladasEstimadasEntregadasContrato - contrato.Agri_nu_barleyTotalReceived__c;
            } 
            System.debug('--> totalRealFaltante : ' + totalRealFaltante);  
        }
    }        
    
    public PageReference validar() {        
        PageReference result = null;                
        
        if(contrato != null) {
            
            List<Agri_Warehouse__c> almacen = [SELECT Id, Agri_fm_percentageSurplus__c, Agri_nu_contractedCapacity__c, Agri_nu_totalDelivered__c, Agri_nu_totalCapacity__c, agri_ls_warehouseCode__c 
                                               FROM Agri_Warehouse__c WHERE Id = : orden.Agri_rb_almacen__c LIMIT 1];
            System.debug('--> almacen : ' + almacen);
            
            if(almacen[0].agri_ls_warehouseCode__c != 'IASA') {
                //---ORDENES DE ENTREGA SON PARA ALMACENES INTERNOS (!= IASA)
                //Valida que el numero de ordenes y el peso estimado > 0
                if(numOrdenes > 0 && orden.Agri_Estimated_weight__c > 0 && contrato.Agri_Tons_to_Produce__c > 0) {
                    validationWarning = '';
                    result = generaOrdenes();
                } else {
                    continuar = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'El número de ordenes de entrega a crear, el peso estimado de la orden y la producción contratada del contrato debe ser mayor a 0'));
                }                 
            } else {
                //---ORDENES DE ENTREGA SON PARA ALMACENES EXTERNOS (== IASA)                             
                Decimal toneladasTodasOrdenesAlmacen = 0;
                for(Delivery_order__c dor : [SELECT Id, Agri_Estimated_weight__c FROM Delivery_order__c WHERE Agri_rb_almacen__c = : orden.Agri_rb_almacen__c AND Agri_Contract__c != : contrato.Id AND Agri_ls_status__c != 'Cancelada' AND Agri_ls_status__c != 'Rechazada' AND Agri_ls_status__c != 'Orden cancelada en SAP']) {
                    if(dor.Agri_Estimated_weight__c != null) {
                        toneladasTodasOrdenesAlmacen += dor.Agri_Estimated_weight__c; 
                    }                
                }
                System.debug('--> toneladasTodasOrdenesAlmacen : ' + toneladasTodasOrdenesAlmacen);
                
                //Valida que el numero de ordenes y el peso estimado > 0
                if(numOrdenes > 0 && orden.Agri_Estimated_weight__c > 0 && contrato.Agri_Tons_to_Produce__c > 0) {                                   
                    Decimal toneladasEstimadas = orden.Agri_Estimated_weight__c * numOrdenes;                             
                    totalToneladas = toneladasRegistradasEnContrato + toneladasEstimadas; 
                    Decimal totalTodasToneladas = totalToneladas + toneladasTodasOrdenesAlmacen;                                                            
                    
                    //Valida que hayan sido capturados los campos de Capacidad Cotnratado, porcentaje Extra y total Recibido en el registro de almacen agro
                    if(almacen[0].Agri_nu_totalDelivered__c != null && almacen[0].Agri_fm_percentageSurplus__c != null && almacen[0].Agri_nu_contractedCapacity__c > 0) {
                        Decimal toneladasAcumuladas = totalTodasToneladas + almacen[0].Agri_nu_totalDelivered__c;
                        Decimal capacidadExtra = almacen[0].Agri_nu_contractedCapacity__c * (almacen[0].Agri_fm_percentageSurplus__c / 100);    
                        Decimal capacidadMaxima = almacen[0].Agri_nu_contractedCapacity__c +  capacidadExtra;
                        
                        //Valida si lo que se va a generar excede a la capacidad maxima del almacen (total contratado mas porcentaje extra)
                        if(toneladasAcumuladas <= capacidadMaxima) {
                            //Valida si ya se rebasó la capacidad contratada pero entra aun dentro del porcentaje extra
                            if(toneladasAcumuladas >= almacen[0].Agri_nu_contractedCapacity__c) {
                                continuar = false;
                                validationWarning = 'La suma de los pesos estimados de las ordenes de entrega a generar excede la capacidad contratada del almacen, pero puede almacenar en el % extra asignado para ese almacen. Desea continuar?';
                            } else {
                                validationWarning = '';
                                result = generaOrdenes();
                            }                                                    
                        } else {
                            continuar = false;
                            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'No puede guardar las ordenes de entrega, ya que la suma de sus pesos estimados excede la capacidad contratada del almacen (incluyendo el % extra)'));                        
                        }
                    } else {
                        continuar = false;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Antes de generar las ordenes, debe capturar la capacidad contratada del almacen y el % extra. Contacte a su administrador para llenar estos campos en el registro de almacen'));
                    }                                                                                     
                } else {
                    continuar = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'El número de ordenes de entrega a crear, el peso estimado de la orden y la producción contratada del contrato debe ser mayor a 0'));
                }                 
            }                                                 
        }       
        
        return result;
    }
    
    public PageReference generaOrdenes() {  
        
        String estatusOrden = 'Nueva';            
        Agri_Warehouse__c aw = [SELECT Id, Agri_ls_warehouseCode__c FROM Agri_Warehouse__c WHERE Id = : orden.Agri_rb_almacen__c LIMIT 1];
        
        if(enviarMail == 'SI') {
            if(aw.Agri_ls_warehouseCode__c == 'IASA') {  //Se manda al agricultor, no se manda a SAP   IASA Externo                      
                estatusOrden = 'Enviada Agricultor';
            } else {  //Se manda a SAP primero, cuando sea exitoso se debe dar clic en boton enviar a agricultor   != IASA Interno
                estatusOrden = 'Enviar a SAP';
            }
        }
        
        
        ID rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Agri_tr_deliveryOrder' AND SObjectType = 'Delivery_order__c'].Id; 
        List<Delivery_order__c> lOrdenesACrear = new List<Delivery_order__c>();
        if(aw.Agri_ls_warehouseCode__c != 'IASA') {
            System.debug('--> ENTRA A INTERNO');
            for(Integer i = 0; i <= numOrdenes - 1; i++) {
                Delivery_order__c nuevaOrden = new Delivery_order__c();
                nuevaOrden.Agri_Agriculture__c = orden.Agri_Agriculture__c;
                nuevaOrden.Agri_Surface__c = orden.Agri_Surface__c;
                nuevaOrden.Agri_Contract__c = orden.Agri_Contract__c;
                nuevaOrden.Agri_Delivery_place_requested__c = orden.Agri_Delivery_place_requested__c;
                nuevaOrden.Agri_Cycle__c = orden.Agri_Cycle__c;
                nuevaOrden.Agri_Other_delivery__c = orden.Agri_Other_delivery__c;
                nuevaOrden.Agri_Category__c = orden.Agri_Category__c;
                nuevaOrden.Agri_Estimated_weight__c = orden.Agri_Estimated_weight__c;
                nuevaOrden.Agri_Delivery_date__c = orden.Agri_Delivery_date__c;
                nuevaOrden.Agri_Observations__c = orden.Agri_Observations__c;
                nuevaOrden.Agri_ls_centre__c = orden.Agri_ls_centre__c;
                nuevaOrden.Agri_rb_almacen__c = orden.Agri_rb_almacen__c;
                nuevaOrden.Agri_Producer_Email__c = contrato.Agri_Farmer__r.ONTAP__Email__c;
                nuevaOrden.Agri_ls_status__c = estatusOrden;
                nuevaOrden.RecordTypeId = rtId;                
                lOrdenesACrear.add(nuevaOrden);                    
            } 
        } else {                                                                     
            Decimal toneladasContratoAdd = contrato.Agri_Tons_to_Produce__c + contrato.Agri_re_barleyAddendum__c + totalRealFaltante;  //(contrato.Agri_Tons_to_Produce__c + contrato.Agri_re_barleyAddendum__c) - entregado; 
            System.debug('--> ENTRA A ALMACEN EXTERNO');  
            System.debug('--> toneladasEstimadasEntregadasContrato : ' + toneladasEstimadasEntregadasContrato);
            System.debug('--> contrato.Agri_Tons_to_Produce__c : ' + contrato.Agri_Tons_to_Produce__c);
            System.debug('--> contrato.Agri_re_barleyAddendum__c : ' + contrato.Agri_re_barleyAddendum__c);
            System.debug('--> totalRealFaltante : ' + totalRealFaltante);
            System.debug('--> totalToneladas : ' + totalToneladas);
            System.debug('--> toneladasContratoAdd : ' + toneladasContratoAdd);                        
            
            if(totalToneladas <= toneladasContratoAdd) {
                for(Integer i = 0; i <= numOrdenes - 1; i++) {
                    Delivery_order__c nuevaOrden = new Delivery_order__c();
                    nuevaOrden.Agri_Agriculture__c = orden.Agri_Agriculture__c;
                    nuevaOrden.Agri_Surface__c = orden.Agri_Surface__c;
                    nuevaOrden.Agri_Contract__c = orden.Agri_Contract__c;
                    nuevaOrden.Agri_Delivery_place_requested__c = orden.Agri_Delivery_place_requested__c;
                    nuevaOrden.Agri_Cycle__c = orden.Agri_Cycle__c;
                    nuevaOrden.Agri_Other_delivery__c = orden.Agri_Other_delivery__c;
                    nuevaOrden.Agri_Category__c = orden.Agri_Category__c;
                    nuevaOrden.Agri_Estimated_weight__c = orden.Agri_Estimated_weight__c;
                    nuevaOrden.Agri_Delivery_date__c = orden.Agri_Delivery_date__c;
                    nuevaOrden.Agri_Observations__c = orden.Agri_Observations__c;
                    nuevaOrden.Agri_ls_centre__c = orden.Agri_ls_centre__c;
                    nuevaOrden.Agri_rb_almacen__c = orden.Agri_rb_almacen__c;
                    nuevaOrden.Agri_Producer_Email__c = contrato.Agri_Farmer__r.ONTAP__Email__c;
                    nuevaOrden.Agri_ls_status__c = estatusOrden;
                    nuevaOrden.RecordTypeId = rtId;                
                    lOrdenesACrear.add(nuevaOrden);                    
                } 
            } else {
                continuar = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'La suma del peso estimado de las ordenes de entrega de este contrato debe corresponder al Tonelaje Disponible por Registrar (producción contratada más adendas de Compra de Cebada, más el faltante real de producto recibido en el contrato).'));
            }
        }
          
        if(!lOrdenesACrear.isEmpty()) {
            try {
                System.debug('---> GUARDA OE : ' + lOrdenesACrear);
                insert lOrdenesACrear;
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Se han generado correctamente las ordenes de entrega'));                                                                                       
                clearvalidationWarning();
                continuar = true;
                asignaContrato();
            } catch(Exception e) {
                System.debug('ERROR : ' + e);
                continuar = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Ha ocurrido un error al generar las ordenes de entrega: ' + e.getMessage()));
            }            
        }                    
                
        return null;
    }   
    
    public void asignaContrato() {
        System.debug('--> Asigna contrato nulo');
        if(orden.Agri_Contract__c != null) {
            contractId = orden.Agri_Contract__c;
            getContract();
        }
    }
    
    public void validaWH() {
        System.debug('--> Entra valida Almace');
        muestraDisponibilidad = false;
        if(orden.Agri_rb_almacen__c != null) {
            for(Agri_Warehouse__c aw : [SELECT Id, Agri_ls_warehouseCode__c FROM Agri_Warehouse__c WHERE Id = : orden.Agri_rb_almacen__c]) {
                if(aw.Agri_ls_warehouseCode__c == 'IASA') {
                    muestraDisponibilidad = true;
                }
            }
        }
        System.debug('--> muestraDisponibilidad : ' + muestraDisponibilidad);
    }
    
    public void clearvalidationWarning(){
        validationWarning = '';
    }        
    
    public PageReference regresar() {	
        PageReference myVFPage = null;
        
        if(contrato != null) {
            myVFPage = new PageReference('/' + contrato.Id);
        } else {
            myVFPage = new PageReference('/home/home.jsp');
        }        
        return myVFPage;
    }    
        
}