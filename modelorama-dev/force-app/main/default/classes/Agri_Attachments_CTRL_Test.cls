@isTest
private class Agri_Attachments_CTRL_Test {

      Profile profile = [Select Id From Profile where Name = 'System Administrator' limit 1];
User usr1 = new User();
/*usr1.ProfileID = profile.Id;
insert usr1;
System.runAs(usr1){}*/

      static testMethod void  ObtainAttachment() {    

                  
                    
                   //perform test
                  Test.startTest();
                  
                  PageReference pageRef = Page.Agri_Attachments;
                  Test.setCurrentPage(pageRef);
                  
                   //Material de prueba de Semilla
                   Agri_Material__c semilla = new Agri_Material__c(
                   Agri_Code__c='Material Test Semilla',
                   Agri_Code_Description__c='Agronegocios',
                   Agri_SKU__c='2003256',
                   Agri_Type__c='Semilla'                   
                   );
                   
                   insert semilla;
                   
                   //Material de prueba de Cebada
                   Agri_Material__c cebada = new Agri_Material__c(
                   Agri_Code__c='IA01',
                   Agri_Code_Description__c='Agronegocios',
                   Agri_SKU__c='2003257',
                   Agri_Type__c='Cebada'                   
                   );
                   
                   insert cebada;
                   
                   //Farmer
                   Account farmer = new Account(
                   
                   Name='Cuenta Test',
                   ONTAP__SAP_Number__c='12345',
                   Agri_Bank_Account__c='12345678901',
                   Phone='5534567890',                   
                   ISSM_RFC__c='HEAL840110KT7',
                   Agri_CURP__c='HEAL840110HMCRLS06',
                   Agri_CLABE__c='062456789012345678',
                   //ONTAP.Email__c='Test@correo.com',
                   BillingState='Aguascalientes',
                   //ONTAP.Municipality__c='ASIENTOS',
                   Agri_Location__c='Location 1',
                   //ONTAP.Colony__c = 'Colony 1',
                   //ONTAP.Street_Number__c='551',
                   Agri_Bank__c='062 Banco Afirme, S.A.'
                   );     
                   
                   insert farmer;
                   
                   system.debug('Bog Farmer 1' + farmer);
                   
                   //Supplier                      
                   Agri_Supplier__c supplier = new Agri_Supplier__c(                   
                   Name='Cuenta Test',
                   Agri_SAP_Number__c='0000012345',
                   Agri_Bank_Account__c='12345678901',
                   Agri_Phone__c='5534567890',                   
                   Agri_RFC__c='HEAL840110KT7',
                   Agri_CURP__c='HEAL840110HMCRLS06',
                   Agri_CLABE__c='062456789012345678',
                   Agri_State__c='Aguascalientes',
                   Agri_Location__c='Location 1',
                   Agri_Bank__c='062',
                   Agri_First_Name__c='Enrique',
                   Agri_Last_Name__c='Gutierrez',
                   Agri_Contact_Last_Name__c='Torres',
                   Agri_Second_Last_Name__c='Alcantara',
                   Agri_Contact_Second_Last_Name__c='Larraga',
                   Agri_Mobile__c='5513428698',
                   Agri_Contact_Phone__c='5513428699',
                   Agri_Email__c='Correo@correo.com',
                   Agri_Contact_Email__c='Correo@correo.com',
                   Agri_Function__c='CO',
                   Agri_Contact_Function__c='CO'
                   
                   );      
                   
                   insert supplier;  
                   
                   system.debug('Bog supplier 1' + supplier);
                                                                        
                   Agri_Contract__c contract = new Agri_Contract__c(                                        
                   Agri_Farmer__c=farmer.Id,
                   Agri_Legal_Personality__c='F',
                   Agri_Variety_Seed__c= semilla.Id,
                   Agri_Centre_Barley__c='IA01',
                   Agri_Variety_Barley__c= cebada.Id,
                   Agri_Seed_yield__c = 16.2,
                   Agri_Seed_volume__c=16.2,
                   Agri_Hectares_to_Be_Planted__c=16.2,
                   Agri_Location_of_The_Property__c='Location',
                   Agri_Brand__c='Brand',
                   Agri_Applied_credit_seed__c='No',
                   Agri_Currency__c='MXN',
                   Agri_Total_Price_Per_Seed__c=7000 ,                  
                   Agri_Estimated_Harvest_Date__c=Date.newInstance(2018,06,2018),
                   Agri_Final_Purchase_Price__c=5000,
                   Agri_Total_Sale_of_Barley__c=50000  ,                 
                   Agri_Contractual_Penalty__c='Pena'
                   ); 
                   
                   insert contract;
                   
                   system.debug('Bog contract 1' + contract);
                   
                   string before = 'Testing base 64 encode';            
                   Blob beforeblob = Blob.valueOf(before);                            
                   //Insert contentdocument data
                   ContentVersion cv = new ContentVersion();
                   cv.title = 'test content';      
                   cv.PathOnClient ='test';           
                   cv.VersionData =beforeblob;          
                   insert cv;    
                   
                   ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

                    ContentDocumentLink newFileShare = new ContentDocumentLink();
                    newFileShare.contentdocumentid = testcontent.contentdocumentid;
                    newFileShare.LinkedEntityId = contract.Id;
                    newFileShare.ShareType= 'V';
                    insert newFileShare;
                    
                    system.debug('Bog newFileShare 1' + newFileShare);

                  // Put Id into the current page Parameters
                  ApexPages.currentPage().getParameters().put('id',contract.Id);
                  
                  Agri_Attachments_CTRL clase = new Agri_Attachments_CTRL(new ApexPages.StandardController(contract));
                  System.assertEquals(clase.lstOfAccounts.size(),0); 
                   
                   Test.StopTest();               
               }
               
               
              static testMethod void  ViewAttachment() {
                
                 //perform test
                  Test.startTest();
                  
                  PageReference pageRef = Page.Agri_Attachments;
                  Test.setCurrentPage(pageRef);
                  
                  //Material de prueba de Semilla
                   Agri_Material__c semilla = new Agri_Material__c(
                   Agri_Code__c='Material Test Semilla',
                   Agri_Code_Description__c='Agronegocios',
                   Agri_SKU__c='2003256',
                   Agri_Type__c='Semilla'                   
                   );
                   
                   insert semilla;
                   
                   //Material de prueba de Cebada
                   Agri_Material__c cebada = new Agri_Material__c(
                   Agri_Code__c='IA01',
                   Agri_Code_Description__c='Agronegocios',
                   Agri_SKU__c='2003257',
                   Agri_Type__c='Cebada'                   
                   );  
                   
                   insert cebada; 
                   
                                      //Farmer
                   Account farmer = new Account(
                   
                   Name='Cuenta Test',
                    ONTAP__SAP_Number__c='12345'
                   ); 
                   
                   insert farmer;    
                   
                   system.debug('Bog Farm' + farmer);
                   
                   //Supplier                      
                   Agri_Supplier__c supplier = new Agri_Supplier__c(                   
                   Name='Cuenta Test',
                   Agri_SAP_Number__c='0000012345',
                   Agri_Bank_Account__c='12345678901',
                   Agri_Phone__c='5534567890',                   
                   Agri_RFC__c='HEAL840110KT7',
                   Agri_CURP__c='HEAL840110HMCRLS06',
                   Agri_CLABE__c='062456789012345678',
                   Agri_State__c='Aguascalientes',
                   Agri_Location__c='Location 1',
                   Agri_Bank__c='062',
                   Agri_First_Name__c='Enrique',
                   Agri_Last_Name__c='Gutierrez',
                   Agri_Contact_Last_Name__c='Torres',
                   Agri_Second_Last_Name__c='Alcantara',
                   Agri_Contact_Second_Last_Name__c='Larraga',
                   Agri_Mobile__c='5513428698',
                   Agri_Contact_Phone__c='5513428699',
                   Agri_Email__c='Correo@correo.com',
                   Agri_Contact_Email__c='Correo@correo.com',
                   Agri_Function__c='CO',
                   Agri_Contact_Function__c='CO'
                   );     
                   
                   insert supplier;    
                   
                                   system.debug('Bog Supp' + supplier);
                  
                   Agri_Contract__c contract = new Agri_Contract__c(                                        
                   Agri_Farmer__c= farmer.Id,
                   Agri_Legal_Personality__c='F',
                   Agri_Variety_Seed__c=semilla.Id,
                   Agri_Centre_Barley__c='IA01',
                   Agri_Variety_Barley__c= cebada.Id,
                   Agri_Seed_yield__c = 16.2,
                   Agri_Seed_volume__c=16.2,
                   Agri_Hectares_to_Be_Planted__c=16.2,
                   Agri_Location_of_The_Property__c='Location',
                   Agri_Brand__c='Brand',
                   Agri_Applied_credit_seed__c='No',
                   Agri_Currency__c='MXN',
                   Agri_Total_Price_Per_Seed__c=7000 ,                  
                   Agri_Estimated_Harvest_Date__c=Date.newInstance(2018,06,2018),
                   Agri_Final_Purchase_Price__c=5000,
                   Agri_Total_Sale_of_Barley__c=50000  ,                 
                   Agri_Contractual_Penalty__c='Pena'); 
                   
                   insert contract;
                   
                   system.debug('BogContrat' + contract);
                   
                   string before = 'Testing base 64 encode';            
                   Blob beforeblob = Blob.valueOf(before);                            
                   //Insert contentdocument data
                   ContentVersion cv = new ContentVersion();
                   cv.title = 'test content';      
                   cv.PathOnClient ='test';           
                   cv.VersionData =beforeblob;          
                   insert cv;    
                   
                   ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

                    ContentDocumentLink newFileShare = new ContentDocumentLink();
                    newFileShare.contentdocumentid = testcontent.contentdocumentid;
                    newFileShare.LinkedEntityId = contract.Id;
                    newFileShare.ShareType= 'V';
                    insert newFileShare;
                    
                    system.debug('Bog FileShare' + newFileShare);

                  // Put Id into the current page Parameters
                  ApexPages.currentPage().getParameters().put('id',contract.Id);
                  
                  Agri_Attachments_CTRL clase = new Agri_Attachments_CTRL(new ApexPages.StandardController(contract));
                  
                  system.debug('Bog Final' + clase);
                  
                  System.assertNotEquals(null,clase.viewAttach()); 
                   
                   Test.StopTest(); 
                
                }
                
}