@isTest
public class Agri_DeliveryOrders_Test {

    static testMethod void generaOrdenesMasivas() {       
        PageReference pref = Page.Agri_DeliveryOrders_VF;
        Test.setCurrentPage(pref);
                        
        Agri_Material__c am = Agri_TestDataFactory.getMaterial();
        insert am;  
                        
        Account ag = Agri_TestDataFactory.getAgricultor();
        insert ag;
        
        Agri_Warehouse__c aw = Agri_TestDataFactory.getAlmacen();
        insert aw;
        
        Agri_Contract__c ac = Agri_TestDataFactory.getContrato();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Farmer__c = ag.Id;
        insert ac;
        
        Delivery_Order__c dor = Agri_TestDataFactory.getOrdenEntrega();
        dor.Agri_Agriculture__c = ag.Id;
        dor.Agri_Contract__c = ac.Id;
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw.Id;
        insert dor;
        
        Agri_Contract__c ac2 = Agri_TestDataFactory.getContrato();
        ac2.Agri_Variety_Barley__c = am.Id;
        ac2.Agri_Variety_Seed__c = am.Id;
        ac2.Agri_Farmer__c = ag.Id;
        insert ac2;
        
        Delivery_Order__c dor2 = Agri_TestDataFactory.getOrdenEntrega();
        dor2.Agri_Agriculture__c = ag.Id;
        dor2.Agri_Contract__c = ac2.Id;
        dor2.Agri_ls_centre__c = 'IA01';
        dor2.Agri_rb_almacen__c = aw.Id;
        insert dor2;
        
        ApexPages.currentPage().getParameters().put('contractId', ac.Id);
        ApexPages.currentPage().getParameters().put('regeneraOrden', 'MASS');
                
        ApexPages.StandardController con = new ApexPages.StandardController(dor);
        Agri_DeliveryOrders_Ctrl ext = new Agri_DeliveryOrders_Ctrl(con);
        
        Test.startTest();   
        ext.enviarMail = 'SI';
        ext.validar();
        ext.validaWH();
        ext.generaOrdenes();        
        ext.clearvalidationWarning();
        ext.regresar();
        ext.asignaContrato();
        Test.stopTest();
        
    }
    
    static testMethod void generaOrdenesMasivasFail() {       
        PageReference pref = Page.Agri_DeliveryOrders_VF;
        Test.setCurrentPage(pref);
                        
        Agri_Material__c am = Agri_TestDataFactory.getMaterial();
        insert am;  
                        
        Account ag = Agri_TestDataFactory.getAgricultor();
        insert ag;
        
        Agri_Warehouse__c aw = Agri_TestDataFactory.getAlmacen();
        insert aw;
        
        Agri_Contract__c ac = Agri_TestDataFactory.getContrato();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Farmer__c = ag.Id;
        insert ac;
        
        Delivery_Order__c dor = Agri_TestDataFactory.getOrdenEntrega();
        dor.Agri_Agriculture__c = ag.Id;
        dor.Agri_Contract__c = ac.Id;
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw.Id;
        insert dor;
        
        Agri_Contract__c ac2 = Agri_TestDataFactory.getContrato();
        ac2.Agri_Variety_Barley__c = am.Id;
        ac2.Agri_Variety_Seed__c = am.Id;
        ac2.Agri_Farmer__c = ag.Id;
        insert ac2;
        
        Delivery_Order__c dor2 = Agri_TestDataFactory.getOrdenEntrega();
        dor2.Agri_Agriculture__c = ag.Id;
        dor2.Agri_Contract__c = ac2.Id;
        dor2.Agri_ls_centre__c = 'IA01';
        dor2.Agri_rb_almacen__c = aw.Id;
        insert dor2;
        
        ApexPages.currentPage().getParameters().put('contractId', ac.Id);
        ApexPages.currentPage().getParameters().put('regeneraOrden', 'MASS');
                
        ApexPages.StandardController con = new ApexPages.StandardController(dor);
        Agri_DeliveryOrders_Ctrl ext = new Agri_DeliveryOrders_Ctrl(con);
        
        Test.startTest();    
        ext.numOrdenes = 100;
        ext.enviarMail = 'NO';
        ext.validar();
        ext.generaOrdenes();
        
        ext.contrato = null;
        ext.regresar();        
        Test.stopTest();
        
    }
    
    static testMethod void cancelaOrdenes() {       
        PageReference pref = Page.Agri_DeliveryOrderCancel_VF;
        Test.setCurrentPage(pref);
        
		Agri_Material__c am = Agri_TestDataFactory.getMaterial();
        insert am;  
                        
        Account ag = Agri_TestDataFactory.getAgricultor();
        insert ag;
        
        Agri_Warehouse__c aw = Agri_TestDataFactory.getAlmacen();
        insert aw;
        
        Agri_Contract__c ac = Agri_TestDataFactory.getContrato();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Farmer__c = ag.Id;
        insert ac;
        
        Delivery_Order__c dor = Agri_TestDataFactory.getOrdenEntrega();
        dor.Agri_Agriculture__c = ag.Id;
        dor.Agri_Contract__c = ac.Id;
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw.Id;
        insert dor;

		ApexPages.currentPage().getParameters().put('contractId', ac.Id);                
        Agri_DeliveryOrderCancel_Ctrl ctrl = new Agri_DeliveryOrderCancel_Ctrl();
        
        Test.startTest();   
        ctrl.actualizaSeleccionados();
        Agri_DeliveryOrderCancel_Ctrl.DeliveryOrderWrapper testWrap = new Agri_DeliveryOrderCancel_Ctrl.DeliveryOrderWrapper(dor);
        testWrap.isSelected = true;
        ctrl.lOrdersWrapper.add(testWrap);
		ctrl.actualizaSeleccionados();
        ctrl.regresar(); 
        ctrl.contractId = '';
        ctrl.regresar();
        Test.stopTest();        
        
    }
    
    static testMethod void generaOrdenesSalida() {       
        PageReference pref = Page.Agri_OutOfMerchandise_VF;
        Test.setCurrentPage(pref);
        
        Agri_Warehouse__c aw = Agri_TestDataFactory.getAlmacen();
        insert aw;
        
        list<RecordType> rList = [Select id From RecordType Where sObjectType = 'Delivery_Order__c' and RecordType.DeveloperName = 'Agri_tr_deliveryMerchandise'];
        
        Delivery_Order__c dor = Agri_TestDataFactory.getOrdenEntrega();
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw.Id;
        dor.Agri_nu_exitQuantityMerchandise__c = 0.5;
        dor.RecordTypeId = rList[0].Id;
        insert dor;

		ApexPages.currentPage().getParameters().put('almacenId', aw.Id); 
        ApexPages.StandardController con = new ApexPages.StandardController(dor);
        Agri_OutOfMerchandise_Ctrl extn = new Agri_OutOfMerchandise_Ctrl(con);
        
        Test.startTest();   
        extn.guardar();
        extn.regresar();
        extn.orden = null;
        extn.regresar();
        Test.stopTest();
        
		Agri_Warehouse__c aw2 = Agri_TestDataFactory.getAlmacen();
        insert aw2;        
        
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw2.Id;
        dor.Agri_nu_exitQuantityMerchandise__c = 0.4;
        update dor;
        
        delete dor;
    }
    
    static testMethod void testTriggerOrdenes() { 
    	Agri_Material__c am = Agri_TestDataFactory.getMaterial();
        insert am;  
                        
        Account ag = Agri_TestDataFactory.getAgricultor();
        insert ag;
        
        Agri_Warehouse__c aw = Agri_TestDataFactory.getAlmacen();
        insert aw;
        
        Agri_Contract__c ac = Agri_TestDataFactory.getContrato();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Farmer__c = ag.Id;
        ac.Agri_nu_barleyTotalReceived__c = 10;
        insert ac;
        
        Delivery_Order__c dor = Agri_TestDataFactory.getOrdenEntrega();
        dor.Agri_Agriculture__c = ag.Id;
        dor.Agri_Contract__c = ac.Id;
        dor.Agri_ls_centre__c = 'IA01';
        dor.Agri_rb_almacen__c = aw.Id;
        dor.Agri_nu_Conteo__c = 11;
        insert dor;
        
        Agri_receiving_orders__c aro = Agri_TestDataFactory.getEntrega();
        aro.Agri_rb_deliveryOrder__c = dor.Id;
		aro.Agri_rb_supplier__c = ag.Id;        
        insert aro;
        
        Agri_Quality_Control__c qa = Agri_TestDataFactory.getQA();
        qa.Agri_pd_entregaPedido__c = aro.Id;        
        insert qa;
        
        dor.Agri_ls_status__c = 'Entregada';
        update dor;
        
        Test.startTest();
        Agri_PayOrderEmail_Ctrl.gestionaEnvioPDF(qa.Id);
        Test.stopTest();
        
        delete dor;        
    }

    
}