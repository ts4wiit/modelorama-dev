/**************************************************************************************
Nombre de la Clase Appex: ISSM_CSAssignATMQuerys_cls
Versión : 1.0
Fecha de Creación : 18 Abril 2018
Funcionalidad : Clase de consultas para el proceso de asignación de usuarios a equipos de cuentas a nivel
Clase de Prueba: ISSM_AdminATM_Usuario_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        18 - Abril - 2018      Versión Original
*************************************************************************************/
public class ISSM_CSAssignATMQuerys_cls {

    public ISSM_CSAssignATMQuerys_cls() {

    }

    /***********************  Consultas de la clase de apex "ISSM_AdminCuentasATM_bch" ***********************/
    public List<Account> getAccountsRegistration(Set<String> CuentasAltaIds_set, String RecordTypeId_str) {
        List<Account> Acc_lst = new List<Account>();
        Acc_lst = [SELECT Id, ISSM_SalesOffice__c
                   FROM Account
                   WHERE ISSM_SalesOffice__c IN: CuentasAltaIds_set
                   AND RecordtypeId =: RecordTypeId_str
                   LIMIT 10000];
        return Acc_lst;
    }

    public List<Account> getAccountsDrop(Set<String> CuentasBajaIds_set, String RecordTypeId_str) {
        List<Account> Acc_Baja_lst = new List<Account>();
        Acc_Baja_lst = [SELECT Id, ISSM_SalesOffice__c
                        FROM Account
                        WHERE ISSM_SalesOffice__c IN: CuentasBajaIds_set
                        AND RecordtypeId =: RecordTypeId_str
                        LIMIT 10000];
        return Acc_Baja_lst;
    }

    public List<Account> getAccountsChange(Set<String> CuentasCambioIds_set, String RecordTypeId_str) {
        List<Account> Acc_Cambio_lst = new List<Account>();
        Acc_Cambio_lst = [SELECT Id, ISSM_SalesOffice__c
                          FROM Account
                          WHERE ISSM_SalesOffice__c IN: CuentasCambioIds_set
                          AND RecordtypeId =: RecordTypeId_str
                          LIMIT 10000];
        return Acc_Cambio_lst;
    }

    public List<AccountTeamMember> getAccountsChangeByUser(Set<String> CuentasCambioATMIds_set, Set<String> UsuariosIds_set) { List<AccountTeamMember> AccTM_Cambio_lst = new List<AccountTeamMember>(); AccTM_Cambio_lst = [SELECT Id, TeamMemberRole FROM AccountTeamMember WHERE AccountId IN: CuentasCambioATMIds_set AND UserId IN: UsuariosIds_set LIMIT 10000]; return AccTM_Cambio_lst; }

    public List<ISSM_ATM_Queue__c> getRegQueueToDelete() {
        List<ISSM_ATM_Queue__c> ATM_Queue_lst = new List<ISSM_ATM_Queue__c>();
        ATM_Queue_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_Ventas__c, Rol__c, Listo_para_Procesar__c
                         FROM ISSM_ATM_Queue__c
                         WHERE Listo_para_Eliminar__c = true
                         LIMIT 10000];
        return ATM_Queue_lst;
    }

    public List<CronTrigger> getJobCronTrigger(String AdminCuentasATM_str) {
        List<CronTrigger> CronTrigger_lst = new List<CronTrigger>();
        CronTrigger_lst = [SELECT Id
                           FROM  CronTrigger
                           WHERE CronJobDetail.Name =: AdminCuentasATM_str
                           LIMIT 10000];
        return CronTrigger_lst;
    }

    /**********************  Consultas de la clase de apex "ISSM_AdminATM_Usuario_ctr" **********************/
    /*public List<AccountTeamMember> getATMByUserByAccount(Id RegistroId, Id RecordTypeId_id) {
        List<AccountTeamMember> AccTM_lst = new List<AccountTeamMember>();
        AccTM_lst = [SELECT Id, UserId, AccountId, Account.Name, TeamMemberRole
                     FROM AccountTeamMember
                     WHERE UserId =: RegistroId
                     AND Account.RecordTypeId =: RecordTypeId_id
                     LIMIT 10000];
        return AccTM_lst;
    }*/
    
    public List<ISSM_ATM_Mirror__c> getATMByUserByAccount(Id RegistroId, Id RecordTypeId_id) {
        List<ISSM_ATM_Mirror__c> AccTM_lst = new List<ISSM_ATM_Mirror__c>();
        AccTM_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_ventas__c, Oficina_de_ventas__r.Name, Rol__c
                     FROM ISSM_ATM_Mirror__c
                     WHERE Usuario_relacionado__c =: RegistroId
                     AND Oficina_de_ventas__r.RecordTypeId =: RecordTypeId_id
                     LIMIT 10000];
        return AccTM_lst;
    }
    
    public List<ISSM_ATM_Mirror__c> getATMByUser(Id UserId_id) {
        List<ISSM_ATM_Mirror__c> AccTM_lst = new List<ISSM_ATM_Mirror__c>();
        AccTM_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_ventas__c, Oficina_de_ventas__r.Name, Rol__c
                     FROM ISSM_ATM_Mirror__c
                     WHERE Usuario_relacionado__c =: UserId_id
                     LIMIT 10000];
        return AccTM_lst;
    }

    public Account getAccountsByRTAccount(Id RecordTypeId_id) {
        Account acc = new Account();
        acc = [SELECT Id, Name, ParentId, ISSM_SalesOffice__c
               FROM Account
               WHERE RecordTypeId =: RecordTypeId_id
               LIMIT 1];
        return acc;
    }

    public AccountTeamMember getATM() {
        AccountTeamMember accTM = new AccountTeamMember();
        accTM = [SELECT Id, UserId, AccountId, Account.Name, TeamMemberRole
                 FROM AccountTeamMember
                 LIMIT 1];
        return accTM;
    }
    
    /*public List<AccountTeamMember> getATMLst() {
        List<AccountTeamMember> AccTM_lst = new List<AccountTeamMember>();
        AccTM_lst = [SELECT Id, UserId, AccountId, Account.Name, TeamMemberRole
                     FROM AccountTeamMember
                     LIMIT 10000];
        return AccTM_lst;
    }*/
    
    public List<ISSM_ATM_Mirror__c> getATMLst() {
        List<ISSM_ATM_Mirror__c> AccTM_lst = new List<ISSM_ATM_Mirror__c>();
        AccTM_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_ventas__c, Oficina_de_ventas__r.Name, Rol__c
                     FROM ISSM_ATM_Mirror__c
                     LIMIT 10000];
        return AccTM_lst;
    }

    public ISSM_ATM_Mirror__c getATMMirror() {
        ISSM_ATM_Mirror__c accTM_Mirror = new ISSM_ATM_Mirror__c();
        accTM_Mirror = [SELECT Id, Usuario_relacionado__c, Rol__c, Oficina_de_ventas__c
                        FROM ISSM_ATM_Mirror__c
                        LIMIT 1];
        return accTM_Mirror;
    }

    public List<ISSM_ATM_Mirror__c> getATMMirrorLst() {
        List<ISSM_ATM_Mirror__c> AccTM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        AccTM_Mirror_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_Ventas__c, Rol__c
                            FROM ISSM_ATM_Mirror__c
                            LIMIT 10000];
        return AccTM_Mirror_lst;
    }

    public List<ISSM_ATM_Mirror__c> getATMMirrorByUser(Id RegistroId) { List<ISSM_ATM_Mirror__c> AccTM_lst = new List<ISSM_ATM_Mirror__c>(); AccTM_lst = [SELECT Id, Usuario_relacionado__c, Oficina_de_Ventas__c, Rol__c FROM ISSM_ATM_Mirror__c WHERE Usuario_relacionado__c =: RegistroId LIMIT 10000]; return AccTM_lst; }
    
    /***************************  Consultas de la clase de apex "ISSM_Usuario_thr" **************************/
    public List<AccountTeamMember> getATMByUsers(String idUsr) { List<AccountTeamMember> AccountTeamMember_lst = new List<AccountTeamMember>(); AccountTeamMember_lst = [SELECT Id, UserId, AccountId, TeamMemberRole FROM AccountTeamMember WHERE UserId =: idUsr LIMIT 10000]; return AccountTeamMember_lst; }

    public List<AccountTeamMember> getATMBySetUsers(Set<String> UsuariosIds_set) { List<AccountTeamMember> AccountTeamMember_lst = new List<AccountTeamMember>(); AccountTeamMember_lst = [SELECT Id, UserId, AccountId, TeamMemberRole FROM AccountTeamMember WHERE UserId IN: UsuariosIds_set LIMIT 10000]; return AccountTeamMember_lst; }

    public List<ISSM_ATM_Mirror__c> getATMMirrorBySetUsers(Set<String> UsuariosIds_set) { List<ISSM_ATM_Mirror__c> AccountTeamMember_Mirror_lst = new List<ISSM_ATM_Mirror__c>(); AccountTeamMember_Mirror_lst = [SELECT Id, Oficina_de_Ventas__c, Oficina_de_Ventas__r.Name, Usuario_relacionado__c FROM ISSM_ATM_Mirror__c WHERE Usuario_relacionado__c IN: UsuariosIds_set LIMIT 10000]; return AccountTeamMember_Mirror_lst; }

    /**************************  Consultas de la clase de apex "ISSM_ATM_Mirror_thr" *************************/
    public List<ISSM_ATM_Mirror__c> getATMMirrorBySetSOBySetUsers(Set<String> OficinaVentasIds_set, Set<String> UsuariosIds_set) {
        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        ATM_Mirror_lst = [SELECT Id, Oficina_de_Ventas__c, Oficina_de_Ventas__r.Name, Usuario_relacionado__c, Rol__c
                          FROM ISSM_ATM_Mirror__c
                          WHERE Oficina_de_Ventas__c IN: OficinaVentasIds_set
                          AND Usuario_relacionado__c IN: UsuariosIds_set
                          AND Es_oficina_de_ventas__c = true
                          AND Es_cuenta__c = false
                          LIMIT 10000];
        return ATM_Mirror_lst;
    }

    public List<ISSM_ATM_Mirror__c> getATMMirrorById(Id regId) {
        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        ATM_Mirror_lst = [SELECT Id, Oficina_de_Ventas__c, Oficina_de_Ventas__r.Name, Usuario_relacionado__c, Rol__c
                          FROM ISSM_ATM_Mirror__c
                          WHERE Id =: regId
                          LIMIT 10000];
        return ATM_Mirror_lst;
    }

    public List<ISSM_ATM_Mirror__c> getATMMirrorByOfByUser(Set<String> OficinaVentasIds_set, Set<String> UsuariosIds_set) {
        List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
        ATM_Mirror_lst = [SELECT Id, Oficina_de_Ventas__c, Oficina_de_Ventas__r.Name, Usuario_relacionado__c
                          FROM ISSM_ATM_Mirror__c
                          WHERE Oficina_de_Ventas__c IN: OficinaVentasIds_set
                          AND Usuario_relacionado__c IN: UsuariosIds_set
                          LIMIT 10000];
        return ATM_Mirror_lst;
    }

    public List<AccountTeamMember> getATMByOfByUser(Set<String> OficinaVentasIds_set, Set<String> UsuariosIds_set) {
        List<AccountTeamMember> ATM_lst = new List<AccountTeamMember>();
        ATM_lst = [SELECT Id, AccountId, UserId, TeamMemberRole
                   FROM AccountTeamMember
                   WHERE AccountId IN: OficinaVentasIds_set
                   AND UserId IN: UsuariosIds_set
                   LIMIT 10000];
        return ATM_lst;
    }

    public List<AccountTeamMember> getATMByAccByUser(String AccountId, String UserId) { List<AccountTeamMember> ATM_lst = new List<AccountTeamMember>(); ATM_lst = [SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: AccountId AND UserId =: UserId LIMIT 10000]; return ATM_lst; }
}