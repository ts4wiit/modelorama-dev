/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description       :   Test class for class TRM_GenerateConditions_cls
*
*  No.           Date              Author                      Description
* 1.0      16-Octubre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_GenerateConditions_tst {
    public static ONTAP__Product__c[] products;
    public static Map<String,String> mapStructures;
    public static Map<String,String> mapCatalogs;
    public static Account [] lstCustomer;
    public static TRM_ConditionRelationships__mdt relation;
    public static TRM_ConditionsManagement__mdt mngRecord;
    public static TRM_ConditionClass__c clssRec;
    public static TRM_ProductScales__c[] lstScals;

     @testSetup static void loadData() {
    //Insert Budget
        TRM_Budget__c budget                = new TRM_Budget__c();
            budget.TRM_BudgetName__c        = 'budget test 01';
            budget.TRM_StartDate__c         = System.now().Date().addDays(2);
            budget.TRM_EndDate__c           = System.now().Date().addDays(6);
            budget.TRM_Status__c            = 'New';
            budget.TRM_BudgetType__c        = 'National';
            budget.TRM_RestrictedBudget__c  = true;
        insert budget;

    //Insert catalogos MDM_Parameter__c
        MDM_Parameter__c parameter1 = createMDMParameter('Cerveza',true,true,'SectorProd','01','Cerveza','SectorProd-01');
        insert parameter1;
        MDM_Parameter__c parameter2 = createMDMParameter('Media',true,true,'GrupoMateriales1','02','Media','GrupoMateriales1-02');
        insert parameter2;
        MDM_Parameter__c parameter3 = createMDMParameter('24/355 ml.',true,true,'GrupoMateriales2','42','24/355 ml.','GrupoMateriales2-42');
        insert parameter3;
        MDM_Parameter__c parameter4 = createMDMParameter('POR DEFINIR',true,true,'ConditionGroup3','99','POR DEFINIR','GpoConditions3-99');
        insert parameter4;
        MDM_Parameter__c parameter5 = createMDMParameter('Botella Abierta Imag',true,true,'Segmento','40','Botella Abierta Imag','Segmento-40');
        insert parameter5;
        MDM_Parameter__c parameter6 = createMDMParameter('ALTIPLANO',true,true,'PriceZone','01','ALTIPLANO','PriceZone-01');
        insert parameter6;

    //Insert Product
        String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);

        ONTAP__Product__c product1              = new ONTAP__Product__c();
            product1.ONTAP__ExternalKey__c      = '000000000003000006';
            product1.ONTAP__MaterialProduct__c  = 'CORONA EXTRA CLARA 24/355 ML CT R';
            product1.ISSM_SectorCode__c         = parameter1.id;
            product1.ISSM_Quota__c              = parameter2.id;
            product1.ISSM_MaterialGroup2__c     = parameter3.id;
            product1.RecordTypeId               = RecType;
            product1.ONTAP__ProductType__c      = 'FERT';
        insert product1;
    // insert conditionClass
        TRM_ConditionClass__c conditionClass            = new TRM_ConditionClass__c();
            conditionClass.TRM_StartDate__c             = System.now().Date().addDays(2);               
            conditionClass.TRM_EndDate__c               = System.now().Date().addDays(4);
            conditionClass.TRM_Budget__c                = budget.Id;
            conditionClass.TRM_ConditionClass__c        = 'ZTPM';
            conditionClass.TRM_AccessSequence__c        = '986';
            conditionClass.TRM_StatePerZone__c          = 'PriceZone-01';
            conditionClass.TRM_Status__c                = 'TRM_Pending';
            conditionClass.TRM_ConditionUnit__c         = 'MXN';
            conditionClass.TRM_ConditionGroup3__c       = parameter4.Id;
            conditionClass.TRM_Sector__c                = '00';
            conditionClass.TRM_Description__c           = 'Condition Class ZTPM 986 - class tst | clone';
            conditionClass.TRM_Segment__c               = 'Segmento-40';
            conditionClass.TRM_DistributionChannel__c   = '01';
        insert conditionClass;

        TRM_ConditionClass__c conditionClass2            = new TRM_ConditionClass__c();
            conditionClass2.TRM_StartDate__c             = System.now().Date().addDays(2);               
            conditionClass2.TRM_EndDate__c               = System.now().Date().addDays(4);
            conditionClass2.TRM_Budget__c                = budget.Id;
            conditionClass2.TRM_ConditionClass__c        = 'ZMIW';
            conditionClass2.TRM_AccessSequence__c        = '974';
            conditionClass2.TRM_StatePerZone__c          = 'PriceZone-01';
            conditionClass2.TRM_Status__c                = 'TRM_Approved';
            conditionClass2.TRM_ConditionUnit__c         = 'MXN';
            conditionClass2.TRM_Description__c           = 'Condition Class ZMIW 974 - class tst 01';
            conditionClass2.TRM_Segment__c               = 'Segmento-40';
            conditionClass2.TRM_DistributionChannel__c   = '01';
            conditionClass2.TRM_SalesOffice__c           = 'FG00';
            conditionClass2.TRM_SalesOrg__c              = '3116';
        insert conditionClass2;

    // insert conditionRecord
        TRM_ConditionRecord__c conditionRecord      = new TRM_ConditionRecord__c();
            conditionRecord.TRM_Product__c          = product1.Id;
            conditionRecord.TRM_ConditionClass__c   = conditionClass.Id;
            conditionRecord.TRM_DummyKey__c         = 'CC-0000000443R0';
            conditionRecord.TRM_UnitMeasure__c      = 'MXN';
            conditionRecord.TRM_Segment__c          = parameter5.Id;
            conditionRecord.TRM_AmountPercentage__c = 234;
            conditionRecord.TRM_StatePerZone__c     = parameter6.Id;
        insert conditionRecord;
    }
    @isTest static void testGenerateConditions() {
        Test.startTest();

            getData();
            getConditionClass('ZTPM');            
            getMetadata('ZTPM_986'); // ZTPM_986 Search By Segment-Zone                 
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);
            
            getConditionClass('ZMIW');            
            getMetadata('ZMIW_974'); // ZMIW_974 Search By Segment-Zone
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);

            getMetadata('ZPR1_903');  // ZPR1_903 Search By Zone
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);

            getMetadata('ZPV2_973');  // ZPV2_973 Search By Product   
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);
            
            getMetadata('ZPR1_922');  // ZPR1_922 Search By Organization
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);

            getMetadata('ZTPM_985');  // ZTPM_985 Search By Segment Office  
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);
            
            getMetadata('ZSP1_977');  // ZSP1_977 Search By office
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);

        // insert customer
            Account customer                        = new Account();
                customer.ISSM_SegmentCode__c        = [SELECT Id FROM MDM_Parameter__c WHERE ExternalId__c = 'Segmento-40' LIMIT 1].id;
                customer.ONTAP__SAPCustomerId__c    = 'FK160100549705';
                customer.ONTAP__SAP_Number__c       = '0100549705';
                customer.ONTAP__ExternalKey__c      = '0100549705';
                customer.ONTAP__SalesOgId__c        = '';
                customer.ONTAP__SalesOffId__c       = '';
                customer.ONTAP__ChannelId__c        = '';
                customer.Name                       = 'TEST';
            insert customer;
            lstCustomer.add(customer);
            getMetadata('ZMIW_971');  // ZMIW_971 Search By Customer
            TRM_ConditionDetail_ctr.mainSaveConditions(products,lstScals,mapStructures,mapCatalogs,lstCustomer,clssRec,relation,mngRecord);


        Test.stopTest();
    }

    @isTest static void testSearchBySegmentZone() {
        Test.startTest();

            getData();
            getConditionClass('ZMIW');            
            getMetadata('ZMIW_974');
            String[] lstSegments = new List<String>();
            String[] lstPriZones = new List<String>();
            lstPriZones.add('PriceZone-01');
            TRM_GenerateConditions_cls generateConditions = new TRM_GenerateConditions_cls();
            generateConditions.SearchBySegmentZone(products,lstSegments,lstPriZones,clssRec,relation,mngRecord,mapStructures,mapCatalogs);

        Test.stopTest();
    }
    @isTest static void testSearchBySegmentOffice() {
        Test.startTest();

            getData();
            getConditionClass('ZMIW');            
            getMetadata('ZTPM_985');
            String[] lstSegments = new List<String>();
            String[] lstSalesOff = new List<String>();
            lstSalesOff.add('FG00');
            TRM_GenerateConditions_cls generateConditions = new TRM_GenerateConditions_cls();
            generateConditions.SearchBySegmentOffice(products,lstSegments,lstSalesOff,clssRec,relation,mngRecord,mapStructures,mapCatalogs);

        Test.stopTest();
    }

    public static void getData(){
        /*Param*/       
    /* 1 */ products        =  [SELECT ONTAP__ExternalKey__c
                                                            ,ONTAP__MaterialProduct__c
                                                            ,ISSM_SectorCode__c 
                                                            ,ISSM_Quota__c  
                                                            ,ISSM_MaterialGroup2__c
                                                            ,RecordTypeId 
                                                            ,ONTAP__ProductType__c
                                                            ,ISSM_UnitPrice__c
                                                        FROM ONTAP__Product__c];
    /* 2 */ mapStructures   = TRM_EditCondition_ctr.getStructures();
    /* 3*/  mapCatalogs     = TRM_EditCondition_ctr.getCatalog();
    /* 4 */ lstCustomer     = new List<Account>();
    /* 8 */ lstScals        = [SELECT TRM_ConditionRecord__c,TRM_Quantity__c,TRM_AmountPercentage__c FROM TRM_ProductScales__c];
    }
    public static void getMetadata(String searchBy){                  
    /* 5 */ relation    = TRM_MainCreateCondition_ctr.getConditionRelationship(searchBy);
    /* 6 */ mngRecord   = TRM_MainCreateCondition_ctr.getConditionManagement(searchBy); 
    }
    public static void getConditionClass(String typeCondition){                  
            String objectType   = 'TRM_ConditionClass__c';  
            String queryString  = 'SELECT ';
                   queryString += String.join(new List<String>(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().keySet()),', ');
                   queryString += ' FROM ' + objectType;
                   queryString += ' WHERE  TRM_ConditionClass__c = \''+typeCondition+'\' LIMIT 1 ';    
    /* 7 */ clssRec   = (TRM_ConditionClass__c) Database.query(queryString);
    }

    public static MDM_Parameter__c createMDMParameter(String name
                                                    , boolean active
                                                    , boolean activeRevenue
                                                    , String catalog
                                                    , String code
                                                    , String description
                                                    , String externalId){
        MDM_Parameter__c parameter      = new MDM_Parameter__c();
            parameter.Name              = name;
            parameter.Active__c         = active;
            parameter.Active_Revenue__c = activeRevenue; 
            parameter.Catalog__c        = catalog;
            parameter.Code__c           = code;
            parameter.Description__c    = description;
            parameter.ExternalId__c     = externalId;
    return parameter;
    }
}