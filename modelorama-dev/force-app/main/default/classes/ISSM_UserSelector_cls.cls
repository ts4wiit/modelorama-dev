/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for User
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public class ISSM_UserSelector_cls extends fflib_SObjectSelector implements ISSM_IUserSelector_cls
{

	public static ISSM_IUserSelector_cls newInstance()
	{
		return (ISSM_IUserSelector_cls) ISSM_Application_cls.Selector.newInstance(User.SObjectType);
	}

	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				User.Id,
				User.ManagerId
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return User.sObjectType;
	}

	public List<User> selectById(Set<ID> idSet)
	{
		return (List<User>) selectSObjectsById(idSet);
	}

	public User selectById(ID userID){
		return (User) selectSObjectsById(userID);
	}

	public List<User> selectById(String userID){
		return (List<User>) selectSObjectsById(userID);
	}		

	/*
	 * For more examples see https://github.com/financialforcedev/fflib-apex-common-samplecode
	 * 
	public List<User> selectBySomethingElse(List<String> somethings)
	{
		assertIsAccessible();
		return (List<User>) Database.query(
				String.format(
				'select {0}, ' +
				  'from {1} ' +
				  'where Something__c in :somethings ' + 
				  'order by {2}',
				new List<String> {
					getFieldListString(),
					getSObjectName(),
					getOrderBy() } ) );
	}
	 */
}