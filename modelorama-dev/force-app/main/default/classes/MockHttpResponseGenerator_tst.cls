/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
global class MockHttpResponseGenerator_tst implements HttpCalloutMock {
	
	global String tourId;
	global String eventId;
	
	public MockHttpResponseGenerator_tst(){}
	
	public MockHttpResponseGenerator_tst(String strTId, String strEId){
		tourId = strTId;
		eventId = strEId;
	}
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        res.setBody('{"result":"ok"}');
        
        if(req.getEndpoint().contains('gettours')){
        	String strBody = '{"tours": [{"_hc_lastop": "PENDING","createddate": null,"_hc_err": null,"sfid": null,"systemmodstamp": "2017-08-28T16:17:00.000Z","issm_alternatename": null,"id": 458,"actualstart": null,"actualend": null,"distance": null,"startmile": null,"endmile": null,"salesofficeid": "FG00","salesorgid": "3116","routedescription": "PRE-FG0001-FG00-Metepec","visitplantype": "Presales","shippingstreet": "METEPEC","shippingcity": "VIALIDAD METEPEC-ZACANGO 2","rfc": "AMH080702RMA","salesagent": "carlos.bernal.alv@gmail.com.mx.issmdsd","estimateddeliverydate": "2017-08-29","vehicleid": null,"tourid": "T-000034835","routeid": "FG00FG0001","softdeleteflag": false,"isactive": true,"starttime": "01:00","endtime": "23:00","timedifference": "-6","exception": null,"percentageoffroute": 75,"lastmodifydate": null,"salesforceid": "a1N63000000SP27EAG","toursubstatus": "Tour vigente","tourstatus": "Assigned","salesofficename": null,"salesorgname": null,"postalcode": null,"shippingstate": null,"lastactivitydate": null}]}';
        	strBody = strBody.replace('a1N63000000SP27EAG', tourId);
        	
        	res.setBody(strBody);
        }
        else if(req.getEndpoint().contains('getevents')){
        	String strBody = '{"events": [{"_hc_err": null,"sfid": null,"createddate": null,"isdeleted": false,"systemmodstamp": "2017-08-28T16:20:00.000Z","_hc_lastop": "PENDING","id": 182971,"tourid": "T-000034838","numbercartons": null,"customerid": "FG000400018911","razonnovisita": null,"razonnoventa": null,"visitstatus": "Pending","visitsubestatus": "Visita no iniciada","latitude": null,"longitude": null,"controlinicio": null,"controlfin": null,"realsequence": null,"endtime": "2017-08-29T04:00:00.000Z","visitid": "a1N63000000SP2AEAW","softdeleteflag": false,"dtlastmodifieddate": null,"starttime": "2017-08-28T06:00:00.000Z","sequence": 21,"isoffroute": true,"orderdetail": null,"salesagent": "fg0008@preventa.com.issmdsd"}]}';
        	res.setBody(strBody);
        }
        else if(req.getEndpoint().contains('updateevents')){
        	String strBody = '{"events":"ok"}';
        	res.setBody(strBody);
        }
        return res;
    }
}