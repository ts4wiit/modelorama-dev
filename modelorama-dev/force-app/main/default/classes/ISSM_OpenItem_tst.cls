/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_OpenItem_tst {
	
	@testSetup
	private static void testSetup(){
		// create Accounts
		List<Account> accounts_lst = new List<Account>{	};
		for(Integer i = 0;i<10;i++){
			accounts_lst.add(new Account(Name='Test Account '+i,ISSM_MainContactA__c=true,ONTAP__SAPCustomerId__c = i<10 ? '000000000' + i : '00000000' + i));
		}
		insert accounts_lst;
	}

	@isTest
	static void testConstructor(){
		ISSM_OpenItem_cls issmOpenItems = new ISSM_OpenItem_cls();

	}


	@isTest
	static void itShould_CreateAnObject(){
		
		Map<String, Account> accouts_map = new Map<String, Account>();
		for(Account acc : getAccounts()){
			accouts_map.put(acc.ONTAP__SAPCustomerId__c, acc);
		}
		system.debug( '\n\n  ****** accouts_map = ' + accouts_map+'\n\n' );
		ISSM_OpenItem_cls issmOpenItems = new ISSM_OpenItem_cls();
		issmOpenItems.createObject(accouts_map);
		issmOpenItems.createObjectDelete(accouts_map);
		issmOpenItems.save();
		issmOpenItems.deleteObjectsFinish();
		issmOpenItems.deleteObjectsFinishExt();

		
	}
	

	private static List<Account> getAccounts(){return [SELECT Name,ONTAP__SAPCustomerId__c,Id FROM Account];}
	
	
}