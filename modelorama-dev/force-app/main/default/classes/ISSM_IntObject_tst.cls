@isTest
public class ISSM_IntObject_tst {
    @testSetup static void testdata(){
        insert new ISSM_IntObject__c(Name='DeleteIntObject', NDays__c='60;60;60', Objects__c='ONTAP__EmptyBalance__c;ONTAP__KPI__c;ONTAP__OpenItem__c', RecordType__c='012360000019pqkAAA;;012360000019pqlAAA');
        insert new ISSM_IntObject__c(Name='EmptyBalance', NDays__c='60', Objects__c='ONTAP__EmptyBalance__c', RecordType__c='');
        insert new ISSM_IntObject__c(Name='EmptyBalanceRT', NDays__c='60', Objects__c='ONTAP__EmptyBalance__c', RecordType__c='012360000019pqkAAA');
        insert new ONTAP__EmptyBalance__c(ONTAP__EmptyBalanceKey__c ='XXXX');
        insert new ONTAP__EmptyBalance__c(ONTAP__EmptyBalanceKey__c ='YYYY', RecordTypeId='012360000019pqkAAA');
    }
    private static testmethod void testIterator(){
        ISSM_IntObject_cls intObj = new ISSM_IntObject_cls('DeleteIntObject', null,null);
        intObj.Iterator();
        intObj = new ISSM_IntObject_cls('EmptyBalanceRT', 'ONTAP__EmptyBalanceKey__c ','YYYY');
        Iterator<SObject> iteratorcls = intObj.Iterator();
        if(iteratorcls.hasnext())
            iteratorcls.next();
        ISSM_IntObject_bch btchTst = new ISSM_IntObject_bch('EmptyBalance', 'ONTAP__EmptyBalanceKey__c ','XXXX');
        Database.executeBatch(btchTst);
    }
    
    private static testmethod void testBatchExecution(){
        test.startTest();
        ISSM_IntObject_sh sh1 = new ISSM_IntObject_sh();
		String sch = '0 0 23 * * ?'; 
        system.schedule('Test ISSM_IntObject_sh', sch, sh1); 
        Test.stopTest();
    }
}