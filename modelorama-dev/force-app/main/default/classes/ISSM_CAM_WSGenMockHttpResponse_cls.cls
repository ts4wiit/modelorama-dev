@IsTest
public class ISSM_CAM_WSGenMockHttpResponse_cls implements HttpCalloutMock{
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint and method.
        system.debug('WSMockHttpresponse: ' + req.getEndpoint());
        switch on req.getEndpoint(){
            when 'https://cam-refrigeradores.us-w1.cloudhub.io/asignacion'{
                //System.assertEquals('https://cam-refrigeradores.us-w1.cloudhub.io/asignacion', req.getEndpoint());
                //System.assertEquals('POST', req.getMethod());
                system.debug('DENTRO MOCK RESPONSE ORDER');
                // Create a fake response
                HttpResponse resOrder = new HttpResponse();
                resOrder.setHeader('Content-Type', 'application/json');
                resOrder.setBody('{"orderID":"12345"}');
                resOrder.setStatusCode(200);
                return resOrder;        
            }
            when 'https://cam-refrigeradores.us-w1.cloudhub.io/disponibilidad'{
                system.debug('DENTRO MOCK RESPONSE EXISTENCIAS');
                // Create a fake response
                HttpResponse resDisponibilidad = new HttpResponse();
                resDisponibilidad.setHeader('Content-Type', 'application/json');
                string strJson = '{ "materials": [ {"unit": "EA", "salesOffice": "OFF", "productId": "8000300", "availableQuantity": 100 } ]}';
                resDisponibilidad.setBody(strJson);
                if(req.getBody().contains('FG00') ) resDisponibilidad.setStatusCode(500);
                else resDisponibilidad.setStatusCode(200);
                return resDisponibilidad;
            }
            when 'https://cam-refrigeradores.us-w1.cloudhub.io/estado'{
                system.debug('DENTRO MOCK RESPONSE Estatus');
                // Create a fake response
                HttpResponse resEstado = new HttpResponse();
                resEstado.setHeader('Content-Type', 'application/json');
                string strJson ='{"tg_cam_sta": [ {"equnr": "0000NUMERODESERIE_", "stat": "COMO", "resp": "001", "message": "No se ha efectuado ninguna modificación en el equipo 1" }  ] }';
                resEstado.setBody(strJson);
                resEstado.setStatusCode(200);
                return resEstado;
            }
            when else{
                system.debug('ELSE MOCK Response');
                HttpResponse res500 = new HttpResponse();
                res500.setHeader('Content-Type', 'application/json');
                res500.setBody('{"ERROR":"error"}');
                res500.setStatusCode(500);
                return res500;               
            }
        }       
    }
}