/**
 * Test class for AllMobileApplicationOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileApplicationOperationTest {

	/**
	 * Test method to setup data.
	 */
	@testSetup
	static void setup() {

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplicationPreventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplicationPreventa;

		//Creating an Application Autoventa.
		AllMobileApplication__c objAllMobileApplicationAutoventa = AllMobileUtilityHelperTest.createAllMobileApplicationObject('AUTOVENTA', '2');
		insert objAllMobileApplicationAutoventa;

		//Creating an Application Application 4.
		AllMobileApplication__c objAllMobileApplicationApplication4 = AllMobileUtilityHelperTest.createAllMobileApplicationObject('APPLICATION4', '4');
		insert objAllMobileApplicationApplication4;
	}

	/**
	 * Test method to test Queueable.
	 */
	static testMethod void testQueueableInsertApplicationInSFAndSendToHeroku() {

		//Variables to enqueue Job.
		String strEventTriggerFlagAfterInsertApplication = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_APPLICATION;
		List<AllMobileApplication__c> lstAllMobileApplicationToInsertInHeroku = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c];
		AllMobileApplicationOperationClass objAllMobileApplicationOperationClassInsert = new AllMobileApplicationOperationClass(lstAllMobileApplicationToInsertInHeroku, strEventTriggerFlagAfterInsertApplication);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileApplicationOperationClassInsert);
		Test.stopTest();
		//End Test.
	}

	/**
	 * Test method to test UpdateApplicationInSFAndSendToHeroku method.
	 */
	static testMethod void testQueueableUpdateApplicationInSFAndSendToHeroku() {

		//Variables.
		String strEventTriggerFlagAfterUpdateApplication = AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_APPLICATION;
		List<AllMobileApplication__c> lstAllMobileApplicationToUpdate = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c];

		//Updating AllMobileApplications.
		for(AllMobileApplication__c objAllMobileApplication : lstAllMobileApplicationToUpdate) {
			objAllMobileApplication.AllMobileSoftDeleteFlag__c = true;
		}
		AllMobileApplicationOperationClass objAllMobileApplicationOperationClassUpdate = new AllMobileApplicationOperationClass(lstAllMobileApplicationToUpdate, strEventTriggerFlagAfterUpdateApplication);

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		System.enqueueJob(objAllMobileApplicationOperationClassUpdate);
		Test.stopTest();
		//End Test.
	}

	/**
	 * Test method to test SyncInsertUpdateApplicationFromHerokuRemoteAction method.
	 */
	static testMethod void testSyncInsertUpdateApplicationFromHerokuRemoteAction() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileApplicationOperationClass.syncInsertUpdateApplicationFromHeroku();
		Test.stopTest();
		//End Test.
	}

	/**
	 * Test method to test syncDeleteAllMobileApplicationInSF method.
	 */
	static testMethod void testsyncDeleteAllMobileApplicationInSF() {

		//Variables.
		List<AllMobileApplication__c> lstAllMobileApplicationInSF = [SELECT Id, Name, AllMobileApplicationId__c, AllMobileSoftDeleteFlag__c, LastModifiedDate FROM AllMobileApplication__c];

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start Test.
		Test.startTest();
		AllMobileApplicationOperationClass.syncDeleteAllMobileApplicationInSF(lstAllMobileApplicationInSF);
		Test.stopTest();
		//End Test.
	}

}