/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class with RouteXAccount__c operations

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       14-06-2017        Andrés Garrido               Creation Class
****************************************************************************************************/
public class AccountByRouteOperations_cls {

    private static VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();

 	/**
    * Method that occurs after insert a records in Account by Route Object,
      add automatically the account to a valid visit plan
    * @params:
      1.-lstNewAccByRoute: list with all new account by route records
    * @return void
    **/
    public static void afterInsertAccountByRoute(list<AccountByRoute__c> lstNewAccByRoute){
        //Map to save the frecuency by each account by route
        map<String, list<Boolean>> mapFrecuencyByAcc = new map<String, list<Boolean>>();
        set<String> setIdRoutes = new set<String>();
        set<String> setIdAccounts =  new set<String>();

        //set the map frecuency depending of the each account
        for(Integer i=0; i< lstNewAccByRoute.size(); i++){
            setIdRoutes.add(lstNewAccByRoute[i].Route__c);
            setIdAccounts.add(lstNewAccByRoute[i].Account__c);
            list<Boolean> lstFrecuency = new list<Boolean>{false, false, false, false, false, false, false};

            lstFrecuency[0] = lstNewAccByRoute[i].Monday__c;
            lstFrecuency[1] = lstNewAccByRoute[i].Tuesday__c;
            lstFrecuency[2] = lstNewAccByRoute[i].Wednesday__c;
            lstFrecuency[3] = lstNewAccByRoute[i].Thursday__c;
            lstFrecuency[4] = lstNewAccByRoute[i].Friday__c;
            lstFrecuency[5] = lstNewAccByRoute[i].Saturday__c;
            lstFrecuency[6] = lstNewAccByRoute[i].Sunday__c;
            //Map Key -> IdRoute-IdAccount
            mapFrecuencyByAcc.put(lstNewAccByRoute[i].Route__c+'-'+lstNewAccByRoute[i].Account__c, lstFrecuency);
        }

        //Call method to asociate an Account to valid visit plan
        if(!setIdRoutes.isEmpty())
            AccountByRouteOperations_cls.addAccountToVisitPlan(setIdRoutes, setIdAccounts, mapFrecuencyByAcc);
    }


    /**
    * Method that occurs after delete a records in Account by Route Object,
      remove all account of visit plan records related with the account route
    * @params:
      1.-lstOldAccByRoute: list with all old account by route records
    * @return void
    **/
    public static void afterDeleteAccountByRoute(list<AccountByRoute__c> lstOldAccByRoute){
        //Map to save the frecuency, default true
        map<String, list<Boolean>> mapFrecuencyByAcc = new map<String, list<Boolean>>();
        set<String> setIdRoutes = new set<String>();
        set<String> setIdAccounts =  new set<String>();

        for(Integer i=0; i< lstOldAccByRoute.size(); i++){
            setIdRoutes.add(lstOldAccByRoute[i].Route__c);
            setIdAccounts.add(lstOldAccByRoute[i].Account__c);
            list<Boolean> lstFrecuencyF = new list<Boolean>{true, true, true, true, true, true, true};
            mapFrecuencyByAcc.put(lstOldAccByRoute[i].Route__c+'-'+lstOldAccByRoute[i].Account__c, lstFrecuencyF);
        }

        //Call method to remove all accounts by visit plan related with the accounts of visit plan deleted
        if(!setIdRoutes.isEmpty())
            AccountByRouteOperations_cls.removeAccountsOfVisitPlan(setIdRoutes, setIdAccounts, mapFrecuencyByAcc);
    }

    /**
    * Method that occurs after update a records in Account by Route Object,
      add or remove the accounts from valid visit plan, depending if check or uncheck the frecuency in the account by route records
    * @params:
      1.-lstOldAccByRoute: list with all old account by route records
      2.-lstNewAccByRoute: list with all new account by route records
    * @return void
    **/
    public static void afterUpdateAccountByRoute(list<AccountByRoute__c> lstOldAccByRoute, list<AccountByRoute__c> lstNewAccByRoute){
        //Map to save the frecuency that changes from true to false
        map<String, list<Boolean>> mapFrecuencyByAcc = new map<String, list<Boolean>>();

        //Map to save the frecuency that changes from false to true
        map<String, list<Boolean>> mapFrecuencyByAccF = new map<String, list<Boolean>>();

        set<String> setIdRoutes = new set<String>();
        set<String> setIdAccounts =  new set<String>();
        Boolean blnProcess = false;

        set<String> setIdRoutesF = new set<String>();
        set<String> setIdAccountsF =  new set<String>();
        Boolean blnProcessF = false;

        //Process de update records
        for(Integer i=0; i< lstNewAccByRoute.size(); i++){
            list<Boolean> lstFrecuency = new list<Boolean>{false, false, false, false, false, false, false};
            list<Boolean> lstFrecuencyF = new list<Boolean>{false, false, false, false, false, false, false};

            //Validate the records that change the frecuency per day (true-->false)
            if(lstOldAccByRoute[i].Monday__c == true && lstNewAccByRoute[i].Monday__c == false)
                lstFrecuency[0] = true;
            if(lstOldAccByRoute[i].Tuesday__c == true && lstNewAccByRoute[i].Tuesday__c == false)
                lstFrecuency[1] = true;
            if(lstOldAccByRoute[i].Wednesday__c == true && lstNewAccByRoute[i].Wednesday__c == false)
                lstFrecuency[2] = true;
            if(lstOldAccByRoute[i].Thursday__c == true && lstNewAccByRoute[i].Thursday__c == false)
                lstFrecuency[3] = true;
            if(lstOldAccByRoute[i].Friday__c == true && lstNewAccByRoute[i].Friday__c == false)
                lstFrecuency[4] = true;
            if(lstOldAccByRoute[i].Saturday__c == true && lstNewAccByRoute[i].Saturday__c == false)
                lstFrecuency[5] = true;
            if(lstOldAccByRoute[i].Sunday__c == true && lstNewAccByRoute[i].Sunday__c == false)
                lstFrecuency[6] = true;

            if(lstFrecuency[0] || lstFrecuency[1] || lstFrecuency[2] || lstFrecuency[3] || lstFrecuency[4] || lstFrecuency[5] || lstFrecuency[6]){
                setIdRoutes.add(lstNewAccByRoute[i].Route__c);
                setIdAccounts.add(lstNewAccByRoute[i].Account__c);
                mapFrecuencyByAcc.put(lstNewAccByRoute[i].Route__c + '-' + lstNewAccByRoute[i].Account__c, lstFrecuency);
                blnProcess = true;
            }

            //Validate the records that change the frecuency per day (false-->true)
            if(lstOldAccByRoute[i].Monday__c == false && lstNewAccByRoute[i].Monday__c == true)
                lstFrecuencyF[0] = true;
            if(lstOldAccByRoute[i].Tuesday__c == false && lstNewAccByRoute[i].Tuesday__c == true)
                lstFrecuencyF[1] = true;
            if(lstOldAccByRoute[i].Wednesday__c == false && lstNewAccByRoute[i].Wednesday__c == true)
                lstFrecuencyF[2] = true;
            if(lstOldAccByRoute[i].Thursday__c == false && lstNewAccByRoute[i].Thursday__c == true)
                lstFrecuencyF[3] = true;
            if(lstOldAccByRoute[i].Friday__c == false && lstNewAccByRoute[i].Friday__c == true)
                lstFrecuencyF[4] = true;
            if(lstOldAccByRoute[i].Saturday__c == false && lstNewAccByRoute[i].Saturday__c == true)
                lstFrecuencyF[5] = true;
            if(lstOldAccByRoute[i].Sunday__c == false && lstNewAccByRoute[i].Sunday__c == true)
                lstFrecuencyF[6] = true;

            if(lstFrecuencyF[0] || lstFrecuencyF[1] || lstFrecuencyF[2] || lstFrecuencyF[3] || lstFrecuencyF[4] || lstFrecuencyF[5] || lstFrecuencyF[6]){
                setIdRoutesF.add(lstNewAccByRoute[i].Route__c);
                setIdAccountsF.add(lstNewAccByRoute[i].Account__c);
                mapFrecuencyByAccF.put(lstNewAccByRoute[i].Route__c + '-' + lstNewAccByRoute[i].Account__c, lstFrecuencyF);
                blnProcessF = true;
            }
        }

        System.debug('\n##setIdRoutes: '+setIdRoutes);
        System.debug('\n##setIdAccounts: '+setIdAccounts);
        System.debug('\n##mapFrecuencyByAcc: '+mapFrecuencyByAcc);
        System.debug('\n##setIdRoutesF: '+setIdRoutesF);
        System.debug('\n##setIdAccountsF: '+setIdAccountsF);
        System.debug('\n##mapFrecuencyByAccF: '+mapFrecuencyByAccF);

        //Call process to remove account by visit plan record related with routes an accounts, occurs when update the frecuncy from true to false
        if(blnProcess)
            AccountByRouteOperations_cls.removeAccountsOfVisitPlan(setIdRoutes, setIdAccounts, mapFrecuencyByAcc);

        //Call process to add account by visit plan record related with routes an accounts, occurs when update the frecuncy from false to true
        if(blnProcessF)
            AccountByRouteOperations_cls.addAccountToVisitPlan(setIdRoutesF, setIdAccountsF, mapFrecuencyByAccF);
    }

    /**
    * Method to remove records in Account By Visit Plan when delete or uncheck the frecuency in Account By route records
    * @params:
      1.-setIdRoutes: set with route Ids
      2.-setIdAccounts: set with account Ids
      3.-mapFrecuency: the map key corresponds a string with routeId and accountId, the value is a list with your frecuency
    * @return void
    **/
    public static void removeAccountsOfVisitPlan(set<String> setIdRoutes, set<String> setIdAccounts, map<String, list<Boolean>> mapFrecuency){
        //Obtain all accounts by visit plan that are related with the routes and are in a valid visit plan
        list<AccountByVisitPlan__c> lstAccByVP = [
            Select  Id, Account__c, VisitPlan__c, VisitPlan__r.Route__c, VisitPlan__r.Monday__c,
                    VisitPlan__r.Tuesday__c, VisitPlan__r.Wednesday__c, VisitPlan__r.Thursday__c,
                    VisitPlan__r.Friday__c, VisitPlan__r.Saturday__c, VisitPlan__r.Sunday__c,
                    VisitPlan__r.ExecutionDate__c, VisitPlan__r.EffectiveDate__c
            From    AccountByVisitPlan__c
            Where   Account__c = :setIdAccounts And VisitPlan__r.Route__c = :setIdRoutes And
            		VisitPlan__r.ExecutionDate__c <= TODAY And VisitPlan__r.EffectiveDate__c >= TODAY
        ];

        //Set to save all visit plan Ids
        set<String> setIdsVisitPlan = new set<String>();
        //Set to save all account Ids
        set<String> setIdsAcc = new set<String>();

        //list to save the account by visit plan records that will delete
        list<AccountByVisitPlan__c> lstAccByVPToDel = new list<AccountByVisitPlan__c>();

        for(AccountByVisitPlan__c accByVP :lstAccByVP){
        	//Validate if the route and account related in the record have a frecuency list
            String strKey = accByVP.VisitPlan__r.Route__c + '-' + accByVP.Account__c;
            if(mapFrecuency.containsKey(strKey)){
                list<Boolean> lstFrec = mapFrecuency.get(strKey);
                //validate that the frecuency match
                if(
                    lstFrec[0] && accByVP.VisitPlan__r.Monday__c ||
                    lstFrec[1] && accByVP.VisitPlan__r.Tuesday__c ||
                    lstFrec[2] && accByVP.VisitPlan__r.Wednesday__c ||
                    lstFrec[3] && accByVP.VisitPlan__r.Thursday__c ||
                    lstFrec[4] && accByVP.VisitPlan__r.Friday__c ||
                    lstFrec[5] && accByVP.VisitPlan__r.Saturday__c ||
                    lstFrec[6] && accByVP.VisitPlan__r.Sunday__c
                ){
                	//add the record to delete
                    setIdsVisitPlan.add(accByVP.VisitPlan__c);
                    lstAccByVPToDel.add(accByVP);
                    setIdsAcc.add(accByVP.Account__c);
                }
            }
        }

        //Delete the records
        if(!lstAccByVPToDel.isEmpty())
            delete lstAccByVPToDel;

        //Delete all events related with accounts and visit plans deleted
        System.debug('\nsetIdsVisitPlan=> '+setIdsVisitPlan);
        System.debug('\n=>setIdsAcc '+setIdsAcc);
        if(!setIdsVisitPlan.isEmpty() && !setIdsAcc.isEmpty())
            AccountByRouteOperations_cls.removeVisitsOfVisitPlan(setIdsAcc, setIdsVisitPlan);
    }

    /**
    * Method to remove events related with the accounts and visit plans, only delete events in initial status (Created)
    * @params:
      1.-setIdsAccounts: set with account Ids
      2.-setIdsVisitPlan: set with visit plan Ids
    * @return void
    **/
    public static void removeVisitsOfVisitPlan(set<String> setIdsAccounts, set<String> setIdsVisitPlan){
        list<Event> lstEvents = [
            Select  Id, VisitList__c, VisitList__r.VisitPlan__c, WhatId, ONTAP__Estado_de_visita__c, VisitList__r.ONTAP__TourStatus__c
            From    Event
            Where   VisitList__r.VisitPlan__c = :setIdsVisitPlan
                And WhatId = :setIdsAccounts
                And VisitList__r.ONTAP__TourStatus__c = : Label.VisitInitialStatus
        ];

        System.debug('\nVisits to del=> '+lstEvents);
        if(!lstEvents.isEmpty()){
            //delete lstEvents;
            Database.DeleteResult[] lstDeleteResults = Database.delete(lstEvents, false);
        }
    }

    /**
    * Method to add an account in a valid visit plan in the same route, when an account is checked in account by route records
    * @params:
      1.-setIdRoutes: set with routes ids
      2.-setIdAccounts: set with accounts ids
      3.-mapFrecuency: map with frecuency in each route account record
    * @return void
    **/
    public static void addAccountToVisitPlan(set<String> setIdRoutes, set<String> setIdAccounts, map<String, list<Boolean>> mapFrecuency){
        //Obtain all valid visit plans related with the routes
        list<VisitPlan__c> lstVisitPlan = [
            Select  Id, Route__c, Monday__c, Tuesday__c, Wednesday__c, ExecutionDate__c,
                    Thursday__c, Friday__c, Saturday__c, Sunday__c, RelatedAccounts__c, EffectiveDate__c,
                     (Select Id, Account__c From AccountsByVisitPlan__r Where Account__c = :setIdAccounts)
            From    VisitPlan__c
            Where   Route__c = :setIdRoutes And
            		ExecutionDate__c <= TODAY And EffectiveDate__c >= TODAY
        ];

        System.debug('\nList Of VP ==> '+lstVisitPlan);

        //Obtain the existed account by visit plan records related with the accounts
        list<AccountByVisitPlan__c> lstExistRecord = [
        	Select 	Id, Account__c, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c,
        			Sunday__c, VisitPlan__c, VisitPlan__r.Route__c, VisitPlan__r.ExecutionDate__c, VisitPlan__r.EffectiveDate__c
        	From 	AccountByVisitPlan__c
        	Where 	Account__c = :setIdAccounts And VisitPlan__r.Route__c = :setIdRoutes And
        			(VisitPlan__r.ExecutionDate__c <= TODAY And VisitPlan__r.EffectiveDate__c >= TODAY)
        ];

        map<String, AccountByVisitPlan__c> mapExistAccount = new map<String, AccountByVisitPlan__c>();
        for(AccountByVisitPlan__c objExistAcc :lstExistRecord){
        	String strKey = objExistAcc.VisitPlan__r.Route__c + '-' + objExistAcc.VisitPlan__c + '-' + objExistAcc.Account__c;
        	mapExistAccount.put(strKey, objExistAcc);
        }

        System.debug('\nMap Exist Account ==> '+mapExistAccount);

        list<AccountByVisitPlan__c> lstObjAccByVP = new list<AccountByVisitPlan__c>();
        
        map<String, Integer> mapCont = new map<String, Integer>();
        for(VisitPlan__c objVP : lstVisitPlan)
        	mapCont.put(objVP.Id, 0);
       

        for(String idAcc :setIdAccounts){
            for(VisitPlan__c objVP : lstVisitPlan){
                String strKey = objVP.Route__c + '-' + idAcc;
                if(mapFrecuency.containsKey(strKey)){
                    list<Boolean> lstFrec = mapFrecuency.get(strKey);
                    if(
                        lstFrec[0] && objVP.Monday__c ||
                        lstFrec[1] && objVP.Tuesday__c ||
                        lstFrec[2] && objVP.Wednesday__c ||
                        lstFrec[3] && objVP.Thursday__c ||
                        lstFrec[4] && objVP.Friday__c ||
                        lstFrec[5] && objVP.Saturday__c ||
                        lstFrec[6] && objVP.Sunday__c
                    ){
                    	String strKey1 = objVP.Route__c + '-' + objVP.Id + '-' + idAcc;
                    	AccountByVisitPlan__c objAccByVP;

                    	if(mapExistAccount.containsKey(strKey1)){
                    		objAccByVP = mapExistAccount.get(strKey1);
                    		objAccByVP.Monday__c = lstFrec[0] && objVP.Monday__c || objAccByVP.Monday__c;
	                        objAccByVP.Tuesday__c = lstFrec[1] && objVP.Tuesday__c || objAccByVP.Tuesday__c;
	                        objAccByVP.Wednesday__c = lstFrec[2] && objVP.Wednesday__c || objAccByVP.Wednesday__c;
	                        objAccByVP.Thursday__c = lstFrec[3] && objVP.Thursday__c || objAccByVP.Thursday__c;
	                        objAccByVP.Friday__c = lstFrec[4] && objVP.Friday__c || objAccByVP.Friday__c;
	                        objAccByVP.Saturday__c = lstFrec[5] && objVP.Saturday__c || objAccByVP.Saturday__c;
	                        objAccByVP.Sunday__c = lstFrec[6] && objVP.Sunday__c || objAccByVP.Sunday__c;
                    	}
                    	else{
	                        objAccByVP = new AccountByVisitPlan__c();
	                        objAccByVP.Account__c = idAcc;
	                        objAccByVP.Monday__c = lstFrec[0] && objVP.Monday__c;
	                        objAccByVP.Tuesday__c = lstFrec[1] && objVP.Tuesday__c;
	                        objAccByVP.Wednesday__c = lstFrec[2] && objVP.Wednesday__c;
	                        objAccByVP.Thursday__c = lstFrec[3] && objVP.Thursday__c;
	                        objAccByVP.Friday__c = lstFrec[4] && objVP.Friday__c;
	                        objAccByVP.Saturday__c = lstFrec[5] && objVP.Saturday__c;
	                        objAccByVP.Sunday__c = lstFrec[6] && objVP.Sunday__c;
	                        objAccByVP.Sequence__c = objVP.RelatedAccounts__c+1+mapCont.get(objVp.Id);
	                        objAccByVP.VisitPlan__c = objVP.Id;
	                        objAccByVP.WeeklyPeriod__c = '1';
	                        mapCont.put(objVp.Id,mapCont.get(objVp.Id)+1);
                    	}
                        lstObjAccByVP.add(objAccByVP);
                    }
                }
            }
        }

        if(!lstObjAccByVP.isEmpty()){
        	list<Database.UpsertResult> lstUR = Database.upsert(lstObjAccByVP, false);
            for(Database.UpsertResult objUR : lstUR){
                if(!objUR.isSuccess())
                	System.debug('Error==>'+objUR.getErrors()[0].getMessage());
            }
        }
    }

    /**
    * Method to validate if an account can coexist or can has the same frecuency in differents routes and
      service models depending of the config in the custom metadata type
    * @params:
      1.-lstNewAccByRoute: list of the new account by route
    * @return void
    **/
    public static void validateServiceModelConfigUpdate(list<AccountByRoute__c> lstOldAccByRoute, list<AccountByRoute__c> lstNewAccByRoute){
    	list<AccountByRoute__c> lstAccByRoute = new list<AccountByRoute__c>();

    	for(Integer i=0; i<lstNewAccByRoute.size(); i++){
	    	if(
	    		lstNewAccByRoute[i].Monday__c != lstOldAccByRoute[i].Monday__c ||
	    		lstNewAccByRoute[i].Tuesday__c != lstOldAccByRoute[i].Tuesday__c ||
	    		lstNewAccByRoute[i].Wednesday__c != lstOldAccByRoute[i].Wednesday__c ||
	    		lstNewAccByRoute[i].Thursday__c != lstOldAccByRoute[i].Thursday__c ||
	    		lstNewAccByRoute[i].Friday__c != lstOldAccByRoute[i].Friday__c ||
	    		lstNewAccByRoute[i].Saturday__c != lstOldAccByRoute[i].Saturday__c ||
	    		lstNewAccByRoute[i].Sunday__c != lstOldAccByRoute[i].Sunday__c
	    	){
	    		lstAccByRoute.add(lstNewAccByRoute[i]);
	    	}
    	}

    	AccountByRouteOperations_cls.validateServiceModelConfig(lstNewAccByRoute);
    }

    /**
    * Method to validate if an account can coexist or can has the same frecuency in differents routes and
      service models depending of the config in the custom metadata type
    * @params:
      1.-lstNewAccByRoute: list of the new account by route
    * @return void
    **/
    public static void validateServiceModelConfig(list<AccountByRoute__c> lstNewAccByRoute){
        set<String> setRoutes = new set<String>();
        set<String> setAccounts = new set<String>();

        //Obtain the ids for the Routes and Accounts
        for(AccountByRoute__c objAccByRoute :lstNewAccByRoute){
            setRoutes.add(objAccByRoute.Route__c);
            setAccounts.add(objAccByRoute.Account__c);
        }

        //Obtain a Route Map
        map<String, ONTAP__Route__c> mapRoute = new map<String, ONTAP__Route__c>([
            Select  Id, ServiceModel__c
            From    ONTAP__Route__c
            Where   Id = :setRoutes
        ]);

        //Obtain the metadata type config for all service models
        list<ParametersVisitControl__mdt> lstConf = [
            Select  Id, CanCoexist__c, ServiceModel__c, ServiceModel2__c, SameFrequency__c
            From    ParametersVisitControl__mdt
        ];

        System.debug('\n----lstConf: '+lstConf);
        if(!lstConf.isEmpty()){

            list<AccountByRoute__c> lstTmpAccByRoute = [
                Select  Id, Account__c, Route__c, Route__r.ServiceModel__c, Monday__c,
                        Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c
                From    AccountByRoute__c
                Where   Account__c = :setAccounts
            ];
            System.debug('\n----lstTmpAccByRoute: '+lstTmpAccByRoute);

            //Map to save all AccountByRoute__c records by each account
            map<String, list<AccountByRoute__c>> mapAccService = new map<String, list<AccountByRoute__c>>();
            for(AccountByRoute__c tmpAccByRoute : lstTmpAccByRoute){
                list<AccountByRoute__c> lstTmp;
                if(mapAccService.containsKey(tmpAccByRoute.Account__c))
                    lstTmp = mapAccService.get(tmpAccByRoute.Account__c);
                else
                    lstTmp = new list<AccountByRoute__c>();

                lstTmp.add(tmpAccByRoute);
                mapAccService.put(tmpAccByRoute.Account__c, lstTmp);
            }

            //for each new AccountByRoute__c record
            for(AccountByRoute__c objAccByRoute :lstNewAccByRoute){
                //validate if the related account have other route with other service model
                if(mapAccService.containsKey(objAccByRoute.Account__c)){
                    //Obtain the related records
                    list<AccountByRoute__c> tmpLstObjAccByRoute = mapAccService.get(objAccByRoute.Account__c);
                    System.debug('\n----tmpLstObjAccByRoute: '+tmpLstObjAccByRoute);
                    //for each record, validate if exist a metadatatype with conditions that allow create a record
                    //maximun for records are processed(one for each service model)
                    for(AccountByRoute__c tmpObjAccByRoute : tmpLstObjAccByRoute){
                        String serviceModel = tmpObjAccByRoute.Route__r.ServiceModel__c;
                        String serviceModel2 = mapRoute.get(objAccByRoute.Route__c).ServiceModel__c;
                        System.debug('\n----serviceModel: '+serviceModel);
                        System.debug('\n----serviceModel2: '+serviceModel2);
                        Boolean exito = true;
                        //for each metadatatype, validate that the service models can not coexist or not have the same frecuency
                        //maximun six records are processed
                        for(ParametersVisitControl__mdt conf :lstConf){
                        	System.debug('\n----conf: '+conf);
                        	//if the service models can not coexist or can not have the same frecuency, the process generate an error
                            if(
                                (
                                	//The service models should be in the metadata types config
                                    (conf.ServiceModel__c == serviceModel && conf.ServiceModel2__c == serviceModel2) ||
                                    (conf.ServiceModel__c == serviceModel2 && conf.ServiceModel2__c == serviceModel)
                                ) &&
                                (
                                	//Can not have coexist
                                    conf.CanCoexist__c == false ||
                                    (
                                    	//Can not have the same frecuency
                                        conf.SameFrequency__c == false &&
                                        (
                                            (objAccByRoute.Monday__c==true && tmpObjAccByRoute.Monday__c==true) ||
                                            (objAccByRoute.Tuesday__c==true && tmpObjAccByRoute.Tuesday__c==true) ||
                                            (objAccByRoute.Wednesday__c==true && tmpObjAccByRoute.Wednesday__c==true) ||
                                            (objAccByRoute.Thursday__c==true && tmpObjAccByRoute.Thursday__c==true) ||
                                            (objAccByRoute.Friday__c==true && tmpObjAccByRoute.Friday__c==true) ||
                                            (objAccByRoute.Saturday__c==true && tmpObjAccByRoute.Saturday__c==true) ||
                                            (objAccByRoute.Sunday__c==true && tmpObjAccByRoute.Sunday__c==true)
                                        )
                                    )

                                )
                            ){
                                objAccByRoute.addError(Label.VPCoexistFrecuencyError.replace('%s1', serviceModel).replace('%s2', serviceModel2));
                                exito = false;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

}