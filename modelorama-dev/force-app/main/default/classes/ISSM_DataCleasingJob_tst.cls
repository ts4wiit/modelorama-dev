/**************************************************************************************
Name apex class: ISSM_DataCleasingJob_tst
Version: 1.0
Createdate: 29 June 2018
Functionality: Test class to process the functionality of the apex class ISSM_DataCleasingJob_bch
Modifications history:
-----------------------------------------------------------------------------
* Developer            -       Date       -        Description
* ----------------------------------------------------------------------------
* Carlos Duque         26 - September - 2017      Original version
* Leopoldo Ortega        29 - June - 2018         Change way to execute batch with parameters from the new custom settings
*************************************************************************************/
@isTest
public class ISSM_DataCleasingJob_tst {
    static Account ObjAccount { get; set; }
    static ONCALL__Call__c ObjCall { get; set; }
    static Account ObjAccOrg { get; set; }
    
    public static ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    static ONTAP__Order__c ObjOrder1 { get; set; }
    static ONTAP__Order__c ObjOrder2 { get; set; }
    
    public static List<ONTAP__EmptyBalance__c> emptyBalance_lst = new List<ONTAP__EmptyBalance__c>();
    public static List<ONTAP__EmptyBalance__c> foundEmptyBalance_lst = new List<ONTAP__EmptyBalance__c>();
    public static List<ONTAP__Order__c> order_lst = new List<ONTAP__Order__c>();
    public static List<ONTAP__Order__c> foundOrder_lst = new List<ONTAP__Order__c>();
    
    static testMethod void testdata() {
        // Get record type of empty balance
        String RTDevNameEmptyBalance = 'ONTAP_Compact';
        String RTEmptyBalance = String.ValueOf(ISSM_CreateCaseGlobal_cls.getRecordType(RTDevNameEmptyBalance));
        
        // Get record type of order
        String RTDevNameOrder = 'OnCall_Order';
        String RTOrder = String.ValueOf(ISSM_CreateCaseGlobal_cls.getRecordType(RTDevNameOrder));
        
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataIsDelete', Order__c = 1, IsActive__c = true, Type__c = 'IsDeleted', NDays__c=0, Objects__c='ONTAP__EmptyBalance__c', RecordType__c = RTEmptyBalance, Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = true);
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataIsDelete', Order__c = 2, IsActive__c = true, Type__c = 'IsDeleted', NDays__c=0, Objects__c='ONTAP__EmptyBalance__c', RecordType__c = '', Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = false);
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataIsDelete', Order__c = 3, IsActive__c = true, Type__c = 'IsDeleted', NDays__c=0, Objects__c='ONTAP__EmptyBalance__c', RecordType__c = '', Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = true, Batch_size__c = 200);
        
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataDays', Order__c = 4, IsActive__c = true, Type__c = 'Days', NDays__c=60, Objects__c='ONTAP__Order__c', RecordType__c = RTOrder, Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = true);
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataDays', Order__c = 5, IsActive__c = true, Type__c = 'Days', NDays__c=30, Objects__c='ONTAP__Order__c', RecordType__c = '', Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = false);
        insert new ISSM_DataCleasingJob__c(Name='DeleteDataDays', Order__c = 6, IsActive__c = true, Type__c = 'Days', NDays__c=0, Objects__c='ONTAP__KPI__c', RecordType__c = '', Email__c = 'test@salesforce.com', Delete_to_Recycle_Bin__c = true, Batch_size__c = 15000);
        
        ONTAP__EmptyBalance__c eb1 = new ONTAP__EmptyBalance__c();
        eb1.ONTAP__EmptyBalanceKey__c = 'XXXX';
        eb1.ISSM_IsDeleted__c = true;
        emptyBalance_lst.add(eb1);
        
        ONTAP__EmptyBalance__c eb2 = new ONTAP__EmptyBalance__c();
        eb2.ONTAP__EmptyBalanceKey__c = 'YYYY';
        eb2.ISSM_IsDeleted__c = false;
        emptyBalance_lst.add(eb2);
        
        // Add to list the empty balance's records to assertions
        insert emptyBalance_lst;
        
        // Verify that record of empty balance has been inserted
        System.assertEquals(emptyBalance_lst.size(), 2);
        
        // Insert the account
        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');
        ObjAccount = new Account(Name = 'TST', ONTAP__Main_Phone__c = '5510988765', ISSM_MainContactE__c = true, ONTAP__SAPCustomerId__c = '100153987', ONTAP__SalesOgId__c = '3116', ONTAP__ChannelId__c = '1');
        insert ObjAccount;
        
        // Insert the sales organization
        ObjAccOrg = new Account(Name = 'Cuenta Org', ONTAP__Main_Phone__c = '12345678', ONTAP__SalesOgId__c = '3116', RecordTypeId = CTRSOQL.getRecordTypeId('Account','SalesOrg'));
        insert ObjAccOrg;
        
        // Insert the call
        ObjCall = new ONCALL__Call__c(Name = 'TST', ONCALL__POC__c = ObjAccount.Id, RecordTypeId = RecTypeId, ISSM_ValidateOrder__c = false);
        insert ObjCall;
        
        // Insert the order 1
        ObjOrder1 = new ONTAP__Order__c(ONCALL__OnCall_Account__c = ObjAccount.Id, ONCALL__Call__c = ObjCall.Id, ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2, ONCALL__Origin__c = Label.ISSM_OriginCall, ONTAP__DeliveryDate__c = System.today()+1, ONTAP__BeginDate__c = System.today());
        /*Date date_dt = date.today();
        Test.setCreatedDate(ObjOrder1.Id, date_dt.addDays(-61));*/
        order_lst.add(ObjOrder1);
        
        // Insert the order 2
        ObjOrder2 = new ONTAP__Order__c(ONCALL__OnCall_Account__c = ObjAccount.Id, ONCALL__Call__c = ObjCall.Id, ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2, ONCALL__Origin__c = Label.ISSM_OriginCall, ONTAP__DeliveryDate__c = System.today()+1, ONTAP__BeginDate__c = System.today());
        order_lst.add(ObjOrder2);
        
        // Add to list the order's records to assertions
        insert order_lst;
        
        // Verify that record of orders has been inserted
        System.assertEquals(order_lst.size(), 2);
    }
    
    private static testmethod void testBatchExecution() {
        // Call method to create data test
        ISSM_DataCleasingJob_tst.testdata();
        
        test.startTest();
        
        Id batchJobId1 = Database.executeBatch(new ISSM_DataCleasingJob_bch('DeleteDataIsDelete',1), 200);
		Id batchJobId2 = Database.executeBatch(new ISSM_DataCleasingJob_bch('DeleteDataDays',1), 200);
        
        // Execute scheduler to process batch
        ISSM_DataCleasingJob_sch sh1 = new ISSM_DataCleasingJob_sch();
		String sch = '0 0 1 * * ?';
        system.schedule('Test ISSM_DataCleasingJob_sch', sch, sh1);
        
        // Call method to sendEmail with error
        ISSM_DataCleasingJob_util.sendEmail('test@salesforce.com', 'process', 'error');
        
        // Call method to parse array to convert to string
        String[] arrayRecordType = new String[]{'123','456','789'};
            ISSM_DataCleasingJob_util.parseArrayRecordType(arrayRecordType);
        
        // Verify that record of empty balance has been deleted
        foundEmptyBalance_lst = [SELECT Id FROM ONTAP__EmptyBalance__c];
        // System.assertEquals(foundEmptyBalance_lst.size(), 1);
        
        // Verify that record of orders has been deleted
        foundOrder_lst = [SELECT Id FROM ONTAP__Order__c];
        // System.assertEquals(foundOrder_lst.size(), 1);
        
        Test.stopTest();
    }
}