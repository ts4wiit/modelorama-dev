@isTest
private class ISSM_CreateCaseForceInCaseHandler_tst {

    @testSetup static void setupTestData() {

        //Create Customs Settings MappingFieldCase
        ISSM_MappingFieldCase__c description_custSett = new ISSM_MappingFieldCase__c();
        description_custSett.Name = 'Description';
        description_custSett.ISSM_APICaseForce__c = 'ONTAP__Description__c';
        description_custSett.Active__c = true;
        insert description_custSett;

        ISSM_MappingFieldCase__c caseNumber_custSett = new ISSM_MappingFieldCase__c();
        caseNumber_custSett.Name = 'ISSM_CaseForceNumber__c';
        caseNumber_custSett.ISSM_APICaseForce__c = 'Id';
        caseNumber_custSett.Active__c = true;
        insert caseNumber_custSett;

        ISSM_MappingFieldCase__c caseStatus_custSett = new ISSM_MappingFieldCase__c();
        caseStatus_custSett.Name = 'status';
        caseStatus_custSett.ISSM_APICaseForce__c = 'ONTAP__Status__c';
        caseStatus_custSett.Active__c = true;
        insert caseStatus_custSett;
        
        ISSM_TypificationMatrix__c TypMat1 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM1', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo', ISSM_AssignedTo__c = 'SUPERVISOR', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat1;
        
        ISSM_TypificationMatrix__c TypMat2 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM2', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Retiro de equipo', ISSM_AssignedTo__c = 'MODELORAMA', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat2;
        
        ISSM_TypificationMatrix__c TypMat3 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM3', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo', ISSM_AssignedTo__c = 'BILLINGMANAGER', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat3;
        
        ISSM_TypificationMatrix__c TypMat4 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM4', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo', ISSM_AssignedTo__c = 'REPARTO', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat4;
        
        ISSM_TypificationMatrix__c TypMat5 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM5', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo', ISSM_AssignedTo__c = 'BOSSREFRIGERATION', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat5;
        
        ISSM_TypificationMatrix__c TypMat6 = new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM6', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo', ISSM_AssignedTo__c = 'TRADEMARKETING', ISSM_Channel__c = 'OnTap/OnCall');        
		insert TypMat6;

    }
    
    @isTest static void testCreateCaseForceInCase() {
        // Create Case Force records
        List <ONTAP__Case_Force__c> caseForce_list = new List <ONTAP__Case_Force__c>();
        List <ONTAP__Case_Force__c> caseForceUpd_list = new List <ONTAP__Case_Force__c>();
        ONTAP__Case_Force__c objInsertCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Description','Asunto','Open');
        
        Account acc = new Account();
        acc.Name = 'Acc 1';
        insert acc;

        ONTAP__Case_Force__c case1 = new ONTAP__Case_Force__c();
        case1.Id =  objInsertCaseForce.Id;
        case1.ONTAP__Status__c = 'Open';
        case1.ONTAP__Description__c = 'Description';
        case1.ONTAP__Subject__c = 'subjectEdit';
        caseForce_list.add(case1);

        Test.startTest();
        
        ISSM_CreateCaseForceInCaseHandler_cls handler = new ISSM_CreateCaseForceInCaseHandler_cls();
        handler.CreateCaseForceInCase(caseForce_list);
        
        case1.ONTAP__Description__c = 'Description2';
        caseForceUpd_list.add(case1);
        update caseForceUpd_list;
        
        handler.UodateToJSon(caseForceUpd_list, caseForce_list);
        
        Test.stopTest();
    }
    
    @isTest static void testCreateATM() {
        Account accAtm = new Account();
        accAtm.Name = 'Acc ATM 1';
        insert accAtm;
        
        // Insertamos un usuario
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@test1test.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@test1test.com');
        insert user1;
        
        Test.startTest();
        // Insertamos una cuenta dentro de un equipo de cuentas
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = accAtm.Id;
        atm.UserId = user1.Id;
        atm.TeamMemberRole = 'Supervisor';
        insert atm;
        Test.stopTest();
    }
    
    @isTest static void testCreateCaseForce() {
        Id caseForceRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_SalesForceCase'].Id;
        Test.startTest();
        ONTAP__Case_Force__c caseforce = new ONTAP__Case_Force__c();
        caseForce.RecordTypeId = caseForceRecordTypeId;
        caseForce.ONTAP__Description__c = 'strDescription';
        caseForce.ONTAP__Subject__c = 'strSubject';
        caseForce.ONTAP__Status__c = 'strStatus';
        insert caseForce;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification1() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        Id queue = [SELECT Id FROM Group WHERE Type = 'Queue' LIMIT 1].Id;
        ISSM_TypificationMatrix__c objTypMat2 = new ISSM_TypificationMatrix__c();
        objTypMat2.ISSM_UniqueIdentifier__c = 'Test2';
        objTypMat2.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat2.ISSM_AssignedTo__c = 'QUEUE';
        objTypMat2.ISSM_OwnerQueue__c = 'ISSM_WithoutOwner';
        objTypMat2.ISSM_IdQueue__c = queue;
        insert objTypMat2;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification2() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat3 = new ISSM_TypificationMatrix__c();
        objTypMat3.ISSM_UniqueIdentifier__c = 'Test3';
        objTypMat3.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat3.ISSM_AssignedTo__c = 'Supervisor';
        insert objTypMat3;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification3() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat4 = new ISSM_TypificationMatrix__c();
        objTypMat4.ISSM_UniqueIdentifier__c = 'Test4';
        objTypMat4.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat4.ISSM_AssignedTo__c = 'Billing Manager';
        insert objTypMat4;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification4() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat5 = new ISSM_TypificationMatrix__c();
        objTypMat5.ISSM_UniqueIdentifier__c = 'Test5';
        objTypMat5.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat5.ISSM_AssignedTo__c = 'Trade Marketing';
        insert objTypMat5;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification5() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat6 = new ISSM_TypificationMatrix__c();
        objTypMat6.ISSM_UniqueIdentifier__c = 'Test6';
        objTypMat6.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat6.ISSM_AssignedTo__c = 'Modelorama';
        insert objTypMat6;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification6() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat7 = new ISSM_TypificationMatrix__c();
        objTypMat7.ISSM_UniqueIdentifier__c = 'Test7';
        objTypMat7.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat7.ISSM_AssignedTo__c = 'Delivery';
        insert objTypMat7;
        Test.stopTest();
    }
    
    @isTest static void testCreateTypification7() {
        Test.startTest();
        // Insertamos la matriz de tipificación
        ISSM_TypificationMatrix__c objTypMat8 = new ISSM_TypificationMatrix__c();
        objTypMat8.ISSM_UniqueIdentifier__c = 'Test8';
        objTypMat8.ISSM_Channel__c = 'OnTap/OnCall';
        objTypMat8.ISSM_AssignedTo__c = 'Boss Refrigeration';
        insert objTypMat8;
        Test.stopTest();
    }
    
}