/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
	Class that allows to deserialize the generated JSON of the service Create Order

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       25/07/2017	  Luis Licona                  CLass created
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_DeserializeCreateOrder_tst {
	
	static testMethod void testParse() {
		String json = '{'+
		'  \"createOrder\" : {'+
		'    \"salesOrganization\" : \"3116\",'+
		'    \"salesOff\" : \"FG00\",'+
		'    \"requiredDate\" : \"2017-07-25\",'+
		'    \"purchaseNumber\" : \"20170724FG00500000|5075071\",'+
		'    \"purchaseDate\" : \"2017-07-24\",'+
		'    \"orderReason\" : \"T3\",'+
		'    \"orderPartners\" : [ {'+
		'      \"partnerRole\" : \"SP\",'+
		'      \"partnerNumber\" : \"100153987\",'+
		'      \"internalControl\" : \"000005\"'+
		'    } ],'+
		'    \"orderConditions\" : [ ],'+
		'    \"name\" : \"FG0050\",'+
		'    \"documentationType\" : \"Y064\",'+
		'    \"DLVSCHDUSE\" : 0,'+
		'    \"division\" : \"0\",'+
		'    \"distributionChannel\" : \"1\",'+
		'    \"createOrderItems\" : [ {'+
		'      \"targetQuantity\" : 4,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : \"CS\",'+
		'      \"productId\" : \"3000005\",'+
		'      \"itemNumber\" : \"10\"'+
		'    }, {'+
		'      \"targetQuantity\" : 5,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : null,'+
		'      \"productId\" : \"3000020\",'+
		'      \"itemNumber\" : \"20\"'+
		'    }, {'+
		'      \"targetQuantity\" : 6,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : \"CS\",'+
		'      \"productId\" : \"3000543\",'+
		'      \"itemNumber\" : \"30\"'+
		'    }, {'+
		'      \"targetQuantity\" : 2,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : \"BA\",'+
		'      \"productId\" : \"000000000003000003\",'+
		'      \"itemNumber\" : \"40\"'+
		'    }, {'+
		'      \"targetQuantity\" : 3,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : \"CS\",'+
		'      \"productId\" : \"3000005\",'+
		'      \"itemNumber\" : \"50\"'+
		'    }, {'+
		'      \"targetQuantity\" : 1,'+
		'      \"sinPromocion\" : \"X\",'+
		'      \"salesUnit\" : \"CS\",'+
		'      \"productId\" : \"3000022\",'+
		'      \"itemNumber\" : \"60\"'+
		'    } ]'+
		'  }'+
		'}';
		ISSM_DeserializeCreateOrder_cls obj = ISSM_DeserializeCreateOrder_cls.parse(json);
		System.assert(obj != null);
	}
	
	
}