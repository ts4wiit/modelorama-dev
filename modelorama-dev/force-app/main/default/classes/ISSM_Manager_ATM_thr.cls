/**************************************************************************************
Nombre del Trigger: ISSM_Manager_ATM_thr
Versión : 1.0
Fecha de Creación : 28 Agosto 2018
Funcionalidad : Clase del trigger ISSM_Manager_ATM_tgr para procesar el cambio de gestor en los usuarios relacionados
Clase de Prueba: ISSM_Manager_ATM_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        28 - Agosto - 2018      Versión Original
*************************************************************************************/
public class ISSM_Manager_ATM_thr {
	
    public static void validExistRole(List<ISSM_Manager_ATM__c> dataManagerATM_lst) {
        List<ISSM_Manager_ATM__c> managerATM_lst = new List<ISSM_Manager_ATM__c>();
        managerATM_lst = [SELECT Id, ISSM_Role__c, ISSM_Manager__c FROM ISSM_Manager_ATM__c];
        
        for (ISSM_Manager_ATM__c reg : dataManagerATM_lst) { if (managerATM_lst.size() > 0) { for (ISSM_Manager_ATM__c manag : managerATM_lst) { if (reg.ISSM_Role__c == manag.ISSM_Role__c) { reg.AddError(System.Label.ISSM_ValidExistRole); } } } }
    }
}