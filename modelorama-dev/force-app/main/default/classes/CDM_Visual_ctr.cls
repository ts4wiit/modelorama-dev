/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria.
Project:  ABInBev (CDM Rules)
Description: Class controller for Lightning component 'CDM_Visual'
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       23-May-2018    Iván Neria. 			   Initial version
***********************************************************************************/
global with sharing  class CDM_Visual_ctr {
    @AuraEnabled
    public static List<CDM_Execution_Order__mdt> getOrderOne(String order){
       
        List<CDM_Execution_Order__mdt> results = [SELECT Id,CDM_Order__c, CDM_Description__c, CDM_Rule__c FROM CDM_Execution_Order__mdt WHERE CDM_Order__c=:order ORDER BY CDM_Rule__c];
        return results;
    }
   

}