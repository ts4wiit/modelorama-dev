/****************************************************************************************************
    General Information
    -------------------
    author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   ABInBev
 
    Description:
    Test Class for ISSM_MaterialAvailability

    Information about changes (versions)
    ================================================================================================
    Number    Dates                Author                       Description         
    ------    --------             --------------------------   -----------------------------------------
    1.0       Diciembre 04, 2018   Marco Zúñiga                 Class creation      
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_MaterialAvailability_tst {
	static Account ObjAccount{get;set;}
	static ONTAP__Product__c ObjProduct{get;set;}
	static List<ONTAP__Product__c> products_lst{get;set;}
	static List<ISSM_Bonus__c> LstBonusesSelected{get;set;}
	static ISSM_Bonus__c ObjBns{get;set;}
	
	private static void startValues(){	
		products_lst = new List<ONTAP__Product__c>();
		LstBonusesSelected = new List<ISSM_Bonus__c>();
		insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable
        );

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='0100169817',
            ONTAP__SalesOgId__c='3116',
            ONTAP__SalesOffId__c = 'FG00',
            ONTAP__ChannelId__c='01',
            ISSM_DistributionCenter__c = 'FG00',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId()
        );
        insert ObjAccount;

        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = Schema.SObjectType.ONTAP__Product__c.getRecordTypeInfosByDeveloperName().get('Products').getRecordTypeId(),
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='3000020',
            ONTAP__UnitofMeasure__c  = 'CS',
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0,
            ISSM_IsSuggested__c = 'S',
            ISSM_Suggested__c = '7',
            ISSM_Brand__c = 'Cucapa',
            ISSM_Size__c = 'Media',
            ISSM_ActualSale__c = '3',
            ONTAP__ProductShortName__c = '3',
            ISSM_Container_Size__c = '3'           
        );
        insert ObjProduct;

        products_lst.add(ObjProduct);

        ObjBns = new ISSM_Bonus__c(
            ISSM_AppliedTo__c       = ObjAccount.Id,
            ISSM_Product__c         = ObjProduct.Id,
            ISSM_SAPOrderNumber__c  = '200',
            ISSM_BonusQuantity__c   = 5,
            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;

        LstBonusesSelected.add(ObjBns);
	}

	@isTest static void testAvailabilities(){
		startValues();

		Test.startTest();
		ISSM_MaterialAvailability_cls ma_obj = new ISSM_MaterialAvailability_cls();
		ISSM_DeserializeMaterialAvailability_cls dma_obj = new ISSM_DeserializeMaterialAvailability_cls();
		System.debug(ObjAccount);
		System.debug(products_lst);
		dma_obj = ma_obj.CallOutMaterialAvailability(ObjAccount.Id, 
													products_lst, 
													true, 
													LstBonusesSelected);
		Test.stopTest();
	}
}