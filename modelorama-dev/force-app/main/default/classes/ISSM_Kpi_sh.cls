/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Scheduler to program the execution of the kpi sincronization 

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       03-08-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_Kpi_sh implements Schedulable {
	/**
	 * Execute method for the scheduler execution
	 * @param  Database.BatchableContext BC
	 */
	global void execute(SchedulableContext sc) {
		List<ISSM_ObjectInterface_cls> sObjectInterfaceList = new List<ISSM_ObjectInterface_cls>();
		sObjectInterfaceList.add(new ISSM_Kpi_cls());
        String stSalesOffice =Label.ISSM_SalesOfficeToProcess;
        List<String> arraySales = stSalesOffice.split(',');
        for(String salesOffice: arraySales){
		  Database.executeBatch(new ISSM_Account_bch(sObjectInterfaceList,true,salesOffice), 80);
        }
	}
}