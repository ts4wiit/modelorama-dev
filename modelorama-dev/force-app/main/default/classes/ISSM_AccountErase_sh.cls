/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Scheduler to program the execution of the cleansing 

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       03-08-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_AccountErase_sh implements Schedulable {
	/**
	 * Execute method for the scheduler execution
	 * @param  Database.BatchableContext BC
	 */
	global void execute(SchedulableContext sc) {
		List<ISSM_ObjectInterface_cls> sObjectInterfaceList = new List<ISSM_ObjectInterface_cls>();
		sObjectInterfaceList.add(new ISSM_Asset_cls());
		sObjectInterfaceList.add(new ISSM_EmptyBalance_cls());
		sObjectInterfaceList.add(new ISSM_OpenItem_cls());
		sObjectInterfaceList.add(new ISSM_Kpi_cls());
    	ISSM_AccountErase_bch accountaBatche = new ISSM_AccountErase_bch(sObjectInterfaceList);
	}
}