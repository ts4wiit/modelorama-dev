/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  Clase la cual genera un registro en Case (Satandard) tal y como se genero en el custom ONTAP__Case_Force__c
para este se utiliza una configuracion personalizada la cual trae name y  ISSM_APICaseForce__c estas 2 columnas contienen los nombres
API de los 2 objetos 
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public class ISSM_CreateCaseForceInCaseHandler_cls {
    ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    /**
    * Descripcion :  Metodo que realiza la insercion en el objeto Estandar y actualiza el registro en custom
    * para llenar el campo ISSM_CaseNumber__c con el numero de caso standard
    * @param  none
    * @return none
    **/
    public void CreateCaseForceInCase(List<ONTAP__Case_Force__c> lstDataCaseForceNew ) {
        Set<Id> lstIdInsertCase = new Set<Id>(); 
        Case objCaseStandard = new Case();
        List<Case>  lstInsertCase = new List<Case>();
        Map<Id,ONTAP__Case_Force__c >   lstUpdateCaseForce = new Map<Id,ONTAP__Case_Force__c >();
        ONTAP__Case_Force__c objCaseForceCustom = new ONTAP__Case_Force__c();
        List<Case>   lstselectCase = new List<Case>();
        List<ISSM_MappingFieldCase__c> lstMappingFields = objCSQuerys.QueryMappingFieldCase(); 
        
        for (ONTAP__Case_Force__c objDataCaseForce : lstDataCaseForceNew) {
            for (ISSM_MappingFieldCase__c objMappingFields :lstMappingFields) {
                // Mapea los campos de la configuracion personalizada (objMappingFields.Name = Nombre API del Objeto Standard,objMappingFields.ISSM_APICaseForce__c= Nombre API del Objeto Custom)
                objCaseStandard.put(objMappingFields.Name,objDataCaseForce.get(objMappingFields.ISSM_APICaseForce__c));
            }
            objCaseStandard.ISSM_CaseForceNumber__c = objDataCaseForce.Id;
            lstInsertCase.add(objCaseStandard);
            objCaseStandard = new Case();
        }
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger); // Inactiva el trigger en l objeto CASO esto para que no se cicle la ejecucion.
        
        insert lstInsertCase;
        
        for (Case ObjIdCase : lstInsertCase) {
            lstIdInsertCase.add(ObjIdCase.Id);
        }
        /*Despues de insertar el caso en objeto STANDARD se toma el ID para Actualizar el CUSTOM y agregarle Id externo ISSM_CaseNumber__c que es el caseNumber del obj STANDARD*/
        // lstselectCase = objCSQuerys.QueryCaseSet(lstIdInsertCase); // No funciona con el perfil. Investigar ¿permisos?
        lstselectCase = 
            [SELECT id, CaseNumber, ISSM_CaseForceNumber__c, AccountId, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c
             FROM CASE 
             WHERE id in :lstIdInsertCase];
        
        // Ajuste para evitar retipificar
        String ownerId_str = '';
        String accountId_str = '';
        Id ownerIdForce;
        
        for (Case objCaseExternalId : lstselectCase) {
            accountId_str = objCaseExternalId.AccountId;
            
            List<ISSM_TypificationMatrix__c> matrixTipification_lst = new List<ISSM_TypificationMatrix__c>();
            if (!Test.isRunningTest()) {
                matrixTipification_lst = [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c, ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_Countries_ABInBev__c, ISSM_Channel__c FROM ISSM_TypificationMatrix__c WHERE ISSM_TypificationLevel1__c =: objCaseExternalId.ISSM_TypificationLevel2__c AND ISSM_TypificationLevel2__c =: objCaseExternalId.ISSM_TypificationLevel3__c AND ISSM_Channel__c = 'OnTap/OnCall' LIMIT 1];
            } else {
                matrixTipification_lst = [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c, ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_Countries_ABInBev__c, ISSM_Channel__c
                                          FROM ISSM_TypificationMatrix__c];
            }
            ISSM_TypificationMatrix__c typificationMatrixResult = matrixTipification_lst[0];
            
            // Buscamos el propietario del case force para actualizarlo
            //Account accId = objCSQuerys.QueryAccount(accountId_str);
            
            Account accId;
            if (!Test.isRunningTest()) { accId = [SELECT Id, OwnerId, ISSM_SalesOffice__c FROM Account WHERE Id =: accountId_str]; } else { accId = [SELECT Id, OwnerId, ISSM_SalesOffice__c FROM Account LIMIT 1]; }
            
            WrOwnerIdCaseForce caseForceOwner = ISSM_CreateCaseForceInCaseHandler_cls.getForceOwner(typificationMatrixResult, accId);
            if (caseForceOwner != null) {
                ownerIdForce = caseForceOwner.caseOwnerId;
                objCaseForceCustom.ONTAP__Assigned_User__c = ownerIdForce;
            }
            
            objCaseForceCustom.ISSM_Entitlement__c = typificationMatrixResult.ISSM_Entitlement__c;
            objCaseForceCustom.id = objCaseExternalId.ISSM_CaseForceNumber__c;
            objCaseForceCustom.ISSM_CaseNumber__c = objCaseExternalId.CaseNumber;
            lstUpdateCaseForce.put(objCaseForceCustom.id, objCaseForceCustom);
        }
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_ONTAPCaseForceTrigger); //'ONTAP__Case_Force__c'
        if (!Test.isRunningTest()) { update lstUpdateCaseForce.values(); } //Update objeto custom agrega el casenumber del standard al ISSM_CaseNumber__c
    }
    
    public static WrOwnerIdCaseForce getForceOwner(ISSM_TypificationMatrix__c typicationMatrix, Account accountData) {
        Id idReturn = null;
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        List<Id> ids = null;
        ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
        //Account accountData = accountCls.getAccount(accountId); 
        Boolean blnUpdateAccountTeam = false;
        List<Account> lstAccounts = new List<Account>();
        //Account salesParnert = accountCls.getParnertAccount(accountId); 
        if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.USER){ idReturn = typicationMatrix.ISSM_OwnerUser__c; } 
        if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.QUEUE){ idReturn = accountData.OwnerId; }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.SUPERVISOR  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
               
               //accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountId);
               if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
                      ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.ISSM_SalesOffice__r.Id); } else { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id); }
               if (ids != null && ids.size() > 0) { idReturn = ids.get(0); }
               else {
                   idReturn = accountData.OwnerId;
                   blnUpdateAccountTeam = true;
               }
           }
        // Assigned to Billing Manager
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BILLINGMANAGER){
            if(accountData.ISSM_SalesOffice__c != null) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.BILLINGMANAGER, accountData.Id); if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else { idReturn = accountData.OwnerId; } }
        // Assigned to Boss Refrigeration
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BOSSREFRIGERATION) {
            if (accountData.ISSM_SalesOffice__c != null) {
                String recordTypeAccountProvider = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Provider_RecordType);
                
                lstAccounts = [SELECT Id, RecordTypeId, ISSM_Typification_Level_1__c, ISSM_Brand__c, ISSM_SalesOffice__c, ISSM_BossRefrigeration__c FROM Account WHERE RecordTypeId =: recordTypeAccountProvider AND ISSM_Typification_Level_1__c =: typicationMatrix.ISSM_TypificationLevel1__c AND ISSM_Brand__c =: typicationMatrix.ISSM_TypificationLevel2__c AND ISSM_SalesOffice__c =: accountData.ISSM_SalesOffice__c];
                    
                    if (lstAccounts != null && !lstAccounts.isEmpty() && lstAccounts[0] != null) {
                        if (lstAccounts[0].ISSM_BossRefrigeration__c != null && String.IsnotBlank(lstAccounts[0].ISSM_BossRefrigeration__c)) { idReturn = lstAccounts[0].ISSM_BossRefrigeration__c; } else { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id); if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } }
                    } else { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id); if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } }
            } else { idReturn = accountData.OwnerId; }
        }
        // Assign to Trade Marketing
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.TRADEMARKETING) { 
            if (accountData.ISSM_SalesOffice__c != null) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.TRADEMARKETING, accountData.Id); if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else { idReturn = accountData.OwnerId; }
        }
        return new WrOwnerIdCaseForce(blnUpdateAccountTeam, idReturn);
    }
    
    class WrOwnerIdCaseForce {
        Boolean blnUpdateAccountTeam { get; private set; }
        Id caseOwnerId { get; private set; }
        
        public WrOwnerIdCaseForce(Boolean blnUpdateAccountTeamParam, Id caseOwnerIdParam){
            blnUpdateAccountTeam = blnUpdateAccountTeamParam;
            caseOwnerId = caseOwnerIdParam;
        }
        
    }
    
    public void UodateToJSon(List<ONTAP__Case_Force__c> LsttriggerNew, List <ONTAP__Case_Force__c> lstDataCaseForceOld){
        List<String> LstJsonCaseForce = new List<String>();
        for(ONTAP__Case_Force__c oCaseForce : LsttriggerNew){   
            LstJsonCaseForce.add(JSON.serialize(oCaseForce, true));
        }
        if(LstJsonCaseForce.size() > 0){
            UpdateCaseForceInCaseFuturo(LstJsonCaseForce);
        }
    }   
    
    @future
    public static void UpdateCaseForceInCaseFuturo(List <String> LStJsonCaseforce /*, List <ONTAP__Case_Force__c> lstDataCaseForceOld*/) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger);
        List <ONTAP__Case_Force__c> lstDataCaseForceNew = new List <ONTAP__Case_Force__c>();
        for(String oJson : LStJsonCaseforce) {
            lstDataCaseForceNew.add((ONTAP__Case_Force__c)JSON.deserialize(oJson, ONTAP__Case_Force__c.class));
        }
        
        set<String> StIdCase = new set<String>();
        for(ONTAP__Case_Force__c oCaseForce : lstDataCaseForceNew) {
            StIdCase.add(oCaseForce.ISSM_CaseNumber__c+'');
        }
        
        Map<String,Id> MapCaseStan = new Map<String,Id>();
        List<Case> lstCaseItera = objCSQuerys.QueryCaseSetStr(StIdCase);
        
        for(Case oCase: lstCaseItera){ MapCaseStan.put(oCase.CaseNumber,oCase.Id); }
        
        Case objCaseStandard = new Case();
        List<Case>  lstUpdateCaseStan = new List<Case>();
        List<ONTAP__Case_Force__c >   lstUpdateCaseForce = new List<ONTAP__Case_Force__c >();
        ONTAP__Case_Force__c objCaseForceCustom = new ONTAP__Case_Force__c();
        List<Case>   lstselectCase = new List<Case>();
        List<ISSM_MappingFieldCase__c> lstMappingFields = objCSQuerys.QueryMappingFieldCase();
        for(ONTAP__Case_Force__c objDataCaseForce : lstDataCaseForceNew){
            objCaseStandard = new Case();
            for(ISSM_MappingFieldCase__c objMappingFields :lstMappingFields){
                //Mapea los campos de la configuracion personalizada (objMappingFields.Name =  Nombre API del Objeto Standard,objMappingFields.ISSM_APICaseForce__c= Nombre API del Objeto Custom)
                objCaseStandard.put(objMappingFields.Name,objDataCaseForce.get(objMappingFields.ISSM_APICaseForce__c));
            } 
            if(objDataCaseForce.ISSM_CaseNumber__c != null || objDataCaseForce.ISSM_CaseNumber__c !=''){
                objCaseStandard.Id = MapCaseStan.get(objDataCaseForce.ISSM_CaseNumber__c);
                
            } else {
                System.debug('######NO HAY VALORES EN EL MAPA');
            }
            
            lstUpdateCaseStan.add(objCaseStandard);
        } 
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_ONTAPCaseForceTrigger);//ONTAP__Case_Force__c
        System.debug('######lstUpdateCaseStan '+lstUpdateCaseStan);
        try{
        	update lstUpdateCaseStan; //Update objeto custom agrega el casenumber del standard al ISSM_CaseNumber__c    
        }catch(dmlexception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
}