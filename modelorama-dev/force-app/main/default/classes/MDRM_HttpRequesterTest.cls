@isTest(seeAllData=false)
public class MDRM_HttpRequesterTest {

    @isTest static void requester() {
        MDRM_HttpRequester_Mock mock = new MDRM_HttpRequester_Mock(200, 'OK', '{"WholesalerId": "123"}');
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        Map<String, String> headers = new Map<String, String>();

        headers.put('Content-Type', 'application/json');
        MDRM_HttpRequester requester = new MDRM_HttpRequester('https://endpoint', 'POST', headers, '');
        HttpResponse response = requester.sendRequest();
        Test.stopTest();

        System.assertEquals(200, response.getStatusCode());
        System.assertEquals('OK', response.getStatus());
    }
}