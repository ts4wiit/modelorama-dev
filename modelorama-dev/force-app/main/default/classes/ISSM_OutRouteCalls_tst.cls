/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Clase de prueba para el controlador ISSM_OutRouteCalls_ctr

    Information about changes (versions)
    ================================================================================================
    Number    Dates           	Author                       Description				Cobertura
    ------    -----------       --------------------------   -----------------------------------------
    1.0       30-Nov-2017   	Luis Licona                  Creación de la Clase   	90%
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_OutRouteCalls_tst {
	
	public static ONCALL__Call__c ObjCall = new ONCALL__Call__c();
	public static Account ObjAccount = new Account();
	public static ISSM_OnCallQueries_cls CTRSOQL 	= new ISSM_OnCallQueries_cls();
	public static ONTAP__Route__c ObjRoute = new ONTAP__Route__c();

	public static void StartValues(){
		insert new ISSM_Parameters__c(Value__c = '5',
			Name = Label.ISSM_OutRouteCalls);

		ObjAccount = new Account(
			Name = 'TST',
			ONTAP__Main_Phone__c='5510988765',
			ISSM_MainContactE__c=true,
			ONTAP__SAPCustomerId__c='100153987',
			ONTAP__SalesOgId__c='3116',
			ONTAP__ChannelId__c='01',
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId()
		);
		insert ObjAccount;

		ObjCall = new ONCALL__Call__c(
			Name 			= 'TST',
			ONCALL__POC__c 	= ObjAccount.Id,
			RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId(),
			ISSM_ValidateOrder__c=false
		);

		ObjRoute = new ONTAP__Route__c(
			ServiceModel__c 	= 'Telesales',
			RouteManager__c 	= UserInfo.getUserId(),
			ONTAP__RouteId__c 	= 'FG0050',
			ONTAP__SalesOfficeId__c = 'FG00',
			ISSM_SAPUserId__c 	= '12345678',
			RecordTypeId 		= Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId()
			);
		insert ObjRoute;
	}


	@isTest static void SaveCall() {
		StartValues();
		
		ISSM_OutRouteCalls_ctr.WrapperpCallList newRecord = new ISSM_OutRouteCalls_ctr.WrapperpCallList();
		ONCALL__Call__c newCallRecord = new ONCALL__Call__c();        
		newRecord.record = ObjCall;
		newRecord.StrName= 'TST';
		
		ISSM_OutRouteCalls_ctr CTR = new ISSM_OutRouteCalls_ctr();
		CTR.waCallList = new List<ISSM_OutRouteCalls_ctr.WrapperpCallList>();
		CTR.waCallList.Add(newRecord);
		CTR.SaveMultipleCalls();
		CTR.rowToRemove=0;
		CTR.waCallList.Add(newRecord);
		CTR.removeRowFromCallList();
	}
	

	@isTest static void DontSaveCall() {
		StartValues();
		
		Test.startTest();
			ISSM_OutRouteCalls_ctr.WrapperpCallList newRecord = new ISSM_OutRouteCalls_ctr.WrapperpCallList();
			ONCALL__Call__c newCallRecord = new ONCALL__Call__c();        
			newRecord.record = ObjCall;
			newRecord.StrName= 'TST';
			
			ISSM_OutRouteCalls_ctr CTR = new ISSM_OutRouteCalls_ctr();
			
			CTR.waCallList.Add(newRecord);
			CTR.waCallList.get(0).record.Name = null;
			CTR.SaveMultipleCalls();
		Test.stopTest();
	}	
}