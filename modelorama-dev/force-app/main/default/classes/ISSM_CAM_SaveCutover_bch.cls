/**
 * Developed by:	   Avanxo México
 * Author:			   Oscar Alvarez
 * Project:			   AbInbev - CAM
 * Description:		   Clase batch Apex for save the historical cut over
 *
 *  No.        Fecha                  Autor               Descripción
 *  1.0    13-Noviembre-2018         Oscar Alvarez             CREATION
 *
 */
global class ISSM_CAM_SaveCutover_bch implements Database.Batchable<sObject>, Database.stateful {

	global ISSM_Asset__c[] lstAsset   = new List<ISSM_Asset__c>();
    global  String operation;
	
	global ISSM_CAM_SaveCutover_bch(List<ISSM_Asset__c> lstAssetCAM,String ope){
		lstAsset 	= lstAssetCAM;
		operation	= ope;	
	}
	
	global ISSM_Asset__c[] start(Database.BatchableContext BC) {
   		System.debug('#### BATCH START ####');
		return lstAsset;
	}

   	global void execute(Database.BatchableContext BC, List<ISSM_Asset__c> scope) {
   		System.debug('#### BATCH EXECUTE  ####');
   		if(operation == Label.ISSM_CAM_OperationSaveAndExit){
   			update scope;
   		}
   		if(operation == Label.ISSM_CAM_OperationProcessCutOver){
   			//clean object asset
   			String IdRecordTypeHistory = [SELECT Id  FROM RecordType WHERE DeveloperName ='History' AND SobjectType ='ISSM_CutOver_History__c'].Id;
   			List<ISSM_CutOver_History__c> lstCutOverHistory = new List<ISSM_CutOver_History__c>();
            Map<String, ISSM_EstatusSFDC_CutOverReason__c> mapEstatusCutOverReason = ISSM_EstatusSFDC_CutOverReason__c.getAll();
   			for(ISSM_Asset__c asset : scope){
   				//new record for history
   				if(!asset.ISSM_CutOver__c){
	   				ISSM_CutOver_History__c cutOverHistory 	= new ISSM_CutOver_History__c();
	   				cutOverHistory.ISSM_AssetCAM__c         = asset.Id;
					cutOverHistory.ISSM_Asset_number__c     = asset.Asset_number__c;
					cutOverHistory.ISSM_Centre__c           = asset.ISSM_Centre__c;
					cutOverHistory.ISSM_Equipment_number__c = asset.Equipment_number__c;
					cutOverHistory.ISSM_Material_number__c  = asset.ISSM_Material_number__c;
					cutOverHistory.ISSM_Serial_Number__c    = asset.ISSM_Serial_Number__c;
					cutOverHistory.ISSM_Status_SAP__c       = asset.ISSM_Status_SAP__c;
					cutOverHistory.ISSM_Status_SFDC__c      = mapEstatusCutOverReason.get(asset.ISSM_CutOverReason__c).ISSM_CAM_StatusSFDC__c;
                    asset.ISSM_Status_SFDC__c      			= mapEstatusCutOverReason.get(asset.ISSM_CutOverReason__c).ISSM_CAM_StatusSFDC__c;
					cutOverHistory.ISSM_Type__c				= asset.Type__c;
					cutOverHistory.ISSM_CutOverReason__c	= asset.ISSM_CutOverReason__c;	
					cutOverHistory.RecordTypeId				= IdRecordTypeHistory;		
					lstCutOverHistory.add(cutOverHistory);   					
   				}
				asset.ISSM_CutOverReason__c = Test.isRunningTest() ? 'Lost Unit' : null;	
				asset.ISSM_CutOver__c       = false;	
   			}
   			update scope;
   			if(lstCutOverHistory.size() > 0)
   				insert lstCutOverHistory;   			
   		}
	 
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('#### BATCH FINISH  ####');
		
	}
	
}