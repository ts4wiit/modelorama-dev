/**
 * This class execute a batch which delete applications in Salesforce.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileApplicationBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	global Database.QueryLocator start(Database.BatchableContext objBatchableContext) {
		return Database.getQueryLocator(AllMobileStaticVariablesClass.STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_APPLICATIONS);
	}

	global void execute(Database.BatchableContext objBatchableContext, List<AllMobileApplication__c> lstAllMobileApplicationSF) {
		AllMobileApplicationOperationClass.syncDeleteAllMobileApplicationInSF(lstAllMobileApplicationSF);
	}

	global void finish(Database.BatchableContext objBatchableContext) {
	}
}