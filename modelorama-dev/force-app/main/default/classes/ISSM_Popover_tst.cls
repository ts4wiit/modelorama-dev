/**
 * Created by:			Avanxo México
 * Author:				Carlos Pintor / cpintor@avanxo.com
 * Project:				AbInbev - Trade Revenue Management
 * Description:			Apex class to test the methods of the Apex controller 'ISSM_Popover_ctr'.
 *
 * N.     Date             Author                     Description
 * 1.0    06-19-2018       Carlos Pintor              Created
 *
 */
@isTest
public class ISSM_Popover_tst {
    
    @testSetup 
    static void setupData() {
        String RecordTypeAccount = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        String RecordTypeAccountSalesOrg = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
            
        //Creamos Cuenta con SalesOrg
        Account  objAccountSalesOrg = new Account();
            objAccountSalesOrg.Name = 'Name Sales Org';
            objAccountSalesOrg.RecordTypeId = RecordTypeAccountSalesOrg;
            objAccountSalesOrg.ONTAP__SalesOgId__c = 'OR01';
            insert objAccountSalesOrg;
        
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
        	objAccount.ONTAP__SAP_Number__c = '0123456789';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ONTAP__SalesOffId__c = 'SO01';
            objAccount.ONTAP__SalesOgId__c ='OR01';
            insert objAccount;
    }
    
    static testMethod void testGetLimitRecords(){
        //get the sales office record
        Account off01 = [SELECT Id FROM Account WHERE Name = 'Name SalesOffice' LIMIT 1];
        
        //Create the combo limit related to the sales structure
        ISSM_ComboLimit__c comboLimit01 = new ISSM_ComboLimit__c();
        comboLimit01.ISSM_ComboLevel__c = 'ISSM_SalesOffice';
        comboLimit01.ISSM_SalesStructure__c = off01.Id;
        comboLimit01.ISSM_AllowedCombos__c = 1000;
        comboLimit01.ISSM_IsActive__c = true;
        comboLimit01.RecordTypeId = Schema.getGlobalDescribe().get('ISSM_ComboLimit__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_LimitComboMx').getRecordTypeId();
        Insert comboLimit01;
        
        ISSM_Combos__c combo01 = new ISSM_Combos__c();
        combo01.ISSM_StartDate__c 			= 		Date.today() + 2;
        combo01.ISSM_EndDate__c				=		Date.today() + 30;
        combo01.ISSM_LongDescription__c		=		'Long description for the new combo';
        combo01.ISSM_ShortDescription__c	=		'ShortDescr';
        combo01.ISSM_NumberSalesOffice__c	=		10;
        combo01.ISSM_NumberByCustomer__c	=		1;
        combo01.ISSM_Currency__c			=		'MXN';
        combo01.ISSM_ComboLimit__c			=		comboLimit01.Id;
        combo01.ISSM_TypeApplication__c		=		'ALLMOBILE;TELESALES;B2B;BDR';
        combo01.RecordTypeId				=		Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        Insert combo01;
        
        Test.startTest();
        List<ISSM_ComboLimit__c> comboLimitList = ISSM_Popover_ctr.getLimitRecords(combo01.Id);
        System.assert(comboLimitList.size() > 0);
        Test.stopTest();
    }

}