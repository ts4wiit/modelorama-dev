/****************************************************************************************************
    Información general
    -------------------
    author:     Marco Zuñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Clase para validar campos requeridos de cuenta y usuario en la generación del pedido

    Information about changes (versions)
    ================================================================================================
    Number    Dates                Author                       Description              
    ------    --------             --------------------------   ------------------------------------
    1.0       04-Septiembre-2017   Marco Zuñiga                 Creación de la Clase
    1.1       04-Septiembre-2017   Luis Licona                  Creación de clase para reuitlizar en 
                                                                diferentes clases 
	1.2       04-Septiembre-2017   Hector Diaz                  Se agregara funcion para no dejar crear pedido fuera de horario 
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_ValidateRequestedFields_cls {
	public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();
	public ISSM_ValidateRequestedFields_cls(){}
	
    //Valida campos necesario a nivel de cuenta
	public List<String> checkAccountRequiredValues(Account[] ObjAcc){ //With DSD
        Map<String, Schema.SObjectField> accountFieldsMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        List<String> LstMissedFieldsA = new List<String>();
        
        for(Account objAccount : ObjAcc){            
            LstMissedFieldsA.add(objAccount.ONTAP__SAP_Number__c         == null ? accountFieldsMap.get('ONTAP__SAP_Number__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__SalesOgId__c          == null ? accountFieldsMap.get('ONTAP__SalesOgId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__ChannelId__c          == null ? accountFieldsMap.get('ONTAP__ChannelId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__SalesOffId__c         == null ? accountFieldsMap.get('ONTAP__SalesOffId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_Invoicingdocument__c    == null ? accountFieldsMap.get('ISSM_Invoicingdocument__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONCALL__Ship_To_Name__c      == null ? accountFieldsMap.get('ONCALL__Ship_To_Name__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_Invoicingdocument__c    == Label.ISSM_IncorrectInvoicing ? accountFieldsMap.get('ISSM_Invoicingdocument__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_DistributionCenter__c   == null ? accountFieldsMap.get('ISSM_DistributionCenter__c').getDescribe().getLabel() : null);
        }    
     
        Integer i = 0;
        while (i < LstMissedFieldsA.size()){
            if(LstMissedFieldsA.get(i) == null)
                LstMissedFieldsA.remove(i);
            else
                i++;
        }
        
        return LstMissedFieldsA;
    }

    public List<String> checkAccReqValuesUT(Account[] ObjAcc){ //Without DSD
        Map<String, Schema.SObjectField> accountFieldsMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        List<String> LstMissedFieldsA = new List<String>();

        //Boolean hasRouteAcc_Str = false;
        
        for(Account objAccount : ObjAcc){  //Iterates only one record         
            LstMissedFieldsA.add(objAccount.ONTAP__SAP_Number__c         == null ? accountFieldsMap.get('ONTAP__SAP_Number__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__SalesOgId__c          == null ? accountFieldsMap.get('ONTAP__SalesOgId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__ChannelId__c          == null ? accountFieldsMap.get('ONTAP__ChannelId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONTAP__SalesOffId__c         == null ? accountFieldsMap.get('ONTAP__SalesOffId__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_Invoicingdocument__c    == null ? accountFieldsMap.get('ISSM_Invoicingdocument__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ONCALL__Ship_To_Name__c      == null ? accountFieldsMap.get('ONCALL__Ship_To_Name__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_Invoicingdocument__c    == Label.ISSM_IncorrectInvoicing ? accountFieldsMap.get('ISSM_Invoicingdocument__c').getDescribe().getLabel() : null);
            LstMissedFieldsA.add(objAccount.ISSM_DistributionCenter__c   == null ? accountFieldsMap.get('ISSM_DistributionCenter__c').getDescribe().getLabel() : null);            
            LstMissedFieldsA.add(objAccount.ISSM_CustomerType__c         == Label.ISSM_ERROR ? accountFieldsMap.get('ISSM_CustomerType__c').getDescribe().getLabel() : null);
        }    

     
        Integer i = 0;
        while (i < LstMissedFieldsA.size()){
            if(LstMissedFieldsA.get(i) == null)
                LstMissedFieldsA.remove(i);
            else
                i++;
        }
        
        return LstMissedFieldsA;
    }

    public List<String> checkOrderRouteWDSD(String StrAccId, String StrServModel){ // With DSD
        List<String> LstMissedFieldsR = new List<String>(); 

        AccountByRoute__c[] ObjAccByRoute = CTRSOQL.getAccByRoute(StrAccId,StrServModel);
        System.debug('ObjAccByRoute = ' + ObjAccByRoute);

        if(ObjAccByRoute.size() > 0){
            if(ObjAccByRoute[0].Route__r.ISSM_SAPUserId__c == null){
                LstMissedFieldsR.add(Label.ISSM_NoSAPId);
            }    
        }else{
            LstMissedFieldsR.add(Label.ISSM_NoSAPId);
        }

        return LstMissedFieldsR;
    }

    public List<String> checkOrderRouteWODSD(String StrRouteCode){ //Without DSD
        List<String> LstMissedFieldsR = new List<String>(); 

        ONTAP__Route__c ObjRoute = StrRouteCode != null ? CTRSOQL.getRoute(StrRouteCode) : new ONTAP__Route__c();

        if(ObjRoute.ISSM_SAPUserId__c == null || ObjRoute == null){
            LstMissedFieldsR.add(Label.ISSM_NoSAPId);
        }

        return LstMissedFieldsR;
    }

    public List<String> checkCallList(ONCALL__Call__c ObjCall){        
        List<String> LstMissedFields = new List<String>(); 
        Map<String, Schema.SObjectField> userFieldsMap = Schema.getGlobalDescribe().get('ONCALL__Call__c').getDescribe().fields.getMap();        

        ONCALL__Call__c[] LstCalls = new List<ONCALL__Call__c>();
        LstCalls.add(ObjCall);

        for(ONCALL__Call__c objCurrentCall : LstCalls){       
            LstMissedFields.add(objCurrentCall.CallList__c       == null ? userFieldsMap.get('CallList__c').getDescribe().getLabel() : null);
        }

        Integer i = 0;
        while (i < LstMissedFields.size()){
            if(LstMissedFields.get(i) == null)
                LstMissedFields.remove(i);
            else
                i++;
        }

        return LstMissedFields;
    }

    public List<String> checkRoute(ONCALL__Call__c ObjCall){        
        List<String> LstMissedFields = new List<String>(); 
        Map<String, Schema.SObjectField> userFieldsMap = Schema.getGlobalDescribe().get('ONTAP__Route__c').getDescribe().fields.getMap();        

        ONCALL__Call__c[] LstCalls = new List<ONCALL__Call__c>();
        LstCalls.add(ObjCall);

        for(ONCALL__Call__c objCurrentCall : LstCalls){        
            LstMissedFields.add(objCurrentCall.CallList__r.Route__r.ONTAP__RouteId__c == null ? userFieldsMap.get('ONTAP__RouteId__c').getDescribe().getLabel() : null);
            LstMissedFields.add(objCurrentCall.CallList__r.Route__r.ISSM_SAPUserId__c == null ? userFieldsMap.get('ISSM_SAPUserId__c').getDescribe().getLabel() : null);
        }

        Integer i = 0;
        while (i < LstMissedFields.size()){
            if(LstMissedFields.get(i) == null)
                LstMissedFields.remove(i);
            else
                i++;
        }

        return LstMissedFields;
    }


    //valida si el TU cuenta con llamadas posteriores al dia en que se ejecuta el pedido sobre la cuenta
    public List<ONCALL__Call__c> checkCallsUniversalTelesales(String AccountId, Id RecordType){        
        return CTRSOQL.getCalls(AccountId,RecordType);
    }
    
     /*BLOQUE PARA MOSTRAR ALERTA CUANDO SE REQUIERE UN PEDIDO FUERA DE LA HORA DE SERVICIO*/
    public Boolean CheckOutValidate (List<Account> ObjAcc){
    	Boolean validateCheckout = false;
    	List<String> lstSplitDateCheckOut = new List<String>();
 		if( String.valueOf(ObjAcc[0].ISSM_SalesOrg__r.ISSM_CheckOut__c) !='' && String.valueOf(ObjAcc[0].ISSM_SalesOrg__r.ISSM_CheckOut__c)!=null){ 
 			lstSplitDateCheckOut =  String.valueOf(ObjAcc[0].ISSM_SalesOrg__r.ISSM_CheckOut__c).split(':');  
			String HourSystemNow =  (string.valueOf(system.now().hour()) == '00' ? '12' : string.valueOf(system.now().hour()));				
			System.debug('Hora : '+ HourSystemNow   +'--- '+  lstSplitDateCheckOut[0]  );
			System.debug('Minute  : '+ string.valueOf(system.now().minute())  + '--- '+  lstSplitDateCheckOut[1] );
			
		
			if(Integer.valueOf(HourSystemNow) >= Integer.valueOf(lstSplitDateCheckOut[0])  &&  Integer.valueOf(system.now().minute()) > Integer.valueOf(lstSplitDateCheckOut[1])){ 
				validateCheckout  = true;
			} 
 				
 		}
    	
		return validateCheckout;
    }
    
    
}