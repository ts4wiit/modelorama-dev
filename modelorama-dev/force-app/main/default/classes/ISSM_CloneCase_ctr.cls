/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 30-Octubre-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/
public with sharing class ISSM_CloneCase_ctr {
    
    ApexPages.StandardController standardController;
	String fieldsCase = '';
	String fieldsCaseClone = '';
	List<String> lstFieldsClone = new List<String>();
	List<Case> lstInsertCase = new List<Case>();
	Case record = new Case();
	/*
	*/ 
    public ISSM_CloneCase_ctr(ApexPages.StandardController standardController) {
        this.standardController = standardController;
       	for(ISSM_FieldsCloneCase__c objFieldsClone : [Select id,Name FROM ISSM_FieldsCloneCase__c Where ISSM_Active__c = true]){
   			if(objFieldsClone.Name != null && objFieldsClone.Name != ''){
   				lstFieldsClone.add(objFieldsClone.Name);
   			}
   		}
		if (!Test.isRunningTest()){
			this.standardController.addFields(lstFieldsClone);	
		}else{
			record = (Case)standardController.getRecord();
		}
			
    }
    
    /*
    */
    public PageReference cloneCaseInCaseForce(){
    	PageReference acctPage; 
		Case cloneCase = new Case();
		Case currentCase = (Case) standardController.getRecord();
		for(String objFieldsClone : lstFieldsClone){
			 cloneCase.put(objFieldsClone,currentCase.get(objFieldsClone));
		} 
		cloneCase.ISSM_IsCloned__c = true;
		if(cloneCase != null){ 
			try{ 
				insert cloneCase; 
			}catch(Exception ex){
				System.debug('Exception : '+ex);
			}
			
			String CaseEditId = cloneCase.id; //Se requiere para la visualforce
      		acctPage = new PageReference(System.label.ISSM_PageReferenceConsoleCaseEdit +CaseEditId);
      		acctPage.setRedirect(true);   
        
			return acctPage;
		}else{
			return null;  
		}
    } 
}