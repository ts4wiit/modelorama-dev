/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Batch to related all records to the object Model of CDM (Only for MDM_Account).
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       2-05-2018   Iván Neria			   Initial version
***********************************************************************************/
global class CDM_UpdateAccountRshp_bch implements Database.Batchable<sObject>
{

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT Id, KUNNR__c, VTWEG__c, VKORG__c, SPART__c'
        +' FROM CDM_Temp_Interlocutor__c WHERE MDM_Account__c=null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        List<String> lstKUNNR = new List<String>();
        for ( CDM_Temp_Interlocutor__c tempInt : (List<CDM_Temp_Interlocutor__c>)scope)
        {
          lstKUNNR.add(tempInt.KUNNR__c);
        }
        List<MDM_Account__c> lstMDMAcc= CDM_UpdateRshpsController_ctr.getMDMAccounts(lstKUNNR);
        List<CDM_Temp_Interlocutor__c> lstInterToUpdate= new List<CDM_Temp_Interlocutor__c>();
        //Set helper to avoid duplicity in list of Interlocutors.
        Set<String> setHelper = new Set<String>();
        
        for(CDM_Temp_Interlocutor__c  tempIntToUpdate: (List<CDM_Temp_Interlocutor__c>)scope){
            
            for(MDM_Account__c tempAccToUpdate: lstMDMAcc){
               
                if(tempAccToUpdate.PARVW__c == tempIntToUpdate.KUNNR__c && tempAccToUpdate.VKORG__c == tempIntToUpdate.VKORG__c && tempAccToUpdate.SPART__c == tempIntToUpdate.SPART__c && tempAccToUpdate.VTWEG__c == tempIntToUpdate.VTWEG__c){
                    tempIntToUpdate.MDM_Account__c= tempAccToUpdate.Id;
                    
                    if(!setHelper.contains(tempIntToUpdate.Id)){
                        setHelper.add(tempIntToUpdate.Id);
                        lstInterToUpdate.add(tempIntToUpdate);
                    }
                    
                }
            }
            
        }
        //update scope;
        update lstInterToUpdate;
    }  
    global void finish(Database.BatchableContext BC)
    {
        EmailTemplate template =[SELECT Id FROM EmailTemplate WHERE Name=:Label.CDM_Apex_CDM_Relation_MDM_Account];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setTemplateId(template.Id);
        mail.setTargetObjectId(System.UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}