public class ContentProvController {
    public String archivoSeleccionado{get;set;}
    public String 			Type 	{get; set;}
    public String 			fN 		{get; set;}
    public blob 			file 	{get; set;}
    public String 			title	{get; set;}
    public ContentVersion 	cv 		{get; set;}
    public Agri_Supplier__c seller	{get;set;}
    
    public ContentProvController (ApexPages.StandardController stdController){
        seller = (Agri_Supplier__c) stdController.getRecord();
        seller = [SELECT Id, Name, Agri_Last_Name__c, Agri_Account_Type__c FROM Agri_Supplier__c WHERE Id =: Seller.Id];
    }
    
    public List<SelectOption> getArchivosOptionsProv(){
        List<selectOption> 	archivoListSO 	= new List<selectOption>();
        if(seller.Agri_Account_Type__c == 'Fisica') { 
            archivoListSO.add(new SelectOption('--Ninguno--','--Ninguno--')); 
            archivoListSO.add(new SelectOption('INE_1_INE','INE_1_INE')); 
            archivoListSO.add(new SelectOption('CURP_1_CURP','CURP_1_CURP')); 
            archivoListSO.add(new SelectOption('COMDOM_1_COMDOM','COMDOM_1_COMDOM'));
            archivoListSO.add(new SelectOption('ESCUBA_1_ESCUBA','ESCUBA_1_ESCUBA '));
            archivoListSO.add(new SelectOption('TITPRO_1_TITPRO','TITPRO_1_TITPRO'));
            archivoListSO.add(new SelectOption('RECOAG_1_RECOAG','RECOAG_1_RECOAG'));
            archivoListSO.add(new SelectOption('RFC_1_RFC','RFC_1_RFC'));
        }
        
        if(seller.Agri_Account_Type__c == 'Moral') {
            archivoListSO.add(new SelectOption('--Ninguno--','--Ninguno--'));
            archivoListSO.add(new SelectOption('CURP_1_CURP','CURP_1_CURP')); 
            archivoListSO.add(new SelectOption('ACTCON_1_ACTCON','ACTCON_1_ACTCON'));
            archivoListSO.add(new SelectOption('COMDOM_1_COMDOM','COMDOM_1_COMDOM'));
            archivoListSO.add(new SelectOption('ESCUBA_1_ESCUBA','ESCUBA_1_ESCUBA '));
            archivoListSO.add(new SelectOption('RFC_1_RFC','RFC_1_RFC'));
            archivoListSO.add(new SelectOption('INE_2_INE','INE_2_INE')); 
            archivoListSO.add(new SelectOption('CURP_2_CURP','CURP_2_CURP')); 
            archivoListSO.add(new SelectOption('CODOFI_2_CODOFI','CODOFI_2_CODOFI'));            
        }        
        
        return archivoListSO;    
    }        
    
    public PageReference upload() {
        ContentVersion cv 			= new ContentVersion();
        cv.versionData 				= file;
        cv.title 					= archivoSeleccionado;
        cv.pathOnClient 			= fN;
        cv.FirstPublishLocationId 	= ''+Seller.Id; 
        try
        {
            insert cv;
            system.debug('*********************Result'+cv.pathonClient);
        }
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Document in Library'));
            return null;
        }
        finally
        {
            cv= new ContentVersion();
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'El archivo se adjuntó correctamente al prospecto '+ Seller.Name + ' ' + Seller.Agri_Last_Name__c));
        return null;
    }
}