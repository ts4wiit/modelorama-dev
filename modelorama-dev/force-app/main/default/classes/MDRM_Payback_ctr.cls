public with sharing class MDRM_Payback_ctr{

    public String currentRecordId {get;set;}

    public MDRM_Payback_ctr(ApexPages.StandardSetController controller) {
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
    }
}