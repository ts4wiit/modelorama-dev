/**************************************************************************************
Nombre de la Clase Apex de Prueba: ISSM_CustomerServiceQuerys_tst
Versión : 1.0
Fecha de Creación : 09 Octubre 2018
Funcionalidad : Clase de prueba de la clase ISSM_CustomerServiceQuerys_cls
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega       09 - Octubre - 2018     Versión Original
*************************************************************************************/
@isTest
public class ISSM_CustomerServiceQuerys_tst {
	static testmethod void testMethod1() {
        
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        
        // Insertamos un usuario
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@test9test.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@test9test.com');
        insert user1;
        
        // Obtenemos el tipo de registro de las cuentas
        String rtAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        
        // Insertamos un registro de matriz de tipificación
        List<ISSM_TypificationMatrix__c> matTyp_lst = new List<ISSM_TypificationMatrix__c>();
        Set<String> idsMatTyp_set = new Set<String>();
        ISSM_TypificationMatrix__c tM1 = new ISSM_TypificationMatrix__c();
        tm1.ISSM_UniqueIdentifier__c = 'TM - 00001';
        tM1.ISSM_TypificationLevel1__c = 'Modelorama';
        tM1.ISSM_TypificationLevel2__c = 'Modelorama empresarios';
        tM1.ISSM_TypificationLevel3__c = 'Quejas y sugerencias';
        tM1.ISSM_TypificationLevel4__c = 'Producto dañado';
        tM1.ISSM_AssignedTo__c = 'Modelorama';
        tM1.ISSM_OwnerQueue__c = 'A135';
        insert tM1;
        matTyp_lst.add(tM1);
        idsMatTyp_set.add(tM1.Id);
        
        // Insertamos un grupo
        Group newGroup = new Group();
        newGroup.Name = 'Test';
        newGroup.DeveloperName = 'A135';
        insert newGroup;
        
        // Creamos una cuenta
        List<Id> account_lst = new List<Id>();
        Set<String> account_set = new Set<String>();
        List<Account> acc_lst = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Account Prospect';
        objAccount.ONTAP__Street__c = 'SMateo';
        objAccount.ONTAP__Street_Number__c = '233';
        objAccount.ONTAP__Classification__c = 'Botella Abierta';
        objAccount.ONTAP__Segment__c = 'Buena Carta';
        objAccount.ONTAP__Main_Phone__c = '555555555';
        objAccount.ISSM_LastPaymentPlanDate__c = null;
        objAccount.ISSM_LastContactDate__c = null;
        objAccount.RecordTypeId = rtAccount;
        objAccount.ONTAP__SalesOffId__c = 'SO1';
        objAccount.ONTAP__SalesOgId__c = 'SO2';
        insert objAccount;
        account_set.add(objAccount.Id);
        account_lst.add(objAccount.Id);
        acc_lst.add(objAccount);
        
        // Insertamos un documento
        ISS_DocumentTypeOpenItems__c docOP = new ISS_DocumentTypeOpenItems__c();
        docOP.Name = 'Test';
        docOP.ISSM_Active__c = true;
        insert docOP;
        
        // Guardamos los ids de las oficinas de ventas
        Set<String> lstSalesOffId = new Set<String>();
        String RecordTypeSalesOfficetId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOffice_RecordType);
        lstSalesOffId.add(objAccount.ONTAP__SalesOffId__c);
        
        // Guardamos los ids de las organizaciones de ventas
        Set<String> lstSalesOgId = new Set<String>();
        String RecordTypeSalesOrg = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_SalesOrg_RecordType);
        lstSalesOgId.add(objAccount.ONTAP__SalesOgId__c);
        
        // Insertamos un inventario
        ISSM_Asset_Inventory__c inv =
            new ISSM_Asset_Inventory__c( Name = 'VNTEST',  ISSM_Stock_SFDC__c = 20,  ISSM_Material_Number__c = '8000300', ISSM_Centre__c='OFF');
        insert inv;

        // Insertamos un case force
        List<Id> caseForce_lst = new List<Id>();
        Set<Id> caseForce_set = new Set<Id>();
        ONTAP__Case_Force__c cf_sol =
            new ONTAP__Case_Force__c( ONTAP__Subject__c='Solicitud',
                                     ONTAP__Account__c = objAccount.id,  ISSM_Asset_Inventory__c = inv.id,  ONTAP__Quantity__c = 1, ONTAP__Send_to_Approval__c = true,
                                     ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_ClassificationLevel2__c = 'Entrega de equipo',
                                     ISSM_ApprovalStatus__c='In Progress', ISSM_Delivery_Date_CAM__c = date.today().toStartOfWeek()+7);
        caseForce_lst.add(cf_sol.Id);
        caseForce_set.add(cf_sol.Id);
        
        // Insertamos un comentario del case force
        List<Id> lstIdsCaseComment = new List<Id>();
        ONTAP__Case_Force_Comment__c cfComment = new ONTAP__Case_Force_Comment__c(ONTAP__Case_Force__c = cf_sol.Id, ONTAP__Comment__c = 'Test Comment');
        lstIdsCaseComment.add(cfComment.Id);
        
        // Creamos un caso
        List<Case> case_lst = new List<Case>();
        Case objCase = new Case();
        objCase.Subject = 'Subject';
        objCase.Description = 'Description';
        objCase.Status = 'Closed';
        objCase.ISSM_TypificationLevel1__c = 'Refrigeración';
        objCase.ISSM_TypificationLevel2__c = 'Entrega de equipo';
        objCase.AccountId = objAccount.Id;
        insert objCase;
        case_lst.add(objCase);
        
        Set<String> idsCase_set = new Set<String>();
        List<String> idsCase_lst = new List<String>();
        
        idsCase_set.add(objCase.Id);
        idsCase_lst.addAll(idsCase_set);
        
        // Insertamos la configuración personalizada para mapear los campos
        ISSM_MappingFieldCase__c mpf = new ISSM_MappingFieldCase__c();
        mpf.Name = 'Mpf_Test_1';
        mpf.ISSM_APICaseForce__c = 'Mpf_Test_1';
        mpf.Active__c = true;
        insert mpf;
        
        // Insertamos la configuración personalizada para la configuración de la app
        ISSM_AppSetting_cs__c appSett = new ISSM_AppSetting_cs__c();
        appSett.ISSM_IdQueueWithoutOwner__c = '';
        appSett.ISSM_IdQueueFriendModel__c = '';
        appSett.ISSM_IdProviderWithOutMail__c = '';
        appSett.ISSM_IdQueueTelecolletion__c = '';
        insert appSett;
        
        // Insertamos una llamada
        String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
        List<ONCALL__Call__c> lstDataNewCall = new List<ONCALL__Call__c>();
        ONCALL__Call__c ObjCall = new ONCALL__Call__c();
        objCall.RecordTypeId = RecordTypeCallId;
        ObjCall.ISSM_Active__c = true;
        ObjCall.Name = 'Call Telecollection';
        ObjCall.ONCALL__POC__c = objAccount.Id;
        insert ObjCall;
        lstDataNewCall.add(ObjCall);
        
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        // Insertamos MDRM_Modelorama_Postal_Code__c Agency
        MDRM_Modelorama_Postal_Code__c objModeloramaPostalCode = new  MDRM_Modelorama_Postal_Code__c();
        objModeloramaPostalCode.MDRM_Postal_Code__c = '57710';
        objModeloramaPostalCode.Name = 'ModeloramaPstalC';
        objModeloramaPostalCode.MDRM_City__c = 'Cuauhtémoc';
        insert objModeloramaPostalCode;
        
        // Insertamos una oficina de ventas
        Account  objAccountSalesOffice = new Account();
        objAccountSalesOffice.Name = 'Name SalesOffice';
        objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
        insert objAccountSalesOffice;
        
        // Insertamos QueryColonyxAgency
        Set<Id> idsColonyAccount_set = new Set<Id>();
        ISSM_ColonyxAgency__c objQueryColonyxAgency = new ISSM_ColonyxAgency__c();
        objQueryColonyxAgency.ISSM_ColonyNamePC__c =  objModeloramaPostalCode.Id;
        objQueryColonyxAgency.ISSM_AgencyName__c = objAccountSalesOffice.Id;
        insert objQueryColonyxAgency;
        idsColonyAccount_set.add(objQueryColonyxAgency.Id);
        
        Set<Id> idsSalesOffice_set = new Set<Id>();
        idsSalesOffice_set.add(objAccountSalesOffice.Id);
        
        List<Account> lstAccounts = new List<Account>();
        List<Account> lstAccountsSalesOffice = new List<Account>();
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Provider_RecordType);
        lstAccountsSalesOffice = objCSQuerys.QueryAccountSalesOffice(case_lst);
        
        // Insertamos una partida abierta
        Set<Id> idOpenItemSet = new Set<Id>();
        ISSM_OpenItemB__c objOpenItemDebit = new ISSM_OpenItemB__c();
        objOpenItemDebit.ISSM_Account__c = objAccount.Id;
        objOpenItemDebit.ISSM_DueDate__c = System.today()-35;
        objOpenItemDebit.ISSM_Amounts__c =15000;
        objOpenItemDebit.ISSM_Debit_Credit__c = 'S';
        insert objOpenItemDebit;
        idOpenItemSet.add(objOpenItemDebit.Id);
        
        // Insertamos un archivo adjunto
        Set<String> setAttcQuery = new Set<String>();
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('Cuerpo del archivo');
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = objAccount.Id;
        insert attachment;
        setAttcQuery.add(objAccount.Id);
        
        Test.startTest();
        
        List<CaseMilestone> lst01 = objCSQuerys.QueryCaseMilestone(ObjCase.Id);
        List<CaseMilestone> lst02 = objCSQuerys.QueryCaseMilestoneLst(idsCase_lst);
        
        List<ISSM_TypificationMatrix__c> lst03 = objCSQuerys.QueryTypificationMatrix(tM1.Id);
        List<ISSM_TypificationMatrix__c> lst04 = objCSQuerys.QueryTypificationMatrixBySet(idsMatTyp_set);
        
        List<ISSM_TypificationMatrix__c> lst05 = objCSQuerys.QueryTypificationMatrixAllLevels(tm1.ISSM_TypificationLevel1__c, tm1.ISSM_TypificationLevel2__c, tm1.ISSM_TypificationLevel3__c, tm1.ISSM_TypificationLevel4__c, null, null);
        List<ISSM_TypificationMatrix__c> lst06 = objCSQuerys.QueryTypificationMatrixAllLevelsByChannel(tm1.ISSM_TypificationLevel1__c, tm1.ISSM_TypificationLevel2__c, null);
        
        ISSM_TypificationMatrix__c lst07 = objCSQuerys.QueryTypificationMatrixLimit(tM1.Id);
        List<SObject> lst08 = objCSQuerys.QueryTypificationMatrixVF();
        List<Case> lst09 = objCSQuerys.QueryCase(objCase.Id);
        
        List<Case> lst10 = objCSQuerys.QueryCaseLst(caseForce_lst);
        List<Case> lst11 = objCSQuerys.QueryCaseSet(caseForce_set);
        
        List<Case> lst12 = objCSQuerys.QueryCaseSetStr(idsCase_set);
        List<Case> lst13 = objCSQuerys.QueryCaseSetStrIN(idsCase_set);
        
        Case lst14 = objCSQuerys.QueryCaseCaseNumber(objCase.Id);
        List<ONTAP__Case_Force_Comment__c> lst15 = objCSQuerys.QueryCaseForceComment(lstIdsCaseComment);
        
        List<ISSM_MappingFieldCase__c> lst16 = objCSQuerys.QueryMappingFieldCase();
        List<ISSM_AppSetting_cs__c> lst17 = objCSQuerys.QueryAppSettings();
        ISSM_AppSetting_cs__c lst18 = objCSQuerys.QueryAppSettingsObj();
        List<ISSM_AppSetting_cs__c> lst19 = objCSQuerys.QueryAppSettingsList();
        
        Integer lst20 = objCSQuerys.getAccountNumberCampaing();
        Account lst21 = objCSQuerys.QueryAccount(objAccount.Id);
        
        List<Group> lst100 = objCSQuerys.QueryGroup(matTyp_lst);
        List<Account> lst101 = objCSQuerys.QueryAccountList(RecordTypeAccountId, 'Mantenimiento', 'Cooler sin garantía (Flat fee)', lstAccountsSalesOffice);
        
        List<Account> lst22 = objCSQuerys.QueryAccountSalesOffice(case_lst);
        Account lst23 = objCSQuerys.QueryAccounIdInObj(account_lst);
        List<Account> lst24 = objCSQuerys.QueryAccounIdInLst(account_lst);
        
        List<Account> lst102 = objCSQuerys.QueryAccounSalesOffice(lstSalesOffId, RecordTypeSalesOfficetId);
        List<Account> lst103 = objCSQuerys.QueryAccounSalesOrganization(lstSalesOgId,RecordTypeSalesOrg);
        
        // Guardamos el parentId de las oficinas d eventa
        Set<String> salesOfficeParentIdSet = new Set<String>();
        salesOfficeParentIdSet = new ISSM_Account_cls().getSetSpecificField(lst102, 'ParentId');
        List<Account> lst104 = objCSQuerys.QueryAccounSOParentId(salesOfficeParentIdSet, RecordTypeSalesOrg);
        
        List<ONTAP__Case_Force__c> lst25 = objCSQuerys.QueryCaseForce(caseForce_set);
        List<ONCALL__Call__c> lst26 = objCSQuerys.QueryOncallCall(ObjCall.Id);
        ONCALL__Call__c lst27 = objCSQuerys.QueryOncallCallOrderBy(ObjCall.Id);
        
        List<User> lst28 = objCSQuerys.QueryUser(acc_lst);
        List<ISSM_ColonyxAgency__c> lst29 = objCSQuerys.QueryColonyxAgency(idsColonyAccount_set);
        
        List<AccountTeamMember> lst30 = objCSQuerys.QueryAccountTeamMember(System.label.ISSM_TeamMemberRole_Supervisor, idsSalesOffice_set);
        List<AccountTeamMember> lst31 = objCSQuerys.QueryAccountTeamMemberObj(System.label.ISSM_TeamMemberRole_Supervisor, objAccount.Id);
        
        Profile lst32 = objCSQuerys.QueryProfile();
        
        List<String> documentTypeOrderI_lst = new List<String>();
        for (ISS_DocumentTypeOpenItems__c objDocumentTypeOrderI : [SELECT Id, Name FROM ISS_DocumentTypeOpenItems__c WHERE ISSM_Active__c = true]) { documentTypeOrderI_lst.add(objDocumentTypeOrderI.Name); }
        Set<ID> idsAccounts_set = new ISSM_Account_cls().getMapAccountId((List<Account>)acc_lst).keySet();
        List<ISSM_OpenItemB__c> lst105 = objCSQuerys.getOpenItems(idsAccounts_set, documentTypeOrderI_lst);
        
        Map<ID, ISSM_OpenItemB__c> lst106 = objCSQuerys.getOpenItemsById(idOpenItemSet);
        List<Attachment> lst107 = objCSQuerys.QueryAttachment(setAttcQuery);

        Test.stopTest();
    }
}