@isTest
public class ISSM_AttachmentTgr_tst {
	@testsetup
    private static void setData(){
        Id prospectRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Prospect'].Id;
        Account acc = new Account(Name='TestName', recordtypeid=prospectRecordTypeId, ONTAP__Negotiation_Status__c='En revisión de documentos');
        insert acc;
        acc = new Account(Name='TestName', recordtypeid=prospectRecordTypeId, ONTAP__Negotiation_Status__c='Capturado');
        insert acc;
    }
    
    private static testmethod void testAttachments() {
        Telecollection_Campaign__c tc = new Telecollection_Campaign__c();
        tc.Name = 'Test Tc';
        insert tc;
        
        Account acc1 = new Account();
        acc1.Name = 'Test Acc 1';
        acc1.ONTAP__SAP_Number__c = '12345';
        insert acc1;
        
        Date dt = date.today();
        ISSM_OpenItemB__c op = new ISSM_OpenItemB__c();
        op.ISSM_Account__c = acc1.Id;
        op.ISSM_Amounts__c = 100;
        op.ISSM_DueDate__c = dt.addDays(-30);
        op.ISSM_Debit_Credit__c = 'S';
        insert op;
        
        ISSM_ParametersOpenItem__c paramOP = new ISSM_ParametersOpenItem__c();
        paramOP.Name = 'TestParamOP';
        paramOP.ISSM_RangoMinimoAceptado__c = 8;
        paramOP.ISSM_RangoMaximoAceptado__c = 90;
        paramOP.ISSM_SaldoVencidoMayorA__c = 1;
        paramOP.ISSM_Active__c = true;
        insert paramOP;
        
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@testtest.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@testtest.com');
        insert user1;
        
        ISSM_AppSetting_cs__c appSett = new ISSM_AppSetting_cs__c();
        appSett.ISSM_IdQueueTelecolletion__c = user1.Id;
        insert appSett;
        
        Attachment attchTC = new Attachment(ParentId = tc.Id,Name = 'test.txt', Body = Blob.valueOf('12345'), Description='test desc');
        insert attchTC;
    }
    
    private static testmethod void testAttachments2() {
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@testtest.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@testtest.com');
        insert user1;
        
        ISSM_AppSetting_cs__c appSett = new ISSM_AppSetting_cs__c();
        appSett.ISSM_IdQueueTelecolletion__c = user1.Id;
        insert appSett;
        
        Account acc = getAccountEnNegociacion();
        Attachment attch = new Attachment(ParentId = acc.Id,Name = 'test.txt', Body = Blob.valueOf('testbody'), Description='test desc');
        insert attch;
        
        acc = getAccountEnRevisionDoc();
        attch = new Attachment(ParentId = acc.Id,Name = 'test.txt', Body = Blob.valueOf('testbody'), Description='test desc');
        insert attch;
    }
    
    private static Account getAccountEnNegociacion(){
        return [SELECT Id, Name FROM Account where ONTAP__Negotiation_Status__c = 'Capturado'];
    }
    private static Account getAccountEnRevisionDoc(){
        return [SELECT Id, Name FROM Account where ONTAP__Negotiation_Status__c = 'En revisión de documentos'];
    }
}