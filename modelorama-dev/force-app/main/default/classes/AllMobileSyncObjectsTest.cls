/**
 * Test class AllMobileSyncObjectsClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileSyncObjectsTest {

	/**
	 * Test method to test calloutGetAllRouteAppVersion.
	 */
	static testMethod void testCalloutGetAllRouteAppVersionFromHeroku() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseGetBody = AllMobileSyncObjectsClass.calloutGetAllRouteAppVersionFromHeroku();

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutReadAllVersionFromHeroku.
	 */
	static testMethod void testCalloutReadAllVersionFromHeroku() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseGetBody = AllMobileSyncObjectsClass.calloutReadAllVersionFromHeroku();

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutReadAllApplicationFromHeroku.
	 */
	static testMethod void testCalloutReadAllApplicationFromHeroku() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseGetBody = AllMobileSyncObjectsClass.calloutReadAllApplicationFromHeroku();

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutReadAllDeviceUserFromHeroku.
	 */
	static testMethod void testCalloutGetAllDeviceUserFromHeroku() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseGetBody = AllMobileSyncObjectsClass.calloutGetAllDeviceUserFromHeroku();

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutReadAllCatUserTypeFromHeroku.
	 */
	static testMethod void testCalloutGetAllCatUserTypeFromHeroku() {

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseGetBody = AllMobileSyncObjectsClass.calloutGetAllCatUserTypeFromHeroku();

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutInsertRouteAppVersionIntoHeroku.
	 */
	static testMethod void testCalloutInsertRouteAppVersionIntoHeroku() {

		//Create RouteAppVersion Heroku object.
		AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass = AllMobileUtilityHelperTest.createRouteAppVersionHerokuObjectClass(3, 'FG0003', 'VENTA+_(V1.0.0)_02_03_PRO', Date.newInstance(2030, 07, 15), Date.newInstance(2098, 10, 20), false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutInsertRouteAppVersionIntoHeroku(objAllMobileRouteAppVersionHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutUpdateRouteAppVersionIntoHeroku.
	 */
	static testMethod void testCalloutUpdateRouteAppVersionIntoHeroku() {

		//Create RouteAppVersion Heroku object.
		AllMobileSyncObjectsClass.AllMobileRouteAppVersionHerokuObjectClass objAllMobileRouteAppVersionHerokuObjectClass = AllMobileUtilityHelperTest.createRouteAppVersionHerokuObjectClass(3, 'FG0003', 'VENTA+_(V1.0.0)_02_03_PRO', Date.newInstance(2030, 07, 15), Date.newInstance(2098, 10, 20), false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutUpdateRouteAppVersionIntoHeroku(objAllMobileRouteAppVersionHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutInsertVersionIntoHeroku.
	 */
	static testMethod void testCalloutInsertVersionIntoHeroku() {

		//Create Version Heroku object.
		AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass = AllMobileUtilityHelperTest.createVersionHerokuObjectClass('VENTA+_(V3.0.0)_02_03_PRO', 1, false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutInsertVersionIntoHeroku(objAllMobileVersionHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutUpdateVersionIntoHeroku.
	 */
	static testMethod void testCalloutUpdateVersionIntoHeroku() {

		//Create Version Heroku object.
		AllMobileSyncObjectsClass.AllMobileVersionHerokuObjectClass objAllMobileVersionHerokuObjectClass = AllMobileUtilityHelperTest.createVersionHerokuObjectClass('VENTA+_(V3.0.0)_02_03_PRO', 1, false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutUpdateVersionIntoHeroku(objAllMobileVersionHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutInsertApplicationIntoHeroku.
	 */
	static testMethod void testCalloutInsertApplicationIntoHeroku() {

		//Create Application Heroku object.
		AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass = AllMobileUtilityHelperTest.createApplicationHerokuObjectClass(1, 'PREVENTA', '1', 1, false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutInsertApplicationIntoHeroku(objAllMobileApplicationHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutUpdateApplicationIntoHeroku.
	 */
	static testMethod void testCalloutUpdateApplicationIntoHeroku() {

		//Create Application Heroku object.
		AllMobileSyncObjectsClass.AllMobileApplicationHerokuObjectClass objAllMobileApplicationHerokuObjectClass = AllMobileUtilityHelperTest.createApplicationHerokuObjectClass(1, 'PREVENTA', '1', 1, false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutUpdateApplicationIntoHeroku(objAllMobileApplicationHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutInsertDeviceUserIntoHeroku.
	 */
	static testMethod void testCalloutInsertDeviceUserIntoHeroku() {

		//Create Application Heroku object.
		AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass = AllMobileUtilityHelperTest.createDeviceUserHerokuObjectClass('100', 'ALL', 'FG00', 'Griselda Gómez', true, 'FG0001', 'gris@jessy.com', '345621', 'aslkjkjh435MAASD', false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutInsertDeviceUserIntoHeroku(objAllMobileDeviceUserHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutUpdateDeviceUserIntoHeroku.
	 */
	static testMethod void testCalloutUpdateDeviceUserIntoHeroku() {

		//Create Device User Heroku object.
		AllMobileSyncObjectsClass.AllMobileDeviceUserHerokuObjectClass objAllMobileDeviceUserHerokuObjectClass = AllMobileUtilityHelperTest.createDeviceUserHerokuObjectClass('100', 'ALL', 'FG00', 'Griselda Gómez', true, 'FG0001', 'gris@jessy.com', '345621', 'aslkjkjh435MAASD', false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutUpdateDeviceUserIntoHeroku(objAllMobileDeviceUserHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutInsertCatUserTypeIntoHeroku.
	 */
	static testMethod void testCalloutInsertCatUserTypeIntoHeroku() {

		//Create CatUserType Heroku object.
		AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass = AllMobileUtilityHelperTest.createCatUserTypeHerokuObjectClass('ALL', 'Almacén lleno', true, '1000000000', false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutInsertCatUserTypeIntoHeroku(objAllMobileCatUserTypeHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test calloutUpdateCatUserTypeIntoHeroku.
	 */
	static testMethod void testCalloutUpdateCatUserTypeIntoHeroku() {

		//Create Device User Heroku object.
		AllMobileSyncObjectsClass.AllMobileCatUserTypeHerokuObjectClass objAllMobileCatUserTypeHerokuObjectClass = AllMobileUtilityHelperTest.createCatUserTypeHerokuObjectClass('ALL', 'Almacén lleno', true, '1000000000', false, DateTime.newInstance(2018, 12, 24, 12, 45, 39));

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.calloutUpdateCatUserTypeIntoHeroku(objAllMobileCatUserTypeHerokuObjectClass);

		//Stop test.
		Test.stopTest();
	}

	/**
	 * Test method to test encryptPasswordDeviceUser.
	 */
	static testMethod void testEncryptPasswordDeviceUser() {

		//Create Device User Heroku object.
		String strPasswordClearText = 'Memento1';

		//Insert Custom Settings (AllMobile Endpoints).
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();

		//Set Mock HttpResponse.
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();
		String strHttpResponseStatusCode = AllMobileSyncObjectsClass.encryptPasswordDeviceUser(strPasswordClearText);

		//Stop test.
		Test.stopTest();
	}
}