/******************************************************************************* 
* Developed by	: 	Avanxo México
* Author				: 	Oscar Alvarez
* Project			: 	AbInbev - Trade Revenue Management
* Description		: 	Schedulable for integration with mulesoft and sending the combo to SAP. The schedule has the scope of MODIFICATION
*						Run every day at 11:30:00 p.m.
*
* No.       Date              Author                      Description
* 1.0    22-Junio-2018      Oscar Alvarez                   creation
*******************************************************************************/
global class ISSM_IntegrateCmbModifyMulsoft_sch implements Schedulable {
    
    @TestVisible
    public static final String CRON_EXPR = Label.TRM_CronExprIntegrateCmbModify;
    /*
    * Method Name	: executeScheduleModify  
    * creation      : Call this from Anonymous Apex to schedule at the default regularity
    */
    global static String executeScheduleModify() {
        ISSM_IntegrateCmbModifyMulsoft_sch job = new ISSM_IntegrateCmbModifyMulsoft_sch();
        return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchApexIntgrCmbModify + System.now()) : Label.TRM_NameSchApexIntgrCmbModify), CRON_EXPR, job);
    }
    global void execute(SchedulableContext sc) {
        List<String> lstIdCombos = new List<String>();
        List<ISSM_Combos__c> lstModifiedCmbs = [SELECT Id FROM ISSM_Combos__c WHERE ISSM_SynchronizedWithSAP__c = false AND ISSM_ModifiedCombo__c = true AND (ISSM_StatusCombo__c = 'ISSM_Approved'  OR ISSM_StatusCombo__c = 'ISSM_SyncFail')];

        for(ISSM_Combos__c combo : lstModifiedCmbs) lstIdCombos.add(combo.Id);
        //llamado al batch
        if(lstIdCombos.size() > 0) Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstIdCombos,Label.TRM_OperationModify), 1000);
    }
}