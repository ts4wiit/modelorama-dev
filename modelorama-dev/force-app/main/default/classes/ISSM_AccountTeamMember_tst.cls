@isTest
private class ISSM_AccountTeamMember_tst {
    static Account dataAccount;
    static AccountTeamMember accountTeamMember;
    static User user;

    @isTest static void testGetUserIdForAccountTeamMember() {
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_AccountTeamMember_cls.SUPERVISOR, user.Id);
        Test.startTest();
        ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
        List<Id> ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_AccountTeamMember_cls.SUPERVISOR, dataAccount.Id);
        System.assert( ids != null,'Ids is not null');
        System.assertEquals(ids.size(),1);
        System.assertEquals(ids.get(0),user.Id);
        Test.stopTest();
    }
    
}