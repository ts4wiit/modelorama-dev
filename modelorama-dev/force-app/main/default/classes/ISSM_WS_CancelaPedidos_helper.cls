/**************************************************************************************
Nombre de la Clase Apex: ISSM_WS_CancelaPedidos_helper
Versión : 1.0
Fecha de Creación : 21 Mayo 2018
Funcionalidad : Clase helper de la clase de apex ISSM_WSCancelaPedidos
Historial de Modificaciones:
Clase de Prueba: ISSM_WSCancelaPedidos_tst
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        21 - Mayo - 2018      Versión Original
  Hectro Diaz            21 - Junio - 2018     Unificacion de codigo
*************************************************************************************/
public class ISSM_WS_CancelaPedidos_helper {

    public ISSM_WS_CancelaPedidos_helper() { }
    
    // Update status order to canceled when the order's cancellations thats correct -- Callout with SAP
    public static Boolean UpdateStatusOrder(List<ONTAP__Order__c> order_lst, String message_str, String label_str) {
    	System.debug('*********************** HMDH 3 *************************** ');
        try {
            List<ONTAP__Order__c> Orders_lst = new List<ONTAP__Order__c>();
            List<ONTAP__Order__c> UpdOrders_lst = new List<ONTAP__Order__c>();
            for (ONTAP__Order__c order : order_lst) {
                order.OnCall_SAPOrderCancellationResult__c = message_str;
                order.ONTAP__OrderStatus__c = label_str;     
                // ; 
                UpdOrders_lst.add(order);
            }
            
            if (UpdOrders_lst.size() > 0) {
                update UpdOrders_lst;
                return true;
            } else { 
            	return false; 
            }
        } catch (Exception ex) { 
        	System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' && ' + 'LINE NUMBER: ' + ex.getLineNumber());
        	 return false; 
      	}
    }

    // Update status order to canceled when the order's cancellations to emergency thats correct -- Change directly in Salesforce
  /* public static Boolean UpdateStatusOrderEmergency(List<ONTAP__Order__c> order_lst) {
    	System.debug('*********************** HMDH 4 *************************** ');
        try {
            List<ONTAP__Order__c> Orders_lst = new List<ONTAP__Order__c>();
            List<ONTAP__Order__c> UpdOrders_lst = new List<ONTAP__Order__c>();
			if(order_lst != null && !order_lst.isEmpty()){
				for (ONTAP__Order__c order : order_lst) {
                order.OnCall_SAPOrderCancellationResult__c = 'El pedido de emergencia fue cancelado con éxito.';   
                order.ONTAP__OrderStatus__c = System.label.ISSM_Canceled;   
                     
                UpdOrders_lst.add(order);
            	}
           	}
            if (UpdOrders_lst.size() > 0) {
                Database.update (UpdOrders_lst);
                return true;    
            } else { 
            	return false; 
            }
        } catch (Exception ex) { 
        	System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' && ' + 'LINE NUMBER: ' + ex.getLineNumber()); 
        	return false; 
        }
    }

    // Save error when the order's cancellation isn't correct -- Callout with SAP
    public static Boolean ProcessErrorToCancelOrder(List<ONTAP__Order__c> order_lst, String message_str) {
    	System.debug('*********************** HMDH 5 *************************** ');
        try {
            List<ONTAP__Order__c> Orders_lst = new List<ONTAP__Order__c>();
            List<ONTAP__Order__c> UpdOrders_lst = new List<ONTAP__Order__c>();
            
            for (ONTAP__Order__c order : order_lst) {
               order.OnCall_SAPOrderCancellationResult__c = message_str;
               order.ONTAP__OrderStatus__c = System.label.ISSM_OrderStatusClosed;   	
                UpdOrders_lst.add(order);
            }
            
            if (UpdOrders_lst.size() > 0) {
                Database.update (UpdOrders_lst);
                return false;
            } else { 
            	return true; 
            }
        } catch (Exception ex) { 
        	System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' && ' + 'LINE NUMBER: ' + ex.getLineNumber()); 
        	return false; 
        }
    }*/
}