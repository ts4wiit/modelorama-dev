/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Batch Class for generation of conditión records

    Information about changes (versions)
    ===============================================================================
    No.    Date             	Author                      Description
    1.0    06-September-2018    Luis Licona                 Creation
    ******************************************************************************* */
global class TRM_GenerateConditions_bch implements Database.Batchable<sObject>,Database.AllowsCallouts{
	
	global TRM_ConditionRecord__c[] lstConditions = new List<TRM_ConditionRecord__c>();
	global ONTAP__Product__c[] lstTotalProducts   = new List<ONTAP__Product__c>();
	global ONTAP__Product__c[] lstProductsProcess = new List<ONTAP__Product__c>();
	global Map<String,String> mapStructures 	  = new Map<String,String>();
	global Map<String,String> mapCatalogs 		  = new Map<String,String>();
	global Account[] lstCustomer 				  = new List<Account>();
	global TRM_ConditionClass__c condClssRec 	  = new TRM_ConditionClass__c();
	global TRM_ConditionRelationships__mdt mtdRelation = new TRM_ConditionRelationships__mdt();
	global TRM_ConditionsManagement__mdt mtdMngRecord = new TRM_ConditionsManagement__mdt();
	global Boolean byRecursiveMethod;
	global TRM_ProductScales__c[] lstScales = new List<TRM_ProductScales__c>();
	
	/**
    * @description  Constructor method: A validation is generated to calculate the total of records that will be generated, 
    *                                   if the total reads the 100,000 records, then the batch becomes recursive for each 
    *                                   cycle less than 100,000 records are sent
    *
    * @param    lstProducts          List of selected products
    * @param    lstConditionScales   List of scales by material selected
    * @param    mapStructures        Map with sales structures (Office, Organization)
    * @param    mapCatalogs          Map with segment catalog and Zone
    * @param    lstCustomer          List of selected Customers
    * @param    condClssRec          Condition Class Registration
    * @param    mtdRelation          Metadata indicating that fields are full
    * @param    mtdMngRecord         Metadata indicating the type of condition
    */
	global TRM_GenerateConditions_bch(ONTAP__Product__c[] products, 
                                       Map<String,String> structures,
                                       Map<String,String> catalogs,
                                       Account[] customer,
                                       TRM_ConditionClass__c clssRec,
                                       TRM_ConditionRelationships__mdt relation,
                                       TRM_ConditionsManagement__mdt mngRecord,
                                       TRM_ProductScales__c[] lstScals) {

		TRM_ConditionClass__c tmpClssRec = [SELECT Id,Name,TRM_ReadyApproval__c,TRM_AccessSequence__c
						                          ,TRM_ConditionClass__c,TRM_ConditionGroup3__c
						                          ,TRM_ConditionUnit__c,TRM_DLCGpo__c,TRM_Sector__c
						                          ,TRM_DistributionChannel__c,TRM_ExternalId__c
						                          ,TRM_FieldSetApiName__c,TRM_PriceList__c
						                          ,TRM_PriceZoneOnly__c,TRM_Quota__c
						                          ,TRM_SalesOfficeOnly__c,TRM_SalesOffice__c
						                          ,TRM_SalesOrg__c,TRM_Segment__c
						                          ,TRM_StatePerZone__c,TRM_Status__c
						                          ,TRM_TotalConditionRecords__c
                                            	  ,TRM_Utilization__c,TRM_Continent__c
						                    FROM  TRM_ConditionClass__c 
						                    WHERE Id =: clssRec.Id][0];

		tmpClssRec.TRM_ReadyApproval__c = clssRec.TRM_ReadyApproval__c;
		mapStructures 	= structures;
		mapCatalogs 	= catalogs;
		lstCustomer  	= customer;
		condClssRec 	= tmpClssRec;
		mtdRelation 	= relation;
		mtdMngRecord 	= mngRecord;
		lstScales		= lstScals;

		String strGenerateBy = mngRecord.TRM_GenerateBy__c;
		String[] lstPriZones = (String.isBlank(clssRec.TRM_StatePerZone__c)) ? null : clssRec.TRM_StatePerZone__c.split(';');
        String[] lstSalesOff = (String.isBlank(clssRec.TRM_SalesOffice__c)) ? null : clssRec.TRM_SalesOffice__c.split(';');
        String[] lstSegments = (String.isBlank(clssRec.TRM_Segment__c)) ? null : clssRec.TRM_Segment__c.split(';');
        Integer numPriZones  = lstPriZones == null ? 0 :lstPriZones.size();
        Integer numSalesOff  = lstSalesOff == null ? 0 :lstSalesOff.size();
        Integer numSegments  = lstSegments == null ? 0 :lstSegments.size();       
        Integer numConditionRecordBatch = Test.isRunningTest() ? 2 :Integer.valueOf(Label.TRM_NumConditionRecordBatch);

        if((strGenerateBy == 'TRM_SearchByCustomer' 	 && (products.size() * customer.size())           >= numConditionRecordBatch) 
		 ||(strGenerateBy == 'TRM_SearchBySegmentOffice' && (products.size() * numSalesOff * numSegments) >= numConditionRecordBatch) 
		 ||(strGenerateBy == 'TRM_SearchBySegmentZone' 	 && (products.size() * numPriZones * numSegments) >= numConditionRecordBatch)){

	        ONTAP__Product__c[] newlstTotalProducts  = new List<ONTAP__Product__c>();
	        Integer numOfProductsInBatch = Test.isRunningTest() ? 1 : Integer.valueOf(Label.TRM_NumOfProductsInBatch);

	        for(Integer i = 0; i < products.size(); i++){        	
	        	if(i <  numOfProductsInBatch) lstProductsProcess.add(products[i]);        	
	        	if(i >= numOfProductsInBatch) newlstTotalProducts.add(products[i]);
	        }

	        lstTotalProducts = newlstTotalProducts;
	        byRecursiveMethod = true;

    	}else{
	    	lstTotalProducts = products;
	    	byRecursiveMethod = false;
    	}
	}

	/**
    * @description  Start method batch
    */
	global TRM_ConditionRecord__c[] start(Database.BatchableContext context) {
		System.debug('####################START BATCH: TRM_GenerateConditions_bch####################');
		ONTAP__Product__c[] lstProds = new List<ONTAP__Product__c>();
		lstProds = (byRecursiveMethod) ? lstProductsProcess : lstTotalProducts; 
		
		lstConditions = TRM_ConditionDetail_ctr.generatorOfConditions(lstProds,
																	  mapStructures,
																	  mapCatalogs,
																	  lstCustomer,
																	  condClssRec,
																	  mtdRelation,
																	  mtdMngRecord);

		return lstConditions;
	}

	/**
    * @description  Execute method batch for generate conditions 
    */
   	global void execute(Database.BatchableContext BC, List<TRM_ConditionRecord__c> lstCond) {
		System.debug('####################EXECUTE BATCH: TRM_GenerateConditions_bch#################### ');
   		upsert lstCond TRM_DummyKey__c;

   		TRM_ProductScales__c[] lstScales2 = new List<TRM_ProductScales__c>();
   		for(TRM_ConditionRecord__c objCondRec :lstCond){
			for(TRM_ProductScales__c objSc : lstScales){
				if(objCondRec.TRM_Product__c == objSc.TRM_ConditionRecord__c){
					TRM_ConditionRecord__c objRef 	= new TRM_ConditionRecord__c(TRM_DummyKey__c=objCondRec.TRM_DummyKey__c);   
					TRM_ProductScales__c objS 		= new TRM_ProductScales__c();
					objS.TRM_ConditionRecord__r 	= objRef;
					objS.TRM_AmountPercentage__c 	= objSc.TRM_AmountPercentage__c;
					objS.TRM_Quantity__c 			= objSc.TRM_Quantity__c;
					lstScales2.add(objS);
				}
			}
		}

		if(lstScales2.size()>0){
			insert lstScales2;
		}
	}
	
	/**
    * @description  Finish method batch: Validate that it is the last cycle of the batch to end or not recurcivity.
    *                                    In case it has been the last cycle, it sends to approval the class condition if and only if it is PPM Corporate.
    */
	global void finish(Database.BatchableContext BC) {
		System.debug('####################FINISH BATCH: TRM_GenerateConditions_bch####################');
		System.debug('lstTotalProducts.size(): ' + lstTotalProducts.size());
		System.debug('byRecursiveMethod ' + byRecursiveMethod);
		if (lstTotalProducts.size() > 0 && byRecursiveMethod){
			Database.executeBatch(new TRM_GenerateConditions_bch(lstTotalProducts,
																mapStructures,
																mapCatalogs,
																lstCustomer,
																condClssRec,
																mtdRelation,
																mtdMngRecord,
																lstScales));
		}else{
			List<TRM_ConditionClass__c> listCondClssRec = new List<TRM_ConditionClass__c>();
			Id userId = UserInfo.getUserId();
			User[] lst = [SELECT ManagerId FROM User WHERE Id =: userId];
			
			if(String.isBlank(lst[0].ManagerId) && condClssRec.TRM_ReadyApproval__c){
	            condClssRec.TRM_Status__c = 'TRM_Approved';
	            listCondClssRec.add(condClssRec);
	            TRM_SendClassConditionToSAP_cls.sendClassConditionToSAP(listCondClssRec);
	            TRM_ConditionClass__c[] lstConditionClass = [SELECT TRM_Status__c,TRM_ReadyApproval__c 
	                                                         FROM 	TRM_ConditionClass__c 
	                                                         WHERE 	Id =: condClssRec.Id];

	            lstConditionClass[0].TRM_Status__c ='TRM_Approved';
	            lstConditionClass[0].TRM_ReadyApproval__c = true;          
	            update lstConditionClass;
	        }
		}
	}
}