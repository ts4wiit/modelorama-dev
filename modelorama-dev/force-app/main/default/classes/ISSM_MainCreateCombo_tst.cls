@isTest
public class ISSM_MainCreateCombo_tst {
    
    @testSetup
    static void setupData(){
        //Create MDM Parameters (GrupoMateriales1)
        MDM_Parameter__c cupo01 = new MDM_Parameter__c();
        cupo01.Name				=		'Familiar';
        cupo01.Code__c			=		'01';
        cupo01.Description__c	=		'Familiar';
        cupo01.Catalog__c		=		'GrupoMateriales1';
        cupo01.Active__c		=		true;
        cupo01.Active_Revenue__c =		true;
        Insert cupo01;
        
        //Create an Account record with RecordType Sales Regional Office 
        Account DRV01 = new Account();
        DRV01.Name							=		'DRV 01';
        DRV01.ONTAP__SAPCustomerId__c       =		'GM00001';
        DRV01.RecordTypeId					=		Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_RegionalSalesDivision').getRecordTypeId();
        Insert DRV01;
        
        //Create an Account record with RecordType Sales Org
        Account ORG01 = new Account();
        ORG01.Name							=		'ORG 01';
        ORG01.ONTAP__SAPCustomerId__c		=		'3111';
        ORG01.RecordTypeId					=		Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOrg').getRecordTypeId();
        ORG01.ISSM_ParentAccount__c			=		DRV01.Id;
        Insert ORG01;
        
        //Create an Account record with RecordType Sales Office
        Account OFF01 = new Account();
        OFF01.Name							=		'OFF 01';
        OFF01.ONTAP__SAPCustomerId__c		=		'FP01';
        OFF01.RecordTypeId					=		Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        OFF01.ISSM_ParentAccount__c			=		ORG01.Id;
        Insert OFF01;

        User ppmLocal = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Price Promotion Manager' LIMIT 1].Id,
            LastName = 'Price Promotion Manager',
            Email = 'ppmlocal@abi.com',
            Username = 'ppmlocal@abi.com',
            CompanyName = 'ABI',
            Title = 'PPM Local',
            Alias = 'ppmLocal',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        insert ppmLocal;
        
        User ppmRegional01 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Price Promotion Manager' LIMIT 1].Id,
            LastName = 'Price Promotion Manager Regional',
            Email = 'ppmregional01@abi.com',
            Username = 'ppmregional01@abi.com',
            CompanyName = 'ABI',
            Title = 'PPM Regional',
            Alias = 'region01',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        insert ppmRegional01;
        
        User ppmLocal02 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Price Promotion Manager' LIMIT 1].Id,
            LastName = 'Price Promotion Manager Local',
            Email = 'ppmlocal02@abi.com',
            Username = 'ppmlocal02@abi.com',
            CompanyName = 'ABI',
            Title = 'PPM Local 02',
            Alias = 'local02',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = ppmRegional01.Id
        );
        insert ppmLocal02;
    }
    
    static testMethod void testSaveComboRelationships(){
        //get Regional Sales Office record
        Account drv01 = [SELECT Id FROM Account WHERE Name = 'DRV 01' LIMIT 1];
        //get Sales Org record
        Account org01 = [SELECT Id FROM Account WHERE Name = 'ORG 01' LIMIT 1];
        //get Sales Office record
        Account off01 = [SELECT Id FROM Account WHERE Name = 'OFF 01' LIMIT 1];
        
        MDM_Parameter__c cupo01 = [SELECT Id FROM MDM_Parameter__c WHERE Name = 'Familiar' AND Code__c = '01' AND Catalog__c = 'GrupoMateriales1'];
        
        //Create a Combo Limit record, level ISSM_SalesOffice
        ISSM_ComboLimit__c comboLimit01 = new ISSM_ComboLimit__c();
        comboLimit01.ISSM_ComboLevel__c		=		'ISSM_SalesOffice';
        comboLimit01.ISSM_SalesStructure__c	=		off01.Id;
        comboLimit01.ISSM_AllowedCombos__c	=		10;
        comboLimit01.ISSM_IsActive__c		=		true;
        comboLimit01.RecordTypeId			=		Schema.getGlobalDescribe().get('ISSM_ComboLimit__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_LimitComboMx').getRecordTypeId();
        Insert comboLimit01;
        
        ISSM_Combos__c combo01 = new ISSM_Combos__c();
        combo01.ISSM_StartDate__c 			= 		Date.today() + 2;
        combo01.ISSM_EndDate__c				=		Date.today() + 30;
        combo01.ISSM_LongDescription__c		=		'Long description for the new combo';
        combo01.ISSM_ShortDescription__c	=		'ShortDescr';
        combo01.ISSM_NumberSalesOffice__c	=		10;
        combo01.ISSM_NumberByCustomer__c	=		1;
        combo01.ISSM_Currency__c			=		'MXN';
        combo01.ISSM_ComboLimit__c			=		comboLimit01.Id;
        combo01.ISSM_TypeApplication__c		=		'ALLMOBILE;TELESALES;B2B;BDR';
        combo01.RecordTypeId				=		Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        Insert combo01;
        
        Test.startTest();
        //Get Parent Sales Structure for the Sales Office off01
        ISSM_MainCreateCombo_ctr.SalesStructure salesStructure = ISSM_MainCreateCombo_ctr.getParentSalesStructure(off01.Id);
        System.assertNotEquals(null, salesStructure.parentId);
        System.assertNotEquals(null, salesStructure.grandParentId);
        
        //Get the record type id by developer name
        String ISSM_ComboByCustomerMx = ISSM_MainCreateCombo_ctr.getRecordTypeIdByDeveloperName('ISSM_ComboByAccount__c', 'ISSM_ComboByCustomerMx');
        String ISSM_ComboByCustomerMX_describe = Schema.getGlobalDescribe().get('ISSM_ComboByAccount__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboByCustomerMx').getRecordTypeId();
        System.assertEquals(ISSM_ComboByCustomerMX_describe, ISSM_ComboByCustomerMx);
        
        List<ISSM_ComboByAccount__c> comboByAccList = new List<ISSM_ComboByAccount__c>();
        ISSM_ComboByAccount__c comboByAcc = new ISSM_ComboByAccount__c();
        comboByAcc.ISSM_ComboNumber__c		=		combo01.Id;
        comboByAcc.ISSM_RegionalSalesDirection__c = drv01.Id;
        comboByAcc.ISSM_SalesOrganization__c	  = org01.Id;
        comboByAcc.ISSM_SalesOffice__c			  = off01.Id;
        comboByAcc.RecordTypeId					  = ISSM_ComboByCustomerMx;
        comboByAccList.add(comboByAcc);
        
        ISSM_ComboByAccount__c comboByAcc02 = new ISSM_ComboByAccount__c();
        comboByAcc02.ISSM_RegionalSalesDirection__c = drv01.Id;
        comboByAcc02.ISSM_SalesOrganization__c	  = org01.Id;
        comboByAcc02.ISSM_SalesOffice__c			  = off01.Id;
        comboByAcc02.RecordTypeId					  = ISSM_ComboByCustomerMx;
        
        //Insert ComboByAccount records
        ISSM_MainCreateCombo_ctr.saveComboByAccount(comboByAccList,combo01.Id);
        List<ISSM_ComboByAccount__c> savedComboByAccList = [SELECT Id FROM ISSM_ComboByAccount__c WHERE ISSM_ComboNumber__c = :combo01.Id ];
        System.assertEquals(1, savedComboByAccList.size());
        
        comboByAccList.add(comboByAcc02);
        ISSM_MainCreateCombo_ctr.saveComboByAccount(comboByAccList,combo01.Id);
        
        //Add combo materials
        List<ISSM_ComboByProduct__c> comboByProdList = new List<ISSM_ComboByProduct__c>();
        String ISSM_ProdByComboMx = ISSM_MainCreateCombo_ctr.getRecordTypeIdByDeveloperName('ISSM_ComboByProduct__c', 'ISSM_ProdByComboMx');
        ISSM_ComboByProduct__c comboByProd = new ISSM_ComboByProduct__c();
        comboByProd.ISSM_ComboNumber__c			=		combo01.Id;
        comboByProd.ISSM_QuantityProduct__c		=		1;
        comboByProd.ISSM_UnitPriceTax__c			=		100;
        comboByProd.ISSM_Quota__c				=		cupo01.Id;
        comboByProd.ISSM_Type__c				=		'Quota';
        comboByProd.RecordTypeId				=		ISSM_ProdByComboMx;
        comboByProdList.add(comboByProd);
        //Insert ComboByProduct records
        ISSM_MainCreateCombo_ctr.saveComboByProductList(comboByProdList);
        List<ISSM_ComboByProduct__c> savedComboByProdList = [SELECT Id FROM ISSM_ComboByProduct__c WHERE ISSM_ComboNumber__c = :combo01.Id];
        System.assertEquals(1, savedComboByProdList.size());
        
        ISSM_ComboByProduct__c comboByProd02 = new ISSM_ComboByProduct__c();
        comboByProd02.ISSM_QuantityProduct__c		=		1;
        comboByProd02.ISSM_UnitPriceTax__c			=		100;
        comboByProd02.ISSM_Quota__c				=		cupo01.Id;
        comboByProd02.ISSM_Type__c				=		'Quota';
        comboByProd02.RecordTypeId				=		ISSM_ProdByComboMx;
        comboByProdList.add(comboByProd02);
        ISSM_MainCreateCombo_ctr.saveComboByProductList(comboByProdList);
        
        //Send for approval
        String processName = 'ISSM_ApprovalCombo';
        User[] ppmLocal = [SELECT Id,ManagerId FROM User WHERE Username = 'ppmlocal02@abi.com' LIMIT 1];
        ISSM_Combos__c[] LstC = [SELECT Id FROM ISSM_Combos__c];
        System.debug('#####ppmLocal '+ppmLocal);
        System.debug('#####LstC '+LstC);
        ISSM_MainCreateCombo_ctr.submitForApproval(LstC[0].Id, 'Send for approval',ppmLocal[0].Id, processName);
        
        Test.stopTest();
    }
    
 //   static testMethod void testValidateManager() {
	//	User PPLOCAL 			= [SELECT Id FROM User WHERE Username = 'ppmlocal@abi.com' LIMIT 1];
	//	Test.startTest();
 //       System.runAs(PPLOCAL){
 //           ISSM_MainCreateCombo_ctr.validateManager();
 //       }
	//	Test.stopTest();
	//}

    static testMethod void testUpdateStatusCombo() {

        //Create an Account record with RecordType Sales Office
        Account OFF02 = new Account();
        OFF02.Name                          =       'OFF 01';
        OFF02.ONTAP__SAPCustomerId__c       =       'FP01';
        OFF02.RecordTypeId                  =       Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        //OFF02.ISSM_ParentAccount__c         =       ORG01.Id;
        Insert OFF02;

        //Create a Combo Limit record, level ISSM_SalesOffice
        ISSM_ComboLimit__c comboLimit01 = new ISSM_ComboLimit__c();
        comboLimit01.ISSM_ComboLevel__c     =       'ISSM_SalesOffice';
        comboLimit01.ISSM_SalesStructure__c =       OFF02.Id;
        comboLimit01.ISSM_AllowedCombos__c  =       11;
        comboLimit01.ISSM_IsActive__c       =       true;
        comboLimit01.RecordTypeId           =       Schema.getGlobalDescribe().get('ISSM_ComboLimit__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_LimitComboMx').getRecordTypeId();
        Insert comboLimit01;
        
        ISSM_Combos__c combo01 = new ISSM_Combos__c();
        combo01.ISSM_StartDate__c           =       Date.today() + 2;
        combo01.ISSM_EndDate__c             =       Date.today() + 30;
        combo01.ISSM_LongDescription__c     =       'Long description for the new combo';
        combo01.ISSM_ShortDescription__c    =       'ShortDescr';
        combo01.ISSM_NumberSalesOffice__c   =       11;
        combo01.ISSM_NumberByCustomer__c    =       1;
        combo01.ISSM_Currency__c            =       'MXN';
        combo01.ISSM_ComboLimit__c          =       comboLimit01.Id;
        combo01.ISSM_TypeApplication__c     =       'ALLMOBILE;TELESALES;B2B;BDR';
        combo01.RecordTypeId                =       Schema.getGlobalDescribe().get('ISSM_Combos__c').getDescribe().getRecordTypeInfosByDeveloperName().get('ISSM_ComboMx').getRecordTypeId();
        Insert combo01;

        Test.startTest();
            ISSM_MainCreateCombo_ctr.updateStatusCombo(combo01.Id);
        Test.stopTest();
    }

}