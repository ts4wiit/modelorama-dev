/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ISSM  AB Int Bev (OnTap)
Description:  
---------------------------------------------------------------------------------
Version     Date      Author                Description
------ ---------- ---------------------------------------------------------------
1.0     10-Julio-2017 Rodrigo RESENDIZ (RR)      Creator.
1.1     25-Julio-2017 Rodrigo RESENDIZ (RR)     Assign case to the specific person.
2.0     03- Oct -2017 Rodrigo RESENDIZ (RR)     Multi category POCE
2.1     03- May -2018 Rodrigo RESENDIZ (RR)     Ignore grouping of POCE if not mapped
***********************************************************************************/

public without sharing class ISSM_SurveyTakenTgrHnd_cls {
    
    private static final String COMPLETED_SURVEY_STATUS = 'Completada';
    private static final String OPEN_SURVEY_STATUS = 'Abierta';
    private static final String CASE_TO_CLASSIFY_STATUS = 'Pending to Classify';
    private static final Decimal PASSING_SCORE;
    private static final Id POCE_SURVEY_RECORD_TYPE_ID; 
    private static final Id CASE_FORCE_RECORD_TYPE_ID; 
    
    static{
        CASE_FORCE_RECORD_TYPE_ID = [select Id from RecordType where developerName='ISSM_SalesForceCase' limit 1].Id;
        POCE_SURVEY_RECORD_TYPE_ID = [select Id from RecordType where developerName='POCE' limit 1].Id; 
        PASSING_SCORE = ISSM_SurveyAssignmentCase__c.getValues('Passing Score').ISSM_Value__c;
    }
    
    public static void afterUpdate(Map<Id,ONTAP__SurveyTaker__c> newMap, Map<Id,ONTAP__SurveyTaker__c> oldMap){
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [START]');
        
        Set<Id> surveyTakerIds_set = new Set<Id>();
        Set<Id> surveyIds_set = new Set<Id>();
        Set<Id> relatedAccountsIds = new Set<Id>();
        Set<String> groupingElements_set = new Set<String>();
        Map<Id,Id> survey_SurveyRT_map = new Map<Id,Id>();
        Integer positionToRemove_int = -1;
        Set<Id> accountIds_set = new Set<Id>();
        Map<Id, Map<String,Id>> teamMember_map = new Map<Id, Map<String,Id>>();
        
        for(ONTAP__SurveyTaker__c surveyTaker_obj : newMap.values()){
            surveyIds_set.add(surveyTaker_obj.ONTAP__Survey__c);
        }
        for(ONTAP__Survey__c survey_obj : [SELECT Id, RecordTypeId FROM ONTAP__Survey__c WHERE Id IN: surveyIds_set]){
            survey_SurveyRT_map.put(survey_obj.Id, survey_obj.RecordTypeId);
        }
        for(ONTAP__SurveyTaker__c surveyTaker_obj : newMap.values()){
            // if the survey is POCE and the new status is "Completada" and the old status was "Abierta" and the total score is not null
            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [survey_SurveyRT_map.get(surveyTaker_obj.ONTAP__Survey__c) = '+survey_SurveyRT_map.get(surveyTaker_obj.ONTAP__Survey__c)+']');
            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [surveyTaker_obj.ONTAP__Status__c = '+surveyTaker_obj.ONTAP__Status__c+']');
            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [oldMap.get(surveyTaker_obj.Id).ONTAP__Status__c = '+oldMap.get(surveyTaker_obj.Id).ONTAP__Status__c+']');
            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [surveyTaker_obj.ONTAP__Total_Score__c = '+surveyTaker_obj.ONTAP__Total_Score__c+']');
            if(survey_SurveyRT_map.get(surveyTaker_obj.ONTAP__Survey__c) == POCE_SURVEY_RECORD_TYPE_ID
               && surveyTaker_obj.ONTAP__Status__c == COMPLETED_SURVEY_STATUS 
               && oldMap.get(surveyTaker_obj.Id).ONTAP__Status__c == OPEN_SURVEY_STATUS
               && surveyTaker_obj.ONTAP__Total_Score__c != null){ 
                   
                   surveyTakerIds_set.add(surveyTaker_obj.Id);
                   relatedAccountsIds.add(surveyTaker_obj.ONTAP__Account__c);
               }
        }
        
        /*fill account set*/
        for(AccountTeamMember teamMember_obj : [SELECT UserId, TeamMemberRole, AccountId FROM AccountTeamMember WHERE AccountId IN: relatedAccountsIds]){
            if(!teamMember_map.containsKey(teamMember_obj.AccountId)){
                teamMember_map.put(teamMember_obj.AccountId, new Map<String,Id>{teamMember_obj.TeamMemberRole.deleteWhitespace() => teamMember_obj.UserId});
            }else{
                teamMember_map.get(teamMember_obj.AccountId).put(teamMember_obj.TeamMemberRole.deleteWhitespace(), teamMember_obj.UserId);
            }
        }
        system.debug('### teamMember_map = '+teamMember_map);
        
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [surveyTakerIds_set.size() = '+surveyTakerIds_set.size()+']');
        
        if(surveyTakerIds_set.size()>0){
            Map<Id,List<ONTAP__SurveyQuestionResponse__c>> surveyResponses_map = new Map<Id,List<ONTAP__SurveyQuestionResponse__c>>();
            List<ONTAP__Case_Force__c> casesToBeCreated_lst = new List<ONTAP__Case_Force__c>();
            List<ONTAP__Case_Force__c> existingOpenCases_lst = new List<ONTAP__Case_Force__c>();
            
            // populated the map with the Id of the SurveyTaker and the List of associated questions
            for(ONTAP__SurveyQuestionResponse__c surveyResponse_obj : [SELECT ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c,ONTAP__Survey_Question__r.ONTAP__POCE_Weight__c, ONTAP__SurveyTaker__r.ONTAP__Account__r.OwnerId, ONTAP__SurveyTaker__r.ONTAP__Account__c, Id, ONTAP__POCE_Score__c,ONTAP__SurveyTaker__c FROM ONTAP__SurveyQuestionResponse__c WHERE ONTAP__SurveyTaker__c IN:surveyTakerIds_set]){
                if(!surveyResponses_map.containsKey(surveyResponse_obj.ONTAP__SurveyTaker__c)){
                    surveyResponses_map.put(surveyResponse_obj.ONTAP__SurveyTaker__c, new List<ONTAP__SurveyQuestionResponse__c>{surveyResponse_obj});
                }else{
                    surveyResponses_map.get(surveyResponse_obj.ONTAP__SurveyTaker__c).add(surveyResponse_obj);
                }
                // add to a set all the groupings included in the survey
                groupingElements_set.add(surveyResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c);
            }
            //iterates over the selected SurveyTaker Ids
            for(Id surveyTaker_id : surveyTakerIds_set){
                //adds the generated cases to a list of cases to be inserted
                casesToBeCreated_lst.addAll(createCasesByGrouping(groupingElements_set,surveyResponses_map.get(surveyTaker_id), teamMember_map));
            }
            //if there are generated cases, the process inserts all those cases
            try{
                for(ONTAP__Case_Force__c caseForce_obj : [SELECT Id, ONTAP__Subject__c, ONTAP__Status__c,ONTAP__Account__c FROM ONTAP__Case_Force__c WHERE ONTAP__Status__c != 'Closed' AND ONTAP__Account__c IN:relatedAccountsIds]){
                    for(Integer i = 0; i < casesToBeCreated_lst.size(); i++){
                        if(caseForce_obj.ONTAP__Account__c == casesToBeCreated_lst[i].ONTAP__Account__c && casesToBeCreated_lst[i].ONTAP__Subject__c == caseForce_obj.ONTAP__Subject__c){
                            positionToRemove_int = i;
                            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping ['+casesToBeCreated_lst[i].ONTAP__Subject__c+' == '+caseForce_obj.ONTAP__Subject__c+']');
                            break;
                        }
                    }
                    if(positionToRemove_int >= 0){
                        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping ["A generated Case was removed due to duplicity"]');
                        
                        casesToBeCreated_lst.remove(positionToRemove_int);
                        positionToRemove_int = -1;
                    }
                }
                System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [casesToBeCreated_lst.size() = '+casesToBeCreated_lst.size()+']');
                if(!casesToBeCreated_lst.isEmpty())
                    insert casesToBeCreated_lst;
            }catch(DmlException e){
                System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [DML ERROR]');
            }
        }
        
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.afterUpdate [END]');
    }
    
    /* Trigger Method to be executed before Insertion */
    public static List<ONTAP__Case_Force__c> createCasesByGrouping(Set<String> groupingElements_set, List<ONTAP__SurveyQuestionResponse__c> surveyResponses_lst,Map<Id, Map<String,Id>> teamMember_map){
        Id toAssignId_Id;

        List<ONTAP__Case_Force__c> casesToBeCreated_lst = new List<ONTAP__Case_Force__c>();
        Map<Id,Map<String, Decimal>> groupingAndScorePercentage_map = new Map<Id,Map<String, Decimal>>();
        Map<Id,Map<String,Decimal>> surveyTakerAndTotalScore_map = new  Map<Id,Map<String,Decimal>>();
        Map<Id,Id> idTakerAndIdAccount_map = new Map<Id,Id>();
        Map<Id,Id> idTakerAndIdOwner_map = new Map<Id,Id>();
        Decimal tempScore_dec = 0;
        
        Map<String,ISSM_SurveyAssignmentCase__c> toAssign_map = new Map<String,ISSM_SurveyAssignmentCase__c>();
        //fill the map for the case assignment process
        for(ISSM_SurveyAssignmentCase__c assign : ISSM_SurveyAssignmentCase__c.getall().values()){
            toAssign_map.put(assign.ISSM_Grouping__c.deleteWhitespace(), assign);
        }
                
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [surveyResponses_lst.size() = '+surveyResponses_lst.size()+']');
        
        //Loop to sum up all the scores from the question responses, grouped by SurveyTakerId and grouping
       for(ONTAP__SurveyQuestionResponse__c sResponse_obj : surveyResponses_lst){
            // iterate over all the groupings from the surveys
            for(String gElements_str : groupingElements_set){
                //if the response have the same group as the one from the cycle
                if(gElements_str == sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c){
                    if(!groupingAndScorePercentage_map.containsKey(sResponse_obj.ONTAP__SurveyTaker__c)){
                        groupingAndScorePercentage_map.put(sResponse_obj.ONTAP__SurveyTaker__c, new Map<String, Decimal>{gElements_str => sResponse_obj.ONTAP__POCE_Score__c});
                    }else{                      
                        if(groupingAndScorePercentage_map.get(sResponse_obj.ONTAP__SurveyTaker__c).containsKey(gElements_str)){
                            if(sResponse_obj.ONTAP__POCE_Score__c!= null)
                            tempScore_dec = groupingAndScorePercentage_map.get(sResponse_obj.ONTAP__SurveyTaker__c).get(gElements_str) + sResponse_obj.ONTAP__POCE_Score__c;
                        }else{
                            tempScore_dec = sResponse_obj.ONTAP__POCE_Score__c;
                        }
                        
                        groupingAndScorePercentage_map.get(sResponse_obj.ONTAP__SurveyTaker__c).put(gElements_str, tempScore_dec);
                    }
                }
            }
            //map the SurveyTakerId with the Account related Id
            idTakerAndIdAccount_map.put(sResponse_obj.ONTAP__SurveyTaker__c, sResponse_obj.ONTAP__SurveyTaker__r.ONTAP__Account__c);
            //map the SurveyTakerId with the Useer Id related to the account
            idTakerAndIdOwner_map.put(sResponse_obj.ONTAP__SurveyTaker__c, sResponse_obj.ONTAP__SurveyTaker__r.ONTAP__Account__r.OwnerId);
        }
        //
        for(ONTAP__SurveyQuestionResponse__c sResponse_obj : surveyResponses_lst){
                if(!surveyTakerAndTotalScore_map.containsKey(sResponse_obj.ONTAP__SurveyTaker__c)){
                    surveyTakerAndTotalScore_map.put(sResponse_obj.ONTAP__SurveyTaker__c, new Map<String,Decimal>{sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c => sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Weight__c});
                }else{
                    if(surveyTakerAndTotalScore_map.get(sResponse_obj.ONTAP__SurveyTaker__c).containsKey(sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c)){
                        tempScore_dec = surveyTakerAndTotalScore_map.get(sResponse_obj.ONTAP__SurveyTaker__c).get(sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c) + sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Weight__c;
                        surveyTakerAndTotalScore_map.get(sResponse_obj.ONTAP__SurveyTaker__c).put(sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c,tempScore_dec);
                    }else{
                        surveyTakerAndTotalScore_map.get(sResponse_obj.ONTAP__SurveyTaker__c).put(sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Question_Grouping__c, sResponse_obj.ONTAP__Survey_Question__r.ONTAP__POCE_Weight__c);
                    }
                    
                }
        }
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [groupingAndScorePercentage_map = '+groupingAndScorePercentage_map+']');
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [groupingElements_set = '+groupingElements_set+']');
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [surveyTakerAndTotalScore_map = '+surveyTakerAndTotalScore_map+']');
        Boolean flagToRun = false;
        for(Id surveyTaker_obj : groupingAndScorePercentage_map.keySet()) {
            for(String grouping : groupingAndScorePercentage_map.get(surveyTaker_obj).keySet()){
            	flagToRun |= toAssign_map.containskey(grouping);
            }
            
        }
        if(flagToRun){
            for(Id surveyTaker_obj : groupingAndScorePercentage_map.keySet()) {
                for(String gElements_str : groupingElements_set){
                    if(toAssign_map.containskey(gElements_str)){
                        Decimal percentage_dec = (groupingAndScorePercentage_map.get(surveyTaker_obj).get(gElements_str)/surveyTakerAndTotalScore_map.get(surveyTaker_obj).get(gElements_str))*100;
                        if(percentage_dec < PASSING_SCORE) {
                            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [Group Score Fail = '+gElements_str+']');
                            System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [Score Fail = '+percentage_dec+']');
                            Id idAccount = idTakerAndIdAccount_map.get(surveyTaker_obj);
                            Id idNewOwner = idTakerAndIdOwner_map.get(surveyTaker_obj);
                            if(teamMember_map.get(idTakerAndIdAccount_map.get(surveyTaker_obj))==null){
                                toAssignId_Id = idNewOwner;
                            }else if(toAssign_map.get(gElements_str)==null){
                                toAssignId_Id = idNewOwner;
                            }else if(teamMember_map.get(idAccount).get(toAssign_map.get(gElements_str).ISSM_AccountTeamRole__c.deleteWhitespace()) == null){
                                toAssignId_Id = idNewOwner;
                            }else{
                                toAssignId_Id = teamMember_map.get(idAccount).get(toAssign_map.get(gElements_str).ISSM_AccountTeamRole__c.deleteWhitespace());
                            }
                            if(toAssign_map.containskey(gElements_str)){
                                casesToBeCreated_lst.add(new ONTAP__Case_Force__c(ONTAP__Account__c = idAccount,
                                                                                  ONTAP__Description__c = toAssign_map.get(gElements_str).ISSM_CaseDescription__c, 
                                                                                  ONTAP__Status__c = CASE_TO_CLASSIFY_STATUS,
                                                                                  ONTAP__Subject__c = toAssign_map.get(gElements_str).ISSM_CaseSubject__c, 
                                                                                  RecordTypeId = CASE_FORCE_RECORD_TYPE_ID,
                                                                                  ISSM_ClassificationLevel2__c = toAssign_map.get(gElements_str).ISSM_Classification__c,
                                                                                  ONTAP__Assigned_User__c = toAssignId_Id
                                                                                 ));
                            }
                        }
                    }
                }
            }
        }
        System.debug(':::: ISSM_SurveyTakenTgrHnd_cls.createCasesByGrouping [END]');
        return casesToBeCreated_lst;
    }
}