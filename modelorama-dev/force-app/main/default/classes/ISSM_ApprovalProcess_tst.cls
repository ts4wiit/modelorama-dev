@isTest
public class ISSM_ApprovalProcess_tst {
    
    
    @testSetup static void loadData() {
        
        String IdProfile = [Select id from Profile where Name = 'Price Promotion Manager'].get(0).Id;
        
        User userPPMCorp = new User();
        userPPMCorp.ProfileId = IdProfile;
        userPPMCorp.Alias='ppmcorpt';
        userPPMCorp.CommunityNickname='ppmcorpt';
        userPPMCorp.DefaultGroupNotificationFrequency='N';
        userPPMCorp.DigestFrequency='D';
        userPPMCorp.Email='ngonzalez@avanxo.com';
        userPPMCorp.EmailEncodingKey='ISO-8859-1';
        userPPMCorp.EmailPreferencesAutoBcc=true;
        userPPMCorp.EmailPreferencesAutoBccStayInTouch=false;
        userPPMCorp.EmailPreferencesStayInTouchReminder=true;
        userPPMCorp.ForecastEnabled=false;
        userPPMCorp.IsActive=true;
        userPPMCorp.LanguageLocaleKey='es';
        userPPMCorp.LastName='PPM Corporativo';
        userPPMCorp.LocaleSidKey='es_MX';
        userPPMCorp.ReceivesAdminInfoEmails=true;
        userPPMCorp.ReceivesInfoEmails=true;
        userPPMCorp.TimeZoneSidKey='America/Mexico_City';
        userPPMCorp.Title='Corporativo';
        userPPMCorp.Username='ppmcorporativotst@avanxo.com';
        insert userPPMCorp;
        
        User userPPMReg = new User();
        userPPMReg.ProfileId = IdProfile;
        userPPMReg.Alias='ppmregt';
        userPPMReg.CommunityNickname='ppmregionalt';
        userPPMReg.DefaultGroupNotificationFrequency='N';
        userPPMReg.DigestFrequency='D';
        userPPMReg.Email='ngonzalez@avanxo.com';
        userPPMReg.EmailEncodingKey='ISO-8859-1';
        userPPMReg.EmailPreferencesAutoBcc=true;
        userPPMReg.EmailPreferencesStayInTouchReminder=true;
        userPPMReg.IsActive=true;
        userPPMReg.ISSM_User_country__c='Mexico';
        userPPMReg.LanguageLocaleKey='en_US';
        userPPMReg.LastName='PPM Regional';
        userPPMReg.LocaleSidKey='es_MX';
        userPPMReg.ManagerId=userPPMCorp.Id;
        userPPMReg.ONTAP__Business_Unit__c='3120-CMM Acapulco';
        userPPMReg.ReceivesAdminInfoEmails=true;
        userPPMReg.ReceivesInfoEmails=true;
        userPPMReg.TimeZoneSidKey='America/Mexico_City';
        userPPMReg.Title='Regional';
        userPPMReg.Username='ppmregionaltst@avanxo.com';
        insert userPPMReg;
        
        User userPPMLocal = new User();
        userPPMLocal.ProfileId = IdProfile;
        userPPMLocal.Alias='ppmlocaa';
        userPPMLocal.CommunityNickname='ppmlocaa';
        userPPMLocal.DefaultGroupNotificationFrequency='N';
        userPPMLocal.DigestFrequency='D';
        userPPMLocal.Email='llicona@avanxo.com';
        userPPMLocal.EmailEncodingKey='ISO-8859-1';
        userPPMLocal.EmailPreferencesAutoBcc=true;
        userPPMLocal.EmailPreferencesStayInTouchReminder=true;
        userPPMLocal.IsActive=true;
        userPPMLocal.LanguageLocaleKey='es';
        userPPMLocal.LastName='PPM Local';
        userPPMLocal.LocaleSidKey='es_MX';
        userPPMLocal.ManagerId=userPPMReg.Id;
        userPPMLocal.ReceivesAdminInfoEmails=true;
        userPPMLocal.ReceivesInfoEmails=true;
        userPPMLocal.TimeZoneSidKey='America/Mexico_City';
        userPPMLocal.Title='PPM Local';
        userPPMLocal.Username='ppmlocaltst@avanxo.com';
        insert userPPMLocal;
        

        //create SEGMENTO
        MDM_Parameter__c segmento01 = new MDM_Parameter__c();
        segmento01.Active_Revenue__c= true;
        segmento01.Active__c        = true;
        segmento01.Catalog__c       = 'Segmento';
        segmento01.Code__c          = '42';
        segmento01.Description__c   = 'Foco (Tradicional)';
        segmento01.ExternalId__c    = 'Segmento-42';
        segmento01.Name             = 'Foco (Tradicional)';
        insert segmento01;
        
        Account grupoModeloAccount01 = new Account();
        grupoModeloAccount01.IsExcludedFromRealign=false;
        grupoModeloAccount01.ISSM_AccountToDel__c=false;
        grupoModeloAccount01.ISSM_AttachmentProcessed__c=false;
        grupoModeloAccount01.ISSM_EmptyCreditQty__c=false;
        grupoModeloAccount01.ISSM_HasBonus__c=false;
        grupoModeloAccount01.ISSM_MainContactA__c=false;
        grupoModeloAccount01.ISSM_MainContactB__c=false;
        grupoModeloAccount01.ISSM_MainContactC__c=false;
        grupoModeloAccount01.ISSM_MainContactD__c=false;
        grupoModeloAccount01.ISSM_MainContactE__c=false;
        grupoModeloAccount01.ISSM_SegmentCode__c=segmento01.Id;
        grupoModeloAccount01.ISSM_StartCall__c=false;
        grupoModeloAccount01.ISSM_ValidateOrder__c=false;
        grupoModeloAccount01.Name='Grp Mdlo';
        grupoModeloAccount01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        grupoModeloAccount01.ONCALL__Ignore_Objective_Flag__c=false;
        grupoModeloAccount01.ONCALL__No_Call__c=false;
        grupoModeloAccount01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        grupoModeloAccount01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        grupoModeloAccount01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        grupoModeloAccount01.ONCALL__PO_Required__c=false;
        grupoModeloAccount01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        grupoModeloAccount01.ONCALL__Seasonal__c=false;
        grupoModeloAccount01.ONTAP__Classification__c='Botella Cerrada';
        grupoModeloAccount01.ONTAP__Decision_Maker__c=false;
        grupoModeloAccount01.ONTAP__Is_Wholesaler__c=false;
        grupoModeloAccount01.ONTAP__Loan_Applied__c=false;
        grupoModeloAccount01.ONTAP__Not_Interested__c=false;
        grupoModeloAccount01.ONTAP__SAPCustomerId__c='ABINBEV';
        grupoModeloAccount01.ONTAP__Segment__c='Botella Cerrada';
        grupoModeloAccount01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName = 'Account'].get(0).Id;
        insert grupoModeloAccount01;
        
        //Inser Division general ventas
        Account drv01 = new Account();
        drv01.IsExcludedFromRealign=false;
        drv01.ISSM_AccountToDel__c=false;
        drv01.ISSM_AttachmentProcessed__c=false;
        drv01.ISSM_EmptyCreditQty__c=false;
        drv01.ISSM_HasBonus__c=false;
        drv01.ISSM_MainContactA__c=false;
        drv01.ISSM_MainContactB__c=false;
        drv01.ISSM_MainContactC__c=false;
        drv01.ISSM_MainContactD__c=false;
        drv01.ISSM_MainContactE__c=false;
        drv01.ISSM_ParentAccount__c=grupoModeloAccount01.Id;
        drv01.ISSM_StartCall__c=false;
        drv01.ISSM_ValidateOrder__c=false;
        drv01.Name='DRV Centro Sur';
        drv01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        drv01.ONCALL__Ignore_Objective_Flag__c=false;
        drv01.ONCALL__No_Call__c=false;
        drv01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        drv01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        drv01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        drv01.ONCALL__PO_Required__c=false;
        drv01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        drv01.ONCALL__Seasonal__c=false;
        drv01.ONTAP__Decision_Maker__c=false;
        drv01.ONTAP__Is_Wholesaler__c=false;
        drv01.ONTAP__Loan_Applied__c=false;
        drv01.ONTAP__Negotiation_Status__c='Nuevo';
        drv01.ONTAP__Not_Interested__c=false;
        drv01.ONTAP__SAPCustomerId__c='GM00004';
        drv01.RecordTypeId=[SELECT id FROM RecordType where DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;
        insert drv01;
        
        Account salesOrg01 = new Account();
        salesOrg01.IsExcludedFromRealign=false;
        salesOrg01.ISSM_AccountToDel__c=false;
        salesOrg01.ISSM_AttachmentProcessed__c=false;
        salesOrg01.ISSM_EmptyCreditQty__c=false;
        salesOrg01.ISSM_HasBonus__c=false;
        salesOrg01.ISSM_MainContactA__c=false;
        salesOrg01.ISSM_MainContactB__c=false;
        salesOrg01.ISSM_MainContactC__c=false;
        salesOrg01.ISSM_MainContactD__c=false;
        salesOrg01.ISSM_MainContactE__c=false;
        salesOrg01.ISSM_ParentAccount__c=drv01.Id;
        salesOrg01.ISSM_SegmentCode__c=segmento01.Id;
        salesOrg01.ISSM_StartCall__c=false;
        salesOrg01.Name='CMM Acapulco';
        salesOrg01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        salesOrg01.ONCALL__Ignore_Objective_Flag__c=false;
        salesOrg01.ONCALL__No_Call__c=false;
        salesOrg01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        salesOrg01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        salesOrg01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        salesOrg01.ONCALL__PO_Required__c=false;
        salesOrg01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        salesOrg01.ONCALL__Seasonal__c=false;
        salesOrg01.ONTAP__Decision_Maker__c=false;
        salesOrg01.ONTAP__Is_Wholesaler__c=false;
        salesOrg01.ONTAP__Loan_Applied__c=false;
        salesOrg01.ONTAP__Negotiation_Status__c='Nuevo';
        salesOrg01.ONTAP__Not_Interested__c=false;
        salesOrg01.ONTAP__SalesOgId__c='3120';
        salesOrg01.ONTAP__SAPCustomerId__c='3120';
        salesOrg01.RecordTypeId= [SELECT Id FROM RecordType where DeveloperName= 'SalesOrg'].get(0).Id;
        insert salesOrg01;        
        
        Account salesOffice01 = new Account();
        salesOffice01.EndTime__c='0.833333333333333';
        salesOffice01.IsExcludedFromRealign=false;
        salesOffice01.ISSM_AccountToDel__c=false;
        salesOffice01.ISSM_AttachmentProcessed__c=false;
        salesOffice01.ISSM_EmptyCreditQty__c=false;
        salesOffice01.ISSM_HasBonus__c=false;
        salesOffice01.ISSM_MainContactA__c=false;
        salesOffice01.ISSM_MainContactB__c=false;
        salesOffice01.ISSM_MainContactC__c=false;
        salesOffice01.ISSM_MainContactD__c=false;
        salesOffice01.ISSM_MainContactE__c=false;
        salesOffice01.ISSM_ParentAccount__c=salesOrg01.Id;
        salesOffice01.ISSM_SalesOrg__c=salesOrg01.Id;
        salesOffice01.ISSM_SegmentCode__c=segmento01.Id;
        salesOffice01.ISSM_StartCall__c=false;
        salesOffice01.ISSM_ValidateOrder__c=false;
        salesOffice01.Name='CMM Ometepec';
        salesOffice01.ONCALL__Do_Not_Include_in_Call_LIst__c=false;
        salesOffice01.ONCALL__Ignore_Objective_Flag__c=false;
        salesOffice01.ONCALL__No_Call__c=false;
        salesOffice01.ONCALL__OnCall_Ignore_PL_POCFilter__c=false;
        salesOffice01.ONCALL__OnCall_Ignore_PO_POCFilter__c=false;
        salesOffice01.ONCALL__ONTAP_Manual_Assign_Proposal_List__c=false;
        salesOffice01.ONCALL__PO_Required__c=false;
        salesOffice01.ONCALL__Remote_Survey_Vendor_Enabled__c=false;
        salesOffice01.ONCALL__Seasonal__c=false;
        salesOffice01.ONTAP__Decision_Maker__c=false;
        salesOffice01.ONTAP__Is_Wholesaler__c=false;
        salesOffice01.ONTAP__Loan_Applied__c=false;
        salesOffice01.ONTAP__Negotiation_Status__c='Nuevo';
        salesOffice01.ONTAP__Not_Interested__c=false;
        salesOffice01.ONTAP__SalesOfficeDescription__c='CMM Acapulco';
        salesOffice01.ONTAP__SalesOffId__c='FD10';
        salesOffice01.ONTAP__SalesOgId__c='3120';
        salesOffice01.ONTAP__SAPCustomerId__c='FD10';
        salesOffice01.ParentId=salesOrg01.Id;
        salesOffice01.PercentageOffRoute__c=30;
        salesOffice01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName= 'SalesOffice'].get(0).Id;
        salesOffice01.TimeDifference__c='1';
        insert salesOffice01;
        
        ISSM_ComboLimit__c comboLimit01 = new ISSM_ComboLimit__c();  
        comboLimit01.ISSM_AllowedCombos__c=50;
        comboLimit01.ISSM_ComboLevel__c='ISSM_SalesOffice';
        comboLimit01.ISSM_IsActive__c=true;
        comboLimit01.ISSM_SalesStructure__c=salesOffice01.Id;
        comboLimit01.RecordTypeId= [SELECT Id FROM RecordType where DeveloperName = 'ISSM_LimitComboMx'].get(0).Id;
        insert comboLimit01;
        
        ISSM_Combos__c  combo01 = new ISSM_Combos__c();
        combo01.ISSM_ComboLimit__c=comboLimit01.Id;
        combo01.ISSM_ComboType__c='Mixed';
        combo01.ISSM_Currency__c='MXN';
        combo01.ISSM_EndDate__c=System.Today();
        combo01.ISSM_ExternalKey__c='9000000430';
        combo01.ISSM_LongDescription__c='Combo Off';
        combo01.ISSM_NumberByCustomer__c=1;
        combo01.ISSM_NumberSalesOffice__c=1000;
        combo01.ISSM_ShortDescription__c='Combo Off';
        combo01.ISSM_StartDate__c=System.Today();
        combo01.ISSM_StatusCombo__c='ISSM_Open';
        combo01.ISSM_SynchronizedWithSAP__c=false;
        combo01.ISSM_TypeApplication__c='ALLMOBILE;TELESALES;B2B;BDR';
        combo01.RecordTypeId=[SELECT Id FROM RecordType where DeveloperName= 'ISSM_ComboMx'].get(0).Id;
        insert combo01;     
    }
    @isTest static void testSubmitAndProcessApprovalRequest() {
        test.startTest();
        
        ISSM_Combos__c combo = [Select Id,Name from ISSM_Combos__c].get(0);
        User userLocal = [SELECT Id, ManagerId FROM User WHERE Alias = 'ppmlocaa'].get(0);
        System.debug('userLocal: '+ userLocal);
        
        ISSM_ApprovalProcess_cls.submitForApproval(combo.Id, 'Favor de aprobar', 'ISSM_ApprovalCombo', userLocal.Id);   // enviar a aprobar
        ISSM_ApprovalProcess_cls.submitAndProcessApprovalRequest(combo.Id);												// Aprobar
        test.stopTest();
    }
    @isTest static void testSubmitAndProcessApprovalRequestNull() {
        test.startTest();
        
        ISSM_Combos__c combo = [Select Id,Name from ISSM_Combos__c].get(0);
        User userLocal = [SELECT Id, ManagerId FROM User WHERE Alias = 'ppmlocaa'].get(0);
        System.debug('userLocal: '+ userLocal);
        
        ISSM_ApprovalProcess_cls.submitForApproval((new  ISSM_Combos__c()).Id, 'Favor de aprobar', 'ISSM_ApprovalCombo', userLocal.Id);	// enviar a aprobar
        ISSM_ApprovalProcess_cls.submitAndProcessApprovalRequest((new  ISSM_Combos__c()).Id);                                   		// Aprobar
        test.stopTest();
    }
}