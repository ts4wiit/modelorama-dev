/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class with ONTAP__Tour__c operations

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       19-07-2017        Andrés Garrido               Creation Class
    1.1       28-09-2017        Nelson Sáenz                 Modified Class
****************************************************************************************************/
global class TourOperations_cls implements Queueable{
    public static set<id> setIdsRoutes                   =   new set<Id>();
    public static list<ONTAP__Route__c> lstOnTapRoute       =   new list<ONTAP__Route__c>();
    public static map<id, ONTAP__Route__c>  mapIdRoutes  =   new map<id, ONTAP__Route__c>();
    public set<Id>  setIdsVsPlan                            =   new set<Id>();
    public list<ONTAP__Tour__c> lstRecentCreatedTour        =   new list<ONTAP__Tour__c>();
    public TourOperations_cls(set<Id> setIdsVsPlan, list<ONTAP__Tour__c> lstRecentCreatedTour)
    {
        this.setIdsVsPlan           = setIdsVsPlan;
        this.lstRecentCreatedTour   = lstRecentCreatedTour;
    }
    /**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when a record is insert
    * @params:
      1.-mapNewTour: map with all new tour records
    * @return void
    **/
    public static void syncInsertToursToExternalObject(map<Id, ONTAP__Tour__c> mapTour){
    	map<Id,Id> mapIds = new map<Id,Id>();
    	set<Id> setIds = new set<Id>();
    	
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	
    	list<RecordType> lstRT = [Select Id From RecordType Where DeveloperName = :lstRecordType];
    	for(RecordType rt : lstRT)
    		mapIds.put(rt.Id, rt.Id);
    	
    	for(ONTAP__Tour__c objTour : mapTour.values()){
    		if(mapIds.containsKey(objTour.RecordTypeId))
    			setIds.add(objTour.Id);
    	}
    	
    	//Call a future method
    	if(!setIds.isEmpty())
			TourOperations_cls.insertExternalToursWS(setIds);
    }

    /**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when a record is update
    * @params:
      1.-lstOldTour: list with all new tour records
      2.-lstNewTour: list with all old tour records
    * @return void
    **/
    public static void syncUpdateToursToExternalObject(list<ONTAP__Tour__c> lstOldTour, list<ONTAP__Tour__c> lstNewTour){
    	map<Id,Id> mapIds = new map<Id,Id>();
    	set<Id> setIds = new set<Id>();
    	
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	
    	list<RecordType> lstRT = [Select Id From RecordType Where DeveloperName = :lstRecordType];
    	for(RecordType rt : lstRT)
    		mapIds.put(rt.Id, rt.Id);
    	
    	//Validate if the Tour Status, SubStatus Or IsActive field are changed
		for(Integer i=0; i<lstNewTour.size(); i++){
			if(
				mapIds.containsKey(lstNewTour[i].RecordTypeId) && 
				(lstOldTour[i].ONTAP__IsActive__c != lstNewTour[i].ONTAP__IsActive__c ||
				lstOldTour[i].ONTAP__TourStatus__c != lstNewTour[i].ONTAP__TourStatus__c ||
				lstOldTour[i].TourSubStatus__c != lstNewTour[i].TourSubStatus__c)
			)
				setIds.add(lstNewTour[i].Id);
		}

		if(!setIds.isEmpty())
			TourOperations_cls.updateExternalToursWS(setIds);
    }

    /**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when a record is delete
    * @params:
      1.-mapNewTour: map with all new tour records
    * @return void
    **/
    public static void syncDeleteToursToExternalObject(map<Id, ONTAP__Tour__c> mapTour){
    	map<Id,Id> mapIds = new map<Id,Id>();
    	set<Id> setIds = new set<Id>();
    	
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	
    	list<RecordType> lstRT = [Select Id From RecordType Where DeveloperName = :lstRecordType];
    	for(RecordType rt : lstRT)
    		mapIds.put(rt.Id, rt.Id);
    	
    	for(ONTAP__Tour__c objTour : mapTour.values()){
    		if(mapIds.containsKey(objTour.RecordTypeId))
    			setIds.add(objTour.Id);
    	}
    	
    	//Call a future method
    	if(!setIds.isEmpty())
    		TourOperations_cls.deleteExternalToursWS(setIds);
    }

    
    /**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when the record is insert
    * @params:
      1.-setIds: set of Ids tour to synchronize with external object
    * @return void
    **/
    @future(callout=true)
    public static void insertExternalToursWS(set<Id> setIds){
    	TourOperations_cls.insertExternalTours(setIds, true);
    }
    
    /**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when the record is insert
    * @params:
      1.-setIds: set of Ids tour to synchronize with external object
    * @return Boolean
    **/
    public static Boolean insertExternalTours(set<Id> setIds, Boolean blnUpdate){
    	//Obtain the record types that will be sinchronize with heroku
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	//Obtain the data from the tour records
    	list<ONTAP__Tour__c> lstTours = [
    		Select 	Id, RecordType.DeveloperName, ONTAP__TourId__c, ONTAP__TourStatus__c, TourSubStatus__c, Route__c,
    				Route__r.Vehicle__r.ONTAP__VehicleId__c, ONTAP__TourDate__c, Route__r.RouteManager__r.UserName,
    				Route__r.ONTAP__RouteName__c, ONTAP__IsActive__c, VisitPlan__c, EstimatedDeliveryDate__c,
    				Route__r.ONTAP__SalesOffice__r.Exception__c, Route__r.ONTAP__SalesOffice__r.PercentageOffRoute__c,
    				Route__r.ONTAP__SalesOffice__r.StartTime__c, Route__r.ONTAP__SalesOffice__r.EndTime__c,
    				Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOffId__c, Route__r.ONTAP__SalesOffice__r.ISSM_RFC__c,
    				Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOgId__c, Route__r.ONTAP__SalesOfficeRouteIdKey__c,
    				Route__r.ONTAP__SalesOffice__r.ShippingStreet, Route__r.ONTAP__SalesOffice__r.ShippingCity,
    				Route__r.ONTAP__SalesOffice__r.TimeDifference__c, Route__r.ServiceModel__c,
    				Route__r.ONTAP__RouteId__c, Route__r.ONTAP__SalesOffice__r.Name, Route__r.ONTAP__SalesOffice__r.ISSM_SalesOrg__r.Name 
    		From	ONTAP__Tour__c
    		Where	RecordType.DeveloperName = :lstRecordType And Id = :setIds
    	];

    	System.debug('\nTours a procesar: '+lstTours.size());    	
    	SyncObjects.SyncTourObject objSync = new SyncObjects.SyncTourObject();

    	//Create all external tour records
    	for(ONTAP__Tour__c objTour :lstTours){
    		String strIdTour = objTour.Id+'';
    		SyncObjects.TourObject objExtTour 		= 	new SyncObjects.TourObject();
    		objExtTour.salesforceid					= 	strIdTour;
    		objExtTour.estimateddeliverydate 		=	objTour.EstimatedDeliveryDate__c;
    		objExtTour.isactive						=	objTour.ONTAP__IsActive__c;
    		objExtTour.percentageoffroute			= 	objTour.Route__r.ONTAP__SalesOffice__r.PercentageOffRoute__c;
    		objExtTour.rfc							= 	objTour.Route__r.ONTAP__SalesOffice__r.ISSM_RFC__c;
    		objExtTour.routedescription				= 	objTour.Route__r.ONTAP__RouteName__c;
    		objExtTour.routeid						= 	objTour.Route__r.ONTAP__RouteId__c;
    		objExtTour.salesagent					= 	objTour.Route__r.RouteManager__r.UserName;
    		objExtTour.salesofficename				= 	objTour.Route__r.ONTAP__SalesOffice__r.Name;
    		objExtTour.salesofficeid				= 	objTour.Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOffId__c;
    		objExtTour.salesorgname					=	objTour.Route__r.ONTAP__SalesOffice__r.ISSM_SalesOrg__r.Name;
    		objExtTour.salesorgid					= 	objTour.Route__r.ONTAP__SalesOffice__r.ONTAP__SalesOgId__c;
    		objExtTour.shippingcity					= 	objTour.Route__r.ONTAP__SalesOffice__r.ShippingStreet;
    		objExtTour.shippingstreet				= 	objTour.Route__r.ONTAP__SalesOffice__r.ShippingCity;
    		objExtTour.timedifference				=	objTour.Route__r.ONTAP__SalesOffice__r.TimeDifference__c;
    		objExtTour.tourid						=	objTour.ONTAP__TourId__c;
    		objExtTour.tourstatus					=	objTour.ONTAP__TourStatus__c;
    		objExtTour.toursubstatus				=	objTour.TourSubStatus__c;
    		objExtTour.vehicleid					=	objTour.Route__r.Vehicle__r.ONTAP__VehicleId__c;
    		objExtTour.visitplantype				=	objTour.Route__r.ServiceModel__c;
			objExtTour.starttime					= 	objTour.Route__r.ONTAP__SalesOffice__r.StartTime__c;
			objExtTour.endtime						=	objTour.Route__r.ONTAP__SalesOffice__r.EndTime__c;
			objExtTour.tourdate						= 	objTour.ONTAP__TourDate__c;
			objExtTour.exceptiontime				=	objTour.Route__r.ONTAP__SalesOffice__r.Exception__c;
    		objSync.tours.add(objExtTour);
    		
    		objTour.SynchronizedHeroku__c			= 	true;
    	}

    	//if not is empty insert record in external object
    	if(!objSync.tours.isEmpty()){
    		String strStatusCode = SyncObjects.calloutInsertHerokuTours(objSync);
    		if(strStatusCode == '200' && !Test.isRunningTest()){ 
    			if(blnUpdate) update lstTours;
    			
    			return true;
    		}
    	}
    	
    	return false;
    }

	/**
    * Method that synchronize the Tour object in SFDC with Tour object in Heroku, occurs when the record is update
    * @params:
      1.-setIds: set of Ids tour to synchronize with external object
    * @return void
    **/
    @future(callout=true)
    public static void updateExternalToursWS(set<Id> setIds){
    	map<Id, ONTAP__Tour__c> mapTour = new map<Id, ONTAP__Tour__c>([
    		Select 	Id, ONTAP__TourStatus__c, TourSubStatus__c, ONTAP__IsActive__c
    		From	ONTAP__Tour__c
    		Where	Id = :setIds
    	]);
		
		SyncObjects.UpdateTourObject objSync = new SyncObjects.UpdateTourObject();
		
    	for(ONTAP__Tour__c objTour : mapTour.values()){
    		SyncObjects.ObjTourUpdate objExtTour = new SyncObjects.ObjTourUpdate();
    		objExtTour.salesforceid 	= objTour.Id;
			objExtTour.tourstatus 		= objTour.ONTAP__TourStatus__c;
    		objExtTour.toursubstatus 	= objTour.TourSubStatus__c;
    		objExtTour.isactive 		= objTour.ONTAP__IsActive__c;
    		
    		objSync.tours.add(objExtTour);
    	}

		//if not is empty insert record in external object
    	if(!objSync.tours.isEmpty()){
    		String strStatusCode = SyncObjects.calloutUpdateHerokuTours(objSync);
    		if(strStatusCode == '200')
    			System.debug('\nSuccess Update tours');

    	}
    }
    
    @future(callout=true)
    public static void deleteExternalToursWS(set<Id> setIds){
    	SyncObjects.DeleteTourObject objSync = new SyncObjects.DeleteTourObject();
    	for(String strId :setIds){
    		SyncObjects.ObjTourDelete objDel = new SyncObjects.ObjTourDelete();
    		objDel.salesforceid = strId;
    		
    		objSync.tours.add(objDel);
    	}
    	
    	if(!objSync.tours.isEmpty()){
    		String strStatusCode = SyncObjects.calloutDeleteHerokuTours(objSync);
    		if(strStatusCode == '200'){
    			System.debug('\nDELETE SUCCESS');
    		}

    	}
    }
    
    /**
    * Method to set the first tour to assigned and validate if an already assigned
    * @params:
      1.-lstTours: list Tours to set ONTAP__TourStatus__c 'Assigned'
    * @return void
    * 1.1
    **/
    public static void assignTour(list<ONTAP__Tour__c> lstTours)
    {
        map<Id, list<ONTAP__Tour__c>> mapIdListTours    = new map<Id, list<ONTAP__Tour__c>>();
        list<ONTAP__Tour__c> lstOnTapTour               = new list<ONTAP__Tour__c>();
        set<Id> setIdsRoutesAssignTour                  = new set<Id>();


        for(ONTAP__Tour__c objTour: lstTours)
        {
            setIdsRoutes.add(objTour.Route__c);
            if(objTour.ONTAP__TOURSTATUS__C == label.TourStatusAssigned)
            {
                lstOnTapTour = mapIdListTours.get(objTour.Route__c);
                if(lstOnTapTour ==  null)
                {
                    lstOnTapTour = new list<ONTAP__Tour__c>();
                    mapIdListTours.put(objTour.Route__c, lstOnTapTour);
                }
                lstOnTapTour.add(objTour);
            }
        }

        mapIdRoutes      =   getVisiPlanxTours(setIdsRoutes);

        System.debug('mapIdRoutes===>>>'+mapIdRoutes);
        System.debug('mapIdListTours===>>>'+mapIdListTours);

        for(ONTAP__Tour__c objTour: lstTours)
        {
            if(mapIdRoutes.containsKey(objTour.Route__c)
                && mapIdListTours.containsKey(objTour.Route__c))
            {
                if(mapIdRoutes.get(objTour.Route__c).Tours__r.isEmpty()
                    && mapIdListTours.get(objTour.Route__c).size() == 1)
                {
                    if(objTour.ONTAP__TOURSTATUS__C == label.TourStatusAssigned)
                        setIdsRoutesAssignTour.add(objTour.Route__c);
                }

                if(mapIdRoutes != null && !mapIdRoutes.isEmpty()
                    && mapIdListTours != null && !mapIdListTours.isEmpty())
                {
                    if(mapIdRoutes.get(objTour.Route__c).Tours__r.size() == 1
                        && mapIdListTours.get(objTour.Route__c).size() >= 1 )
                    {
                        objTour.addError(label.error_assigned_tour);
                    }
                }
            }
        }
        System.debug('**_** setIdsRoutesAssignTour== >>'+setIdsRoutesAssignTour);
        if(!setIdsRoutesAssignTour.isEmpty())
        	ID jobID = System.enqueueJob(new TourOperations_cls(setIdsRoutesAssignTour, null));
    }
    /**
    * Method in charge of set the Tourid of the route with the assigned tour
    * @params:
      1.-Set Ids visit plans of tours
    * @return void
    * 1.1
    **/
    public void execute(QueueableContext context)
    {
    	if(lstRecentCreatedTour != null && !lstRecentCreatedTour.isEmpty())
            update lstRecentCreatedTour;
            
        /*map<id, ONTAP__Route__c>  mapIdRoutes      =   getVisiPlanxTours(setIdsVsPlan);

        for(Id idRoute: mapIdRoutes.keyset())
        {
            if(mapIdRoutes != null && !mapIdRoutes.isEmpty())
            {
                for(ONTAP__Tour__c objTour: mapIdRoutes.get(idRoute).Tours__r)
                {
                    if(objTour.ONTAP__TourStatus__c == label.TourStatusAssigned)
                        lstOnTapRoute.add(new ONTAP__Route__c(Id =  mapIdRoutes.get(idRoute).Id, ONTAP__TourId__c = objTour.ONTAP__TourId__c));
                }
            }
        }

        System.debug('lstOnTapRoute===>>'+lstOnTapRoute);
        if(!lstOnTapRoute.isEmpty() && !Test.isRunningTest()) update lstOnTapRoute;*/
        
        
    }
    /**
    * Method in charge of validate if there is already a tour assigned in the update and assign the tour to the route
    * @params:
      1.- listTourOld, listTourNew
    * @return void
    * 1.1
    **/
    public static void validateTourAssigned(list<ONTAP__Tour__c> listTourOld, list< ONTAP__Tour__c> listTourNew)
    {
    	VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
        for(ONTAP__Tour__c objTour: listTourNew) {
            setIdsRoutes.add(objTour.Route__c);
        }
        set<Id> setIdsRoutesFinish                  =  new set<Id>();
        set<Id> setIdsVsPlanAssign                  =  new set<Id>();
        List<ONTAP__Tour__c> lstRecentCreatedTour   =  new List<ONTAP__Tour__c>();
        map<id, ONTAP__Route__c>  mapIdRoutes       =  getVisiPlanxTours(setIdsRoutes);

        if(listTourNew != null && !listTourNew.isEmpty())
        {
            for(integer i = 0; i < listTourNew.size(); i++)
            {
                if((listTourOld[i].ONTAP__TourStatus__c != listTourNew[i].ONTAP__TourStatus__c)
                    && listTourNew[i].ONTAP__TourStatus__c == label.TourStatusAssigned)
                {
                    if(mapIdRoutes.get(listTourNew[i].Route__c).Tours__r.size() >= 1)
                    {
                        listTourNew[i].ONTAP__TourStatus__c.addError(label.error_assigned_tour);
                    }
                    else
                    {
                        setIdsVsPlanAssign.add(listTourNew[i].Route__c);
                    }
                }
                if((listTourOld[i].ONTAP__TourStatus__c != listTourNew[i].ONTAP__TourStatus__c)
                    && listTourNew[i].ONTAP__TourStatus__c == label.TourStatusFinished)
                {
                    setIdsRoutesFinish.add(listTourNew[i].Route__c);
                    listTourNew[i].ONTAP__IsActive__c = false;
                }
            }
        }

        if(setIdsRoutesFinish != null && !setIdsRoutesFinish.isEmpty())
        {
            map<Id, ONTAP__Route__c> mapIdRouteListTours   =  new map<Id, ONTAP__Route__c>([SELECT Id,
                                                            (SELECT Id, CreatedDate, ONTAP__TourDate__c, ONTAP__TourStatus__c, TourSubStatus__c, ONTAP__IsActive__c 
                                                            FROM Tours__r
                                                            WHERE ONTAP__TourStatus__c = 'Created'
                                                            ORDER BY ONTAP__TourDate__c ASC LIMIT 1)
                                                            FROM ONTAP__Route__c
                                                            WHERE id IN : setIdsRoutesFinish]);
            if(mapIdRouteListTours != null && !mapIdRouteListTours.isEmpty())
            {
                for(Id idVsPlan: mapIdRouteListTours.keyset())
                {
                    for(ONTAP__Tour__c objTour: mapIdRouteListTours.get(idVsPlan).Tours__r)
                    {
                        objTour.ONTAP__TourStatus__c    =   visitPlanSettings.AssignedTourStatus__c;
                        objTour.TourSubStatus__c		= 	visitPlanSettings.AssignedTourSubStatus__c;
                        objTour.ONTAP__IsActive__c		= 	true;
                        
                        lstRecentCreatedTour.add(objTour);
                    }
                }
            }
        }
        if((setIdsRoutesFinish != null && !setIdsRoutesFinish.isEmpty())
            || (setIdsVsPlanAssign != null && !setIdsRoutesFinish.isEmpty()))
        {
            lstRecentCreatedTour = lstRecentCreatedTour != null ? lstRecentCreatedTour : null;
            ID jobID = System.enqueueJob(new TourOperations_cls(setIdsRoutesFinish, lstRecentCreatedTour));
        }
    }
    /**
    * Method for consulting route plans
    * @params:
      1.- setIds
    * @return void
    * 1.1
    **/
    public static map<id, ONTAP__Route__c> getVisiPlanxTours(set<id> setIdsRoutes)
    {
        map<id, ONTAP__Route__c>  mapIdRoutes       =   new map<id, ONTAP__Route__c>([SELECT Id, ONTAP__TourId__c,
                                                                                        (SELECT Id, ONTAP__TourId__c, ONTAP__TourStatus__c, Route__c
                                                                                        FROM Tours__r
                                                                                        WHERE ONTAP__TourStatus__c =: label.TourStatusAssigned)
                                                                                    FROM ONTAP__Route__c WHERE Id IN: setIdsRoutes]);

        System.debug('mapIdRoutes===>>>'+mapIdRoutes);
        return mapIdRoutes;
    }
    
    
    /**
     * @description Method to send tours an events to Heroku
     * @param 
     * @return Boolean
     * @version 1.0
     **/
    @RemoteAction
    webservice static void sendToursToHeroku(){  
    	//Obtain the record types that will be sinchronize with heroku
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	
    	map<Id, ONTAP__Tour__c> mapTours = new map<Id, ONTAP__Tour__c>([
    		Select 	Id
    		From	ONTAP__Tour__c
    		Where	ONTAP__IsActive__c = true And SynchronizedHeroku__c = false And
    				RecordType.DeveloperName = :lstRecordType
    	]);
    	
    	TourOperations_cls.insertExternalTours(mapTours.keySet(), true); 
    }
    
    /**
     * @description Method to update the visit period config
     * @param num Indicates the number of days that the process generate a visit list
     * @return void
     * @version 1.0
     **/
    @RemoteAction
    webservice static void updateVisitPeriodConfig(Integer num){
    	SyncHerokuParams__c obj = SyncHerokuParams__c.getAll().get('SyncToursEvents');
		obj.VisitPeriodConfig__c = Integer.valueOf(num);
		update obj;
    }
}