/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_TelecollectionCampaignTgr_tst {

    @testSetup static void setup() {
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        Telecollection_Campaign__c objTelecollectionC = new Telecollection_Campaign__c();
        objTelecollectionC.Name =  'CampaignTest';
        objTelecollectionC.Start_Date__c = System.today();
        objTelecollectionC.End_Date__c = System.today()+1;
        objTelecollectionC.Active__c = true;
        objTelecollectionC.SoqlFilters__c='';
        insert  objTelecollectionC;
    }

    static testMethod void TestInsertCampaign() {
        ISSM_TelecollectionCampaignHandler_cls objTelecollectionCampaignHandler = new ISSM_TelecollectionCampaignHandler_cls();
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        Telecollection_Campaign__c objTelecollectionUpdate = new Telecollection_Campaign__c();
        Telecollection_Campaign__c objTelecollectionC = new Telecollection_Campaign__c();
        objTelecollectionC.Name =  'CampaignTest';
        objTelecollectionC.Start_Date__c = System.today();
        objTelecollectionC.End_Date__c = System.today()+1;
        objTelecollectionC.Active__c = true;
        objTelecollectionC.SoqlFilters__c='';
        
        Test.startTest(); 
            insert  objTelecollectionC;
        Test.stopTest();      
    }

    static testMethod void TestEditCampaign() {
        ISSM_TelecollectionCampaignHandler_cls objTelecollectionCampaignHandler = new ISSM_TelecollectionCampaignHandler_cls();
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        Telecollection_Campaign__c objTelecollectionC = [select name, Start_Date__c, End_Date__c, Active__c, SoqlFilters__c from Telecollection_Campaign__c where name ='CampaignTest' Limit 1];
        
        
        Test.startTest(); 
            objTelecollectionC.Active__c = false;
            update objTelecollectionC;
            
        Test.stopTest();      
    }

    static testMethod void TestDeleteCampaign() {
        ISSM_TelecollectionCampaignHandler_cls objTelecollectionCampaignHandler = new ISSM_TelecollectionCampaignHandler_cls();
        List<ISSM_TriggerFactory__c> fn_CreateTriggerFactory =ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        Telecollection_Campaign__c objTelecollectionC = [select name, Start_Date__c, End_Date__c, Active__c, SoqlFilters__c from Telecollection_Campaign__c where name ='CampaignTest' Limit 1];
        
        
        Test.startTest(); 
            delete objTelecollectionC;
            
        Test.stopTest();      
    }
}