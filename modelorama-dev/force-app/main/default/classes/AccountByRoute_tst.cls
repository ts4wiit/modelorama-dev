/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Apex class to test methods for AccountByRoute_cls functionality

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    05/09/2017 Daniel Peñaloza            Created Class
1.1    19/04/2018 Rodrigo Resendiz           Add methods to set BDR as the Owner of Accounts W-010925,W-011098
*******************************************************************************/

@isTest
private class AccountByRoute_tst {
    private static final Integer NUM_ACCOUNTS = 10;
    private static final Boolean[] LST_FRECUENCIES = new Boolean[] { true, true, true, true, true, true, true };
    private static final AccountRouteSettings__mdt accountSettings = DevUtils_cls.getAccountRouteSettings();
    private static final String SUPERVISOR_ROLE = accountSettings.SupervisorTeamMemberRole__c;
    private static final String SALESAGENT_ROLE = accountSettings.SalesAgentTeamMemberRole__c;

    @testSetup static void setup() {
        User supervisor = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 11111);
        insert supervisor;
        User integrationUser = TestUtils_tst.generateIntegrationUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 11111);
        insert integrationUser; 

        // Create Route
        ONTAP__Route__c route = TestUtils_tst.generateFullRoute();
        route.Supervisor__c = supervisor.Id;
        insert route;
        
        // Create Accounts
        Account[] lstAccounts = new List<Account>();
        for (Integer i = 0; i < NUM_ACCOUNTS; i++) {
            Account acc = TestUtils_tst.generateAccount(null, null, i, route.ONTAP__SalesOffice__c);
            lstAccounts.add(acc);
        }
        insert lstAccounts;
    }

    /**
     * Should add Route Supervisor and Sales Agent to Account Teams
     */
	@isTest static void should_add_users_to_account_teams() {
		// Delete Current Account team members
        delete [SELECT Id FROM AccountTeamMember];

        ONTAP__Route__c route = [SELECT Id, Supervisor__c, RouteManager__c, ONTAP__SalesOffice__c FROM ONTAP__Route__c where ServiceModel__c!='BDR' AND ServiceModel__c!='Telesales' LIMIT 1];

        Account[] lstAccounts = [
            SELECT Id,
                Name,
                RecordTypeId,
                ONTAP__Preferred_Service_Days__c,
                ONTAP__SAPCustomerId__c,ONTAP__SAP_Number__c,
                ISSM_SalesOffice__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Name != 'Test Account 123'
        ];
        System.assertEquals(NUM_ACCOUNTS, lstAccounts.size());

        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, LST_FRECUENCIES);
            lstAccountsByRoute.add(accByRoute);
        }

        Test.startTest();
            insert lstAccountsByRoute;
        Test.stopTest();

        // Verify results
        AccountTeamMember[] lstSalesAgents = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.RouteManager__c
                AND TeamMemberRole = :SALESAGENT_ROLE
        ];

        AccountTeamMember[] lstSupervisors = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.Supervisor__c
                AND TeamMemberRole = :SUPERVISOR_ROLE
        ];

        System.assertEquals(NUM_ACCOUNTS, lstSalesAgents.size());
        System.assertEquals(NUM_ACCOUNTS, lstSupervisors.size());
        delete route;
	}

    /**
     * Should remove Route Supervisor and Sales Agent from Account Teams on Accounts by Route Deletion
     */
	@isTest static void should_remove_users_from_account_team() {
		// Delete Current Account team members
        delete [SELECT Id FROM AccountTeamMember];

        ONTAP__Route__c route = [SELECT Id, Supervisor__c, RouteManager__c, ONTAP__SalesOffice__c FROM ONTAP__Route__c WHERE ServiceModel__c!='BDR' AND ServiceModel__c!='Telesales' LIMIT 1];

        Account[] lstAccounts = [
            SELECT Id,
                Name,
                RecordTypeId,
                ONTAP__Preferred_Service_Days__c,
                ONTAP__SAPCustomerId__c,
                ISSM_SalesOffice__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Name != 'Test Account 123'
        ];
        System.assertEquals(NUM_ACCOUNTS, lstAccounts.size());

        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, LST_FRECUENCIES);
            lstAccountsByRoute.add(accByRoute);
        }

        insert lstAccountsByRoute;

        // Verify results
        AccountTeamMember[] lstSalesAgents = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.RouteManager__c
                AND TeamMemberRole = :SALESAGENT_ROLE
        ];

        AccountTeamMember[] lstSupervisors = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.Supervisor__c
                AND TeamMemberRole = :SUPERVISOR_ROLE
        ];

        System.assertEquals(NUM_ACCOUNTS, lstSalesAgents.size());
        System.assertEquals(NUM_ACCOUNTS, lstSupervisors.size());

        Test.startTest();
            delete lstAccountsByRoute;
        Test.stopTest();

        // Verify results
        lstSalesAgents = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.RouteManager__c
                AND TeamMemberRole = :SALESAGENT_ROLE
        ];

        lstSupervisors = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.Supervisor__c
                AND TeamMemberRole = :SUPERVISOR_ROLE
        ];

        System.assertEquals(0, lstSalesAgents.size());
        System.assertEquals(0, lstSupervisors.size());
	}

    /**
     * Should remove Route Supervisor and Sales Agent from Account Teams on Route Deletion
     */
    @isTest static void should_remove_users_from_account_team_on_route_deletion() {
        // Delete Current Account team members
        delete [SELECT Id FROM AccountTeamMember];

        ONTAP__Route__c route = [SELECT Id, Supervisor__c, RouteManager__c, ONTAP__SalesOffice__c FROM ONTAP__Route__c WHERE ServiceModel__c!='BDR' AND ServiceModel__c!='Telesales' LIMIT 1];

        Account[] lstAccounts = [
            SELECT Id,
                Name,
                RecordTypeId,
                ONTAP__Preferred_Service_Days__c,
                ONTAP__SAPCustomerId__c,
                ISSM_SalesOffice__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Name != 'Test Account 123'
        ];
        System.assertEquals(NUM_ACCOUNTS, lstAccounts.size());

        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, LST_FRECUENCIES);
            lstAccountsByRoute.add(accByRoute);
        }

        insert lstAccountsByRoute;

        // Verify results
        AccountTeamMember[] lstSalesAgents = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.RouteManager__c
                AND TeamMemberRole = :SALESAGENT_ROLE
        ];

        AccountTeamMember[] lstSupervisors = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.Supervisor__c
                AND TeamMemberRole = :SUPERVISOR_ROLE
        ];

        System.assertEquals(NUM_ACCOUNTS, lstSalesAgents.size());
        System.assertEquals(NUM_ACCOUNTS, lstSupervisors.size());

        Test.startTest();
            delete route;
        Test.stopTest();

        // Verify results
        lstSalesAgents = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.RouteManager__c
                AND TeamMemberRole = :SALESAGENT_ROLE
        ];

        lstSupervisors = [
            SELECT Id
            FROM AccountTeamMember
            WHERE AccountId IN :lstAccounts
                AND UserId = :route.Supervisor__c
                AND TeamMemberRole = :SUPERVISOR_ROLE
        ];

        System.assertEquals(0, lstSalesAgents.size());
        System.assertEquals(0, lstSupervisors.size());
    }
    /**
     * Should remove Route Supervisor and Sales Agent from Account Teams on Route Deletion
     * (1.1)
     */
    @isTest static void should_set_bdr_as_owner_of_accts() {
		
        User supervisor = [SELECT Id FROM User WHERE Profile.Name =:TestUtils_tst.PROFILE_NAME_PRESALES limit 1];
        test.startTest();
        //Create BDR route
        ONTAP__Route__c route = TestUtils_tst.generateFullRouteBDR();
        route.Supervisor__c = supervisor.Id;
        insert route;
         
		Account[] lstAccounts = new List<Account>();
        for (Integer i = 0; i < NUM_ACCOUNTS; i++) {
            Account acc = TestUtils_tst.generateAccount(null, null, i, route.ONTAP__SalesOffice__c);
            lstAccounts.add(acc);
        }
        insert lstAccounts;
        System.assertEquals(false, (lstAccounts.isEmpty()));
        
        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, LST_FRECUENCIES);
            lstAccountsByRoute.add(accByRoute);
        }
        insert lstAccountsByRoute;
        
        delete route;
        
        Test.stopTest();
    }
    /**
     * Should remove Route Supervisor and Sales Agent from Account Teams on Route Deletion
     * (1.1)
     */
    @isTest static void should_add_telesales_to_acct_team() {
		
        User supervisor = [SELECT Id FROM User WHERE Profile.Name =:TestUtils_tst.PROFILE_NAME_PRESALES limit 1];
        test.startTest();
        //Create Telesales route
        ONTAP__Route__c route = TestUtils_tst.generateFullRouteTelesale();
        route.Supervisor__c = supervisor.Id;
        insert route;
         
		Account[] lstAccounts = new List<Account>();
        for (Integer i = 0; i < NUM_ACCOUNTS; i++) {
            Account acc = TestUtils_tst.generateAccount(null, null, i, route.ONTAP__SalesOffice__c);
            lstAccounts.add(acc);
        }
        insert lstAccounts;
        System.assertEquals(false, (lstAccounts.isEmpty()));
        
        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, LST_FRECUENCIES);
            lstAccountsByRoute.add(accByRoute);
        }
        insert lstAccountsByRoute;
        
        delete route;
        
        Test.stopTest();
    }
}