/******************************************************************************* 
* Developed by	: 	Avanxo México
* Author				: 	Oscar Alvarez
* Project			: 	AbInbev - Trade Revenue Management
* Description		: 	Schedulable for integration with mulesoft and sending the combo to SAP. The schedule has the scope of MODIFICATION
*						Run every day at 12:00:00 a.m.
*
* No.       Date              Author                      Description
* 1.0    22-Junio-2018      Oscar Alvarez                   creation
*******************************************************************************/
global class ISSM_IntegrateCmbCancelMulsoft_sch implements Schedulable {
    
    @TestVisible
    public static final String CRON_EXPR = Label.TRM_CronExprIntegrateCmbCancel;
    /*
    * Method Name	: executeScheduleCancel
    * creation      : Call this from Anonymous Apex to schedule at the default regularity
    */
    global static String executeScheduleCancel() {
        ISSM_IntegrateCmbCancelMulsoft_sch job = new ISSM_IntegrateCmbCancelMulsoft_sch();
        return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchApexIntgrCmbCancel + System.now()) : Label.TRM_NameSchApexIntgrCmbCancel), CRON_EXPR, job);
    }
    global void execute(SchedulableContext sc) {
        
        //We look for the predetermined combos
        List<ISSM_Combos__c> lstCanceledCmbs = [SELECT ISSM_ExternalKey__c FROM ISSM_Combos__c WHERE ISSM_SynchronizedWithSAP__c = false AND ISSM_StatusCombo__c = 'ISSM_Cancelled' AND ISSM_CancellationDate__c = TODAY];
        System.debug('lstCanceledCmbs --->' + lstCanceledCmbs.size()+' Detail -->' + lstCanceledCmbs);
        //Variables by default
        String cancel = Label.TRM_OperationCancel;
        
        //Process the Canceled
        for(ISSM_Combos__c combo : lstCanceledCmbs){
            String strSerializeJson = Json.serialize(new ISSM_SendComboToMulesoft_cls.WrpCombo(combo.ISSM_ExternalKey__c,cancel));
            ISSM_SendComboToMulesoft_cls.SendComboToMulesoftIntegrate(strSerializeJson,cancel);
        }
    }
}