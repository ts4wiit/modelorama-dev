/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Batch use to realize the external objects sincronization

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_Account_bch implements Database.Batchable<sObject> {
	
	String query;
	ISSM_Account_cls accountCls;
	Map<String, Account> mapAccounts;
	Map<Id, Account> mapAccountsId;
	Set<String> setSapNumberAccount;
	List<ISSM_ObjectInterface_cls> sObjectInterfaceList;

	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 */
	public ISSM_Account_bch(List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam ) {
		sObjectInterfaceList =  sObjectInterfaceListParam;
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		query='SELECT Id,ONTAP__SAPCustomerId__c FROM Account where ONTAP__SAPCustomerId__c != \'\' and RecordTypeId=\''+ devRecordTypeId +'\'';

	}
	
	/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  String sapCustomerId Specific sap customer id to sincronice
	 */
	public ISSM_Account_bch(List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam, String sapCustomerId ) {
		sObjectInterfaceList =  sObjectInterfaceListParam;
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		query='SELECT Id,ONTAP__SAPCustomerId__c FROM Account where ONTAP__SAPCustomerId__c != \'\' and ONTAP__SAPCustomerId__c = \''+ sapCustomerId +'\' and RecordTypeId=\''+ devRecordTypeId +'\'';

	}

		/**
	 * Class constructor		                             
	 * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
	 * @param  String sapCustomerId Specific sap customer id to sincronice
	 */
	public ISSM_Account_bch(List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam, Boolean salesOffice, String SalesOfficeParam ) {
		sObjectInterfaceList =  sObjectInterfaceListParam;
		Id devRecordTypeId;
		String stSalesOffice =Label.ISSM_SalesOfficeToProcess;
		if(SalesOfficeParam != null && SalesOfficeParam != ''){
			stSalesOffice = SalesOfficeParam;
		}
		stSalesOffice = stSalesOffice.replace(',', '\',\'');
		stSalesOffice = '\''+ stSalesOffice +'\'';
		try{
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		query='SELECT Id,ONTAP__SAPCustomerId__c FROM Account where ONTAP__SAPCustomerId__c != \'\' and RecordTypeId=\''+ devRecordTypeId +'\' and ONTAP__SalesOffId__c IN ('+ stSalesOffice +')';

	}

	/**
	 * Start method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('### ISSM_Account_bch start()'); 
		return Database.getQueryLocator(query);
	}

	/**
	 * Execute method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		System.debug('### ISSM_Account_bch execute()'); 
   		system.debug( '\n\n  ****** scope = ' + scope+'\n\n' );
   		accountCls = new ISSM_Account_cls();
   		mapAccounts = accountCls.getMapAccountSapCustomerId(scope);
   		for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
   			sObjectInterface.createObject(mapAccounts);
   		}
   		for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
   			sObjectInterface.save();
   		}

	}

	/**
	 * Finish method for the batch execution
	 * @param  Database.BatchableContext BC
	 */
	global void finish(Database.BatchableContext BC) {
		System.debug('### ISSM_Account_bch: finish() called');
	}
	
}