/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Scheduler to program the execution of the orders cleansing 

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       03-08-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
global class ISSM_OrderErase_sh implements Schedulable {
	/**
	 * Execute method for the scheduler execution
	 * @param  Database.BatchableContext BC
	 */
	global void execute(SchedulableContext sc) {
		Id devRecordTypeId;
		try{
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
		}
		system.debug('id ' + devRecordTypeId);

		AggregateResult[] groupedResults=[SELECT count(Id) cant,ONTAP__OrderAccount__c accountId FROM ONTAP__Order__c where  RecordTypeId =:devRecordTypeId GROUP BY ONTAP__OrderAccount__c HAVING COUNT(Id) > 8  ];
		System.debug(groupedResults);
		Set<String> setId = new Set<String>();
		for (AggregateResult ar : groupedResults)  {
		    if(ar.get('accountId')!= null){
		    	setId.add((String)ar.get('accountId'));
		    }
		}
		System.debug(setId);
		Database.executeBatch(new ISSM_OrderErase_bch(setId), 500);
	}
}