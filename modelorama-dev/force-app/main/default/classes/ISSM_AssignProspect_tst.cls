/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_AssignProspect_tst {
/**************************************   Assign Prospect **************************************************/
    /*
    Asignacion de prospecto pero sin equipo de cuenta
    */
    static testMethod void  AssignProspectNotAccountTeamMember() {
        String RecordTypeAccountIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        ISSM_CreateDataTest_cls.fn_CreatetProspecSalesOffice(true,'System Administrator');
       
        //Creamos MDRM_Modelorama_Postal_Code__c Agency
        MDRM_Modelorama_Postal_Code__c objModeloramaPostalCode = new  MDRM_Modelorama_Postal_Code__c();
            objModeloramaPostalCode.MDRM_Postal_Code__c = '57710';
            objModeloramaPostalCode.Name = 'ModeloramaPstalC';
            objModeloramaPostalCode.MDRM_City__c = 'Cuauhtémoc';
            insert objModeloramaPostalCode;
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;
        
        //Creamos QueryColonyxAgency
        ISSM_ColonyxAgency__c objQueryColonyxAgency = new ISSM_ColonyxAgency__c();
        objQueryColonyxAgency.ISSM_ColonyNamePC__c =  objModeloramaPostalCode.Id;
        objQueryColonyxAgency.ISSM_AgencyName__c = objAccountSalesOffice.Id;
        insert objQueryColonyxAgency;
            
        Test.startTest();   
            //Cuenta tipo provider
            Account objAccount = new  Account();
                objAccount.Name = 'Account Prospect';
                objAccount.ONTAP__Street__c = 'SMateo';
                objAccount.ONTAP__Street_Number__c = '233';
                objAccount.ISSM_ColonyPC__c = objModeloramaPostalCode.Id;
                objAccount.ONTAP__Classification__c = 'Botella Abierta';
                objAccount.ONTAP__Segment__c = 'Buena Carta';
                objAccount.ONTAP__Main_Phone__c = '555555555';
                objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
                objAccount.RecordTypeId = RecordTypeAccountIdProspect;
                insert objAccount;
        Test.stopTest();        
    }
    
    /*
    Asignacion de prospecto Con equipo de cuenta
    */
    static testMethod void  AssignProspectAccountTeamMember() {
        AccountTeamMember accountTeamMember;
        User user;
        String RecordTypeAccountIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        ISSM_CreateDataTest_cls.fn_CreatetProspecSalesOffice(true,'System Administrator');
       
        //Creamos MDRM_Modelorama_Postal_Code__c Agency
        MDRM_Modelorama_Postal_Code__c objModeloramaPostalCode = new  MDRM_Modelorama_Postal_Code__c();
            objModeloramaPostalCode.MDRM_Postal_Code__c = '57710';
            objModeloramaPostalCode.Name = 'ModeloramaPstalC';
            objModeloramaPostalCode.MDRM_City__c = 'Cuauhtémoc';
            insert objModeloramaPostalCode;
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;
            
        //Creamos Equipo de cuenta para la cuenta de Sales office
          user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
          accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, objAccountSalesOffice.Id, 'Gerente', user.Id);
        
        //Creamos QueryColonyxAgency
        ISSM_ColonyxAgency__c objQueryColonyxAgency = new ISSM_ColonyxAgency__c();
        objQueryColonyxAgency.ISSM_ColonyNamePC__c =  objModeloramaPostalCode.Id;
        objQueryColonyxAgency.ISSM_AgencyName__c = objAccountSalesOffice.Id;
        insert objQueryColonyxAgency;
        
        Test.startTest();   
            //Cuenta tipo provider
            Account objAccount = new  Account();
                objAccount.Name = 'Account Prospect';
                objAccount.ONTAP__Street__c = 'SMateo';
                objAccount.ONTAP__Street_Number__c = '233';
                objAccount.ISSM_ColonyPC__c = objModeloramaPostalCode.Id;
                objAccount.ONTAP__Classification__c = 'Botella Abierta';
                objAccount.ONTAP__Segment__c = 'Buena Carta';
                objAccount.ONTAP__Main_Phone__c = '555555555';
                objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
                objAccount.RecordTypeId = RecordTypeAccountIdProspect;
                insert objAccount;
        Test.stopTest();    
    }
     /*
    Asignacion de prospecto Con equipo de cuenta
    */
    static testMethod void  AssignProspectNoColonyxAgency() {
        String RecordTypeAccountIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        ISSM_CreateDataTest_cls.fn_CreatetProspecSalesOffice(true,'System Administrator');
       
        //Creamos MDRM_Modelorama_Postal_Code__c Agency
        MDRM_Modelorama_Postal_Code__c objModeloramaPostalCode = new  MDRM_Modelorama_Postal_Code__c();
            objModeloramaPostalCode.MDRM_Postal_Code__c = '57710';
            objModeloramaPostalCode.Name = 'ModeloramaPstalC';
            objModeloramaPostalCode.MDRM_City__c = 'Cuauhtémoc';
            insert objModeloramaPostalCode;
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;
            
      Test.startTest();
            //Cuenta tipo provider
            Account objAccount = new  Account();
                objAccount.Name = 'Account Prospect';
                objAccount.ONTAP__Street__c = 'SMateo';
                objAccount.ONTAP__Street_Number__c = '233';
                objAccount.ISSM_ColonyPC__c = objModeloramaPostalCode.Id;
                objAccount.ONTAP__Classification__c = 'Botella Abierta';
                objAccount.ONTAP__Segment__c = 'Buena Carta';
                objAccount.ONTAP__Main_Phone__c = '555555555';
                objAccount.ISSM_SalesOffice__c = objAccountSalesOffice.Id;
                objAccount.RecordTypeId = RecordTypeAccountIdProspect;
                insert objAccount;
        Test.stopTest();
    }
    
  /**************************************   AssignSaleOffice **************************************************/
    /*
    Asignacion de SalesOffice sin oficina de venta ni organizacion de venta
    */
    static testMethod void  AssignSalesOfficeNotSalesOfAndSalesOrg() {
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name Account';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOffice;
    }
    /*
    Asignacion de SalesOffice con oficina de venta y con organizacion de venta
    */
    static testMethod void  AssignSalesOfficeSalesOfAndSalesOrg() {
        String RecordTypeAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeAccountSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();
        
        //Creamos Cuenta con SalesOffice Parent
        Account  objAccountSalesOfficeParent = new Account();
            objAccountSalesOfficeParent.Name = 'Name SalesOffice Parent';
            objAccountSalesOfficeParent.RecordTypeId = RecordTypeAccountSalesOffice;
            insert objAccountSalesOfficeParent;
            
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
            objAccountSalesOffice.Name = 'Name SalesOffice';
            objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
            objAccountSalesOffice.ONTAP__SalesOffId__c = 'SO01';
            objAccountSalesOffice.ParentId = objAccountSalesOfficeParent.Id;
            insert objAccountSalesOffice;
        
            
        //Creamos Cuenta con SalesOrg
        Account  objAccountSalesOrg = new Account();
            objAccountSalesOrg.Name = 'Name Sales Org';
            objAccountSalesOrg.RecordTypeId = RecordTypeAccountSalesOrg;
            objAccountSalesOrg.ONTAP__SalesOgId__c = 'OR01';
            insert objAccountSalesOrg;
        
        //Creamos Cuenta con Tipo de registro Account
        Account  objAccount = new Account();
            objAccount.Name = 'Name Account';
            objAccount.RecordTypeId = RecordTypeAccount;
            objAccount.ONTAP__SalesOffId__c = 'SO01';
            objAccount.ONTAP__SalesOgId__c ='OR01';
            insert objAccount;
    }
  
    static testMethod void AssignSalesOfficeByGeo() {
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user1 = new User(Alias = 'admin', Email='systemadmin@testorg.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@testorg.com');
        insert user1;
        
        String RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Name Account 1';
        objAccount1.RecordTypeId = RecordTypeAccountSalesOffice;
        objAccount1.ONTAP__SalesOffId__c = 'SO01';
        objAccount1.ONTAP__SalesOgId__c ='OR01';
        objAccount1.ONTAP__Latitude__c = '19.4341834';
        objAccount1.ONTAP__Longitude__c = '-99.9116125';
        objAccount1.ONTAP__Classification__c = 'Botella Abierta';
        insert objAccount1;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Name Account 2';
        objAccount2.RecordTypeId = RecordTypeAccountSalesOffice;
        objAccount2.ONTAP__SalesOffId__c = 'SO01';
        objAccount2.ONTAP__SalesOgId__c ='OR01';
        objAccount2.ONTAP__Latitude__c = '19.4341834';
        objAccount2.ONTAP__Longitude__c = '-98.2116125';
        objAccount2.ONTAP__Classification__c = 'BA/BC';
        insert objAccount2;
        
        ISSM_CS_Lead__c objCSLead = new ISSM_CS_Lead__c();
        objCSLead.Name = 'AssignSOToLead';
        objCSLead.ISSM_Distancia_maxima__c = 25.00;
        insert objCSLead;
        
        List<Account> lstDataLead = new List<Account>();
        Account objProspect1 = new Account();
        objProspect1.Name = 'Name Prospect 1';
        objProspect1.RecordTypeId = RecordTypeProspect;
        objProspect1.ISSM_LatProspect__c = 19.4341834;
        objProspect1.ISSM_LongProspect__c = -99.2116125;
        objProspect1.ONTAP__Classification__c = 'Botella Abierta';
        objProspect1.ISSM_Channel__c = 'B2B';
        insert objProspect1;
        lstDataLead.add(objProspect1);
        
        Account objProspect2 = new Account();
        objProspect2.Name = 'Name Prospect 2';
        objProspect2.RecordTypeId = RecordTypeProspect;
        objProspect2.ISSM_LatProspect__c = 19.4341834;
        objProspect2.ISSM_LongProspect__c = -99.2116125;
        objProspect1.ONTAP__Classification__c = 'Botella Cerrada';
        insert objProspect2;
        
        Test.startTest();
        
        ISSM_Google__c data = new ISSM_Google__c();
        data.Name = 'Test API 1';
        data.API__c = '1234ABCD';
        data.IsActive__c = true;
        insert data;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(objAccount1);
		ISSM_AssignProspect_cls extension = new ISSM_AssignProspect_cls(controller);
        
        PageReference pageRef1 = Page.ISSM_AssignProspectByGeo_pag;
        pageRef1.getParameters().put('latitude', '19.4341834');
        pageRef1.getParameters().put('longitude', '-99.2116125');
        pageRef1.getParameters().put('average', '-79.7774291');
        Test.setCurrentPage(pageRef1);
        
        extension.AssignSaleOfficeByGeo();
        
        PageReference pageRef2 = Page.ISSM_AssignAddressToLead_pag;
        pageRef2.getParameters().put('route', 'Calle');
        pageRef2.getParameters().put('street_number', '1234');
        pageRef2.getParameters().put('postal_code', '1234');
        Test.setCurrentPage(pageRef2);
        
        extension.callMethods();
        extension.callSetAddressLead();
        
        Test.stopTest();
        
    }
  
}