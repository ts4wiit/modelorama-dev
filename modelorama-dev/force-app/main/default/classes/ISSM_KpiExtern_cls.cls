/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to get external kpi

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_KpiExtern_cls implements ISSM_ObjectExternInterface_cls{
	
	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setSapCustomerId set of sap customer id to request
	 * @param  Boolean isDeleted if the record must be deleted
	 */
	public static List<salesforce_ontap_kpi_c__x> getList(Set<String> setSapCustomerId, Boolean isDeleted){
		if(!isDeleted){
			if(test.isRunningTest()){
				List<salesforce_ontap_kpi_c__x> kpi_lst = new List<salesforce_ontap_kpi_c__x>();
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '10',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '11',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '12',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '13',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '14',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '15',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '16',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '17',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '18',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '19',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '20',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '21',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '22',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '23',isdeleted__c=false, issm_sapcustomerid_c__c='1029120920192'));
				return kpi_lst;
			} else {
				return [SELECT createdbyid__c,createdby_ontap_sap_account_id_c__c,createddate__c,DisplayUrl,ExternalId,
				hc_err__c,hc_lastop__c,Id,id__c,isdeleted__c,issm_associatedoffice_c__c,issm_associatedoffice_r_oncall_customer__c,
				issm_beercurrentmonthboxconversion_c__c,issm_beercurrentmonthgoal_c__c,issm_beercurrentyearboxconversion_c__c,
				issm_beercurrentyeargoal_c__c,issm_beergoalreductionreason_c__c,issm_beerpreviousmonthboxconversion_c__c,
				issm_beerpreviousmonthgoal_c__c,issm_beerpreviousyearboxconversion_c__c,issm_beerpreviousyeargoal_c__c,
				issm_countrycode_c__c,issm_ordertype_c__c,issm_salesoffid_c__c,issm_sapcustomerid_c__c,issm_sap_number_c__c,
				issm_sodacurrentmonthboxconversion_c__c,issm_sodacurrentmonthgoal_c__c,issm_sodacurrentyearboxconversion_c__c,
				issm_sodacurrentyeargoal_c__c,issm_sodagoalreductionreason_c__c,issm_sodapreviousmonthboxconversion_c__c,
				issm_sodapreviousmonthgoal_c__c,issm_sodapreviousyearboxconversion_c__c,issm_sodapreviousyeargoal_c__c,
				issm_uniqueid_c__c,issm_watercurrentmonthboxconversion_c__c,issm_watercurrentmonthgoal_c__c,
				issm_watercurrentyearboxconversion_c__c,issm_watercurrentyeargoal_c__c,issm_watergoalreductionreason_c__c,
				issm_waterpreviousmonthboxconversion_c__c,issm_waterpreviousmonthgoal_c__c,issm_waterpreviousyearboxconversion_c__c,
				issm_waterpreviousyeargoal_c__c,lastmodifiedbyid__c,lastmodifiedby_ontap_sap_account_id_c__c,lastmodifieddate__c,
				name__c,ontap_account_id_c__c,ontap_account_id_r_oncall_customer_numbe__c,ontap_actual_c__c,ontap_billtosapcustomerid_c__c,
				ontap_categorie_c__c,ontap_days_passed_in_period_c__c,ontap_kpi_end_date_c__c,ontap_kpi_id_c__c,ontap_kpi_name_c__c,
				ontap_kpi_parent_id_c__c,ontap_kpi_start_date_c__c,ontap_percentage_increase_lastyear_c__c,ontap_target_c__c,
				ontap_total_days_in_period_c__c,ontap_unit_of_measure_c__c,ontap_user_id_c__c,ontap_user_id_r_ontap_sap_account_id_c__c,
				sfid__c,systemmodstamp__c FROM salesforce_ontap_kpi_c__x
				where issm_sapcustomerid_c__c in :setSapCustomerId and systemmodstamp__c >= YESTERDAY];
			}
		} else {
			if(test.isRunningTest()){
				List<salesforce_ontap_kpi_c__x> kpi_lst = new List<salesforce_ontap_kpi_c__x>();
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '10',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '11',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '12',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '13',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '14',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '15',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '16',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '17',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '18',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '19',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '20',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '21',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '22',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				kpi_lst.add(new salesforce_ontap_kpi_c__x(ontap_kpi_id_c__c = '23',isdeleted__c=true, issm_sapcustomerid_c__c='1029120920192'));
				return kpi_lst;
			} else {
				return [SELECT createdbyid__c,createdby_ontap_sap_account_id_c__c,createddate__c,DisplayUrl,ExternalId,
				hc_err__c,hc_lastop__c,Id,id__c,isdeleted__c,issm_associatedoffice_c__c,issm_associatedoffice_r_oncall_customer__c,
				issm_beercurrentmonthboxconversion_c__c,issm_beercurrentmonthgoal_c__c,issm_beercurrentyearboxconversion_c__c,
				issm_beercurrentyeargoal_c__c,issm_beergoalreductionreason_c__c,issm_beerpreviousmonthboxconversion_c__c,
				issm_beerpreviousmonthgoal_c__c,issm_beerpreviousyearboxconversion_c__c,issm_beerpreviousyeargoal_c__c,
				issm_countrycode_c__c,issm_ordertype_c__c,issm_salesoffid_c__c,issm_sapcustomerid_c__c,issm_sap_number_c__c,
				issm_sodacurrentmonthboxconversion_c__c,issm_sodacurrentmonthgoal_c__c,issm_sodacurrentyearboxconversion_c__c,
				issm_sodacurrentyeargoal_c__c,issm_sodagoalreductionreason_c__c,issm_sodapreviousmonthboxconversion_c__c,
				issm_sodapreviousmonthgoal_c__c,issm_sodapreviousyearboxconversion_c__c,issm_sodapreviousyeargoal_c__c,
				issm_uniqueid_c__c,issm_watercurrentmonthboxconversion_c__c,issm_watercurrentmonthgoal_c__c,
				issm_watercurrentyearboxconversion_c__c,issm_watercurrentyeargoal_c__c,issm_watergoalreductionreason_c__c,
				issm_waterpreviousmonthboxconversion_c__c,issm_waterpreviousmonthgoal_c__c,issm_waterpreviousyearboxconversion_c__c,
				issm_waterpreviousyeargoal_c__c,lastmodifiedbyid__c,lastmodifiedby_ontap_sap_account_id_c__c,lastmodifieddate__c,
				name__c,ontap_account_id_c__c,ontap_account_id_r_oncall_customer_numbe__c,ontap_actual_c__c,ontap_billtosapcustomerid_c__c,
				ontap_categorie_c__c,ontap_days_passed_in_period_c__c,ontap_kpi_end_date_c__c,ontap_kpi_id_c__c,ontap_kpi_name_c__c,
				ontap_kpi_parent_id_c__c,ontap_kpi_start_date_c__c,ontap_percentage_increase_lastyear_c__c,ontap_target_c__c,
				ontap_total_days_in_period_c__c,ontap_unit_of_measure_c__c,ontap_user_id_c__c,ontap_user_id_r_ontap_sap_account_id_c__c,
				sfid__c,systemmodstamp__c FROM salesforce_ontap_kpi_c__x
				where issm_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted];
			}
		}
	}

	/**
	 * Get a set of external id for the object		                             
	 * @param  List<sObject> sObjectList list of objects to return the set
	 */
	public static Set<String> getSetExternalId(List<sObject> sObjectList){
		Set<String> kpisSet = new Set<String>();
		salesforce_ontap_kpi_c__x kpis; 
		for(Integer i = 0; i < sObjectList.size(); i++){
				kpis = (salesforce_ontap_kpi_c__x)sObjectList.get(i);
		        kpisSet.add(kpis.ontap_kpi_id_c__c+'');
		}
		return kpisSet;
	}
}