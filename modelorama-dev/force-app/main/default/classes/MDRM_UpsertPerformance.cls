/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Upsert batch for Performance when a KPI changes.  

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       07/02/2018      Cindy Fuentes		           Upsert batch for Performance when a KPI changes.  
==============================================================================================================================
*********************************************************************************************************************************/
global class MDRM_UpsertPerformance implements Database.Batchable<sObject>, database.stateful{
    private String query;
    
    public MDRM_UpsertPerformance(){
        datetime today = datetime.now();
        //get the query from the controller
        query = 'select name,id, ONTAP__Account_id__r.Z019__c, lastmodifieddate, ONTAP__Account_id__c, ONTAP__Kpi_id__c, ONTAP__Actual__c, MDRM_Actual_Hectoliters__c from ONTAP__KPI__c where LastModifieddate = today';
        // query = 'select name,id, ONTAP__Account_id__r.Z019__c, lastmodifieddate, ONTAP__Account_id__c, ONTAP__Kpi_id__c, ONTAP__Actual__c, MDRM_Actual_Hectoliters__c from ONTAP__KPI__c';
    }
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        //First update Hectoliters
        List<ONTAP__KPI__c> KPIS = new List<ONTAP__KPI__c>();
        List<ONTAP__KPI__c> lstKPI = new List<ONTAP__KPI__c>();
        for(sObject s : scope){
            lstKPI.add( (ONTAP__KPI__c)s );
        }   
        if(lstKPI.size() >0 && lstKPI !=null){
            
            List<ONTAP__Product__c> lstProd = new List<ONTAP__Product__c>( [select ISSM_Factor_for_hectoliters__c,  ONTAP__ProductId__c, id,
                                                                            ISSM_Sector__c from ONTAP__Product__c 
                                                                            where ONTAP__ProductId__c != null and ISSM_Sector__c ='cerveza']);
            
            Map<integer,Decimal> mapKpi_idToFactor = new Map<integer,Decimal>();
            string Beer;
            for(ONTAP__Product__c Prod: lstProd){
                if(prod.ISSM_Sector__c=='Cerveza'){
                    beer = prod.ISSM_Sector__c;
                    mapKpi_idToFactor.put(integer.valueOf(Prod.ONTAP__ProductId__c), Prod.ISSM_Factor_for_hectoliters__c);
                    
                }
            }
            
            system.debug('IDs para conversion '+ mapKpi_idToFactor);
            set<id> idkpi = new set<id>();
            for(ONTAP__KPI__c Thek :lstKPI){
                idkpi.add(thek.id);
                integer KPIID = integer.valueof(thek.ONTAP__Kpi_id__c);
                system.debug('DECIMAL KPI '+ KPIID);
                if(mapKpi_idToFactor.containskey(KPIID)){
                    
                    if(thek.ONTAP__Actual__c== null){
                        thek.ONTAP__Actual__c=0;
                    }
                    if(mapKpi_idToFactor.get(KPIID)!=null){
                        Thek.MDRM_Actual_Hectoliters__c = thek.ONTAP__Actual__c * mapKpi_idToFactor.get(KPIID); 
                        system.debug('Hectolitros Actuales'+ Thek.MDRM_Actual_Hectoliters__c);
                    }
                }
            }
            
            update lstKPI;
            system.debug('KPI coon conversion '+ lstKPI);
            list<ONTAP__KPI__c> lstkpi2 = new list<ONTAP__KPI__c>([select id, MDRM_Actual_Hectoliters__c from ONTAP__KPI__c where id in:idkpi]);
            
            
            List<MDRM_Modelorama_Performance__c> lstPerf= new List<MDRM_Modelorama_Performance__c>();
            set<string> lstKeyKPI = new set<string>();
            map<string,ONTAP__KPI__c> mapKPI = new map<string,ONTAP__KPI__c>();
            map<id,ONTAP__KPI__c> mapKPIID = new map<id,ONTAP__KPI__c>();
            
            // Creates the Key for the upsert.
            for(ONTAP__KPI__c thisKPI: lstKPI){  
                string thismonth=string.valueof(thisKPI.lastmodifieddate.month());
                if(thismonth == string.valueof(11) ||thismonth == string.valueof(12)){
                    thismonth =string.valueof(thisKPI.lastmodifieddate.month());
                }else{
                    thismonth = string.valueof(0)+string.valueof(thisKPI.lastmodifieddate.month());
                }
                string accZ019 = string.valueof(thisKPI.ONTAP__Account_id__r.Z019__c);
                if(accZ019==null){
                    accZ019 ='00';
                }
                string theyear = string.valueof(thisKPI.lastmodifieddate.year());
                string keyKPI = accZ019+thismonth+theyear;
                lstKeyKPI.add(keyKPI);
                thiskpi.MDRM_Performance_External_ID__c =keyKPI;
                // Using the key, creates a new performance.
                mapKPI.put(keyKPI, new ONTAP__KPI__c(id = thisKPI.id));  
                mapKPIID.put(thiskpi.id, new ONTAP__KPI__c(id = thisKPI.id));
            }
        
            update lstKPI;
            
            if(beer =='Cerveza'){
 list<ONTAP__KPI__c> lstTHEKPIs = new list<ONTAP__KPI__c>([select id,MDRM_Performance_External_ID__c, lastmodifieddate,
                                                                      ONTAP__Account_id__r.Z019__c from ONTAP__KPI__c
                                                                      where id in: mapKPI.values()]);
                for(ONTAP__KPI__c thisUNIQUEKPI : lstTHEKPIs){
                    integer thismonth = thisUNIQUEKPI.lastmodifieddate.month();
                    string theyear = string.valueof(thisUNIQUEKPI.lastmodifieddate.year());
                    string keyKPI = string.valueof(thisUNIQUEKPI.ONTAP__Account_id__r.Z019__c)+string.valueof(thismonth)+theyear;
                    MDRM_Modelorama_Performance__c theperf =new MDRM_Modelorama_Performance__c(MDRM_Store__c = thisUNIQUEKPI.ONTAP__Account_id__c, 
                                                                                               MDRM_Month__c =thismonth,
                                                                                               MDRM_year__c = integer.valueof(theyear),
                                                                                               MDRM_External_ID__c = thisUNIQUEKPI.MDRM_Performance_External_ID__c);
                    lstPerf.add(theperf);
                }
                
              
                system.debug('PERFORMANCE' + lstPerf);
                Schema.SObjectField f = MDRM_Modelorama_Performance__c.Fields.MDRM_External_ID__c;
                Database.UpsertResult [] cr = Database.upsert(lstPerf, f, false);
                system.debug('UPSERT '+ cr);
                
                Map<String,id> mapPerf_id = new Map<String,id>();
                for(MDRM_Modelorama_Performance__c performance: lstPerf){
                    mapPerf_id.put(performance.MDRM_External_ID__c, performance.id);
                }
                
                for(ONTAP__KPI__c Thisk :lstKPI){
                    
                    string KPIPERFID = string.valueof(Thisk.MDRM_Performance_External_ID__c);
                    if(mapPerf_id.containskey(KPIPERFID)){
                        ThisK.MDRM_Modelorama_Performance__c = mapPerf_id.get(KPIPERFID);
                    }
                }
                
                update lstKPI;
                system.debug('KPI con Performance'+ lstKPI);
                //The actual amounts for each KPI are added to the performance.
                map<string,MDRM_Modelorama_Performance__c> mapPerf = new map<string,MDRM_Modelorama_Performance__c>();
                
                for(MDRM_Modelorama_Performance__c theperf :lstPerf){
                    mapPerf.put(theperf.MDRM_External_ID__c, theperf);
                    if(theperf.MDRM_Actual_Volume__c==null){
                        theperf.MDRM_Actual_Volume__c =0; 
                    }
                }system.debug('Perfomances to update '+ mapPerf);
                
                
                system.debug('New KPI '+ lstKPI);
                for(ONTAP__KPI__c KPI:lstKPI){
                    if(mapPerf.get(kpi.MDRM_Performance_External_ID__c) != null){
                        
                        decimal actualHL =mapPerf.get(kpi.MDRM_Performance_External_ID__c).MDRM_Actual_Volume__c;
                        if(KPI.MDRM_Actual_Hectoliters__c != null){
                            decimal HL =+KPI.MDRM_Actual_Hectoliters__c; 
                            actualHL = actualHL+HL;
                        }else{
                            decimal HL =0; 
                            actualHL = actualHL+HL;
                        }
                        mapPerf.get(kpi.MDRM_Performance_External_ID__c).MDRM_Actual_Volume__c =actualHL;
                        
                    }
                }
                
                update mapPerf.values();
                system.debug('new perf '+ mapPerf);
                
            }
        }
    }
    global void finish(Database.BatchableContext BC){
    }    
}