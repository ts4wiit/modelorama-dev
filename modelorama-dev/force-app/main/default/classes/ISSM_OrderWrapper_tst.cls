/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_OrderWrapper_tst {
	
	@isTest static void test_method_one() {
		// Add the opportunity wrapper objects to a list.
        ISSM_OrderWrapper_cls[] orderList = new List<ISSM_OrderWrapper_cls>();
        Date begindate1 = Date.today().addDays(20);
        Date begindate2 = Date.today().addDays(10);
        Date begindate3 = Date.today().addDays(30);
        orderList.add( new ISSM_OrderWrapper_cls(new ONTAP__Order__c(
            ONTAP__BeginDate__c=begindate1)));
        orderList.add( new ISSM_OrderWrapper_cls(new ONTAP__Order__c(
            ONTAP__BeginDate__c=begindate2)));
        orderList.add( new ISSM_OrderWrapper_cls(new ONTAP__Order__c(
            ONTAP__BeginDate__c=begindate3)));
        
        // Sort the wrapper objects using the implementation of the 
        // compareTo method.
        orderList.sort();
        
        // Verify the sort order
        System.assertEquals(begindate3, orderList[0].order.ONTAP__BeginDate__c);
	}
	
}