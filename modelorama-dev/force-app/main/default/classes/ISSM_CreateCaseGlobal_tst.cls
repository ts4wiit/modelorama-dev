/***********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto: ISSM  AB Int Bev (Customer Service)
Descripción: CLase que realiza la insercion de caso 
Clase de prueba: ISSM_CreateCaseGlobal_tst
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    25-Agosto-2017   Hector Diaz (HD)     Creador
1.1    26-Abril-2017    Leopoldo Ortega      Se creó en la vertical
***********************************************************************************/
@istest 
public class ISSM_CreateCaseGlobal_tst {
    @testSetup static void setup(){
        ID regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='ISSM_RegionalSalesDivision'].Id;
        Account locationAcct_obj = new Account(Name='CMM Del Bajio',ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId);
        insert locationAcct_obj;
        regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='SalesOrg'].Id;
        locationAcct_obj = new Account(Name='Metropolitana',ONTAP__SalesOfficeDescription__c='',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId, parentId=locationAcct_obj.Id);
        insert locationAcct_obj;
        regionalSalesDivisionId = [SELECT Id FROM RecordType WHERE DeveloperName='SalesOffice'].Id;
        locationAcct_obj = new Account(ISSM_BillingManager__c=UserInfo.getUserId(),Name='Doctores',ONTAP__SalesOfficeDescription__c='FF10',ONTAP__SalesOffId__c='',RecordTypeId=regionalSalesDivisionId, parentId=locationAcct_obj.id);
        insert locationAcct_obj;
        ISSM_TypificationMatrix__c TypMat= new ISSM_TypificationMatrix__c (ISSM_UniqueIdentifier__c='TESTCAM', ISSM_TypificationLevel1__c = 'Refrigeración', ISSM_TypificationLevel2__c='Entrega de equipo');        
		insert TypMat;
    }
    //ISSM_CreateCaseGlobal_cls.CreateCase
    @isTest
    private static void tstCreateCase(){
        Account acct = new Account(Name='test acc');
        insert acct;
        Case casexx = new Case();
        insert casexx;
        ISSM_CreateCaseGlobal_cls.CreateCase('Refrigeración','Pending','Pending','Pending',
                                             'Pending', 'Pending',true,
                                             ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'),
                                             new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c='Queue', ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                                                            ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', 
                                                                            ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                                                            ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                                                            ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                                                            ISSM_SLAProvider__c=false),
                                             UserInfo.getUserId(), UserInfo.getUserId(),
                                             new Account(ONTAP__Email__c='test@est.com')
                                             ,acct.Id , casexx.Id,'String strDescription', '123', 1);
    }
    // Assigned To USER
    @isTest 
    private static void testGetCaseCustomerService_AssignedToUser(){
        Case casexx = new Case();
        insert casexx;
        Account salesOffice = [SELECT Id FROM Account WHERE Name ='Doctores' LIMIT 1];
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.USER, ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', ISSM_OwnerUser__c=UserInfo.getUserId(),
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        Account acc = new Account(name='test',ONTAP__Email__c='test@est.com', OwnerId=UserInfo.getUserId(),ISSM_SalesOffice__c = salesOffice.Id);
        insert acc;
        
        String rtCAM = '';
        rtCAM = ISSM_CreateCaseGlobal_cls.getRecordType_str('CAM_Asset_Assignation');
        
        test.startTest();
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea', '123', 1, rtCAM);
        test.stopTest();
    }
    // Assigned To QUEUE
    @isTest
    private static void testGetCaseCustomerService_AssignedToQUEUE(){
        Case casexx = new Case();
        insert casexx;
        Account salesOffice = [SELECT Id FROM Account WHERE Name ='Doctores' LIMIT 1];
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.QUEUE, ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', 
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        
        Account acc = new Account(name='test',ONTAP__Email__c='test@est.com', OwnerId=UserInfo.getUserId(),ISSM_SalesOffice__c = salesOffice.Id);
        insert acc;
        test.startTest();
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea', '123', 1, '');
        test.stopTest();
    }
    //Assigned To SUPERVISOR / MODELORAMA / REPARTO
    @isTest
    private static void testGetCaseCustomerService_AssignedToSupervisorModeloramaReparto(){
        Case casexx = new Case();
        insert casexx;
        Account salesOffice = [SELECT Id FROM Account WHERE Name ='Doctores' LIMIT 1];
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.MODELORAMA , ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', 
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        Account acc = new Account(name='test',ONTAP__Email__c='test@est.com', OwnerId=UserInfo.getUserId(),ISSM_SalesOffice__c = salesOffice.Id);
        insert acc;
        List<AccountTeamMember> aTeamMember_lst = new List<AccountTeamMember>{
            new AccountTeamMember(UserId=UserInfo.getUserId(),TeamMemberRole=ISSM_Constants_cls.SUPERVISOR, AccountId=acc.Id),
                new AccountTeamMember(UserId=UserInfo.getUserId(),TeamMemberRole=ISSM_Constants_cls.MODELORAMA, AccountId=acc.Id),
                new AccountTeamMember(UserId=UserInfo.getUserId(),TeamMemberRole=ISSM_Constants_cls.REPARTO, AccountId=acc.Id)
                };
        insert aTeamMember_lst;
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea', '123', 1, '');
    }
    // Assigned To BILLINGMANAGER
    @isTest 
    private static void testGetCaseCustomerService_AssignedToBILLINGMANAGER(){
        Case casexx = new Case();
        Account salesOffice = [SELECT Id, ISSM_BillingManager__c FROM Account WHERE Name ='Doctores' LIMIT 1];
        insert casexx;
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.BILLINGMANAGER, ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', ISSM_OwnerUser__c=UserInfo.getUserId(),
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        Account acc = new Account(name='test',ONTAP__Email__c='test@est.com', OwnerId=UserInfo.getUserId(),ISSM_SalesOffice__c = salesOffice.Id);
        insert acc;
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea', '123', 1, '');
    }
    // Assigned To BOSSREFRIGERATION
    @isTest    
    private static void testGetCaseCustomerService_AssignedToBOSSREFRIGERATION(){
        Case casexx = new Case();
        insert casexx;
        Account salesOffice = [SELECT Id, ISSM_BillingManager__c FROM Account WHERE Name ='Doctores' LIMIT 1];
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.BOSSREFRIGERATION, ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', ISSM_OwnerUser__c=UserInfo.getUserId(),
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        Account acc = new Account(name='test',ONTAP__Email__c='test@est.com', OwnerId=UserInfo.getUserId(),ISSM_SalesOffice__c = salesOffice.Id);
        insert acc;
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea', '123', 1, '');
    }/*
    // Assigned To TRADEMARKETING
    @isTest    
    private static void testGetCaseCustomerService_AssignedToTRADEMARKETING(){
        Case casexx = new Case();
        insert casexx;
        ISSM_TypificationMatrix__c matrix = new ISSM_TypificationMatrix__c(ISSM_Active__c=true, ISSM_AssignedTo__c=ISSM_Constants_cls.USER, ISSM_CaseClosedSendLetter__c='No', ISSM_CaseRecordType__c='ISSM_ProductRequest', ISSM_Countries_ABInBev__c='México', 
                                           ISSM_Entitlement__c='55036000000ncCEAAY', ISSM_IdQueue__c='00G36000002iqzxEAA', ISSM_OwnerUser__c=UserInfo.getUserId(),
                                           ISSM_OpenCaseSendLetter__c='Yes', ISSM_OwnerQueue__c='ISSM_ModeloAmigo', ISSM_Priority__c='Low', ISSM_SendOwnerNotification__c=false, ISSM_TypificationLevel1__c='Consumidores y publico en general', 
                                           ISSM_TypificationLevel2__c='Quejas y sugerencias', ISSM_TypificationLevel3__c='Producto', ISSM_TypificationLevel4__c='Sensorial', ISSM_TypificationLevel5__c='Lata', 
                                           ISSM_TypificationLevel6__c='Olor descompuesto podrido', ISSM_UniqueIdentifier__c='México - TM - 0280', 
                                           ISSM_SLAProvider__c=false);
        Account acc = new Account(ONTAP__Email__c='test@est.com', OwnerId='',ISSM_SalesOffice__c = '');
        insert acc;
        ISSM_CreateCaseGlobal_cls.getCaseCustomerService(matrix, 
            acc,new ISSM_AppSetting_cs__c(), 
            ISSM_CreateCaseGlobal_cls.getRecordType('ISSM_GeneralCase'), true, casexx.Id, 'String strDescriptionTextArea');
    }*/
}