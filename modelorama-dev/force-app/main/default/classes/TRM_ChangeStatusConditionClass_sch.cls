/******************************************************************************* 
* Developed by      : 	Avanxo México
* Author			: 	Oscar Alvarez
* Project			: 	AbInbev - Trade Revenue Management
* Description		: 	Schedulable for the execution of work flows (change of status).
*						Run every day at 12:00:00 a.m.
*
* No.         Date               Author                      Description
* 1.0    11-Octubre-2018      Oscar Alvarez                   Creación
*******************************************************************************/
global class TRM_ChangeStatusConditionClass_sch implements Schedulable {
  /*
   * Run every day at 12:00:00 a.m.
   * 
  */
  @TestVisible
  public static final String CRON_EXPR = Label.TRM_CronExprChangeStatusCC;  
  /*
    Call this from Anonymous Apex to schedule at the default regularity
  */
  global static String scheduleIt() {
  	TRM_ApprovalConditions_ctr.unSchedule(Label.TRM_NameSchStatusCC);
    TRM_ChangeStatusConditionClass_sch job = new TRM_ChangeStatusConditionClass_sch();
    return System.schedule((Test.isRunningTest() ? (Label.TRM_NameSchStatusCC + System.now()) : Label.TRM_NameSchStatusCC), CRON_EXPR, job);
  }
   /*
    * Execution of the flow, for the change of status of the Condition class. Change status:
    * 	-ISSM_Actived
    * 	-ISSM_Expired
  */
  global void execute(SchedulableContext sc) {
      Map<String, Object> params = new Map<String, Object>();
      //Flow for ISSM_Expired and ISSM_Actived
    	Flow.Interview.TRM_StatusConditionClass flowExpiredCCAndActivedCC = new Flow.Interview.TRM_StatusConditionClass(params);
      	if(!Test.isRunningTest()){flowExpiredCCAndActivedCC.start();}
  }
}