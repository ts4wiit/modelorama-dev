/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Rodrigo Resendiz (RR)
Proyecto:  ISSM  AB Int Bev (OnTap)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 25-Julio-2017 Rodrigo Resendiz (RR) Creation.
2.0 28-Sept-2018  Leopoldo Ortega (LO)  Update - Line documented - caseForce_obj.OwnerId = newIdToAssign_id; because the owner, always should be who that created
***********************************************************************************/
public without sharing class ISSM_ONTAPClassifyCaseForce_cls {
    
    public class NoAccountTeamFoundException extends Exception {}
    public class NoRecordSuchTypeException extends Exception {}
    
    private static final Id CASE_FORCE_RECORD_TYPE_ID; 
        
    static {
        CASE_FORCE_RECORD_TYPE_ID = [select Id from RecordType where developerName='ISSM_SalesForceCase' limit 1].Id; 
    }
    /**
    * Description: Method to be executed on before insert trigger that assigns the case to the specified person on the account team
    * @param  none
    * @return none
	**/
    public static void onBeforeInsert(List<ONTAP__Case_Force__c> triggerNew) {
        Set<Id> accountIds_set = new Set<Id>();
        Id newIdToAssign_id;
        Id rt;
        String recordtypeId;
        String recordtypeIdCF;
        String CFrecordTypeId;
        Id rtCF;
        Map<Id, Map<String,Id>> teamMember_map = new Map<Id, Map<String,Id>>();
        Map<String,ISSM_SurveyAssignmentCase__c> toAssign_map = new Map<String,ISSM_SurveyAssignmentCase__c>();
        
        //fill account set
        for (ONTAP__Case_Force__c caseForce_obj : triggerNew) {
            accountIds_set.add(caseForce_obj.ONTAP__Account__c);
        }
        //account members
        for (AccountTeamMember teamMember_obj : [SELECT UserId, TeamMemberRole, AccountId FROM AccountTeamMember WHERE AccountId IN: accountIds_set]) { if (!teamMember_map.containsKey(teamMember_obj.AccountId)) { teamMember_map.put(teamMember_obj.AccountId, new Map<String,Id>{teamMember_obj.TeamMemberRole.deleteWhitespace() => teamMember_obj.UserId}); } else { teamMember_map.get(teamMember_obj.AccountId).put(teamMember_obj.TeamMemberRole.deleteWhitespace(), teamMember_obj.UserId); } }
        //custom setting survey assignment fetch 
        for (ISSM_SurveyAssignmentCase__c assign : ISSM_SurveyAssignmentCase__c.getall().values()) { toAssign_map.put(assign.ISSM_Classification__c.deleteWhitespace(), assign); }
        //iterate over the trigger new content
        for (ONTAP__Case_Force__c caseForce_obj : triggerNew) {
            if (caseForce_obj.RecordTypeId == CASE_FORCE_RECORD_TYPE_ID) {
                if (caseForce_obj.ISSM_ClassificationLevel3__c != null) {
                    if (teamMember_map.get(caseForce_obj.ONTAP__Account__c) == null) { newIdToAssign_id = caseForce_obj.OwnerId; } else if (toAssign_map.get(caseForce_obj.ISSM_ClassificationLevel3__c.deleteWhitespace()) == null) { newIdToAssign_id = caseForce_obj.OwnerId; } else if (teamMember_map.get(caseForce_obj.ONTAP__Account__c).get(toAssign_map.get(caseForce_obj.ISSM_ClassificationLevel3__c.deleteWhitespace()).ISSM_AccountTeamRole__c.deleteWhitespace()) == null) { newIdToAssign_id = caseForce_obj.OwnerId; } else { newIdToAssign_id = teamMember_map.get(caseForce_obj.ONTAP__Account__c).get(toAssign_map.get(caseForce_obj.ISSM_ClassificationLevel3__c.deleteWhitespace()).ISSM_AccountTeamRole__c.deleteWhitespace()); } caseForce_obj.ONTAP__Assigned_User__c = newIdToAssign_id;
                    //caseForce_obj.OwnerId = newIdToAssign_id;
                }
            }
        }
    }
}