/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_UpdateBranchAccount_tst {

    static testMethod void TestUpdateAccountBranchTradicional() {
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Account');   
        
        Account ovbAccount = new Account();
        ovbAccount.Name = 'Tradicional';
        ovbAccount.ONCALL__KDKG2__c = 'G2';
        ovbAccount.ISSM_KVGR5__c = '2';
        insert ovbAccount;
    }
        static testMethod void TestUpdateAccountBranchPerfecta() {
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Account');   
        
        Account ovbAccount = new Account();
        ovbAccount.Name = 'Perfecta';
        ovbAccount.ONCALL__KDKG2__c = 'G3';
        ovbAccount.ISSM_KVGR5__c = '2';
        insert ovbAccount;
    }
        static testMethod void TestUpdateAccountBranchCanalModerno() {
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Account');   
        
        Account ovbAccount = new Account();
        ovbAccount.Name = 'Canal moderno - Orden de compra';
        ovbAccount.ONCALL__KDKG2__c = 'G2';
        ovbAccount.ISSM_KVGR5__c = '1';
        insert ovbAccount;
    }
        static testMethod void TestUpdateAccountBranchFolioRecepcion() {
        String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Account');   
        
        Account ovbAccount = new Account();
        ovbAccount.Name = 'Canal moderno - Folio de recepción';
        ovbAccount.ONCALL__KDKG2__c = 'G1';
        ovbAccount.ISSM_KVGR5__c = '2';
        insert ovbAccount;
    }
}