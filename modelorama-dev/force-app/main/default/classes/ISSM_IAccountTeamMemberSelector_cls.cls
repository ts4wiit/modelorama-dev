public interface ISSM_IAccountTeamMemberSelector_cls {
	List<AccountTeamMember> selectById(Set<ID> idSet);
	AccountTeamMember selectById(ID accountTeamMemberID);
	List<AccountTeamMember> selectById(String accountTeamMemberID);
	List<AccountTeamMember> selectByMultipleFields(String strTeamMemberRole,set<Id> accountSetID);

}