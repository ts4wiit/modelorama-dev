/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Batch to related all records to the object Model of CDM
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       23-April-2018   Iván Neria			   Initial version.
***********************************************************************************/

global class CDM_ReprocessBatch_bch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT  Id, Name, MDM_Control_Check__c'
        +' FROM MDM_Temp_Account__c WHERE MDM_Control_Check__c=false';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
	List<String> lstIds = new List<String>();
        for(MDM_Temp_Account__c tempAccToUpdate : (List<MDM_Temp_Account__c>)scope){
            lstIds.add(tempAccToUpdate.Id);
        }
        for(MDM_Temp_Account__c tempAccToUpdate : (List<MDM_Temp_Account__c>)scope){
            if(MDM_Account_cls.validationReprocess(lstIds).contains(tempAccToUpdate.Id)){
                tempAccToUpdate.MDM_HasErrors__c=true;
            } else{
               tempAccToUpdate.MDM_Control_Check__c=true; 
            }
        }    
        update scope;
    }  
    global void finish(Database.BatchableContext BC)
    {
        EmailTemplate template =[SELECT Id FROM EmailTemplate WHERE Name=:Label.CDM_Apex_CDM_Reprocess_Data];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setTemplateId(template.Id);
        mail.setTargetObjectId(System.UserInfo.getUserId());
        mail.setSaveAsActivity(false);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}