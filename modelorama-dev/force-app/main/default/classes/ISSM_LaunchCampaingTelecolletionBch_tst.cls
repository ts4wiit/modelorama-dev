/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */ 
@isTest  
private  class ISSM_LaunchCampaingTelecolletionBch_tst {
    public static List<ISSM_IntervalsByCountry__c> intervalsByCountry;
    public static List<ISS_DocumentTypeOpenItems__c> DocumentTypeOpenItems;
    public static  List<ISSM_TriggerFactory__c> AppTriggerFactory;
    public static  List<ISSM_AppSetting_cs__c> AppSetting;
        
    static testMethod void myUnitTest() {
        intervalsByCountry = ISSM_CreateDataTest_cls.fn_CreateIntervalsByCountry(true,'Interval1','MX','8','90');
        User createUser =  ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'hdiaz@acme.com');
        DocumentTypeOpenItems = ISSM_CreateDataTest_cls.fn_CreateDocumentTypeOpenItems(true,'DocumentType');
        
        // Cretae configuracion personalizada 
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo','ISSM_Telecolletion') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSettingTelecollection(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'),mapIdQueue.get('ISSM_Telecolletion'));
        AppTriggerFactory = ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        
        
        List<Account> lstAccount = new List<Account>();
        //Generamos campaña  Telecollection_Campaign__c
        Telecollection_Campaign__c objCreateCampaign = new Telecollection_Campaign__c();
        objCreateCampaign.Name = 'EjemploCampaignTest';
        objCreateCampaign.Start_Date__c = System.today();
        objCreateCampaign.End_Date__c =  System.today();
        objCreateCampaign.Active__c = true;
        objCreateCampaign.SoqlFilters__c='';
        insert objCreateCampaign;
      
       //Generamos RegionalSalesdivision 
        String RecordTypeRegional = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();        
        Account objCreateRegionalDiv  = new Account();
        objCreateRegionalDiv.Name ='RegionaSales';
        objCreateRegionalDiv.RecordTypeId = RecordTypeRegional;
        insert  objCreateRegionalDiv;
        
        //Generamos SalesOrg
        String RecordTypeSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();        
        Account objTypeSalesOrg  = new Account();
        objTypeSalesOrg.Name ='SalesOrg';
        objTypeSalesOrg.ISSM_ParentAccount__c = objCreateRegionalDiv.Id;
        objTypeSalesOrg.RecordTypeId =RecordTypeSalesOrg;
         insert objTypeSalesOrg;
        
        //Generamos SalesOffice
        String RecordTypeSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        Account ObjTypeSalesOffice  = new Account();
        ObjTypeSalesOffice.Name ='SalesOffcie';
        ObjTypeSalesOffice.ISSM_ParentAccount__c = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.ParentId = objTypeSalesOrg.Id;
        ObjTypeSalesOffice.RecordTypeId = RecordTypeSalesOffice;
        insert ObjTypeSalesOffice;
        
        //Generamos cuenta para los filtros
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();        
        
        Account objCreateAccount  = new Account();
        objCreateAccount.Name ='EjemploCuenta';
        objCreateAccount.ONCALL__Risk_Category__c = null;
        objCreateAccount.ONTAP__Classification__c ='Botella Abierta';
        objCreateAccount.ISSM_LastContactDate__c = null;
        objCreateAccount.ISSM_LastPaymentPlanDate__c= null;
        objCreateAccount.ISSM_LastPromisePaymentDate__c= null;
        objCreateAccount.ISSM_RegionalSalesDivision__c =objCreateRegionalDiv.Id;
        objCreateAccount.ISSM_SalesOffice__c =ObjTypeSalesOffice.Id;
        objCreateAccount.ISSM_SalesOrg__c =objTypeSalesOrg.Id;
        objCreateAccount.RecordTypeId = RecordTypeAccountId;
        insert objCreateAccount; 

		List<ISSM_OpenItemB__c> lstOpenI = new List<ISSM_OpenItemB__c>();
        
        //Creamos ONTAP__OpenItem__c 
        ISSM_OpenItemB__c objOpenItem = new ISSM_OpenItemB__c(); 
        objOpenItem.ISSM_Account__c = objCreateAccount.Id;
        objOpenItem.ISSM_DueDate__c = System.today()-20;
        objOpenItem.ISSM_Amounts__c =15000; 
        objOpenItem.ISSM_Debit_Credit__c='S'; 
		//objOpenItem.ONTAP__Account__r = objCreateAccount;
        lstOpenI.add(objOpenItem);
        
		//Creamos ONTAP__OpenItem__c 
        ISSM_OpenItemB__c objOpenItem2 = new ISSM_OpenItemB__c();
       	objOpenItem.ISSM_Account__c = objCreateAccount.Id;
        objOpenItem2.ISSM_DueDate__c = System.today()-20;
        objOpenItem2.ISSM_Amounts__c =15000;
        objOpenItem2.ISSM_Debit_Credit__c='S';
		lstOpenI.add(objOpenItem2);
    	lstAccount.add(objCreateAccount);
    	
		insert lstOpenI; 
		List<Account> queryAcc = [select Id,Name,ONCALL__Risk_Category__c,ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c, ISSM_LastPromisePaymentDate__c,(Select id,ISSM_Account__c,ISSM_DocumentId__c, ISSM_ExpirationDays__c,ISSM_Debit_Credit__c From Open_Items__r) from Account Where Id IN(Select ISSM_Account__c From ISSM_OpenItemB__c) AND ISSM_FilterCondicion__c = true AND RecordTypeId=:RecordTypeAccountId Limit 12000];
        Database.BatchableContext dbBC;
      
        Test.startTest();
            ISSM_LaunchCampaingTelecolletion_bch objLaunchCampaingTelecolletion = new ISSM_LaunchCampaingTelecolletion_bch(
                    objCreateCampaign.Name,
                    objCreateCampaign.Start_Date__c,
                    objCreateCampaign.End_Date__c,
                    objCreateCampaign.Active__c,
                    objCreateCampaign.Id,
                    objCreateCampaign.SoqlFilters__c);
                   
            objLaunchCampaingTelecolletion.start(dbBC);
           objLaunchCampaingTelecolletion.execute(dbBC,queryAcc);
            objLaunchCampaingTelecolletion.finish(dbBC);
        Test.stopTest();
        
    }
}