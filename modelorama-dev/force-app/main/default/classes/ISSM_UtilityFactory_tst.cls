/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Test Class for 'ISSM_UtilityFactory_cls'

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    31-Mayo-2018    	Luis Licona                 Creation
******************************************************************************* */
@isTest
private class ISSM_UtilityFactory_tst {
	
	@testSetup static void setupData(){	
        ONTAP__Product__c PRODUCT = new ONTAP__Product__c();
	        PRODUCT.ONTAP__ProductId__c 		= '3000005';
	        PRODUCT.ONTAP__MaterialProduct__c 	= '3000005';
	        PRODUCT.ONTAP__ProductShortName__c 	= 'VICTORIA';
	        PRODUCT.ONTAP__ProductType__c 		= 'FERT';
        Insert PRODUCT;
	}

	@isTest static void tstMethod_executeQuery() {
		String StrQuery = 'SELECT ONTAP__ProductId__c FROM ONTAP__Product__c WHERE ONTAP__ProductId__c = '+'\'' + '3000005' + '\'';
		Test.startTest();
			sObject[] LstsObject = ISSM_UtilityFactory_cls.executeQuery(StrQuery); 
			System.assertEquals(!LstsObject.isEmpty(),true);
		Test.stopTest();
	}
	
	@isTest static void tstMethod_createLstItems() {
		sObject[] itemList = new sObject[]{new ONTAP__Product__c(ONTAP__ProductId__c='3000005')};
		Test.startTest();
			String[] LstString = ISSM_UtilityFactory_cls.createLstItems(itemList,'ONTAP__ProductId__c');
			System.assertEquals(!LstString.isEmpty(),true);
		Test.stopTest();
	}

	@isTest static void tstMethod_getSelectOptions() {
		Test.startTest();
			String[] LstString = ISSM_UtilityFactory_cls.getSelectOptions(new ISSM_Combos__c(),
																		'ISSM_TypeApplication__c');
			System.assertEquals(!LstString.isEmpty(),true);
		Test.stopTest();
	}

	@isTest static void tstMethod_getRecordTypeIdByDeveloperName() {
		Test.startTest();
			String StrRecordType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('Account','SalesOffice');
			System.assertEquals(String.isNotBlank(StrRecordType),true);
		Test.stopTest();
	}
	
}