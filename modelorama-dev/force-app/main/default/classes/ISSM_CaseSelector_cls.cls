/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for Case
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public without sharing class ISSM_CaseSelector_cls extends fflib_SObjectSelector implements ISSM_ICaseSelector_cls
{

    public static ISSM_ICaseSelector_cls newInstance()
    {
        return (ISSM_ICaseSelector_cls) ISSM_Application_cls.Selector.newInstance(Case.SObjectType);
    }

    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
                Case.Id, 
                Case.Status,
                Case.ISSM_CaseForceNumber__c,
                Case.DataTargetCaseMilestone__c,
                Case.ISSM_Accountant__c,
                Case.CaseNumber
            };
    }

    public Schema.SObjectType getSObjectType()
    {
        return Case.sObjectType;
    }

    public List<Case> selectById(Set<ID> idSet)
    {
        return (List<Case>) selectSObjectsById(idSet);
    }

    public Case selectById(ID caseID){
        return (Case) selectSObjectsById(caseID);
    }

    public List<Case> selectById(String caseID){
        return (List<Case>) selectSObjectsById(caseID);
    }

    public Map<Id,Case> selectByIdMap(Set<ID> idSet){
        return new Map<Id, Case>((List<Case>) selectSObjectsById(idSet));
    }   

    public List<Case> selectByCaseNumber(Set<String> caseNumber)
    {
        //assertIsAccessible();
        return (List<Case>) Database.query(
                String.format(
                'select {0} ' +
                  'from {1} ' +
                  'where CaseNumber in :caseNumber ' + 
                  'order by {2}',
                new List<String> {
                    getFieldListString(),
                    getSObjectName(),
                    getOrderBy() } ) );
    }

    public List<Case> selectByCaseForceNumber(List<Id> caseForceNumber)
    {
        //assertIsAccessible();
        return (List<Case>) Database.query(
                String.format(
                'select {0} ' +
                  'from {1} ' +
                  'where ISSM_CaseForceNumber__c in :caseForceNumber ' + 
                  'order by {2}',
                new List<String> {
                    getFieldListString(),
                    getSObjectName(),
                    getOrderBy() } ) );
    }

    public Case selectByCaseNumber(String strCaseNumber){
        Set<String> caseNumberSet = new Set<String>();
        caseNumberSet.add(strCaseNumber);
        List<Case> caseList = selectByCaseNumber(caseNumberSet);
        if(caseList != null && caseList.size() > 0){
            return caseList[0];
        }
        return null;
    }
}