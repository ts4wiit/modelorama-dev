/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador para pagina de inicio toma pedido ontap

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description              
    ------    --------        --------------------------   -----------------------------------------
    1.0       15-Junio-2017   Luis Licona                  Creación de la Clase     
   	1.2       04-Septiembre-2017   Hector Diaz             Se agregara funcion para no dejar crear pedido fuera de horario 
    ================================================================================================
****************************************************************************************************/
global with sharing class ISSM_StratPageOrder_ctr {
    public List<ONTAP__Order__c> OrdersToday_lst{get;set;}
    public ONCALL__Call__c call_Obj{get;set;} 
    public Boolean BlnHaveOrder{get;set;}
    public String[] lstSucc{get;set;}
    public Id IdOrder{get;set;}
    public ONTAP__Order__c OrderInstance{get;set;}
    private Account[] ObjAcc{get;set;}
    public String[] LstMissedFields{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    public ISSM_ValidateRequestedFields_cls CTRVFR = new ISSM_ValidateRequestedFields_cls();

    public ISSM_StratPageOrder_ctr(ApexPages.StandardController controller){
        BlnHaveOrder    = false; //Indica que dio clic al boton star call
        lstSucc         = new List<String>();
        IdOrder         = controller.getId();
        LstMissedFields = new List<String>();
        Id RecType=CTRSOQL.getRecordTypeId('ONTAP__Order__c','Emergency');
         
        if(String.isNotBlank(IdOrder)){
            OrderInstance = CTRSOQL.getOrderbyRecType(RecType,IdOrder);
            ObjAcc = CTRSOQL.getAccountListbyId(OrderInstance.ONCALL__OnCall_Account__c);
            System.debug('***OrderInstance : '+OrderInstance);
            String callRecType_Id = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId();
            call_Obj = CTRSOQL.getCallByOrder(callRecType_Id,OrderInstance.ONCALL__OnCall_Account__c,OrderInstance.Id);//CTRSOQL.getCallbyId(String.valueOf(lstCallXorder[0].Id));
            System.debug('call_Obj = ' + call_Obj);
        } 
    }

    /**
    * Realiza el redireccionamiento de la pagina de inicio de la llamada
    * @param  null
    * @return  PageReference: pr indica la pagina que se mostrara al ingresar a la llamada
    **/
    public PageReference startPage(){ 
        PageReference pr;

        if(OrderInstance != null){
            String[] LstMFA = CTRVFR.checkAccountRequiredValues(ObjAcc);
            if(LstMFA.size() > 0){
                LstMissedFields.add(Label.ISSM_MissedFieldsA);
                LstMissedFields.addAll(LstMFA);
                System.debug(LstMissedFields);
            }
			Boolean ValidateCheckOut  = CTRVFR.CheckOutValidate(ObjAcc);
            if(ValidateCheckOut){ 
				LstMissedFields.add(Label.ISSM_CallCheckOut);
			} 
            String[] LstMFRWDSD = CTRVFR.checkOrderRouteWDSD(OrderInstance.ONCALL__OnCall_Account__c, Label.ISSM_OriginCall);
            System.debug('LstMFRWDSD = ' + LstMFRWDSD);

            String[] LstMFRWODSD = CTRVFR.checkOrderRouteWODSD(OrderInstance.ONCALL__OnCall_Account__r.ONCALL__OnCall_Route_Code__c);
            System.debug('LstMFRWODSD = ' + LstMFRWODSD);

            String[] LstMFR = LstMFRWDSD.size() > 0 ? (LstMFRWODSD.size() > 0 ? LstMFRWODSD : LstMFRWDSD) : LstMFRWDSD;
            System.debug('LstMFR = ' + LstMFR);
            if(LstMFR.size() > 0){
                LstMissedFields.addAll(LstMFR);
            }
        }

        OrdersToday_lst = CTRSOQL.getOrdersByAccount(OrderInstance.ONCALL__OnCall_Account__c);
        System.debug(OrdersToday_lst);
        if(OrdersToday_lst.size() > 0){
            LstMissedFields.clear();
            for(ONTAP__Order__c currentOrder : OrdersToday_lst){
                if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Reparto){
                    LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                    if(OrderInstance.ONCALL__Call__c != null){
                        LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                        call_Obj.ISSM_CallResult__c = Label.ISSM_OrderByRep;
                    }                        
                }
                if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_MiModelo){
                    LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                    if(OrderInstance.ONCALL__Call__c != null){
                        LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                        call_Obj.ISSM_CallResult__c = Label.ISSM_OrderByMiModelo;
                    }
                }
                if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Preventa){
                    LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                    if(OrderInstance.ONCALL__Call__c != null){
                        LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                        call_Obj.ISSM_CallResult__c = Label.ISSM_OrderByPresales;
                    }                        
                }
                if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Autoventa){
                    LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                    if(OrderInstance.ONCALL__Call__c != null){
                        LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                        call_Obj.ISSM_CallResult__c = Label.ISSM_OrderByAutosales;
                    }                    
                }
            }     
            if(LstMissedFields.size() > 0 && OrderInstance.ONCALL__Call__c != null){
                System.debug(call_Obj.ISSM_CallResult__c);
                update call_Obj;
            }
        }

        if(Test.isRunningTest())
            LstMissedFields = new List<String>();

        if(OrderInstance != null && LstMissedFields.isEmpty()){
            //Valida si el pedido ya fue terminado
            if(OrderInstance.ISSM_EndOrder__c && OrderInstance.ONTAP__OrderStatus__c == Label.ISSM_OrderStatus3){
                BlnHaveOrder=true;
                String strparam = OrderInstance.owner.Profile.Name != Label.ISSM_Profile2 ? 'true' : 'false';
                pr = new PageReference(Label.ISSM_PageReference1);
                pr.getParameters().put(Label.ISSM_IdOrder,OrderInstance.Id);
                pr.getParameters().put(Label.ISSM_FlgOrd,Label.ISSM_True);
                pr.getParameters().put('ONCALL',strparam);//determina si es un pedido generado por ONTAO o desde ONCALL 
                pr.setRedirect(true);
            }else if(OrderInstance.ONTAP__OrderStatus__c != Label.ISSM_OrderStatus3)
                lstSucc.add(Label.ISSM_OrderMnsg3);
            else
                lstSucc.add(Label.ISSM_OrderMnsg4);
        }
        return pr;
    }
}