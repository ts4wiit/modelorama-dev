@isTest
public class MDRM_ContentDocumentLink_tst {
    @isTest static void general_test() {
        
        String[] strAdmin = new String[]{'System Administrator','Administrador del sistema'};
            
        Profile p = [SELECT Id FROM Profile WHERE Name IN: strAdmin];
        
        User Legal_Approver = [SELECT Id FROM User WHERE Profile.name = 'Expansors' AND IsActive = TRUE LIMIT 1];
        User UEN_Approver = [SELECT Id FROM User WHERE Profile.name = 'Expansors' AND IsActive = TRUE LIMIT 1];
        User Compliance_Approver = [SELECT Id FROM User WHERE Profile.name = 'Expansors' AND IsActive = TRUE LIMIT 1];
        User System_Administrator = [SELECT Id FROM User WHERE ProfileId =: p.id AND IsActive = TRUE LIMIT 1];
        User Corporate_Approver = [SELECT Id FROM User WHERE Profile.name = 'Expansors' AND IsActive = TRUE LIMIT 1];
        
        User u2 = new User(Alias = 'MDRM', Email='MDRM_newuser_MDRM@MDRM.com',
                           EmailEncodingKey='UTF-8', LastName='MDRM_Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='MDRM_newuser_MDRM@MDRM.com',
                           MDRM_Legal_Approver__c = Legal_Approver.id,
                           MDRM_UEN_Approver__c = UEN_Approver.id,
                           MDRM_Compliance_Approver__c = Compliance_Approver.id,
                           MDRM_Corporate_Affairs_Approver__c = System_Administrator.id,
                           MDRM_Procurement_Approver__c = System_Administrator.id,
                           MDRM_Corporative_Approver__c = Corporate_Approver.id);
        
        System.runAs(u2) {
          
            Id idRTAcc = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Expansor' LIMIT 1].ID;
            Account acc = new Account(name = 'Expansor Test', RecordTypeId =idRTAcc );
            Insert acc;
            MDRM_Document__c doc = new MDRM_Document__c(MDRM_Account__c = acc.id,MDRM_File_Subtype__c = Label.MDRM_BusinessCase,MDRM_File_Type__c = 'Documento');
            
            Insert doc;
            
            string idacc = acc.id;
            String docName = Label.MDRM_BusinessCase;
            
            ContentVersion cv = new ContentVersion();
            cv.VersionData   = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv.Title = docName;
            cv.PathOnClient  = docName+'.pdf';
            cv.Description = doc.MDRM_File_Subtype__c;
            Insert cv;
            
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.id AND IsLatest = TRUE LIMIT 1].ContentDocumentId;
            cdl.LinkedEntityId = doc.id;
            cdl.ShareType = 'V';
            insert cdl;
            
            doc.MDRM_Send_for_Approval__c = Label.MDRM_Send_for_Approval_YES;
            Update doc;
            
            
            test.startTest();
            try{
                delete cdl;
            }catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains(Label.MDRM_DocumentDMLWarning) ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            test.stopTest();
            
        }
    }
}