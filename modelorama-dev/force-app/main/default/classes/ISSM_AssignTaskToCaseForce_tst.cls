/***********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Leopoldo Ortega (LO)
Proyecto: ISSM  AB Int Bev (Customer Service)
Descripción: CLase de prueba de la clase ISSM_AssignTaskToCaseForce
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    12-Junio-2017    Leopoldo Ortega      Se creó en la vertical
***********************************************************************************/
@isTest
public class ISSM_AssignTaskToCaseForce_tst {
    public static User user;
    public static Account salesOffice;
    public static Account salesOrg;
    public static Account drv;
    public static Case caseObj;
    public static List<Case> Case_lst;
    public static List<ISSM_CS_CAM__c> CSCAM_lst;
    
    static testmethod void assignTaskToCaseForce() {
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeSalesOrgAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();
        String RecordTypeDRVAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();
        
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true, RecordTypeSalesOfficeAccountId, 'AccountTest', user.Id, user.Id);
        salesOrg = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOrg(true, RecordTypeSalesOrgAccountId, 'AccountTest', user.Id, user.Id);
        drv = ISSM_CreateDataTest_cls.fn_CreateAccountDRV(true, RecordTypeDRVAccountId, 'AccountTest', user.Id, user.Id);
        
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user = new User(Alias = 'admin', Email='systemadmin@testorg.com', IsActive = true, EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin1@testorg.com');
        insert user;
        
        test.startTest();
        
        ISSM_CS_CAM__c config = new ISSM_CS_CAM__c();
        config.Name = 'Test Config';
        config.SetupOwnerId = user.Id;
        insert config;
        
        Account acct = new Account(Name='test acc');
        insert acct;
        
        ONTAP__Case_Force__c ObjCaseForce = new ONTAP__Case_Force__c();
        ObjCaseForce.ONTAP__Description__c = 'Descripción';
        ObjCaseForce.ONTAP__Subject__c = 'Asunto';
        ObjCaseForce.ONTAP__Status__c = 'Abierto';
        ObjCaseForce.ONTAP__Account__c = acct.Id;
        ObjCaseForce.ISSM_StatusSerialNumber__c = 2;
        insert ObjCaseForce;
        
        Case ObjCase = new Case();
        ObjCase.Description = 'Descripción';
        ObjCase.Subject = 'Asunto';
        ObjCase.Status = 'Abierto';
        ObjCase.AccountId = acct.Id;
        ObjCase.ISSM_StatusSerialNumber__c = 2;
        ObjCase.ISSM_CaseForceNumber__c = ObjCaseForce.Id;
        insert ObjCase;
        
        ISSM_Asset__c asset = new ISSM_Asset__c();
        asset.Name = 'Test Asset';
        asset.ISSM_Serial_Number__c = '123';
        asset.ISSM_Provider_Serial_Number__c = '123';
        asset.ISSM_Invoice_date__c = date.today() - 10;
        asset.ISSM_Warranty_Date__c = date.today() + 10;
        insert asset;
        
        ONTAP__Account_Asset__c accAsset = new ONTAP__Account_Asset__c();
        accAsset.ISSM_Asset_CAM__c = asset.Id;
        accAsset.ONTAP__Account__c = acct.Id;
        insert accAsset;
        
        AccountTeamMember atm1 = new AccountTeamMember();
        atm1.AccountId = salesOffice.Id;
        atm1.UserId = user.Id;
        atm1.TeamMemberRole = 'Trade Marketing';
        insert atm1;
        
        AccountTeamMember atm2 = new AccountTeamMember();
        atm2.AccountId = salesOrg.Id;
        atm2.UserId = user.Id;
        atm2.TeamMemberRole = 'Trade Marketing';
        insert atm2;
        
        AccountTeamMember atm3 = new AccountTeamMember();
        atm3.AccountId = drv.Id;
        atm3.UserId = user.Id;
        atm3.TeamMemberRole = 'Trade Marketing';
        insert atm3;
        
        test.stopTest();
    }
}