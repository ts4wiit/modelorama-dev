@isTest
public class ContentControllerTest {

    static testMethod void testContentProveedor() {
        PageReference pref = Page.ContentProv;
        Test.setCurrentPage(pref);
        
        Agri_Supplier__c asp = new Agri_Supplier__c();
        asp.Name = 'Test';
        asp.Agri_Account_Type__c = 'Moral';
        asp.Agri_First_Name__c = 'Prueba';
        asp.Agri_Last_Name__c = 'LN';
        asp.Agri_Contact_Email__c = 'test@domain.com';
        asp.Agri_RFC__c = 'JIH891025S11';
        asp.Agri_CLABE__c = '021345678912345678';
        insert asp;
        
        ApexPages.StandardController con = new ApexPages.StandardController(asp);
        ContentProvController ext = new ContentProvController(con);    
        
        ext.file = Blob.valueOf('Test Content');
        ext.archivoSeleccionado = 'INE_1_INE';
        ext.fN = 'Test.txt';
        ext.title = 'Tst';
        ext.Type = 'txt';
        ext.cv = new ContentVersion();
        		        
        ext.upload();
        ext.getArchivosOptionsProv();        
        
        ext.file = null;
        ext.archivoSeleccionado = '';
        ext.fN = '';
        
        ext.upload();                
    }
    
    static testMethod void testContentProveedor2() {
        PageReference pref = Page.ContentProv;
        Test.setCurrentPage(pref);
        
        Agri_Supplier__c asp = new Agri_Supplier__c();
        asp.Name = 'Test';
        asp.Agri_First_Name__c = 'Prueba';
        asp.Agri_Last_Name__c = 'LN';
        asp.Agri_Contact_Email__c = 'test@domain.com';
        asp.Agri_CLABE__c = '021345678912345678';
        asp.Agri_Account_Type__c = 'Fisica';
        asp.Agri_RFC__c = 'JIHV891025S11';
        asp.Agri_CURP__c = 'JIHV891025HDFMRC04';
        insert asp;
        
        ApexPages.StandardController con = new ApexPages.StandardController(asp);
        ContentProvController ext = new ContentProvController(con);    
        
        ext.file = Blob.valueOf('Test Content');
        ext.archivoSeleccionado = 'INE_1_INE';
        ext.fN = 'Test.txt';
        ext.title = 'Tst';
        ext.Type = 'txt';
        ext.cv = new ContentVersion();
        		        
        ext.upload();
        ext.getArchivosOptionsProv();
    }
    
    static testMethod void testContentContrato() {
        PageReference pref = Page.ContentContract;
        Test.setCurrentPage(pref);
        
        Agri_Material__c am = new Agri_Material__c();
        am.Name = 'Test';
        am.Agri_Type__c = 'Semilla';
        insert am;        
        
        Agri_Contract__c ac = new Agri_Contract__c();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Bank_Account_Modelo__c = '12345678901';
        ac.Agri_Name_of_Bank_Account_Modelo__c = '021 HSBC';
        ac.Agri_Bank_Reference_Number__c = '12345678901';
        ac.Agri_Legal_Personality__c = 'M';
        insert ac;
        
        ApexPages.StandardController con = new ApexPages.StandardController(ac);
        ContentContractController ext = new ContentContractController(con);    
        
        ext.file = Blob.valueOf('Test Content');
        ext.archivoSeleccionado = 'INE_1_INE';
        ext.fN = 'Test.txt';
        ext.title = 'Tst';
        ext.Type = 'txt';
        ext.cv = new ContentVersion();
        		        
        ext.upload();
        ext.getArchivosOptionsContr();
        
        ac.Agri_Legal_Personality__c = 'F';
        update ac;
        ext.getArchivosOptionsContr();
        
        ext.file = null;
        ext.archivoSeleccionado = '';
        ext.fN = '';
        
        ext.upload();
    }
    
    static testMethod void testContentContrato2() {
        PageReference pref = Page.ContentContract;
        Test.setCurrentPage(pref);
        
        Agri_Material__c am = new Agri_Material__c();
        am.Name = 'Test';
        am.Agri_Type__c = 'Semilla';
        insert am;        
        
        Agri_Contract__c ac = new Agri_Contract__c();
        ac.Agri_Variety_Barley__c = am.Id;
        ac.Agri_Variety_Seed__c = am.Id;
        ac.Agri_Bank_Account_Modelo__c = '12345678901';
        ac.Agri_Name_of_Bank_Account_Modelo__c = '021 HSBC';
        ac.Agri_Bank_Reference_Number__c = '12345678901';
        ac.Agri_Legal_Personality__c = 'F';
        insert ac;
        
        ApexPages.StandardController con = new ApexPages.StandardController(ac);
        ContentContractController ext = new ContentContractController(con);    
        
        ext.file = Blob.valueOf('Test Content');
        ext.archivoSeleccionado = 'INE_1_INE';
        ext.fN = 'Test.txt';
        ext.title = 'Tst';
        ext.Type = 'txt';
        ext.cv = new ContentVersion();
        		        
        ext.upload();
        ext.getArchivosOptionsContr();
    }
    
}