public class MDRM_VinculoWrapped {
    public String z019 {get; set;}
    public String Name {get; set;}
    public String ONTAP_Province {get; set;}
    public String ONTAP_PostalCode {get; set;}
    public String ONTAP_Municipality {get; set;}
    public String ONTAP_Colony {get; set;}
    public String ONTAP_Street {get; set;}
    public String ONTAP_Street_Number {get; set;}
    
    public MDRM_VinculoWrapped(){
    }
}