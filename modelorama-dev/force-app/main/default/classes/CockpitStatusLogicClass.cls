/**
 * This class contains the logic to assign a color status for DRV, ORG, OFFICE and ROUTE.
 * <p /><p />
 * @author Alberto Gómez
 */
public with sharing class CockpitStatusLogicClass {

	/**
	 * Wrapper class for ONTAP__Tour__c for a temporal status.
	 */
	public class CockpitTourWrapperClass {

		//Variables.
		public String strTourDRVId;
		public String strTourDRVDescription;
		public String strTourOrgId;
		public String strTourOrgDescription;
		public String strTourOfficeId;
		public String strTourOfficeDescription;
		public String strTourRouteId;
		public String strTourId;
		public String strTourStatus;
		public String strTourSubStatus;
		public String strTourStatusColorText;
		public Date datTourDate;
	}

	/**
	 * Wrapper class for Account (Office) for add a temporal status of the Office.
	 */
	public class CockpitOfficeWrapperClass {

		//Variables.
		public String strOfficeDRVId;
		public String strOfficeDRVDescription;
		public String strOfficeOrgId;
		public String strOfficeOrgDescription;
		public String strOfficeId;
		public String strOfficeDescription;
		public String strOfficeStatusColorText;
		public Date datOfficeDate;
		public List<CockpitTourWrapperClass> lstCockpitTourWrapperClass;

		//Constructor of the inner class.
		public CockpitOfficeWrapperClass() {
			lstCockpitTourWrapperClass = new List<CockpitTourWrapperClass>();
		}
	}

	/**
	 * Wrapper class for Account(Org) for add a temporal status of the Org.
	 */
	public class CockpitOrgWrapperClass {

		//Variables.
		public String strOrgDRVId;
		public String strOrgDRVDescription;
		public String strOrgId;
		public String strOrgDescription;
		public String strOrgStatusColorText;
		public Date datOrgDate;
		public List<CockpitOfficeWrapperClass> lstCockpitOfficeWrapperClass;

		//Constructor of the inner class.
		public CockpitOrgWrapperClass() {
			lstCockpitOfficeWrapperClass = new List<CockpitOfficeWrapperClass>();
		}
	}

	/**
	 * Wrapper class for Account(DRV) for add a temporal status of the DRV.
	 */
	public class CockpitDRVWrapperClass {

		//Variables.
		public String strDRVId;
		public String strDRVDescription;
		public String strDRVStatusColorText;
		public Date datDRVDate;
		public List<CockpitOrgWrapperClass> lstCockpitOrgWrapperClass;

		//Constructor.
		public CockpitDRVWrapperClass() {
			lstCockpitOrgWrapperClass = new List<CockpitOrgWrapperClass>();
		}
	}

	/**
	 * This method creates list of Tour Wrapper class.
	 *
	 * @param lstOntapTourQueried	List<ONTAP__Tour__c>.
	 * @return List<CockpitTourWrapperClass>.
	 */
	public static List<CockpitTourWrapperClass> createLstCTourWCFLstOntapTour(List<ONTAP__Tour__c> lstOntapTourQueried) {

		//Variables.
		CockpitTourWrapperClass objCTourWCGenerated;
		List<CockpitTourWrapperClass> lstCTourWCGenerated = new List<CockpitTourWrapperClass>();

		//Create the Tour Wrapper list.
		if(!lstOntapTourQueried.isEmpty()) {
			for(ONTAP__Tour__c objOntapTour : lstOntapTourQueried) {
				objCTourWCGenerated = new CockpitTourWrapperClass();
				objCTourWCGenerated.strTourDRVId = objOntapTour.DRVId__c;
				objCTourWCGenerated.strTourDRVDescription = objOntapTour.DRV__c;
				objCTourWCGenerated.strTourOrgId = objOntapTour.OrgId__c;
				objCTourWCGenerated.strTourOrgDescription = objOntapTour.SalesOrg__c;
				objCTourWCGenerated.strTourOfficeId = objOntapTour.SalesOffice__c;
				objCTourWCGenerated.strTourOfficeDescription = objOntapTour.SalesOfficeName__c;
				objCTourWCGenerated.strTourRouteId = objOntapTour.ONTAP__RouteId__c;
				objCTourWCGenerated.strTourId = objOntapTour.AlternateName__c;
				objCTourWCGenerated.strTourStatus = objOntapTour.ONTAP__TourStatus__c;
				objCTourWCGenerated.strTourSubStatus = objOntapTour.TourSubStatus__c;
				objCTourWCGenerated.strTourStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
				objCTourWCGenerated.datTourDate = objOntapTour.ONTAP__TourDate__c;
				lstCTourWCGenerated.add(objCTourWCGenerated);
			}
			return lstCTourWCGenerated;
		}
		return null;
	}

	/**
	 * This method update the status color of the List of Tour Wrapper class.
	 *
	 * @param lstCTourWCToUpdateStatusText	List<CockpitTourWrapperClass>.
	 * @return List<CockpitTourWrapperClass>
	 */
	public static List<CockpitTourWrapperClass> updateStatusColorTextOfLstCTourWC(List<CockpitTourWrapperClass> lstCTourWCToUpdateStatusText) {

		//Variables.
		List<String> lstStringOntapSubStatusGroupGreen = (AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_SUBSTATUS_GROUP_GREEN).split(AllMobileStaticVariablesClass.STRING_SYMBOL_SEMI_COLON);
		List<String> lstStringOntapSubStatusGroupYellow = (AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_SUBSTATUS_GROUP_YELLOW).split(AllMobileStaticVariablesClass.STRING_SYMBOL_SEMI_COLON);
		List<String> lstStringOntapSubStatusGroupRed = (AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_SUBSTATUS_GROUP_RED).split(AllMobileStaticVariablesClass.STRING_SYMBOL_SEMI_COLON);

		//Update the Tour Wrapper list.
		if(!lstCTourWCToUpdateStatusText.isEmpty()) {
			for(CockpitTourWrapperClass objCTourWCIterated : lstCTourWCToUpdateStatusText) {
				if(lstStringOntapSubStatusGroupGreen.contains(objCTourWCIterated.strTourSubStatus)) {
					objCTourWCIterated.strTourStatusColorText = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_STATUS_VERDE;
				} else if(lstStringOntapSubStatusGroupYellow.contains(objCTourWCIterated.strTourSubStatus)) {
					objCTourWCIterated.strTourStatusColorText = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_STATUS_AMARILLO;
				} else if(lstStringOntapSubStatusGroupRed.contains(objCTourWCIterated.strTourSubStatus)) {
					objCTourWCIterated.strTourStatusColorText = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_STATUS_ROJO;
				}
			}
			return lstCTourWCToUpdateStatusText;
		}
		return null;
	}

	/**
	 * This method creates a list of Office Wrapper class.
	 *
	 * @param lstCTourWCToGenerateLstOfficeWC	List<CockpitTourWrapperClass>.
	 * @return List<CockpitOfficeWrapperClass>.
	 */
	public static List<CockpitOfficeWrapperClass> createLstCOfficeWCFLstCTourWC(List<CockpitTourWrapperClass> lstCTourWCToGenerateLstOfficeWC) {

		//Variables.
		Integer intFlagExistinOffice = 0;
		CockpitOfficeWrapperClass objCOfficeWC;
		List<CockpitOfficeWrapperClass> lstCOfficeWCGenerated = new List<CockpitOfficeWrapperClass>();

		//Create the Office Wrapper list.
		if(!lstCTourWCToGenerateLstOfficeWC.isEmpty()) {
			for(CockpitTourWrapperClass objCTourWCIterated : lstCTourWCToGenerateLstOfficeWC) {
				if(!lstCOfficeWCGenerated.isEmpty()) {
					for(CockpitOfficeWrapperClass objCOfficeWCIterated : lstCOfficeWCGenerated) {

						//If the CockpitTourWrapperClass object finds a CockpitOfficeWrapperClass that can be associated with, just add it.
						if((objCOfficeWCIterated.strOfficeId == objCTourWCIterated.strTourOfficeId) && !(objCOfficeWCIterated.lstCockpitTourWrapperClass.contains(objCTourWCIterated))) {
							objCOfficeWCIterated.lstCockpitTourWrapperClass.add(objCTourWCIterated);
							intFlagExistinOffice++;
						}
					}

					//Create new CockpitOfficeWrapperClass object when the CockpitTourWrapperClass has no CockpitOfficeWrapperClass to associate with and add it.
					if(intFlagExistinOffice == 0) {
						objCOfficeWC = new CockpitOfficeWrapperClass();
						objCOfficeWC.strOfficeDRVId = objCTourWCIterated.strTourDRVId;
						objCOfficeWC.strOfficeDRVDescription = objCTourWCIterated.strTourDRVDescription;
						objCOfficeWC.strOfficeOrgId = objCTourWCIterated.strTourOrgId;
						objCOfficeWC.strOfficeOrgDescription = objCTourWCIterated.strTourOrgDescription;
						objCOfficeWC.strOfficeId = objCTourWCIterated.strTourOfficeId;
						objCOfficeWC.strOfficeDescription = objCTourWCIterated.strTourOfficeDescription;
						objCOfficeWC.strOfficeStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
						objCOfficeWC.datOfficeDate = objCTourWCIterated.datTourDate;
						objCOfficeWC.lstCockpitTourWrapperClass.add(objCTourWCIterated);
						lstCOfficeWCGenerated.add(objCOfficeWC);
					}
					intFlagExistinOffice = 0;
				} else {
					objCOfficeWC = new CockpitOfficeWrapperClass();
					objCOfficeWC.strOfficeDRVId = objCTourWCIterated.strTourDRVId;
					objCOfficeWC.strOfficeDRVDescription = objCTourWCIterated.strTourDRVDescription;
					objCOfficeWC.strOfficeOrgId = objCTourWCIterated.strTourOrgId;
					objCOfficeWC.strOfficeOrgDescription = objCTourWCIterated.strTourOrgDescription;
					objCOfficeWC.strOfficeId = objCTourWCIterated.strTourOfficeId;
					objCOfficeWC.strOfficeDescription = objCTourWCIterated.strTourOfficeDescription;
					objCOfficeWC.strOfficeStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
					objCOfficeWC.datOfficeDate = objCTourWCIterated.datTourDate;
					objCOfficeWC.lstCockpitTourWrapperClass.add(objCTourWCIterated);
					lstCOfficeWCGenerated.add(objCOfficeWC);
				}
			}
			return lstCOfficeWCGenerated;
		}
		return null;
	}

	/**
	 * This method updates the status color of the List of Office Wrapper class.
	 *
	 * @param lstCOfficeWCToUpdateStatusText	List<CockpitOfficeWrapperClass>.
	 * @return List<CockpitOfficeWrapperClass>.
	 */
	public static List<CockpitOfficeWrapperClass> updateStatusColorTextOfLstCOfficeWC(List<CockpitOfficeWrapperClass> lstCOfficeWCToUpdateStatusText) {

		//Variables.
		Set<String> setStringTourStatusFLstTourWC = new Set<String>();

		//Update the Office Wrapper list.
		if(!lstCOfficeWCToUpdateStatusText.isEmpty()) {
			for(CockpitOfficeWrapperClass objCOfficeWCIterated : lstCOfficeWCToUpdateStatusText) {
				setStringTourStatusFLstTourWC = new Set<String>();
				setStringTourStatusFLstTourWC = generateSetStringStatusColorTextFLstTourWC(objCOfficeWCIterated.lstCockpitTourWrapperClass);
				if(setStringTourStatusFLstTourWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_ROJO)) {
					objCOfficeWCIterated.strOfficeStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_ROJO;
				} else if(setStringTourStatusFLstTourWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_AMARILLO)) {
					objCOfficeWCIterated.strOfficeStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_AMARILLO;
				} else if(setStringTourStatusFLstTourWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_VERDE)) {
					objCOfficeWCIterated.strOfficeStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_OFFICE_STATUS_VERDE;
				}
			}
			return lstCOfficeWCToUpdateStatusText;
		}
		return null;
	}

	/**
	 * This method generates a set of Status color text of a List of Tour Wrapper class.
	 *
	 * @param lstCTourWCToGenerateSetStringStatus	List<CockpitTourWrapperClass>.
	 * @return Set<String>.
	 */
	public static Set<String> generateSetStringStatusColorTextFLstTourWC(List<CockpitTourWrapperClass> lstCTourWCToGenerateSetStringStatus) {

		//Variables.
		Set<String> setStringTourStatusFLstTourWC = new Set<String>();

		//Create the set of colors.
		if(!lstCTourWCToGenerateSetStringStatus.isEmpty()) {
			for(CockpitTourWrapperClass objCTourWCIterated : lstCTourWCToGenerateSetStringStatus) {
				setStringTourStatusFLstTourWC.add(objCTourWCIterated.strTourStatusColorText);
			}
			return setStringTourStatusFLstTourWC;
		}
		return null;
	}

	/**
	 * This method creates a list of Organization Wrapper Class from a list of Office Wrapper class.
	 *
	 * @param lstCOfficeWCToGenerateLstOrgWC.
	 * @return List<CockpitOrgWrapperClass>.
	 */
	public static List<CockpitOrgWrapperClass> createLstCOrgWCFLstCOfficeWC(List<CockpitOfficeWrapperClass> lstCOfficeWCToGenerateLstOrgWC) {

		//Variables.
		CockpitOrgWrapperClass objCOrgWC;
		List<CockpitOrgWrapperClass> lstCOrgWCGenerated = new List<CockpitOrgWrapperClass>();
		Integer intFlagExistingOrg = 0;
		if(!lstCOfficeWCToGenerateLstOrgWC.isEmpty()) {
			for(CockpitOfficeWrapperClass objCOfficeWCIterated : lstCOfficeWCToGenerateLstOrgWC) {
				if(!lstCOrgWCGenerated.isEmpty()) {
					for(CockpitOrgWrapperClass objCOrgWCIterated : lstCOrgWCGenerated) {
						if((objCOrgWCIterated.strOrgId == objCOfficeWCIterated.strOfficeOrgId) && !(objCOrgWCIterated.lstCockpitOfficeWrapperClass.contains(objCOfficeWCIterated))) {
							objCOrgWCIterated.lstCockpitOfficeWrapperClass.add(objCOfficeWCIterated);
							intFlagExistingOrg++;
						}
					}
					if(intFlagExistingOrg == 0) {
						objCOrgWC = new CockpitOrgWrapperClass();
						objCOrgWC.strOrgDRVId = objCOfficeWCIterated.strOfficeDRVId;
						objCOrgWC.strOrgDRVDescription = objCOfficeWCIterated.strOfficeDRVDescription;
						objCOrgWC.strOrgId = objCOfficeWCIterated.strOfficeOrgId;
						objCOrgWC.strOrgDescription = objCOfficeWCIterated.strOfficeOrgDescription;
						objCOrgWC.strOrgStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
						objCOrgWC.datOrgDate = objCOfficeWCIterated.datOfficeDate;
						objCOrgWC.lstCockpitOfficeWrapperClass.add(objCOfficeWCIterated);
						lstCOrgWCGenerated.add(objCOrgWC);
					}
					intFlagExistingOrg = 0;
				} else {
					objCOrgWC = new CockpitOrgWrapperClass();
					objCOrgWC.strOrgDRVId = objCOfficeWCIterated.strOfficeDRVId;
					objCOrgWC.strOrgDRVDescription = objCOfficeWCIterated.strOfficeDRVDescription;
					objCOrgWC.strOrgId = objCOfficeWCIterated.strOfficeOrgId;
					objCOrgWC.strOrgDescription = objCOfficeWCIterated.strOfficeOrgDescription;
					objCOrgWC.strOrgStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
					objCOrgWC.datOrgDate = objCOfficeWCIterated.datOfficeDate;
					objCOrgWC.lstCockpitOfficeWrapperClass.add(objCOfficeWCIterated);
					lstCOrgWCGenerated.add(objCOrgWC);
				}
			}
			return lstCOrgWCGenerated;
		}
		return null;
	}

	/**
	 * This method updates the color status of a list of Organization Wrapper class.
	 *
	 * @param lstCOrgWCToUpdateStatusText	List<CockpitOrgWrapperClass>.
	 * @return List<CockpitOrgWrapperClass>.
	 */
	public static List<CockpitOrgWrapperClass> updateStatusColorTextOfLstCOrgWC(List<CockpitOrgWrapperClass> lstCOrgWCToUpdateStatusText) {

		//Variables.
		Set<String> setStringOfficeStatusFLstOfficeWC = new Set<String>();

		//Update the Org Wrapper list.
		if(!lstCOrgWCToUpdateStatusText.isEmpty()) {
			for(CockpitOrgWrapperClass objCOrgWCIterated : lstCOrgWCToUpdateStatusText) {
				setStringOfficeStatusFLstOfficeWC = new Set<String>();
				setStringOfficeStatusFLstOfficeWC = generateSetStringStatusColorTextFLstOfficeWC(objCOrgWCIterated.lstCockpitOfficeWrapperClass);
				if(setStringOfficeStatusFLstOfficeWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_ROJO)) {
					objCOrgWCIterated.strOrgStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_ROJO;
				} else if(setStringOfficeStatusFLstOfficeWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_AMARILLO)) {
					objCOrgWCIterated.strOrgStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_AMARILLO;
				} else if(setStringOfficeStatusFLstOfficeWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_VERDE)) {
					objCOrgWCIterated.strOrgStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_ORG_STATUS_VERDE;
				}
			}
			return lstCOrgWCToUpdateStatusText;
		}
		return null;
	}

	/**
	 * This method generates a set of string of color status of a list of Office Wrapper class.
	 *
	 * @param lstCOfficeWCToGenerateSetStringStatus	List<CockpitOfficeWrapperClass>.
	 * @return Set<String>.
	 */
	public static Set<String> generateSetStringStatusColorTextFLstOfficeWC(List<CockpitOfficeWrapperClass> lstCOfficeWCToGenerateSetStringStatus) {

		//Variables.
		Set<String> setStringOfficeStatusFLstOfficeWC = new Set<String>();

		//Create the set of status color.
		if(!lstCOfficeWCToGenerateSetStringStatus.isEmpty()) {
			for(CockpitOfficeWrapperClass objCOfficeWCIterated : lstCOfficeWCToGenerateSetStringStatus) {
				setStringOfficeStatusFLstOfficeWC.add(objCOfficeWCIterated.strOfficeStatusColorText);
			}
			return setStringOfficeStatusFLstOfficeWC;
		}
		return null;
	}

	/**
	 * This method creates a list of DRV Wrapper class from a list of Organization Wrapper class.
	 *
	 * @param lstCOrgWCToGenerateLstDRVWC	List<CockpitOrgWrapperClass>.
	 * @return List<CockpitDRVWrapperClass>.
	 */
	public static List<CockpitDRVWrapperClass> createLstCDRVWCFLstCOrgWC(List<CockpitOrgWrapperClass> lstCOrgWCToGenerateLstDRVWC) {

		//Variables.
		Integer intFlagExistingDRV = 0;
		CockpitDRVWrapperClass objCDRVWC;
		List<CockpitDRVWrapperClass> lstCDRVWCGenerated = new List<CockpitDRVWrapperClass>();

		//Create the DRV Wrapper list.
		if(!lstCOrgWCToGenerateLstDRVWC.isEmpty()) {
			for(CockpitOrgWrapperClass objCOrgWCIterated : lstCOrgWCToGenerateLstDRVWC) {
				if(!lstCDRVWCGenerated.isEmpty()) {
					for(CockpitDRVWrapperClass objCDRVWCIterated : lstCDRVWCGenerated) {
						if((objCDRVWCIterated.strDRVId == objCOrgWCIterated.strOrgDRVId) && !(objCDRVWCIterated.lstCockpitOrgWrapperClass.contains(objCOrgWCIterated))) {
							objCDRVWCIterated.lstCockpitOrgWrapperClass.add(objCOrgWCIterated);
							intFlagExistingDRV++;
						}
					}
					if(intFlagExistingDRV == 0) {
						objCDRVWC = new CockpitDRVWrapperClass();
						objCDRVWC.strDRVId = objCOrgWCIterated.strOrgDRVId;
						objCDRVWC.strDRVDescription = objCOrgWCIterated.strOrgDRVDescription;
						objCDRVWC.strDRVStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
						objCDRVWC.datDRVDate = objCOrgWCIterated.datOrgDate;
						objCDRVWC.lstCockpitOrgWrapperClass.add(objCOrgWCIterated);
						lstCDRVWCGenerated.add(objCDRVWC);
					}
					intFlagExistingDRV = 0;
				} else {
					objCDRVWC = new CockpitDRVWrapperClass();
					objCDRVWC.strDRVId = objCOrgWCIterated.strOrgDRVId;
					objCDRVWC.strDRVDescription = objCOrgWCIterated.strOrgDRVDescription;
					objCDRVWC.strDRVStatusColorText = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
					objCDRVWC.datDRVDate = objCOrgWCIterated.datOrgDate;
					objCDRVWC.lstCockpitOrgWrapperClass.add(objCOrgWCIterated);
					lstCDRVWCGenerated.add(objCDRVWC);
				}
			}
			return lstCDRVWCGenerated;
		}
		return null;
	}

	/**
	 * This method updates the color status of a list of DRV Wrapper class.
	 *
	 * @param lstCDRVWCToUpdateStatusText	List<CockpitDRVWrapperClass>.
	 * @return List<CockpitDRVWrapperClass>.
	 */
	public static List<CockpitDRVWrapperClass> updateStatusColorTextOfLstCDRVWC(List<CockpitDRVWrapperClass> lstCDRVWCToUpdateStatusText) {

		//Variables.
		Set<String> setStringOrgStatusFLstOrgWC = new Set<String>();

		//Update the status color.
		if(!lstCDRVWCToUpdateStatusText.isEmpty()) {
			for(CockpitDRVWrapperClass objCDRVWCIterated : lstCDRVWCToUpdateStatusText) {
				setStringOrgStatusFLstOrgWC = new Set<String>();
				setStringOrgStatusFLstOrgWC = generateSetStringStatusColorTextFLstOrgWC(objCDRVWCIterated.lstCockpitOrgWrapperClass);
				if(setStringOrgStatusFLstOrgWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_ROJO)) {
					objCDRVWCIterated.strDRVStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_ROJO;
				} else if(setStringOrgStatusFLstOrgWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_AMARILLO)) {
					objCDRVWCIterated.strDRVStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_AMARILLO;
				} else if(setStringOrgStatusFLstOrgWC.contains(AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_VERDE)) {
					objCDRVWCIterated.strDRVStatusColorText = AllMobileStaticVariablesClass.STRING_ACCOUNT_DRV_STATUS_VERDE;
				}
			}
			return lstCDRVWCToUpdateStatusText;
		}
		return null;
	}

	/**
	 * This method generates a set of color status from a list of Organization Wrapper class.
	 *
	 * @param lstCOrgWCToGenerateSetStringStatus	List<CockpitOrgWrapperClass>.
	 * @return Set<String>.
	 */
	public static Set<String> generateSetStringStatusColorTextFLstOrgWC(List<CockpitOrgWrapperClass> lstCOrgWCToGenerateSetStringStatus) {

		//Variables.
		Set<String> setStringOrgStatusFLstOrgWC = new Set<String>();

		//Create the set of status color.
		if(!lstCOrgWCToGenerateSetStringStatus.isEmpty()) {
			for(CockpitOrgWrapperClass objCOrgWCIterated : lstCOrgWCToGenerateSetStringStatus) {
				setStringOrgStatusFLstOrgWC.add(objCOrgWCIterated.strOrgStatusColorText);
			}
			return setStringOrgStatusFLstOrgWC;
		}
		return null;
	}

	/**
	 * This method generates a list of Objects according the visualization level of the front end.
	 *
	 * @param intVisualizationLevel	Integer.
	 * @param datExecutionDate	Date.
	 * @param strIdSearch	String.
	 * @param strTourStatus 	String.
	 */
	public static Map<Integer,Object> generateListObjsAccordVisualLevel(Integer intVisualizationLevel, Date datExecutionDate, String strIdSearch, String strTourStatus) {

		//Variables.
		Map<Integer,Object> mapsIntObjectsResults = new Map<Integer,Object>();
		List<CockpitTourWrapperClass> lstCkpitTourWC = new List<CockpitTourWrapperClass>();
		List<CockpitOfficeWrapperClass> lstCkpitOfficeWC = new List<CockpitOfficeWrapperClass>();
		List<CockpitOrgWrapperClass> lstCkpitOrgWC = new List<CockpitOrgWrapperClass>();
		List<CockpitDRVWrapperClass> lstCkpitDRVWC = new List<CockpitDRVWrapperClass>();

		//Generate Wrapper list according the visualization level.
		switch on intVisualizationLevel {

			//DRV.
			when 0 {
				List<ONTAP__tour__c> lstResultsOntapTour = getONTAPTourToSF(strIdSearch, intVisualizationLevel, datExecutionDate, strTourStatus);
				if(lstResultsOntapTour.isEmpty()) {
					return null;
				}

				//Create and update Org Wrapper list.
				lstCkpitTourWC = createLstCTourWCFLstOntapTour(lstResultsOntapTour);
				lstCkpitTourWC = updateStatusColorTextOfLstCTourWC(lstCkpitTourWC);

				//Create and update Office Wrapper list.
				lstCkpitOfficeWC = createLstCOfficeWCFLstCTourWC(lstCkpitTourWC);
				lstCkpitOfficeWC = updateStatusColorTextOfLstCOfficeWC(lstCkpitOfficeWC);

				//Create and update Org Wrapper list.
				lstCkpitOrgWC = createLstCOrgWCFLstCOfficeWC(lstCkpitOfficeWC);
				lstCkpitOrgWC = updateStatusColorTextOfLstCOrgWC(lstCkpitOrgWC);

				//Create and update DRV Wrapper list.
				lstCkpitDRVWC = createLstCDRVWCFLstCOrgWC(lstCkpitOrgWC);
				lstCkpitDRVWC = updateStatusColorTextOfLstCDRVWC(lstCkpitDRVWC);

				//Assign previous wrapper list to map.
				mapsIntObjectsResults.put(0, lstCkpitDRVWC);
				mapsIntObjectsResults.put(1, lstCkpitOrgWC);
				mapsIntObjectsResults.put(2, lstCkpitOfficeWC);
				mapsIntObjectsResults.put(3, lstCkpitTourWC);
			}

			//UEN
			when 1 {
				List<ONTAP__tour__c> lstResultsOntapTour = getONTAPTourToSF(strIdSearch, intVisualizationLevel, datExecutionDate, strTourStatus);
				if(lstResultsOntapTour.isEmpty()) {
					return null;
				}

				//Create and update Org Wrapper list.
				lstCkpitTourWC = createLstCTourWCFLstOntapTour(lstResultsOntapTour);
				lstCkpitTourWC = updateStatusColorTextOfLstCTourWC(lstCkpitTourWC);

				//Create and update Office Wrapper list.
				lstCkpitOfficeWC = createLstCOfficeWCFLstCTourWC(lstCkpitTourWC);
				lstCkpitOfficeWC = updateStatusColorTextOfLstCOfficeWC(lstCkpitOfficeWC);

				//Create and update Org Wrapper list.
				lstCkpitOrgWC = createLstCOrgWCFLstCOfficeWC(lstCkpitOfficeWC);
				lstCkpitOrgWC = updateStatusColorTextOfLstCOrgWC(lstCkpitOrgWC);

				//Assign previous wrapper list to map.
				mapsIntObjectsResults.put(1, lstCkpitOrgWC);
				mapsIntObjectsResults.put(2, lstCkpitOfficeWC);
				mapsIntObjectsResults.put(3, lstCkpitTourWC);
			}

			//OFFICE.
			when 2 {
				List<ONTAP__tour__c> lstResultsOntapTour = getONTAPTourToSF(strIdSearch, intVisualizationLevel, datExecutionDate, strTourStatus);
				if(lstResultsOntapTour.isEmpty()) {
					return null;
				}

				//Create and update Org Wrapper list.
				lstCkpitTourWC = createLstCTourWCFLstOntapTour(lstResultsOntapTour);
				lstCkpitTourWC = updateStatusColorTextOfLstCTourWC(lstCkpitTourWC);

				//Create and update Office Wrapper list.
				lstCkpitOfficeWC = createLstCOfficeWCFLstCTourWC(lstCkpitTourWC);
				lstCkpitOfficeWC = updateStatusColorTextOfLstCOfficeWC(lstCkpitOfficeWC);

				//Assign previous wrapper list to map.
				mapsIntObjectsResults.put(2, lstCkpitOfficeWC);
				mapsIntObjectsResults.put(3, lstCkpitTourWC);
			}

			//ROUTE, Tour Id, Tour Status (DRV, Org, Office, Route).
			when 3, 4, 5, 6, 7, 8 {
				List<ONTAP__tour__c> lstResultsOntapTour = getONTAPTourToSF(strIdSearch, intVisualizationLevel, datExecutionDate, strTourStatus);
				if(lstResultsOntapTour.isEmpty()) {
					return null;
				}

				//Create and update Org Wrapper list.
				lstCkpitTourWC = createLstCTourWCFLstOntapTour(lstResultsOntapTour);
				lstCkpitTourWC = updateStatusColorTextOfLstCTourWC(lstCkpitTourWC);

				//Assign previous wrapper list to map.
				mapsIntObjectsResults.put(intVisualizationLevel, lstCkpitTourWC);
			}
		}
		return mapsIntObjectsResults;
	}

	/**
	 * This method get a list of Tours.
	 *
	 * @param strId	String.
	 * @param intVisualizationLevel	Integer.
	 * @param datExecutionDate	Date.
	 * @param strTourStatus	String.
	 * @return List<ONTAP__tour__c>.
	 */
	public static List<ONTAP__tour__c> getONTAPTourToSF(String strId, Integer intVisualizationLevel, Date datExecutionDate, String strTourStatus) {

		//Variables.
		List<ONTAP__tour__c> lstToursToSF;
		String strdatExecutionDate = Json.serialize(datExecutionDate).replace('"',AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK);

		//DRV.
		if(intVisualizationLevel == 0) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_DRVID_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate;
			lstToursToSF = Database.query(strQuery);

		//ORG.
		} else if(intVisualizationLevel == 1) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_ORG_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate;
			lstToursToSF = Database.query(strQuery);

		//OFFICE.
		} else if(intVisualizationLevel == 2) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_DATE_AND_OFFICE_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate;
			lstToursToSF = Database.query(strQuery);

		//ROUTE.
		} else if(intVisualizationLevel == 3) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_STATUS_BY_ROUTE_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate;
			lstToursToSF = Database.query(strQuery);

		//Tour Id.
		} else if(intVisualizationLevel == 4) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_ALTERNATE_NAME_WHERE_LIKE + strId  + AllMobileStaticVariablesClass.STRING_PERCENT + AllMobileStaticVariablesClass.STRING_SIMPLE_QUOTE;
			lstToursToSF = Database.query(strQuery);

		//Tour Status by DRV.
		} else if(intVisualizationLevel == 5) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_DRV_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate  + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_STATUS_FILTER_WHERE + strTourStatus + AllMobileStaticVariablesClass.STRING_SIMPLE_QUOTE;
			lstToursToSF = Database.query(strQuery);

		//Tour Status by Org.
		} else if(intVisualizationLevel == 6) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_ORG_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate  + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_STATUS_FILTER_WHERE + strTourStatus + AllMobileStaticVariablesClass.STRING_SIMPLE_QUOTE;
			lstToursToSF = Database.query(strQuery);

		//Tour Status by Office.
		} else if(intVisualizationLevel == 7) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_OFFICE_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate  + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_STATUS_FILTER_WHERE + strTourStatus + AllMobileStaticVariablesClass.STRING_SIMPLE_QUOTE;
			lstToursToSF = Database.query(strQuery);

		//Tour Status by Route.
		} else if(intVisualizationLevel == 8) {
			String strQuery = AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_SELECT;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_ALL_QUERIES_COCKPITSTATUSLOGICCLASS_FROM;
			strQuery += AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_BY_TOUR_STATUS_ROUTE_WHERE_LIKE + strId + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_DATE_FILTER_WHERE + strdatExecutionDate  + AllMobileStaticVariablesClass.STRING_ONTAP_TOUR_QUERY_TOUR_STATUS_FILTER_WHERE + strTourStatus + AllMobileStaticVariablesClass.STRING_SIMPLE_QUOTE;
			lstToursToSF = Database.query(strQuery);
		}
		return lstToursToSF;
	}
}