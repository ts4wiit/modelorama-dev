@isTest
private class ISSM_UserSelector_tst {
	
	@isTest static void selectById() {

		User user1 = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
		User user2 = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba2'+ Datetime.now().millisecond());
        
		Set<Id> idSet = new Set<Id>();
		idSet.add(user1.Id);
		idSet.add(user2.Id);

		Test.startTest();		
		List<User> users = ISSM_UserSelector_cls.newInstance().selectById( idSet);
		Test.stopTest();
		
		system.assertEquals(2,users.size());
	}
	
	@isTest static void selectByIdUserId() {
		User user1 = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        
		Test.startTest();		
		User user = ISSM_UserSelector_cls.newInstance().selectById( user1.Id);
		Test.stopTest();
		
		system.assertNotEquals(user, null);
		system.assertEquals(user.Id,user1.Id);
	}

	@isTest static void selectByIdUserIdString() {

		User user1 = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        
		Test.startTest();		
		List<User> users = ISSM_UserSelector_cls.newInstance().selectById( user1.Id + '');
		Test.stopTest();
		
		system.assertNotEquals(users, null);
		system.assertEquals(1,users.size());
	}

}