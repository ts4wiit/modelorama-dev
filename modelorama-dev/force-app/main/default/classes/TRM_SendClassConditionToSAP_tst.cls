/******************************************************************************* 
* Developed by      :   Avanxo México
* Author            :   Oscar Alvarez
* Project           :   AbInbev - Trade Revenue Management
* Description       :   Test class for batch class TRM_SendClassConditionToSAP_cls
*
*  No.           Date              Author                      Description
* 1.0    14-Septiembre-2018      Oscar Alvarez                   CREATION
*******************************************************************************/
@isTest
private class TRM_SendClassConditionToSAP_tst {
    @testSetup static void loadData() {
    
    //Insert custom configuration
         insert new ISSM_PriceEngineConfigWS__c(ISSM_EndPoint__c = 'https://trm-admin-conditions.us-w1.cloudhub.io/api/conditions',
                                               ISSM_Method__c = 'POST',
                                               ISSM_HeaderContentType__c = 'application/json; charset=UTF-8',
                                               Name = 'SendClassConditionMulesoft');
    //Insert Budget
        TRM_Budget__c budget                = new TRM_Budget__c();
            budget.TRM_BudgetName__c        = 'budget test 01';
            budget.TRM_StartDate__c         = System.now().Date().addDays(2);
            budget.TRM_EndDate__c           = System.now().Date().addDays(6);
            budget.TRM_Status__c            = 'New';
            budget.TRM_BudgetType__c        = 'National';
            budget.TRM_RestrictedBudget__c  = true;
        insert budget;

    //Insert catalogos MDM_Parameter__c
        MDM_Parameter__c parameter1 = createMDMParameter('Cerveza',true,true,'SectorProd','01','Cerveza','SectorProd-01');
        insert parameter1;
        MDM_Parameter__c parameter2 = createMDMParameter('Media',true,true,'GrupoMateriales1','02','Media','GrupoMateriales1-02');
        insert parameter2;
        MDM_Parameter__c parameter3 = createMDMParameter('24/355 ml.',true,true,'GrupoMateriales2','42','24/355 ml.','GrupoMateriales2-42');
        insert parameter3;
        MDM_Parameter__c parameter4 = createMDMParameter('POR DEFINIR',true,true,'ConditionGroup3','99','POR DEFINIR','GpoConditions3-99');
        insert parameter4;
        MDM_Parameter__c parameter5 = createMDMParameter('Botella Abierta Imag',true,true,'Segmento','40','Botella Abierta Imag','Segmento-40');
        insert parameter5;
        MDM_Parameter__c parameter6 = createMDMParameter('ALTIPLANO',true,true,'PriceZone','01','ALTIPLANO','PriceZone-01');
        insert parameter6;

    //Insert Product
        String RecType = ISSM_UtilityFactory_cls.getRecordTypeIdByDeveloperName('ONTAP__Product__c',Label.TRM_Products);

        ONTAP__Product__c product1              = new ONTAP__Product__c();
            product1.ONTAP__ExternalKey__c      = '000000000003000006';
            product1.ONTAP__MaterialProduct__c  = 'CORONA EXTRA CLARA 24/355 ML CT R';
            product1.ISSM_SectorCode__c         = parameter1.id;
            product1.ISSM_Quota__c              = parameter2.id;
            product1.ISSM_MaterialGroup2__c     = parameter3.id;
            product1.RecordTypeId               = RecType;
            product1.ONTAP__ProductType__c      = 'FERT';
        insert product1;
    // insert conditionClass
        TRM_ConditionClass__c conditionClass            = new TRM_ConditionClass__c();
            conditionClass.TRM_StartDate__c             = System.now().Date().addDays(2);               
            conditionClass.TRM_EndDate__c               = System.now().Date().addDays(4);
            conditionClass.TRM_Budget__c                = budget.Id;
            conditionClass.TRM_ConditionClass__c        = 'ZMIW';
            conditionClass.TRM_AccessSequence__c        = '974';
            conditionClass.TRM_StatePerZone__c          = 'PriceZone-01';
            conditionClass.TRM_Status__c                = 'TRM_Approved';
            conditionClass.TRM_ConditionUnit__c         = 'MXN';
            conditionClass.TRM_Description__c           = 'Condition Class ZMIW 974 - class tst';
            conditionClass.TRM_Segment__c               = 'Segmento-40';
            conditionClass.TRM_DistributionChannel__c   = '01';
        insert conditionClass;
    }

    @isTest static void testsendClassConditionToSAP(){
        Test.startTest();
        List<TRM_ConditionClass__c> lstConditionClass = [ SELECT Id
                                                                ,TRM_Status__c
                                                                ,TRM_ConditionClass__c
                                                                ,TRM_ReadyApproval__c
                                                                ,TRM_ExternalId__c FROM TRM_ConditionClass__c];
        TRM_SendClassConditionToSAP_cls.sendClassConditionToSAP(lstConditionClass);
        Test.stopTest();
    }
    @isTest static void testsendCondSAPSettlementPeriods(){
        Test.startTest();
            // insert settlement Periods
            TRM_SettlementPeriods__c settlementPeriods      = new TRM_SettlementPeriods__c();
                settlementPeriods.TRM_ConditionClas__c      = 'ZMIW';
                settlementPeriods.TRM_Description__c        = 'Período de Liquidación TEST';
                settlementPeriods.TRM_EffectiveDate__c      = System.now().Date().addDays(1);
                settlementPeriods.TRM_EndTime__c            = '23:45:00';
                settlementPeriods.TRM_RecordVolume__c       = 10000;
                settlementPeriods.TRM_StartTime__c          = '00:00:00';
            Insert settlementPeriods;
            TRM_ConditionClass__c[] lstConditionClass = new List<TRM_ConditionClass__c>();
            for(TRM_ConditionClass__c cc: [ SELECT Id
                                                ,TRM_Status__c
                                                ,TRM_ConditionClass__c
                                                ,TRM_ReadyApproval__c
                                                ,TRM_ExternalId__c 
                                          FROM TRM_ConditionClass__c]){
                cc.TRM_Status__c = 'TRM_Cancelled';
                cc.TRM_ReadyApproval__c = false;
                lstConditionClass.add(cc);
            }
            update lstConditionClass;
            TRM_SendClassConditionToSAP_cls.sendClassConditionToSAP(lstConditionClass);
        Test.stopTest();
    }

    @isTest static void testSendClassCondNotFuture(){
        Test.startTest();
            List<TRM_ConditionClass__c> lstConditionClass = [ SELECT Id
                                                                ,TRM_Status__c
                                                                ,TRM_ConditionClass__c
                                                                ,TRM_ReadyApproval__c
                                                                ,TRM_ExternalId__c FROM TRM_ConditionClass__c];
            String strJSON = TRM_ApprovalConditions_ctr.generateJson(lstConditionClass, Label.TRM_CreateCC, Integer.valueOf(Label.TRM_NumberLotsProcessed));
            TRM_SendClassConditionToSAP_cls.sendClassCondNotFuture(strJSON,Label.TRM_CreateCC);
        Test.stopTest();
    }

    

    public static MDM_Parameter__c createMDMParameter(String name
                                                    , boolean active
                                                    , boolean activeRevenue
                                                    , String catalog
                                                    , String code
                                                    , String description
                                                    , String externalId){
        MDM_Parameter__c parameter      = new MDM_Parameter__c();
            parameter.Name              = name;
            parameter.Active__c         = active;
            parameter.Active_Revenue__c = activeRevenue; 
            parameter.Catalog__c        = catalog;
            parameter.Code__c           = code;
            parameter.Description__c    = description;
            parameter.ExternalId__c     = externalId;
    return parameter;
    }
}