/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		:   Class that generates the request and response for the integration of the creation of combos in SAP
*
* No.       Fecha              Autor                      Descripción
* 1.0    11-Junio-2018      Oscar Alvarez                   Creación
*******************************************************************************/
public with sharing class ISSM_SendComboToMulesoft_cls{
    Public static Integer counterRetries = 0;
    Public static List<ISSM_WebServicesLog__c> lstWebServicesLog= new List<ISSM_WebServicesLog__c>();
    /*
    * Method Name	: SendComboToMulesoft
    * Purpose		: Generates the JSON request for the integration
    * @Param        : lstCombo = List of combos to integrate
    */
    @InvocableMethod  
    public static  void SendComboToMulesoft(List<ISSM_Combos__c> lstCombo){
        List<String> lstIdCombos = new List<String>();
        String strExternalKeyCmb =  lstCombo[0].ISSM_ExternalKey__c;        
        String create = Label.TRM_OperationCreate;
        for(ISSM_Combos__c combo : lstCombo) lstIdCombos.add(combo.Id);
        Database.executeBatch(new TRM_SendComboToMulesoft_bch('',lstIdCombos,create), Integer.valueOf(Label.TRM_NumberLotsProcessed));
    }
    /*
    * Method Name	: SendComboToMulesoftIntegrate
    * Purpose		: Send the request to SAP through mulesoft with method future
    * @Param        : strSerializeJson = request JSON
    * @Param        : operation = operation that will execute mulesoft (A, B or C)	
    */
    @Future(callout=true)
    public static void SendComboToMulesoftIntegrate(String strSerializeJson,String operation){
        String nameConfigCustom = Label.TRM_NameConfigCustom;        
        ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
        try{
            System.debug('StrSerializeJsonMA--> '+ strSerializeJson);
            List<ISSM_CallOutService_ctr.WprResponseService> response = new List<ISSM_CallOutService_ctr.WprResponseService>();
            response = CTR.SendRequest(strSerializeJson,nameConfigCustom); 
            System.debug('###RESPONSE: '+response); 
            System.debug('###Start saving log');
            saveLogWS(response, nameConfigCustom, strSerializeJson, operation); 
            insert lstWebServicesLog;
            System.debug('###End saving log');  
            if(Test.isRunningTest()) throw new MyException('Exception test');
           }catch(Exception exc){System.debug('EXC_CANCELCOMBO_AVAILABLE: '+exc+'Message: '+exc.getMessage());}	
    }
    /*
    * Method Name	: SendComboToMulesoftIntegrate
    * Purpose		: Send the request to SAP through mulesoft with method future
    * @Param        : strSerializeJson = request JSON
    * @Param        : operation = operation that will execute mulesoft (A, B or C)	
    */
    public static List<ISSM_WebServicesLog__c> SendCmbToMulesoftNotFuture(String strSerializeJson,String operation){
        
        String nameConfigCustom = Label.TRM_NameConfigCustom;   
        ISSM_CallOutService_ctr CTR = new ISSM_CallOutService_ctr();
        
        try{
            List<ISSM_CallOutService_ctr.WprResponseService> response = new List<ISSM_CallOutService_ctr.WprResponseService>();
            response = CTR.SendRequest(strSerializeJson,nameConfigCustom); 
            System.debug('###RESPONSE-RETRY: '+response);
            saveLogWS(response, nameConfigCustom, strSerializeJson, operation); 
            if(Test.isRunningTest()) throw new MyException('Exception test');
           }catch(Exception exc){System.debug('ERROR: '+exc+'Message: '+exc.getMessage());}	
        return lstWebServicesLog;
    }
    
    /*
    * Method Name	: saveLogWS
    * Purpose		: Save the response of the service and if it fell into error it retry (Retry is controlled with the label Label.TRM_RetrySendCombo)
    * @Param        : response =  wrapper of response; 
    * @Param        : nameConfigCustom = custom configuration name; 
    * @Param        : requestSerializeJson = request JSON;
    * @Param        : operation = operation that will execute mulesoft (A, B or C)				
    */
     private static void saveLogWS(List<ISSM_CallOutService_ctr.WprResponseService> response
                                   , String nameConfigCustom
                                   , String requestSerializeJson
                                   , String operation){
                                       
            counterRetries ++;
            Map<String, ISSM_PriceEngineConfigWS__c> MapConfiguracionWs = ISSM_PriceEngineConfigWS__c.getAll();                                
            Integer maxLengthRequest = Integer.valueOf(Label.TRM_MaxLengthRequest);
            ISSM_WebServicesLog__c webServiceLog = new ISSM_WebServicesLog__c();                           
                webServiceLog.ISSM_EndPoint__c = MapConfiguracionWs.get(nameConfigCustom).ISSM_EndPoint__c;
                webServiceLog.ISSM_Message__c = response[0].strMessageError != null ? Label.TRM_Error :Label.TRM_Success;
                webServiceLog.ISSM_Request__c = requestSerializeJson.length() > maxLengthRequest ? requestSerializeJson.substring(0, maxLengthRequest) : requestSerializeJson;
                webServiceLog.ISSM_Response__c = response[0].strBodyService != null ? response[0].strBodyService : response[0].strMessageError;
                webServiceLog.ISSM_Source__c = operation == Label.TRM_OperationCreate ? (Label.TRM_CreateCombos + ' - ' + Label.TRM_OperationCreate) : (operation == Label.TRM_OperationCancel ? (Label.TRM_CancelCombos + ' - ' + Label.TRM_OperationCancel) : (operation == Label.TRM_OperationModify ? (Label.TRM_ModifyCombos + ' - ' + Label.TRM_OperationModify) :''));
     		lstWebServicesLog.add(webServiceLog);
            if(Test.isRunningTest()) webServiceLog.ISSM_Message__c =  Label.TRM_Error;
            if(webServiceLog.ISSM_Message__c == Label.TRM_Error &&  counterRetries < Integer.valueOf(Label.TRM_RetrySendCombo)){
                    SendCmbToMulesoftNotFuture(requestSerializeJson,operation);
            }                           
   	}
    // Wrapper for cancellation combo 
    public class WrpCombo{
        public String ISSM_ExternalKey_c;
        public String Operation;
        
        public WrpCombo(String ISSM_ExternalKey_c, String Operation){
            this.ISSM_ExternalKey_c = ISSM_ExternalKey_c;
            this.Operation = Operation;
        }
    }
    public class MyException extends Exception{}
}