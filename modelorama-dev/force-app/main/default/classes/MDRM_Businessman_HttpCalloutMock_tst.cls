@isTest
global class MDRM_Businessman_HttpCalloutMock_tst implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();

        String XmlRequest = '';
        
        /*XmlRequest += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">';
        XmlRequest += '<soapenv:Header>';
        XmlRequest += '</soapenv:Header>';
        XmlRequest += '<soapenv:Body>';
        XmlRequest += '<urn:login>';
        XmlRequest += '<urn:username>lmorales@avanxo.com.mdrmdev</urn:username>';
        XmlRequest += '<urn:password>sb:modelo05</urn:password>';
        XmlRequest += '</urn:login>';
        XmlRequest += '</soapenv:Body>';
        XmlRequest += '</soapenv:Envelope>';*/

        XmlRequest = '{type: "Contact", Id: "003g000001YuQPsAAN", MobilePhone: "30012345", Phone: "10012345", Email: "prueba@prueba.com"}';

        //response.setHeader('Content-Type', 'application/json');
        response.setHeader('Content-Type', 'text/xml;charset=utf-16');
        //response.setHeader('SOAPAction', 'urn:partner.soap.sforce.com/Soap/loginRequest');

        //response.setHeader('Content-length', string.valueof(XmlRequest.length()));
        //response.setMethod('POST');

        //response.setBody('{"animals": ["majestic badger", "fluffy bunny", "scary bear", "chicken", "mighty moose"]}');
        response.setBody(XmlRequest);
        //response.setCompressed(true);

        response.setStatusCode(200);
        return response; 
    }
}