/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que permite generar el request y response para el servicio de cExistencias

    Information about changes (versions) 
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       07-Julio-2017   Luis Licona                  Creación de la Clase
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_MaterialAvailability_cls {
	
	public WrpMaterials[] LstMaterials 	   = new List<WrpMaterials>();
	public MainMaterials[] LstAllMaterials = new List<MainMaterials>();
	public ISSM_CallOutService_ctr CTR 	   = new ISSM_CallOutService_ctr();
	public ISSM_OnCallQueries_cls CTRSOQL  = new ISSM_OnCallQueries_cls(); 
	public String StrSalesOff{get;set;}
	public ISSM_DeserializeMaterialAvailability_cls ObjDeserealized{get;set;}
	
	//invoque integration, serialize and deserialeze json	
	public ISSM_DeserializeMaterialAvailability_cls CallOutMaterialAvailability(String StrIdAcc, ONTAP__Product__c[] LstProd, 
																				Boolean isBonus, ISSM_Bonus__c[] LstBonuses){
		try{

		StrSalesOff = CTRSOQL.getSalesOfficeAccnt(StrIdAcc);

		if(!LstProd.isEmpty()){
		    for(ONTAP__Product__c objProd : LstProd)
		        LstMaterials.add(new WrpMaterials(objProd.ONCALL__Material_Number__c,StrSalesOff,objProd.ISSM_Uint_Measure_Code__c));
		}
	
		if(!LstBonuses.isEmpty() && isBonus){
			for(ISSM_Bonus__c objBonus : LstBonuses)
		        LstMaterials.add(new WrpMaterials(objBonus.ISSM_Material_Number__c,StrSalesOff,objBonus.ISSM_Product__r.ISSM_Uint_Measure_Code__c));   
	    }

		LstAllMaterials.add(new MainMaterials(LstMaterials));

		if(!LstAllMaterials.isEmpty()){
			String StrSerializeJson = JSON.serializePretty(LstAllMaterials[0]);
			System.debug('StrSerializeJsonMA-->'+StrSerializeJson);
			List<ISSM_CallOutService_ctr.WprResponseService> response = CTR.SendRequest(StrSerializeJson,Label.ISSM_ConfigurationMatAvailable);
			System.debug(loggingLevel.Error, '*** response: ' + response);
			System.debug(loggingLevel.Error, '*** Test.isRunningTest(): ' + Test.isRunningTest());
			ObjDeserealized = ISSM_DeserializeMaterialAvailability_cls.parse(response[0].strBodyService);//(!Test.isRunningTest()) ? ISSM_DeserializeMaterialAvailability_cls.parse(response[0].strBodyService) : ISSM_DeserializeMaterialAvailability_cls.parse('{ "numClient": "0100169817", "startDate": "2018-10-01", "EndDate": "2018-10-31", "CrossSelling": [ { "sku": "3000005", "numBoxes": 7, "curentSale": 3, "brand": "Cucapa", "packageSize": "Media", "rank": 1, "documentType": 0 }, { "sku": "3000060", "numBoxes": 15, "curentSale": 5, "brand": "Corona Extra", "packageSize": "Bote", "rank": 2, "documentType": 0 }, { "sku": "3004017", "numBoxes": 5, "curentSale": 1, "brand": "Corona Extra", "packageSize": "Bote", "rank": 1, "documentType": 0 } ], "Replanishment": [ { "sku": "3000071", "numBoxes": 13, "curentSale": 5, "brand": "Corona Extra", "packageSize": "Bote", "rank": 10, "documentType": 1 }, { "sku": "3000020", "numBoxes": 15, "curentSale": 2, "brand": "Cucapa", "packageSize": "Media", "rank": 10, "documentType": 1 } ], "Substitute": [ { "brand": "Michelob", "packageSize": "Bote", "substitute": 1, "brandSubstitute1": "Corona Extra", "packageSizeSubstitute1": "Bote", "brandSubstitute2": "3000055", "documentType": 2 }, { "brand": "Negra Modelo", "packageSize": "Media", "substitute": 1, "brandSubstitute1": "Corona Extra", "packageSizeSubstitute1": "Bote", "brandSubstitute2": "3000027", "documentType": 2 } ] }');
			System.debug('ObjDeserealizedMA--->'+ObjDeserealized);
		}
		}catch(Exception exc){System.debug('EXCMATERIALAVAILABLE '+exc);exc.setMessage(' '+exc.getMessage()+ ' | '+Label.ISSM_ConfigurationMatAvailable);throw exc;
		}		  
		return ObjDeserealized;
	}

	//Wrapper for serialize json
	public class MainMaterials{
		WrpMaterials[] materials;

		public MainMaterials(WrpMaterials[] materials){
			this.materials= materials;
		}
	}

	public class WrpMaterials{
		public String productId;
		public String salesOffice;
		public String unit;
		
		public WrpMaterials(String productId, String salesOffice, String unit){
			this.productId= productId;
			this.salesOffice=salesOffice;
			this.unit=unit;
		}
	}
}