/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Wrapper for OnCall MaterialAvailable

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       25-Agosto-2017        Luis Licona                  Class Creation
    ================================================================================================
****************************************************************************************************/
global class ISSM_Wrapper_cls {
	public ISSM_Wrapper_cls() {}
   
	/**
    * Wrapper Class for the Deals
    **/
    global class WrpDealsMaterial{
        global String discountCondition;
        global Decimal discountAmount;
        global String material;
        global Integer quantity;
        global Decimal unitPrice;
        global String promoNumber;
        
        public WrpDealsMaterial(String discountCondition,Decimal discountAmount,String material,Integer quantity,Decimal unitPrice,
                                String promoNumber){
            this.discountCondition = discountCondition;
            this.discountAmount = discountAmount;
            this.material = material ;
            this.quantity = quantity ;
            this.unitPrice = unitPrice ;
            this.promoNumber = promoNumber ;
        }
    }

    /**
    * Wrappe Class for the Deals to send a SAP 
    **/
    global class WrpDealsSend{
        global String idOrderSAP;
        global WrpDealsMaterial[] materials;
             
        global WrpDealsSend(String idOrderSAP,WrpDealsMaterial[] materials){
            this.idOrderSAP = idOrderSAP;
            this.materials = materials;
            
        }
    }
}