/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
Comentarios ":XXXX
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0   28-Agosto-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
public  class ISSM_NewTaskHandler_cls {
   
    public static void UpdateCallEffective(List<Task> lstTaskReceived, Decimal NumberCallsMade) {
        //try {
            ONCALL__Call__c objCall = new ONCALL__Call__c();
            List<ONCALL__Call__c> lstCall = new List<ONCALL__Call__c>();
            
            for(Task objTaskItera : lstTaskReceived ) {
                objCall.Id                          = objTaskItera.WhatId;
                objCall.ONCALL__Date__c             = System.now();
                objCall.ONCALL__Call_Status__c      = objTaskItera.Status;
                objCall.ISSM_CallEffectiveness__c   = objTaskItera.ISSM_CallEffectiveness__c;
                objCall.ISSM_NumberCallsMade__c     = NumberCallsMade + 1;
                objCall.ISSM_PaymentPromise__c      = objTaskItera.ISSM_PaymentPromise__c;  
                objCall.ISSM_PaymentPromiseDate__c  = objTaskItera.ISSM_PaymentPromiseDate__c;
                objCall.ISSM_PaymentPlanRequest__c  = objTaskItera.ISSM_PaymentPlanRequest__c;
                objCall.ISSM_PaymentPlanMaxDate__c  = objTaskItera.ISSM_PaymentPlanMaxDate__c;
                objCall.ISSM_ClarificationRequest__c= objTaskItera.ISSM_ClarificationRequest__c;
                objCall.ISSM_ClarificationType__c   = objTaskItera.ISSM_ClarificationType__c;
                lstCall.add(objCall);
                objCall = new ONCALL__Call__c();
            }
            if(lstCall != null && !lstCall.IsEmpty()) {
                update lstCall;
                System.debug('####lstCall : '+lstCall);
            }
        /*} catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }*/
    }
    
      public static void UpdateCallNotEffective(List <Task> lstTaskReceived,Decimal NumberCallsMade) {
        ONCALL__Call__c objCall = new ONCALL__Call__c();
        List<ONCALL__Call__c> lstCall = new List<ONCALL__Call__c>();
        
        for(Task objTaskItera : lstTaskReceived ) {
            objCall.Id                          = objTaskItera.WhatId;
            objCall.ONCALL__Date__c             = System.now();
            objCall.ONCALL__Call_Status__c      = objTaskItera.Status;
            objCall.ISSM_CallEffectiveness__c   = objTaskItera.ISSM_CallEffectiveness__c;
            objCall.ISSM_NumberCallsMade__c     = NumberCallsMade + 1;
            lstCall.add(objCall);
            objCall = new ONCALL__Call__c ();
        }
        if(lstCall != null && !lstCall.IsEmpty()){
            update lstCall;
        }
    }
    
    public static void UpdateCallBackQueue(List <Task> lstTaskReceived,String IdCall) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        ONCALL__Call__c objCall = new ONCALL__Call__c();
        List<ONCALL__Call__c> lstCall = new List<ONCALL__Call__c>();
        ISSM_AppSetting_cs__c oIdQueueWithoutOwner = new  ISSM_AppSetting_cs__c ();
        ONCALL__Call__c QueryCallOrder;
        
        oIdQueueWithoutOwner =  objCSQuerys.QueryAppSettingsObj();
        System.debug('oIdQueueWithoutOwner  : ' + oIdQueueWithoutOwner);
        QueryCallOrder = objCSQuerys.QueryOncallCallOrderBy(IdCall);
        
        // System.debug('#####QueryCallOrder  : '+QueryCallOrder );    
        //String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);                   
        if(QueryCallOrder != null){
            for(Task objTaskItera : lstTaskReceived ){
                objCall.Id  = objTaskItera.WhatId;
                objCall.ONCALL__Date__c = System.now();
                objCall.OwnerId = oIdQueueWithoutOwner.ISSM_IdQueueTelecolletion__c;
                //objCall.ISSM_Order__c = QueryCallOrder.ISSM_Order__c + 1;
                lstCall.add(objCall);
                objCall = new ONCALL__Call__c ();
            } 
        }       
        if(lstCall != null && !lstCall.IsEmpty()){
            System.debug('lstCall : '+lstCall);
            update lstCall;
        }
    }
}