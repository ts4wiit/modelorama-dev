global class ISSM_CAM_UpdateAccountBlacklist_sch  implements Schedulable{
    global void execute(SchedulableContext sc) {
        	ISSM_CAM_UpdateAccountBlacklist_bch bch = new ISSM_CAM_UpdateAccountBlacklist_bch();
        	Database.executeBatch(bch);    
    }
}