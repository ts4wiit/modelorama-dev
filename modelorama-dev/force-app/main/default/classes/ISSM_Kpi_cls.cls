/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to sincronice kpi

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_Kpi_cls implements ISSM_ObjectInterface_cls{

    ONTAP__KPI__c accountAsset;
    public List<ONTAP__KPI__c> kpis;
    private List<salesforce_ontap_kpi_c__x> kpisDelete;

    /**
     * Class constructor                                     
     * @param  List<ISSM_ObjectInterface_cls> sObjectInterfaceListParam List of objects to sincronice
     * @param  Map<String, Account> mapAccountParam
     */
    public ISSM_Kpi_cls(List<salesforce_ontap_kpi_c__x> kpiExtParam, Map<String, Account> mapAccountParam) {
        kpis = getList(kpiExtParam, mapAccountParam);
    }

    /**
     * Class constructor                                     
     */
    public ISSM_Kpi_cls() {
    }

    /**
     * Create Objects to sincronice                                  
     * Map<String, Account> mapAccount map of the accounts to sincronice records
     */
    public void createObject(Map<String, Account> mapAccount){
        kpis = getList(null, mapAccount);
    }

    /**
     * Create objects to delete                                  
     * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
     */
    public void createObjectDelete(Map<String, Account> mapAccount){
        kpisDelete = ISSM_KpiExtern_cls.getList(mapAccount.keySet(), true);
    }

    /**
     * Get a list of the new records result of the sincronization                                    
     * @param  List<sObject> accountAssetsExt list of external objects
     * @param  Map<String, Account> mapAccount map of the accounts to sincronice records
     */
    public List<sObject> getList(List<sObject> kpiExt, Map<String, Account> mapAccount){
        List<ONTAP__KPI__c> kpisReturn;
        ONTAP__KPI__c kpi;
        Set<String> accountSap =mapAccount.keySet() ;
        kpisReturn = new List<ONTAP__KPI__c>();
        if(test.isRunningTest()){
            Decimal j = 0;
            for(Integer i = 0;i<15;i++){
                salesforce_ontap_kpi_c__x accountKpiExt = new salesforce_ontap_kpi_c__x(
                    ontap_kpi_id_c__c = j+'',
                    isdeleted__c=false, 
                    issm_sapcustomerid_c__c ='000000000'+i);
                kpisReturn.add(getKPI(mapAccount , accountKpiExt, null));
                j++;
            }
        } else{
            for(SObject accountKpiExt: [SELECT issm_sapcustomerid_c__c,issm_uniqueid_c__c ,ontap_actual_c__c,ontap_categorie_c__c,
                ontap_days_passed_in_period_c__c,ontap_kpi_end_date_c__c,ontap_kpi_id_c__c, 
                ontap_kpi_name_c__c,ontap_kpi_parent_id_c__c,ontap_kpi_start_date_c__c,ontap_percentage_increase_lastyear_c__c, 
                ontap_target_c__c, ontap_total_days_in_period_c__c, ontap_unit_of_measure_c__c, systemmodstamp__c FROM salesforce_ontap_kpi_c__x
                where issm_sapcustomerid_c__c in :accountSap and systemmodstamp__c >= LAST_MONTH]){ //and systemmodstamp__c >= YESTERDAY WPA retirado para prueba KPI sin fecha
                kpisReturn.add(getKPI(mapAccount , accountKpiExt, null));
            }
        }
        return kpisReturn;
    }


    /**
     * Get asset record                                  
     * @param  Map<String, Account> mapAccount
     * @param  sObject accountAssetExt
     * @param  Id devRecordTypeId
     */
    private ONTAP__KPI__c getKPI(Map<String, Account> mapAccount , sObject accountKpiExt, Id devRecordTypeId){
        ONTAP__KPI__c kpi;
        kpi = new ONTAP__KPI__c();
        try{
            kpi.ONTAP__Account_id__c = mapAccount.get(((salesforce_ontap_kpi_c__x)accountKpiExt).issm_sapcustomerid_c__c).Id;
            kpi.ONTAP__Actual__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_actual_c__c;
            kpi.ONTAP__Categorie__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_categorie_c__c;
            kpi.ONTAP__Days_passed_in_period__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_days_passed_in_period_c__c;
            kpi.ONTAP__Kpi_end_date__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_end_date_c__c;
            if(((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_id_c__c != null && ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_id_c__c != ''){
                kpi.ONTAP__Kpi_id__c =  Decimal.valueOf(((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_id_c__c) ;
            }
            
            kpi.ONTAP__KPI_name__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_name_c__c;
            if(((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_parent_id_c__c != null && ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_parent_id_c__c != ''){
                kpi.ONTAP__Kpi_parent_id__c =  Decimal.valueOf(((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_parent_id_c__c) ;
            }
            
            kpi.ONTAP__Kpi_start_date__c =((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_kpi_start_date_c__c;
            kpi.ONTAP__Percentage_increase_lastyear__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_percentage_increase_lastyear_c__c;
            kpi.ONTAP__Target__c =((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_target_c__c;
            kpi.ONTAP__Total_days_in_period__c = ((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_total_days_in_period_c__c;
            kpi.ONTAP__Unit_of_Measure__c =((salesforce_ontap_kpi_c__x)accountKpiExt).ontap_unit_of_measure_c__c;
            kpi.ISSM_UniqueId__c =((salesforce_ontap_kpi_c__x)accountKpiExt).issm_uniqueid_c__c;
        } catch(NullPointerException ne){
            system.debug( '\n\n  ****** ne = ' + ne+'\n\n' );
        }
        return kpi;
    }


    /**
     * Save the result of the sincronization process                                     
     */
    public void save(){
        if(kpis != null){
            try {
                upsert kpis ISSM_UniqueId__c;
            } catch (DmlException e) {
                System.debug(e.getMessage());
            }

        }
    }

    /**
     * Delete internal records that has the flag isdeleted on true                                   
     */
    public Boolean deleteObjectsFinish(){
        if(kpisDelete != null){
            Set<String> kpisSet = ISSM_KpiExtern_cls.getSetExternalId(kpisDelete);
            List<ONTAP__KPI__c> toDeleteKpi_lst = getKPIS(kpisSet);
            Boolean success = true;
            try{
                System.debug('### ISSM_Asset_cls: toDeleteKpi_lst size ('+toDeleteKpi_lst.size()+')'); 
                Database.DeleteResult[] drList = Database.delete(toDeleteKpi_lst);
                for(Database.DeleteResult dr : drList) {
                    success &= dr.isSuccess();
                }
                return success;
            } catch(Exception e){
                System.debug('### ISSM_Asset_cls: removeExistingAssets (DELETE FAILED)'); 
                return false;
            }
        }
        return false;
    }

    /**
     * Get list of kpis based on ONTAP__Kpi_id__c                                    
     * @param  Set<String> assetsSet
     */
    private List<ONTAP__KPI__c> getKPIS(Set<String> kpisSet){
        Set<Decimal> kpisIdSet = new Set<Decimal>();
        for (String kpiId : kpisSet) {
            System.debug(kpiId);
            kpisIdSet.add(Decimal.valueOf(kpiId));
        }

        return [SELECT Id FROM ONTAP__KPI__c WHERE ONTAP__Kpi_id__c IN : kpisIdSet ];
    }

    /**
     * Delete external records that has the flag isdeleted on true                                   
     */
    public void deleteObjectsFinishExt(){

        if(kpisDelete != null){
            try{
                Database.DeleteResult[] drListExt = Database.deleteAsync(kpisDelete);
                system.debug( '\n\n  ****** drListExt = ' + drListExt +'\n\n' );
            } catch(Exception e){
                System.debug('### ISSM_Asset_cls: removeExistingAssets (DELETE FAILED)'); 
            }
        }

    }

}