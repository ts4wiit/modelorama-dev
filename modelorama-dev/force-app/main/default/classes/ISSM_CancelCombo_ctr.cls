/******************************************************************************* 
* Desarrollado por	: 	Avanxo México
* Autor				: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Controlador de ISSM_CancelModal_lcp, para la extracción de los registros de combos con estatus 
*
* No.       Fecha              Autor                      Descripción
* 1.0    10-Mayo-2018      Oscar Alvarez                   Creación
*******************************************************************************/
public class ISSM_CancelCombo_ctr {
    @AuraEnabled
    public static void cancelCombos(String strCombos){
        
        List<ISSM_Combos__c> lstCombos = new List<ISSM_Combos__c>();
        Set<Id> stIdCombos = new Set<Id>(); 
        Set<String> stMotivoCancelled = new Set<String>(); 
        map<Id,String> mapCombos = new map<Id,String>();
        
        List<ComboXMotivo> lstComboXMotivo = (List<ComboXMotivo>) System.JSON.deserialize(strCombos, List<ComboXMotivo>.class);
        
        
        for(ComboXMotivo comboXMotivo : lstComboXMotivo){
            stIdCombos.add((Id)comboXMotivo.id);
            mapCombos.put((Id)comboXMotivo.id,comboXMotivo.motivo);        
        }
        
        //Obtener los combos que fueron aprobados para su cancelación y cambier el estatus a cancelado 
        //List <ISSM_CancelCombo_rest.WpprCancelCombo> lstWpprCancelCombo = new List <ISSM_CancelCombo_rest.WpprCancelCombo>();
        List<ISSM_Combos__c> newLstCombos = [SELECT Id,ISSM_StatusCombo__c,ISSM_CancellationsReasons__c,ISSM_SynchronizedWithSAP__c FROM ISSM_Combos__c WHERE Id IN : stIdCombos];
        for (ISSM_Combos__c combo : newLstCombos){
            for(Id id : stIdCombos){
                if (id == combo.Id){
                    combo.ISSM_StatusCombo__c 					= 'ISSM_Cancelled';
                    combo.ISSM_CancellationsReasons__c 			= mapCombos.get(id);
                    combo.ISSM_SynchronizedWithSAP__c			= false;
                    combo.ISSM_SynchronizedDescribeCodeSAP__c	= '';
                    combo.ISSM_SynchronizedCodeSAP__c			= '';
                }
            }
        }
        update newLstCombos;
    }
    
    public static String getSelectOptions(sObject objObject, String strFld) {
        System.debug('objObject --->' + objObject);
        System.debug('strFld --->' + strFld);
        List<PickListValues> lstPickListValues = new List<PickListValues>();
        Schema.sObjectType objType = objObject.getSObjectType();// Get the object type of the SObject.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();// Describe the SObject using its object type.
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();// Get a map of fields for the SObject
        
        //Valor por  dafault
        PickListValues pickListValueDefault = new PickListValues();
        pickListValueDefault.value = '';
        pickListValueDefault.label = Label.TRM_Select;
        lstPickListValues.add(pickListValueDefault);
        // Get the list of picklist values for this field.
        Schema.PicklistEntry[] values = fieldMap.get(strFld).getDescribe().getPickListValues();
        // Add these values to the selectoption list.
        for(Schema.PicklistEntry a: values) {
            PickListValues pickListValues = new PickListValues();
            pickListValues.value = a.getValue();
            pickListValues.label = a.getLabel();
            lstPickListValues.add(pickListValues);
        }
        
        String JsonMsg	= JSON.serialize(lstPickListValues);
        System.debug('JsonMsg ---->' + JsonMsg);
        return JsonMsg;
    }
    
    /*
    * Method Name	: getAccRecords
    * Purpose		: To get the wrapper of Columns and Headers
    * @Param         : strObjectName-> Nombre del objeto a extraer registros
    */
    @AuraEnabled
    public static DataTableResponse getComboRecords(String strObjectName){                
        
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        List<String> lstFieldsToQuery = new List<String>();
        List<String> lstFieldDefault = new List<String>{'Name',
                                                        'ISSM_StatusCombo__c',
                                                        'ISSM_ComboType__c',
                                                        'ISSM_SalesStructure__c',
                                                        'ISSM_TypeApplication__c',
                                                        'ISSM_StartDate__c',
                                                        'ISSM_EndDate__c',
                                                        'ISSM_NumberSalesOffice__c',
                                                        'ISSM_NumberByCustomer__c'};
       	DataTableResponse response = new DataTableResponse();
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(strObjectName).getDescribe().fields.getMap();
        
        for( Schema.SObjectField sfield : fieldMap.Values() ){
            schema.describefieldresult dfield = sfield.getDescribe();
            String dataType = String.valueOf(dfield.getType()).toLowerCase();
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            if (lstFieldDefault.contains(String.valueOf(dfield.getName()))){
                //Create a wrapper instance and store label, fieldname and type.
                DataTableColumns datacolumns = new DataTableColumns( String.valueOf(dfield.getLabel()) , 
                                                                    String.valueOf(dfield.getName()), 
                                                                    String.valueOf(dfield.getType()).toLowerCase() == 'datetime' ? 'date' : String.valueOf(dfield.getType()).toLowerCase());
                lstDataColumns.add(datacolumns);
                lstFieldsToQuery.add(String.valueOf(dfield.getName())); 
            }
        }
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(!lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',');
            query 		+= ' FROM '+strObjectName;
            //query 		+=' WHERE ISSM_StatusCombo__c = \'ISSM_Open\'';	
            query 		+=' WHERE ISSM_SynchronizedWithSAP__c = true AND ISSM_StatusCombo__c = \'ISSM_Actived\'';	
            System.debug(query);
            response.lstDataTableData = Database.query(query);
            
        }
        return response;
    }
        
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled public List<sObject> lstDataTableData {get;set;} 
        @AuraEnabled public String motivos {get;set;} 
        @AuraEnabled public boolean selectedRow {get;set;} 
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
            this.motivos = getSelectOptions(new ISSM_Combos__c(), 'ISSM_CancellationsReasons__c');
            this.selectedRow = false;
        }
    }
    public class ComboXMotivo{
        public String id;
        public String motivo;
        
    }
    public class PickListValues{
        public String value;
        public String label;
        
    }
}