global with sharing class CDM_ExecuteUpdateRshps_ws {
     @AuraEnabled
    WebService static string ExecuteProcessRules(){
        CDM_UpdateRelaionships_bch b = new CDM_UpdateRelaionships_bch();
        ID batchprocessid = Database.executeBatch(b, 200); 
        return batchprocessid;
   } 

}