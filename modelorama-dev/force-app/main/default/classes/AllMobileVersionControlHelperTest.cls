/**
 * Test class for AllMobileVersionControlClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileVersionControlHelperTest {

	/**
	 * Test method to setup data.
	 */
	static testMethod void setup() {

		//Get Record Types.
		RecordType objRecordTypeAccountSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOffice');
		RecordType objRecordTypeAccountSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('SalesOrg');
		RecordType objRecordTypeVersionControlRoute = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileRoute');
		RecordType objRecordTypeVersionControlSalesOffice = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileSalesOffice');
		RecordType objRecordTypeVersionControlSalesOrg = AllMobileUtilityHelperTest.getRecordTypeObject('AllMobileSalesOrg');
		RecordType objRecordTypeOnTapRouteSales = AllMobileUtilityHelperTest.getRecordTypeObject('Sales');

		//Creating a Route... BEGINS.
		//Creating a User... BEGINS.

		//Get Profile: 'System Administrator'.
 		Profile objProfileSystemAdministrator = AllMobileUtilityHelperTest.getProfileObject('System Administrator');

		//Create a User.
		User objUserAgent = AllMobileUtilityHelperTest.createUserObject(objProfileSystemAdministrator, 'albertogomez@abinbev.org.dev', 'agome', 'alberto@gmail.com', 'ISO-8859-1', 'Gomez', 'en_US', 'es_MX', 'America/Mexico_City');
		insert objUserAgent;
		//Creating a User...ENDS

		//Create a Sales Org.
		Account objSalesOrg = AllMobileUtilityHelperTest.createSalesOrgObject(objRecordTypeAccountSalesOrg, 'Org1');
		insert objSalesOrg;

		//Create a Sales Office.
		Account objSalesOffice = AllMobileUtilityHelperTest.createSalesOfficeObject(objRecordTypeAccountSalesOffice, 'Office1Org1', 'FG00', objSalesOrg);
		insert objSalesOffice;

		//Create a Vehicle assigned to the office.
		ONTAP__Vehicle__c objOnTapVehicle = AllMobileUtilityHelperTest.createOnTapVehicle('Vehicle1', objSalesOffice, 'Vehicle-1234', 'AXNF2837', false, 956454, '654345654');
		insert objOnTapVehicle;

		//Creating an Application Preventa.
		AllMobileApplication__c objAllMobileApplication = AllMobileUtilityHelperTest.createAllMobileApplicationObject('PREVENTA', '1');
		insert objAllMobileApplication;

		//Creating a Version Preventa.
		AllMobileVersion__c objAllMobileVersion = AllMobileUtilityHelperTest.createAllMobileVersionObject('1.0.0', objAllMobileApplication);
		insert objAllMobileVersion;

		//Create a Route.
		ONTAP__Route__c objOnTapRoute = AllMobileUtilityHelperTest.createOntapRouteObject(objRecordTypeOnTapRouteSales, objUserAgent, 'Presales', 'FG0001', 'Ruta centro', objSalesOffice, objOnTapVehicle);
		insert objOnTapRoute;
		//Creating a Route... ENDS

		//Creating a VersionControl RecordType: Route.
		AllMobileVersionControl__c objAllMobileVersionControlRoute = AllMobileUtilityHelperTest.createVersionControlObject(Date.newInstance(2030, 06, 01), Date.newInstance(2099, 06, 30), FALSE, objRecordTypeVersionControlRoute, objAllMobileApplication, objAllMobileVersion, objOnTapRoute, objSalesOffice, objSalesOrg);
		insert objAllMobileVersionControlRoute;

		//Creating a VersionControl RecordType: Office.
		AllMobileVersionControl__c objAllMobileVersionControlSalesOrg = AllMobileUtilityHelperTest.createVersionControlObject(Date.newInstance(2030, 06, 01), Date.newInstance(2099, 06, 30), FALSE, objRecordTypeVersionControlSalesOrg, objAllMobileApplication, objAllMobileVersion, objOnTapRoute, objSalesOffice, objSalesOrg);
		insert objAllMobileVersionControlSalesOrg;

		//Creating a VersionControl RecordType: Org.
		AllMobileVersionControl__c objAllMobileVersionControlSalesOffice = AllMobileUtilityHelperTest.createVersionControlObject(Date.newInstance(2030, 06, 01), Date.newInstance(2099, 06, 30), FALSE, objRecordTypeVersionControlSalesOffice, objAllMobileApplication, objAllMobileVersion, objOnTapRoute, objSalesOffice, objSalesOrg);
		insert objAllMobileVersionControlSalesOffice;
	}
}