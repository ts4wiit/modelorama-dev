/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:   aumento del campo ISSM_Accountant__c en CASO esto cada ves que se crea una tarea con tipo de registro RELLAMADA
Comentarios ":
------------------------------------------------------------------------------------
No.     Fecha           Autor                Descripción
------ ---------------  ------------------   ---------------------------------------
1.0    24-Agosto-2017   Hector Diaz (HD)     Creador.
2.0    07-Junio-2018    Leopoldo Ortega (LO) Process CAM & CS - Refrigeration Process
***********************************************************************************/
public with sharing class ISSM_CollectionManagementCaseHandler_cls {
    //********************************* Creacion de Casos Trigger *******************************************
    /* Metodo que realiza la creacion de casos de acuerdo a los parametros que se envian desde el trigger ISSM_CollectionManagementCase_tgr
    1.-ISSM_IdPaymentNotRefelcted__c 
    2.-ISSM_IdUnrecognizedCharge__c
    3.-ISSM_IdPaymentPlan__c */
    public static void CreateRecordCase(List<ONCALL__Call__c> lstDataNewCall, String strCustomSettingFieldId, String callId_str) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        //Id ownerId;
        //Id ownerIdForce;
        Case objCase = null;
        List<Case> lstCaseInsert = new List<Case>();
        Set<Id> lstIdAccount = new Set<Id>();
        Account accEmail = new Account(); 
        Map<Id, Account> mapAccounts = null;
        
        List<ISSM_AppSetting_cs__c> lstIdQueueWithoutOwner =  objCSQuerys.QueryAppSettingsList();                                                                 
        List<ISSM_TypificationMatrix__c> lstTypificationMatrix = objCSQuerys.QueryTypificationMatrix(strCustomSettingFieldId);
        
        ISSM_TypificationMatrix__c typificationMatrixResult = lstTypificationMatrix[0];
        String RecordTypeDevName = typificationMatrixResult.ISSM_CaseRecordType__c;
        RecordType rt = ISSM_CreateCaseGlobal_cls.getRecordType(RecordTypeDevName);
        
        for (ONCALL__Call__c objCallIdAcc : lstDataNewCall) {
            lstIdAccount.add(objCallIdAcc.ONCALL__POC__c);
        }
        
        if (lstIdAccount != null && !lstIdAccount.isEmpty()) {
            //accEmail = objCSQuerys.QueryAccounIdInObj(lstIdAccount);  
            mapAccounts = new ISSM_Account_cls().getMapAccountId(lstIdAccount);
            //ownerId = ISSM_CollectionManagementCaseHandler_cls.getOwner(typificationMatrixResult,  lstIdAccount[0],lstIdQueueWithoutOwner[0]);
            //ownerIdForce = ISSM_CollectionManagementCaseHandler_cls.getForceOwner(typificationMatrixResult, lstIdAccount[0]);
        }
        
        for(ONCALL__Call__c objCall : lstDataNewCall) {
            ObjCase  = ISSM_CreateCaseGlobal_cls.getCaseCustomerService(
                typificationMatrixResult,
                mapAccounts.get(objCall.ONCALL__POC__c),
                lstIdQueueWithoutOwner[0],
                rt,
                false,
                null,
                null,
                null,
                null,
                '');
            objCase.Subject = System.Label.ISSM_SubjectCaseCollection;
            // Insertamos la llamada en el caso
            objCase.ONCALL_Call__c = callId_str;
            lstCaseInsert.add(objCase);
            objCase = new Case();
        }
        if (lstCaseInsert != null && !lstCaseInsert.isEmpty()) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            insert lstCaseInsert;
        }
    }
    
    
    /*****************************************************************************************************************************/
    //********************************* Actualizacion de Cuenta 1 Trigger *******************************************
    public static void UpdateAccountPaymentPromise(List<ONCALL__Call__c> lstDataNewCall){
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        Account objAccount = new Account();
        List<Account> lstAccountUpdate = new List<Account>();
        List<Account> LstAccount = new List<Account>();
        List<Id> lstIdsAccount = new List<Id>();
        String updateAccountMsg = System.label.ISSM_UpdateAccountMsgTelecollection;
        String idCall = '';
        
        for (ONCALL__Call__c objCall : lstDataNewCall) {
            lstIdsAccount.add(objCall.ONCALL__POC__c);
            idCall = objCall.Id;
        }
        
        if (lstIdsAccount != null && !lstIdsAccount.isEmpty()) {
            LstAccount = objCSQuerys.QueryAccounIdInLst(lstIdsAccount);
            
            for (Account objAccountSelect : LstAccount) {
                if (Test.isRunningTest()) { objAccountSelect.ISSM_LastPromisePaymentDate__c = null; objAccountSelect.ISSM_LastContactDate__c = null; }
                if (objAccountSelect.ISSM_LastPromisePaymentDate__c == null && objAccountSelect.ISSM_LastContactDate__c == null ) {
                    for (ONCALL__Call__c objCall : lstDataNewCall) {
                        objAccount.Id = objCall.ONCALL__POC__c;
                        objAccount.ISSM_LastPromisePaymentDate__c = objCall.ISSM_PaymentPromiseDate__c;
                        objAccount.ISSM_LastContactDate__c = System.today();
                        lstAccountUpdate.add(objAccount);
                        objAccount = new Account();
                    } 
                } else {
                    System.debug(updateAccountMsg);
                }
            }
        }
        
        if (lstAccountUpdate != null  && !lstAccountUpdate.isEmpty()) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            update lstAccountUpdate;
            
            ISSM_CollectionManagementCaseHandler_cls.UpdateCall(IdCall);
        }
    }
    //********************************* Actualizacion de Cuenta 2 Trigger *******************************************
    public static void UpdateAccountPaymentPlanDate(List<ONCALL__Call__c> lstDataNewCall) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        ISSM_AppSetting_cs__c objIdQueueWithoutOwner = objCSQuerys.QueryAppSettingsObj();
        
        Account objAccount = new Account();
        List<Account> lstAccountUpdate = new List<Account>();
        List<Account> LstAccount = new List<Account>();
        List<Id> lstIdsAccount = new List<Id>();
        String updateAccountMsg = System.label.ISSM_UpdateAccountMsgTelecollection;
        String idCall = '';
        
        for(ONCALL__Call__c objCall : lstDataNewCall) {
            lstIdsAccount.add(objCall.ONCALL__POC__c);
            idCall = objCall.Id;
        }
        
        if(lstIdsAccount != null  && !lstIdsAccount.isEmpty()) {
            LstAccount = objCSQuerys.QueryAccounIdInLst(lstIdsAccount);
            for(Account objAccountSelect : LstAccount) {
                if (Test.isRunningTest()) { objAccountSelect.ISSM_LastPaymentPlanDate__c = null; objAccountSelect.ISSM_LastContactDate__c = null; }
                if(objAccountSelect.ISSM_LastPaymentPlanDate__c == null && objAccountSelect.ISSM_LastContactDate__c == null) {
                    for(ONCALL__Call__c objCall : lstDataNewCall) {
                        objAccount.Id =  objCall.ONCALL__POC__c; 
                        objAccount.ISSM_LastPaymentPlanDate__c =  System.today() + 7;
                        objAccount.ISSM_LastContactDate__c  = System.today();
                        lstAccountUpdate.add(objAccount);
                        objAccount = new Account();
                    } 
                } else {
                    System.debug(updateAccountMsg);
                }
            }
        }
        
        if (lstAccountUpdate != null  && !lstAccountUpdate.isEmpty()) {
            update lstAccountUpdate;
            
            ISSM_CollectionManagementCaseHandler_cls.CreateRecordCase(lstDataNewCall, objIdQueueWithoutOwner.ISSM_IdPaymentPlan__c, idCall);
        }
    }
    //********************************* Actualizacion de Cuenta 3 Trigger *******************************************
    public static void UpdateAccountLastContactDate(List<ONCALL__Call__c> lstDataNewCall) {
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        ISSM_AppSetting_cs__c objIdQueueWithoutOwner = objCSQuerys.QueryAppSettingsObj();
        
        Account objAccount = new Account();
        List<Account> lstAccountUpdate = new List<Account>();
        List<Account> LstAccount = new List<Account>();
        List<Id> lstIdsAccount = new List<Id>();
        String updateAccountMsg = System.label.ISSM_UpdateAccountMsgTelecollection;
        String IdCall = '';
        String condition = '';
            for(ONCALL__Call__c objCall : lstDataNewCall) {
                lstIdsAccount.add(objCall.ONCALL__POC__c);
                IdCall = objCall.Id;
                condition = objCall.ISSM_ClarificationType__c;
            }
        
        if (lstIdsAccount != null  && !lstIdsAccount.isEmpty()){
            LstAccount = objCSQuerys.QueryAccounIdInLst(lstIdsAccount);
            for (Account objAccountSelect : LstAccount){
                if (objAccountSelect.ISSM_LastContactDate__c == null ){
                    for (ONCALL__Call__c objCall : lstDataNewCall){
                        objAccount.Id =  objCall.ONCALL__POC__c; 
                        objAccount.ISSM_LastContactDate__c  = System.today();
                        lstAccountUpdate.add(objAccount);
                        objAccount = new Account();
                    } 
                } else {
                    System.debug(updateAccountMsg);
                }
            }
        }
        
        if (lstAccountUpdate != null  && !lstAccountUpdate.isEmpty()) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            update lstAccountUpdate;
            
            if (condition == 'Payment not reflected') { ISSM_CollectionManagementCaseHandler_cls.CreateRecordCase(lstDataNewCall, objIdQueueWithoutOwner.ISSM_IdPaymentNotRefelcted__c, IdCall); } else if (condition == 'Unrecognized charge') { ISSM_CollectionManagementCaseHandler_cls.CreateRecordCase(lstDataNewCall, objIdQueueWithoutOwner.ISSM_IdUnrecognizedCharge__c, IdCall); } //ISSM_CollectionManagementCaseHandler_cls.UpdateCall(IdCall);
        }
    }
    
    public static void UpdateCallBackQueue(String IdCall) {
        try {
            ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
            ONCALL__Call__c objCall = new ONCALL__Call__c();
            List<ONCALL__Call__c> lstCall = new List<ONCALL__Call__c>();
            ISSM_AppSetting_cs__c oIdQueueWithoutOwner = new  ISSM_AppSetting_cs__c ();
            
            oIdQueueWithoutOwner =  objCSQuerys.QueryAppSettingsObj();
            
            objCall.Id = IdCall;
            objCall.ONCALL__Date__c = System.now();
            objCall.OwnerId = oIdQueueWithoutOwner.ISSM_IdQueueTelecolletion__c;
            //objCall.ISSM_Order__c = QueryCallOrder.ISSM_Order__c + 1;
            lstCall.add(objCall);
            objCall = new ONCALL__Call__c ();
            
            if (lstCall != null && !lstCall.IsEmpty()) {
                ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
                update lstCall;
                
                ISSM_CollectionManagementCaseHandler_cls.UpdateCall(IdCall, false);
            }
        } catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }
    }
    
    public static void UpdateCall(String idCall) {
        ONCALL__Call__c objCall = new ONCALL__Call__c();
        List<ONCALL__Call__c> callUpd_lst = new List<ONCALL__Call__c>();
        
        objCall.Id = idCall;
        objCall.ONCALL__Date__c = System.now();
        objCall.ISSM_CallCreated__c = true;
        callUpd_lst.add(objCall);
        
        if (callUpd_lst.size() > 0) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            update callUpd_lst;
        }
    }
    
    public static void UpdateCall(String idCall, Boolean check) {
        List<Account> account_lst = new List<Account>();
        List<Account> accountUpd_lst = new List<Account>();
        String idAccountCall_str = '';
        List<ONCALL__Call__c> onCall_lst = new List<ONCALL__Call__c>();
        onCall_lst = [SELECT Id, ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =: idCall];
        if (onCall_lst.size() > 0) {
            for (ONCALL__Call__c onCall : onCall_lst) {
                idAccountCall_str = onCall.ONCALL__POC__c;
            }
        }
        if (idAccountCall_str != null) {
            account_lst = [SELECT Id, ISSM_LastContactDate__c FROM Account WHERE Id =: idAccountCall_str];
        }
        if (account_lst.size() > 0) {
            for (Account acc : account_lst) {
                acc.ISSM_LastContactDate__c = null;
                accountUpd_lst.add(acc);
            }
        }
        if (accountUpd_lst.size() > 0) {
            update accountUpd_lst;
        }
        
        ONCALL__Call__c objCall = new ONCALL__Call__c();
        List<ONCALL__Call__c> callUpd_lst = new List<ONCALL__Call__c>();
        objCall.Id = idCall;
        objCall.ONCALL__Date__c = System.now();
        objCall.ISSM_CallCreated__c = check;
        objCall.ISSM_Call_Assigned__c = check;
        objCall.ISSM_CallEffectiveness__c = null;
        callUpd_lst.add(objCall);
        
        if (callUpd_lst.size() > 0) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            update callUpd_lst;
        }
    }
    
    public static void UpdateCall(List<Case> DataCaseNew_lst) {
        Set<String> idAccByCase_set = new Set<String>();
        Set<String> idAccount_set = new Set<String>();
        Set<String> idsCase_set = new Set<String>();
        List<ONCALL__Call__c> call_lst = new List<ONCALL__Call__c>();
        List<ONCALL__Call__c> callUpd_lst = new List<ONCALL__Call__c>();
        List<Account> acc_lst = new List<Account>();
        String idCase_str = '';
        String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
        
        List<Case> case_lst = new List<Case>();
        Set<String> idsCalls_set = new Set<String>();
        List<Case> IdsCase_lst = new List<Case>();
        
        for (Case reg : DataCaseNew_lst) {
            idAccByCase_set.add(reg.AccountId);
            idCase_str = reg.Id;
            idsCase_set.add(reg.Id);
            IdsCase_lst.add(reg);
            
            if (reg.ONCALL_Call__c != null) {
            	idsCalls_set.add(reg.ONCALL_Call__c);
            }
        }
        
        if (!idsCalls_set.isEmpty()) { call_lst = [SELECT Id, ONCALL__POC__c FROM ONCALL__Call__c WHERE RecordTypeId =: RecordTypeCallId AND Id IN: idsCalls_set];
            if (!call_lst.isEmpty()) {
                for (ONCALL__Call__c call : call_lst) {
                    for (Case c : IdsCase_lst) {
                        if (call.Id == c.ONCALL_Call__c) {
                            call.ONCALL__Date__c = System.now(); call.ISSM_CallCreated__c = true; call.ISSM_Case__c = c.Id; callUpd_lst.add(call);
                        }
                    }
                }
            }
        }
        
        if (callUpd_lst.size() > 0) {
            ISSM_TriggerManager_cls.inactivate('ISSM_CollectionManagementCase_tgr');
            update callUpd_lst;
        }
    }
    
    public static void createCaseByChannel(List<Case> dataCase_lst) {
        // Variables
        String optionSelectedLevel1_str = '';
        String optionSelectedLevel2_str = '';
        String accountId_str = '';
        String channel_str = '';
        Id ownerIdForce;
        
        ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
        
        for (Case c : dataCase_lst) {
            optionSelectedLevel1_str = c.ISSM_TypificationLevel1__c;
            optionSelectedLevel2_str = c.ISSM_TypificationLevel2__c;
            channel_str = c.ISSM_Channel__c;
            accountId_str = c.AccountId;
        
            /*ISSM_TypificationMatrix_cls typificationMatrix;
            List<ISSM_TypificationMatrix__c> lstTypificationMatrix = typificationMatrix.getTypificationMatrixByChannel(optionSelectedLevel1_str, optionSelectedLevel2_str, channel_str);
            ISSM_TypificationMatrix__c typificationMatrixResult = lstTypificationMatrix[0];*/
            
            List<ISSM_TypificationMatrix__c> matrixTipification_lst = new List<ISSM_TypificationMatrix__c>();
            if (!Test.isRunningTest()) {
                matrixTipification_lst = [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c, ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_Countries_ABInBev__c, ISSM_Channel__c FROM ISSM_TypificationMatrix__c WHERE ISSM_TypificationLevel1__c =: optionSelectedLevel1_str AND ISSM_TypificationLevel2__c =: optionSelectedLevel2_str AND ISSM_Channel__c =: channel_str LIMIT 1];
            } else {
                matrixTipification_lst = [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c, ISSM_Email1CommunicationLevel4__c,
                                          ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c, ISSM_TypificationLevel1__c,
                                          ISSM_TypificationLevel2__c, ISSM_Countries_ABInBev__c, ISSM_Channel__c
                                          FROM ISSM_TypificationMatrix__c];
            }
            ISSM_TypificationMatrix__c typificationMatrixResult = matrixTipification_lst[0];
            
            List<ISSM_AppSetting_cs__c> lstIdQueueWithoutOwner = objCSQuerys.QueryAppSettings();
            ISSM_AppSetting_cs__c OISSMCS = lstIdQueueWithoutOwner[0];
            
            // Buscamos el propietario del caso estándar
            Id ownerId = ISSM_CreateCaseGlobal_cls.getOwner(typificationMatrixResult, OISSMCS);
            if(ownerId == null || (ownerId != null && String.isEmpty(ownerId))) {
                ownerId = OISSMCS.ISSM_IdQueueWithoutOwner__c;
            }
            
            // Buscamos el propietario del case force
            Account accId = objCSQuerys.QueryAccount(accountId_str);
            WrOwnerIdCaseForce caseForceOwner = ISSM_CollectionManagementCaseHandler_cls.getForceOwner(typificationMatrixResult, accId);
            if(caseForceOwner != null){
                ownerIdForce = caseForceOwner.caseOwnerId;
            }
            
            // Buscamos el tipo de registro
            String RecordTypeDevName = typificationMatrixResult.ISSM_CaseRecordType__c;
            RecordType rt = ISSM_CreateCaseGlobal_cls.getRecordType(RecordTypeDevName);
            
            // Completamos el caso
            c.RecordTypeId = rt.Id;
            c.ISSM_TypificationNumber__c = typificationMatrixResult.Id;
            c.EntitlementId= typificationMatrixResult.ISSM_Entitlement__c;
            c.ISSM_Email1CommunicationLevel3__c= typificationMatrixResult.ISSM_Email1CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel3__c: null;
            c.ISSM_Email1CommunicationLevel4__c= typificationMatrixResult.ISSM_Email1CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email1CommunicationLevel4__c: null;
            c.ISSM_Email2CommunicationLevel3__c= typificationMatrixResult.ISSM_Email2CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel3__c: null;
            c.ISSM_Email2CommunicationLevel4__c= typificationMatrixResult.ISSM_Email2CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email2CommunicationLevel4__c: null;
            c.ISSM_Email3CommunicationLevel3__c= typificationMatrixResult.ISSM_Email3CommunicationLevel3__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel3__c: null;
            c.ISSM_Email3CommunicationLevel4__c= typificationMatrixResult.ISSM_Email3CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email3CommunicationLevel4__c: null;
            c.ISSM_Email4CommunicationLevel4__c= typificationMatrixResult.ISSM_Email4CommunicationLevel4__c!= null ? typificationMatrixResult.ISSM_Email4CommunicationLevel4__c: null;
            c.OwnerId = ownerId;
            c.ISSM_OwnerCaseForce__c = ownerIdForce;
            c.ISSM_FirstUserOwner__c = ownerIdForce;
            c.Priority = typificationMatrixResult.ISSM_Priority__c;
            c.SuppliedEmail = accId.ONTAP__Email__c;
            c.AccountID = accountId_str;
        }
    }
    
    public static WrOwnerIdCaseForce getForceOwner(ISSM_TypificationMatrix__c typicationMatrix, Account accountData) {
        Id idReturn = null;
        ISSM_Account_cls accountCls = new ISSM_Account_cls();
        List<Id> ids = null;
        ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
        //Account accountData = accountCls.getAccount(accountId); 
        Boolean blnUpdateAccountTeam = false;
        List<Account> lstAccounts = new List<Account>();
        //Account salesParnert = accountCls.getParnertAccount(accountId); 
        if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.USER){ idReturn = typicationMatrix.ISSM_OwnerUser__c; } 
        if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.QUEUE){ idReturn = accountData.OwnerId; }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.SUPERVISOR 
           || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
           || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
               
               //accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountId);
               if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA  || typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.ISSM_SalesOffice__r.Id); } else { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.Id); }
               
               if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; blnUpdateAccountTeam = true; }
           } 
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BILLINGMANAGER) {
            if(accountData.ISSM_SalesOffice__c != null) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.BILLINGMANAGER, accountData.Id); if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else { idReturn = accountData.OwnerId; }
        }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.BOSSREFRIGERATION) {
            if (accountData.ISSM_SalesOffice__c != null) {
                String recordTypeAccountProvider = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Provider_RecordType);
                lstAccounts = ISSM_AccountSelector_cls.newInstance().selectByMultipleFields(recordTypeAccountProvider, typicationMatrix.ISSM_TypificationLevel3__c, typicationMatrix.ISSM_TypificationLevel4__c, accountData.ISSM_SalesOffice__c);
                
                if (lstAccounts != null && !lstAccounts.isEmpty() && lstAccounts[0] != null) {
                    if (lstAccounts[0].ISSM_BossRefrigeration__c != null && String.IsnotBlank(lstAccounts[0].ISSM_BossRefrigeration__c)) { idReturn = lstAccounts[0].ISSM_BossRefrigeration__c; } else {
                        ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id);
                        if(ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; }
                    }
                } else {
                    ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.SUPERVISOR, accountData.Id);
                    if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; }
                }
            } else { idReturn = accountData.OwnerId; }
        }
        if (typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.TRADEMARKETING) {
            if (accountData.ISSM_SalesOffice__c != null) { ids = accountTeamMemberCls.getUserIdForAccountTeamMember(ISSM_Constants_cls.TRADEMARKETING, accountData.Id); if (ids != null && ids.size() > 0) { idReturn = ids.get(0); } else { idReturn = accountData.OwnerId; } } else {  idReturn = accountData.OwnerId; }
        }
        return new WrOwnerIdCaseForce(blnUpdateAccountTeam, idReturn);//idReturn;
    }
    
    class WrOwnerIdCaseForce {
        Boolean blnUpdateAccountTeam { get; private set; }
        Id caseOwnerId { get; private set; }
        
        public WrOwnerIdCaseForce(Boolean blnUpdateAccountTeamParam, Id caseOwnerIdParam){
            blnUpdateAccountTeam = blnUpdateAccountTeamParam;
            caseOwnerId = caseOwnerIdParam;
        }
        
    }
    
    /**
* xxxxxxxxxx
* @param  none
* @return none  
**/
    /* public static Id getOwner(ISSM_TypificationMatrix__c typicationMatrix, Id accountId,ISSM_AppSetting_cs__c OISSMCS){

Id idReturn = null;
Account salesOffice = null;
Account accountData = null;
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.QUEUE){
idReturn = typicationMatrix.ISSM_IdQueue__c;
} else if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.USER 
|| typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.SUPERVISOR 
|| typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.BILLINGMANAGER 
|| typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.TRADEMARKETING){
idReturn = OISSMCS.ISSM_IdQueueFriendModel__c;
} else{
idReturn = '';
}
return idReturn;

}*/
    
    /**
* xxxxxxxxxx
* @param  none
* @return none  
**/
    /*public static  Id getForceOwner(ISSM_TypificationMatrix__c typicationMatrix, Id accountId){
Id idReturn = null;
ISSM_Account_cls accountCls = new ISSM_Account_cls();
Account accountData = accountCls.getAccount(accountId); 
Account salesParnert = accountCls.getParnertAccount(accountId); 
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.USER){
idReturn = typicationMatrix.ISSM_OwnerUser__c;
} 
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.QUEUE){
idReturn = accountData.OwnerId;
}
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.SUPERVISOR
|| typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
|| typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
ISSM_AccountTeamMember_cls accountTeamMemberCls = new ISSM_AccountTeamMember_cls();
List<Id> ids = null;//accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountId);
if(typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.MODELORAMA 
|| typicationMatrix.ISSM_AssignedTo__c ==ISSM_Constants_cls.REPARTO){
ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountData.ISSM_SalesOffice__r.Id);
} else {
ids = accountTeamMemberCls.getUserIdForAccountTeamMember(typicationMatrix.ISSM_AssignedTo__c, accountId);
}
if(ids != null && ids.size() > 0){
idReturn = ids.get(0);
}
else{
idReturn = accountData.OwnerId;
// blnUpdateAccountTeam = true;
}
} 
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.BILLINGMANAGER){
if(accountData.ISSM_SalesOffice__c != null){
if( accountData.ISSM_SalesOffice__r.ISSM_BillingManager__c != null){
idReturn = accountData.ISSM_SalesOffice__r.ISSM_BillingManager__c;
} 
else{
idReturn = accountData.OwnerId;
}
} 
else{
idReturn = accountData.OwnerId;
}
} 
if(typicationMatrix.ISSM_AssignedTo__c == ISSM_Constants_cls.TRADEMARKETING){
if(accountData.ISSM_SalesOffice__c != null){
if( accountData.ISSM_SalesOffice__r.ISSM_TradeMarketing__c != null){
idReturn = accountData.ISSM_SalesOffice__r.ISSM_TradeMarketing__c;
}
else{
idReturn = accountData.OwnerId;
}
}
else{
idReturn = accountData.OwnerId;
}
}
System.debug('##### : idReturn:' +idReturn);
return idReturn;
}*/
    
    
}