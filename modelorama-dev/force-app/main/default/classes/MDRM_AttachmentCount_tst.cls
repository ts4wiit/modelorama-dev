@isTest
private class MDRM_AttachmentCount_tst {
	
	@isTest static void InsertDelete_test() {		
		//Crear Cuenta
		Account acc = createAccount();

		test.StartTest();

		//Crear Attachment
		Attachment atti = createAttachment(acc.Id);

		//Eliminar attachment
		delete atti;

		undelete atti;

		test.StopTest();
	}

	//Data
	public static Account createAccount() {
	    Account account = new Account();
	    account.Name = 'Prueba Expansor Nombre';
	    
	    RecordType recordType = getRecordTypeByName('MDRM_Expansor', 'Account');

	    if (recordType != null){
	    	account.RecordType = recordType;
    		account.RecordTypeid = recordType.id;
    	}

	    insert account;
	    
	    return account;
	}

	public static Attachment createAttachment(Id id) {
	    Attachment att = new Attachment();
	    att.ParentId = id;
	    att.Name = 'Prueba Attachment';
	    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
	    att.Body = bodyBlob;
	    att.ContentType = 'image';
	    insert att;
	    
	    return att;
	}

	public static RecordType getRecordTypeByName(String recordTypeName, String sObjectType) {
    return [
				SELECT   Id, Name, Description
				FROM   RecordType
				WHERE   SObjectType = : sObjectType
				AND   DeveloperName = : recordTypeName LIMIT 1];
  }
	
}