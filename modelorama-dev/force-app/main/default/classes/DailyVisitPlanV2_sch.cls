/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Sheduler program batch and generate visit plans

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       05-07-2017        Nelson Sáenz Leal (NSL)  Creation Class
****************************************************************************************************/
global with sharing class DailyVisitPlanV2_sch implements Schedulable {
	public Integer serviceModelStep {get;set;}
    private Integer visitPeriodConfig;
	
	
	global DailyVisitPlanV2_sch(){
		this.serviceModelStep = 1;
	}
	
	global DailyVisitPlanV2_sch(Integer intStep){
		this.serviceModelStep = intStep;
	}
    global DailyVisitPlanV2_sch(Integer intStep, Integer vPeriodConfig){
		this.serviceModelStep = intStep;
        visitPeriodConfig = vPeriodConfig;
	}
    global void execute(SchedulableContext sc){
    	VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
    	Integer batchSize = visitPlanSettings.BatchSize__c != null ? Integer.valueOf(visitPlanSettings.BatchSize__c) : 50;
    	Integer batchSizeHK = visitPlanSettings.BatchSizeHK__c != null ? Integer.valueOf(visitPlanSettings.BatchSizeHK__c) : 10;
        if(visitPeriodConfig==null){ 
            if(this.serviceModelStep == 1)
                database.executebatch(new DailyVisitPlanV2_bch('Telesales', 1), batchSize);	
            else if(this.serviceModelStep == 2)
                database.executebatch(new DailyVisitPlanV2_bch('BDR', 2), batchSize);
            else if(this.serviceModelStep == 3)
                database.executebatch(new DailyVisitPlanV2_bch('Autosales', 3), batchSize);
            else if(this.serviceModelStep == 4)
                database.executebatch(new DailyVisitPlanV2_bch('Presales',  4), batchSize);
            else if(this.serviceModelStep == 5)
                database.executebatch(new SendToursEventsToHeroku_bch(), batchSizeHK);
        }else{
            if(this.serviceModelStep == 1)
                database.executebatch(new DailyVisitPlanV2_bch('Telesales', 1,visitPeriodConfig), batchSize);	
            else if(this.serviceModelStep == 2)
                database.executebatch(new DailyVisitPlanV2_bch('BDR', 2, visitPeriodConfig), batchSize);
            else if(this.serviceModelStep == 3)
                database.executebatch(new DailyVisitPlanV2_bch('Autosales', 3, visitPeriodConfig), batchSize);
            else if(this.serviceModelStep == 4)
                database.executebatch(new DailyVisitPlanV2_bch('Presales',  4, visitPeriodConfig), batchSize);
            else if(this.serviceModelStep == 5)
                database.executebatch(new SendToursEventsToHeroku_bch(), batchSizeHK);
        }
    	
    }
}