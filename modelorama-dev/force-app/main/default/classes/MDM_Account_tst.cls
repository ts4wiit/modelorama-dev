/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "MDM_Account_cls". This class is the query class of the project CDM
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class MDM_Account_tst {
    public static Account  salesOrg;
    public static Account  salesOrgAC;
    public static Account  salesOffice;
    public static AdmissibleCenter__c admissibleCenter;
    public static SalesTeambySalesOffice__c salesTeambySalesOffice;
    public static CDM_Temp_Interlocutor__c cdmTempInterlocutor;
    public static Set<String> lstSetSector;
    public static Set<String> lstSetChannel;
    public static MDM_Temp_Account__c mdmTempAccount;
    public static MDM_Temp_Account__c mdmTempAccount2;
    public static SalesArea__c salesArea;
    public static List <Sector__c> listInsertSector;
    public static List <DistributionChannel__c> listInsertChannel;
    //Get RecordTypeId
    public static list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account'];
    public static map<String, RecordType> mapRT = new map<String, RecordType>();
    
    public static void StartValuesGeneric(){
        
        GR_Sales_Team__c grSalesTeam 	= new GR_Sales_Team__c();
        grSalesTeam.Name 				= 'FZ01  Supervisor 2';
        grSalesTeam.Code__c 			= '274';
        insert grSalesTeam;
        
        //mapa de RecordType
        for(RecordType rt : lstRT)
            mapRT.put(rt.DeveloperName, rt);       
        
    }
    public static void StartValuesAdmissibleCenter(){
        //Get recordTypes
        StartValuesGeneric();
        //Create sales Organization 
        salesOrgAC 							= new Account();
        salesOrgAC.RecordTypeId         		= mapRT.get('SalesOrg').Id;
        salesOrgAC.Name 						= 'CMM San Luis Potosí';
        salesOrgAC.ONTAP__SAPCustomerId__c 	= '3109';
        insert salesOrgAC;
        
        //Create SupplierCenter
        SupplierCenter__c supplierCenter = new SupplierCenter__c();
        supplierCenter.Name				= 'SAG OCAMPO';
        supplierCenter.Code__c			= 'GB16';
        insert supplierCenter;
        
        //Create admissibleCenter
        admissibleCenter = new AdmissibleCenter__c();
        admissibleCenter.name 				= '3109-02-GB16';
        admissibleCenter.Id_External__c  	= '3109-02-GB16';
        admissibleCenter.VKORGId__c			= salesOrgAC.Id;
        admissibleCenter.VTWEGId__c			= listInsertChannel.get(1).Id;
        admissibleCenter.VWERKId__c			= supplierCenter.Id;
        insert admissibleCenter;     
    }
    
    public static void StartValuesSalesTeambySalesOffice(){        
        //Create SalesTeamByOffice
        salesTeambySalesOffice = new SalesTeambySalesOffice__c();
        salesTeambySalesOffice.Name = '3018-M01';
        salesTeambySalesOffice.Id_External__c = '3018-M01';
        insert salesTeambySalesOffice;
        
    }
    public static void StartValuesGetInterlocutors(){        
        //Create CDM_Temp_Interlocutor__c
        cdmTempInterlocutor = new CDM_Temp_Interlocutor__c();
        cdmTempInterlocutor.MDM_Temp_Account__c = mdmTempAccount.Id;
        cdmTempInterlocutor.PARVW__c			='SH';
        cdmTempInterlocutor.KUNNR__c			='0100572166';
        insert cdmTempInterlocutor;
        
    }
    public static void StartValuesSalesArea(){
        //var private 
        integer auxCont = 1;
        //Get recordTypes
        StartValuesGeneric();
        //Create Sector
        Set<String> lstSetSector = new Set<String>{'Común', 'Cerveza', 'Agua','Refrescos','Jugos','Promocionales','Otros','Empaques','Servicios','Materia Prima'};
            listInsertSector = new List<Sector__c>();        
        for (String nameSector : lstSetSector){
            Sector__c sector = new Sector__c(name = nameSector, Code__c = auxCont > 9 ?  String.valueOf(auxCont) : '0' + String.valueOf(auxCont++));
            listInsertSector.add(sector);
        }
        upsert listInsertSector;
        
        //Create distribution channel
        auxCont = 1;
        Set<String> lstSetChannel = new Set<String>{'TRADICIONAL', 'MODERNO', 'INTERCOMPAÑIA','EXPORTACIONES','AGENCIA PARTICULAR','COMPAÑIA RELACIONADA','EMPLEADOS','CLIENTES VARIOS','MATERIA PRIMA','INTRAGRUPO'};
            listInsertChannel = new List<DistributionChannel__c>();        
        for (String nameChannel : lstSetChannel){
            DistributionChannel__c channel = new DistributionChannel__c(name = nameChannel, Code__c = auxCont > 9 ?  String.valueOf(auxCont) : '0' + String.valueOf(auxCont++));
            listInsertChannel.add(channel);
        }
        upsert listInsertChannel;
        
        //create sales Organization 
        salesOrg 							= new Account();
        salesOrg.RecordTypeId         		= mapRT.get('SalesOrg').Id;
        salesOrg.Name 						= 'CMM Hidalgo';
        salesOrg.ONTAP__SAPCustomerId__c 	= '3108';
        insert salesOrg;
        
        //Create sales Office
        salesOffice 						= new Account();
        salesOffice.RecordTypeId         	= mapRT.get('SalesOffice').Id;
        salesOffice.Name 					= 'CMM Actopan';
        salesOffice.ONTAP__SalesOgId__c 	= '3108';
        salesOffice.ONTAP__SAPCustomerId__c	= 'GC01';
        salesOffice.ISSM_ParentAccount__c	= salesOrg.Id;
        insert salesOffice;  
        
        //Create SalesArea
        salesArea 					= new SalesArea__c ();
        salesArea.Name 				= '3108-01-01-GC01';
        salesArea.Id_External__c 	= '3108-01-01-GC01';
        salesArea.SPARTId__c 		= listInsertSector.get(0).Id;
        salesArea.VKBURId__c 		= salesOffice.Id;
        salesArea.VKORGId__c 		= salesOrg.Id;
        salesArea.VTWEGId__c 		= listInsertChannel.get(0).Id;
        insert salesArea;        
    }
    
    public static void StartValuesMDMTempAccount(){
        //Create MDM_Temp_Account__c
        //Load Static resources 
        List<sObject> ls = Test.loadData(MDM_Temp_Account__c.sObjectType, 'MDMTempAccount');
        mdmTempAccount = [SELECT Id,KTOKD__c, ANRED__c,NAME1__c,NAME2__c,PARVW__c,KUNN2__c  FROM MDM_Temp_Account__c  ORDER BY KTOKD__c  ASC limit 1];System.debug('mdmTempAccount1 = '+mdmTempAccount);
        mdmTempAccount2 = [SELECT Id,KTOKD__c, ANRED__c,NAME1__c,NAME2__c,PARVW__c,KUNN2__c  FROM MDM_Temp_Account__c ORDER BY KTOKD__c DESC limit 1]; System.debug('mdmTempAccount2 = '+mdmTempAccount2);
    }
    
    @isTest static void testGetSalesAreas() {
        // Load data
        StartValuesSalesArea();
        Set<String> salesOffice_lst = new Set<String>(); 
        salesOffice_lst.add(salesOffice.ONTAP__SAPCustomerId__c);
        
        
        test.startTest();
        Map<String, List<SalesArea__c>> mapSalesAreas = MDM_Account_cls.getSalesAreas(salesOffice_lst);
        MDM_Account_cls.salesAreas.clear();
        System.AssertEquals(1,MDM_Account_cls.getSalesAreas(salesOffice_lst).size());
        test.stopTest();
    }

    @isTest static void testgetGRSalesTeam() {
        //Load data
        StartValuesGeneric();
        
        test.startTest();
        //Assert validation for method getGRSalesTeam() must to return values created in  StartValuesGeneric()
        System.assertEquals(new Set<String>{'274'},MDM_Account_cls.getGRSalesTeam()); 
        Set<String> setGRSalesTeamGran= new Set<String>();
        MDM_Account_cls.GRSalesTeamGranSet.clear();
        MDM_Account_cls.GRSalesTeamGranSet.addAll(setGRSalesTeamGran);
        //Assert Validation must to return the current values in private static variable GRSalesTeamGranSet, in this case was assigned a new Instance.
        System.assertEquals(new Set<String>(),MDM_Account_cls.getGRSalesTeam()); 
        test.stopTest();
    }
    
    @isTest static void testGetKUNN2() {
        //Load data
        StartValuesMDMTempAccount();
        
        test.startTest();
        //Assert validation for method getGRSalesTeam() must to return values created in  StartValuesMDMTempAccount()
        System.assertEquals(1,MDM_Account_cls.getKUNN2(new Set<String>{mdmTempAccount2.KUNN2__c}).size());
        Set<String> setKUNN2 = new Set<String>();
        MDM_Account_cls.setKUNN2.clear();
        MDM_Account_cls.setKUNN2.addAll(setKUNN2);
        //Assert Validation must to return the current values in private static variable setKUNN2, in this case was assigned a new Instance.
        System.assertEquals(new Set<String>(),MDM_Account_cls.getKUNN2(new Set<String>{mdmTempAccount2.KUNN2__c}));
        test.stopTest();
    }
    @isTest static void testGetKUNNRR19() {
        //Load data
        StartValuesMDMTempAccount();
        StartValuesGetInterlocutors();
        
        test.startTest();
        
        Map<String, CDM_Temp_Interlocutor__c> mpKRR19 = new Map<String, CDM_Temp_Interlocutor__c>();
        mpKRR19.put(mdmTempAccount.Id, cdmTempInterlocutor); 
        //Assert validation for method getKUNNRR19() must to return values if find a MDM_Temp_Account record with same PARVW__c and Z001, in this case doesnt apply
        System.assertEquals(0,MDM_Account_cls.getKUNNRR19(mpKRR19).size());
        Set<String> setKUNNRR19 = new Set<String>();
        MDM_Account_cls.setKUNNRR19.clear();
        MDM_Account_cls.setKUNNRR19.addAll(setKUNNRR19);
        //Assert Validation must to return the current values in private static variable setKUNNRR19, in this case was assigned a new Instance.
        System.assertEquals(new Set<String>(),MDM_Account_cls.getKUNNRR19(mpKRR19));
        
        test.stopTest();
    }
    @isTest static void testGetKUNN2R19() {
        //Load data
        StartValuesMDMTempAccount();
        StartValuesGetInterlocutors();
        
        test.startTest();        
        
        Set<String> stK2 = new Set<String>();
        stK2.add(cdmTempInterlocutor.KUNN2__c); 
        //Assert validation for method getKUNN2R19() must to return a map that contains CDM_Temp_Interlocutor records with Funciont SH where KUNNR__c & KUNN2 are not the same.
        System.assertEquals(cdmTempInterlocutor.Id,MDM_Account_cls.getKUNN2R19(stK2).get(cdmTempInterlocutor.Id).Id);
        Map<String, CDM_Temp_Interlocutor__c> mapInterlocutorsR19 = new Map<String, CDM_Temp_Interlocutor__c>();
        MDM_Account_cls.mapInterlocutorsR19.clear();
        MDM_Account_cls.mapInterlocutorsR19.putAll(mapInterlocutorsR19);
        MDM_Account_cls.getR0028(stk2);
        //Assert Validation must to return the current values in private static variable getKUNN2R19, in this case was assigned a new Instance.
        System.assertEquals(new Map<String, CDM_Temp_Interlocutor__c>(),MDM_Account_cls.getKUNN2R19(stK2));
        
        test.stopTest();
    }
    @isTest static void testGetMapKUNNR() {
        //Load data
        StartValuesMDMTempAccount();
        
        test.startTest();
        //Assert Validation must return a record with the same PARVw__c & Z019
        System.assertEquals('0100572166',MDM_Account_cls.getMapKUNNR(new Set<String>{mdmTempAccount2.PARVW__c}).get('0100572166-3106').PARVW__c); 
        test.stopTest();
    }
    @isTest static void testGetSalesAreaGranularity() {
        //Load data
        StartValuesSalesArea();
        StartValuesAdmissibleCenter();
        
        test.startTest();
        
        MDM_Account_cls.getAdmCenterGranularity(new Set<String>{admissibleCenter.Id_External__c});
        Set<String> AdmCenterGranSet = new Set<String>();
        MDM_Account_cls.AdmCenterGranSet.clear();
        MDM_Account_cls.AdmCenterGranSet.addAll(AdmCenterGranSet);
        MDM_Account_cls.getAdmCenterGranularity(new Set<String>{admissibleCenter.Id_External__c});
        MDM_Account_cls.getSalesAreaGranularity(new Set<String>{salesArea.Id_External__c});
        Set<String> setSalesAreaGran  = new  Set<String>();   
        MDM_Account_cls.salesAreaGranSet.clear();
        MDM_Account_cls.salesAreaGranSet.addAll(setSalesAreaGran);
        MDM_Account_cls.getSalesAreaGranularity(new Set<String>{salesArea.Id_External__c});
        
        test.stopTest();
    }
    @isTest static void testGetSalesTeamGranularity() {
        //Load data
        StartValuesSalesArea();
        StartValuesSalesTeambySalesOffice();
        
        test.startTest();
        
        MDM_Account_cls.getSalesTeamGranularity(new Set<String>{salesTeambySalesOffice.Id_External__c});
        Set<String> salesTeamGranSet = new Set<String>();
        MDM_Account_cls.SalesTeamGranSet.clear();
        MDM_Account_cls.SalesTeamGranSet.addAll(salesTeamGranSet);
        MDM_Account_cls.getSalesTeamGranularity(new Set<String>{salesTeambySalesOffice.Id_External__c});
        
        test.stopTest();
    }
    @isTest static void testGetInterlocutors() {
        //Load data
        StartValuesMDMTempAccount();
        StartValuesGetInterlocutors();
        
        test.startTest();
        
        MDM_Account_cls.getInterlocutors(new Set<String>{mdmTempAccount.Id}, 'SH');
        Map<String, CDM_Temp_Interlocutor__c> mapInterlocutors = new Map<String, CDM_Temp_Interlocutor__c>();
        MDM_Account_cls.interlocutors.clear();
        MDM_Account_cls.interlocutors.putAll(mapInterlocutors);
        MDM_Account_cls.getInterlocutors(new Set<String>{mdmTempAccount.Id}, 'SH');
        
        test.stopTest();
    }
    
    @isTest static void testvalidationReprocess() {
        //Load data
        StartValuesMDMTempAccount();
        MDM_Rule__c rule = new MDM_Rule__c ();
        rule.MDM_Code__c		='R0008';
        insert rule;
        MDM_Validation_Result__c val = new MDM_Validation_Result__c();
        val.MDM_RuleId__c			=rule.Id;
        val.MDM_Temp_AccountId__c = mdmTempAccount.Id;
        insert val;
        
        test.startTest();
        
        MDM_Account_cls.validationReprocess(new List<String>{mdmTempAccount.Id});
        MDM_Account_cls.validationReprocess(new List<String>{mdmTempAccount.Id});
        
        test.stopTest();
    }
    
    @isTest static void testgetZ001() {
        //Load data
        MDM_Temp_Account__c temp = new MDM_Temp_Account__c();
        temp.KTOKD__c		='Z001';
        temp.AUFSD__c		='01';
        temp.LIFSD__c		='01';
        temp.FAKSD__c		='01';
        temp.PARVW__c		='010095678';
        insert temp;
        
        test.startTest();
        
        MDM_Account_cls.getZ001(new Set<String>{temp.PARVW__c});
        MDM_Account_cls.getZ001(new Set<String>{temp.PARVW__c});
        
        test.stopTest();
    }
    @isTest static void testisZ019() {
      	//Load data
        StartValuesMDMTempAccount();
        test.startTest();
        
        MDM_Account_cls.isZ019(new Set<String>{mdmTempAccount.PARVW__c});
        MDM_Account_cls.isZ019(new Set<String>{mdmTempAccount.PARVW__c});
        MDM_Account_cls.getKUNN2r25();
        
        test.stopTest();
    }
    
    @isTest static void testGets() {
        //Load data
        StartValuesGeneric();
        
        test.startTest();
        
        list<RecordType> lstRecordType = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'];
        Set<String> sizeKALKSvalues= MDM_Account_cls.getKALKSvalues();
        System.assertEquals(4, sizeKALKSvalues.size());
        
        MDM_Account_cls.getAccountMappingLst();
        Map<String,MDM_AccountFieldsMapping__mdt> mapAccountMapping = new Map<String,MDM_AccountFieldsMapping__mdt>();
        MDM_Account_cls.accountMapping.clear();
        MDM_Account_cls.accountMapping.putAll(mapAccountMapping);
        MDM_Account_cls.getAccountMappingLst();
        
        MDM_Account_cls.getInstance();
        Map<String, Map<String,MDM_Parameter__c>> mapCatalogElements = new Map<String, Map<String,MDM_Parameter__c>>();
        MDM_Account_cls.catalogElements.clear();
        MDM_Account_cls.catalogElements.putAll(mapCatalogElements);
        MDM_Account_cls.getInstance();
        
        MDM_Account_cls.getInstanceByDescription();
        Map<String, Map<String,MDM_Parameter__c>> mapCatalogElementsByDescription = new Map<String, Map<String,MDM_Parameter__c>>();
        MDM_Account_cls.catalogElementsByDescription.clear();
        MDM_Account_cls.catalogElementsByDescription.putAll(mapCatalogElementsByDescription);
        MDM_Account_cls.getInstanceByDescription();
        
        MDM_Account_cls.getAccountRT();
        Map<String,Id> mapAccountRecordTypes = new Map<String,Id>();
        MDM_Account_cls.accountRecordTypes.clear();
        MDM_Account_cls.accountRecordTypes.putAll(mapAccountRecordTypes);
        MDM_Account_cls.getAccountRT();
        
        MDM_Account_cls.getSalesOffice();
        Map<String, Account> mapSalesOffice = new Map<String, Account>();
        MDM_Account_cls.salesOffice.clear();
        MDM_Account_cls.salesOffice.putAll(mapSalesOffice);
        MDM_Account_cls.getSalesOffice();
        
        MDM_Account_cls.getSupplierCenter();
        Set<String> supplierCenterSet = new Set<String>();
        MDM_Account_cls.supplierCenterSet.clear();
        MDM_Account_cls.supplierCenterSet.addAll(supplierCenterSet);
        MDM_Account_cls.getSupplierCenter();
        
        MDM_Account_cls.getSalesOrg();
        Map<String, Account> mapSalesOrg = new Map<String, Account>();
        MDM_Account_cls.salesOrg.clear();
        MDM_Account_cls.salesOrg.putAll(mapSalesOrg);
        MDM_Account_cls.getSalesOrg();
        
        MDM_Account_cls.getRulesToApply('MDM_Temp_Account__c');
        Map<String,Map<String, MDM_Rule__c>> mapRulesToApply = new Map<String,Map<String, MDM_Rule__c>>();
        MDM_Account_cls.rulesToApply.clear();
        MDM_Account_cls.rulesToApply.putAll(mapRulesToApply);
        MDM_Account_cls.getRulesToApply('MDM_Temp_Account__c');
        test.stopTest();
    }
    @isTest static void testGetCatalogObject() {
        test.startTest();
        
        Map<String,Id> resultCatObject = new Map<String,Id>();
        resultCatObject.putAll(MDM_Account_cls.getCatalogObject('Account', 'Name', new List<String> {'Id','Name'}));
        Map<String, Map<String,Id>> sObjectCatalog = new Map<String, Map<String,Id>>();
        sObjectCatalog.put(resultCatObject.get('Account'), resultCatObject);
        MDM_Account_cls.sObjectCatalog.clear();
        MDM_Account_cls.sObjectCatalog.putAll(sObjectCatalog);
        MDM_Account_cls.getCatalogObject('Account', 'Name', new List<String> {'Id','Name'});
        
        test.stopTest();
    }
    @isTest static void testGetCatalogObjectCity2() {
        test.startTest();
        
        Map<String,Id> resultCatObject = new Map<String,Id>();
        resultCatObject.putAll(MDM_Account_cls.getCatalogObjectCity2('MDRM_Modelorama_Postal_Code__c', 'Name', new List<String> {'Id','Name'}));
        Map<String, Map<String,Id>> sObjectCatalog = new Map<String, Map<String,Id>>();
        sObjectCatalog.put(resultCatObject.get('MDRM_Modelorama_Postal_Code__c'), resultCatObject);
        MDM_Account_cls.sObjectCatalog.clear();
        MDM_Account_cls.sObjectCatalog.putAll(sObjectCatalog);
        MDM_Account_cls.getCatalogObjectCity2('MDRM_Modelorama_Postal_Code__c', 'Name', new List<String> {'Id','Name'});
        
        test.stopTest();
    }
    @isTest static void testGetGranularity() {
        //Load data
        StartValuesSalesArea(); 
        
        test.startTest(); 
               
        List<SalesArea__c> lst2 = [SELECT Id, Name, Id_External__c FROM SalesArea__c ];
        MDM_Account_cls.getGranularity('\''+lst2.get(0).Id_External__c+'\'','SalesArea__c') ;          
        
        test.stopTest();
    }
    
    @isTest static void testGetRegionalSalesDivision() {
        Account regionalSalesDivision 					= new Account();
        regionalSalesDivision.RecordTypeId         		= [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;//mapRT.get('ISSM_RegionalSalesDivision').Id;
        regionalSalesDivision.Name 						= 'CMM San Luis Potosí';
        regionalSalesDivision.ONTAP__SAPCustomerId__c 	= '3109';
        insert regionalSalesDivision;    
        
        test.startTest();
        
        MDM_Account_cls.getRegionalSalesDivision();
        Map<String, Account> mapRegionalsalesD = new Map<String, Account>();
        MDM_Account_cls.regionalsalesD.clear();
        MDM_Account_cls.regionalsalesD.putAll(mapRegionalsalesD);
        MDM_Account_cls.getRegionalSalesDivision();       
        
        test.stopTest();
    }
    
    
    @isTest static void testGetStructure() {
        
        Account regionalSalesDivision 					= new Account();
        regionalSalesDivision.RecordTypeId         		= [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;
        regionalSalesDivision.Name 						= 'CMM San Luis Potosí';
        regionalSalesDivision.ONTAP__SAPCustomerId__c 	= '3109';
        insert regionalSalesDivision; 
        
        Account regionalSalesDivision2 					= new Account();
        regionalSalesDivision2.RecordTypeId         		= [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;
        regionalSalesDivision2.Name 						= 'CMM San Luis Potosí';
        regionalSalesDivision2.ONTAP__SAPCustomerId__c 	= '3109';
        regionalSalesDivision2.ISSM_ParentAccount__c 	= regionalSalesDivision.Id;
        insert regionalSalesDivision2;
        
        Account regionalSalesDivision3 					= new Account();
        regionalSalesDivision3.RecordTypeId         	= [SELECT Id FROM RecordType WHERE DeveloperName = 'ISSM_RegionalSalesDivision'].get(0).Id;
        regionalSalesDivision3.Name 					= 'CMM San Luis Potosí 2';
        regionalSalesDivision3.ONTAP__SAPCustomerId__c 	= '3109';
        regionalSalesDivision3.ISSM_ParentAccount__c 	= regionalSalesDivision.Id;
        insert regionalSalesDivision3;   
        
        Account sOrg 						= new Account();
        sOrg.RecordTypeId         			= [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOrg'].get(0).Id;
        sOrg.Name 							= 'CMM San Luis Potosí';
        sOrg.ONTAP__SAPCustomerId__c 		= '3109';
        sOrg.ISSM_ParentAccount__c 			= regionalSalesDivision.Id;
        insert sOrg;
        
        Account sOff 					= new Account();
        sOff.RecordTypeId         		= [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesOffice'].get(0).Id;
        sOff.Name 						= 'CMM Actopan';
        sOff.ONTAP__SalesOgId__c 		= '3109';
        sOff.ONTAP__SAPCustomerId__c	= 'GC01';
        sOff.ISSM_ParentAccount__c		= regionalSalesDivision.Id;
        insert sOff;
        
        test.startTest();
        
        MDM_Account_cls.getStructure();
        Map<String,Map<String,Set<String>>> mapStructure = new Map<String,Map<String,Set<String>>>();
        MDM_Account_cls.structure.clear();
        MDM_Account_cls.structure.putAll(mapStructure);
        MDM_Account_cls.getStructure();      
        
        test.stopTest();
    }
    
    
    @isTest static void testIsPicklistValue() {
        test.startTest();
        
        Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe(); 
        Map<String, Schema.SobjectField> MDMTempAccountFields = sObjectTypeMap.get('MDM_Account__c').getDescribe().fields.getMap(); 
        MDM_Account_cls.isPicklistValue(MDMTempAccountFields.get('KATR9__c').getDescribe(), 'test');
        
        test.stopTest();
    }    
    
}