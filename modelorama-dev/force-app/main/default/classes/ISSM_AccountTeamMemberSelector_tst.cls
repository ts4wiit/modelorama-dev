@isTest
private class ISSM_AccountTeamMemberSelector_tst {
    
    static testMethod void testselectById()
    {
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        AccountTeamMember accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        List<AccountTeamMember> accountList = new List<AccountTeamMember> {accountTeamMember};      
        Set<Id> idSet = new Set<Id>();
        for(AccountTeamMember item : accountList)
            idSet.add(item.Id);
            
        Test.startTest();       
        List<AccountTeamMember> result = ISSM_AccountTeamMemberSelector_cls.newInstance().selectById(idSet);        
        Test.stopTest();
        
        system.assertEquals(1,result.size());
        
    }

    static testMethod void testselectById2(){
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        AccountTeamMember accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        
        Test.startTest();       
        AccountTeamMember result = ISSM_AccountTeamMemberSelector_cls.newInstance().selectById(accountTeamMember.ID);       
        Test.stopTest();

        system.assertNotEquals(result, null);
        system.assertEquals(result.Id, accountTeamMember.Id);
    }

    static testMethod void testselectById3(){
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        AccountTeamMember accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        
        Test.startTest();       
        List<AccountTeamMember> result = ISSM_AccountTeamMemberSelector_cls.newInstance().selectById(accountTeamMember.ID+'');      
        Test.stopTest();

        system.assertNotEquals(result, null);
        system.assertEquals(1, result.size());
    }

    static testMethod void testselectByMultipleFields()
    {
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        AccountTeamMember accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        List<AccountTeamMember> accountList = new List<AccountTeamMember> {accountTeamMember};      
        Set<Id> idSet = new Set<Id>();
        for(AccountTeamMember item : accountList)
            idSet.add(item.Id);
            
        Test.startTest();   
        try{
        List<AccountTeamMember> result = ISSM_AccountTeamMemberSelector_cls.newInstance().selectByMultipleFields(ISSM_Constants_cls.SUPERVISOR,idSet);      
        }catch(Exception ex){

        }
        Test.stopTest();
        
        
        
    }
    
}