/**
 * This class is the test class for the AllMobileDeviceUserControllerExtension class.
 * <p /><p />
 * @author Alberto Gómez
 */
@isTest
public class AllMobileDeviceUserCtrlExtensionTest {

	/**
	 * Test method.
	 */
	static testMethod void test() {

		//Insert Device User object.
		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c();
		insert objAllMobileDeviceUser;
		ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAllMobileDeviceUser);
		AllMobileDeviceUserControllerExtension objAMDeviceUserControllerExtension = new AllMobileDeviceUserControllerExtension(objStandardController);
	}

	/**
	 * Test assignPassword method.
	 */
	static testMethod void testAssignMethod() {

		//Mock responses.
		AllMobileUtilityHelperTest.insertAllMobileCustomSetting();
		Test.setMock(HttpCalloutMock.class, new AllMobileMockHttpResponseGeneratorTest());

		//Start test.
		Test.startTest();

		//Insert Device User object.
		AllMobileDeviceUser__c objAllMobileDeviceUser = new AllMobileDeviceUser__c();
		insert objAllMobileDeviceUser;
		ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAllMobileDeviceUser);
		AllMobileDeviceUserControllerExtension objAMDeviceUserControllerExtension = new AllMobileDeviceUserControllerExtension(objStandardController);
		objAMDeviceUserControllerExtension.strPassword = 'Grissa';
		objAMDeviceUserControllerExtension.assignPassword();

		//Stop test.
		Test.stopTest();
	}
}