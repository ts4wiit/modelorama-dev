/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    clase de prueba para cubrir el controlador ISSM_StratPageOrder_ctr

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description              
    ------    --------        --------------------------   -----------------------------------------
    1.0       15-Junio-2017   Luis Licona                  Creación de la Clase     
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_StratPageOrder_tst {
	
	static Account ObjAccount{get;set;}
	static Account ObjAccount1{get;set;}
	static Account ObjAccountHD{get;set;}
	static Account ObjAccount2{get;set;}
	static ONCALL__Call__c ObjCall{get;set;}
	static ONCALL__Call__c ObjCall2{get;set;}
	static ApexPages.StandardController sc{get;set;}
	static ApexPages.StandardController sc2{get;set;}
	static ONTAP__Order__c ObjOrder{get;set;}
	static ONTAP__Order__c ObjOrder2{get;set;}
	static ONTAP__Order__c ObjOrder3{get;set;}
	static ONTAP__Route__c ObjRoute{get;set;}
	static AccountByRoute__c ObjAccByRoute{get;set;}

	static Profile p{get;set;}
	static User user1{get;set;}
	static Id IdRec{get;set;}

	public static void StartValues(){
		ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
		Id RecType 	     = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('Emergency').getRecordTypeId();
		Id StrRecTypeAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        Id StrRecType    = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SalesOffice').getRecordTypeId();
        Id RecTypeCal    = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get('ISSM_TelesalesCall').getRecordTypeId();
        IdRec            = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Sales').getRecordTypeId();

		ObjAccount = new Account(
			Name = 'TST1',
			ONTAP__Main_Phone__c='5510988765',
			ISSM_MainContactE__c=true
		);
		insert ObjAccount;
		

		ObjAccount1 = new Account(
			Name = 'TST2',
			ONTAP__Main_Phone__c      = '5510988764',
			ISSM_MainContactE__c      = true,
			ONTAP__SalesOffId__c      = 'FG00',
			RecordTypeId 			  = StrRecType
		);
		insert ObjAccount1;

		ObjAccountHD = new Account(
			Name = 'TST3',
			ONTAP__Main_Phone__c      = '5510988765',
			ISSM_MainContactE__c      = true,
			ONTAP__SAP_Number__c      = '010016981',
			ONTAP__SalesOgId__c       = '3116',
			ONTAP__ChannelId__c       = '01',	
			ONCALL__Ship_To_Name__c   = '0100169817',
			RecordTypeId 			  = StrRecTypeAcc,
			ISSM_SalesOffice__c       = ObjAccount1.Id
		);
		insert ObjAccountHD;

		ObjCall = new ONCALL__Call__c(
			Name 			= 'TSt4',
			ONCALL__POC__c 	= ObjAccountHD.Id,
			ISSM_ValidateOrder__c=true,
			RecordTypeId = RecTypeCal,
			ISSM_CallResult__c = Label.ISSM_EffectiveCall,
			ONCALL__Date__c = System.today()
		);
		insert ObjCall;
		
		ObjCall2 = new ONCALL__Call__c(
			Name 			      = 'TST5',
			ONCALL__POC__c 	      = ObjAccountHD.Id,
			ISSM_ValidateOrder__c = true,
			RecordTypeId          = RecTypeCal,
			ISSM_CallResult__c    = Label.ISSM_EffectiveCall,
			ONCALL__Date__c       = System.today() + 1
		);
		insert ObjCall2;

		ObjOrder = new ONTAP__Order__c( 
			ONCALL__OnCall_Account__c   = ObjAccountHD.Id,
			ONTAP__OrderAccount__c      = ObjAccountHD.Id,
			ONCALL__Call__c             = ObjCall.id,
			ONTAP__OrderStatus__c       = Label.ISSM_OrderStatus3,
			ONCALL__Origin__c           = 'AUT',  
            ONCALL__SAP_Order_Number__c = '111111',
			ONTAP__DeliveryDate__c      = System.today()+1,
			ONTAP__BeginDate__c         = System.today(),
			ISSM_EndOrder__c            = true,
			RecordTypeId                = RecType
		);
		insert ObjOrder;

		ObjOrder2 = new ONTAP__Order__c(
			ONCALL__OnCall_Account__c   = ObjAccountHD.Id,
			ONTAP__OrderAccount__c      = ObjAccountHD.Id,
			ONCALL__Call__c             = ObjCall2.id,
			ONTAP__OrderStatus__c       = 'In Progress',
			ONCALL__Origin__c           = 'PRE', //preventa
            ONCALL__SAP_Order_Number__c = '1111112',   
			ONTAP__DeliveryDate__c      = System.today()+1,
			ONTAP__BeginDate__c         = System.today(),
			ISSM_EndOrder__c            = true,
			RecordTypeId                = RecType
		);
		insert ObjOrder2;
		
		ObjOrder3 = new ONTAP__Order__c(
			ONCALL__OnCall_Account__c=ObjAccountHD.Id,
			ONTAP__OrderAccount__c = ObjAccountHD.Id,
			ONTAP__OrderStatus__c = 'Open',
			ONCALL__Origin__c         = 'B2B-0000', //mi modelo ,
            ONCALL__SAP_Order_Number__c   = '1111113'  ,    
			ONTAP__DeliveryDate__c= System.today()+1,
			ONTAP__BeginDate__c= System.today(),
			ISSM_EndOrder__c = false,
			RecordTypeId  = RecType
		);
		insert ObjOrder3;
		
		p = [SELECT Id FROM Profile WHERE Name='ISSM-SalesTeam'];

		user1 = new User(
			Alias               = 'TV', 
        	Email               = 'tv@gmodelo.com',
			EmailEncodingKey    = 'UTF-8', 
			LastName            = 'Testing', 
			LanguageLocaleKey   = 'en_US',
			LocaleSidKey        = 'en_US', 
			ProfileId           = p.Id,
			TimeZoneSidKey      = 'America/Los_Angeles', 
			UserName            = 'televentas1@gmodelo.com.mx',
			IsActive            = true);
		insert user1;

		ObjRoute = new ONTAP__Route__c(
								ONTAP__RouteId__c     = 'FG0050',
								ISSM_SAPUserId__c     = '5060764',
								RecordTypeId 	      = IdRec,
								RouteManager__c       = user1.Id,
								ONTAP__SalesOffice__c = ObjAccountHD.Id,
								ServiceModel__c		  = Label.ISSM_OriginCall
							);
		insert ObjRoute;

		sc  = new ApexPages.standardController(ObjOrder);
		sc2 = new ApexPages.standardController(ObjOrder2);	
		System.debug('SELECT 1  : '+[SELECT Id
                FROM ONCALL__Call__c
                WHERE ONCALL__POC__c =: ObjAccountHD.Id]);		        
	}


	@isTest static void OnlyStartPage() {
		Test.startTest();
			StartValues();
			ISSM_StratPageOrder_ctr CTR = new ISSM_StratPageOrder_ctr(sc);
			PageReference pr = CTR.startPage();
			System.assertEquals(true,CTR.BlnHaveOrder);
		Test.stopTest();
	}
	
	@isTest static void DataComplete() {				
		Test.startTest();			
			StartValues();
				
			ISSM_StratPageOrder_ctr CTR = new ISSM_StratPageOrder_ctr(sc2);
			PageReference pr = CTR.startPage();
		Test.stopTest();		
	}
}