/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex class controller for lightning component 'CDM_FilterExecution_cmp'
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-Mayo-2018   Iván Neria			   Initial version
***********************************************************************************/
global with sharing class CDM_FilterExecution_ctr {

    @AuraEnabled
    WebService static string ExecuteProcessRules(Integer lmtQry,  String[] KTOKD, String VKORG, String VKBUR, String SPART, String VTWEG ){
        MDM_ProcessRules_bch b = new MDM_ProcessRules_bch(lmtQry, KTOKD, VKORG, VKBUR, SPART, VTWEG);
        ID batchprocessid = Database.executeBatch(b, 100); 
        return batchprocessid;
   }
     @AuraEnabled
    WebService static string ExecuteRelationships(){
        CDM_UpdateRelaionships_bch b = new CDM_UpdateRelaionships_bch();
        ID batchprocessid = Database.executeBatch(b, 50); 
        return batchprocessid;
   }
   @AuraEnabled
    WebService static string ExecuteReprocessRules( ){
        CDM_ReprocessBatch_bch b = new CDM_ReprocessBatch_bch();
        ID batchprocessid = Database.executeBatch(b, 300); 
        return batchprocessid;
   }
    @AuraEnabled
    WebService static string ExecuteDeleteProcess( ){
        CDM_MassDeleteValidationRes_bch b = new CDM_MassDeleteValidationRes_bch();
        ID batchprocessid = Database.executeBatch(b, 500); 
        return batchprocessid;
   }
    @AuraEnabled	
     public static integer getNewRecords(){
       	integer results = [SELECT COUNT() FROM MDM_Temp_Account__c WHERE CDM_Flag_Control__c=false];
        return results;
    }
    @AuraEnabled	
     public static List<MDM_Rule__c> getRules(){
            if(CDM_FilterExecution_ctr.getNewRecords()>0){
                List<MDM_Rule__c> lstRules= [SELECT Id,MDM_IsActive__c, MDM_Code__c,MDM_Rule_Description__c, CDM_Order__c  FROM MDM_Rule__c ];
                for(MDM_Rule__c rule: lstRules){
                    rule.MDM_IsActive__c = (rule.MDM_Code__c==Label.CDM_Apex_CodeRule8 || rule.MDM_Code__c==Label.CDM_Apex_CodeRule14) ? true: false;
                }  
             update lstRules;
            }
         List<MDM_Rule__c> results = (CDM_FilterExecution_ctr.getNewRecords()>0) ? [SELECT Id,MDM_IsActive__c, MDM_Code__c,MDM_Rule_Description__c,CDM_Order__c  FROM MDM_Rule__c WHERE (MDM_Code__c=:Label.CDM_Apex_CodeRule14 OR MDM_Code__c=:Label.CDM_Apex_CodeRule8) ORDER BY CDM_Order__c] : [SELECT Id,MDM_IsActive__c, MDM_Code__c,MDM_Rule_Description__c,CDM_Order__c  FROM MDM_Rule__c  ORDER BY CDM_Order__c] ;
        return results;
    }
    // Get Rules actived to execute the batch rules
    @AuraEnabled	
     public static List<String> getRulesApply(){
         List<MDM_Rule__c> results = [SELECT Id,MDM_IsActive__c, MDM_Code__c,MDM_Rule_Description__c, CDM_Order__c  FROM MDM_Rule__c  WHERE MDM_IsActive__c=true];
         List<String> strlst = new List<String>();
         for(MDM_Rule__c s: results){
             strlst.add(s.MDM_Code__c);
         }
        return strlst;
    }
	// Get total records available to apply the rules
    @AuraEnabled
    public static integer getTotalTemp(){
        integer results = [SELECT COUNT() FROM MDM_Temp_Account__c WHERE MDM_control_check__c=true];
        return results;
    }
    @AuraEnabled
    public static List<String> getSalesOff(){
        List<Account> results= [ SELECT Id, Name, ONTAP__SalesOffId__c FROM Account WHERE RecordType.Name=:Label.CDM_Apex_SalesOff ORDER BY ONTAP__SalesOffId__c];
        List<String> values = new List<String>();
        values.add('--');
        for(Account i:results){
            values.add(i.ONTAP__SalesOffId__c);
        }
		return values;
    }
    //Method to update list of Sales Office depending the Sales Org selected
    @AuraEnabled
    public static List<String> getSalesOffOnChange(String org){
        List<SalesArea__c > results= (org=='--')? [ SELECT Id,Id_External__c,SPART__c,VKBUR__c,VKORG__c,VTWEG__c FROM SalesArea__c ] : [ SELECT Id,Id_External__c,SPART__c,VKBUR__c,VKORG__c,VTWEG__c FROM SalesArea__c WHERE VKORG__c=:org];
        List<String> values = new List<String>();
        Set<String> setHelper = new Set<String>();
        values.add('--');
        for(SalesArea__c  i:results){
            setHelper.add(i.VKBUR__c);
        }
        for(String iter: setHelper){
            values.add(iter);
        }
		return values;
    }
	//Update Total records that the Batch will process
    @AuraEnabled
    public static integer updateTotal( String[] KTOKD, String VKORG, String VKBUR, String SPART, String VTWEG){
        String query = 'SELECT COUNT() FROM MDM_Temp_Account__c WHERE MDM_Control_Check__c= true ' ;
        
        query= (KTOKD.isEmpty()) ?query :query+'AND KTOKD__c IN: KTOKD';
        query= (VKORG=='--'||VKORG==null) ?query :query+' AND VKORG__c= \''+VKORG +'\'';
        query= (VKBUR=='--'||VKBUR==null) ?query :query+' AND VKBUR__c= \''+VKBUR +'\'';
        query= (SPART=='--'||SPART==null) ?query :query+' AND SPART__c= \''+SPART +'\'';
        query= (VTWEG=='--'||VTWEG==null) ?query :query+' AND VTWEG__c= \''+VTWEG +'\'';
        integer lst = Database.countQuery(query);
            return lst;
    }
    @AuraEnabled
    public static List<String> getSalesOrg(){
        List<Account> results= [ SELECT Id, Name,ONTAP__SalesOgId__c, ONTAP__SAPCustomerId__c FROM Account WHERE RecordType.Name=:Label.CDM_Apex_SalesOrg AND ONTAP__SAPCustomerId__c!=null ORDER BY ONTAP__SalesOgId__c];
        List<String> values = new List<String>();
        values.add('--');
        for(Account i:results){
            if(i.ONTAP__SAPCustomerId__c.substring(0,2)==Label.CDM_Apex_31){
                values.add(i.ONTAP__SAPCustomerId__c);
            }
            
        }
		return values;
    }
    @AuraEnabled
    public static List<String> getSector(){
        List<Sector__c> results= [ SELECT Id, Name,Code__c FROM Sector__c  ORDER BY Code__c];
        List<String> values = new List<String>();
        values.add('--');
        for(Sector__c i: results){
            values.add(i.Code__c);
        }
		return values;
    }
    @AuraEnabled
    public static List<String> getChannel(){
           List<DistributionChannel__c> results= [ SELECT Id, Name,Code__c FROM DistributionChannel__c  ORDER BY Code__c];
        List<String> values = new List<String>();
        values.add('--');
        for(DistributionChannel__c i: results){
            values.add(i.Code__c);
        }
		return values;
    }
    @AuraEnabled
    public static List<String> getKTOKD(){
        
		Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe(); 
		Map<String, Schema.SobjectField> MDMTempAccountFields = sObjectTypeMap.get(Label.CDM_Apex_MDM_Account_c).getDescribe().fields.getMap(); 
		Schema.DescribeFieldResult sb = MDMTempAccountFields.get(Label.CDM_Apex_KTOKD_c).getDescribe();
		List<String> picklistValue = new List<String>();picklistValue.add('--');
		for(Schema.PicklistEntry f : sb.getPicklistValues()){
            picklistValue.add(f.getValue());
        }
        
		return picklistValue;
    }
    // Method for get Picklist Values for Multipicklist
    @AuraEnabled
    public static List<Sector__c> getExpenseDetails(){
		Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe(); 
		Map<String, Schema.SobjectField> MDMTempAccountFields = sObjectTypeMap.get(Label.CDM_Apex_MDM_Account_c).getDescribe().fields.getMap(); 
		Schema.DescribeFieldResult sb = MDMTempAccountFields.get(Label.CDM_Apex_KTOKD_c).getDescribe();
		List<Sector__c> picklistValue = new List<Sector__c>();
		for(Schema.PicklistEntry f : sb.getPicklistValues()){
            
            Sector__c mdm = new Sector__c(Name= f.getValue());
            picklistValue.add(mdm);
        }
        
		return picklistValue;

    }
    @AuraEnabled
    public static void changeStatus (String code, Boolean status){
        MDM_Rule__c rule = [SELECT Id, MDM_Code__c, MDM_IsActive__c FROM MDM_Rule__c WHERE  MDM_Code__c=:code];
        rule.MDM_IsActive__c = status;
        update rule;
    }
}