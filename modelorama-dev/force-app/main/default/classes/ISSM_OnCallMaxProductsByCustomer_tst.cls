/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_OnCallMaxProductsByCustomer_tst {
    static Account ObjAcc = new Account();
    static ONTAP__Product__c ObjProduct2 = new ONTAP__Product__c();
    static List<ONTAP__Product__c> lstObjProduct2 = new List<ONTAP__Product__c>();
    
    @isTest static void tesOnCallMaxProductsByCustomer(){
        ISSM_OnCallMaxProductsByCustomer_cls objOncallMaxProducts = new ISSM_OnCallMaxProductsByCustomer_cls();
         Id recTypeIdAcc = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        
        ObjAcc.Name                    = 'Account 1';
        ObjAcc.ISSM_CreditLimit__c     = 1000;
        ObjAcc.ONTAP__SAP_Number__c    = '0123456789';
        ObjAcc.RecordTypeId            = recTypeIdAcc;
        ObjAcc.ONTAP__Credit_Amount__c = 10;
        ObjAcc.ONCALL__OnCall_Route_Code__c = 'FG0050';
        insert ObjAcc;
        
        ObjProduct2 = new ONTAP__Product__c(
            //RecordTypeId              = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
            
        );
        lstObjProduct2.add(ObjProduct2);
        insert lstObjProduct2;
        
        test.startTest();
            objOncallMaxProducts.getMaxBySKUByCustomer(ObjAcc.Id,lstObjProduct2);
        test.stopTest();
     }
}