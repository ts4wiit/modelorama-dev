/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_CallBatch_tst {
	static Account dataAccount;
	static String RecordTypeAccountId;
    static String devRecordTypeId;
	
	static testMethod void testCallBatch(){
		try{
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
            devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
		} catch(NullPointerException e){
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
        	devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();

		}
		dataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
    	/*************************************************************/
    	ApexPages.currentPage().getParameters().put('Id', dataAccount.Id);

        ONTAP__Order__c Objorder = new ONTAP__Order__c();
        Objorder.ONCALL__Origin__c = 'Tapwiser';
        Objorder.ONTAP__OrderAccount__c = dataAccount.Id;
        Objorder.RecordTypeId = devRecordTypeId;
        insert Objorder;
        /*************************************************************/    
            
        ApexPages.StandardController objAccount = new ApexPages.StandardController(dataAccount);
    	Test.startTest();
    	ISSM_CallBatch_ctr objAccountCtr = new ISSM_CallBatch_ctr(objAccount);
    	update dataAccount;
        //objAccountCtr.autoRun();
    	objAccountCtr.syncObjects();
        objAccountCtr.syncOrderItemsOnly();
    	Test.stopTest();
    }
	
}