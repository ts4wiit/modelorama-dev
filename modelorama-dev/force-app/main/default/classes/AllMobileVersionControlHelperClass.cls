/**
 * This class serves as helper class for AllMobileVersionControlTrigger trigger.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileVersionControlHelperClass {

	/**
	 * This method initiates the insertion of VersionControl object.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 */
	public static void initiateInsertVersionControl(List<AllMobileVersionControl__c> lstAllMobileVersionControl) {

		//Inner variables.
		Set<Id> setIdRecordTypesFromListVersionControl = new Set<Id>();
		List<RecordType> lstRecordTypeFromSetIdRecordType = new List<RecordType>();

		//Initiate logic.
		setIdRecordTypesFromListVersionControl = getSetIdRecordTypesFromListVersionControl(lstAllMobileVersionControl);
		lstRecordTypeFromSetIdRecordType = getRecordTypeListFromSetIdRecordTypes(setIdRecordTypesFromListVersionControl);
		filterRecordTypeAndCreateRouteAppVersion(lstAllMobileVersionControl, lstRecordTypeFromSetIdRecordType);
	}

	/**
	 * This method initiates the insertion of RouteAppVersion object according on RecordTpye: Route, SalesOffice, SalesOrg.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 * @param lstRecordType	List<RecordType>
	 */
	public static void filterRecordTypeAndCreateRouteAppVersion(List<AllMobileVersionControl__c> lstAllMobileVersionControl, List<RecordType> lstRecordType) {

		//Inner variables.
		Integer intI = 0;
		RecordType objRecordType;
		AllMobileRouteAppVersion__c objAllMobileRouteAppVersion = new AllMobileRouteAppVersion__c();
		Set<Id> setIdOfficeFromSetIdOrg = new Set<Id>();
		Set<Id> setIdOrgFromObjVersionControl = new Set<Id>();
		Set<Id> setIdOfficeFromObjVersionControl = new Set<Id>();
		Set<Id> setIdRouteFromObjVersionControl = new Set<Id>();
		List<String> lstRouteAppVersionIdAll = new List<String>();
		List<ONTAP__Route__c> lstRouteFromSetId = new List<ONTAP__Route__c>();
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion = new List<AllMobileRouteAppVersion__c>();
		List<AllMobileRouteAppVersion__c> lstRouteAppVersionFromSetIdRouteTempToInsert = new List<AllMobileRouteAppVersion__c>();
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToInsert = new List<AllMobileRouteAppVersion__c>();


		//Filter by RecordType from AllMobileVersionControl object.
		for(AllMobileVersionControl__c objAllMobileVersionControl : lstAllMobileVersionControl) {

			//Set variables to new.
			setIdOrgFromObjVersionControl = new Set<Id>();
			setIdOfficeFromSetIdOrg = new Set<Id>();
			setIdRouteFromObjVersionControl = new Set<Id>();
			lstRouteFromSetId = new List<ONTAP__Route__c>();
			lstRouteAppVersionIdAll = new List<String>();
			lstRouteAppVersionFromSetIdRouteTempToInsert = new List<AllMobileRouteAppVersion__c>();
			lstAllMobileRouteAppVersionToInsert = new List<AllMobileRouteAppVersion__c>();
			objRecordType = lstRecordType[intI];

			//Insertion logic for SalesOrg.
			if(objRecordType.DeveloperName == AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_ORG) {
				setIdOrgFromObjVersionControl = getSetIdOrgFromObjVersionControl(objAllMobileVersionControl);
				setIdOfficeFromSetIdOrg = getSetIdOfficeFromSetIdOrg(setIdOrgFromObjVersionControl);
				lstRouteFromSetId = getListRouteFromSetIdOffice(setIdOfficeFromSetIdOrg, objAllMobileVersionControl.AllMobileApplicationLK__c);

				//Insertion logic for SalesOffice.
			} else if(objRecordType.DeveloperName == AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_OFFICE) {
				setIdOfficeFromObjVersionControl = getSetIdOfficeFromObjVersionControl(objAllMobileVersionControl);
				lstRouteFromSetId = getListRouteFromSetIdOffice(setIdOfficeFromObjVersionControl, objAllMobileVersionControl.AllMobileApplicationLK__c);

				//Insertion logic for Route.
			} else if(objRecordType.DeveloperName == AllMobileStaticVariablesClass.STRING_RECORD_TYPE_DEVELOPER_NAME_ALLMOBILE_ROUTE) {
				setIdRouteFromObjVersionControl = getSetIdRoutesFromObjVersionControl(objAllMobileVersionControl);
				lstRouteFromSetId = getListRouteFromSetIdRoute(setIdRouteFromObjVersionControl, objAllMobileVersionControl.AllMobileApplicationLK__c);
			}
			lstRouteAppVersionFromSetIdRouteTempToInsert = generateListRouteAppVersionFromListRoute(lstRouteFromSetId, objAllMobileVersionControl);
			lstRouteAppVersionIdAll = generateListRouteAppVersionIdAll();
			lstAllMobileRouteAppVersionToInsert = compareListRouteAppVersionAndGenerateListRouteAppVersionToInsert(lstRouteAppVersionFromSetIdRouteTempToInsert, lstRouteAppVersionIdAll);
			insert lstAllMobileRouteAppVersionToInsert;
			intI++;
		}
	}

	/**
	 * This method returns a Set of RecordTypes Ids.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 * @return Set<Id>
	 */
	public static Set<Id> getSetIdRecordTypesFromListVersionControl(List<AllMobileVersionControl__c> lstAllMobileVersionControl) {
		Set<Id> setIdRecordTypes = new Set<Id>();
		for(AllMobileVersionControl__c objAllMobileVersionControl : lstAllMobileVersionControl) {
			setIdRecordTypes.add(objAllMobileVersionControl.RecordTypeId);
		}
		return setIdRecordTypes;
	}

	/**
	 * This method returns a List of RecordTypes.
	 *
	 * @param setIdRecordTypes	Set<Id>
	 * @return List<RecordType>
	 */
	public static List<RecordType> getRecordTypeListFromSetIdRecordTypes(Set<Id> setIdRecordTypes) {
		List<RecordType> lstRecordType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE Id =: setIdRecordTypes];
		return lstRecordType;
	}

	/**
	 * This method returns a Set of Org Ids.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 * @return Set<Id>
	 */
	public static Set<Id> getSetIdOrgFromObjVersionControl(AllMobileVersionControl__c objAllMobileVersionControl) {
		Set<Id> setIdOrgFromObjVersionControl = new Set<Id>();
		setIdOrgFromObjVersionControl.add(objAllMobileVersionControl.AllMobileSalesOrgLK__c);
		return setIdOrgFromObjVersionControl;
	}

	/**
	 * This method returns a Set of Office Ids.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 * @return Set<Id>
	 */
	public static Set<Id> getSetIdOfficeFromObjVersionControl(AllMobileVersionControl__c objAllMobileVersionControl) {
		Set<Id> setIdOfficeFromObjVersionControl = new Set<Id>();
		setIdOfficeFromObjVersionControl.add(objAllMobileVersionControl.AllMobileSalesOfficeLK__c);
		return setIdOfficeFromObjVersionControl;
	}

	/**
	 * This method returns a Set of Routes Ids.
	 *
	 * @param lstAllMobileVersionControl	List<AllMobileVersionControl__c>
	 * @return Set<Id>
	 */
	public static Set<Id> getSetIdRoutesFromObjVersionControl(AllMobileVersionControl__c objAllMobileVersionControl) {
		Set<Id> setIdRoutesFromObjVersionControl = new Set<Id>();
		setIdRoutesFromObjVersionControl.add(objAllMobileVersionControl.AllMobileRouteLK__c);
		return setIdRoutesFromObjVersionControl;
	}

	/**
	 * This method returns a Set of Office Ids from a Set of Org Ids.
	 *
	 * @param setIdOrg	Set<Id>
	 * @return Set<Id>
	 */
	public static Set<Id> getSetIdOfficeFromSetIdOrg(Set<Id> setIdOrg) {
		Set<Id> setIdOfficeFromSetIdOrg = new Set<Id>();
		List<Account> lstAccountOfficeFromSetIdOrg = [SELECT Id, Name, RecordType.Name FROM Account WHERE ISSM_SalesOrg__c =: setIdOrg];
		for(Account objAccount : lstAccountOfficeFromSetIdOrg) {
			setIdOfficeFromSetIdOrg.add(objAccount.Id);
		}
		return setIdOfficeFromSetIdOrg;
	}

	/**
	 * This method returns a List of Routes from a Set of Office Ids filtering by an Application.
	 *
	 * @param setIdOffice	Set<Id>
	 * @return List<ONTAP__Route__c>
	 */
	public static List<ONTAP__Route__c> getListRouteFromSetIdOffice(Set<Id> setIdOffice, Id IdApplication) {
		List<ONTAP__Route__c> lstOnTapRouteFromSetIdOffice = [SELECT Id, Name, ONTAP__SalesOffice__c, ONTAP__RouteId__c, ONTAP__RouteName__c, AllMobileApplicationNameLK__c FROM ONTAP__Route__c WHERE ONTAP__SalesOffice__c =: setIdOffice AND AllMobileApplicationNameLK__c =: IdApplication];
		return lstOnTapRouteFromSetIdOffice;
	}

	/**
	 * This method returns a List of Routes from a Set of Route Ids filtering by Application of the Version Control.
	 *
	 * @param setIdRoute	Set<Id>
	 * @return List<ONTAP__Route__c>
	 */
	public static List<ONTAP__Route__c> getListRouteFromSetIdRoute(Set<Id> setIdRoute, Id IdApplication) {
		List<ONTAP__Route__c> lstOnTapRouteFromSetIdRoute = [SELECT Id, Name, ONTAP__SalesOffice__c, ONTAP__RouteId__c, ONTAP__RouteName__c, AllMobileApplicationNameLK__c FROM ONTAP__Route__c WHERE Id =: setIdRoute AND AllMobileApplicationNameLK__c =: IdApplication];
		return lstOnTapRouteFromSetIdRoute;
	}

	/**
	 * This method returns a List of RouteAppVersion objects from a List of Routes and VersionControl object.
	 *
	 * @param lstRoute	List<ONTAP__Route__c>
	 * @param objAllMobileVersionControl	AllMobileVersionControl__c
	 * @return List<AllMobileRouteAppVersion__c>
	 */
	public static List<AllMobileRouteAppVersion__c> generateListRouteAppVersionFromListRoute(List<ONTAP__Route__c> lstRoute, AllMobileVersionControl__c objAllMobileVersionControl) {

		//Inner variables.
		Integer intI = 0;
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersion = new List<AllMobileRouteAppVersion__c>();
		AllMobileRouteAppVersion__c objAllMobileRouteAppVersion = new AllMobileRouteAppVersion__c();

		//Generation of AllMobileRouteAppVersion object.
		for(ONTAP__Route__c objRoute : lstRoute) {
			objAllMobileRouteAppVersion = new AllMobileRouteAppVersion__c();
			objAllMobileRouteAppVersion.AllMobileRouteLK__c = objRoute.Id;
			objAllMobileRouteAppVersion.AllMobileRouteName__c = objRoute.ONTAP__RouteName__c;
			objAllMobileRouteAppVersion.AllMobileRouteId__c = objRoute.ONTAP__RouteId__c;
			objAllMobileRouteAppVersion.AllMobileApplicationLK__c = objAllMobileVersionControl.AllMobileApplicationLK__c;
			objAllMobileRouteAppVersion.AllMobileVersionLK__c = objAllMobileVersionControl.AllMobileVersionLK__c;
			objAllMobileRouteAppVersion.AllMobileVersionId__c = objAllMobileVersionControl.AllMobileVersionId__c;
			objAllMobileRouteAppVersion.AllMobileApplicationId__c = Decimal.valueOf(objAllMobileVersionControl.AllMobileApplicationId__c);
			objAllMobileRouteAppVersion.AllMobileValidStartDate__c = objAllMobileVersionControl.AllMobileValidStartDate__c;
			objAllMobileRouteAppVersion.AllMobileValidEndDate__c = objAllMobileVersionControl.AllMobileValidEndDate__c;
			objAllMobileRouteAppVersion.AllMobileSoftDeleteFlag__c = objAllMobileVersionControl.AllMobileSoftDeleteFlag__c;
			objAllMobileRouteAppVersion.AllMobileRouteAppVersionId__c = objAllMobileRouteAppVersion.AllMobileRouteId__c + AllMobileStaticVariablesClass.STRING_SYMBOL_HYPHEN + objAllMobileRouteAppVersion.AllMobileVersionId__c;
			lstAllMobileRouteAppVersion.add(objAllMobileRouteAppVersion);
		}
		return lstAllMobileRouteAppVersion;
	}

	/**
	 * This method returns a List of all RouteAppVersion object.
	 *
	 * @return List<AllMobileRouteAppVersion__c>
	 */
	public static List<AllMobileRouteAppVersion__c> generateListRouteAppVersionAll() {
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionAll = [SELECT Id, Name, AllMobileRouteAppVersionId__c FROM AllMobileRouteAppVersion__c];
		return lstAllMobileRouteAppVersionAll;
	}

	/**
	 * This method returns a List of all RouteAppVersionId.
	 *
	 * @return List<String>
	 */
	public static List<String> generateListRouteAppVersionIdAll() {

		//Inner variables.
		List<String> lstRouteAppVersionId = new List<String>();
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionAll = generateListRouteAppVersionAll();

		//Generation of List of RouteAppVersionId.
		for(AllMobileRouteAppVersion__c objAllMobileRouteAppVersion : lstAllMobileRouteAppVersionAll) {
			lstRouteAppVersionId.add(objAllMobileRouteAppVersion.AllMobileRouteAppVersionId__c);
		}
		return lstRouteAppVersionId;
	}

	/**
	 * This method returns a List of all RouteAppVersion objects without duplicates to be inserted.
	 *
	 * @param lstAllMobileRouteAppVersionTempToInsert 	List<AllMobileRouteAppVersion__c>
	 * @param lstRouteAppVersionIdAll 	List<String>
	 * @return List<AllMobileRouteAppVersion__c>
	 */
	public static List<AllMobileRouteAppVersion__c> compareListRouteAppVersionAndGenerateListRouteAppVersionToInsert(List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionTempToInsert, List<String> lstRouteAppVersionIdAll) {

		//Inner variables.
		Boolean blnIsRouteAppVersionIdInlstAllMobileRouteAppVersionAll = FALSE;
		List<AllMobileRouteAppVersion__c> lstAllMobileRouteAppVersionToInsertFinal = new List<AllMobileRouteAppVersion__c>();
		if(!lstRouteAppVersionIdAll.isEmpty() && lstRouteAppVersionIdAll != null) {

			//Generate new list without duplicates of RouteAppVersionId.
			for(AllMobileRouteAppVersion__c objListAllMobileRouteAppVersionTempToInsert : lstAllMobileRouteAppVersionTempToInsert) {
				blnIsRouteAppVersionIdInlstAllMobileRouteAppVersionAll = lstRouteAppVersionIdAll.contains(objListAllMobileRouteAppVersionTempToInsert.AllMobileRouteAppVersionId__c);
				if(blnIsRouteAppVersionIdInlstAllMobileRouteAppVersionAll == FALSE) {
					lstAllMobileRouteAppVersionToInsertFinal.add(objListAllMobileRouteAppVersionTempToInsert);
				}
			}
			return lstAllMobileRouteAppVersionToInsertFinal;
		} else {
			return lstAllMobileRouteAppVersionTempToInsert;
		}
	}
}