/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventOperations_tst {

    static testMethod void testEventOperationsInsert() {
		EventOperations_tst.insertCustomSetting();
    	
    	// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_tst());
        
        EventOperations_cls.sendEventsToHeroku();
    	
    	// Sales Office Account
        Account salesOfficeAccount = TestUtils_tst.generateAccount(TestUtils_tst.RECORD_TYPE_ACCOUNT_SALESOFFICE, null, 12345, null);
        insert salesOfficeAccount;

        // Customer Account
        Account customerAccount = TestUtils_tst.generateAccount(null, null, 123, salesOfficeAccount.Id);
        insert customerAccount;
        
        ONTAP__Route__c objRoute = TestUtils_tst.generateFullRoute();
        insert objRoute;
        
        list<Boolean> frecuencies = new list<Boolean>{true, true, true, true, true, true, true};
        VisitPlan__c objVisitPlan = TestUtils_tst.generateVisitPlan(objRoute.Id, Date.today().addDays(-30), Date.today().addDays(30), frecuencies, 'ABCDEF');
        insert objVisitPlan;   
        
        AccountByVisitPlan__c objAccByVP = TestUtils_tst.generateAccountByVisitPlan(objVisitPlan.Id, customerAccount.Id, '1', frecuencies);
        insert objAccByVP;
        
        objVisitPlan = [Select Id, VisitPlanType__c From VisitPlan__c Where Id = :objVisitPlan.Id];
        
        ONTAP__Tour__c objTour = TestUtils_tst.generateTour(objVisitPlan, objRoute);
        objTour.ONTAP__IsActive__c = true;
        insert objTour;
        
        objTour = [Select Id, Route__r.RouteManager__c, Route__r.OwnerId From ONTAP__Tour__c Where Id = :objTour.Id];
        
        System.debug('\nManager==>'+objTour.Route__r.RouteManager__c);
        
        list<Event> lstEvents = TestUtils_tst.generateEvents(10, objAccByVP, objTour);
        
        Test.startTest();
        	insert lstEvents;
        	
        	lstEvents[0].OrderDetailText__c = '1234-abc-A&5678-def-B';
        	lstEvents[1].OrderDetailText__c = '1234-abc-A&5678-def-B';
        	
        	update lstEvents;
        	
        	lstEvents[0].OrderDetailText__c = '1234-abc-A&5672348-defgh-B';
        	update lstEvents[0];
        	
        	delete lstEvents[5];
        Test.stopTest();
    }
    
    
    public static void insertCustomSetting(){
   		SyncHerokuParams__c objConf = new SyncHerokuParams__c();
        objConf.Name = 'SyncToursEvents';
        objConf.StartTime__c = 0;
        objConf.EndTime__c = 24;
        objConf.IsActive__c = true;
        objConf.LastModifyDate__c = DateTime.now().addDays(-30);
        objConf.RecordTypeIds__c = 'Presales';
        objConf.RunFrequency__c = 60;
        insert objConf;
        
        list<ISSM_PriceEngineConfigWS__c> lstConf = new list<ISSM_PriceEngineConfigWS__c>();
        ISSM_PriceEngineConfigWS__c objConf1 = new ISSM_PriceEngineConfigWS__c();
        objConf1.Name = 'DeleteHerokuTours';
        objConf1.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf1.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deletetours';
        objConf1.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf1.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf1);
        
        ISSM_PriceEngineConfigWS__c objConf2 = new ISSM_PriceEngineConfigWS__c();
        objConf2.Name = 'DeleteHerokuEvents';
        objConf2.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf2.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/deleteevents';
        objConf2.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf2.ISSM_Method__c = 'DELETE';
        lstConf.add(objConf2);
        
        ISSM_PriceEngineConfigWS__c objConf3 = new ISSM_PriceEngineConfigWS__c();
        objConf3.Name = 'GetHerokuTours';
        objConf3.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf3.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/gettours';
        objConf3.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf3.ISSM_Method__c = 'POST';
        lstConf.add(objConf3);
        
        ISSM_PriceEngineConfigWS__c objConf4 = new ISSM_PriceEngineConfigWS__c();
        objConf4.Name = 'GetHerokuEvents';
        objConf4.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf4.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/getevents';
        objConf4.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf4.ISSM_Method__c = 'POST';
        lstConf.add(objConf4);
        
        ISSM_PriceEngineConfigWS__c objConf5 = new ISSM_PriceEngineConfigWS__c();
        objConf5.Name = 'InsertHerokuTours';
        objConf5.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf5.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/inserttours';
        objConf5.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf5.ISSM_Method__c = 'POST';
        lstConf.add(objConf5);
        
        ISSM_PriceEngineConfigWS__c objConf6 = new ISSM_PriceEngineConfigWS__c();
        objConf6.Name = 'InsertHerokuEvents';
        objConf6.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf6.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/insertevents';
        objConf6.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf6.ISSM_Method__c = 'POST';
        lstConf.add(objConf6);
        
        ISSM_PriceEngineConfigWS__c objConf7 = new ISSM_PriceEngineConfigWS__c();
        objConf7.Name = 'UpdateHerokuTours';
        objConf7.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf7.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updatetours';
        objConf7.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf7.ISSM_Method__c = 'PUT';
        lstConf.add(objConf7);
        
        ISSM_PriceEngineConfigWS__c objConf8 = new ISSM_PriceEngineConfigWS__c();
        objConf8.Name = 'UpdateHerokuEvents';
        objConf8.ISSM_AccessToken__c = 'BASIC VjVjSjdUQmQ4aFZaMUZVaGFZUVN2ZTJRbTd4a1Y5TkVZUEtRdkNkYTpQMFR6WVhUc0N4WE9EOWdPaTgydUg3Tnk0a09kMXdBc0FTaGFiZDZP';
        objConf8.ISSM_EndPoint__c = 'https://mex-issm-int-api-dev.herokuapp.com/api/v1/updateevents';
        objConf8.ISSM_HeaderContentType__c = 'application/json; charset=UTF-8	';
        objConf8.ISSM_Method__c = 'PUT';
        lstConf.add(objConf8);
        
        insert lstConf;
   	}
}