/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
	Test Class for ISSM_DeserializePricingMotor_cls

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       20/06/2017	  Luis Licona                  CLass created
    ================================================================================================
****************************************************************************************************/
@isTest
private class ISSM_DeserializePricingMotor_tst {
    
    static testMethod void testParse() {
        String json = '{'+
        '  \"Token\" : \"4098237329980327982310483277032840982740717239847230473870317093\",'+
        '  \"organizacionVentas\" : \"3116\",'+
        '  \"orderItem\" : [ {'+
        '    \"quantity\" : 43,'+
        '    \"materialNumber\" : \"3000020\"'+
        '  }, {'+
        '    \"quantity\" : 32,'+
        '    \"materialNumber\" : \"3000543\"'+
        '  } ],'+
        '  \"customerNumber\" : \"100153987\",'+
        '  \"canalDistribucion\" : \"1\"'+
        '}';
        ISSM_DeserializePricingMotor_cls obj = ISSM_DeserializePricingMotor_cls.parse(json);
        System.assert(obj != null);
    }
}