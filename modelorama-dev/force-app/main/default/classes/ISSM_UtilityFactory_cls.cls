/******************************************************************************** 
    Company:            Avanxo México
    Author:             Luis Licona
    Customer:           AbInbev - Trade Revenue Management
    Descripción:        Class that allows searches for filling relationships and filling information in 
                        wrapper classes

    Information about changes (versions)
    ===============================================================================
    No.    Date             Author                      Description
    1.0    17-April-2018    Luis Licona                 Creation
    ******************************************************************************* */
public with sharing class ISSM_UtilityFactory_cls {

    /**
    * @description  Method that executes the query
    * @param        strQuery    Contains the sentence to execute
    * @return       Return a sObject[] with All Values of return Database.query
    */
    public static sObject[] executeQuery(String strQuery){
        sObject[] lstReturn   = new List<sObject>();
        sObject[] lstOfRecords = Database.query(strQuery);
        for (sObject obj: lstOfRecords) {
            lstReturn.add(obj);
        }
        System.debug('lstReturn--> '+lstReturn);
        return lstReturn;
    }

    /**
    * @description: Method that generates a list of strings from a sObject
    * @param        itemList        List of sObject records
    * @param        strFieldName    Field from which the list will be generated (Id, Name ...)
    * @return       Return a String[] with All Values of PickList
    */
    public static String[] createLstItems(sObject[] itemList,String strFieldName){
    	String[] lstExcludeitems    = new List<String>();
        String StrExcludeitems      = '';
        for(sObject item : itemList ){
            String StrParam = '\'' + item.get(strFieldName) + '\'';
            lstExcludeitems.add(StrParam);
        }
        
        return lstExcludeitems;
    }

    /**
    * @description: Method that allows you to retrieve the values ​​of a Selection List field
    * @param        objObject   Object where the selection list is
    * @param        strFld      Field of type selection list
    * @return       Return a String[] with All Values of PickList
    */
    public static String[] getSelectOptions(sObject objObject, String strFld) {
        List<PickListValues> lstPickListValues = new List<PickListValues>();
        Schema.sObjectType objType = objObject.getSObjectType();// Get the object type of the SObject.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();// Describe the SObject using its object type.
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();// Get a map of fields for the SObject
        // Get the list of picklist values for this field.
        Schema.PicklistEntry[] values = fieldMap.get(strFld).getDescribe().getPickListValues();
        // Add these values to the selectoption list.
        for(Schema.PicklistEntry a: values) {
            PickListValues pickListValues = new PickListValues();
            pickListValues.value = a.getValue();
            pickListValues.label = a.getLabel();
            lstPickListValues.add(pickListValues);
        }
        
        String[] JsonMsg  = new List<String>();
        JsonMsg.add(JSON.serialize(lstPickListValues));
        System.debug('JsonMsg ---->' + JsonMsg);

        return JsonMsg;
    }
    
    /**
    * @description: Get RecordTypeId by DeveloperName
    * @param        StrObject           SobjectType of RecordType
    * @param        DeveloperName       API Name from RecordType
    * @return       Return a String with Id of RecordType
    */
    public static String getRecordTypeIdByDeveloperName(String objectType, String developerName){
        String recordTypeId = Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosByDeveloperName().get(developerName).getRecordTypeId();
        return recordTypeId; 
    }

    /**
    * @description  Wrapper class for PickListValues
    */
    public class PickListValues{
        public String value;
        public String label;
    }

    /**
    * @description: Method to get a record by id, the Type of the object is specified in the parameter objectType.
    *               All standard and custom fields are retrieved.
    * @param    objectType      String with the Object API Name of the record to retrieve
    * @param    id              Salesforce Id of the record to retrieve
    * @return   Return a sObject record of the type specified by the parameter objectType
    */
    public static sObject getRecordById(String objectType, String id) {
        sObject record;
        List<sObject> resultList = new List<sObject>();
        if( id != null ){
            String queryString = 'SELECT ';
                    queryString += String.join(new List<String>(Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap().keySet()),', ');
                    queryString += ' FROM ' + objectType;
                    queryString += ' WHERE Id = \'' + id + '\'';
                    queryString += ' LIMIT 1';
            resultList = executeQuery(queryString);
        }
        if(resultList.size() == 1){
            record = resultList[0];
        }
        System.debug('#### return record:' + record);
        return record;
    }
}