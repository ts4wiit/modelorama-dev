/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ISSM_AccountEraseExternBch_tst {
	
		@testSetup
	private static void testSetup(){
		Id RecordTypeAccountId;
		try{
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
		} catch(NullPointerException e){
			RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta').getRecordTypeId();
		}
		// create Accounts
		List<Account> accounts_lst = new List<Account>{	};
		for(Integer i = 0;i<10;i++){
			accounts_lst.add(new Account(Name='Test Account '+i,ISSM_MainContactA__c=true,RecordTypeId = RecordTypeAccountId,ONTAP__SAPCustomerId__c = i<10 ? '000000000' + i : '00000000' + i));
		}
		insert accounts_lst;
	}
	
	@isTest static void test_method_one() {
		List<ISSM_ObjectInterface_cls> sObjectInterfaceList = new List<ISSM_ObjectInterface_cls>();
		sObjectInterfaceList.add(new ISSM_Asset_cls());
		sObjectInterfaceList.add(new ISSM_EmptyBalance_cls());
		sObjectInterfaceList.add(new ISSM_OpenItem_cls());
		sObjectInterfaceList.add(new ISSM_Kpi_cls());
		sObjectInterfaceList.add(new ISSM_Order_cls());
		Database.executeBatch(new ISSM_AccountEraseExt_bch(sObjectInterfaceList), 80);
	}
	
}