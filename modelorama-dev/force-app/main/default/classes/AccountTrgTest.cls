/**
 * @Created by: Alfonso de la Cuadra Martinez (Everis)
 * @Description: Test class for Trigger class. Is important for this test have records on History field tracking 
 * 		on metadata object. This class just insert an account and allows the trigger execute MDRM_HistoryFieldTracking class.
 *		Module: Modeloramas
 */

@isTest(seeAllData=false)
public class AccountTrgTest {
    
    @testSetup static void test() {
        UserRole urDrv = new UserRole(Name='DRV Modeloramas Test');
        insert urDrv;
        UserRole urSup = new UserRole(Name='Supervisor Modeloramas Test', ParentRoleId=urDrv.Id);
        insert urSup;
        UserRole urBdr = new UserRole(Name='BDR Modeloramas Test', ParentRoleId=urSup.Id);
        insert urBdr;
        
        User uDrv = new User(
             ProfileId = [SELECT Id, Name, UserType FROM Profile WHERE PermissionsViewAllData = true LIMIT 1].Id,
             LastName = 'last',
             Email = urDrv.Id + '@modeloramas.mx.com',
             Username = urDrv.Id + '@modeloramas.mx.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = urDrv.Id
        );
        insert uDrv;
        
        User uBdr = new User(
            ProfileId = [SELECT Id, Name, UserType FROM Profile WHERE Name = 'System Administrator' OR Name = 'Administrador del sistema' LIMIT 1].Id,
            LastName = 'last',
            Email = urBdr.Id + '@modeloramas.mx.com',
            Username = urBdr.Id + '@modeloramas.mx.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = urBdr.Id,
            MDRM_Corporative_Approver__c = uDrv.Id,
            MDRM_Legal_Approver__c = uDrv.Id,
            MDRM_LiderGerenteApprover__c = uDrv.Id,
            MDRM_Procurement_Approver__c = uDrv.Id,
            MDRM_UEN_Approver__c = uDrv.Id,
            MDRM_SupervisorApprover__c = uDrv.Id,
            EmployeeNumber = '010101010101'
        );
        Insert uBdr;
        
        MDRM_ModeloramaHelperTest.createFullViewInfo(uBdr.Id);
    }

    @isTest static void testInsert() {
		Account acc = [SELECT Name FROM Account WHERE Name = 'Modelorama'];

		Test.startTest();
		List<MDRM_Modelorama_Status_Change__c> lchange = [SELECT Id, MDRM_FieldName__c 
														  FROM MDRM_Modelorama_Status_Change__c 
														  WHERE MDRM_Account_Status_Change__c =: acc.Id];
		System.assertNotEquals(null, lchange, 'History field tracking was not created');
		System.assertNotEquals(0, lchange.size(), 'History field tracking was not created');
		Test.stopTest();
	}
    
}