/**
 * This the schedule class for AllMobileApplicationBatchClass batch class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileApplicationSchedulerClass implements Schedulable {
	global void execute(SchedulableContext objSchedulableContext) {
		AllMobileApplicationBatchClass objAllMobileApplicationBatchClass = new AllMobileApplicationBatchClass();
		Database.executeBatch(objAllMobileApplicationBatchClass);
	}
}