/**
 * This class is the controller extension for the AllMobileDeviceUser visualforce page.
 * <p /><p />
 * @author Alberto Gómez
 */
public class AllMobileDeviceUserControllerExtension {

	//Variables.
	public AllMobileDeviceUser__c objAllMobileDeviceUser;
	public String strPassword {get; set;}

	/**
	 * Constructor
	 *
	 * @param objStandardController ApexPages.StandardController
	 */
	public AllMobileDeviceUserControllerExtension(ApexPages.StandardController objStandardController) {
		strPassword = AllMobileStaticVariablesClass.STRING_CHARACTER_BLANK;
		objAllMobileDeviceUser = (AllMobileDeviceUser__c)objStandardController.getRecord();
	}

	public void assignPassword() {
		if(String.isNotEmpty(strPassword)) {
			objAllMobileDeviceUser.AllMobileDeviceUserPasswordText__c = strPassword;
		}
	}
}