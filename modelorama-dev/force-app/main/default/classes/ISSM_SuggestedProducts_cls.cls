/****************************************************************************************************
    General Information
    -------------------
    author:     Hector Diaz
    email:      hdiaz@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Class to calculate the percentage of suggested products and push products

    Information about changes (versions)
    ================================================================================================
    Number    Date                  Author                       Description
    ------    -------------         --------------------------   -----------------------------------
    1.0       19-Octubre-2018       Hector Diaz                 Class Creation
    ================================================================================================
****************************************************************************************************/
public class ISSM_SuggestedProducts_cls {
	public ISSM_DeserializeSuggestedOrder_cls ObjDeserealized;
	
	public static String requestAutentication(){
		String Json_str = ''; 	
		String strAccessToken = '';	 
		Json_str = JSON.serializePretty(new WprRequestAutentication(Label.ISSM_UserForSuggested,Label.ISSM_PasswordForSuggested));
		System.debug('***Json_str : '+Json_str);
		try{
			if(String.isNotBlank(Json_str)){

				List<ISSM_CallOutService_ctr.WprResponseService>  lstWprSend  = new List<ISSM_CallOutService_ctr.WprResponseService>();
				ISSM_CallOutService_ctr objSenRequest = new ISSM_CallOutService_ctr();
				
				lstWprSend = objSenRequest.SendRequest(Json_str,Label.ISSM_ConfigWSAutorizationSuggested);
				System.debug(lstWprSend[0].strBodyService);
				
				if (lstWprSend[0].strBodyService != null) {
					Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(lstWprSend[0].strBodyService);
					strAccessToken = String.valueOf(responseMap.get('access_token'));
				}else{
					System.debug(logginglevel.INFO, 'No hay token de acceso');
					strAccessToken =  null;
				}
			}			
		}catch(Exception e){
			System.debug('Exception  : '+ e);
		}
		
		return strAccessToken;
	}
	
	public ISSM_DeserializeSuggestedOrder_cls requestSuggestedOrder(String idSapCustomer,Date startDate, Date finishDate){		
		
		
		System.debug('** idSapCustomer : '+idSapCustomer);
		System.debug('** startDate : '+startDate);
		System.debug('** finishDate : '+finishDate);

		String Json_str = ''; 		 
		Json_str = JSON.serializePretty(new WprRequestSuggested(idSapCustomer,startDate,finishDate));
		System.debug('JSON REQUEST  : '+Json_str);
		try{
			if(String.isNotBlank(Json_str)){

				List<ISSM_CallOutService_ctr.WprResponseService>  lstWprSend  = new List<ISSM_CallOutService_ctr.WprResponseService>();
				ISSM_CallOutService_ctr objSenRequest = new ISSM_CallOutService_ctr();
				lstWprSend = objSenRequest.SendRequest(Json_str,Label.ISSM_ConfigWSSuggestedOrder);
				System.debug('** lstWprSend : '+lstWprSend);

				ObjDeserealized = ISSM_DeserializeSuggestedOrder_cls.parse(lstWprSend[0].strBodyService);		
				System.debug('********ObjDeserealized ** ');	 
				System.debug(ObjDeserealized);	
			}			
		}catch(Exception e){
			System.debug('Exception  : '+ e);
		}
	
        return ObjDeserealized;		
	}
 
    public WprSuggestedCovered getSuggestedProds(Map<String,ONTAP__Product__c> prods_map,
                                                    ISSM_DeserializeSuggestedOrder_cls desSugOrd_obj, 
                                                    String type_Str){        

        Map<String,String> brandSizeSub_map = new Map<String,String>();

        System.debug(loggingLevel.Error, '*** type_Str: ' + type_Str);
        System.debug(loggingLevel.Error, '*** desSugOrd_obj: ' + desSugOrd_obj);
        if(type_Str == 'Suggested' && desSugOrd_obj.Replanishment != null && desSugOrd_obj.Substitute != null){
            for(ISSM_DeserializeSuggestedOrder_cls.Substitute substitute_obj : desSugOrd_obj.Substitute){
                if(prods_map.containsKey(substitute_obj.brandsubstitute2)){
                    brandSizeSub_map.put(substitute_obj.brand + '-' + substitute_obj.packagesize, substitute_obj.brandsubstitute2);
                }                
            }
        }

        System.debug('brandSizeSub_map = ' + brandSizeSub_map);

        List<ONTAP__Product__c> prodsToReturn_lst = new List<ONTAP__Product__c>();        
        if(type_Str == 'Suggested' && desSugOrd_obj.Replanishment != null){
            for(ISSM_DeserializeSuggestedOrder_cls.Replanishment replanishment_obj : desSugOrd_obj.Replanishment){
                if(prods_map.containsKey(replanishment_obj.sku)){
                    prods_map.get(replanishment_obj.sku).ISSM_IsSuggested__c        = 'S';
                    prods_map.get(replanishment_obj.sku).ISSM_Suggested__c          = String.valueOf(replanishment_obj.numboxes);
                    prods_map.get(replanishment_obj.sku).ISSM_Brand__c              = replanishment_obj.brand;
                    prods_map.get(replanishment_obj.sku).ISSM_Size__c               = replanishment_obj.packagesize;
                    prods_map.get(replanishment_obj.sku).ISSM_ActualSale__c         = String.valueOf(replanishment_obj.curentsale);
                    prods_map.get(replanishment_obj.sku).ONTAP__ProductShortName__c = String.valueOf(getCoveredPercentageBySKU(replanishment_obj.numboxes,replanishment_obj.curentsale));
                    prods_map.get(replanishment_obj.sku).ISSM_Container_Size__c     = brandSizeSub_map.containsKey(replanishment_obj.brand + '-' + replanishment_obj.packagesize) ? brandSizeSub_map.get(replanishment_obj.brand + '-' + replanishment_obj.packagesize) : 'NA';

                    System.debug('replanishment_obj = ' + replanishment_obj);

                    prodsToReturn_lst.add(prods_map.get(replanishment_obj.sku));
                }                
            }
            Decimal aver_Dec = getAllCoveredPercentage(prodsToReturn_lst);

            return new WprSuggestedCovered(prodsToReturn_lst,aver_Dec);
        }
        if(type_Str == 'Push' && desSugOrd_obj.CrossSelling != null){
            for(ISSM_DeserializeSuggestedOrder_cls.CrossSelling crossSelling_obj : desSugOrd_obj.CrossSelling){
                if(prods_map.containsKey(crossSelling_obj.sku)){
                    prods_map.get(crossSelling_obj.sku).ISSM_IsSuggested__c         = 'P';
                    prods_map.get(crossSelling_obj.sku).ISSM_Suggested__c           = String.valueOf(crossSelling_obj.numboxes);
                    prods_map.get(crossSelling_obj.sku).ISSM_Brand__c               = crossSelling_obj.brand;
                    prods_map.get(crossSelling_obj.sku).ISSM_Size__c                = crossSelling_obj.packagesize;
                    prods_map.get(crossSelling_obj.sku).ISSM_ActualSale__c          = String.valueOf(crossSelling_obj.curentsale);
                    prods_map.get(crossSelling_obj.sku).ONTAP__ProductShortName__c  = String.valueOf(getCoveredPercentageBySKU(crossSelling_obj.numboxes,crossSelling_obj.curentsale));                
                    
                    prodsToReturn_lst.add(prods_map.get(crossSelling_obj.sku));
                }                
            }
            Decimal aver_Dec = getAllCoveredPercentage(prodsToReturn_lst);

            return new WprSuggestedCovered(prodsToReturn_lst,aver_Dec);
        }

        return new WprSuggestedCovered(new List<ONTAP__Product__c>(), 0.0);
    }	

    public Decimal getCoveredPercentageBySKU(Integer boxes_Int, Integer currentBoxes_Int){
        return (boxes_Int != null && boxes_Int != 0) ? (Decimal.valueOf((currentBoxes_Int * 100)) / Decimal.valueOf(boxes_Int)) : 0.0;
    }

    public Decimal getAllCoveredPercentage(List<ONTAP__Product__c> prods_lst){
        Decimal average_Dec = 0.0;
        for(ONTAP__Product__c prod_Obj : prods_lst){
            average_Dec += prod_Obj.ISSM_IsSuggested__c == 'S' || prod_Obj.ISSM_IsSuggested__c == 'P' ? Decimal.valueOf(prod_Obj.ONTAP__ProductShortName__c) : 0.0;
        }
        return prods_lst.size() != 0 && prods_lst != null ? average_Dec.divide(Decimal.valueOf(prods_lst.size()), 2) : 0.0;
    }
	
	public class WprRequestSuggested {
    	String numClient = '';
    	Date startDate = null;  
    	Date endDate =  null;
    	
    	public WprRequestSuggested(String numClient,Date startDate, Date endDate ){
        	this.numClient = numClient; 
        	this.startDate = startDate;
        	this.endDate = endDate;
    	}
    }
    
    public class WprSuggestedCovered{
        public List<ONTAP__Product__c> products_lst;
        public Decimal covered_Dec;

        public WprSuggestedCovered(List<ONTAP__Product__c> products_lst, 
                                    Decimal covered_Dec){
            this.products_lst = products_lst;
            this.covered_Dec = covered_Dec;
        }
    }
    
    public class WprRequestAutentication {
    	String username = '';
    	String password = '';
    
    	
    	public WprRequestAutentication(String username ,String password ){
        	this.username = username; 
        	this.password = password;
    	}
    }
	
}