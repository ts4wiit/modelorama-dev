/**
* This class is the schedule for the AllMobileRouteAppVersionBatchClass class.
* <p /><p />
* @author Alberto Gómez
*/
global class AllMobileRouteAppVersionSchedulerClass implements Schedulable {

	/**
	 * Execute AllMobileRouteAppVersionBatchClass class.
	 */
	global void execute(SchedulableContext objSchedulableContext) {
		AllMobileRouteAppVersionBatchClass objAllMobileRouteAppVersionBatchClass = new AllMobileRouteAppVersionBatchClass();
		Database.executeBatch(objAllMobileRouteAppVersionBatchClass);
	}
}