/**********************************************************************************************************
    General information
    -------------------
    author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    company:    Avanxo México
    Project:    Salesforce Implementation
    Customer:   Grupo Modelo

    Description:
    Controller to get the last 8 Order of a POC.

    Test Class: 
    ISSM_OrderList_tst

    Information about changes (versions)
    =======================================================================================================
    Number    Dates           Author                       Description                      
    ------    --------        --------------------------   ------------------------------   
    1.0       14-Junio-2017   Marco ZúñigasCretion of the  Apex class                       
    =======================================================================================================
**********************************************************************************************************/

public with sharing class ISSM_OrderList_ctr {
    public ONTAP__Order__c[] OrderList{get;set;}
    public ONCALL__Call__c OrderListItem{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL = new ISSM_OnCallQueries_cls();
    
    /*
        Metodo constructor
    */
    public ISSM_OrderList_ctr(ApexPages.StandardController controller){

        OrderListItem = CTRSOQL.getCallbyId(controller.getId());   
        Datetime Dt   = Datetime.parse(System.now().format());  
        OrderList     = CTRSOQL.getLastOrders(OrderListItem.ONCALL__POC__c,Dt);
    }
}