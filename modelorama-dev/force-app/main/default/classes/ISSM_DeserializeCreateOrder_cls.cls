/****************************************************************************************************
    Información general
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    Implementación Salesforce
    Customer:   Grupo Modelo

    Description:
    Controlador que deserealiza el response de creación de pedidos

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       11-Julio-2017   Luis Licona                  Creación de la Clase
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_DeserializeCreateOrder_cls {
	
	public String orderID;
	public List<Error> error;
    public String StrRequest;
    public Datetime ReqDate;
	
	public class Error {
		public String errorID;
		public String errorNumber;
		public String errorMessage;
	}
	
	public static ISSM_DeserializeCreateOrder_cls parse(String json) {
		return (ISSM_DeserializeCreateOrder_cls) System.JSON.deserialize(json, ISSM_DeserializeCreateOrder_cls.class);
	}
}