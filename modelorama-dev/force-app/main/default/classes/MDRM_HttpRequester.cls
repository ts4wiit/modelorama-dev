public with sharing class MDRM_HttpRequester {
    private String endpoint;
    private String httpMethod;
    private Map<String, String> headers;
    private String body;

    public MDRM_HttpRequester(String endpoint, String httpMethod, Map<String, String> headers, String body) {
        this.endpoint = endpoint;
        this.httpMethod = httpMethod;
        this.headers = headers;
        this.body = body;
    }

    public HttpResponse sendRequest() {

        HttpRequest request = new HttpRequest();
        request.setTimeout(120000);

        request.setEndpoint(this.endpoint);
        request.setMethod(this.httpMethod);
        
        System.debug(this.endpoint);
        System.debug(this.headers);

        for(String headerKey : this.headers.keySet()) {
            request.setHeader(headerKey, this.headers.get(headerKey));
        }

        if(this.body != null) {
            request.setBody(this.body);
        }

        Http http = new Http();
        return http.send(request);
    }
}