/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest 
private class ISSM_TypificationLevels_tst { 
    public static String strOptionSelectedLevel1 = '';
    public static PageReference acctPage;
    public static Account DataAccount;
    public static  List<ISSM_AppSetting_cs__c> AppSetting;
    public static Case caseObj;
    public static ISSM_TypificationMatrix__c  TypificationMatrix;
    public static ISSM_TypificationMatrix__c  TypificationMatrix3;
    public static ISSM_TypificationMatrix__c  TypificationMatrix4;
    public static String strAccountID;
    public static User user;
    public static QueueSObject queueCase;
    static AccountTeamMember accountTeamMember;
    public static Account salesOffice;
    public static Account salesOrg;
    public static Account drv;
    public static ISSM_TypificationMatrix__c  typificationMatrix2;
    public static Case DataCase;
    public static  List<ISSM_TriggerFactory__c> AppTriggerFactory;
    public static List<ISSM_IntervalsByCountry__c> intervalsByCountry;
    public static List<ISS_DocumentTypeOpenItems__c> DocumentTypeOpenItems;
    
    /*@testSetup static void setupQueue() {
    queueCase = ISSM_CreateDataTest_cls.fn_CreateQueue(true, 'Queue Test Cases');
    }*/
    
    static testMethod void TestTypificationLevel0() {  
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeAccountSalesOffice = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeSalesOrgAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();
        String RecordTypeDRVAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        String RecordTypeAccountIdProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        
        //Creamos proveedor sin correo 
        Account  objAccountProviderWithOutMail = new Account(); 
        objAccountProviderWithOutMail.Name = 'proveedor sin correo';
        objAccountProviderWithOutMail.ONTAP__Email__c = 'hdiaz@avanxo.com';
        objAccountProviderWithOutMail.RecordTypeId = RecordTypeAccountIdProvider;
        insert objAccountProviderWithOutMail;
        List<ISSM_AppSetting_cs__c> AppSettingProvider =  ISSM_CreateDataTest_cls.fn_CreateAppSettingProviderWit(true,objAccountProviderWithOutMail.Id);
        
        //Creamos Cuenta con SalesOffice
        Account  objAccountSalesOffice = new Account();
        objAccountSalesOffice.Name = 'Name SalesOffice';
        objAccountSalesOffice.RecordTypeId = RecordTypeAccountSalesOffice;
        insert objAccountSalesOffice;
        List<Account> accProvider  = ISSM_CreateDataTest_cls.fn_CreateAccountProvider (true,RecordTypeAccountIdProvider,objAccountSalesOffice.Id);
        
        /*Map<String,Id> mapIdQueue = new Map<String,Id>();
        List<Group> lstselectGroup3 =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        for(Group objgroupId : lstselectGroup3){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        //Creamos el usuario y la tipificacion para posterior insertarla en la configuracion personalizada
        User user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        ISSM_TypificationMatrix__c  TypificationMatrix1 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00002','Gestión Cobranza','Aclaracion','Pago no reconocido',null,null,null,'Trade Marketing',user.Id,null);    
        ISSM_TypificationMatrix__c  TypificationMatrix2 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00003','Gestión Cobranza','Aclaracion','Cargo no reconocido',null,null,null,'Trade Marketing',user.Id,null);   
        ISSM_TypificationMatrix__c  TypificationMatrix3 = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00004','Gestión Cobranza','Aclaracion','Plan de pago',null,null,null,'Trade Marketing',user.Id,null);  
        List<ISSM_AppSetting_cs__c> AppSettingProviderTypification =  ISSM_CreateDataTest_cls.fn_CreateAppSettingTypification(true,String.valueOf(TypificationMatrix1.Id),String.valueOf(TypificationMatrix2.Id),String.valueOf(TypificationMatrix3.Id),mapIdQueue.get('ISSM_WithoutOwner'));*/
        
        intervalsByCountry = ISSM_CreateDataTest_cls.fn_CreateIntervalsByCountry(true,'Interval1','MX','8','90');
        DocumentTypeOpenItems = ISSM_CreateDataTest_cls.fn_CreateDocumentTypeOpenItems(true,'DocumentType');
        List<ISSM_FieldsCloneCase__c> lstCloneCaseCustomSetting = ISSM_CreateDataTest_cls.fn_CreateFieldsCloneCase(true,'Description');
        
        ISSM_CreateDataTest_cls.fn_CreatetProspecSalesOffice(true,'System Administrator');
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 1);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        //user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true, RecordTypeSalesOfficeAccountId, 'AccountTest', user.Id, user.Id);
        salesOrg = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOrg(true, RecordTypeSalesOrgAccountId, 'AccountTest', user.Id, user.Id);
        drv = ISSM_CreateDataTest_cls.fn_CreateAccountDRV(true, RecordTypeDRVAccountId, 'AccountTest', user.Id, user.Id);
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Modelorama',null,null,null,null,null,'Trade Marketing',user.id,null);
        TypificationMatrix3  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'TM - 00009',null,null,null,null,null,null);
        TypificationMatrix4  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix2(true,'México - TM - 0008','Modelorama',null,null,null,null,null,'Supervisor',user.id,null);
        ISSM_MappingFieldCase__c MappingFields = ISSM_CreateDataTest_cls.fn_CreateCaseForceMapping(true,'ISSM_CaseForceNumber__c','id');
        ONTAP__Case_Force__c objCaseForce = ISSM_CreateDataTest_cls.fn_CreateCaseForce(true,'Descripcion CaseForce','Asunto CaseFroce','Open'); 
        Case objCase = ISSM_CreateDataTest_cls.fn_CreateCase(true,'Descripcion CaseForce','Asunto CaseFroce','New',objCaseForce.Id,1);
        Case UpdateCase = ISSM_CreateDataTest_cls.fn_UpdateCase(true,'DescripcionUpdate','AsuntoUpdate','Escalated',objCase.id);
        CaseComment objCaseComment = ISSM_CreateDataTest_cls.fn_CreateCaseComment(true, 'Descripcion comentario',objCase.Id);
        ONTAP__Case_Force_Comment__c objCaseForceComment = ISSM_CreateDataTest_cls.fn_CreateCaseForceComment(true, objCaseForce.Id,'Comentarios CaseComment');
        DataCase = ISSM_CreateDataTest_cls.fn_CreateCaseToolManager(true,'Descripcion','Asunto','New',false,false,'Level1');
        // Cretae configuracion personalizada 
		List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo','ISSM_Telecolletion') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        // AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
		// AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSettingTelecollection(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'),mapIdQueue.get('ISSM_Telecolletion'));
		AppTriggerFactory = ISSM_CreateDataTest_cls.fn_CreateTriggerFactory(true,'Telecollection_Campaign__c','ISSM_TelecollectionCampaignHandler_cls');
        
        List<Group> lstselectGroup1 =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue1 = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup1){
            mapIdQueue1.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        // AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue1.get('ISSM_WithoutOwner'),mapIdQueue1.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();    
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        
        // Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        Id idOwner = user.Id;
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        objTypificationLevels.strDescriptionTextArea='';
        objTypificationLevels.strOptionSelectedLevel1='Level1';
        objTypificationLevels.strOptionSelectedLevel2='Level2';
        objTypificationLevels.strOptionSelectedLevel3='Level3';
        objTypificationLevels.strOptionSelectedLevel4='level4';
        objTypificationLevels.strOptionSelectedLevel5='level5';
        objTypificationLevels.strOptionSelectedLevel6='level6';
        objTypificationLevels.getTypificationLevels();
        
        ISSM_Asset__c asset = new ISSM_Asset__c();
        asset.Name = 'Test Asset';
        asset.ISSM_Serial_Number__c = '123';
        asset.ISSM_Provider_Serial_Number__c = '123';
        asset.ISSM_Invoice_date__c = date.today() - 10;
        asset.ISSM_Warranty_Date__c = date.today() + 10;
        insert asset;
        
        ONTAP__Account_Asset__c accAsset = new ONTAP__Account_Asset__c();
        accAsset.ISSM_Asset_CAM__c = asset.Id;
        accAsset.ONTAP__Account__c = DataAccount.Id;
        insert accAsset;
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = salesOffice.Id;
        atm.UserId = user.Id;
        atm.TeamMemberRole = 'Trade Marketing';
        insert atm;
        
        objTypificationLevels.verifyWarranty();
        objTypificationLevels.assignTaskToCustomer();
        
        Test.stopTest();
    }
    static testMethod void TestTypificationLevel2() {  
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeSalesOrgAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();
        String RecordTypeDRVAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 1);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        //user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true, RecordTypeSalesOfficeAccountId, 'AccountTest', user.Id, user.Id);
        salesOrg = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOrg(true, RecordTypeSalesOrgAccountId, 'AccountTest', user.Id, user.Id);
        drv = ISSM_CreateDataTest_cls.fn_CreateAccountDRV(true, RecordTypeDRVAccountId, 'AccountTest', user.Id, user.Id);
        DataAccount = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Refrigeración','Retiro de equipo',null,null,null,null,'Trade Marketing',user.id,null);
        List<Group> lstselectGroup = [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();    
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        objTypificationLevels.strDescriptionTextArea='textarea';
        objTypificationLevels.strOptionSelectedLevel1='Refrigeración';
        objTypificationLevels.strOptionSelectedLevel2='Retiro de equipo';
        objTypificationLevels.strOptionSelectedLevel3='Level3';
        objTypificationLevels.strOptionSelectedLevel4='level4';
        objTypificationLevels.strOptionSelectedLevel5='level5';
        objTypificationLevels.strOptionSelectedLevel6='level6';
        objTypificationLevels.getTypificationLevels();
        
        ISSM_Asset__c asset = new ISSM_Asset__c();
        asset.Name = 'Test Asset';
        asset.ISSM_Serial_Number__c = '111';
        asset.ISSM_Provider_Serial_Number__c = '111';
        asset.ISSM_Invoice_date__c = date.today() - 10;
        asset.ISSM_Warranty_Date__c = date.today() + 10;
        insert asset;
        
        ONTAP__Account_Asset__c accAsset = new ONTAP__Account_Asset__c();
        accAsset.ISSM_Asset_CAM__c = asset.Id;
        accAsset.ONTAP__Account__c = DataAccount.Id;
        insert accAsset;
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = salesOrg.Id;
        atm.UserId = user.Id;
        atm.TeamMemberRole = 'Trade Marketing';
        insert atm;
        
        objTypificationLevels.verifyWarranty();
        // objTypificationLevels.assignTaskToCustomer();
        
        Test.stopTest();
    }
    static testMethod void TestTypificationLevel3() {  
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        String RecordTypeSalesOrgAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Org').getRecordTypeId();
        String RecordTypeDRVAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Regional Sales Division').getRecordTypeId();
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 2);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        //user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true, RecordTypeSalesOfficeAccountId, 'AccountTest', user.Id, user.Id);
        salesOrg = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOrg(true, RecordTypeSalesOrgAccountId, 'AccountTest', user.Id, user.Id);
        drv = ISSM_CreateDataTest_cls.fn_CreateAccountDRV(true, RecordTypeDRVAccountId, 'AccountTest', user.Id, user.Id);
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Modelorama','Opera un Modelorama o rentame tu local','Opera un Modelorama',null,null,null,'Trade Marketing',user.id,null);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();    
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        
        objTypificationLevels.strDescriptionTextArea='textarea';
        objTypificationLevels.strOptionSelectedLevel1='Modelorama';
        objTypificationLevels.strOptionSelectedLevel2='Opera un Modelorama o rentame tu local';
        objTypificationLevels.strOptionSelectedLevel3='Opera un Modelorama';
        objTypificationLevels.strOptionSelectedLevel4='level4';
        objTypificationLevels.strOptionSelectedLevel5='level5';
        objTypificationLevels.strOptionSelectedLevel6='level6';
        objTypificationLevels.getTypificationLevels();
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        
        ISSM_Asset__c asset = new ISSM_Asset__c();
        asset.Name = 'Test Asset';
        asset.ISSM_Serial_Number__c = '222';
        asset.ISSM_Provider_Serial_Number__c = '222';
        asset.ISSM_Invoice_date__c = date.today() - 10;
        asset.ISSM_Warranty_Date__c = date.today() + 10;
        insert asset;
        
        ONTAP__Account_Asset__c accAsset = new ONTAP__Account_Asset__c();
        accAsset.ISSM_Asset_CAM__c = asset.Id;
        accAsset.ONTAP__Account__c = DataAccount.Id;
        insert accAsset;
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = drv.Id;
        atm.UserId = user.Id;
        atm.TeamMemberRole = 'Trade Marketing';
        insert atm;
        
        objTypificationLevels.verifyWarranty();
        // objTypificationLevels.assignTaskToCustomer();
        
        Test.stopTest();
    }
    static testMethod void TestTypificationLevel4() {  
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 3);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true,RecordTypeSalesOfficeAccountId,'AccountTest', user.Id, user.Id  );
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Modelorama','Modelorama empresarios','Administración','Licencias',null,null,'Trade Marketing',user.id,null);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();    
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        
        objTypificationLevels.strDescriptionTextArea='textarea';
        objTypificationLevels.strOptionSelectedLevel1='Modelorama';
        objTypificationLevels.strOptionSelectedLevel2='Modelorama empresarios';
        objTypificationLevels.strOptionSelectedLevel3='Administración';
        objTypificationLevels.strOptionSelectedLevel4='Licencias';
        objTypificationLevels.strOptionSelectedLevel5='level5';
        objTypificationLevels.strOptionSelectedLevel6='level6';
        objTypificationLevels.getTypificationLevels();
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        Test.stopTest();
    }
    static testMethod void TestTypificationLevel5() {  
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();        
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest2');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 1);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true,RecordTypeSalesOfficeAccountId,'AccountTest', user.Id, user.Id  );
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0002','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler con garantía','Imbera',null,'Trade Marketing',user.id,null);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();    
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        
        objTypificationLevels.strDescriptionTextArea='textarea';
        objTypificationLevels.strOptionSelectedLevel1='Clientes y clientes nuevos';
        objTypificationLevels.strOptionSelectedLevel2='Refrigeración';
        objTypificationLevels.strOptionSelectedLevel3='Mantenimiento';
        objTypificationLevels.strOptionSelectedLevel4='Cooler con garantía';
        objTypificationLevels.strOptionSelectedLevel5='Imbera';
        objTypificationLevels.strOptionSelectedLevel6='level6';
        objTypificationLevels.getTypificationLevels();
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        Test.stopTest();
    }
    static testMethod void TestTypificationLevel6() {   
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();            
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 2);
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true,RecordTypeSalesOfficeAccountId,'AccountTest', user.Id, user.Id  );
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','Trade Marketing',user.id,null);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        apexpages.currentpage().getparameters().put('def_account_id' , DataAccount.Id); 
        
        //Comienza la ejecucion de la prueba 
        Test.startTest();   
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        objTypificationLevels.getFields();
        objTypificationLevels.getDataSetFields(); 
        
        objTypificationLevels.strDescriptionTextArea='textarea';
        objTypificationLevels.strOptionSelectedLevel1='Clientes y clientes nuevos';
        objTypificationLevels.strOptionSelectedLevel2='Refrigeración';
        objTypificationLevels.strOptionSelectedLevel3='Mantenimiento';
        objTypificationLevels.strOptionSelectedLevel4='Cooler';
        objTypificationLevels.strOptionSelectedLevel5='Bajo cero';
        objTypificationLevels.strOptionSelectedLevel6='Agencia';
        objTypificationLevels.getTypificationLevels();
        acctPage = objTypificationLevels.EditCase();
        objTypificationLevels.CleanFormTypification();
        Test.stopTest();
    }
    
    
    static testMethod void testGetOwnerUser() {
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','User', user.id, null);
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 3);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        Test.startTest();
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        System.assert( idOwner != null,'Ids is not null');
        //System.assertEquals(idOwner,user.Id);**********
        Test.stopTest();
    }
    
    static testMethod void testGetOwnerQueue() {
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        queueCase = ISSM_CreateDataTest_cls.fn_CreateQueue(true, 'Queue Test Cases');
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','Queue', user.id, queueCase.Id);
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 1);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        Test.startTest();
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        Id idOwner =ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        System.assert( idOwner != null,'Ids is not null');
        System.assertEquals(idOwner,queueCase.Id);
        Test.stopTest();
    }
    
    static testMethod void testGetOwnerAccountTeamMember(){
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest');
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','Supervisor', user.id, null);
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 2);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        Test.startTest();
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        System.assert( idOwner != null,'Ids is not null');
        //System.assertEquals(idOwner,user.Id);**********
        Test.stopTest();
    }
    static testMethod void testGetOwnerAccountBilling(){
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('OnCall POC / Sold To').getRecordTypeId();
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true,RecordTypeSalesOfficeAccountId,'AccountTest', user.Id, user.Id  );
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','Billing Manager', user.id, null);
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 3);
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        
        Test.startTest();
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        System.assert( idOwner != null,'Ids is not null');
        //System.assertEquals(idOwner,user.Id);*********************
        Test.stopTest();
    }
    
    static testMethod void testGetOwnerAccountTrade(){
        String RecordTypeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('OnCall POC / Sold To').getRecordTypeId();
        String RecordTypeSalesOfficeAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sales Office').getRecordTypeId();
        user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true,'prueba@prueba.issm.prueba1'+ Datetime.now().millisecond());
        // user = ISSM_CreateDataTest_cls.fn_CreateAdminUser(true);
        salesOffice = ISSM_CreateDataTest_cls.fn_CreateAccountSalesOffice(true,RecordTypeSalesOfficeAccountId,'AccountTest', user.Id, user.Id  );
        DataAccount  = ISSM_CreateDataTest_cls.fn_CreateAccount(true,RecordTypeAccountId,'AccountTest', salesOffice);
        accountTeamMember = ISSM_CreateDataTest_cls.fn_CreateAccountTeamMember(true, dataAccount.Id, ISSM_Constants_cls.SUPERVISOR, user.Id);
        TypificationMatrix  = ISSM_CreateDataTest_cls.fn_CreateTypificationMatrix(true,'México - TM - 0001','Clientes y clientes nuevos','Refrigeración','Mantenimiento','Cooler','Bajo cero','Agencia','Trade Marketing', user.id, null);
        caseObj = ISSM_CreateDataTest_cls.fn_CreateCase(true, 'Descripcion', 'Asunto', 'New', null, 1);
        List<Group> lstselectGroup =  [Select id,developerName From Group g where DeveloperName  IN ('ISSM_WithoutOwner','ISSM_ModeloAmigo') order by developername ];
        Map<String,Id> mapIdQueue = new Map<String,Id>();
        for(Group objgroupId : lstselectGroup){
            mapIdQueue.put(objgroupId.DeveloperName,objgroupId.Id);
        }
        AppSetting = ISSM_CreateDataTest_cls.fn_CreateAppSetting(true,mapIdQueue.get('ISSM_WithoutOwner'),mapIdQueue.get('ISSM_ModeloAmigo'));
        ApexPages.StandardController ObjstandardCase = new ApexPages.StandardController(caseObj);
        
        Test.startTest();
        ISSM_TypificationLevels_ctr objTypificationLevels = new ISSM_TypificationLevels_ctr(ObjstandardCase);
        Id idOwner = ISSM_CreateCaseGlobal_cls.getOwner(TypificationMatrix,AppSetting[0]);
        System.assert( idOwner != null,'Ids is not null');
        //System.assertEquals(idOwner,user.Id);***********
        
        Test.stopTest();
    }
}