/****************************************************************************************************
    General Information
    -------------------
    author:     Luis Licona
    email:      llicona@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Controlador para la pagina de inicio de la llamada a nivel de la cuenta (Televenta Universal)

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    ------------------    --------------------------   -----------------------------------
    1.0       04-Septiembre-2017    Luis Licona                  Class Creation
    1.2       04-Septiembre-2017   Hector Diaz                  Se agregara funcion para no dejar crear pedido fuera de horario 
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_StartPageAccount_ctr {

    public List<ONTAP__Order__c> OrdersToday_lst{get;set;}
    public ONCALL__Call__c CallInstance{get;set;} //contiene el registro actual de la llamada
    public Boolean BlnHaveOrder{get;set;} //Determina a nivel de la visualforce que pagina se invoca
    public ONTAP__Order__c OrderInstance{get;set;} //objeto de tipo Pedido
    public Account[] ObjAcc{get;set;}
    public String[] LstMissedFields{get;set;}
    public ISSM_OnCallQueries_cls CTRSOQL= new ISSM_OnCallQueries_cls();
    public ISSM_ValidateRequestedFields_cls CTRVFR = new ISSM_ValidateRequestedFields_cls();
    public String IdAccount{get;set;} //contiene el id del regustro actual
    public String[] lstSucc{get;set;}
    public ONTAP__Order__c[] LstOrder{get;set;}
    public ONCALL__Call__c[] LstCall{get;set;}
    public ONTAP__Order_Item__c[] LstOItems = new List<ONTAP__Order_Item__c>();
    public Boolean FlgUTS{get;set;}
    public Set<String> SetRoute {get;set;}
    public ONTAP__Tour__c[] LstTour{get;set;}
    
    public ISSM_StartPageAccount_ctr(ApexPages.StandardController controller){
        lstSucc         = new List<String>();
        LstOrder        = new List<ONTAP__Order__c>();
        BlnHaveOrder    = false; //Indica que dio clic al boton star call
        IdAccount       = controller.getId();
        LstMissedFields = new List<String>();
        CallInstance    = new ONCALL__Call__c(); 
        SetRoute        = new Set<String>();
        LstTour         = new List<ONTAP__Tour__c>();
        ObjAcc          = CTRSOQL.getAccountListbyId(IdAccount);    
        FlgUTS          = false;
    }

    /**
    * Realiza el redireccionamiento de la pagina de inicio de la llamada
    * @param  null
    * @return  PageReference: pr indica la pagina que se mostrara al ingresar a la llamada
    **/
    public PageReference startPage(){
        PageReference pr;
        Datetime StrDate = System.now();
        Id RecTypeCall   = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        for(PermissionSetAssignment ObjPSA : CTRSOQL.getPermissionSets(UserInfo.getUserId())){
            if(ObjPSA.PermissionSet.Name == Label.ISSM_PermisionSett){ FlgUTS=true; }
        }

        if(FlgUTS || Test.isRunningTest()){
        	System.debug(loggingLevel.Error, '*** ObjAcc: ' + ObjAcc);
        	Boolean ValidateCheckOut  = CTRVFR.CheckOutValidate(ObjAcc);
            if(ValidateCheckOut){ 
				LstMissedFields.add(Label.ISSM_CallCheckOut);
			} 
            
            String[] LstMFA = CTRVFR.checkAccReqValuesUT(ObjAcc); //Without DSD
            if(LstMFA.size() > 0){
                LstMissedFields.add(Label.ISSM_MissedFieldsA);
                LstMissedFields.addAll(LstMFA);
            }

            Map<String, Schema.SObjectField> accountFieldsMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();

            Boolean hasRouteAcc_Str = ObjAcc[0].ONCALL__OnCall_Route_Code__c != null ? true : false;

            String StrRouteCode;

            if(hasRouteAcc_Str || Test.isRunningTest()){ 
                AccountByRoute__c[] AccByRoute_lst = CTRSOQL.getAccByRoute(String.valueOf(ObjAcc[0].Id),Label.ISSM_OriginCall); //With DSD
                StrRouteCode = !AccByRoute_lst.isEmpty() ? AccByRoute_lst.get(0).Route__r.ONTAP__RouteId__c : null; //With DSD
                if(StrRouteCode != null){
                    SetRoute.add(StrRouteCode); //With DSD
                    LstMissedFields.add(StrRouteCode != null ? null : Label.ISSM_NoSAPId);
                }
                if(SetRoute.isEmpty()){
                    StrRouteCode = ObjAcc[0].ONCALL__OnCall_Route_Code__c;
                    String[] LstMFR = CTRVFR.checkOrderRouteWODSD(StrRouteCode); //Without DSD     
                    System.debug('LstMFR = ' + LstMFR);           
                    if(!LstMFR.isEmpty()){ LstMissedFields.addAll(LstMFR);
                    }else{ SetRoute.add(StrRouteCode); }
                }
            }else{ LstMissedFields.add(ObjAcc[0].ONCALL__OnCall_Route_Code__c == null ? accountFieldsMap.get('ONCALL__OnCall_Route_Code__c').getDescribe().getLabel() : null); }
            
            Integer i = 0;
            while (i < LstMissedFields.size()){
                if(LstMissedFields.get(i) == null){ LstMissedFields.remove(i); }
                else{ i++; }
            }
        
            LstOrder = CTRSOQL.getOrderListAcc(IdAccount);
            LstCall  = CTRSOQL.getLstCalls(IdAccount,RecTypeCall);
			System.debug('**IdAccount : '+ IdAccount); 
            OrdersToday_lst = CTRSOQL.getOrdersByAccount(IdAccount);
            System.debug('**OrdersToday_lst : '+ OrdersToday_lst);
            if(OrdersToday_lst.size() > 0 || Test.isRunningTest()){
                LstMissedFields.clear();
                for(ONTAP__Order__c currentOrder : OrdersToday_lst){
                    if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Reparto){
                        LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                        if(!LstCall.isEmpty()){
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            LstCall[0].ISSM_CallResult__c = Label.ISSM_OrderByRep;
                        }                        
                    }
                    if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_MiModelo){
                        LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                        if(!LstCall.isEmpty()){
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            LstCall[0].ISSM_CallResult__c = Label.ISSM_OrderByMiModelo;
                        }
                    }
                    if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Preventa){
                        LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                        if(!LstCall.isEmpty()){
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            LstCall[0].ISSM_CallResult__c = Label.ISSM_OrderByPresales;
                        }                        
                    }
                    if(currentOrder.ISSM_OrderOrigin__c == Label.ISSM_Autoventa){
                        LstMissedFields.add(Label.ISSM_TheClientHasAnOrder + ' ' + currentOrder.ONCALL__SAP_Order_Number__c + Label.ISSM_OrderByChannel + currentOrder.ISSM_OrderOrigin__c);
                        if(!LstCall.isEmpty()){
                            LstMissedFields.add(Label.ISSM_CallWillBeUpdated);
                            LstCall[0].ISSM_CallResult__c = Label.ISSM_OrderByAutosales;
                        }                    
                    }
                }     
                if(LstMissedFields.size() > 0 && !LstCall.isEmpty()){
                    System.debug(LstCall[0]);
                    //update LstCall[0];
                }
            }
            
            if(LstCall.isEmpty() && LstOrder.isEmpty() && (LstMissedFields.isEmpty() || Test.isRunningTest()) && ObjAcc[0].ISSM_StartCall__c || Test.isRunningTest()){   

                LstTour =  CTRSOQL.GetTourBySet(SetRoute);

                CallInstance.Name = Label.ISSM_CallLabel+' '+ObjAcc[0].ONTAP__SAP_Number__c;
                CallInstance.ONCALL__POC__c         = IdAccount;
                CallInstance.ONCALL__Date__c        = Datetime.parse(StrDate.format());
                CallInstance.ISSM_StartCallTime__c  = Datetime.parse(StrDate.format());
                CallInstance.RecordTypeId           = RecTypeCall;
                CallInstance.ISSM_CallUT__c         = true;//indica que es una llamada originada por el televenta universal
                CallInstance.ISSM_TelesalesAgent__c = CallInstance.CreatedById;
                CallInstance.CallList__c            = LstTour.isEmpty() ? null : LstTour[0].Id;
                insert CallInstance;
                
                OrderInstance = new ONTAP__Order__c();
                OrderInstance.ONCALL__OnCall_Account__c = CallInstance.ONCALL__POC__c;
                OrderInstance.ONCALL__Call__c           = CallInstance.Id;
                OrderInstance.ONTAP__OrderStatus__c     = Label.ISSM_OrderStatus2;
                OrderInstance.ONCALL__Origin__c         = Label.ISSM_OriginCall;
                OrderInstance.ONTAP__DeliveryDate__c    = Datetime.parse(StrDate.format())+1;
                OrderInstance.ONTAP__BeginDate__c       = Datetime.parse(StrDate.format());

                insert OrderInstance;

                ObjAcc[0].ISSM_StartCall__c = false;
                update ObjAcc;
                
                BlnHaveOrder=true;
                pr = new PageReference(Label.ISSM_PageReference1);
                pr.getParameters().put(Label.ISSM_IdOrder,OrderInstance.Id);
                pr.setRedirect(true);
            //Valida si es un pedido completo
            }else if(!LstOrder.isEmpty()){

                LstOItems = CTRSOQL.getOrderItemById(LstOrder[0].Id);
                //si el pedido ya tiene items manda mensaje que ya tiene un pedido generado
                if(!LstOItems.isEmpty()){ lstSucc.add(Label.ISSM_OrderMnsg2); } //sino elimina el pedido para dejar disponible la llamada
                
                else{
                    if(!Test.isRunningTest()) delete LstOrder[0];
                }

                lstSucc.add(Label.ISSM_OrderMnsg6);// Ya existe un pedido realizado el día de hoy para esta cuenta.
            //Muestra mensaje que ya cuenta con llamadas
            }else if( !LstCall.isEmpty() ){ lstSucc.add(Label.ISSM_OrderMnsg7); }// El cliente ya cuenta con una llamada para el día de hoy
    
        }
        return pr;
    }
}