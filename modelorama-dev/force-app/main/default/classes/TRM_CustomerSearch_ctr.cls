/**
 * Development by:		Avanxo Mexico
 * Author:				Carlos Pintor / cpintor@avanxo.com
 * Project:				AbInbev - Trade Revenue Management
 * Description:			Apex Controller class of the Lightning Component 'TRM_CustomerSearch_lcp'.
 * 						Methods included achives the processes: 
 * 						- get field definitions of a field set to set the columns of a Lightning Data Table
 * 						- get a list of Accounts to display in a Lightning Data Table
 * 						- get a sObject Record from Salesforce
 * 						
 *
 * N.     Date             Author                     Description
 * 1.0    2018-08-31       Carlos Pintor              Created
 *
 */
public with sharing class TRM_CustomerSearch_ctr {
 
	/**
    * @description  Receives two parameters with the sObject API name and the fieldset API name to search for. 
    *               The Field Definition results are returned in a 'DataTableColumn' type list.
    * 				The result list is used to set the attribute 'columns' of a Lightning Data Table component
    * 				For each Field Definition, the returned information contains:
    *				- Field Label
    * 				- Field Path (Field API Name)
    * 				- Field Type
    * 
    * @param    sObjectApiName 		API name of the Object. E.g: 'Account'
    * @param    fieldSetApiName 	API name of the Field Set. E.g: 'TRM_ConditionClassCustomerDataTable'
    * 
    * @return   Return a list of type 'DataTableColumn' with definition of fields.
    * 			Each field definition contains: Label, FieldPath, Type
    */
	@AuraEnabled
	public static List<DataTableColumn> getDataTableColumnsByFieldset(String sObjectApiName, String fieldSetApiName) {
		System.debug('#### start TRM_CustomerSearch_ctr.getDataTableColumnsByFieldset()');
		//Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(sObjectApiName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetApiName);

		List<DataTableColumn> returnList = new List<DataTableColumn>();

		for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
			//Create a wrapper instance and store label, fieldname and type.
            DataTableColumn dataColumn = new DataTableColumn( String.valueOf(eachFieldSetMember.getLabel()) , 
                                                                String.valueOf(eachFieldSetMember.getFieldPath()), 
                                                                String.valueOf(eachFieldSetMember.getType()).toLowerCase() );
            returnList.add(dataColumn);
		}
		System.debug('#### DataTableColumns: ' + returnList);
		return returnList;
	}

	/**
    * @description  Receives a list of parameters to create a dynamic SOQL query to get Accounts for the component TRM_CustomerSearch_lcp
    *				This method is called when:
    *				- The Lightning Component TRM_CustomerSearch_lcp is rendered for the first time
    *				- The user writes a search word to filter account results
    *				- The user writes a list of Account External Keys to search accounts
    * 
    * @param 			searchByList 			Indicates if the user selected the option to seach by list mode. E.g: true
    * @param 			searchKeyWord 			String with the search word that the user wrote to filter accounts. E.g: 'FK00'
    * @param 			fieldList 	 			String with a list of Field API names to request in the SOQL query. E.g: 'ONTAP__ExternalKey__c, Name'
    * @param 			objectType 	 			Object API name to request in the SOQL query. E.g: 'Account'
    * @param 			recordType 	 			Developer Name of the record type to request in the SOQL query. E.g: 'Account'
    * @param 			fieldOrderByList		String with a list of Field API names to order the results of the query. E.g: 'ONTAP__ExternalKey'
    * @param 			numberOfRowsToReturn	Maximum number of record to retrieve. E.g: '3000'
    * @param 			filterIdList 			Array of strings with the Salesforce ID of records to exclude in the search. E.g: '5tdS4l3sf0rc31d'
    * @param 			filterBySegment			Indicates if the records must be filter by segment. E.g: true
    * @param 			segmentList 			String with a list of Segment Codes to filter the records. E.g: 'Segmento-40, Segmento-41, Segmento-42'
    * @param 			filterByZone 			Indicates if the records must be filter by Price Zone. E.g: true
    * @param 			priceZoneCatCod			String with a Price Zone Code to filter the records. E.g: '01, 06'
    * @param 			filterBySalesOffice		Indicates if the records must be filter by Sales Office. E.g: true
    * @param 			salesOfficeId			String with a Standard Salesforce Id of a Sales Office to filter records. E.g: '5tdS4l3sf0rc31d'
    * @param 			filterBySalesOrg		Indicates if the records must be filter by Sales Org. E.g: true
    * @param 			salesOrgList 			String with a list of Sales Org External Keys to filter the records. E.g: '3116'
    * @param 			filterByDiscList		Indicates if the records must be filter by Discount List. E.g: true
    * @param 			discListCatalogCode		String with a Discount List Code to filter the records. E.g: '[1, [2'
    * 
    * @return   Return a list of Accounts with the fields requested by the SOQL query field list
    */
	@AuraEnabled
	public static Account[] getAccounts(
		Boolean searchByList, 
		String searchKeyWord,
		String fieldList, 
		String objectType, 
		String recordType, 
		String fieldOrderByList,
		String numberOfRowsToReturn, 
		String[] filterIdList, 
		Boolean filterBySegment, 
		String segmentList,
		Boolean filterByZone, 
		String priceZoneCatCod,
		Boolean filterBySalesOffice, 
		String salesOfficeId,
		Boolean filterBySalesOrg, 
		String salesOrgList,
		Boolean filterByDiscList, 
		String discListCatalogCode
		){
		System.debug('#### start TRM_CustomerSearch_ctr.getAccounts()');
		List<String> segmentCodeList = new List<String>();
		if(segmentList != null && segmentList.length() > 0){
			segmentCodeList = segmentList.split(';');
			for(Integer i = 0; i < segmentCodeList.size(); i++){
				segmentCodeList.set(i, '\'' + segmentCodeList.get(i) + '\'');
			}
		}

		/*List<String> priceZoneList = new List<String>();
		if(zoneList != null && zoneList.length() > 0){
			zoneList = zoneList.remove('PriceZone-');
			priceZoneList = zoneList.split(';');
			for(Integer i = 0; i < priceZoneList.size(); i++){
				priceZoneList.set(i, '\'' + priceZoneList.get(i) + '\'');
			}
		}*/

		/*List<String> salesOfficeIdList = new List<String>();
		if(salesOfficeList != null && salesOfficeList.length() > 0){
			salesOfficeIdList = salesOfficeList.split(';');
			for(Integer i = 0; i < salesOfficeIdList.size(); i++){
				salesOfficeIdList.set(i, '\'' + salesOfficeIdList.get(i) + '\'');
			}
		}*/

		List<String> salesOrgIdList = new List<String>();
		if(salesOrgList != null && salesOrgList.length() > 0){
			salesOrgIdList = salesOrgList.split(';');
			for(Integer i = 0; i < salesOrgIdList.size(); i++){
				salesOrgIdList.set(i, '\'' + salesOrgIdList.get(i) + '\'');
			}
		}

		List<Account> returnList = new List<Account>();
		List<String> keyWordList = new List<String>();
		if(searchByList){
			keyWordList = String.escapeSingleQuotes(searchKeyWord).split(' ');
			for(Integer i = 0; i < keyWordList.size(); i++){
				keyWordList.set( i, '\'' + keyWordList.get(i) + '\'' );
			}
		} else{
			searchKeyWord   ='\'%' + String.escapeSingleQuotes(searchKeyWord) + '%\'';
		}
		
		String queryString = 'SELECT Id, ' + fieldList;
				queryString += ' FROM ' + objectType;
				queryString += ' WHERE RecordType.DeveloperName = \'' + recordType + '\'';
				queryString += ' AND Name != null';
				queryString += ' AND ONTAP__ExternalKey__c != null';
				queryString += ' AND ONTAP__SalesOgId__c != null';
				queryString += ' AND ONTAP__SalesOffId__c != null';
				queryString += ' AND ISSM_SalesOrg__c != null';
				queryString += ' AND ISSM_SalesOffice__c != null';
				queryString += ' AND TRM_PriceZone__c != null';
				queryString += ' AND TRM_DLCGpo__c != null';
				queryString += ' AND ONTAP__ChannelId__c != null';
				queryString += ' AND ISSM_SegmentCode__c != null';
				queryString += filterIdList.size() > 0 ? ' AND Id NOT IN (' + String.join(filterIdList, ',') + ')' : '';
				queryString += filterBySegment == true && segmentCodeList.size() > 0 ? ' AND ISSM_SegmentCode__r.ExternalId__c IN (' + String.join(segmentCodeList, ',') + ')' :'';
				//queryString += filterByZone == true && priceZoneList.size() > 0 ? ' AND TRM_PriceZone__c IN (' + String.join(priceZoneList, ',') + ')' :'';
				queryString += filterByZone == true && priceZoneCatCod.length() > 0 ? ' AND TRM_PriceZone__c = \'' + priceZoneCatCod + '\'' : '';
				//queryString += filterBySalesOffice == true && salesOfficeIdList.size() > 0 ? ' AND ONTAP__SalesOffId__c IN (' + String.join(salesOfficeIdList, ',') + ')' :'';
				queryString += filterBySalesOffice && salesOfficeId.length() > 0 ? ' AND ISSM_SalesOffice__c = \'' + salesOfficeId + '\'' : '';
				queryString += filterBySalesOrg == true && salesOrgIdList.size() > 0 ? ' AND ONTAP__SalesOgId__c IN (' + String.join(salesOrgIdList, ',') + ')' :'';
				queryString += filterByDiscList == true && discListCatalogCode.length() > 0 ? ' AND TRM_DLCGpo__c = \'' + discListCatalogCode + '\'' :'';
				queryString += searchByList ? ' AND ONTAP__ExternalKey__c IN (' + String.join(keyWordList, ',') + ')' : '';
				//queryString += ' AND ( ONTAP__ExternalKey__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' AND ( ONTAP__ExternalKey__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR Name LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR ONTAP__SalesOgId__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR ONTAP__SalesOffId__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR ISSM_SalesOfficeName__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR ISSM_SegmentCodeName__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR TRM_PriceZone__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' OR TRM_DLCGpo__c LIKE ' + searchKeyWord;
				queryString += searchByList ? '' : ' )';
				queryString += ' ORDER BY ' + fieldOrderByList;
				queryString += ' LIMIT ' + numberOfRowsToReturn;
		System.debug('#### queryString:' + queryString);
		returnList = ISSM_UtilityFactory_cls.executeQuery(queryString);
		System.debug('#### accounts list size:' + returnList.size());
		return returnList;
	}

	/**
    * @description  Receives a list of parameters to create a dynamic SOQL query to get Accounts by Id List for the component TRM_CustomerSearch_lcp
    *				This method is called when:
    *				- The Lightning Component TRM_CustomerSearch_lcp is rendered in EditMode and requires to load selected customers
    * 
    * @param 			fieldList 	 			String with a list of Field API names to request in the SOQL query. E.g: 'ONTAP__ExternalKey__c, Name'
    * @param 			objectType 	 			Object API name to request in the SOQL query. E.g: 'Account'
    * @param 			customerIdList 			Salesforce Id List of the accounts to retrieve.
    * @param 			numberOfRowsToReturn	Maximum number of record to retrieve. E.g: '3000'
    * 
    * @return   Return a list of Accounts with the fields requested by the SOQL query field list
    */
	@AuraEnabled
	public static Account[] getAccountsByIdList(String fieldList, String objectType, String[] customerIdList, String numberOfRowsToReturn){
		System.debug('#### start TRM_CustomerSearch_ctr.getAccountsByIdList()');
		List<Account> returnList = new List<Account>();

		String queryString = 'SELECT Id, ' + fieldList;
				queryString += ' FROM ' + objectType;
				queryString += ' WHERE Id IN (' + String.join(customerIdList, ',') + ')';
				queryString += ' LIMIT ' + numberOfRowsToReturn;
		System.debug('#### queryString:' + queryString);
		returnList = ISSM_UtilityFactory_cls.executeQuery(queryString);
		return returnList;
	}

	/**
    * @description  Receives two parameters with the object API name and the Salesforce record Id to retrieve.
    * 
    * @param    objectType 			API name of the Object. E.g: 'MDM_Parameter__c'
    * @param    id 	 		 		Salesforce Standard Id of the Record. E.g: '5tdS4l3sf0rc31d'
    * 
    * @return   Return an instance of sObject with the record retrieved from the server
    */
	@AuraEnabled
	public static sObject getRecordById(String objectType, String id) {
		sObject returnValue;
		if( id != null || id != ''){
			returnValue = ISSM_UtilityFactory_cls.getRecordById(objectType, id);
		}
		return returnValue;
	}

	/*public class RecordWrapper{
		@AuraEnabled public sObject record;
		@AuraEnabled public Boolean selected;

		public RecordWrapper(sObject record, Boolean selected){
			this.record = record;
			this.selected = selected;
		}
	}*/

	//Wrapper class to hold Columns with headers
    public class DataTableColumn {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumn(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
}