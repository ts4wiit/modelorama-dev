/**************************************************************************************
Nombre de la Clase Apex: ISSM_AdminCuentasATM_bch
Versión : 1.0
Fecha de Creación : 09 Abril 2018
Funcionalidad : Batch para Buscar Registros de el Objeto ISSM_ATM_Queue__c y Procesar las Altas, Bajas y/o Cambios
Clase de Prueba: ISSM_AdminCuentasATM_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        09 - Abril - 2018      Versión Original
*************************************************************************************/
global class ISSM_AdminCuentasATM_bch implements Database.Batchable<sObject> {
    // Instanciamos la clase para realizar las consultas necesarias
    public ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();

    global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT Id, Oficina_de_Ventas__c, Usuario_relacionado__c, Rol__c, Accion__c, Listo_para_Procesar__c, Listo_para_Eliminar__c FROM ISSM_ATM_Queue__c WHERE Listo_para_Procesar__c = true';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ISSM_ATM_Queue__c> scope)
    {
        try {
            // Obtenemos el Record Type "Account"
            String RecordTypeId_str = '';
            RecordTypeId_str = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account').getRecordTypeId();
            
            Set<String> CuentasIds_set = new Set<String>();
            Set<String> UsuariosIds_set = new Set<String>();

            Set<String> CuentasAltaIds_set = new Set<String>();
            Set<String> CuentasBajaIds_set = new Set<String>();
            Set<String> CuentasCambioIds_set = new Set<String>();

            Set<String> CuentasBajaATMIds_set = new Set<String>();
            Set<String> CuentasCambioATMIds_set = new Set<String>();

            Set<String> Roles_set = new Set<String>();
            
            List<ISSM_ATM_Mirror__c> AccTM_lst = new List<ISSM_ATM_Mirror__c>();
            List<ISSM_ATM_Mirror__c> AccTM_Ins_lst = new List<ISSM_ATM_Mirror__c>();
            List<ISSM_ATM_Mirror__c> AccTM_Upd_lst = new List<ISSM_ATM_Mirror__c>();
            
            List<Account> Acc_lst = new List<Account>();
            List<Account> Acc_Baja_lst = new List<Account>();
            List<Account> Acc_Cambio_lst = new List<Account>();
            List<Account> Acc_Upd_lst = new List<Account>();

            List<AccountTeamMember> ATM_New_lst = new List<AccountTeamMember>();
            List<AccountTeamMember> AccTM_Baja_lst = new List<AccountTeamMember>();
            List<AccountTeamMember> AccTM_Cambio_lst = new List<AccountTeamMember>();
            List<AccountTeamMember> AccTM_Upd_Cambio_lst = new List<AccountTeamMember>();
            
            List<ISSM_ATM_Queue__c> ATM_Queue_Altas_lst = new List<ISSM_ATM_Queue__c>();
            List<ISSM_ATM_Queue__c> ATM_Queue_Bajas_lst = new List<ISSM_ATM_Queue__c>();
            List<ISSM_ATM_Queue__c> ATM_Queue_Cambios_lst = new List<ISSM_ATM_Queue__c>();

            List<ISSM_ATM_Queue__c> Upd_ATM_Queue_Altas_lst = new List<ISSM_ATM_Queue__c>();
            List<ISSM_ATM_Queue__c> Upd_ATM_Queue_Bajas_lst = new List<ISSM_ATM_Queue__c>();
            List<ISSM_ATM_Queue__c> Upd_ATM_Queue_Cambios_lst = new List<ISSM_ATM_Queue__c>();
            
            for (ISSM_ATM_Queue__c reg : scope)
            {
                if (reg.Accion__c == 'Alta') {
                    CuentasAltaIds_set.add(reg.Oficina_de_Ventas__c);
                    ATM_Queue_Altas_lst.add(reg);

                    reg.Listo_para_Eliminar__c = true;
                    Upd_ATM_Queue_Altas_lst.add(reg);
                } else if (reg.Accion__c == 'Baja') {
                    CuentasBajaIds_set.add(reg.Oficina_de_Ventas__c);
                    ATM_Queue_Bajas_lst.add(reg);

                    reg.Listo_para_Eliminar__c = true;
                    Upd_ATM_Queue_Bajas_lst.add(reg);
                } else if (reg.Accion__c == 'Cambio') {
                    CuentasCambioIds_set.add(reg.Oficina_de_Ventas__c);
                    ATM_Queue_Cambios_lst.add(reg);

                    reg.Listo_para_Eliminar__c = true;
                    Upd_ATM_Queue_Cambios_lst.add(reg);
                }
            }
            
            // Lógica para cuando se dan de alta usuarios en oficinas de venta
            if (ATM_Queue_Altas_lst.size() > 0) {
                if (CuentasAltaIds_set.size() > 0) {
                    // Obtiene las cuentas que coincidan con el set de altas
                    Acc_lst = objCSQuerys.getAccountsRegistration(CuentasAltaIds_set, RecordTypeId_str);

                    if (Acc_lst.size() > 0) {
                        for (Account acc : Acc_lst) {
                            for (ISSM_ATM_Queue__c atmQL : ATM_Queue_Altas_lst) {
                                // Insertamos los usuarios en el equipo de cuentas de los clientes
                                AccountTeamMember ATM = new AccountTeamMember(AccountId = acc.Id, UserId = atmQL.Usuario_relacionado__c, TeamMemberRole = atmQL.Rol__c);
                                ATM_New_lst.add(ATM);
                            }
                        }
                    }
                    if (ATM_New_lst.size() > 0) {
                        insert ATM_New_lst;
                    }
                }
            }

            // Lógica para cuando se dan de baja usuarios en oficinas de venta
            if (ATM_Queue_Bajas_lst.size() > 0) {
                if (CuentasBajaIds_set.size() > 0) {
                    // Obtiene las cuentas que coincidan con el set de bajas
                    Acc_Baja_lst = objCSQuerys.getAccountsDrop(CuentasBajaIds_set, RecordTypeId_str);

                    if (Acc_Baja_lst.size() > 0) {
                        for (Account acc : Acc_Baja_lst) {
                            CuentasBajaATMIds_set.add(acc.Id);
                            for (ISSM_ATM_Queue__c atmQL : ATM_Queue_Bajas_lst) {
                                UsuariosIds_set.add(atmQL.Usuario_relacionado__c);
                            }
                        }

                        if (CuentasBajaATMIds_set.size() > 0) {
                            AccTM_Baja_lst = [SELECT Id
                                              FROM AccountTeamMember
                                              WHERE AccountId IN: CuentasBajaATMIds_set
                                              AND UserId IN: UsuariosIds_set];
                        }

                        if (AccTM_Baja_lst.size() > 0) {
                            // Eliminamos los usuarios en el equipo de cuentas de los clientes
                            delete AccTM_Baja_lst;
                        }
                    }
                }
            }

            // Lógica para cuando se cambian roles en los usuarios de las oficinas de ventas
            if (ATM_Queue_Cambios_lst.size() > 0) {
                if (CuentasCambioIds_set.size() > 0) {
                    // Obtiene las cuentas que coincidan con el set de cambios
                    Acc_Cambio_lst = objCSQuerys.getAccountsChange(CuentasCambioIds_set, RecordTypeId_str);

                    if (Acc_Cambio_lst.size() > 0) {
                        for (Account acc : Acc_Cambio_lst) {
                            CuentasCambioATMIds_set.add(acc.Id);
                            for (ISSM_ATM_Queue__c atmQL : ATM_Queue_Cambios_lst) {
                                UsuariosIds_set.add(atmQL.Usuario_relacionado__c);
                            }
                        }

                        if (CuentasCambioATMIds_set.size() > 0) {
                            // Obtiene las cuentas que coincidan con el set de cambios por usuario
                            AccTM_Cambio_lst = objCSQuerys.getAccountsChangeByUser(CuentasCambioATMIds_set, UsuariosIds_set);
                        }

                        // Código para que si se cambia el rol de supervisor, se elimina el usuario de todas sus clientes
                        /*if (AccTM_Cambio_lst.size() > 0) {
                            // Eliminamos los usuarios en el equipo de cuentas de los clientes
                            delete AccTM_Cambio_lst;
                        }*/

                        // Este código aplica para cuando se cambia el rol en la oficina de ventas y se aplica para todos sus clientes
                        if (AccTM_Cambio_lst.size() > 0) {
                            for (AccountTeamMember ATM : AccTM_Cambio_lst) {
                                for (ISSM_ATM_Queue__c atmQL : ATM_Queue_Cambios_lst) {
                                    ATM.TeamMemberRole = atmQL.Rol__c; // Rol_str
                                    AccTM_Upd_Cambio_lst.add(ATM);
                                }
                            }
                            if (AccTM_Upd_Cambio_lst.size() > 0) {
                                // Actualizamos los usuarios en el equipo de cuentas de los clientes
                                update AccTM_Upd_Cambio_lst;
                            }
                        }
                    }
                }
            }

            if (Upd_ATM_Queue_Altas_lst.size() > 0) {
                update Upd_ATM_Queue_Altas_lst;
            }

            if (Upd_ATM_Queue_Bajas_lst.size() > 0) {
                update Upd_ATM_Queue_Bajas_lst;
            }

            if (Upd_ATM_Queue_Cambios_lst.size() > 0) {
                update Upd_ATM_Queue_Cambios_lst;
            }
            
        } catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }
    }  
    global void finish(Database.BatchableContext BC)
    {
        List<ISSM_ATM_Queue__c> ATM_Queue_lst = new List<ISSM_ATM_Queue__c>();
        List<ISSM_ATM_Queue__c> Del_ATM_Queue_lst = new List<ISSM_ATM_Queue__c>();
        
        try {
            // Obtiene los registros del objeto "ISSM_ATM_Queue__c" en donde "Listo_para_eliminar = true"
            ATM_Queue_lst = objCSQuerys.getRegQueueToDelete();
            
            if (ATM_Queue_lst.size() > 0) {
                for (ISSM_ATM_Queue__c atmQueue : ATM_Queue_lst) {
                    Del_ATM_Queue_lst.add(atmQueue);
                }
            }
            if (Del_ATM_Queue_lst.size() > 0) {
                delete Del_ATM_Queue_lst;
            }
            programBatch();

        } catch (Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }
    }

    public static void programBatch() {
        // Instanciamos la clase para realizar las consultas necesarias
        ISSM_CSAssignATMQuerys_cls objCSQuerys = new ISSM_CSAssignATMQuerys_cls();

        // Asignación de variables para tiempos de ejecución del proceso scheduler
        String Segundos_str = '0'; // Se ejecuta de acuerdo a los minutos
        String Minuto_01_str = '01'; // Se ejecuta al minuto 01 de cada hora
        String Minuto_10_str = '10'; // Se ejecuta al minuto 10 de cada hora
        String Minuto_20_str = '20'; // Se ejecuta al minuto 20 de cada hora
        String Minuto_30_str = '30'; // Se ejecuta al minuto 30 de cada hora
        String Minuto_40_str = '40'; // Se ejecuta al minuto 40 de cada hora
        String Minuto_50_str = '50'; // Se ejecuta al minuto 50 de cada hora
        String Horas_str = '7-19'; // Se ejecuta entre las 7am y las 7pm
        String Dias_str = '*'; // Se ejecuta todos los días
        String Meses_str = '*'; // Se ejecuta todos los meses
        String Interrogacion_str = '?';
        String Ano_str = '*'; // Se ejecuta todos los años

        String Tiempo_01_str = Segundos_str + ' ' + Minuto_01_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;
        String Tiempo_10_str = Segundos_str + ' ' + Minuto_10_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;
        String Tiempo_20_str = Segundos_str + ' ' + Minuto_20_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;
        String Tiempo_30_str = Segundos_str + ' ' + Minuto_30_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;
        String Tiempo_40_str = Segundos_str + ' ' + Minuto_40_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;
        String Tiempo_50_str = Segundos_str + ' ' + Minuto_50_str + ' ' + Horas_str + ' ' + Dias_str + ' ' + Meses_str + ' ' + Interrogacion_str + ' ' + Ano_str;

        String AdminCuentasATM_str = 'ISSM_AdminCuentasATM_sch '; // + System.today();
        try {
            List<CronTrigger> CronTrigger_lst = new List<CronTrigger>();
            // Obtiene el job que coincida con el nombre
            CronTrigger_lst = objCSQuerys.getJobCronTrigger(AdminCuentasATM_str);

            if (CronTrigger_lst.size() == 0) {
                String job_01_Id = System.schedule(AdminCuentasATM_str + '_01', Tiempo_01_str, new ISSM_AdminCuentasATM_sch());
                String job_10_Id = System.schedule(AdminCuentasATM_str + '_10', Tiempo_10_str, new ISSM_AdminCuentasATM_sch());
                String job_20_Id = System.schedule(AdminCuentasATM_str + '_20', Tiempo_20_str, new ISSM_AdminCuentasATM_sch());
                String job_30_Id = System.schedule(AdminCuentasATM_str + '_30', Tiempo_30_str, new ISSM_AdminCuentasATM_sch());
                String job_40_Id = System.schedule(AdminCuentasATM_str + '_40', Tiempo_40_str, new ISSM_AdminCuentasATM_sch());
                String job_50_Id = System.schedule(AdminCuentasATM_str + '_50', Tiempo_50_str, new ISSM_AdminCuentasATM_sch());
            }
        } catch(Exception ex) {
            System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
        }
    }
}