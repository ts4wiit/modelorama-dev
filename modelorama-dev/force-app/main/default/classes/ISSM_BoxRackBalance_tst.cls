/****************************************************************************************************
    General Information
    -------------------
    author:     Ernesto Gutiérrez
    email:      egutierrez@avanxo.com
    company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description:
    Test class to cover ISSM_BoxRackBalance_cls 
	
    Information about changes (versions)
    ================================================================================================
    Number    Dates                Author                       Description          
    ------    --------             --------------------------   ------------------------------------
    1.0       07-September-2018    Ernesto Gutiérrez            Class Creation
    ================================================================================================
****************************************************************************************************/
@isTest 
public class ISSM_BoxRackBalance_tst {

    static ONTAP__Order__c ObjOrder{get;set;}
    static ONTAP__Order__c ObjOrder2{get;set;}
    static ONTAP__Product__c ObjProduct{get;set;}
  	static ISSM_ProductByPlant__c ObjProductByPlant{get;set;}
    static ONTAP__Product__c ObjProduct2{get;set;}
    static ONTAP__Product__c ObjProduct3{get;set;}
    static Account ObjAccount{get;set;}//Cuentas
    static ONCALL__Call__c ObjCall{get;set;}//Llamadas
    static ONTAP__Order_Item__c ObjOI{get;set;}
    static ONTAP__Order_Item__c ObjOI2{get;set;}
    static ONTAP__Order_Item__c[] LstOI{get;set;}
    static ONTAP__Product__c[] LstProduc{get;set;}
    static ApexPages.StandardController sc{get;set;}
    static String IdOrderr{get;set;}
    static String IdAccnt{get;set;}
    static ONTAP__Order_Item__c[] lstIds{get;set;}
    static ISSM_Bonus__c ObjBns{get;set;}
    static ISSM_Bonus__c ObjBns2{get;set;}
    static ISSM_Bonus__c[] LstBns{get;set;}
    static EmptyBalanceB__c ObjEB{get;set;}
    static EmptyBalanceB__c[] LstObjEB2{get;set;}
    static EmptyBalanceB__c[] LstEB{get;set;}
    static ISSM_NewOrder_ctr.PriceCondition[] LstPC{get;set;}
    public static ISSM_OnCallQueries_cls CTRSOQL    = new ISSM_OnCallQueries_cls();
    static Map<Integer,String> ZTPMMap = new Map<Integer,String>();
    static ISSM_ProductByOrg__c ObjPBO{get;Set;}
    static Account ObjAccOrg{get;set;}//Cuentas
    static Account ObjAccDistributionCenter{get;set;}//Cuentas
    public static ISSM_CreateOrder_ctr createOrder = new ISSM_CreateOrder_ctr();
    public static ISSM_PricingMotor_cls priceMotor = new ISSM_PricingMotor_cls();
    static ONTAP__Product__c[] lstObjProduct{get;set;}
    
    public static void StartValues(){
        //LstAccount = Test.loadData(Account.sObjectType, 'ISSM_AccountDataTst');

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = Label.ISSM_AccessTokenTst,
            ISSM_EndPoint__c = Label.ISSM_EPPricingEngine,
            ISSM_Method__c = Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c = Label.ISSM_HeaderContentTypeTst,
            Name = Label.ISSM_ConfigurationWSTst);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c=Label.ISSM_EPMaterialAvailable,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c=Label.ISSM_HeaderContentTypeTst,
            Name=Label.ISSM_ConfigurationMatAvailable);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EPCreateOrder,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json; charset=UTF-8',
            Name=Label.ISSM_ConfigCreateOrder);

        insert new ISSM_PriceEngineConfigWS__c(ISSM_AccessToken__c = '',
            ISSM_EndPoint__c= Label.ISSM_EndPointDeals,
            ISSM_Method__c=Label.ISSM_MethodTst,
            ISSM_HeaderContentType__c='application/json;',
            Name='ConfigWSDeals');


        Map<String,Schema.RecordTypeInfo> MapSchema = Schema.SObjectType.ONTAP__Product__c.RecordTypeInfosByName;
        String StrRecType = MapSchema.get(Label.ISSM_Products).getRecordTypeId();
        LstObjEB2 = new List<EmptyBalanceB__c>();
        LstOI = new List<ONTAP__Order_Item__c>();
        LstBns = new List<ISSM_Bonus__c>();
        LstEB = new List<EmptyBalanceB__c>();
        LstPC = new List<ISSM_NewOrder_ctr.PriceCondition>();

        Id RecTypeId = CTRSOQL.getRecordTypeId('ONCALL__Call__c','ISSM_TelesalesCall');

        ObjAccount = new Account(
            Name = 'TST',
            ONTAP__Main_Phone__c='5510988765',
            ISSM_MainContactE__c=true,
            ONTAP__SAPCustomerId__c='100153987',
            ONTAP__SalesOgId__c='3116',
            ONTAP__ChannelId__c='1'
        );
        insert ObjAccount;
        
        

        ObjAccOrg = new Account(Name = 'Cuenta Org',
            ONTAP__Main_Phone__c= '12345678',
            ONTAP__SalesOgId__c = '3116',
            RecordTypeId = CTRSOQL.getRecordTypeId('Account','SalesOrg'));
        insert ObjAccOrg;
        
        
        ObjAccDistributionCenter = new Account(Name = 'CMM METEPEC',
            ONTAP__Main_Phone__c= '12345678',
            ONTAP__SalesOgId__c = '3116',
            ISSM_DistributionCenter__c = 'FG00',
            RecordTypeId = CTRSOQL.getRecordTypeId('Account','ISSM_DistributionCenter'));
        insert ObjAccDistributionCenter;

        ObjCall = new ONCALL__Call__c(
            Name            = 'TST',
            ONCALL__POC__c  = ObjAccount.Id,
            RecordTypeId = RecTypeId,
            ISSM_ValidateOrder__c=false
        );
        insert ObjCall;

        ObjProduct2 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Envase CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '2000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0
            
        );
        insert ObjProduct2;

        ObjProduct3 = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'Caja CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='4000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0
            
        );
        insert ObjProduct3;

        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  = '3000020',
            ONTAP__UnitofMeasure__c     = Label.ISSM_BOX,
            ISSM_Is_returnable__c       = true,
            ISSM_Discount__c            = 5.0,
            ISSM_Empties_Material__c    = ObjProduct2.Id,
            ISSM_BoxRack__c             = ObjProduct3.Id

        );
        insert ObjProduct;


        ObjPBO = new ISSM_ProductByOrg__c(
            ISSM_RelatedAccount__c      =   ObjAccOrg.Id,
            ISSM_AssociatedProduct__c   =   ObjProduct.Id,
            ISSM_Empties_Material__c    =   ObjProduct2.Id,
            ISSM_EmptyRack__c           =   ObjProduct3.Id,
            RecordTypeId                =   CTRSOQL.getRecordTypeId('ISSM_ProductByOrg__c','ISSM_OnCallProduct'));

        insert ObjPBO;


        ObjOrder = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder;


        ObjOrder2 = new ONTAP__Order__c(
            ONCALL__OnCall_Account__c=ObjAccount.Id,
            ONCALL__Call__c = ObjCall.Id,
            ONTAP__OrderStatus__c = Label.ISSM_OrderStatus2,
            ONCALL__Origin__c= Label.ISSM_OriginCall,
            ONTAP__DeliveryDate__c= System.today()+1,
            ONTAP__BeginDate__c= System.today()
        );
        insert ObjOrder2;

        ObjOI = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_Is_returnable__c = true,
            ISSM_Uint_Measure_Code__c= 'CS',
            ONCALL__SAP_Order_Item_Number__c = '3000005',
            ISSM_EmptyMaterial__c   = '2000005',
            ISSM_EmptyRack__c = '4000005',
            ISSM_ComboNumber__c = '8768:8768'
        );
        insert ObjOI;


        ObjOI2 = new ONTAP__Order_Item__c(
            ONTAP__CustomerOrder__c = ObjOrder2.Id,
            ONCALL__OnCall_Product__c  = ObjProduct.Id,
            ONCALL__OnCall_Quantity__c = 10,
            ISSM_UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c = true
        );

        insert ObjOI2;

        ObjBns = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct.Id,
                            ISSM_SAPOrderNumber__c  = '300',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns;


        ObjBns2 = new ISSM_Bonus__c(
                            ISSM_AppliedTo__c       = ObjAccount.Id,
                            ISSM_Product__c         = ObjProduct2.Id,
                            ISSM_SAPOrderNumber__c  = '200',                            
                            ISSM_BonusQuantity__c   = 5,
                            ISSM_IsBonusApplied__c  = false
        );
        insert ObjBns2;


        ObjEB = new EmptyBalanceB__c(
                            Account__c         = ObjAccount.Id,
                            SAPOrderNumber__c  = '1',
                            DueDate__c         = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c  = 2,
                            MaterialProduct__c = 'ENVASE',
                            EmptyBalanceKey__c = '321',
                            Product__c         = ObjProduct.Id,
                            Process__c         = false
        );

        LstObjEB2.add(new EmptyBalanceB__c(
                            Account__c = ObjAccount.Id,
                            SAPOrderNumber__c = '2',
                            DueDate__c = System.today()+1,
                            BoxesNumRequest__c = 3,
                            BoxesNumReturn__c = 2,
                            MaterialProduct__c = 'CAJA',
                            EmptyBalanceKey__c = '4321',
                            Product__c = ObjProduct.Id,
                            Process__c = false
        ));
        insert ObjEB;
        
        lstObjProduct = new List<ONTAP__Product__c>();
        ObjProduct = new ONTAP__Product__c(
            RecordTypeId                = StrRecType,
            ONTAP__MaterialProduct__c   = 'CORONA EXTRA BOTE H-C 24/355 ML CT',
            ONCALL__Material_Number__c  ='3000020',
            ONTAP__UnitofMeasure__c  = Label.ISSM_BOX,
            ISSM_Is_returnable__c  =true,
            ISSM_Discount__c =5.0,
            ISSM_Empties_Material__c = ObjProduct2.Id,
            ISSM_BoxRack__c = ObjProduct3.Id,
            ISSM_ComboNumber__c = '1',
            ONCALL__OnCall_Product__c = ObjProduct3.Id,
            ISSM_QuantityInput__c = '3.2'
        );
        insert ObjProduct;
		lstObjProduct.add(ObjProduct);
		
        LstBns.add(ObjBns);
        LstEB.add(ObjEB);
        LstOI.add(ObjOI);
        LstPC.add(new ISSM_NewOrder_ctr.PriceCondition(Label.ISSM_ZPVM,5.0,10));
        Apexpages.currentPage().getParameters().put(Label.ISSM_IdOrder, ObjOrder.Id);
        lstIds  = new List<ONTAP__Order_Item__c>();
        lstIds.add(ObjOI);
        ZTPMMap.put(3000005, '');
        
        
        //lstObjProduct = new List<ONTAP__Product__c>();
        ObjProductByPlant = new ISSM_ProductByPlant__c(
     		ISSM_AssociatedProduct__c = ObjProduct.Id,
     		ISSM_AssociatedEmpty__c = ObjProduct2.Id,
     		ISSM_DistributionCenter__c  = ObjAccDistributionCenter.Id
        );
        insert ObjProductByPlant;
		//lstObjProduct.add(ObjProduct);

    }

    @isTest static void TestOrderTaker(){
        //GET RECORDS VALUES
        StartValues();
        ISSM_EmptyBalanceProcess_cls.WrpEmptyBalance WrpEB;
        ISSM_EmptyBalanceProcess_cls.WrpEmptyBalance WrpEB2;
        ISSM_BoxRackBalance_cls.WrpBoxRackBalance WrpBoxRackBalance;
        ISSM_NewOrder_ctr CTR = new ISSM_NewOrder_ctr();
        String StrPay = ISSM_NewOrder_ctr.StrPayMethod;
                    
        Test.startTest();

            List<SelectOption> SelOpts  = CTR.LevelList;
            List<SelectOption> SelOpts2 = CTR.getCallResultValues;
            List<SelectOption> SelOpts3 = CTR.getCallLaterValues;
            String result_Str1 = CTR.getAllPicklistValues();
            String result_Str2 = CTR.getAllSelectedValues();
            System.assertEquals(true,SelOpts.size()>0);
            System.assertEquals(true,SelOpts2.size()>0);
            System.assertEquals(true,SelOpts3.size()>0);

            CTR.StrCallResult     = String.valueOf(SelOpts2[0].getValue());
            CTR.StrSgtTimeCalling = String.valueOf(SelOpts3[0].getValue());
            System.assertNotEquals(null, CTR.StrCallResult);
            System.assertNotEquals(null, CTR.StrSgtTimeCalling);


            /////
            String s1 = CTR.StrCondition = 's1';
            String s2 = ISSM_NewOrder_ctr.StrTotal;
            String s3 = ISSM_NewOrder_ctr.StrTotalDesc;
            String s4 = ISSM_NewOrder_ctr.StrTotalIVA;
            /////

            ONTAP__Order_Item__c[] OBJPRODI = ISSM_NewOrder_ctr.getOrderItem(ObjOrder.Id);
            System.assertEquals(false,OBJPRODI.size()>0);

            ISSM_FillRate__c[] OBJFR = ISSM_NewOrder_ctr.getFillRate(lstIds, lstIds, lstIds, ObjOrder.Id, ObjAccount.Id);
            System.assertEquals(true,OBJFR.size()>0);

            ISSM_Bonus__c[] OBJPRODBNS      = ISSM_NewOrder_ctr.readBonuses(ObjAccount.Id);
            System.assertEquals(true,OBJPRODBNS.size()>0);
            
            ISSM_Bonus__c[] OBJPRODBNS2     = ISSM_NewOrder_ctr.processBonusSelected(OBJPRODBNS,Double.valueOf(1000.10),Double.valueOf(100.20),String.valueOf(ObjOrder.Id),'');
            System.assertEquals(true,OBJPRODBNS2.size()>0);
            
            LstProduc =  new List<ONTAP__Product__c>();
            WrpEB2 = ISSM_NewOrder_ctr.getEmptyBalance(LstOI,OBJPRODBNS2,ObjAccount.Id,ObjOrder.Id,LstProduc);
            System.assertEquals(true,WrpEB2 != null);
            
            //***************** WrpBoxRackBalance
            WrpBoxRackBalance = ISSM_NewOrder_ctr.getBoxRackBalance(LstOI,OBJPRODBNS2,ObjAccount.Id,ObjOrder.Id,lstObjProduct);
            //System.assertEquals(true,WrpBoxRackBalance != null);
            //***************** WrpBoxRackBalance
            
            ISSM_NewOrder_ctr.WprSuggPushCovered OBJPROD = ISSM_NewOrder_ctr.select_getProducts(ObjAccount.id, '3116','00000000');
            //System.assertEquals(false,OBJPROD.size()>0);

            CTR.updateAccount();
            CTR.noAction();
            ONTAP__Product__c[] OBJPROD2 = ISSM_NewOrder_ctr.select_getProductsONTAP(ObjAccount.ONTAP__SalesOgId__c);
            ONTAP__Product__c[] OBJPROD3 = ISSM_NewOrder_ctr.select_getProductsInvType(ObjAccount.ONTAP__SalesOgId__c,'Y029',ObjAccount.Id,'EXTRA');

            ObjAccount = ISSM_NewOrder_ctr.updateAccountAJS(ObjAccount.Id,'COMENTARIO PRUEBA','5555555555','5555555555',
                                                           '5555555555','tst@emailtst.com',true,true,true,true,true, new List<String>());
            String StrCall    = ISSM_NewOrder_ctr.updateCall(ObjCall.Id,CTR.StrCallResult,'','COMENTARIO PRUEBA');

            ISSM_NewOrder_ctr.RefreshOrder(ObjOrder.Id); 
            
            String[] LstStr = ISSM_NewOrder_ctr.endCallWithoutOrder(ObjOrder.Id,ObjCall.id,'5');
            System.assertEquals(true,!LstStr.isEmpty());
            
            String[] LstStr2= ISSM_NewOrder_ctr.endCallWithoutOrderOnTap(ObjOrder2.Id);
            System.assertEquals(true,!LstStr2.isEmpty());

            String[] LstStr3 = ISSM_NewOrder_ctr.endCallModernChannel(ObjOrder2.Id, ObjAccount.Id, ObjCall.Id,LstOI, '5','COMENTARIO PRUEBA',
                                                                     '12345QAZ','Y013','5555555555','5555555555','5555555555',
                                                                     'tst@emailtst.com',true,true,true,true,true, new List<String>(),'1');
            System.assertEquals(true,!LstStr3.isEmpty());
            
        Test.stopTest();
    }

}