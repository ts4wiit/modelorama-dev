/****************************************************************************************************
    General Information
    -------------------
    author:     Andrés Morales
    email:      lmorales@avanxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Permite clasificar las imágenes para determinar donde se mostrarán en el PDF

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                    	Description          
    ------    --------        --------------------------   	-----------------------------------------
    1.0       16/08/2017      Andrés Morales				Class created
    ================================================================================================
****************************************************************************************************/

public with sharing class MDRM_Expansor_Pdf_Config_ctr {
	public list<Attachment> images {get;set;}

	private final Account acc;
    
    public MDRM_Expansor_Pdf_Config_ctr(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
        getImages();
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Texto del error'));
    }

    public void getImages(){
        List<Attachment> images = [SELECT Id, Name, Description 
        							FROM Attachment 
        							WHERE ParentId = :this.acc.Id 
        							AND ContentType LIKE 'image%'];
        this.images = images;
    }

    public void config_PDF(){
		Integer intCounter =0;

        Map<String, Integer> mapCounters = new Map<String, Integer>{'Croquis' => 1,
        		'Volumen'  => 1,
        		'ZPotenciales' => 1,
        		'NivelSE' => 1,
        		'Reporte' => 1,
        		'Estudio1' => 1,
				'Estudio2' => 1,
        		'Entorno' => 1,
        		'Vistas' => 1,
        		'Levantamiento' => 1,
                'Layout' => 1,
                'Fachada' => 1,
                'Render' => 1,
                'Presupuesto' => 1,
                'Operando' => 1};

        try {
            for(Attachment attch : this.images){
	            
	        	if (mapCounters.containsKey(attch.Description)){
	        		intCounter = mapCounters.get(attch.Description);
	        		mapCounters.put(attch.Description, intCounter + 1);	            	
	        	}
	        	else{
	        		intCounter = 1;
	        	}
	        	
	        	//Take the extensión of file
	        	String strExt = attch.Name.substringafter('.');       	

	            //Make the new name
	            if (attch.Description != ''){
	            	attch.Name = attch.Description + '_' + String.valueOf(intCounter) + '.' + strExt;	
	            }
			}

            Database.update(images);              
        	
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            //return null;
        }

    }

    public PageReference save_clasification() {
        PageReference pagRef = null;

        config_PDF();

        //Redireccionar a página de cuenta
		pagRef = new PageReference('/' + this.acc.id);
		pagRef.setRedirect(true);  

        return pagRef;
    }

    public PageReference PDF_generate(){
		PageReference pagRef = null;

        config_PDF();

        //Redireccionar a página de cuenta
		pagRef = new PageReference('/apex/MDRM_Expansor_Pdf_pag?id=' + this.acc.id);
		pagRef.setRedirect(true); 

		return pagRef;
    }

}