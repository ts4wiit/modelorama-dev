/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Controller used for account external objects sincronization

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       04-08-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_CallBatch_ctr {

    private final sObject mysObject;
    List<sObject> listAccount;
    ISSM_Account_cls accountCls;
    Map<String, Account> mapAccounts;
    List<sObject> orders;
    String sapCustomerId;
    Map<String, ONTAP__Order__c> mapOrders;
    ISSM_Order_cls orderCls;
    String strOrderNum ='';
     String strOrderNumFinal ='';
  
  

    /**
     * Class constructor                                     
     */
    public ISSM_CallBatch_ctr(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        system.debug( '\n\n  ****** mysObject = ' + mysObject+'\n\n' );
        listAccount = new List<SObject>();
        listAccount.add(mysObject);
        sapCustomerId = ((Account)mysObject).ONTAP__SAPCustomerId__c;
        accountCls = new ISSM_Account_cls();
        orderCls = new ISSM_Order_cls();
        Id devRecordTypeId;
        try{
            devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
        } catch(NullPointerException e){
            devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
        }
        orders =[SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false and ONTAP__OrderAccount__r.ONTAP__SAPCustomerId__c=: sapCustomerId and RecordTypeId=: devRecordTypeId];
    }

    /** WP 20180108: This method is deprecated
     * Method that call the sincronization process                                     
     */
    /* autoRun original
    public PageReference autoRun() {
 
        String theId = ApexPages.currentPage().getParameters().get('id');
        List<ISSM_ObjectInterface_cls> sObjectInterfaceList = new List<ISSM_ObjectInterface_cls>();
        
        sObjectInterfaceList.add(new ISSM_Asset_cls());
        sObjectInterfaceList.add(new ISSM_EmptyBalance_cls());
        sObjectInterfaceList.add(new ISSM_OpenItem_cls());
        sObjectInterfaceList.add(new ISSM_Kpi_cls());
        sObjectInterfaceList.add(new ISSM_Order_cls());
        
        mapOrders = orderCls.getMapOrderOrderNumber(orders);
        List<salesforce_ontap_order_item_c__x> orderItems_lst = ISSM_OrderItemExtern_cls.getOrderItemsExten(mapOrders.keySet());
        system.debug( '\n\n  ****** orderItems_lst = ' + orderItems_lst+'\n\n' );

        ISSM_OrderItem_cls orderItemsList = new ISSM_OrderItem_cls(orderItems_lst, mapOrders);
        
        
        mapAccounts = accountCls.getMapAccountSapCustomerId(listAccount);
        for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
            sObjectInterface.createObject(mapAccounts);
        }
        for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
            sObjectInterface.save();
        }

        
        orderItemsList.save();
 
        // Redirect the user back to the original page
        PageReference pageRef = null;
        if(theId != null){
            pageRef = new PageReference('/' + theId);
            pageRef.setRedirect(true);
        }
        return pageRef;
 
    }*/
    
    public PageReference syncObjects() { 
 
        String theId = ApexPages.currentPage().getParameters().get('id');
        List<ISSM_ObjectInterface_cls> sObjectInterfaceList = new List<ISSM_ObjectInterface_cls>();
        mapAccounts = accountCls.getMapAccountSapCustomerId(listAccount); // WP se reubica...
        sObjectInterfaceList.add(new ISSM_Asset_cls());
        sObjectInterfaceList.add(new ISSM_Kpi_cls());
        sObjectInterfaceList.add(new ISSM_Order_cls());
        sObjectInterfaceList.add(new ISSM_EmptyBalance_cls());
        sObjectInterfaceList.add(new ISSM_OpenItem_cls());
        
        for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
            // system.debug('*** Valores de mapAccount en llamado: '+mapAccounts );
            // system.debug('*** Interface a ejecutar: '+sObjectInterface);
            sObjectInterface.createObject(mapAccounts);
        }
        
        // Guardado de registros.
        for(ISSM_ObjectInterface_cls sObjectInterface: sObjectInterfaceList){
            sObjectInterface.save();
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Sincronizando vista 360... Por favor espere un momento. '));
 
        // Redirect the user back to the original page
        return null; 
    }
    
    public PageReference syncOrderItemsOnly() {
 
        String theId = ApexPages.currentPage().getParameters().get('id');
        
        // Sección de procesamiento de los Order Items
        Id devRecordTypeId;
        try{
            devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall Order').getRecordTypeId();
        } catch(NullPointerException e){
            devRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByName().get('OnCall_Order').getRecordTypeId();
        }
        system.debug('*** Id de la cuenta a procesar en Order Items: '+theId);
        orders =[SELECT Id,ONCALL__SAP_Order_Number__c FROM ONTAP__Order__c where ISSM_OnCall__c = false and ONTAP__OrderAccount__c=: theId and RecordTypeId=: devRecordTypeId and LastModifiedDate = TODAY];
        
        system.debug('*** Pedidos a procesar sus items: '+orders.size());
         
        mapOrders = orderCls.getMapOrderOrderNumber(orders);
        System.debug('**TEST vALu : '+mapOrders.values());
        System.debug('**TEST : '+mapOrders.size());
        //system.debug('*** Items en mapa: '+mapOrders.size());
        //####################################################################
        //redefinición para cuando el tamaño de Pedidos sea superior a 80
        Integer intValueFinal = 0;
        Decimal intNumIterationDivision = 0.0;
        Decimal intNumIteration = 0.0;
        Integer intSize = mapOrders.size();
        Map<String, ONTAP__Order__c> orderMap = new Map<String, ONTAP__Order__c>();
        ONTAP__Order__c order;

        salesforce_ontap_order_item_c__x[] orderItems_lst = new List<salesforce_ontap_order_item_c__x>();
        salesforce_ontap_order_item_c__x[] orderItems_lst2 = new List<salesforce_ontap_order_item_c__x>();

        //si son mas de 80 pedidos
        if(intSize>50 || Test.isRunningTest()){
            //se obtienen el numero de iteraciones que se deben hacer de acuerdo al numero de pedidos/80
            intValueFinal = 50;
            intNumIterationDivision  = decimal.Valueof(mapOrders.size()) / decimal.valueof(intValueFinal);
            intNumIteration          = Integer.valueOf(Math.ceil(intNumIterationDivision));

            //Inicia la iteracion de acuerdo al resultado obtenido en intNumIteration
            for(Integer j = 0; j < intNumIteration ; j++){
                System.debug('#j# '+j);
                System.debug('#intNumIteration# '+intNumIteration);
                //Recorre los 80 pedido de ese bloque
                if(orders.size()<50)
                    intValueFinal = orders.size(); 

                for(Integer x = 0; x < intValueFinal ; x++){
                    System.debug('x: '+x);
                    System.debug('intValueFinal: '+intValueFinal);
                    order = (ONTAP__Order__c)orders.get(0); 
                    orderMap.put(order.ONCALL__SAP_Order_Number__c, order);
                    orders.remove(0);       
                }
                //se envia los items de la iteración y posteripormente se reinicia el mapa
                orderItems_lst2 = new List<salesforce_ontap_order_item_c__x>();
                orderItems_lst2 = ISSM_OrderItemExtern_cls.getOrderItemsExten(orderMap.keySet());
                orderMap        = new Map<String, ONTAP__Order__c>();
                orderItems_lst.addAll(orderItems_lst2);
            } 
        //si son menos de 80  procesa normal la info       
        }else{
            orderItems_lst = ISSM_OrderItemExtern_cls.getOrderItemsExten(mapOrders.keySet());
        }
        
        //####################################################################

        //List<salesforce_ontap_order_item_c__x> orderItems_lst = ISSM_OrderItemExtern_cls.getOrderItemsExten(mapOrders.keySet());
        //system.debug( '\n\n  ****** orderItems_lst size = ' + orderItems_lst.size()+'\n\n' );
        ISSM_OrderItem_cls orderItemsList = new ISSM_OrderItem_cls(orderItems_lst, mapOrders);
        // Fin de Sección de procesamiento de los Order Items
        
        if(!orderItems_lst.isEmpty()){
            orderItemsList.save();
        }
 
        // Redirect the user back to the original page
        PageReference pageRef = null;
        if(theId != null){
            pageRef = new PageReference('/' + theId);
            pageRef.setRedirect(true);
        }
        return pageRef;
 
    }
}