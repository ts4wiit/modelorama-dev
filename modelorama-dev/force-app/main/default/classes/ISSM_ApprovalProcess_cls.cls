/******************************************************************************* 
* Company       	: 	Avanxo México
* Author			: 	Oscar Alvarez
* Proyecto			: 	AbInbev - Trade Revenue Management
* Descripción		: 	Make the sending of combos for approval and approval of combos.
*
* No.       Fecha               Autor                      Descripción
* 1.0       15-Mayo-2018        Oscar Alvarez              Creación
*******************************************************************************/
public class ISSM_ApprovalProcess_cls {
    /**
    * @description   : Method that allows approval of combos
    * 
    * @param      strId             id combo to approve 
    *
    * @return none
    */
    public static void submitAndProcessApprovalRequest(String strId) {
        
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments(Label.TRM_CommentsApproval);
        req2.setAction(Label.TRM_ApprovalAction);
        System.debug(strId);
        Id workItemId = getWorkItemId(strId);
        req2.setWorkitemId(workItemId);        
        try{
            Approval.ProcessResult result2 =  Approval.process(req2);
            System.debug('###submitAndProcessApprovalRequest### '+result2);
        }catch(Exception exc){
            System.debug('EXCEPTION IN ISSM_ApprovalProcess_cls.submitAndProcessApprovalRequest-> '+exc.getStackTraceString());
        }
    }

    /**
    * @description	: Method that allows sending to approval of combos
    * 
    * @param    strId        ID Object send to approve 
    * @param    comment      Comments attached to the approval 
    * @param    nameProcess  API name of the approval process 
    * @param    userId       ID User who sends the approval 
    *
    * @return none
    */
    public static void submitForApproval(String strId, String comment, String nameProcess, String userId) {
        // Create an approval request for the Combo
        Approval.ProcessSubmitRequest PSRCOMBO = new Approval.ProcessSubmitRequest(); 
        PSRCOMBO.setComments(comment);
        PSRCOMBO.setObjectId(strId);
        PSRCOMBO.setSubmitterId(userId);
        PSRCOMBO.setProcessDefinitionNameOrId(nameProcess);
        
        try{
            Approval.ProcessResult result = Approval.process(PSRCOMBO);
            System.debug('###submitForApproval### '+result);
        }catch(Exception exc){
            System.debug('EXCEPTION IN ISSM_ApprovalProcess_cls.submitForApproval-> '+exc.getStackTraceString());
        }
    }


    /**
    * @description   : Method allows to obtain the id of the work sent
    * 
    * @param    targetObjectId    id of combo to send 
    *
    * @return   Id of work
    */
    private  static Id getWorkItemId(Id targetObjectId){
        Id retVal = null;
        for(ProcessInstanceWorkitem workItem  : [SELECT p.Id FROM ProcessInstanceWorkitem p WHERE p.ProcessInstance.TargetObjectId =: targetObjectId]){ 
            retVal  =  workItem.Id;
        }
        return retVal;
    }    
}