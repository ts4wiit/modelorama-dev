/**
 * This class serves as batch class for AllMobileCatUserTypeOperationClass class.
 * <p /><p />
 * @author Alberto Gómez
 */
global class AllMobileCatUserTypeBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	/**
	 * This method returns a list with CatUserType records to be deleted.
	 *
	 * @param objBatchableContext	Database.BatchableContext
	 */
	global Database.QueryLocator start(Database.BatchableContext objBatchableContext) {
		return Database.getQueryLocator(AllMobileStaticVariablesClass.STRING_BATCHABLE_DATABASE_GET_QUERY_LOCATOR_SALESFORCE_ALL_CAT_USER_TYPES);
	}

	/**
	 * This method execute a batch to delete the CatUserType records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 * @param List<AllMobileCatUserType__c>
	 */
	global void execute(Database.BatchableContext objBatchableContext, List<AllMobileCatUserType__c> lstAllMobileCatUserTypeSF) {
		AllMobileCatUserTypeOperationClass.syncDeleteAllMobileCatUserTypeInSF(lstAllMobileCatUserTypeSF);
	}

	/**
	 * This method finish the execution of batch which deletes the CatUserType records.
	 *
	 * @param objBatchableContext Database.BatchableContext
	 */
	global void finish(Database.BatchableContext objBatchableContext) {
	}
}