/**********************************************************************************
Developed by: Avanxo Mexico
Author: Iván Neria  (IN)
Project:  ABInBev (CDM Rules)
Description: Apex test class for "CDM_FilterExecution_ctr". 
---------------------------------------------------------------------------------
Version       Date         Author                   Description
------ ---------- ---------------------------------------------------------------
1.0       29-May-2018   Iván Neria			   Initial version
***********************************************************************************/
@isTest
public class CDM_FilterExecutionCtr_tst {
    
    public static list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account'];
    public static map<String, RecordType> mapRT = new map<String, RecordType>();
    
    @testSetup static void createData(){
        List<sObject> ls = Test.loadData(MDM_Temp_Account__c.sObjectType, 'MDMTempAccount');
        //Update Flag of new records
        MDM_Temp_Account__c tmp = [SELECT Id, MDM_Control_Check__c, MDM_HasErrors__c FROM MDM_Temp_Account__c ORDER BY NAME DESC LIMIT 1];
        tmp.MDM_Control_Check__c=false;
        update tmp;
        
        //Update Flag of new records that contains error in RULE 0008
        MDM_Temp_Account__c tmp2 = [SELECT Id, MDM_Control_Check__c, MDM_HasErrors__c FROM MDM_Temp_Account__c ORDER BY NAME ASC LIMIT 1];
        tmp2.MDM_HasErrors__c=false;
        update tmp2;
        
        //MDM Rule record
        MDM_Rule__c rule5 = new MDM_Rule__c();
        rule5.MDM_Code__c = 'R0008';
        rule5.MDM_IsActive__c= true;
        rule5.MDM_Object__c = 'MDM_Temp_Account__c';
        rule5.MDM_Rule_Description__c = 'Número de cuenta anterior (ALTKN) no debe de estar vacío.';
        rule5.MDM_Error_Message__c = 'El cliente no tiene número de cuenta anterior (ALTKN).';
        upsert rule5;
        
        
        //Validation result Record
        MDM_Validation_Result__c val = new MDM_Validation_Result__c();
        val.ANRED_DESCRIPTION__c = 'El valor no existe dentro de su catálogo.';
        val.ANRED_HAS_ERROR__c =true  ;
        val.MDM_Temp_AccountId__c = tmp2.Id;
        val.MDM_RuleId__c	=rule5.Id;
        insert val;
        
        //MDM Account Record
        MDM_Account__c acc = new MDM_Account__c();
        acc.NAME1__c =	'IVAN';
        acc.Unique_Id__c = '01000582731100005';
        insert acc;
        
        //CDM Temp Interlocutor Record
        CDM_Temp_Interlocutor__c inter = new CDM_Temp_Interlocutor__c();
        inter.KUNNR__c	='0100056358';
        insert inter;
        
        //CDM Temp Tax Record
        CDM_Temp_Taxes__c tax = new CDM_Temp_Taxes__c();
        tax.KUNNR__c ='0100056358';
        tax.TAXKD__c ='1';
        insert tax;
        
        for(RecordType rt : lstRT)
          mapRT.put(rt.DeveloperName, rt); 
        
         //create sales Organization 
        Account salesOrg 					= new Account();
        salesOrg.RecordTypeId         		= mapRT.get('SalesOrg').Id;
        salesOrg.Name 						= 'CMM Hidalgo';
        salesOrg.ONTAP__SAPCustomerId__c 	= '3108';
        insert salesOrg;
        
        //Create sales Office
        Account salesOffice 				= new Account();
        salesOffice.RecordTypeId         	= mapRT.get('SalesOffice').Id;
        salesOffice.Name 					= 'CMM Actopan';
        salesOffice.ONTAP__SalesOgId__c 	= '3108';
        salesOffice.ONTAP__SAPCustomerId__c	= 'GC01';
        salesOffice.ISSM_ParentAccount__c	= salesOrg.Id;
        insert salesOffice;  
        
        
        Integer auxCont = 0;
        Set<String> lstSetChannel = new Set<String>{'TRADICIONAL', 'MODERNO', 'INTERCOMPAÑIA','EXPORTACIONES','AGENCIA PARTICULAR','COMPAÑIA RELACIONADA','EMPLEADOS','CLIENTES VARIOS','MATERIA PRIMA','INTRAGRUPO'};
            List<DistributionChannel__c> listInsertChannel = new List<DistributionChannel__c>();        
        for (String nameChannel : lstSetChannel){
            DistributionChannel__c channel = new DistributionChannel__c(name = nameChannel, Code__c = auxCont > 9 ?  String.valueOf(auxCont) : '0' + String.valueOf(auxCont++));
            listInsertChannel.add(channel);
        }
        upsert listInsertChannel;
        auxCont = 0;
        //create Sector
        Set<String> lstSetSector = new Set<String>{'Común', 'Cerveza', 'Agua','Refrescos','Jugos','Promocionales','Otros','Empaques','Servicios','Materia Prima'};
        List<Sector__c> listInsertSector = new List<Sector__c>();        
        for (String nameSector : lstSetSector){
            Sector__c sector = new Sector__c(name = nameSector, Code__c = auxCont > 9 ?  String.valueOf(auxCont) : '0' + String.valueOf(auxCont++));
            listInsertSector.add(sector);
        }
        upsert listInsertSector;
        
        SalesArea__c area = new SalesArea__c ();
        area.SPARTId__c		=listInsertSector[0].Id;
        area.VKBURId__c		=salesOffice.Id;
        insert area;
        
        
    }
    static testmethod void generalTestClass() {
        Test.startTest();
        // Assert validation in class 'CDM_UpdateRelaionshipsBch_tst'
        CDM_FilterExecution_ctr.ExecuteRelationships();
        // Assert validation in  class 'CDM_ReprocessBatch_bch'
        CDM_FilterExecution_ctr.ExecuteReprocessRules();
        //Assert Validation for method 'getSalesOffOnChange(String)', must returns all SalesOrgs created
        System.assertEquals(new List<String>{'--','GC01'}, CDM_FilterExecution_ctr.getSalesOffOnChange('--'));
        //Assert validation for method 'getRules()' must to return the only one MDM_Rule created
        System.assertEquals(1, CDM_FilterExecution_ctr.getRules().size());
        //Assert validation for method 'getRulesApply()' must to return the only one MDM_Rule created and active
        System.assertEquals(1, CDM_FilterExecution_ctr.getRulesApply().size());
        //Assert validation for method 'getExpenseDetails()' that returns values of GlobalPicklist 'AccountGroupGlobalList'
        System.assertEquals(8, CDM_FilterExecution_ctr.getExpenseDetails().size());
        //Assert validation for method 'getTotalTemp()' must to returns all records createn in static resource(10), but 1 was updated for cover other methods.
        System.assertEquals(9, CDM_FilterExecution_ctr.getTotalTemp());
        //Assert validation for method 'getSalesOff()' must return a default value '--' and the record created
        System.assertEquals(2, CDM_FilterExecution_ctr.getSalesOff().size());
        //Assert validation for method 'getSalesOrg()' must return a default value '--' and the record created
        System.assertEquals(2, CDM_FilterExecution_ctr.getSalesOrg().size());
        //Assert validation for method 'getSector()' must return a default value '--' and the ten records created
        System.assertEquals(11, CDM_FilterExecution_ctr.getSector().size());
        //Assert validation for method 'getChannel()' must return a default value '--' and the ten records created
        System.assertEquals(11, CDM_FilterExecution_ctr.getChannel().size());
        //Assert validation for method 'getKTOKD()' that returns a default value '--' and values of GlobalPicklist 'AccountGroupGlobalList'
        System.assertEquals(9, CDM_FilterExecution_ctr.getKTOKD().size());
        //Assert validation for method 'changeStatus(Code, boolean)' that updates field MDM_IsActive__c in the rule indicated in the first parameter
        CDM_FilterExecution_ctr.changeStatus('R0008', false);
        MDM_Rule__c rule = [SELECT Id, MDM_IsActive__c FROM MDM_Rule__c];
        System.assertEquals(false, rule.MDM_IsActive__c);
        //Assert validation for method WS 'ExecuteProcessRules()' execute rules over records in the parameters
        CDM_FilterExecution_ctr.ExecuteProcessRules(1, new String[]{'Z001'}, '3123', 'FW00', '0', '1' );
        List<MDM_Temp_Account__c> tmpAcc = [SELECT Id FROM MDM_Temp_Account__c WHERE MDM_Control_Check__c=true];
        System.assertEquals(9, tmpAcc.size());
        //Assert validation for method 'updateTotal()' to get total records in MDM_Temp_Account availables due the parameters choosen in the component 'CDM_FilterExecution_cmp'
        CDM_FilterExecution_ctr.updateTotal(new String[]{'Z001'},null,null,null,null);
        List<MDM_Temp_Account__c> tmpAcc2 = [SELECT Id FROM MDM_Temp_Account__c WHERE KTOKD__c='Z001'];
        System.assertEquals(tmpAcc2.size(), CDM_FilterExecution_ctr.updateTotal(new String[]{'Z001'},null,null,null,null));
        Test.stopTest();
    }
    
    //Method delete all created data for this class in the main objects
    static testmethod void massDelete(){
        Test.startTest();
        CDM_FilterExecution_ctr.ExecuteDeleteProcess();
        Test.stopTest();
        //Assert validation applies for 'CDM_MassDeleteValidationRes_bch', 'CDM_MassDeleteTaxes_bch', 'CDM_MassDeleteMDMTempAcc_bch', 'CDM_MassDeleteMDMAccount_bch' & 'CDM_MassDeleteInterlocutors_bch'
		List<MDM_Temp_Account__c> 		tam1 	= [SELECT Id FROM MDM_Temp_Account__c];
        List<MDM_Account__c> 			tam2 	= [SELECT Id FROM MDM_Account__c];
        List<CDM_Temp_Interlocutor__c> 	tam3 	= [SELECT Id FROM CDM_Temp_Interlocutor__c];
        List<CDM_Temp_Taxes__c> 		tam4 	= [SELECT Id FROM CDM_Temp_Taxes__c];
        List<MDM_Validation_Result__c> 	tam5 	= [SELECT Id FROM MDM_Validation_Result__c];
        System.assertEquals(0, tam1.size()); 
        System.assertEquals(0, tam2.size()); 
        System.assertEquals(0, tam3.size()); 
        System.assertEquals(0, tam4.size()); 
        System.assertEquals(0, tam5.size()); 
    }
}