/****************************************************************************************************
    General Information
    -------------------
    author:     Carlos Duque
    email:      cduque@avanxo.com
    company:    Avanxo México
    Project:    ISSM
    Customer:   Grupo Modelo

    Description:
    Class to implements interface used to get external Assets

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       19-07-2017      Carlos Duque                  Class create
    ================================================================================================
****************************************************************************************************/
public with sharing class ISSM_AssetsExtern_cls implements ISSM_ObjectExternInterface_cls{

	/**
	 * get a list ob external object base on a set sapcustomer id and id the record must be erase		                             
	 * @param  Set<String> setSapCustomerId set of sap customer id to request
	 * @param  Boolean isDeleted if the record must be deleted
	 */
	public static List<salesforce_ontap_account_asset_c__x> getList(Set<String> setSapCustomerId, Boolean isDeleted){
		if(!isDeleted){
			if(test.isRunningTest()){
				List<salesforce_ontap_account_asset_c__x> asset_lst = new List<salesforce_ontap_account_asset_c__x>();
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003117',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003118',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003119',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=false, issm_sap_number_c__c='1029120920192'));
				return asset_lst;
			} else {
				return [SELECT Id, ExternalId, createddate__c, id__c, isdeleted__c, 
				issm_salesoffid_c__c, issm_sap_number_c__c, name__c, ontap_account_c__c, 
				ontap_account_r_ontap_sapcustomerid_c__c, ontap_asset_code_c__c, 
				ontap_asset_description_c__c, ontap_asset_ownership_c__c, 
				ontap_asset_status_c__c, ontap_brand_c__c, ontap_cam_vendor_c__c, 
				ontap_capacity_c__c, ontap_componentid_c__c, ontap_ibase_c__c, 
				ontap_inventory_date_c__c, ontap_last_modified_date_sap_c__c, 
				ontap_latitude_c__c, ontap_longitude_c__c, ontap_manufacturer_c__c, 
				ontap_manufacturer_serial_number_c__c, ontap_quantity_c__c, 
				ontap_serial_number_c__c, ontap_warranty_date_c__c, systemmodstamp__c,issm_sapcustomerid_c__c 
				FROM salesforce_ontap_account_asset_c__x 
				where issm_sapcustomerid_c__c in :setSapCustomerId and systemmodstamp__c >= YESTERDAY];
			}
		} else {
			if(test.isRunningTest()){
				List<salesforce_ontap_account_asset_c__x> asset_lst = new List<salesforce_ontap_account_asset_c__x>();
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003117',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003118',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003119',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				asset_lst.add(new salesforce_ontap_account_asset_c__x(ontap_asset_code_c__c = '0003003120',isdeleted__c=true, issm_sap_number_c__c='1029120920192'));
				return asset_lst;
			} else {
				return [SELECT Id, ExternalId, createddate__c, id__c, isdeleted__c, 
				issm_salesoffid_c__c, issm_sap_number_c__c, name__c, ontap_account_c__c, 
				ontap_account_r_ontap_sapcustomerid_c__c, ontap_asset_code_c__c, 
				ontap_asset_description_c__c, ontap_asset_ownership_c__c, 
				ontap_asset_status_c__c, ontap_brand_c__c, ontap_cam_vendor_c__c, 
				ontap_capacity_c__c, ontap_componentid_c__c, ontap_ibase_c__c, 
				ontap_inventory_date_c__c, ontap_last_modified_date_sap_c__c, 
				ontap_latitude_c__c, ontap_longitude_c__c, ontap_manufacturer_c__c, 
				ontap_manufacturer_serial_number_c__c, ontap_quantity_c__c, 
				ontap_serial_number_c__c, ontap_warranty_date_c__c, issm_sapcustomerid_c__c 
				FROM salesforce_ontap_account_asset_c__x 
				where issm_sapcustomerid_c__c in :setSapCustomerId and isdeleted__c =: isDeleted];
			}
		}
	}

	/**
	 * Get a set of external id for the object		                             
	 * @param  List<sObject> sObjectList list of objects to return the set
	 */
	public static Set<String> getSetExternalId(List<sObject> sObjectList){
		Set<String> assetsSet = new Set<String>();
		salesforce_ontap_account_asset_c__x accountAsset; 
		for(Integer i = 0; i < sObjectList.size(); i++){
				accountAsset = (salesforce_ontap_account_asset_c__x)sObjectList.get(i);
		        assetsSet.add(accountAsset.ontap_asset_code_c__c);
		}
		return assetsSet;
	}
}