/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control RouteXAccount__c events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       14-06-2017        Andrés Garrido               Creation Class
    1.1       01-09-2017        Daniel Peñaloza              Add route manager to Account Team
****************************************************************************************************/

trigger AccountByRoute_tgr on AccountByRoute__c (after delete, after insert, after update, before delete, before insert, before update) {
    if(trigger.isBefore) {
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','BeforeInsert')){
            // Validar que solo se aplique para Cuentas con Ruta de Preventa y Autoventa
            AccountByRoute__c[] lstAccountsToProcess = AccountByRoute_cls.filterAccounts(Trigger.new);
            AccountByRouteOperations_cls.validateServiceModelConfig(trigger.new);

            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','BeforeInsert');
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','BeforeUpdate')){
            // Validar que solo se aplique para Cuentas con Ruta de Preventa y Autoventa
            AccountByRoute__c[] lstAccountsToProcess = AccountByRoute_cls.filterAccounts(Trigger.new);
            AccountByRouteOperations_cls.validateServiceModelConfigUpdate(trigger.old, trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','BeforeUpdate');
        }
        else if(trigger.isDelete){
            // Remove Supervisor and Sales Agent from Account team
            //AccountByRoute_cls.removeUsersFromAccountTeam(Trigger.old); // original
			AccountByRoute_cls.removeUsersFromAccountTeam(Trigger.old,null); // fix resendiz y wp
        }
    }
    else if(trigger.isAfter){
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterInsert')){
            AccountByRouteOperations_cls.afterInsertAccountByRoute(trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterInsert');
            // Add Supervisor and Sales Agent to Account team
            AccountByRoute_cls.addUsersToAccountTeam(Trigger.new);
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterUpdate')){
            AccountByRouteOperations_cls.afterUpdateAccountByRoute(trigger.old, trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterUpdate');
        }
        else if(trigger.isDelete && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByRoute_tgr','AfterDelete')){
            AccountByRouteOperations_cls.afterDeleteAccountByRoute(trigger.old);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByRoute_tgr','AfterDelete');
        }
    }
}