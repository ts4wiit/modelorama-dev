/**
 * Trigger for the AllMobileRouteAppVersion__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
trigger AllMobileRouteAppVersionTrigger on AllMobileRouteAppVersion__c (after insert, after update) {
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			AllMobileRouteAppVersionHelperClass.syncRouteAppVersionWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_INSERT);
		}
		if(Trigger.isUpdate) {
			AllMobileRouteAppVersionHelperClass.syncRouteAppVersionWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_ROUTE_APP_VERSION_AFTER_UPDATE);
		}
	}
}