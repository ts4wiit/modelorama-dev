trigger MDRM_BusinessmanActive on MDRM_Businessman_Expansor__c (after insert, after delete, after update, after undelete) {
    set <Id> accIds = new set <Id> ();
    
    if(Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate){
        For (MDRM_Businessman_Expansor__c relacion : Trigger.new){
            accIds.add(relacion.MDRM_Businessman__c);
        }
    }
    
    if(Trigger.isDelete || Trigger.isUpdate){
        For (MDRM_Businessman_Expansor__c relacion : Trigger.old){
            accIds.add(relacion.MDRM_Businessman__c);
        }
    }
   

}