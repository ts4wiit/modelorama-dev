/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  Trigger para el objeto Case el cual antes de ejecutarse realiza ciertas operaciones que contienes la clase ISSM_UpdateCaseInCaseForceHandler_cls
Comentarios ":CAPS" --> Carlos Pintor
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017  Hector Diaz (HD)      Creador.
2.0 25-Julio-2017  Joseph Ceron (JC)     Creacion de casos force after insert
3.0 22-Agosto-2017 Hector Diaz (HD)      Creacion de casos Trigger.IsBefore para asignar proveedores
4.0 07-Junio-2018  Leopoldo Ortega (LO)  Process CAM & CS - Assign task to case - ISSM_AssignTaskToCase.assignTaskToCase
***********************************************************************************/

/**
    * Descripcion :  
    * @param  Trigger.oldMap, Trigger.newMap
    * @return none  
**/
trigger ISSM_CaseTrigger_tgr on Case (before insert,before update,after insert,after update) {
    ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
    //ISSM_TriggerFactory_cls.createHandler(Case.sObjectType);
    if(Trigger.IsAfter){
        String CaseID =  '';
        List<CaseMilestone> lstUpdateCaseMilestone = new List<CaseMilestone>();
        List<CaseMilestone> lstCaseMilestones = new List<CaseMilestone>();
        CaseMilestone objCaseUpdateMilestone = new CaseMilestone();
        Boolean flagUpdateMilestone = false;
        Boolean flagUpdateCaseMilestone = false;
        List<String> lstIds = new List<String>();
        Set<String> SetIds = new Set<String>();
        Map <String, CaseMilestone> cm = new Map <String, CaseMilestone>();//Daniel Daza 20172019
        if(!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_CaseTrigger)) {
            if(Trigger.isInsert){
                ISSM_UpdateCaseInCaseForceHandler_cls objUpdateCaseInCaseForceHandler = new ISSM_UpdateCaseInCaseForceHandler_cls();
                objUpdateCaseInCaseForceHandler.InsertCaseInCaseForce(Trigger.new);
                
                // Assign task to case
                ISSM_AssignTaskToCase.assignTaskToCase(Trigger.new);
                
                // Update call
                ISSM_CollectionManagementCaseHandler_cls.UpdateCall(Trigger.new);
            }
            if(Trigger.isInsert  || Trigger.isUpdate){   
                for(Case objCase : Trigger.New){                    
                    CaseID = objCase.ID;
                    if(objCase.Status == System.label.ISSM_CaseStatusClosed) { flagUpdateCaseMilestone=true; } else {  
                        SetIds.add(CaseID);
                        lstIds.addAll(SetIds);
                    }
                }
                
                // Assign address to caseForce
                ISSM_UpdateCaseInCaseForceHandler_cls objUpdateCaseInCaseForceHandler = new ISSM_UpdateCaseInCaseForceHandler_cls();
                objUpdateCaseInCaseForceHandler.AssignAddressCaseInCaseForce(Trigger.new);
                 
                if (!lstIds.isEmpty()) { try { ISSM_CaseTriggerMilestone_cls.UpdateTargetDateMilestone(lstIds); } catch(Exception ex) { System.debug('##### Exception '+ ex); } }
                if(flagUpdateCaseMilestone) { lstCaseMilestones = objCSQuerys.QueryCaseMilestone(CaseID); for(CaseMilestone objCaseMilestone : lstCaseMilestones) { flagUpdateMilestone = true; objCaseMilestone.CompletionDate = Datetime.now(); } }
                    
                if(flagUpdateMilestone) { ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger); for (Case reg : Trigger.new) { if ((reg.Status != 'Closed') && (reg.Status != 'Cerrado')) { update lstCaseMilestones; } } }
            
            }
            
        }
        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger);
    }
    if (Trigger.IsBefore) {
        if (!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_CaseTrigger)) {
            if (Trigger.isInsert) {
                Set<String> typNumber_set = new Set<String>();
                List<ISSM_TypificationMatrix__c> lstTypification = new List<ISSM_TypificationMatrix__c>();
                
                for (Case objCaseTrigger : Trigger.New) {
                    // Cases created from B2B
                    if (objCaseTrigger.ISSM_Channel__c == 'B2B') {
                        if ((objCaseTrigger.ISSM_TypificationLevel1__c != null) && (objCaseTrigger.ISSM_TypificationLevel2__c != null)) {
                            ISSM_CollectionManagementCaseHandler_cls.createCaseByChannel(Trigger.new);
                        }
                    } //if (Test.isRunningTest()) { ISSM_CollectionManagementCaseHandler_cls.createCaseByChannel(Trigger.new); }
                    
                    if (objCaseTrigger.ISSM_TypificationNumber__c != null || objCaseTrigger.ISSM_TypificationNumber__c != '') {
                        typNumber_set.add(objCaseTrigger.ISSM_TypificationNumber__c);
                    }
                }
                
                if (!typNumber_set.isEmpty()) {
                    lstTypification = objCSQuerys.QueryTypificationMatrixBySet(typNumber_set);
                }
                
                if (!lstTypification.isEmpty()) {
                    for (ISSM_TypificationMatrix__c objlstTypification : lstTypification) {
                        if (objlstTypification.ISSM_SLAProvider__c ==  true) { ISSM_AssignProvider_cls.AssignProvider(Trigger.New,objlstTypification.ISSM_TypificationLevel3__c,objlstTypification.ISSM_TypificationLevel4__c); }
                    }
                }
            }
            if (Trigger.isUpdate) {
                ISSM_UpdateCaseInCaseForceHandler_cls objUpdateCaseInCaseForceHandler = new ISSM_UpdateCaseInCaseForceHandler_cls(); objUpdateCaseInCaseForceHandler.UpdateCaseInCaseForce(Trigger.oldMap, Trigger.newMap);
            }
        }
    }
}