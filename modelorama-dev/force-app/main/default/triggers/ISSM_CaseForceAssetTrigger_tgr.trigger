trigger ISSM_CaseForceAssetTrigger_tgr on ISSM_Case_Force_Asset__c (before insert, after insert) {
    if(trigger.IsBefore && trigger.IsInsert) {
        ISSM_CAM_cls.CaseForceAsset_CoolerValidation(Trigger.New);
    }
    if(trigger.IsAfter && trigger.IsInsert) {
        ISSM_CAM_cls.CaseForceAsset_CoolerChangeStatus(Trigger.New, Trigger.NewMap);
    }

}