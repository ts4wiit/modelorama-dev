/**************************************************************************************
Nombre del Trigger: ISSM_SurveyInvitation_tgr
Versión : 1.0
Fecha de Creación : 06 Septiembre 2018
Funcionalidad : Trigger para procesar la invitación de encuesta para obtener la liga de la encuesta y ésta sea pública
Clase de Prueba: ISSM_SurveyInvitation_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega     06 - Septiembre - 2018      Versión Original
*************************************************************************************/
trigger ISSM_SurveyInvitation_tgr on SurveyInvitation (after insert) {
    List<SurveyInvitation> surveyInv_lst = new List<SurveyInvitation>();
    for (SurveyInvitation reg : Trigger.new) {
        surveyInv_lst.add(reg);
    }
    ISSM_SurveyInvitation_thr.setBoolean(surveyInv_lst);
}