/****************************************************************************************************
    General Information
    -------------------
    author:     Andrés Morales
    email:      lmorales@avanxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
    Trigger under Attachment object

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                    	Description          
    ------    --------        --------------------------   	-----------------------------------------
    1.0       26/10/2017      Andrés Morales				Created
    ================================================================================================
****************************************************************************************************/

trigger MDRM_AttachmentsOnAccount_trg on attachment (after insert, 
	after update, 
	after delete, 
	after undelete) {

    AttachmentTriggerHandler handle = new AttachmentTriggerHandler(trigger.new, trigger.oldMap);
    
    if (trigger.isAfter)
    {
        if (trigger.isInsert) handle.afterInsert();
        //if (trigger.isUpdate) handle.afterUpdate();
        if (trigger.isDelete) handle.afterDelete();
        if (trigger.isUndelete) handle.afterUndelete();
    }
	
}