/****************************************************************************************************
    General Information
    -------------------
    author:     Fernando Engel 
    email:      fernando.engel.funes@accenture.com
    company:    Accenture
    Customer:   Grupo Modelo

    Description:
    Stop modifications over record on approval process

    Information about changes (versions)
    ================================================================================================
    Number    Dates           Author                        Description          
    ------    --------        --------------------------    -----------------------------------------
    1.0       29/06/2017      Fernando Engel                Trigger created
    ================================================================================================
****************************************************************************************************/
trigger MDRM_Document on MDRM_Document__c (before delete) {
    set<Id> listdocId = new set<Id>();
    Map<Id,MDRM_Document__c> listDocument;
    
    List<MDRM_Document__c> lstTrigger = Trigger.isDelete ? Trigger.old : Trigger.new;
    
    for(MDRM_Document__c doc: lstTrigger){
        If( doc.MDRM_Send_for_Approval__c == Label.MDRM_Send_for_Approval_YES && (doc.MDRM_Status__c == 'Enviado a Aprobar' || doc.MDRM_Status__c == 'Aprobado') ){
            doc.Name.addError(Label.MDRM_DocumentDMLWarning);
        }
    }
}