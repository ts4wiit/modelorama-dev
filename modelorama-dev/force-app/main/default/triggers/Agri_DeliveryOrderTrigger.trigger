trigger Agri_DeliveryOrderTrigger on Delivery_order__c (after insert, after update, before delete, before insert) {

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            Agri_DeliveryOrderTriggerHandler.asignaFolio(Trigger.new);
            Agri_DeliveryOrderTriggerHandler.gestionaInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            Agri_DeliveryOrderTriggerHandler.gestionaUpdate(Trigger.new);
        }
    }        
    
    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            Agri_DeliveryOrderTriggerHandler.gestionaDelete(Trigger.old);
        }
    }
        
    
}