/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control Event events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       27-07-2017        Andrés Garrido               Creation Class
	2.0       07-06-2018        Rodrigo Resendiz			 Validations for modeloramas
****************************************************************************************************/
trigger Event_tgr on Event (after delete, after insert, after update, before delete, before insert, before update) {
    if(trigger.isBefore) {
        if(trigger.isInsert){
            EventOperations_cls.fillSequence(trigger.new);
            EventOperations_cls.manageOrderDetail(trigger.new, null);
            System.debug(':::: UserInfo.getProfileId() '+UserInfo.getProfileId());
            if(MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('AccountCompleteData').contains(UserInfo.getProfileId())
               || MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('RepeatedAccountVisitInTime').contains(UserInfo.getProfileId())){ 
                List<Event> filteredList = MDRM_EventValidation_cls.filterEventsByWhatId(trigger.new, '001');
                if(MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('AccountCompleteData').contains(UserInfo.getProfileId())){
                    if(!filteredList.isEmpty()) MDRM_EventValidation_cls.validateAccountCompleteData(filteredList); 
                }
                if(MDRM_EventValidation_cls.APPLY_TO_PROFILE_NAME_ID.get('RepeatedAccountVisitInTime').contains(UserInfo.getProfileId())){
                    if(!filteredList.isEmpty()) MDRM_EventValidation_cls.validateRepeatedAccountVisitInTime(filteredList); 
                }
            }
        }
        else if(trigger.isUpdate){
            EventOperations_cls.manageOrderDetail(trigger.new, trigger.old);
        }
        else if(trigger.isDelete){
            EventOperations_cls.validateTourStatusForDelete(Trigger.old);
        }
    }
    else if(trigger.isAfter){
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('Event_tgr','AfterInsertSync')){
            EventOperations_cls.syncInsertEventToExternalObject(trigger.newMap);
        }
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('Event_tgr','AfterInsertSort')){
            EventOperations_cls.updateSortListBySequence(trigger.new, trigger.old, false, true);
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSort');
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('Event_tgr','AfterUpdateSort')){
            EventOperations_cls.updateSortListBySequence(trigger.new, trigger.old, false, false);
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
        }
        else if(trigger.isDelete){
            if(!TriggerExecutionControl_cls.hasAlreadyDone('Event_tgr','AfterDeleteSync'))
                EventOperations_cls.syncDeleteEventToExternalObject(trigger.oldMap);
            
            if(!TriggerExecutionControl_cls.hasAlreadyDone('Event_tgr','AfterDeleteEventsSync'))
                EventOperations_cls.updateSortListBySequence(trigger.new, trigger.old, true, false);
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
        }
    }
}