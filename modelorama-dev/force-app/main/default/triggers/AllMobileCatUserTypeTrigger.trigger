/**
 * Trigger for the AllMobileCatUserType__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
trigger AllMobileCatUserTypeTrigger on AllMobileCatUserType__c (before insert, after insert, after update) {
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			AllMobileCatUserTypeHelperClass.validateCatUserTypeNameDuplication(Trigger.new);
		}
	}
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			AllMobileCatUserTypeHelperClass.syncCatUserTypeWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_INSERT);
		}
		if(Trigger.isUpdate) {
			AllMobileCatUserTypeHelperClass.syncCatUsertypeWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_CAT_USER_TYPE_AFTER_UPDATE);
		}
	}
}