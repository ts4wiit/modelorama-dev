/**************************************************************************************
Nombre del Trigger: ISSM_Manager_ATM_tgr
Versión : 1.0
Fecha de Creación : 28 Agosto 2018
Funcionalidad : Trigger para procesar el cambio de gestor en los usuarios relacionados
Clase de Prueba: ISSM_Manager_ATM_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        28 - Agosto - 2018      Versión Original
*************************************************************************************/
trigger ISSM_Manager_ATM_tgr on ISSM_Manager_ATM__c (before insert) {
    String RolOld_str = '';
    for (ISSM_Manager_ATM__c reg : Trigger.new) {
        if (Trigger.IsBefore) {
            if (Trigger.IsInsert) {
                ISSM_Manager_ATM_thr.validExistRole(Trigger.new);
            }
        }
    }
}