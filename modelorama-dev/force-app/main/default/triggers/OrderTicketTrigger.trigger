trigger OrderTicketTrigger on ONTAP__Order__c (before insert, after insert, before update) {
    List<Id> orderIdsToSendEmails = new List<Id>();
    List<Id> orderIdsToGenerateTicketOnStatusClose = new List<Id>();
    List<ONTAP__Order__c> ordersToUpdateEmailFlag = new List<ONTAP__Order__c>();

    Id orderWithPriceRtId = [SELECT Id FROM RecordType WHERE SObjectType = 'ONTAP__Order__c' AND DeveloperName = 'Order_with_Price'].Id;


    for (ONTAP__Order__c order: Trigger.new) {
        if (Trigger.isBefore) {
            if (order.ONTAP__Send_ticket_via_email__c) {
                orderIdsToSendEmails.add(order.Id);
                order.ONTAP__Send_ticket_via_email__c = false;
            }

            if (order.RecordTypeId == orderWithPriceRtId && order.ONTAP__OrderStatus__c == 'Closed'
                    && (Trigger.isInsert || Trigger.oldMap.get(order.Id).ONTAP__OrderStatus__c != 'Closed')) {
                orderIdsToGenerateTicketOnStatusClose.add(order.Id);
            }
        } else {
            if (Trigger.isInsert) {
                if (order.ONTAP__Send_ticket_via_email__c) {
                    orderIdsToSendEmails.add(order.Id);
                    order.ONTAP__Send_ticket_via_email__c = false;
                    ordersToUpdateEmailFlag.add(order);
                }
            }
        }
    }

    if (orderIdsToSendEmails.size() > 0) {
        GenerateTicketController.sendEmails(orderIdsToSendEmails);
    }
    if (orderIdsToGenerateTicketOnStatusClose.size() > 0) {
        GenerateTicketController.createTicketsForOrders(orderIdsToGenerateTicketOnStatusClose);
    }
    if (!ordersToUpdateEmailFlag.isEmpty()) {
        update ordersToUpdateEmailFlag;
    }
}