/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
Comentarios ":
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 23-Agosto-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
trigger ISSM_AssignOwnerIdAccountTeam_tgr on Account (before insert, before update, after update, after insert) {
    
    if (!ISSM_TriggerManager_cls.isInactive('ISSM_AssignOwnerIdAccountTeam_tgr')) {
        if (Trigger.IsBefore) {
            if (Trigger.isInsert || Trigger.isUpdate) {
                System.debug('#####ENTRO ISSM_AssignOwnerIdAccountTeam_tgr -- IsBefore ');
                ISSM_AssignProspect_cls.AssignProspect(Trigger.new);
                ISSM_AssignProspect_cls.AssignSaleOffice(Trigger.new);
                //ISSM_TriggerManager_cls.inactivate('ISSM_AssignOwnerIdAccountTeam_tgr');
            }
        }
        for (Account reg : Trigger.new) {
            if (Trigger.IsAfter) {
                if (Trigger.IsInsert) {
                    if (reg.ISSM_Channel__c == 'B2B') {
                        ISSM_AssignProspect_cls.AssignSaleOfficeByProvider(Trigger.new);
                    }
                }
            }
        }
    }
}