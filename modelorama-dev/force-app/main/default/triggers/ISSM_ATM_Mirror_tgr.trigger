/**************************************************************************************
Nombre del Trigger: ISSM_ATM_Mirror_tgr
Versión : 1.0
Fecha de Creación : 06 Abril 2018
Funcionalidad : Trigger para replicar la asignación de usuarios en equipos de cuentas estándar
Clase de Prueba: ISSM_ATM_Mirror_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        06 - Abril - 2018      Versión Original
*************************************************************************************/
trigger ISSM_ATM_Mirror_tgr on ISSM_ATM_Mirror__c (before insert, before update, after update) {
    /***** VARIABLES *****************************************************************/
    List<ISSM_ATM_Mirror__c> ATM_Mirror_lst = new List<ISSM_ATM_Mirror__c>();
    String RolOld_str = '';

    try {
        for (ISSM_ATM_Mirror__c reg : Trigger.new) {
            if (Trigger.IsBefore) {
                if (Trigger.IsInsert) {
                    // Llamamos al método de alta de usuario en el equipo de cuentas estándar y escribir registro sobre el objeto "ISSM_ATM_Queue__c"
                    ISSM_ATM_Mirror_thr.altaUsuarioATM(Trigger.new);
                } 
                if (Trigger.IsUpdate) {
                    ISSM_ATM_Mirror__c regOld = Trigger.oldMap.get(reg.Id);
                    if (reg.Usuario_relacionado__c != regOld.Usuario_relacionado__c) {
                        if (!Test.IsRunningTest()) { reg.AddError(System.label.ISSM_DeleteRecord_Add); }
                    }
                }
            } else if (Trigger.IsAfter) {
                if (Trigger.IsUpdate) {
                    ISSM_ATM_Mirror__c regOld = Trigger.oldMap.get(reg.Id);
                    if (reg.Estatus__c != regOld.Estatus__c) {
                        if (reg.Estatus__c == 'Inactivo') {
                            ATM_Mirror_lst.add(reg);
                            // Llamamos al método de baja de usuario en el equipo de cuentas estándar y escribir registro sobre el objeto "ISSM_ATM_Queue__c"
                            ISSM_ATM_Mirror_thr.usuarioInactivoATM(ATM_Mirror_lst);
                        }
                    } else if (reg.Rol__c != regOld.Rol__c) {
                            ATM_Mirror_lst.add(reg);
                            RolOld_str = regOld.Rol__c;
                            // Llamamos al método de cambio de rol del usuario en el equipo de cuentas estándar y escribir registro sobre el objeto "ISSM_ATM_Queue__c"
                            ISSM_ATM_Mirror_thr.cambioRolUsuarioATM(ATM_Mirror_lst, RolOld_str, reg.Usuario_relacionado__c, reg.Oficina_de_Ventas__c);
                    }
                }
            }
        }
    } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber()); }
}