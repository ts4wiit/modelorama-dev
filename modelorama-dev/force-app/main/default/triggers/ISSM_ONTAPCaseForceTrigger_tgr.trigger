/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/

/**
    * Descripcion :
    * @param  none
    * @return none
**/
trigger ISSM_ONTAPCaseForceTrigger_tgr on ONTAP__Case_Force__c (after insert, after update, before insert, before update) {
    try {
        if(Trigger.IsBefore){
            if(!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_ONTAPCaseForceTrigger)) {
                if(Trigger.isInsert) {  
                Id RecordTypeId_id =  Schema.SObjectType.ONTAP__Case_Force__c.getRecordTypeInfosByDeveloperName().get('CAM_Asset_UnAssignation').getRecordTypeId();
              		List<ONTAP__Case_Force__c> lstCaseAssign = [select id,ISSM_Asset_CAM__c from ONTAP__Case_Force__c where ISSM_Asset_CAM__c =: Trigger.new[0].ISSM_Asset_CAM__c and ONTAP__Status__c != 'Closed' and recordTypeId =:RecordTypeId_id]; 
                   	System.debug('***** lstCaseAssign : ');
                   	System.debug(lstCaseAssign);
                    
                    for(ONTAP__Case_Force__c  objIterateCase :  Trigger.new ){
                    	if(lstCaseAssign.size() > 0  && RecordTypeId_id == objIterateCase.RecordTypeId &&  objIterateCase.RecordTypeId !=null   && (objIterateCase.ISSM_Asset_CAM__c !=null || objIterateCase.ISSM_Asset_CAM__c!='') ){
                    		System.debug('*****************HMDH crear nuevo registro : ' + Trigger.new);
                         	Trigger.new[0].addError(System.label.CAM_ErrorCreateCaseForce); 
                    		
                    	}
                       // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No se pude generar caso'));    
                    }
                   
                }
            }
        }
        if(Trigger.isAfter){
            if(!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_ONTAPCaseForceTrigger)) {
                if(Trigger.isInsert){
                    ISSM_CreateCaseForceInCaseHandler_cls objCreateCaseForceInCaseHandler = new ISSM_CreateCaseForceInCaseHandler_cls();
                    objCreateCaseForceInCaseHandler.CreateCaseForceInCase(  Trigger.new );
                    //ISSM CAM: Update the Inventory of coolers, substracting the requested quantity in Case_Force from the SFDC Quantity in ISSM_Inventory
                    
                    ISSM_CAM_cls.updateInventory(Trigger.New,-1,null);
                }
                if(Trigger.IsUpdate) {
                    ISSM_CreateCaseForceInCaseHandler_cls objCreateCaseForceInCaseHandler = new ISSM_CreateCaseForceInCaseHandler_cls();
                    objCreateCaseForceInCaseHandler.UodateToJSon(  Trigger.new , Trigger.old);
                }
            }
        }
        
        if(trigger.isBefore && trigger.isInsert) {
            ISSM_ONTAPClassifyCaseForce_cls.onBeforeInsert(trigger.new);
            //Asignar matriz de tipificación por default (EXTERNAL ID: México - TM - 0070: "Entrega de equipo")
            ISSM_CAM_cls.setKPIBeerCurrMonth(ISSM_CAM_cls.retCAMRecords(Trigger.new));
            ISSM_CAM_cls.assignTypMatrix(ISSM_CAM_cls.retCAMRecords(Trigger.New));
        }
        if(trigger.isAfter && trigger.isUpdate) {
            ISSM_CAM_cls.updateInventoryOnReject( Trigger.New, Trigger.oldMap);
        }
    } catch (Exception ex) {
        System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber());
    }
}