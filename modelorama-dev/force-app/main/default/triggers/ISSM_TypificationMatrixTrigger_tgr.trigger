/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/

/**
    * Descripcion :  
    * @param  none
    * @return none  
**/
trigger ISSM_TypificationMatrixTrigger_tgr on ISSM_TypificationMatrix__c (before insert, before update) {
    System.debug('#####ISSM_TypificationMatrixTrigger_tgr');    
    
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate){  
            System.debug('#####ISSM_TypificationMatrixTrigger_tgr -- Trigger.isInsert || Trigger.isUpdate');    
            ISSM_CreatUpdTypifAssignQueueHandler_cls objCreatUpdTypifAssignQueueHandler = new ISSM_CreatUpdTypifAssignQueueHandler_cls();
            objCreatUpdTypifAssignQueueHandler.CreateTypificationAssignQueue( Trigger.new );
        }
    } 
}