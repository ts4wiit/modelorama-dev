/**
 * Trigger for the AllMobileDeviceUser__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
trigger AllMobileDeviceUserTrigger on AllMobileDeviceUser__c (after insert, after update) {
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			AllMobileDeviceUserHelperClass.syncDeviceUserWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_INSERT);
		}
		if(Trigger.isUpdate) {
			AllMobileDeviceUserHelperClass.syncDeviceUserWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_DEVICE_USER_AFTER_UPDATE);
		}
	}
}