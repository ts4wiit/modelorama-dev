/**************************************************************************************
Nombre del Trigger: ISSM_CAM_WSEstatus_trg
Versión : 1.0
Fecha de Creación : 24 Julio 2018
Funcionalidad : Trigger for call web service to change status in the object Asset Log
Clase de Prueba: ISSM_CAM_WSEstatus_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        24 - Julio - 2018      Versión Original
* Daniel Rosales 		 01 - Agosto - 2018		Se envía EQUNR/Equipment_number__c al Webservice en lugar de SERNR/ISSM_Serial_Number__c
* Daniel Rosales         08 - Agosto - 2018     Se envía un mapa con los Equipos a cambiar Estatus, solo sí el estatus se debe enviar a SAP
* Daniel Rosales         15 - Agosto - 2018     Se settea el estatus SFDC de acuerdo al estatus SAP
ALMA	Free Use	- Se pondrá solo si se tiene el número de activo fijo
ALMA	Available	- Se pondrá si el enfriador no tiene número de activo fijo
ASAE	To Deliver	- Se pondrá cuando el enfriador esta por ser entregado desde SAP

*************************************************************************************/
trigger ISSM_CAM_WSEstatus_trg on ISSM_Asset__c (before update, before insert) {
    try {
        Map<String, String> mEstatus_SFDC_SAP = new Map<String, String>();
        for(ISSM_CAM_Estatus_SFDC_SAP__c e: [SELECT ISSM_Status_SAP__c, ISSM_Status_SFDC__c
                                             FROM ISSM_CAM_Estatus_SFDC_SAP__c
                                             WHERE ISSM_Send_to_SAP__c = true]) 
            mEstatus_SFDC_SAP.put(e.ISSM_Status_SFDC__c, e.ISSM_Status_SAP__c) ;
        
        map<String, String> mapCambiaEstatusSFDC = new map<String,String>();
        List<ISSM_Asset__c> lstCambiaEstatusSAP = new List<ISSM_Asset__c>();
        
        for (ISSM_Asset__c reg : Trigger.new) 
            if (Trigger.IsBefore) {
                if (Trigger.IsUpdate) {
                    ISSM_Asset__c regOld = Trigger.oldMap.get(reg.Id);
                    if (reg.ISSM_Status_SFDC__c != regOld.ISSM_Status_SFDC__c){ // CAMBIO ESTATUS SFDC
                        if (!ISSM_CAM_WSEstatus_cls.IsIntegration) { // NO ES INTEGRACION
                            if (mEstatus_SFDC_SAP.containsKey(reg.ISSM_Status_SFDC__c) ) { // Agregar al mapa solo si es un estatus para enviar   
                                mapCambiaEstatusSFDC.put(reg.Equipment_number__c, reg.ISSM_Status_SFDC__c); 
                            } //else{system.debug('ES INTEGRACIÓN --- sysadmin para prueba');}
                        }
                    }	
                    if (ISSM_CAM_WSEstatus_cls.IsIntegration || Test.isRunningTest() ){ // ES INTEGRACION
                        lstCambiaEstatusSAP.add(reg); //agregar a una lista lstCambiaEstatusSAP
                    }
                }
                if(Trigger.IsInsert){ // ES BEFORE INSERT
                    if (ISSM_CAM_WSEstatus_cls.IsIntegration || Test.isRunningTest() ){ // ES INTEGRACION
                        lstCambiaEstatusSAP.add(reg); //agregar a una lista lstCambiaEstatusSAP
                    }
                }
            }
        if(mapCambiaEstatusSFDC.size()>0) // Cambio de estatus SFDC, enviar a SAP - Solo se ejcuta si MAPA > 0
            ISSM_CAM_WSEstatusFuture_cls.callClassWSEstatus(mapCambiaEstatusSFDC);
        
        if(lstCambiaEstatusSAP.size()>0)
            ISSM_CAM_WSEstatus_cls.setEstatusSFDC(lstCambiaEstatusSAP); // logica para settear estatus SFDC de acuerdo a estatus SAP
    } catch (Exception ex) { System.debug('ERROR MESSAGE: ' + ex.getMessage() + ' ### ' + 'LINE NUMBER: ' + ex.getLineNumber() + 'STACK: ' + ex.getStackTraceString()); }
}