/**
 * Trigger for the AllMobileApplication__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
trigger AllMobileApplicationTrigger on AllMobileApplication__c (after insert, after update) {
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			AllMobileApplicationHelperClass.syncApplicationWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_APPLICATION);
		}
		if(Trigger.isUpdate) {
			AllMobileApplicationHelperClass.syncApplicationWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_APPLICATION);
		}
	}
}