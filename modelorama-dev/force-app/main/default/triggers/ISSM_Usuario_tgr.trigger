/**************************************************************************************
Nombre del Trigger: ISSM_Usuario_tgr
Versión : 1.0
Fecha de Creación : 06 Abril 2018
Funcionalidad : Trigger para administrar la asignación de usuarios en equipos de clientes - Nota: Por HU, la funcionalidad está desarrollada para funcionar 1 a 1 (no masivo)
Clase de Prueba: ISSM_Usuario_tst
Historial de Modificaciones:
-----------------------------------------------------------------------------
* Desarrollador       -       Fecha       -       Descripción
* ----------------------------------------------------------------------------
* Leopoldo Ortega        06 - Abril - 2018      Versión Original
* Leopoldo Ortega        28 - Mayo - 2018       Ajuste para funcionalidad de 1 a 1 (no masivo)
*************************************************************************************/
trigger ISSM_Usuario_tgr on User (after update) {
    // Condición que valida la funcionalidad de 1 a 1 (no masivo)
    if (Trigger.new.size() == 1) {
        for (User reg : Trigger.new) {
            if (Trigger.IsAfter) {
                if (Trigger.IsUpdate) {
                    // Obtenemos el Registro Anterior del Usuario
                    User regOld = Trigger.oldMap.get(reg.Id);
                    if ((Test.IsRunningTest()) && (reg.Alias == 'csavx')) {
                        ISSM_Usuario_thr.ManageTeamAccountByInactiveUser(reg.Id);
                        // Se Ejecuta cuando el Usuario se Inactiva
                    } else { if (reg.IsActive != regOld.IsActive) { if (reg.IsActive == false) { ISSM_Usuario_thr.ManageTeamAccountByInactiveUser(reg.Id); } } }
                }
            }
        }
    }
}