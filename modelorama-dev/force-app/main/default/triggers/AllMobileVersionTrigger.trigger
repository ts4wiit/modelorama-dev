/**
 * Trigger for AllMobileVersion__c object.
 * <p /><p />
 * @author Alberto Gomez
 */
trigger AllMobileVersionTrigger on AllMobileVersion__c (before insert, before update, after insert, after update) {
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			AllMobileVersionHelperClass.initiateInsertVersion(Trigger.new);
		}
		if(Trigger.isUpdate) {
			AllMobileVersionHelperClass.initiateInsertVersion(Trigger.new);
		}
	}
	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			AllMobileVersionHelperClass.syncVersionWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_INSERT_VERSION);
		}
		if(Trigger.isUpdate) {
			AllMobileVersionHelperClass.syncVersionWithHeroku(Trigger.new, AllMobileStaticVariablesClass.STRING_TRIGGER_EVENT_AFTER_UPDATE_VERSION);
		}
	}
}