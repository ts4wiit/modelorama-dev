/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción: Trigger que se ejecuta cuando se crea un comentario en el objeto Standard CaseComment
---------------------------------------------------------------------------------
No. 	Fecha      Autor              	Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
***********************************************************************************/

/** 
    * Descripcion :  Antes de realizar el insert del comentario se crea un alogica en la clase  ISSM_CreateCaseCoInCaseForceCHandler_cls
    * @param  none
	* @return none  
**/
trigger ISSM_CaseCommentTrigger_tgr on CaseComment (before insert) {
	if(Trigger.isBefore){
		System.debug('##### Start - ISSM_CaseCommentTrigger_tgr');
		if(!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_CaseCommentTrigger)){
			if(Trigger.isInsert){
				ISSM_TriggerManager_cls.inactivate(System.label.ISSM_ONTAPCaseForceCommentTrigger); 
				ISSM_CreateCaseCoInCaseForceCHandler_cls objCreateCaseCoInCaseForceCHandler = new ISSM_CreateCaseCoInCaseForceCHandler_cls();
				objCreateCaseCoInCaseForceCHandler.CreateCaseCommentInCaseForceComment(Trigger.new );
			}
		}
	}
}