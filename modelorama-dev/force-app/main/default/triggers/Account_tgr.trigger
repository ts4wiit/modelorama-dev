/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Trigger handler for Events related to Account records
LastModified by: Alfonso de la Cuadra
Description: Added lines 25-27, 31-36 to generate FieldTracking

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    22/08/2017 Daniel Peñaloza            Trigger created
*******************************************************************************/

trigger Account_tgr on Account (after insert, after update) {
	private Integer i = 0;

    if (Trigger.isAfter) {
    	MDRM_HistoryFieldTracking histField = new MDRM_HistoryFieldTracking('Account');
        List<MDRM_Modelorama_Status_Change__c> lstsChange = new List<MDRM_Modelorama_Status_Change__c>();
        Id expansorRt = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Expansor').getRecordTypeId();

        if (Trigger.isUpdate) {
            // Filter accounts to remove from Visit Plan and Tour
            AccountOperations_cls.validateAccountsToRemove(Trigger.new);

            for(Account accI : Trigger.new) {
                //System.debug('RT Developer: ' + accI.RecordTypeId);
                if((expansorRt == accI.RecordTypeId)
                  		&& (accI.MDRM_Stage__c != Trigger.oldMap.get(accI.Id).MDRM_Stage__c
                  		|| accI.MDRM_SubStage__c != Trigger.oldMap.get(accI.Id).MDRM_SubStage__c)) {
                    lstsChange.addAll(histField.generateFieldTracking(accI, Trigger.oldMap.get(accI.Id)));
                }
                i++;
        	}
        }
        
        if(Trigger.isInsert) {
        	for(Account accI : Trigger.new) {
                //System.debug('RT Developer: ' + accI.RecordTypeId);
                if(expansorRt == accI.RecordTypeId) {
                    lstsChange.addAll(histField.generateFieldTracking(accI, null));
                }
                i++;
        	}
        }

        if(!lstsChange.isEmpty())
            insert lstsChange;
    }
}