/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:  
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0 22-Junio-2017 Hector Diaz (HD)      Creador.
1.1 01-Agosto-2017 Hector Diaz (HD)      Modificado el Before por AFTER.
***********************************************************************************/

/**
    * Descripcion :  
    * @param  none
    * @return none  
**/
trigger ISSM_ONTAPCaseForceCommentTrigger_tgr on ONTAP__Case_Force_Comment__c (After insert) {
    if(Trigger.isAfter){
        System.debug('##### Start - ISSM_ONTAPCaseForceCommentTrigger_tgr');
        if(!ISSM_TriggerManager_cls.isInactive(System.label.ISSM_ONTAPCaseForceCommentTrigger)){  
            if(Trigger.isInsert){
                ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseCommentTrigger); 
                ISSM_CreateCaseForceCInCaseCoHandler_cls objCreateCaseForceCInCaseCoHandler = new ISSM_CreateCaseForceCInCaseCoHandler_cls();
                objCreateCaseForceCInCaseCoHandler.CreateCaseForceCommentInCaseComment( Trigger.new );  
            }       
        }
    }  
}