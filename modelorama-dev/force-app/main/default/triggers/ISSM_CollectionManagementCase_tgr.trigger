/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:   aumento del campo ISSM_Accountant__c en CASO esto cada ves que se crea una tarea con tipo de registro RELLAMADA
Comentarios ":
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0   24-Agosto-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
trigger ISSM_CollectionManagementCase_tgr on ONCALL__Call__c (after update) {
    
    for (ONCALL__Call__c objCall : Trigger.new) {
        if (!ISSM_TriggerManager_cls.isInactive('ISSM_CollectionManagementCase_tgr')) {
            if (Trigger.IsAfter) {
                if (Trigger.IsUpdate) {
                    System.debug('##### ENTRO  ISSM_CollectionManagementCase_tgr -- Trigger.isAfter -- Trigger.IsUpdate');
                    //String RecordTypeCallId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get(System.Label.ISSM_Telecollection_RecordType).getRecordTypeId();
                    String RecordTypeCallId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Telecollection_RecordType);
                    List<ONCALL__Call__c> callUpd_lst = new List<ONCALL__Call__c>();
                    
                    if (objCall.RecordTypeId == RecordTypeCallId && objCall.ISSM_Active__c != false) {
                        
                        if (objCall.ISSM_CallCreated__c == false) {
                            
                            ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
                            ISSM_AppSetting_cs__c objIdQueueWithoutOwner = objCSQuerys.QueryAppSettingsObj();
                            
                            /*-------------------------------------------Update Call Back Queue --------------------------------------------------------------*/
                            if (Test.isRunningTest()) { ISSM_CollectionManagementCaseHandler_cls.UpdateCallBackQueue(objCall.id); }
                            if ((objCall.ISSM_CallEffectiveness__c == System.label.ISSM_CallEffectiveness_Effective) && (objCall.ISSM_CallEffectiveness__c == 'Effective')) {
                                if ((objCall.ONCALL__Call_Status__c != System.label.ISSM_CallStatus) && (objCall.ONCALL__Call_Status__c != null)) { ISSM_CollectionManagementCaseHandler_cls.UpdateCallBackQueue(objCall.id); }
                            }
                            
                            /*-------------------------------------------Creacion de Casos --------------------------------------------------------------*/                    
                            /* Validacion 1 CREA CASO */
                            if (Test.isRunningTest()) { ISSM_CollectionManagementCaseHandler_cls.UpdateAccountLastContactDate(Trigger.new); }
                            if ((objCall.ONCALL__Call_Status__c == System.Label.ISSM_OncallCallStatus) || (objCall.ONCALL__Call_Status__c == 'Complete')) {
                                if ((objCall.ISSM_CallEffectiveness__c == System.Label.ISSM_CallEffectiveness_Effective) || (objCall.ISSM_CallEffectiveness__c == 'Effective')) {
                                    if ((objCall.ISSM_ClarificationRequest__c == System.Label.ISSM_ClarificationRequest_Yes) || (objCall.ISSM_ClarificationRequest__c == 'Yes')) {
                                        if ((objCall.ISSM_ClarificationType__c == System.Label.ISSM_ClarificationType_PaymentNotR) || (objCall.ISSM_ClarificationType__c == 'Payment not reflected')) { ISSM_CollectionManagementCaseHandler_cls.UpdateAccountLastContactDate(Trigger.new); }
                                    }
                                }
                            }
                            
                            /* Validacion 2 CREA CASO */
                            if (Test.isRunningTest()) { ISSM_CollectionManagementCaseHandler_cls.UpdateAccountLastContactDate(Trigger.new); }
                            if ((objCall.ONCALL__Call_Status__c == System.Label.ISSM_OncallCallStatus) || (objCall.ONCALL__Call_Status__c == 'Complete')) {
                                if ((objCall.ISSM_CallEffectiveness__c == System.Label.ISSM_CallEffectiveness_Effective) || (objCall.ISSM_CallEffectiveness__c == 'Effective')) {
                                    if ((objCall.ISSM_ClarificationRequest__c == System.Label.ISSM_ClarificationRequest_Yes) || (objCall.ISSM_ClarificationRequest__c == 'Yes')) {  
                                        if ((objCall.ISSM_ClarificationType__c == System.Label.ISSM_ClarificationType_UnrecognizedC) || (objCall.ISSM_ClarificationType__c == 'Unrecognized charge')) { ISSM_CollectionManagementCaseHandler_cls.UpdateAccountLastContactDate(Trigger.new); }
                                    }
                                }
                            }
                            
                            /* Validacion 3 CREA CASO */
                            if ((objCall.ONCALL__Call_Status__c == System.Label.ISSM_OncallCallStatus) || (objCall.ONCALL__Call_Status__c == 'Complete')) {
                                if ((objCall.ISSM_CallEffectiveness__c == System.Label.ISSM_CallEffectiveness_Effective) || (objCall.ISSM_CallEffectiveness__c == 'Effective')) {
                                    if ((objCall.ISSM_PaymentPromise__c == System.Label.ISSM_PaymentPromise_No) || (objCall.ISSM_PaymentPromise__c == null)) {
                                        if ((objCall.ISSM_ClarificationRequest__c == System.Label.ISSM_ClarificationRequest_No) || (objCall.ISSM_ClarificationRequest__c == null)) {
                                            if ((objCall.ISSM_PaymentPlanRequest__c == System.Label.ISSM_PaymentPlanRequest_Yes) || (objCall.ISSM_PaymentPlanRequest__c == 'Yes')) {
                                                ISSM_CollectionManagementCaseHandler_cls.UpdateAccountPaymentPlanDate(Trigger.new);
                                            }
                                        }
                                    }
                                }
                            }
                            
                            /*-------------------------------------------Actualizacion de Cuenta Trigger--------------------------------------------------------------*/
                            /* Validacion 1 ACTUALIZA CUENTA */
                            if (objCall.RecordTypeId == RecordTypeCallId) {
                                if (objCall.ISSM_Active__c == true) {
                                    if ((objCall.ONCALL__Call_Status__c == System.Label.ISSM_OncallCallStatus) || (objCall.ONCALL__Call_Status__c == 'Complete')) {
                                        if ((objCall.ISSM_CallEffectiveness__c == System.Label.ISSM_CallEffectiveness_Effective) || (objCall.ISSM_CallEffectiveness__c == 'Effective')) {
                                            if ((objCall.ISSM_PaymentPromise__c == System.Label.ISSM_PaymentPromise_Yes) || (objCall.ISSM_PaymentPromise__c == 'Yes')) {
                                                if ((objCall.ISSM_PaymentPlanRequest__c == System.Label.ISSM_PaymentPlanRequest_No) || (objCall.ISSM_PaymentPlanRequest__c == null)) {
                                                    if ((objCall.ISSM_ClarificationRequest__c == System.Label.ISSM_ClarificationRequest_No) || (objCall.ISSM_ClarificationRequest__c == null)) {
                                                        
                                                        ISSM_CollectionManagementCaseHandler_cls.UpdateAccountPaymentPromise(Trigger.new);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }   
                    }
                }
            }
        }       
    }
}