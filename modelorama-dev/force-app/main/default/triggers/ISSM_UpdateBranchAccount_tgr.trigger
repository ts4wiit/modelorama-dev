/****************************************************************************************************
    General Information
    -------------------
    author:     Hecto Diaz
    email:      hdiaz@avaxo.com
    company:    Avanxo
    Customer:   Grupo Modelo

    Description:
        Update ISSM_Branch__c

    Information about changes (versions) 
    ================================================================================================
    Number    Dates           Author                       Description          
    ------    --------        --------------------------   -----------------------------------------
    1.0       11/Dic/2017      Hector Diaz  HD              Class created decorate class implemented
    ================================================================================================
****************************************************************************************************/
trigger ISSM_UpdateBranchAccount_tgr on Account (before insert, before update) {
    
    if(Trigger.IsBefore){
         if(!ISSM_TriggerManager_cls.isInactive('ISSM_UpdateBranchAccount_tgr')){   
            if(Trigger.isInsert || Trigger.isUpdate){
                String RecordTypeAccountId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN('Account');      
                for(Account  objAccount : Trigger.New){
                    if(objAccount.RecordtypeId == RecordTypeAccountId && objAccount.ISSM_CalculatedInvoicingType__c !=''){
                        if(objAccount.ISSM_CalculatedInvoicingType__c == 'Tradicional'){
                            System.debug('Cuenta a modificar Tradicional ');
                            objAccount.ISSM_Branch__c = 'Tradicional';
                            
                        }
                        if(objAccount.ISSM_CalculatedInvoicingType__c == 'Canal moderno - Folio de recepción'){
                            System.debug('Cuenta a modificar Canal moderno - Folio de recepción ');
                            objAccount.ISSM_Branch__c = 'Canal moderno - Folio de recepción';
                            
                        }
                        if(objAccount.ISSM_CalculatedInvoicingType__c == 'Canal moderno - Orden de compra'){
                            System.debug('Cuenta a modificar Canal moderno - Orden de compra ');
                            objAccount.ISSM_Branch__c = 'Canal moderno - Orden de compra';
                            
                        }
                        if(objAccount.ISSM_CalculatedInvoicingType__c == 'Perfecta'){
                            System.debug('Cuenta a modificar Perfecta ');
                            objAccount.ISSM_Branch__c = 'Perfecta';
                            
                        }
                    }
                }           
            }
         }
    }   
}