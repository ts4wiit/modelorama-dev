/****************************************************************************************************
    General Information
    -------------------
    Author:     Marco Zúñiga
    email:      mzuniga@avanxo.com
    Company:    Avanxo México
    Project:    ISSM OnCall
    Customer:   Grupo Modelo

    Description: Trigger to invoke the calculateOI method of the OpenItemCreditAmount_thr class
                             and update the Credit Amount in the Account when the records are inserted, updated,
                             undeleted or deleted.
    Events for the Trigger: after insert, after update, after undelete, before delete.
    Related classes: xOpenItemCreditAmount_thr
    Related Object: ISSM_OpenItemB__c 

    Information about changes (versions)
    ================================================================================================
    Number    Dates                 Author                       Description          
    ------    -------------         --------------------------   -----------------------------------
    1.0       27-March-2018          Marco Zúñiga                Class Creation
    ================================================================================================
****************************************************************************************************/
Trigger xOpenItem_tgr on ISSM_OpenItemB__c (after insert, after update, after undelete) {
   if(Trigger.isAfter){
     if(!ISSM_TriggerManager_cls.isInactive(Label.ISSM_xOpenItemTgr)){           
           xOpenItemCreditAmount_thr.calculateOI(Trigger.new);
           ISSM_TriggerManager_cls.inactivate(Label.ISSM_xOpenItemTgr); 
       }
   }
}