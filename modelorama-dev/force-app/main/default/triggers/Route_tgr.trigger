/*********************************************************************************************************************************
	General Information
	-------------------
	author: Andrés Garrido
	email: agarrido@avanxo.com
	company: Avanxo Colombia
	Project: ISSM DSD
	Customer: AbInBev Grupo Modelo
	Description: Trigger to control ONTAP_Route__c events

	Information about changes (versions)
	-------------------------------------
	Number	Dates			 Author						Description
	------	--------		  -------------------------------------
	1.0		22-06-2017		Andrés Garrido				Creation Class
	1.1		30-05-2018		Alberto Gómez				Added: assignApplicationIdToRoute method.
	1.2		19-09-2018		Alberto Gómez				Added: updateAutosalesRecordType method in before insert and before update.
************************************************************************************************************************************/
trigger Route_tgr on ONTAP__Route__c (before delete, before insert, before update) {
	if(Trigger.isBefore) {
		Route_cls IssM = new Route_cls();
		if(Trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('Route_tgr','BeforeInsert')) {

			//Validate that it will be applied only for Presales and Autosales routes.
			ONTAP__Route__c[] lstRoutesToProcess = IssM.filterRoutes(Trigger.new);

			//Update RecordType for Autosales Routes.
			AllMobileOnTapRouteHelperClass.updateAutosalesRecordType(Trigger.new);

			//Assign AllMobileApplication Id to the route.
			AllMobileOnTapRouteHelperClass.assignApplicationIdToRoute(Trigger.new);
			TriggerExecutionControl_cls.setAlreadyDone('Route_tgr','BeforeInsert');
		} else if(Trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('Route_tgr','BeforeUpdate')) {

			//Validate that it will be applied only for Presales and Autosales routes.
			ONTAP__Route__c[] lstRoutesToProcess = IssM.filterRoutes(Trigger.new);

			//Update RecordType for Autosales Routes.
			AllMobileOnTapRouteHelperClass.updateAutosalesRecordType(Trigger.new);

			//Assign AllMobileApplication Id to the route.
			AllMobileOnTapRouteHelperClass.assignApplicationIdToRoute(Trigger.new);
			TriggerExecutionControl_cls.setAlreadyDone('Route_tgr','BeforeUpdate');
		} else if(Trigger.isDelete) {
			IssM.deleteRelatedAccountsByRoute(Trigger.oldMap.keySet());
		}
	}
}