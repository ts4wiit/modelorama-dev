/**********************************************************************************
Desarrollado por: Avanxo Mexico
Autor: Hector Diaz  (HD)
Proyecto:  ISSM  AB Int Bev (Customer Service)
Descripción:   aumento del campo ISSM_Accountant__c en CASO esto cada ves que se crea una tarea con tipo de registro RELLAMADA
Comentarios ":
---------------------------------------------------------------------------------
No.     Fecha      Autor                Descripción
------ ---------- ---------------------------------------------------------------
1.0   24-Agosto-2017 Hector Diaz (HD)      Creador.

***********************************************************************************/
trigger ISSM_NewTaskCounterInCase_tgr on Task (after insert) {
    
    /*
    Metodo que realiza el aumento del campo ISSM_Accountant__c en CASO esto cada ves que se crea una tarea con tipo de registro RELLAMADA
    */
    if(Trigger.IsAfter) { 
        if(Trigger.IsInsert) {
            ISSM_CustomerServiceQuerys_cls objCSQuerys = new ISSM_CustomerServiceQuerys_cls();
            //String RecordTypeTaskId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(System.label.ISSM_Recalled_RecordType).getRecordTypeId();
            String RecordTypeTaskId = ISSM_CreateCaseGlobal_cls.ObtanRecordTypeDeveloperN(System.label.ISSM_Recalled_RecordType);
            
            for(Task objtask :  Trigger.New) {
                if(objtask.RecordTypeId == RecordTypeTaskId) {
                    
                    System.debug('TELECOLLECTION - BANDERA DE CONTROL #1');
                    
                    List<Case> lstCaseTask = objCSQuerys.QueryCase(objtask.WhatId);
                    if(lstCaseTask != null &&!lstCaseTask.isEmpty()) {
                        Case objCaseUpdateCounter = new Case ();
                        for(Case objCase : lstCaseTask) {
                            objCaseUpdateCounter.Id = objtask.WhatId;
                            if(objCase.ISSM_Accountant__c != null){
                                objCaseUpdateCounter.ISSM_Accountant__c = objCase.ISSM_Accountant__c + 1;
                                objCaseUpdateCounter.ISSM_RecallDescription__c = objtask.Description;
                            } else {
                                objCaseUpdateCounter.ISSM_Accountant__c = 1;
                                objCaseUpdateCounter.ISSM_RecallDescription__c = objtask.Description;
                            }
                            
                        }
                        ISSM_TriggerManager_cls.inactivate(System.label.ISSM_CaseTrigger); //Inactivamos el trigger 
                        update objCaseUpdateCounter;    
                    } 
                
                /************************************************************ TELECOLLECTION **********************************************************/ 
                } else {  //Si la tarea no es tipo de registro Recalled
                    
                    String RecordTypeCallId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get(System.label.ISSM_Telecollection_RecordType).getRecordTypeId();
                    List<ONCALL__Call__c> lstCall = objCSQuerys.QueryOncallCall(objtask.WhatId);
                    
                    for (ONCALL__Call__c objCallReceived : lstCall) {
                        if (RecordTypeCallId == objCallReceived.RecordTypeId) {
                            if ((objtask.ISSM_CallEffectiveness__c == System.label.ISSM_CallEffectiveness_Effective) || (objtask.ISSM_CallEffectiveness__c == 'Effective')) {
                                ISSM_NewTaskHandler_cls.UpdateCallEffective(Trigger.new, objCallReceived.ISSM_NumberCallsMade__c);
                            }
                        }
                        if (RecordTypeCallId == objCallReceived.RecordTypeId) {
                            if ((objtask.ISSM_CallEffectiveness__c == System.label.ISSM_CallEffectiveness_NoEffective) || (objtask.ISSM_CallEffectiveness__c == 'Not effective')) {
                                ISSM_NewTaskHandler_cls.UpdateCallNotEffective(Trigger.new, objCallReceived.ISSM_NumberCallsMade__c);
                            }
                        }
                        if (objtask.ISSM_CallEffectiveness__c != System.label.ISSM_CallEffectiveness_Effective) {
                            if (objtask.Status != System.label.ISSM_CallStatus) {
                                ISSM_NewTaskHandler_cls.UpdateCallBackQueue(Trigger.new, objCallReceived.id);
                            }
                        }
                    }
                }
            }
        }
    }
}