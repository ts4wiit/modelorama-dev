/*********************************************************************************************************************************
General Information
-------------------
author:     Cindy Fuentes
email:      c.fuentes.tapia@accenture.com
company:    Accenture
Customer:   Grupo Modelo

Description:
Trigger on Event. 

Information about changes (versions)
==============================================================================================================================
Number    Dates           Author                       Description          
------    --------        --------------------------   -----------------------------------------------------------------------
1.0       06/07/2018      Cindy Fuentes		           Trigger to generate new Informative Session when a new event is created.
If the informative session already exists, it's added to the event.
==============================================================================================================================
*********************************************************************************************************************************/
trigger MDRM_InfoSession_tgr on event (after insert){
    List<MDRM_Informative_Session__c> lstSessions = new List<MDRM_Informative_Session__c>();
    list<event> lstEvent = new list<event>();
    set<string> lstKeyevent = new set<string>();
    map<string,event> mapEvent = new map<string,event>();
    Id businessman =[select ID from recordtype where developername='MDRM_Businessman' and sobjecttype ='event' limit 1].id; 
    for (Event ev : Trigger.new) { 
        //Populate the Informative Session's information with the Event's information
        if(ev.recordtypeid == businessman  && businessman != null){
            string keyEvent = string.valueof(ev.StartDateTime)+string.valueof(ev.EndDateTime)+string.valueof(ev.activitydate)+string.valueof(ev.MDRM_UENContact__c);
            lstKeyevent.add(keyEvent);
            mapEvent.put(keyEvent, new event(id = ev.id));  
            lstEvent.add(ev);
            lstSessions.add(new MDRM_Informative_Session__c(MDRM_UEN_CONTACT__c = ev.MDRM_UENContact__c, 
                                                            MDRM_Start__C =ev.StartDateTime,
                                                            MDRM_IDExt__c =keyEvent,
                                                            MDRM_Date__c = ev.activitydate,
                                                            MDRM_End__C = ev.EndDateTime,
                                                            MDRM_Location__c =ev.MDRM_Location__c,
                                                            MDRM_UEN_Name__c = ev.MDRM_Address__c));
            
            // Upsert the Information unto the new Informative Session or the one that already exists. 
             if(lstSessions.size()>0){ 
            Schema.SObjectField f = MDRM_Informative_Session__c.Fields.MDRM_IDExt__c;
            Database.UpsertResult [] cr = Database.upsert(lstSessions, f, false);
        }
            list<mdrm_informative_session__c> lstInfosessions = new list<mdrm_informative_session__c>([select id, MDRM_IDExt__c from mdrm_informative_session__c
                                                                                                       where MDRM_IDExt__c IN: lstKeyevent]);
            
            // Finally, update the Informative Session field on Event with the corresponding Informative Session.
            if(lstInfosessions != null && lstInfosessions.size() > 0){
                for(MDRM_Informative_session__c infoses: lstInfosessions){
                    mapEvent.get(infoses.MDRM_IDExt__c).MDRM_Informative_Session__c = infoses.id;
                    
                }
                
                list<event> lstUpdateEvent = mapEvent.values();
                update lstUpdateEvent;
            }
        }
    }
}