trigger ISSM_Attachment_tgr on Attachment (before insert, after insert) {
    public final static String RT_NAME_PROSPECT = System.Label.ISSM_ProspectRecordType;
    public final static String ACCOUNT_PHOTO = System.Label.ISSM_AccountPhotoOntap;
    public final static String ACCOUNT_OBJECT_NAME = 'Account';
    
    public final static String NEGOTIATION_STATUS_DOCUMENT_REVIEW = System.Label.ISSM_DocumentsInReview;
    public final static String NEGOTIATION_STATUS_CAPTURADO = System.label.ISSM_ProspectCaptured;
    public final static String NEGOTIATION_STATUS_ALTA_DE_CLIENTE = System.label.ISSM_CreateProcessInProgress;
    
    public final static Map<String, String> STATUS_MAP = new Map<String, String> { NEGOTIATION_STATUS_CAPTURADO=> NEGOTIATION_STATUS_DOCUMENT_REVIEW };
        
    // Customer Service
    String nameFile_str = '';
    List<String> fileLines_lst = new List<String>();
    List<String> headersFile_lst = new List<String>();
    List<String> inputValues_lst = new List<String>();
    
    List<String> inputValuesFinal1_lst = new List<String>();
    List<String> inputValuesFinal2_lst = new List<String>();
    
    String openItemsId = '';
    Double dbAmount = 0.0;
    Decimal sumOutStand_dec = 0.00;
    
    public List<RecordType> RecordType_lst { get; set; }
    public Id rTAccount_id { get; set; }
    
    List<Account> account_lst = new List<Account>();
    List<ONCALL__Call__c> call_lst = new List<ONCALL__Call__c>();
    List<Telecollection_Campaign__c> campaign_lst = new List<Telecollection_Campaign__c>();
    
    String campaignName_str = '';
    Set<String> sapNumber_set = new Set<String>();
    
    String openItems;
    String[] opentItemsList = new List<String>();
    Set<Id> idOpenItemSet = new Set<Id>();
    Set<Id> idOpenItemSet2;
    Map<Id,Set<Id>> idsOpenItemCall = new Map<Id,Set<Id>>();
    
    Map<Id, ISSM_OpenItemB__c> openItemsMap = new Map<Id, ISSM_OpenItemB__c>();
    List<ISSM_OpenItemB__c> openItemUpdate = new List<ISSM_OpenItemB__c>();
    ISSM_OpenItemB__c opItem = new ISSM_OpenItemB__c();
    
    List<ONCALL__Call__c> callInsert_lst = new List<ONCALL__Call__c>();
    
    Set<Account> setAccount = new Set<Account>();
    Decimal SaldoVencidoMayorA_dec = 0;
    Decimal RangoMinimoAceptado_dec = 0;
    Decimal RangoMaximoAceptado_dec = 0;
    
    public ISSM_AppSetting_cs__c oIdQueueWithoutOwner;
    
    for (Attachment attch : Trigger.new) {
        String myIdPrefix = String.valueOf(attch.ParentId).substring(0,3);
        String objectPrefix = Schema.getGlobalDescribe().get('Telecollection_Campaign__c').getDescribe().getKeyPrefix();
        if (Trigger.IsAfter) {
            if (Trigger.IsInsert && (myIdPrefix == objectPrefix)) {
                List<ISSM_ParametersOpenItem__c> paramOI_lst = new List<ISSM_ParametersOpenItem__c>();
                paramOI_lst = [SELECT Id, ISSM_SaldoVencidoMayorA__c, ISSM_RangoMinimoAceptado__c, ISSM_RangoMaximoAceptado__c, ISSM_Active__c
                               FROM ISSM_ParametersOpenItem__c
                               WHERE ISSM_Active__c = true];
                
                if (paramOI_lst.size() > 0) {
                    for (ISSM_ParametersOpenItem__c paramOI : paramOI_lst) {
                        SaldoVencidoMayorA_dec = paramOI.ISSM_SaldoVencidoMayorA__c;
                        RangoMinimoAceptado_dec = paramOI.ISSM_RangoMinimoAceptado__c;
                        RangoMaximoAceptado_dec = paramOI.ISSM_RangoMaximoAceptado__c;
                    }
                }
                
                // Obtenemos el Record Type "Account"
                RecordType_lst = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Account' LIMIT 1];
                for (RecordType rt : RecordType_lst) { rTAccount_id = rt.Id; }
                
                nameFile_str = ISSM_Attachment_thr.blobToString(attch.body, 'UTF-8');
                
                // Quitamos los saltos de linea de cada registro
                fileLines_lst = nameFile_str.split('\n');
                
                List<String> inputValuesFinal_lst = new List<String>();
                
                for (Integer i = 0; i < fileLines_lst.size(); i ++) {
                    inputValues_lst = fileLines_lst[i].split(',');
                    inputValuesFinal_lst.add(inputValues_lst[0]);
                }
                
                // Buscamos los clientes en base a los números SAP cargados en el archivo CSV
                if (!Test.isRunningTest()) {
                    account_lst = [SELECT Id, Name, ONTAP__SAP_Number__c, ONCALL__Risk_Category__c,ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c, ISSM_LastPromisePaymentDate__c, (SELECT Id, ISSM_Account__c, ISSM_DocumentId__c, ISSM_Amounts__c, ISSM_ExpirationDays__c, ISSM_Debit_Credit__c FROM Open_Items__r) FROM Account WHERE RecordTypeId =: rTAccount_id AND ONTAP__SAP_Number__c IN: inputValuesFinal_lst];
                } else {
                    account_lst = [SELECT Id, Name, ONTAP__SAP_Number__c, ONCALL__Risk_Category__c,ISSM_LastContactDate__c, ISSM_LastPaymentPlanDate__c, ISSM_LastPromisePaymentDate__c,
                                   (SELECT Id, ISSM_Account__c, ISSM_DocumentId__c, ISSM_Amounts__c, ISSM_ExpirationDays__c, ISSM_Debit_Credit__c FROM Open_Items__r)
                                   FROM Account];
                }
                
                if (!account_lst.isEmpty()) {
                    // Buscamos el nombre de la campaña de donde se cargó el archivo
                    campaign_lst = [SELECT Id, Name FROM Telecollection_Campaign__c WHERE Id =: attch.ParentId];
                    if (!campaign_lst.isEmpty()) {
                        for (Telecollection_Campaign__c camp : campaign_lst) {
                            campaignName_str = camp.Name;
                        }
                    }
                    
                    oIdQueueWithoutOwner = [SELECT ISSM_IdQueueWithoutOwner__c, ISSM_IdQueueFriendModel__c, ISSM_IdQueueTelecolletion__c FROM ISSM_AppSetting_cs__c LIMIT 1];
                    String RecordTypeTelecollection = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
                    
                    if (!account_lst.isEmpty()) {
                        for (Account acc : account_lst) {
                            try {
                                setAccount.add(acc);
                                
                                if (!acc.Open_Items__r.isEmpty()) {
                                    
                                    for (ISSM_OpenItemB__c objOpenI : acc.Open_Items__r) {
                                        
                                        if (objOpenI.ISSM_ExpirationDays__c > RangoMaximoAceptado_dec) { setAccount.remove(acc); break; }
                                        
                                        if (objOpenI.ISSM_ExpirationDays__c < RangoMinimoAceptado_dec && objOpenI.ISSM_Debit_Credit__c == System.label.ISSM_Debit) { setAccount.remove(acc); break; }
                                        
                                        if (objOpenI.ISSM_Amounts__c <= SaldoVencidoMayorA_dec) { setAccount.remove(acc); break; }
                                    }
                                    
                                    // Si la cuenta no tiene Open Items elimina el registro sel SET
                                } else { setAccount.remove(acc); }   
                                
                            } catch(QueryException QE) { setAccount.remove(acc); }
                        }
                        
                        List<String> documentTypeOrderI_lst = new List<String>();
                        for (ISS_DocumentTypeOpenItems__c objDocumentTypeOrderI : [SELECT Id, Name FROM ISS_DocumentTypeOpenItems__c WHERE ISSM_Active__c = true]) { documentTypeOrderI_lst.add(objDocumentTypeOrderI.Name); }
                        
                        Set<ID> idsAccounts_set = new ISSM_Account_cls().getMapAccountId((List<Account>)account_lst).keySet();
                        List<ISSM_OpenItemB__c> lstOpenItem = new ISSM_CustomerServiceQuerys_cls().getOpenItems(idsAccounts_set, documentTypeOrderI_lst);
                        
                        for (Account account : setAccount) {
                            
                            for (ISSM_OpenItemB__c objIteraOI : lstOpenItem) {
                                dbAmount = 0.0;
                                if (objIteraOI.ISSM_Account__c == account.Id) {
                                    sumOutStand_dec += objIteraOI.ISSM_Amounts__c;
                                }
                                openItemsId += objIteraOI.Id + ';';
                            }
                            openItemsId = openItemsId.removeEnd(';');
                            
                            ONCALL__Call__c call = new ONCALL__Call__c();
                            call.Name = account.Name;
                            call.ONCALL__POC__c = account.Id;
                            call.Telecollection_Campaign__c = attch.ParentId;
                            call.ISSM_CampaingName__c = campaignName_str;
                            call.ONCALL__Date__c = System.now();
                            call.OwnerId = oIdQueueWithoutOwner.ISSM_IdQueueTelecolletion__c;
                            call.ISSM_Active__c = true;
                            call.ISSM_OutstandingBalanceAmount__c = sumOutStand_dec;
                            call.RecordTypeId = RecordTypeTelecollection;
                            call.ISSM_Country__c = System.label.Country_CallOpenItem;
                            callInsert_lst.add(call);
                            
                            sumOutStand_dec = 0.00;
                        }
                    }
                }
                if (!callInsert_lst.isEmpty()) {
                    insert callInsert_lst;
                }
            }
        }
    }
    
    
    if (trigger.isAfter && trigger.isInsert) {
        String myIdPrefix;
        String objectPrefix;
        Set<Id> accountIds_set = new Set<Id>();
        Set<Id> attchIds_set = new Set<Id>();
        Map<Id,Account> accountMap = new Map<Id,Account>();
        List<Account> accountsToUpdate_lst = new List<Account>();
        
        Id prospectRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: RT_NAME_PROSPECT].Id;
        for (Attachment attachment_obj : trigger.new) {
            myIdPrefix = String.valueOf(attachment_obj.ParentId).substring(0,3);
            objectPrefix = Schema.getGlobalDescribe().get(ACCOUNT_OBJECT_NAME).getDescribe().getKeyPrefix();
            if(myIdPrefix == objectPrefix) {
                accountIds_set.add(attachment_obj.ParentId);
                attchIds_set.add(attachment_obj.Id);
            }
        }
        for (Account acc_obj : [SELECT Id,ONTAP__Negotiation_Status__c FROM Account WHERE Id IN: accountIds_set AND recordTypeId =: prospectRecordTypeId LIMIT 10000]) {
            accountMap.put(acc_obj.id,acc_obj);
        }
        
        for (Id att_id : attchIds_set) { Attachment attchTmp = trigger.newMap.get(att_id);
            if (attchTmp.Name != ACCOUNT_PHOTO && attchTmp.ParentId != null) {
                if (accountMap.get(attchTmp.ParentId)!=null) {
                    if (STATUS_MAP.get(accountMap.get(attchTmp.ParentId).ONTAP__Negotiation_Status__c)  != null ) {
                        accountMap.get(attchTmp.ParentId).ONTAP__Negotiation_Status__c = STATUS_MAP.get(accountMap.get(attchTmp.ParentId).ONTAP__Negotiation_Status__c);
                        accountsToUpdate_lst.add(accountMap.get(attchTmp.ParentId));
                    }
                }
            }
        }
        if (!accountsToUpdate_lst.isEmpty()) { update accountsToUpdate_lst; }
    }
}