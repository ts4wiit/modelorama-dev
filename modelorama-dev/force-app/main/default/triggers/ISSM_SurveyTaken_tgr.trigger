/**********************************************************************************
Developed by: Avanxo Mexico
Author: Rodrigo RESENDIZ  (RR)
Project:  ISSM  AB Int Bev (OnTap)
Description:  
---------------------------------------------------------------------------------
Version     Date      Author                Description
------ ---------- ---------------------------------------------------------------
1.0     10-Julio-2017 Rodrigo RESENDIZ (RR)      Creator.
***********************************************************************************/

trigger ISSM_SurveyTaken_tgr on ONTAP__SurveyTaker__c (after update) {
    if(trigger.isAfter && trigger.isUpdate){
        ISSM_SurveyTakenTgrHnd_cls.afterUpdate(trigger.newMap, trigger.oldMap);
    }
}