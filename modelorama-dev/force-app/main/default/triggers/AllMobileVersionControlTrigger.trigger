/**
 * Trigger for the AllMobileVersionControl__c object.
 * <p /><p />
 * @author Alberto Gómez
 */
trigger AllMobileVersionControlTrigger on AllMobileVersionControl__c (before insert) {
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			AllMobileVersionControlHelperClass.initiateInsertVersionControl(Trigger.new);
		}
	}
}